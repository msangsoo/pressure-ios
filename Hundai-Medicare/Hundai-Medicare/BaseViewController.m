//
//  BaseViewController.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController () {
    NSDate *viewLoadDate;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view endEditing:YES];

    server = [[ServerCommunication alloc] init];
//    [self.navigationItem.backBarButtonItem setTitle:@" "];
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor whiteColor],
//       NSFontAttributeName:[UIFont fontWithName:@"NanumSquareB" size:17]}];
    
    _model = [Model instance];
    
//    pageLog = [AppPageLog instance];
}

- (void)viewWillAppear:(BOOL)animated {
//    viewLoadDate = [NSDate date];
}

- (void)viewDidAppear:(BOOL)animated {
    if (![[Utils getUserDefault:NAVI_TAB_BAR_MOVE] isEqualToString:@""]) {
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[Utils getUserDefault:NAVI_TAB_BAR_MOVE], @"index", nil];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:NAVI_TAB_BAR_MOVE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
//    NSDate *viewDidDate = [[NSDate alloc] init];
//    double interval = viewDidDate.timeIntervalSinceReferenceDate - viewLoadDate.timeIntervalSinceReferenceDate;
//    NSLog(@"life interval = %f", interval);
//    NSLog(@"viewController = %@",  self.class);
}

#pragma mark - Navigation

- (void)popBack:(NSString *)viewClass {
    for (UIViewController *vw in self.navigationController.viewControllers) {
        NSString *className = [NSString stringWithFormat:@"%@", vw.class];
        if ([className isEqualToString:viewClass]) {
            [self.navigationController popToViewController:vw animated:NO];
            return;
        }
    }
}

@end
