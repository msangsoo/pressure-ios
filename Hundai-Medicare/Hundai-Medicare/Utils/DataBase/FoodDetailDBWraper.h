//
//  FoodCalorieDBWraper.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "CHCSVParser.h"
#import "Utils.h"

@interface FoodDetailDBWraper : MedicareDataBase

- (BOOL) insertFoodDetailDb:(NSDictionary*)dicDetail;
- (BOOL) initFoodDetailDb:(NSArray*)arrDetail idx:(NSString*)idx regdate:(NSString *)regdate;
- (void) insert:(NSArray*)arrDetail;

-(NSMutableArray *) getUserResult:(NSString *)idx;


@end
