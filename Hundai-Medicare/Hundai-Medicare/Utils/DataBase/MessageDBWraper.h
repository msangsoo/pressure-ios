//
//  MessageDBWraper.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 13..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"

@interface MessageDBWraper : MedicareDataBase

- (void)DeleteDb:(NSString*)idx;
- (void)AllDeleteDB;
- (void)insert:(NSArray*)datas;
- (NSArray *)getResult:(NSString *)type;
- (NSArray *)getResultFast:(NSString *)type;
@end
