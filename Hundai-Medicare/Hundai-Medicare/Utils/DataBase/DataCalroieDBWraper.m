//
//  DataCalroieDBWraper.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "DataCalroieDBWraper.h"

@implementation DataCalroieDBWraper

-(id) init {
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    
    return self;
}

- (void)DeleteDb:(NSString *)idx {
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE DATA_CALROIE_ACTIVE_SEQ=='%@'", TABLE_DATA_CALROIE, idx];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}

- (void)insert:(NSArray *)items {
    NSString *sqlHeader  = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES ",
                            TABLE_DATA_CALROIE, DATA_CALROIE_DATA_SN, DATA_CALROIE_ACTIVE_SEQ, DATA_CALROIE_ACTIVE_TIME, DATA_CALROIE_CALORY, DATA_CALROIE_IS_SERVER_REGIST, DATA_CALROIE_REG_DE];
    
    
    for (NSDictionary *item in items) {
        NSString *reg_de = [item objectForKey:@"reg_de"];
        if (reg_de) {
            reg_de = [Utils getTimeFormat:reg_de beTime:@"yyyyMMdd" afTime:@"yyyy-MM-dd"];
        }
        
        NSString *value = [NSString stringWithFormat:@"('%@', '%@', '%@', '%@', '%@', '%@')",
                           [item objectForKey:@"data_sn"],
                           [item objectForKey:@"active_seq"],
                           [item objectForKey:@"active_time"],
                           [item objectForKey:@"calory"],
                           @"Y",
                           [NSString stringWithFormat:@"%@ %@:00", reg_de, [item objectForKey:@"active_time"]]];
        
        NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];
        
        NSLog(@"sql : %@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"InsertDb Error");
        }else {
            NSLog(@"InsertDb OK");
        }
    }
}

/**
 * 걸음 일 그래프
 **/
- (NSArray *)getResultDay:(NSString *)nDate {
    
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for (int i = 1 ; i < 24 ; i ++) {
        strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('$', %@) as integer)=%d THEN %@ End), 0) as h%d, ", strsql, DATA_CALROIE_REG_DE, i, DATA_CALROIE_CALORY, i];
    }
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('$', %@) as integer)=24 THEN %@ End), 0) as h24 ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_DATA_CALROIE];
    
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' ", strsql, DATA_CALROIE_REG_DE, nDate, nDate];
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer)", strsql, DATA_CALROIE_REG_DE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%H"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%d"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *h2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *h3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        
        NSArray *arrRtn2 = @[@([h1 intValue]), @([h2 intValue]), @([h3 intValue]), @([h4 intValue]), @([h5 intValue]), @([h6 intValue]),
                             @([h7 intValue]), @([h8 intValue]), @([h9 intValue]), @([h10 intValue]), @([h11 intValue]), @([h12 intValue]),
                             @([h13 intValue]), @([h14 intValue]), @([h15 intValue]), @([h16 intValue]), @([h17 intValue]), @([h18 intValue]),
                             @([h19 intValue]), @([h20 intValue]), @([h21 intValue]), @([h22 intValue]), @([h23 intValue]), @([h24 intValue])];
        
        [arrResult addObject:arrRtn2];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0)];
        
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}

/**
 * 걸음 주 그래프
 **/
- (NSArray *)getResultWeek:(NSString *)sDate eDate:(NSString *)eDate {
    
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 0 THEN %@ End),0) as W1a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];    //일요일부터 시작
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 1 THEN %@ End),0) as W2a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 2 THEN %@ End),0) as W3a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 3 THEN %@ End),0) as W4a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 4 THEN %@ End),0) as W5a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 5 THEN %@ End),0) as W6a,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$', %@) as integer) WHEN 6 THEN %@ End),0) as W7a ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql = [NSString stringWithFormat:@"%@ FROM %@", strsql, TABLE_DATA_CALROIE];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00:00' and '%@ 23:59:59'", strsql, DATA_CALROIE_REG_DE, sDate, eDate];

    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%w"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        NSArray *arrRtn = @[@([w1 intValue]), @([w2 intValue]), @([w3 intValue]), @([w4 intValue]), @([w5 intValue]), @([w6 intValue]), @([w7 intValue])];
        [arrResult addObject:arrRtn];
    }
    
    if(arrResult.count == 0){
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0),@(0)];
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


/**
 * 걸음 월 그래프
 **/
- (NSArray *)getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth {
    
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"Select "];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 1 THEN %@ End), 0) as D1,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 2 THEN %@ End), 0) as D2,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 3 THEN %@ End), 0) as D3,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 4 THEN %@ End), 0) as D4,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 5 THEN %@ End), 0) as D5,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 6 THEN %@ End), 0) as D6,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 7 THEN %@ End), 0) as D7,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 8 THEN %@ End), 0) as D8,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 9 THEN %@ End), 0) as D9,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 10 THEN %@ End), 0) as D10,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 11 THEN %@ End), 0) as D11,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 12 THEN %@ End), 0) as D12,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 13 THEN %@ End), 0) as D13,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 14 THEN %@ End), 0) as D14,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 15 THEN %@ End), 0) as D15,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 16 THEN %@ End), 0) as D16,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 17 THEN %@ End), 0) as D17,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 18 THEN %@ End), 0) as D18,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 19 THEN %@ End), 0) as D19,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 20 THEN %@ End), 0) as D20,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 21 THEN %@ End), 0) as D21,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 22 THEN %@ End), 0) as D22,", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 23 THEN %@ End), 0) as D23, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 24 THEN %@ End), 0) as D24, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 25 THEN %@ End), 0) as D25, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 26 THEN %@ End), 0) as D26, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 27 THEN %@ End), 0) as D27, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 28 THEN %@ End), 0) as D28, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 29 THEN %@ End), 0) as D29, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 30 THEN %@ End), 0) as D30, ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE cast(strftime('$d', %@) as integer) WHEN 31 THEN %@ End), 0) as D31  ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_CALORY];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@", strsql, TABLE_DATA_CALROIE];
    strsql  = [NSString stringWithFormat:@"%@ WHERE cast(strftime('$Y', %@) as integer)=%@ and cast(strftime('$m', %@) as integer)=%@", strsql, DATA_CALROIE_REG_DE, nYear, DATA_CALROIE_REG_DE, nMonth];
    strsql  = [NSString stringWithFormat:@"%@ Group by strftime('$Y', %@), strftime('$m', %@) ", strsql, DATA_CALROIE_REG_DE, DATA_CALROIE_REG_DE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$d" withString:@"%d"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$Y" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$m" withString:@"%m"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d25 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d26 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d27 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d28 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d29 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d30 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d31 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        
        NSArray *arrRtn = @[@([d1 intValue]), @([d2 intValue]), @([d3 intValue]), @([d4 intValue]), @([d5 intValue]), @([d6 intValue]), @([d7 intValue]), @([d8 intValue]), @([d9 intValue]), @([d10 intValue]),
                            @([d11 intValue]), @([d12 intValue]), @([d13 intValue]), @([d14 intValue]), @([d15 intValue]), @([d16 intValue]), @([d17 intValue]), @([d18 intValue]), @([d19 intValue]), @([d20 intValue]),
                            @([d21 intValue]), @([d22 intValue]), @([d23 intValue]), @([d24 intValue]), @([d25 intValue]), @([d26 intValue]), @([d27 intValue]), @([d28 intValue]), @([d29 intValue]), @([d30 intValue]),
                            @([d31 intValue])
                            ];
        [arrResult addObject:arrRtn];
    }
    
    if(arrResult.count == 0){
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0),@(0),@(0),@(0),@(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),@(0),@(0),@(0),@(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),@(0),@(0),@(0),@(0),
                            @(0)];
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}
@end
