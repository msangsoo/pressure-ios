
//
//  MessageDBWraper.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 13..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "MessageDBWraper.h"

@implementation MessageDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    
    return self;
}

- (void)DeleteDb:(NSString *)idx {
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE idx=='%@'", TABLE_MESSAGE, idx];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}

- (void)AllDeleteDB {
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_MESSAGE];
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"All DeleteDb OK");
    }
}

- (void)insert:(NSArray *)datas {
    
    NSString *sqlHeader  = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) VALUES ", TABLE_MESSAGE, MSG_IDX, MSG_INFRA_TY, MSG_MESSGAE, MSG_REGDATE];
    
    for( int i = 0 ; i < datas.count ; i ++){
        
        if ([[[datas objectAtIndex:i] objectForKey:@"message_de"] length] != 12
            && [[[datas objectAtIndex:i] objectForKey:@"regdate"] length] != 12) continue;
        
        NSString *reg_de = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"message_de"]) {
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"message_de"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }else{
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"regdate"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }
        
        NSString *msg = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"message_cn"]) {
            msg = [[datas objectAtIndex:i] objectForKey:@"message_cn"];
        }else{
            msg = [[datas objectAtIndex:i] objectForKey:@"message"];
        }
        
        
        NSString *value = [NSString stringWithFormat:@"('%@', '%@', '%@', '%@')",
                           [[datas objectAtIndex:i] objectForKey:MSG_IDX],
                           [[datas objectAtIndex:i] objectForKey:MSG_INFRA_TY],
                           msg,
                           reg_de];
        
        NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];
        
        NSLog(@"sql : %@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"InsertDb Error");
        }else {
            NSLog(@"InsertDb OK");
        }
    }
    
}

- (NSArray *)getResult:(NSString *)type {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ ",
                         MSG_IDX, MSG_INFRA_TY, MSG_MESSGAE, MSG_IS_SERVER_REGIST, MSG_REGDATE,TABLE_MESSAGE];
    
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ = %@ ", strsql, MSG_INFRA_TY, type];
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC LIMIT 100",strsql, MSG_REGDATE, MSG_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *infra_ty = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *msg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,2)];
        NSString *regist = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx",  infra_ty, @"infra_ty", msg, @"msg", regist, @"regist", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}



- (NSArray *)getResultFast:(NSString *)type {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ ",
                         MSG_IDX, MSG_INFRA_TY, MSG_MESSGAE, MSG_IS_SERVER_REGIST, MSG_REGDATE,TABLE_MESSAGE];
    
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ = %@ ", strsql, MSG_INFRA_TY, type];
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC LIMIT 1",strsql, MSG_REGDATE, MSG_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *infra_ty = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *msg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,2)];
        NSString *regist = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx",  infra_ty, @"infra_ty", msg, @"msg", regist, @"regist", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

@end
