//
//  PresuDBWraper.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 3. 30..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"


@interface PresuDBWraper : MedicareDataBase

- (void) DeleteDb:(NSString*)idx;
- (void) UpdateDb:(NSString*)idx
 arterialPressure:(NSString*)arterialPressure
diastolicPressure:(NSString*)diastolicPressure
        pulseRate:(NSString*)pulseRate
 systolicPressure:(NSString*)systolicPressure
         drugname:(NSString*)drugname
           reg_de:(NSString*)reg_de;
- (void) insert:(NSArray*)datas;
- (NSArray*) getResult;
- (NSArray*) getBottomResult:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultMain:(NSString *)nDate;

- (NSArray*) getResultDay:(NSString *)nDate;
- (NSArray*) getResultWeek:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth;
- (NSArray*) getResultYear:(NSString *)nYear;

- (NSArray*) getResultDrugDay:(NSString *)nDate;
- (NSArray*) getResultWeekDrug:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultMonthDrug:(NSString *)nYear nMonth:(NSString *)nMonth;

@end
