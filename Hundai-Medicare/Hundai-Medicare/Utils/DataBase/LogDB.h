//
//  LogDB.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 11. 9..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"

@interface LogDB : MedicareDataBase

- (void)DeleteDb;
- (void)insertDb:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod time:(int)time;
- (NSArray *)allSelectDb;

+ (void)insertDb:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod;
+ (NSArray *)todayAllSelect;
+ (void)LogDataDelete;

@end
