//
//  LogDB.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 11. 9..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LogDB.h"

@implementation LogDB

- (id)init {
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    
    return self;
}

- (void)DeleteDb {
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@", TB_LOG];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}

- (void)insertDb:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod time:(int)time {
    NSString *strsql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES", TB_LOG, LOG_L_COD, LOG_M_COD, LOG_S_COD, LOG_TIME, LOG_COUNT, LOG_REGDATE];
    
    NSDate *date = [NSDate date];
    NSString *qry = [NSString stringWithFormat:@"%@ ('%@', '%@', '%@', '%d', '%@', '%@'); ",
                     strsql, l_cod, m_cod, s_cod, time, @"1", date];
    
    if (sqlite3_exec(database, [qry UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"INSERT HealthInfo failed %@",updateStmt);
    }else {
        NSLog(@"INSERT HealthInfo strsql = %@", strsql);
    }
}

- (NSArray *)allSelectDb {
    sqlite3_stmt *statement;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMd"];
    NSString *toDate = [format stringFromDate:[NSDate date]];
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT l_cod, CASE(m_cod) WHEN '' THEN '' ELSE l_cod|m_cod END AS m_cod,"];
    strsql  = [NSString stringWithFormat:@" %@ CASE(s_cod) WHEN '' THEN '' ELSE l_cod|m_cod|s_cod END AS s_cod,", strsql];
    strsql  = [NSString stringWithFormat:@" %@ CASE sum(time) WHEN 0 THEN 0 ELSE sum(time) END AS tiem,", strsql];
    strsql  = [NSString stringWithFormat:@" %@ CASE sum(count) WHEN 0 THEN 0 ELSE sum(count) END AS count, strftime('#$^', regdate) as regdate", strsql];
    strsql  = [NSString stringWithFormat:@" %@ FROM tb_log", strsql];
    strsql  = [NSString stringWithFormat:@" %@ WHERE strftime('#$^', regdate) < '%@'", strsql, toDate];
    strsql  = [NSString stringWithFormat:@" %@ GROUP BY strftime('#$^', regdate), l_cod, m_cod, s_cod", strsql];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
    
    const char *sql = [strsql UTF8String];
    NSLog(@"tb_log allSelectDb sql = %@", strsql);
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement) == SQLITE_ROW) {
        NSString *l_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *m_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *s_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *time = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *count = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        
        NSDictionary *dicTemp;
        if ([s_cod isEqualToString:@""]) {
            dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                       l_cod, @"L_CODE", m_cod, @"M_CODE",
                       time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
        }else if([m_cod isEqualToString:@""] && [s_cod isEqualToString:@""]) {
            dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                       l_cod, @"L_CODE",
                       time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
        }else {
            dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                                     l_cod, @"L_CODE", m_cod, @"M_CODE", s_cod, @"S_CODE",
                                     time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
        }
        
        NSLog(@"dicTemp = %@", dicTemp);
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    return arrResult;
}

+ (void)insertDb:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
    }else {
        NSString *strsql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES", TB_LOG, LOG_L_COD, LOG_M_COD, LOG_S_COD, LOG_TIME, LOG_COUNT, LOG_REGDATE];
        
        NSDate *date = [NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        date = [date dateByAddingTimeInterval:-60 * 60 * 24];
        [format setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        NSString *qry = [NSString stringWithFormat:@"%@ ('%@', '%@', '%@', '%d', '%@', '%@'); ",
                         strsql, l_cod, m_cod, s_cod, 0, @"1", [format stringFromDate:date]];
        
        if (sqlite3_exec(database, [qry UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"INSERT HealthInfo failed %@",updateStmt);
        }else {
            NSLog(@"INSERT HealthInfo strsql = %@", strsql);
        }
    }
}

+ (NSArray *)todayAllSelect {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        
        return @[];
    }else {
        sqlite3_stmt *statement;
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyyMMdd"];
        NSString *toDate = [format stringFromDate:[NSDate date]];
        //        l_cod| //l_cod|m_cod|
        NSString *strsql  = [NSString stringWithFormat:@"SELECT l_cod, CASE(m_cod) WHEN '' THEN '' ELSE m_cod END AS m_cod,"];
        strsql  = [NSString stringWithFormat:@" %@ CASE(s_cod) WHEN '' THEN '' ELSE s_cod END AS s_cod,", strsql];
        strsql  = [NSString stringWithFormat:@" %@ CASE sum(time) WHEN 0 THEN 0 ELSE sum(time) END AS tiem,", strsql];
        strsql  = [NSString stringWithFormat:@" %@ CASE sum(count) WHEN 0 THEN 0 ELSE sum(count) END AS count, strftime('#$^', regdate) as regdate", strsql];
        strsql  = [NSString stringWithFormat:@" %@ FROM tb_log", strsql];
        strsql  = [NSString stringWithFormat:@" %@ WHERE strftime('#$^', regdate) < '%@'", strsql, toDate];
        strsql  = [NSString stringWithFormat:@" %@ GROUP BY strftime('#$^', regdate), l_cod, m_cod, s_cod", strsql];
        
        strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
        strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
        strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
        
        const char *sql = [strsql UTF8String];
        NSLog(@"tb_log allSelectDb sql = %@", strsql);
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            return nil;
        }
        
        NSMutableArray *arrResult = [[NSMutableArray alloc] init];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSString *l_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
            NSString *m_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
            NSString *s_cod = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
            NSString *time = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
            NSString *count = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
            NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
            
            NSDictionary *dicTemp;
            if ([s_cod isEqualToString:@""]) {
                dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                           l_cod, @"L_CODE", m_cod, @"M_CODE",
                           time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
            }else if([m_cod isEqualToString:@""] && [s_cod isEqualToString:@""]) {
                dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                           l_cod, @"L_CODE",
                           time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
            }else {
                dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                           l_cod, @"L_CODE", m_cod, @"M_CODE", s_cod, @"S_CODE",
                           time, @"R_TIME", count, @"R_COUNT", regdate, @"R_DATE", nil];
            }
            
            NSLog(@"dicTemp = %@", dicTemp);
            [arrResult addObject:dicTemp];
        }
        
        sqlite3_finalize(statement);
        return arrResult;
    }
    
    return @[];
}

+ (void)LogDataDelete {
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
    
    }else {
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@", TB_LOG];
        NSLog(@"sql:%@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"Error");
        }else {
            NSLog(@"DeleteDb OK");
        }
    }
}

@end
