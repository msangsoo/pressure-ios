//
//  MedicareDataBase.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MedicareDataBase.h"

@implementation MedicareDataBase

- (id)init {
    self = [super init];
    if( self == nil ) return nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, DBFILE_NAME];
    NSString *fileURL = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:DBFILE_NAME];
    [fileManager copyItemAtPath:fileURL toPath:filePath error:nil];
    BOOL bExist = [fileManager fileExistsAtPath:filePath];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    
    if( YES == bExist )
    {
        [self createTableMessage];
        [self createTableWalk];
        [self createTableDataCalroie];
        [self createTableSugar];
        [self createTablePresu];
        [self createTableWeight];
        [self createTableFoodMain];
        [self createTableFoodDetail];
        [self createTableRecentSearch];
        //        [self createTableFoodCalorie];
        [self createTableLog];
    }
    
    return self;
}

- (void)createTableMessage {
    NSString *messageMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ CHARACTER(1) NULL ,%@ VARCHAR(1000) NULL, %@ BOOLEAN NULL, %@ DATETIME DEFAULT CURRENT_TIMESTAMP)", TABLE_MESSAGE, MSG_IDX, MSG_INFRA_TY, MSG_MESSGAE, MSG_IS_SERVER_REGIST, MSG_REGDATE];
    
    if (sqlite3_exec(database, [messageMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_MESSAGE, sqlite3_errmsg(database));
    }
}

- (void)createTableWalk {
    NSString *walkMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL,%@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ CHARACTER(1) NULL, %@ BOOLEAN, %@ DATETIME DEFAULT CURRENT_TIMESTAMP)",TABLE_WALK, WALK_IDX, WALK_CALORY, WALK_DISTANCE, WALK_STEP, WALK_HEARTRATE, WALK_STRESS, WALK_REGTYPE, WALK_IS_SERVER_REGIST,WALK_REGDATE];
    
    if (sqlite3_exec(database, [walkMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_WALK, sqlite3_errmsg(database));
    }
}

- (void)createTableDataCalroie {
    NSString *dataCalroieQuery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ CHARACTER(14), %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL,  %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ BOOLEAN, %@ VARCHAR(15) NULL)",TABLE_DATA_CALROIE, DATA_CALROIE_IDX, DATA_CALROIE_SPORT_SN, DATA_CALROIE_DATA_SN, DATA_CALROIE_ACTIVE_SEQ, DATA_CALROIE_ACTIVE_TIME, DATA_CALROIE_CALORY, DATA_CALROIE_IS_SERVER_REGIST, DATA_CALROIE_REG_DE];
    
    if (sqlite3_exec(database, [dataCalroieQuery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_DATA_CALROIE, sqlite3_errmsg(database));
    }
}

- (void)createTableSugar {
    NSString *sugarMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ VARCHAR(15) NULL, %@ CHARACTER(1) NULL, %@ VARCHAR(1) NULL,%@ VARCHAR(30) NULL, %@ CHARACTER(1) NULL, %@ BOOLEAN, %@ DATETIME DEFAULT CURRENT_TIMESTAMP)",TABLE_SUGAR, SUGAR_IDX, SUGAR_SUGAR, SUGAR_HILOW, SUGAR_BEFORE, SUGAR_DRUGNAME, SUGAR_REGTYPE, SUGAR_IS_SERVER_REGIST, SUGAR_REGDATE];
    
    if (sqlite3_exec(database, [sugarMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_SUGAR, sqlite3_errmsg(database));
    }
}

- (void)createTablePresu {
    
    NSString *sugarMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL,%@ VARCHAR(30) NULL, %@ CHARACTER(1) NULL, %@ BOOLEAN, %@ BOOLEAN,%@ DATETIME DEFAULT CURRENT_TIMESTAMP)",TABLE_PRESU, PRESU_IDX, PRESU_ARTERIALPRESSURE, PRESU_DIASTOLICPRESSURE, PRESU_PULSERATE, PRESU_SYSTOLICPRESSURE, PRESU_DRUGNAME, PRESU_REGTYPE, PRESU_IS_SERVER_REGIST, PRESU_REGDATE];
    
    if (sqlite3_exec(database, [sugarMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_PRESU, sqlite3_errmsg(database));
    }
}

- (void)createTableWeight {
    
    NSString *weightMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(14) PRIMARY KEY, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL,%@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(15) NULL, %@ VARCHAR(1) NULL, %@ BOOLEAN, %@ DATETIME DEFAULT CURRENT_TIMESTAMP)",
                                 TABLE_WHIGHT, WEIGHT_IDX, WEIGHT_BMR, WEIGHT_BODYWATER, WEIGHT_BONE, WEIGHT_FAT, WEIGHT_HEARTRATE, WEIGHT_MUSCLE, WEIGHT_OBESITY, WEIGHT_WEIGHT, WEIGHT_REGTYPE, WEIGHT_IS_SERVER_REGIST, WEIGHT_REGDATE ];
    if (sqlite3_exec(database, [weightMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_WHIGHT, sqlite3_errmsg(database));
    }
}

- (void)createTableFoodMain {
    
    NSString *foodMainquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ VARCHAR(20), %@ VARCHAR(20), %@ VARCHAR(20), %@ VARCHAR(20),%@ VARCHAR(100), %@ datetime)",
                               TABLE_FOOD_MAIN, FOODMAIN_IDX, FOODMAIN_AMOUNTTIME, FOODMAIN_MEALTTYPE, FOODMAIN_CALORIE, FOODMAIN_PICTURE, FOODMAIN_REGTYPE];
    if (sqlite3_exec(database, [foodMainquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_FOOD_MAIN, sqlite3_errmsg(database));
    }
}

- (void)createTableFoodDetail {
    
    NSString *foodDetailquery = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ VARCHAR(20), %@ VARCHAR(20), %@ VARCHAR(20), %@ datetime)",
                                 TABLE_FOOD_DETAIL, FOODDETAIL_IDX, FOODDETAIL_FOODCODE, FOODDETAIL_FORPEOPLE, FOODDETAIL_REGTYPE];
    if (sqlite3_exec(database, [foodDetailquery UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_FOOD_DETAIL, sqlite3_errmsg(database));
    }
}

- (void)createTableFoodCalorie {

    NSString *foodCalorie = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ INTEGER PRIMARY KEY, %@ VARCHAR(30), %@ VARCHAR(150), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30), %@ VARCHAR(30))",
                             TABLE_FOOD_CALORIE, FOOD_CODE, FOOD_KIND, FOOD_NAME, FOOD_GRAM, FOOD_UNIT, FOOD_CALORIE, FOOD_CARBOHYDRATE, FOOD_PROTEIN, FOOD_FAT, FOOD_SUGARS, FOOD_SALT, FOOD_CHOLESTEROL, FOOD_SATURATED, FOOD_TRANSQUANTI, FORPEOPLE ];
    if (sqlite3_exec(database, [foodCalorie UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_FOOD_CALORIE, sqlite3_errmsg(database));
    }
}

- (void)createTableRecentSearch {
    NSString *recentSearch = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ CHARACTER(30) PRIMARY KEY, %@ DATETIME DEFAULT CURRENT_TIMESTAMP)",
                             TABLE_RECENT_SEARCH, RECENT_SEARCH_KEYWORD, RECENT_SEARCH_TIMESTAMP];
    if (sqlite3_exec(database, [recentSearch UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TABLE_RECENT_SEARCH, sqlite3_errmsg(database));
    }
}

- (void)createTableLog {
    NSString *log = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ TEXT, %@ TEXT, %@ TEXT, %@ INTEGER, %@ INTEGER, %@ TEXT)",
                              TB_LOG, LOG_L_COD, LOG_M_COD, LOG_S_COD, LOG_TIME, LOG_COUNT, LOG_REGDATE];
    if (sqlite3_exec(database, [log UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"%@ Create Error : %s", TB_LOG, sqlite3_errmsg(database));
    }
}


- (void)allTableDelete {
    
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_MESSAGE];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_MESSAGE Delete Error");
    }else {
        NSLog(@"TABLE_MESSAGE Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_SUGAR];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_SUGAR Delete Error");
    }else {
        NSLog(@"TABLE_SUGAR Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_WALK];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_WALK Delete Error");
    }else {
        NSLog(@"TABLE_WALK Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_DATA_CALROIE];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_DATA_CALROIE Delete Error");
    }else {
        NSLog(@"TABLE_DATA_CALROIE Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_SUGAR];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_SUGAR Delete Error");
    }else {
        NSLog(@"TABLE_SUGAR Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_PRESU];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_PRESU Delete Error");
    }else {
        NSLog(@"TABLE_PRESU Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_WHIGHT];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_WHIGHT Delete Error");
    }else {
        NSLog(@"TABLE_WHIGHT Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_FOOD_MAIN];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_FOOD_MAIN Delete Error");
    }else {
        NSLog(@"TABLE_FOOD_MAIN Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_FOOD_DETAIL];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_FOOD_DETAIL Delete Error");
    }else {
        NSLog(@"TABLE_FOOD_DETAIL Delete OK");
    }

    sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_RECENT_SEARCH];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_RECENT_SEARCH Delete Error");
    }else {
        NSLog(@"TABLE_RECENT_SEARCH Delete OK");
    }
    
    sql = [NSString stringWithFormat:@"DELETE FROM %@", TB_LOG];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TB_LOG Delete Error");
    }else {
        NSLog(@"TB_LOG Delete OK");
    }
    
}

@end
