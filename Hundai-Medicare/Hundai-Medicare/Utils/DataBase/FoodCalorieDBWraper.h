//
//  FoodCalorieDBWraper.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "CHCSVParser.h"
#import "Utils.h"

@interface FoodCalorieDBWraper : MedicareDataBase

- (void) dropTable;
- (BOOL) initFoodCalorieDb;
- (NSArray*)getRealList:(NSString*)keyword;
- (int)getFoodNewVersion;
- (void)setFoodDataVersion:(int)version;
@end
