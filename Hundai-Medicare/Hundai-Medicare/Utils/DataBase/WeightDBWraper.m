//
//  WeightDBWraper.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "WeightDBWraper.h"

@implementation WeightDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    
    return self;
}

- (void) DeleteDb:(NSString*)idx
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE idx=='%@'", TABLE_WHIGHT, idx];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}


- (void) UpdateDb:(NSString*)idx
              fat:(NSString*)fat
         obesity:(NSString*)obesity
        bodywater:(NSString*)bodywater
           muscle:(NSString*)muscle
           weight:(NSString*)weight
           reg_de:(NSString*)reg_de
{
    
    NSString *time = [Utils getTimeFormat:reg_de beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@='%@', %@='%@', %@='%@', %@='%@', %@='%@', %@='%@' WHERE %@='%@'",
                     TABLE_WHIGHT,  // Table
                     WEIGHT_FAT, fat,
                     WEIGHT_OBESITY, obesity,
                     WEIGHT_BODYWATER, bodywater,
                     WEIGHT_MUSCLE, muscle,
                     WEIGHT_WEIGHT, weight,
                     WEIGHT_REGDATE, time,
                     WEIGHT_IDX, idx];
                     
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"UpdateDb Error");
    }else {
        NSLog(@"UpdateDb OK");
    }
}

- (void) insert:(NSArray*)datas {
    [self insert:datas isServerReg:YES];
}

- (void) insert:(NSArray*)datas isServerReg:(Boolean)isServerReg {
    
    NSString *sqlHeader  = [NSString stringWithFormat:@" INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES ",
                      TABLE_WHIGHT, WEIGHT_IDX, WEIGHT_BMR,WEIGHT_BODYWATER,  WEIGHT_BONE, WEIGHT_FAT, WEIGHT_HEARTRATE, WEIGHT_MUSCLE, WEIGHT_OBESITY, WEIGHT_WEIGHT, WEIGHT_REGTYPE, WEIGHT_REGDATE];
    
    for( int i = 0 ; i < datas.count ; i ++){
        
        if ([[[datas objectAtIndex:i] objectForKey:@"reg_de"] length] != 12
            && [[[datas objectAtIndex:i] objectForKey:@"regdate"] length] != 12) continue;
        
        NSString *reg_de = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"reg_de"]) {
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"reg_de"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }else{
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"regdate"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }
        
        NSString *value = [NSString stringWithFormat:@"('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_IDX],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_BMR],
                           [[datas objectAtIndex:i] objectForKey:@"bodyWater"],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_BONE],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_FAT],
                           [[datas objectAtIndex:i] objectForKey:@"heartRate"],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_MUSCLE],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_OBESITY],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_WEIGHT],
                           [[datas objectAtIndex:i] objectForKey:WEIGHT_REGTYPE],
                           reg_de];
        
        NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];
        
        NSLog(@"sql : %@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"InsertDb Error");
        }else {
            NSLog(@"InsertDb OK");
        }
    }
    
}

#pragma mark - result select

- (NSArray *)getResult {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                         WEIGHT_IDX, WEIGHT_BMR, WEIGHT_BONE, WEIGHT_HEARTRATE, WEIGHT_WEIGHT, WEIGHT_FAT, WEIGHT_OBESITY, WEIGHT_BODYWATER, WEIGHT_MUSCLE, WEIGHT_REGTYPE, WEIGHT_REGDATE,
                         TABLE_WHIGHT];
    
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, %@ desc, cast(%@ as BIGINT) DESC",strsql, WEIGHT_REGDATE, WEIGHT_REGDATE, WEIGHT_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *bmr = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *bone = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *heartrate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *weight = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *fat = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *obesity = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *bodywater = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *muscle = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *regtype = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", bmr, @"bmr", bone, @"bone", heartrate, @"heartrate",
                                weight, @"weight", fat, @"fat", obesity, @"obesity", bodywater, @"bodywater",
                                muscle, @"muscle", regtype, @"regtype", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

- (NSArray *)getBottomResult {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                         WEIGHT_IDX, WEIGHT_BMR, WEIGHT_BONE, WEIGHT_HEARTRATE, WEIGHT_WEIGHT, WEIGHT_FAT, WEIGHT_OBESITY, WEIGHT_BODYWATER, WEIGHT_MUSCLE, WEIGHT_REGTYPE, WEIGHT_REGDATE,
                         TABLE_WHIGHT];
    
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC LIMIT 1",strsql, WEIGHT_REGDATE, WEIGHT_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *bmr = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *bone = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *heartrate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *weight = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *fat = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *obesity = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *bodywater = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *muscle = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *regtype = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", bmr, @"bmr", bone, @"bone", heartrate, @"heartrate",
                                weight, @"weight", fat, @"fat", obesity, @"obesity", bodywater, @"bodywater",
                                muscle, @"muscle", regtype, @"regtype", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

- (NSString *)getAvgWeight:(NSString *)sDate eDate:(NSString *)eDate {
    sqlite3_stmt *statement;
    
    NSString *strsql = [NSString stringWithFormat:@"SELECT"];
    strsql = [NSString stringWithFormat:@"%@ IFNull(AVG(CAST(%@ as FLOAT)), 0) as WEIGHT_AVG", strsql, WEIGHT_WEIGHT];
    strsql = [NSString stringWithFormat:@"%@ FROM %@", strsql, TABLE_WHIGHT];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59'", strsql, WEIGHT_REGDATE, sDate, eDate];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement) == SQLITE_ROW) {
        NSString *weight = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        
        [arrResult addObject:weight];
    }
           
   sqlite3_finalize(statement);
    
    return [arrResult firstObject];
}

#pragma mark - chart select

/**
 * 체중 일 그래프
 **/
- (NSArray*) getResultDay:(NSString *)nDate type:(int)type
{
    sqlite3_stmt *statement;
    NSString *weight_type = WEIGHT_WEIGHT;
    if(type == 2){
        weight_type = WEIGHT_FAT;
    }
    
    NSString *strsql  = @"SELECT ";
    
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=0 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H0, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=1 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H1, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=2 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H2, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=3 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H3, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=4 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H4, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=5 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H5, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=6 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H6, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=7 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H7, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=8 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H8, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=9 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H9, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=10 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H10, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=11 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H11, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=12 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H12, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=13 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H13, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=14 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H14, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=15 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H15, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=16 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H16,", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=17 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H17,  ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=18 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H18, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=19 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H19, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=20 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H20, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=21 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H21, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=22 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H22, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', A.%@) as integer)=23 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as H23 ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@ as A ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@ INNER JOIN ( ", strsql];
    strsql  = [NSString stringWithFormat:@"%@               SELECT MAX(idx) as idx FROM %@ ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@               WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59:' ", strsql, WEIGHT_REGDATE, nDate, nDate];
    strsql  = [NSString stringWithFormat:@"%@               GROUP BY strftime('!', %@) ", strsql, WEIGHT_REGDATE];
    strsql  = [NSString stringWithFormat:@"%@            ) as B ON A.idx = B.idx ", strsql];
    strsql  = [NSString stringWithFormat:@"%@ WHERE A.%@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND cast(A.%@ as FLOAT) >= 20 and cast(A.%@ as FLOAT) <= 300 ", strsql, WEIGHT_REGDATE, nDate, nDate, WEIGHT_WEIGHT, WEIGHT_WEIGHT];
    strsql  = [NSString stringWithFormat:@"%@ GROUP BY strftime('#', A.%@), strftime('$', A.%@), strftime('^', A.%@)", strsql, WEIGHT_REGDATE, WEIGHT_REGDATE, WEIGHT_REGDATE];
    
    
    /*  김영준과장, 최종인지 최고인지 확인하고 커밋.
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=0 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H0, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=1 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H1, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=2 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H2, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=3 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H3, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=4 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H4, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=5 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H5, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=6 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H6, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=7 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H7, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=8 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H8, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=9 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H9, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=10 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H10, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=11 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H11, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=12 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H12, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=13 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H13, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=14 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H14, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=15 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H15, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=16 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H16, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=17 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H17, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=18 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H18, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=19 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H19, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=20 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H20, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=21 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H21, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=22 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H22, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(MAX(CASE WHEN cast(strftime('!', %@) as integer)=23 THEN cast(ifnull(%@ ,0) as FLOAT) End),2) as H23 ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND cast(%@ as FLOAT) >= 20 and cast(%@ as FLOAT) <= 300 ", strsql, WEIGHT_REGDATE, nDate, nDate, WEIGHT_WEIGHT, WEIGHT_WEIGHT];
    strsql  = [NSString stringWithFormat:@"%@ GROUP BY strftime('#', %@), strftime('$', %@), strftime('^', %@)", strsql, WEIGHT_REGDATE, WEIGHT_REGDATE, WEIGHT_REGDATE];
     */
    strsql = [strsql stringByReplacingOccurrencesOfString:@"!" withString:@"%H"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h0 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *h1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *h2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        
        NSArray *dicTemp = @[h0, h1, h2, h3, h4,h5,
                             h6, h7 ,h8 ,h9 ,h10, h11,
                             h12 ,h13 ,h14 , h15, h16, h17,
                             h18, h19, h20, h21 ,h22 ,h23];
        
        [arrResult addObject:dicTemp];
    }
    
    if(arrResult.count == 0){
//        NSArray *dicTemp = @[@"(null)", @"(null)", @"(null)", @"(null)", @"(null)", @"(null)",
//                             @"(null)", @"(null)", @"(null)", @"(null)", @"(null)", @"(null)",
//                             @"(null)", @"(null)", @"(null)", @"(null)", @"(null)", @"(null)",
//                            @"(null)", @"(null)", @"(null)", @"(null)", @"(null)", @"(null)"];
//        
//        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


/**
 * 체중 주 그래프
 **/
- (NSArray*) getResultWeek:(NSString *)sDate eDate:(NSString *)eDate type:(int)type
{
    sqlite3_stmt *statement;
    
    NSString *weight_type = WEIGHT_WEIGHT;
    if(type == 2){
        weight_type = WEIGHT_FAT;
    }
    
    NSString *strsql  = @"SELECT ";
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=0 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W1, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=1 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W2, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=2 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W3, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=3 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W4, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=4 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W5, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=5 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W6, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=6 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as W7 ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@ as A ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@ WHERE A.%@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND cast(A.%@ as FLOAT) >= 20", strsql, WEIGHT_REGDATE, sDate, eDate, WEIGHT_WEIGHT];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"!" withString:@"%w"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
//        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
//                                w1, @"w1", w2, @"w2", w3, @"w3", w4, @"w4",
//                                w5, @"w5", w6, @"w6", w7, @"w7",
//                                nil];
        
        NSArray *dicTemp = @[w1, w2, w3, w4,w5, w6 ,w7];
        
        [arrResult addObject:dicTemp];
    }
    
    if(arrResult.count == 0){
        
//        NSArray *dicTemp = @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
//
//        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


/**
 * 체중 월 그래프
 **/
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth type:(int)type{
    sqlite3_stmt *statement;
    
    NSString *weight_type = WEIGHT_WEIGHT;
    if(type == 2){
        weight_type = WEIGHT_FAT;
    }
    
    NSString *strsql  = @"SELECT ";
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=1 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D1, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=2 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D2, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=3 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D3, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=4 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D4, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=5 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D5, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=6 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D6, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=7 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D7, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=8 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D8, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=9 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D9, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=10 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D10, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=11 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D11, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=12 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D12, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=13 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D13, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=14 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D14, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=15 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D15, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=16 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D16,", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=17 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D17,  ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=18 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D18, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=19 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D19, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=20 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D20, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=21 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D21, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=22 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D22, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=23 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D23, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=24 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D24, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=25 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D25, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=26 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D26, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=27 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D27, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=28 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D28, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=29 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D29, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=30 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D30, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=31 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as D31 ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@ as A ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@ WHERE cast(strftime('#', %@) as integer) = %@ and cast(strftime('$', %@) as integer) = %@ and cast(%@ as FLOAT) >= 20", strsql, WEIGHT_REGDATE, nYear, WEIGHT_REGDATE, nMonth, WEIGHT_WEIGHT];
    strsql  = [NSString stringWithFormat:@"%@ GROUP BY strftime('#', %@), strftime('$', A.%@)", strsql, WEIGHT_REGDATE, WEIGHT_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"!" withString:@"%d"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d25 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d26 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d27 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d28 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d29 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d30 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d31 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        
//        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
//                                d1, @"d1", d2, @"d2", d3, @"d3", d4, @"d4", d5, @"d5",
//                                d6, @"d6", d7, @"d7", d8, @"d8", d9, @"d9", d10, @"d10",
//                                d11, @"d11", d12, @"d12", d13, @"d13", d14, @"d14", d15, @"d15",
//                                d16, @"d16", d17, @"d17", d18, @"d18", d19, @"d19", d20, @"d20",
//                                d21, @"d21", d22, @"d22", d23, @"d23", d24, @"d24", d25, @"d25",
//                                d26, @"d26", d27, @"d27", d28, @"d28", d29, @"d29", d30, @"d30", d31, @"d31",
//                                nil];
        
        NSArray *dicTemp = @[d1, d2, d3, d4,d5,
                             d6 ,d7 ,d8 ,d9 ,d10, d11,
                             d12 ,d13 ,d14 , d15, d16, d17,
                             d18, d19, d20, d21 ,d22 ,d23,
                             d24, d25, d26, d27 ,d28 ,d29,
                             d30, d31];
        
        [arrResult addObject:dicTemp];
    }
    
    if(arrResult.count == 0){
//        NSArray *dicTemp = @[@"0", @"0", @"0", @"0", @"0", @"0",
//                             @"0" ,@"0" ,@"0" ,@"0" ,@"0", @"0",
//                             @"0" ,@"0" ,@"0" , @"0", @"0", @"0",
//                             @"0", @"0", @"0", @"0" ,@"0" ,@"0",
//                             @"0", @"0", @"0", @"0" ,@"0" ,@"0",
//                             @"0"];
//
//        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


/**
 * 체중 년 그래프
 **/
- (NSArray*) getResultYear:(NSString *)nYear type:(int)type{
    sqlite3_stmt *statement;
    
    NSString *weight_type = WEIGHT_WEIGHT;
    if(type == 2){
        weight_type = WEIGHT_FAT;
    }
    
    NSString *strsql  = @"SELECT "; 
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=1 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M1, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=2 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M2, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=3 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M3, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=4 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M4, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=5 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M5, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=6 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M6, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=7 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M7, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=8 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M8, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=9 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M9, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=10 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M10, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=11 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M11, ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ ROUND(AVG(CASE WHEN cast(strftime('!', A.%@) as integer)=12 THEN cast(ifnull(A.%@ ,0) as FLOAT) End),2) as M12 ", strsql, WEIGHT_REGDATE, weight_type];
    strsql  = [NSString stringWithFormat:@"%@ FROM %@ as A ", strsql, TABLE_WHIGHT];
    strsql  = [NSString stringWithFormat:@"%@ WHERE cast(strftime('#', %@) as integer) = %@ and cast(%@ as FLOAT) >= 20", strsql, WEIGHT_REGDATE, nYear, WEIGHT_WEIGHT];
    strsql  = [NSString stringWithFormat:@"%@ GROUP BY strftime('#', %@)", strsql, WEIGHT_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"!" withString:@"%m"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *m1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *m2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *m3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *m4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *m5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *m6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *m7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *m8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *m9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *m10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *m11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *m12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        
//        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
//                                m1, @"m1", m2, @"m2", m3, @"m3", m4, @"m4",
//                                m5, @"m5", m6, @"m6", m7, @"m7", m8, @"m8",
//                                m9, @"m9", m10, @"m10", m11, @"m11", m12, @"m12",
//                                nil];
        
        NSArray *dicTemp = @[m1, m2, m3, m4,m5,
                             m6 ,m7 ,m8 ,m9 ,m10, m11, m12];
        
        [arrResult addObject:dicTemp];
    }
    
    if(arrResult.count == 0){
//        NSArray *dicTemp = @[@"0", @"0", @"0", @"0", @"0",@"0",
//                             @"0" ,@"0" ,@"0" ,@"0" ,@"0", @"0"];
//
//        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

- (NSArray*) getResult40Week:(NSArray *)weeks {

    NSString *strsql  = @"SELECT";
    for (int i=0; i < [weeks count]; i++) {
        NSString *startDate = [TimeUtils getDayformat:[[weeks objectAtIndex:i] objectForKey:@"STARTDATE"] format:@"yyyy-MM-dd"];
        NSString *endDate   = [TimeUtils getDayformat:[[weeks objectAtIndex:i] objectForKey:@"ENDDATE"] format:@"yyyy-MM-dd"];
        strsql  = [NSString stringWithFormat:@"%@ AVG(CASE WHEN regdate BETWEEN '%@ 00:00' and '%@ 23:59' THEN weight else null END) as D%d %@", strsql, startDate, endDate, i+1, i==[weeks count]-1?@"":@","];
    }
    strsql  = [NSString stringWithFormat:@"%@ From tb_data_weight", strsql];

    sqlite3_stmt *statement;
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *m1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *m2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *m3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *m4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *m5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *m6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *m7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *m8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *m9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *m10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *m11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *m12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *m13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *m14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *m15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *m16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *m17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *m18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *m19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *m20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *m21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *m22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *m23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *m24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *m25 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *m26 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *m27 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *m28 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *m29 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *m30 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *m31 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        NSString *m32 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 31)];
        NSString *m33 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 32)];
        NSString *m34 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 33)];
        NSString *m35 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 34)];
        NSString *m36 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 35)];
        NSString *m37 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 36)];
        NSString *m38 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 37)];
        NSString *m39 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 38)];
        NSString *m40 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 39)];
        
        
        NSArray *dicTemp = @[m1, m2, m3, m4,m5,
                             m6 ,m7 ,m8 ,m9 ,m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20,
                             m21, m22, m23, m24, m25, m26, m27, m28, m29, m30, m31, m32, m33, m34, m35,
                             m36, m37, m38, m39, m40];
        
        [arrResult addObject:dicTemp];
    }
    
    if(arrResult.count == 0){
//        NSArray *dicTemp = @[@"0", @"0", @"0", @"0", @"0",@"0", @"0", @"0", @"0", @"0",
//                             @"0", @"0", @"0", @"0", @"0",@"0", @"0", @"0", @"0", @"0",
//                             @"0", @"0", @"0", @"0", @"0",@"0", @"0", @"0", @"0", @"0",
//                             @"0", @"0", @"0", @"0", @"0",@"0", @"0", @"0", @"0", @"0"];
//        
//        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

@end
