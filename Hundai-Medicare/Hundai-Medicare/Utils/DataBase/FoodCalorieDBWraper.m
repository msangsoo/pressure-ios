//
//  FoodCalorieDBWraper.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodCalorieDBWraper.h"


#define FOOD_DATA_VERSION   1
@implementation FoodCalorieDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}

- (int)getFoodNewVersion {
    return FOOD_DATA_VERSION;
}

- (void)setFoodDataVersion:(int)version {
    NSString *v = [NSString stringWithFormat:@"%d", version];
    [Utils setUserDefault:FOOD_DB_VERSION value:v];
}

- (void) dropTable
{
    NSString *sql = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@;", TABLE_FOOD_CALORIE];
    NSLog(@"DropTable.sql:%@", sql);
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"DropTable.sql Error");
    }else {
        NSLog(@"DropTable.sql OK");
        [self createTableFoodCalorie];
    }
}

- (BOOL) initFoodCalorieDb {
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSURL *fileURL = [bundle URLForResource:@"food_db" withExtension:@"csv"];
    NSArray *actual = [NSArray arrayWithContentsOfCSVURL:fileURL];
    
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_CALORIE, FOOD_CODE, FOOD_KIND, FOOD_NAME, FOOD_GRAM, FOOD_UNIT, FOOD_CALORIE,
                           FOOD_CARBOHYDRATE, FOOD_PROTEIN, FOOD_FAT, FOOD_SUGARS, FOOD_SALT, FOOD_CHOLESTEROL,
                           FOOD_SATURATED, FOOD_TRANSQUANTI];
    
    for (int i=0; i < [actual count]; i++) {
        NSArray *calRow = [actual objectAtIndex:i];
        NSString *code          = [calRow objectAtIndex:0];
        NSString *kind          = [calRow objectAtIndex:1];
        NSString *name          = [calRow objectAtIndex:2];
        NSString *gram          = [calRow objectAtIndex:3];
        NSString *unit          = [calRow objectAtIndex:4];
        NSString *calorie       = [calRow objectAtIndex:5];
        NSString *carbohydrate  = [calRow objectAtIndex:6];
        NSString *protein       = [calRow objectAtIndex:7];
        NSString *fat           = [calRow objectAtIndex:8];
        NSString *sugars        = [calRow objectAtIndex:9];
        NSString *salt          = [calRow objectAtIndex:10];
        NSString *cholesterol   = [calRow objectAtIndex:11];
        NSString *saturated     = [calRow objectAtIndex:12];
        NSString *ctransquantic = [calRow objectAtIndex:13];
        
        NSString *queryValue = [NSString stringWithFormat:@"%@ ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@'); ",
                                  queryHead, code, kind, name, gram, unit, calorie, carbohydrate, protein, fat, sugars, salt, cholesterol, saturated, ctransquantic];
        
        NSLog(@"queryValue[%d] :%@", i, queryValue);
        sqlite3_stmt *updateStmt = nil;
        if (sqlite3_exec(database, [queryValue UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"INSERT failed %@",updateStmt);
        }
    }
    return YES;
}


- (NSArray*)getFoodList:(NSArray*)arr {
    
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    NSMutableString *stringBuffer = [[NSMutableString alloc] init];
    [stringBuffer setString:@""];
    
    NSString *queryHead = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@  ",
                           FOOD_CODE, FOOD_KIND, FOOD_NAME, FOOD_GRAM, FOOD_UNIT, FOOD_CALORIE,
                           FOOD_CARBOHYDRATE, FOOD_PROTEIN, FOOD_FAT, FOOD_SUGARS, FOOD_SALT, FOOD_CHOLESTEROL,
                           FOOD_SATURATED, FOOD_TRANSQUANTI, TABLE_FOOD_CALORIE];
    for (int i=0; i < [arr count]; i++) {
        if(i==0){
            [stringBuffer appendString:@" Where "];
        }else{
            [stringBuffer appendString:@" OR "];
        }
        
        [stringBuffer appendString:[NSString stringWithFormat:@" %@=%@ ", FOOD_CODE, [arr objectAtIndex:i]]];
    }
    
    NSString *sql = [NSString stringWithFormat:@"%@%@", queryHead, stringBuffer];
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getFoodList '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *code = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *kind = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,1)];
        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,2)];
        NSString *gram = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *unit = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *calorie = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *carbohydrate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *protein = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *fat = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *sugars = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *salt = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *cholesterol = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *saturated = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *ctransquantic = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                code, @"code", kind, @"kind", name, @"name", gram, @"gram",
                                unit, @"unit", calorie, @"calorie", carbohydrate, @"carbohydrate", protein, @"protein",
                                fat, @"fat", sugars, @"sugars", salt, @"salt", cholesterol, @"cholesterol",
                                saturated, @"saturated", ctransquantic, @"ctransquantic", nil];
        
        [arrRtn addObject:dicTemp];
    }
 
    return arrRtn;
}

- (NSArray*)getRealList:(NSString *)keyword {
    
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    NSMutableString *stringBuffer = [[NSMutableString alloc] init];
    [stringBuffer setString:@""];
    
    NSString *queryHead = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@  ",
                           FOOD_CODE, FOOD_KIND, FOOD_NAME, FOOD_GRAM, FOOD_UNIT, FOOD_CALORIE,
                           FOOD_CARBOHYDRATE, FOOD_PROTEIN, FOOD_FAT, FOOD_SUGARS, FOOD_SALT, FOOD_CHOLESTEROL,
                           FOOD_SATURATED, FOOD_TRANSQUANTI, TABLE_FOOD_CALORIE];
    if (keyword != nil && [keyword length] > 0) {
        [stringBuffer appendString:[NSString stringWithFormat:@" WHERE %@ like ", FOOD_NAME]];
        [stringBuffer appendString:@"'%"];
        [stringBuffer appendString:[NSString stringWithFormat:@"%@", keyword]];
        [stringBuffer appendString:@"%'"];
//        [stringBuffer appendString:[Utils makeQuery:keyword]];
    }
    [stringBuffer appendString:[NSString stringWithFormat:@" Order by %@ ASC LIMIT 200;", FOOD_NAME]];
    
    NSString *sql = [NSString stringWithFormat:@"%@%@", queryHead, stringBuffer];
    NSLog(@"sql:%@", sql);
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getRealList '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *code = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *kind = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,1)];
        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,2)];
        NSString *gram = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *unit = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *calorie = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *carbohydrate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *protein = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *fat = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *sugars = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *salt = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *cholesterol = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *saturated = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *ctransquantic = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                code, @"code", kind, @"kind", name, @"name", gram, @"gram",
                                unit, @"unit", calorie, @"calorie", carbohydrate, @"carbohydrate", protein, @"protein",
                                fat, @"fat", sugars, @"sugars", salt, @"salt", cholesterol, @"cholesterol",
                                saturated, @"saturated", ctransquantic, @"ctransquantic", nil];
        
//        NSLog(@"dicTemp:%@", dicTemp);
        [arrRtn addObject:dicTemp];
    }
    return (NSArray*)arrRtn;
}

@end
