//
//  WalkDBWraper.m
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 3. 31..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WalkDBWraper.h"

@implementation WalkDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}

- (void) DeleteDb:(NSString*)idx {
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE idx=='%@'", TABLE_WALK, idx];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}

- (void) insert:(NSArray*)datas {
    NSString *sqlHeader  = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) VALUES ",
                            TABLE_WALK, WALK_IDX, WALK_CALORY, WALK_DISTANCE, WALK_STEP, WALK_HEARTRATE, WALK_STRESS, WALK_REGTYPE, WALK_REGDATE];
    
    for( int i = 0 ; i < datas.count ; i ++) {
        if ([[[datas objectAtIndex:i] objectForKey:@"reg_de"] length] != 12
            && [[[datas objectAtIndex:i] objectForKey:@"regdate"] length] != 12) continue;
        
        NSString *reg_de = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"reg_de"]) {
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"reg_de"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }else {
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"regdate"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }
        
        NSString *calory = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"calory"] != nil) {
            calory = [[datas objectAtIndex:i] objectForKey:@"calory"];
        }else {
            calory = [[datas objectAtIndex:i] objectForKey:@"calorie"];
        }
        
        NSString *value = [NSString stringWithFormat:@"('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",
                           [[datas objectAtIndex:i] objectForKey:WALK_IDX],
                           calory,
                           [[datas objectAtIndex:i] objectForKey:WALK_DISTANCE],
                           [[datas objectAtIndex:i] objectForKey:WALK_STEP],
                           [[datas objectAtIndex:i] objectForKey:@"heartRate"],
                           [[datas objectAtIndex:i] objectForKey:WALK_STRESS],
                           [[datas objectAtIndex:i] objectForKey:WALK_REGTYPE],
                           reg_de];
        
        NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];
        
        NSLog(@"sql : %@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"InsertDb Error");
        }else {
            NSLog(@"InsertDb OK");
        }
    }
}

- (void) UpdateDb:(NSString*)idx
           calory:(NSString*)calory
         distance:(NSString*)distance
             step:(NSString*)step
        heartrate:(NSString*)heartrate
           stress:(NSString*)stress
          regtype:(NSString*)regtype
           reg_de:(NSString*)reg_de {
    
    NSString *time = [Utils getTimeFormat:reg_de beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@='%@', %@='%@', %@='%@', %@='%@', %@='%@', %@='%@', %@='%@' WHERE %@='%@'",
                     TABLE_WALK,  // Table
                     WALK_CALORY, calory,
                     WALK_DISTANCE, distance,
                     WALK_STEP, step,
                     WALK_HEARTRATE, heartrate,
                     WALK_STRESS, stress,
                     WALK_REGTYPE, regtype,
                     WALK_REGDATE, time,
                     WALK_IDX, idx];
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"UpdateDb Error");
    }else {
        NSLog(@"UpdateDb OK");
    }
}

- (NSArray*) getResult {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                         WALK_IDX, WALK_CALORY, WALK_DISTANCE, WALK_STEP, WALK_HEARTRATE, WALK_STRESS, WALK_REGTYPE, WALK_REGDATE,
                         TABLE_WALK];
    
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC",strsql, WALK_REGDATE, WALK_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    
    while (sqlite3_step(statement)==SQLITE_ROW) {
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *calory = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *distance = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *step = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *heartrate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,4)];
        NSString *stress = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *regtype = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", calory, @"calory", distance, @"distance", step, @"step",
                                heartrate, @"heartrate", stress, @"stress", regtype, @"regtype", regdate, @"regdate",
                                nil];
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


- (NSArray*) getResultMain:(NSString *)sDate eDate:(NSString *)eDate {
    
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(cast(%@ as integer)),0) as TOTCALORIE, ", strsql, WALK_CALORY];
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(cast(%@ as integer)),0) as TOTSTEP, ", strsql, WALK_STEP];
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(cast(%@ as integer)),0) * 2.5 as DISTANCE, ", strsql, WALK_DISTANCE];
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(cast(%@ as integer)),0) as MOVEMENT, ", strsql, WALK_DISTANCE];
    strsql = [NSString stringWithFormat:@"%@ IFNull(MAX(cast(%@ as integer)),0) as LONGMOVEMENT, ", strsql, WALK_DISTANCE];
    strsql = [NSString stringWithFormat:@"%@ IFNull(MIN(cast(%@ as integer)),0) as MAXSTEP ", strsql, WALK_STEP];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_WALK];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN  '%@ 00:00' AND '%@ 23:59'", strsql, WALK_REGDATE, sDate, eDate];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *TOTCALORIE    = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *TOTSTEP       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *DISTANCE      = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *MOVEMENT      = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *LONGMOVEMENT  = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *MAXSTEP       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                TOTCALORIE, @"TOTCALORIE", TOTSTEP, @"TOTSTEP", DISTANCE, @"DISTANCE",
                                MOVEMENT, @"MOVEMENT", LONGMOVEMENT, @"LONGMOVEMENT", MAXSTEP, @"MAXSTEP",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

/**
 * 걸음 일 그래프
 **/
- (NSArray*) getResultDay:(NSString *)nDate walk:(Boolean)walk {

    NSString *grapType = WALK_CALORY;
    if(walk == NO) {
        grapType = WALK_STEP;
    }
    
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for (int i = 1 ; i < 24 ; i ++) {
        strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('$', %@) as integer)=%d THEN %@ End), 0) as h%d, ", strsql, WALK_REGDATE, i, grapType, i];
    }
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('$', %@) as integer)=24 THEN %@ End), 0) as h24 ", strsql, WALK_REGDATE, grapType];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_WALK];
    
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' ", strsql, WALK_REGDATE, nDate, nDate];
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer)", strsql, WALK_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%H"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%d"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *h2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *h3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        
        NSArray *arrRtn2 = @[@([h1 intValue]), @([h2 intValue]), @([h3 intValue]), @([h4 intValue]), @([h5 intValue]), @([h6 intValue]),
                             @([h7 intValue]), @([h8 intValue]), @([h9 intValue]), @([h10 intValue]), @([h11 intValue]), @([h12 intValue]),
                             @([h13 intValue]), @([h14 intValue]), @([h15 intValue]), @([h16 intValue]), @([h17 intValue]), @([h18 intValue]),
                             @([h19 intValue]), @([h20 intValue]), @([h21 intValue]), @([h22 intValue]), @([h23 intValue]), @([h24 intValue])];
        
        [arrResult addObject:arrRtn2];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0)];
        
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}


/**
 * 걸음 주 그래프
 **/
- (NSArray*) getResultWeek:(NSString *)nDate eDate:(NSString *)eDate walk:(Boolean)walk {
    
    NSString *grapType = WALK_CALORY;
    if(walk == NO) {
        grapType = WALK_STEP;
    }
    
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for (int i = 1 ; i < 7 ; i ++) {
        strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('^', %@) as integer)=%d THEN %@ End), 0) as w%d, ", strsql, WALK_REGDATE, i, grapType, i];
    }
    strsql = [NSString stringWithFormat:@"%@ IFNull(SUM(CASE WHEN cast(strftime('^', %@) as integer)=7 THEN %@ End), 0) as w7 ", strsql, WALK_REGDATE, grapType];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_WALK];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' ", strsql, WALK_REGDATE, nDate, eDate];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%w"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        NSArray *before = @[@([w1 intValue]), @([w2 intValue]), @([w3 intValue]), @([w4 intValue]),
                            @([w5 intValue]), @([w6 intValue]), @([w7 intValue])];
        
        [arrResult addObject:before];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0)];
        
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

/**
 * 걷기 월간 그래프
 **/
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth walk:(Boolean)walk {
    
    NSString *grapType = WALK_CALORY;
    if(walk == NO) {
        grapType = WALK_STEP;
    }
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for(int i = 0 ; i < 30 ; i++){
        strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE WHEN cast(strftime('^', %@) as integer)=%d  THEN %@ End), 0) as D%dB, ", strsql, WALK_REGDATE, i+1,  grapType, i+1];
    }
    strsql = [NSString stringWithFormat:@"%@ ifnull(SUM(CASE WHEN cast(strftime('^', %@) as integer)=31 THEN %@ End), 0) as D31B ", strsql, WALK_REGDATE, grapType];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_WALK];
    strsql = [NSString stringWithFormat:@"%@ WHERE cast(strftime('#', %@) as integer)=%@ AND cast(strftime('$', %@) as integer)=%@",
              strsql, WALK_REGDATE, nYear, WALK_REGDATE, nMonth];
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer), cast(strftime('$', %@) as integer)", strsql, WALK_REGDATE, WALK_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d0b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d1b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d2b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d3b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d4b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d5b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d6b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d7b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d8b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d9b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d10b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d11b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d12b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d13b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d14b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d15b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d16b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d17b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d18b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d19b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d20b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d21b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d22b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d23b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d24b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d25b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d26b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d27b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d28b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d29b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d30b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        
        NSArray *before =@[@([d0b intValue]), @([d1b intValue]), @([d2b intValue]), @([d3b intValue]), @([d4b intValue]), @([d5b intValue]),
                           @([d6b intValue]), @([d7b intValue]), @([d8b intValue]), @([d9b intValue]), @([d10b intValue]), @([d11b intValue]),
                           @([d12b intValue]), @([d13b intValue]), @([d14b intValue]), @([d15b intValue]), @([d16b intValue]), @([d17b intValue]),
                           @([d18b intValue]), @([d19b intValue]), @([d20b intValue]), @([d21b intValue]), @([d22b intValue]), @([d23b intValue]),
                           @([d24b intValue]), @([d25b intValue]), @([d26b intValue]), @([d27b intValue]), @([d28b intValue]), @([d29b intValue]),
                           @([d30b intValue])];
        
        [arrResult addObject:before];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                            @(0)];
        
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}



@end
