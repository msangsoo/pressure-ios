//
//  RecentSearchDBWraper.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 23/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"
#import "TimeUtils.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecentSearchDBWraper : MedicareDataBase
- (void) insert:(NSString*)keyword;
- (NSMutableArray*) getKeywords:(int)limit;
- (void) deleteDb:(NSString*)keyword;
-(void) truncateDb;
@end

NS_ASSUME_NONNULL_END
