//
//  MedicareDataBase.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <sqlite3.h>
#import <Foundation/Foundation.h>

static sqlite3 *database = nil;
static sqlite3_stmt *deleteStmt = nil;
static sqlite3_stmt *addStmt = nil;
static sqlite3_stmt *detailStmt = nil;
static sqlite3_stmt *updateStmt = nil;
static NSString *DBFILE_NAME = @"food_data.db";

// + ----------------------
// 테이블
// + ----------------------
#define TABLE_MESSAGE               @"tb_data_message"       //건강메세지
#define TABLE_WALK                  @"tb_data_walk"          // 걷기
#define TABLE_DATA_CALROIE          @"tb_data_data_calroie"  // 활동
#define TABLE_SUGAR                 @"tb_data_sugar"         // 혈당
#define TABLE_PRESU                 @"tb_data_presu"         // 혈압
#define TABLE_WHIGHT                @"tb_data_weight"        // 몸무게
#define TABLE_FOOD_MAIN             @"tb_data_food_main"     // 식사메인
#define TABLE_FOOD_DETAIL           @"tb_data_food_detail"   // 식사상세(음식)
#define TABLE_FOOD_CALORIE          @"tb_data_food_calorie"  // 식사칼로리
#define TABLE_RECENT_SEARCH         @"tb_data_recent_search"  // 최근검색어

#define TB_CAREBOT                  @"tb_carebot"            // 케어봇
#define TB_LOG                      @"tb_log"                // 로그

// + ----------------------
// 필드
// + ----------------------

// // 건강 메세지
#define MSG_IDX                     @"idx"
#define MSG_MESSGAE                 @"message"
#define MSG_INFRA_TY                @"infra_ty"
#define MSG_IS_SERVER_REGIST        @"is_server_regist"
#define MSG_REGDATE                 @"regdate"

// // 걷기
#define WALK_IDX                    @"idx"
#define WALK_CALORY                 @"calory"
#define WALK_DISTANCE               @"distance"
#define WALK_STEP                   @"step"
#define WALK_HEARTRATE              @"heartrate"
#define WALK_STRESS                 @"stress"
#define WALK_REGTYPE                @"regtype"
#define WALK_REGDATE                @"regdate"
#define WALK_IS_SERVER_REGIST       @"is_server_regist"

// 활동
#define DATA_CALROIE_IDX                @"DATA_CALROIE_IDX"
#define DATA_CALROIE_SPORT_SN           @"DATA_CALROIE_SPORT_SN"
#define DATA_CALROIE_DATA_SN            @"DATA_CALROIE_DATA_SN"
#define DATA_CALROIE_ACTIVE_SEQ         @"DATA_CALROIE_ACTIVE_SEQ"
#define DATA_CALROIE_ACTIVE_TIME        @"DATA_CALROIE_ACTIVE_TIME"
#define DATA_CALROIE_CALORY             @"DATA_CALROIE_CALORY"
#define DATA_CALROIE_IS_SERVER_REGIST   @"DATA_CALROIE_IS_SERVER_REGIST"
#define DATA_CALROIE_REG_DE             @"DATA_CALROIE_REG_DE"


// // 혈당
#define SUGAR_IDX                   @"idx"
#define SUGAR_SUGAR                 @"sugar"
#define SUGAR_HILOW                 @"hiLow"
#define SUGAR_BEFORE                @"before"
#define SUGAR_DRUGNAME              @"drugname"
#define SUGAR_REGTYPE               @"regtype"
#define SUGAR_REGDATE               @"regdate"
#define SUGAR_IS_SERVER_REGIST      @"is_server_regist"

//혈압
#define PRESU_IDX                   @"idx"
#define PRESU_ARTERIALPRESSURE      @"arterialPressure"
#define PRESU_DIASTOLICPRESSURE     @"diastolicPressure"
#define PRESU_PULSERATE             @"pulseRate"
#define PRESU_SYSTOLICPRESSURE      @"systolicPressure"
#define PRESU_DRUGNAME              @"drugname"
#define PRESU_REGTYPE               @"regtype"
#define PRESU_REGDATE               @"regdate"
#define PRESU_IS_SERVER_REGIST      @"is_server_regist"

// // 몸무게
#define WEIGHT_IDX                  @"idx"
#define WEIGHT_BMR                  @"bmr"
#define WEIGHT_BODYWATER            @"bodyWater"
#define WEIGHT_BONE                 @"bone"
#define WEIGHT_FAT                  @"fat"
#define WEIGHT_HEARTRATE            @"heartRate"
#define WEIGHT_MUSCLE               @"muscle"
#define WEIGHT_OBESITY              @"obesity"
#define WEIGHT_WEIGHT               @"weight"
#define WEIGHT_REGTYPE              @"regtype"
#define WEIGHT_IS_SERVER_REGIST     @"is_server_regist"
#define WEIGHT_REGDATE              @"regdate"


// // 식사메인
#define FOODMAIN_IDX           @"idx"
#define FOODMAIN_AMOUNTTIME    @"amounttime"
#define FOODMAIN_MEALTTYPE     @"mealtype"
#define FOODMAIN_CALORIE       @"calorie"
#define FOODMAIN_PICTURE       @"picture"
#define FOODMAIN_REGTYPE       @"regdate"

// 식사상세(음식)
#define FOODDETAIL_IDX          @"idx"
#define FOODDETAIL_FOODCODE     @"foodcode"
#define FOODDETAIL_FORPEOPLE    @"forpeople"
#define FOODDETAIL_REGTYPE      @"regdate"


// 음식데이터
#define FOOD_CODE           @"code"
#define FOOD_KIND           @"kind"
#define FOOD_NAME           @"name"
#define FOOD_GRAM           @"gram"
#define FOOD_UNIT           @"unit"
#define FOOD_CALORIE        @"calorie"
#define FOOD_CARBOHYDRATE   @"carbohydrate"
#define FOOD_PROTEIN        @"protein"
#define FOOD_FAT            @"fat"
#define FOOD_SUGARS         @"sugars"
#define FOOD_SALT           @"salt"
#define FOOD_CHOLESTEROL    @"cholesterol"
#define FOOD_SATURATED      @"saturated"
#define FOOD_TRANSQUANTI    @"ctransquantic"
#define FORPEOPLE           @"forpeople"

// 커뮤니티 최근 검색 키워드
#define RECENT_SEARCH_KEYWORD           @"keyword"
#define RECENT_SEARCH_TIMESTAMP         @"timestamp"

// + ----------------------
// 앱 로그 필드
// + ----------------------
#define LOG_L_COD                       @"l_cod"
#define LOG_M_COD                       @"m_cod"
#define LOG_S_COD                       @"s_cod"
#define LOG_TIME                        @"time"
#define LOG_COUNT                       @"count"
#define LOG_REGDATE                     @"regdate"

@interface MedicareDataBase : NSObject

- (void)createTableFoodCalorie;
- (void)allTableDelete;

@end
