//
//  SugarDBWraper.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "SugarDBWraper.h"

@implementation SugarDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}

- (void) DeleteDb:(NSString*)idx
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE idx=='%@'", TABLE_SUGAR, idx];
    NSLog(@"sql:%@", sql);
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}


- (void) UpdateDb:(NSString*)idx
            sugar:(NSString*)sugar
         drugname:(NSString*)drugname
           before:(NSString*)before
           reg_de:(NSString*)reg_de
{
    
    NSString *time = [Utils getTimeFormat:reg_de beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@='%@', %@='%@', %@='%@', %@='%@' WHERE %@='%@'",
                     TABLE_SUGAR,  // Table
                     SUGAR_SUGAR, sugar,
                     SUGAR_DRUGNAME, drugname,
                     SUGAR_BEFORE, before,
                     SUGAR_REGDATE, time,
                     SUGAR_IDX, idx];
    
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"UpdateDb Error");
    }else {
        NSLog(@"UpdateDb OK");
    }
}


- (void) insert:(NSArray*)datas{
    
    NSString *sqlHeader  = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@) VALUES ",
                      TABLE_SUGAR, SUGAR_IDX, SUGAR_SUGAR, SUGAR_HILOW, SUGAR_BEFORE, SUGAR_DRUGNAME, SUGAR_REGTYPE, SUGAR_REGDATE];
    
    for( int i = 0 ; i < datas.count ; i ++){
        
        if ([[[datas objectAtIndex:i] objectForKey:@"reg_de"] length] != 12
                && [[[datas objectAtIndex:i] objectForKey:@"regdate"] length] != 12) continue;
        
        NSString *reg_de = @"";
        if ([[datas objectAtIndex:i] objectForKey:@"reg_de"]) {
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"reg_de"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }else{
            reg_de = [Utils getTimeFormat:[[datas objectAtIndex:i] objectForKey:@"regdate"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        }
        
        NSString *value = [NSString stringWithFormat:@"('%@', '%@', '%@', '%@', '%@', '%@', '%@')",
                           [[datas objectAtIndex:i] objectForKey:SUGAR_IDX],
                           [[datas objectAtIndex:i] objectForKey:SUGAR_SUGAR],
                           [[datas objectAtIndex:i] objectForKey:@"hiLow"],
                           [[datas objectAtIndex:i] objectForKey:SUGAR_BEFORE],
                           [[datas objectAtIndex:i] objectForKey:SUGAR_DRUGNAME],
                           [[datas objectAtIndex:i] objectForKey:SUGAR_REGTYPE],
                           reg_de];
        
        NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];
        
        NSLog(@"sql : %@", sql);
        
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"InsertDb Error");
        }else {
            NSLog(@"InsertDb OK");
        }
    }
    
}

- (NSArray*) getResult{
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                         SUGAR_IDX, SUGAR_SUGAR, SUGAR_HILOW, SUGAR_BEFORE, SUGAR_DRUGNAME, SUGAR_REGTYPE, SUGAR_REGDATE,
                         TABLE_SUGAR];
    
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC",strsql, SUGAR_REGDATE, SUGAR_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *sugar = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *hilow = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *before = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *drugname =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,4)];
        NSString *regtype = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", sugar, @"sugar", hilow, @"hilow", before, @"before",
                                drugname, @"drugname", regtype, @"regtype", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


- (NSArray*) getBottomResult:(NSString *)sDate eDate:(NSString *)eDate eatState:(int)eatSate{
    sqlite3_stmt *statement;
    
    NSString *eat = @"";
    
    if(eatSate == 0){
        eat = @"'0','1','2','3',''";
    }else if(eatSate == 1){
        eat = @"'0','1','3'";
    }else if(eatSate == 2){
        eat = @"'2'";
    }
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ IFNull(AVG(CASE WHEN (%@ != '2') THEN cast(%@ as integer) End),0) as BEFSUGAR, ", strsql, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ IFNull(AVG(CASE WHEN (%@ = '2') THEN cast(%@ as integer) End),0) as AFTSUGAR, ", strsql, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ IFNull(MAX(CASE WHEN (%@ in (%@)) THEN cast(%@ as integer) End),0) as MAXSUGAR, ", strsql, SUGAR_BEFORE, eat, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ IFNull(MIN(CASE WHEN (%@ in (%@)) THEN cast(%@ as integer) End),0) as MINSUGAR ", strsql, SUGAR_BEFORE, eat, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN  '%@ 00:00' AND '%@ 23:59' AND %@ = '' AND cast(%@ as integer) > 0", strsql, SUGAR_REGDATE, sDate, eDate, SUGAR_DRUGNAME, SUGAR_SUGAR];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *BEFSUGAR = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *AFTSUGAR = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *MAXSUGAR = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *MINSUGAR = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                BEFSUGAR, @"BEFSUGAR", AFTSUGAR, @"AFTSUGAR", MAXSUGAR, @"MAXSUGAR", MINSUGAR, @"MINSUGAR",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}

- (NSArray*) getResultMain:(NSString *)nDate {
    sqlite3_stmt *statement;
    
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                         SUGAR_IDX, SUGAR_SUGAR, SUGAR_HILOW, SUGAR_BEFORE, SUGAR_DRUGNAME, SUGAR_REGTYPE, SUGAR_REGDATE,
                         TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ = '' AND cast(%@ as integer) > 0 AND %@ BETWEEN '%@ 00:00' AND '%@ 23:59'", strsql, SUGAR_DRUGNAME, SUGAR_SUGAR, SUGAR_REGDATE, nDate, nDate];
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) DESC LIMIT 1",strsql, SUGAR_REGDATE, SUGAR_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *sugar = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *hilow = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *before = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *drugname = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *regtype = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *regdate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", sugar, @"sugar", hilow, @"hilow", before, @"before",
                                drugname, @"drugname", regtype, @"regtype", regdate, @"regdate",
                                nil];
        
        [arrResult addObject:dicTemp];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


#pragma mark - chart select

/**
 * 혈당 일 그래프
 **/
- (NSArray*) getResultDay:(NSString *)nDate 
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=1 AND (%@ !='2') THEN %@ End) as H1B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=1 AND (%@ ='2') THEN %@ End) as H1A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=2 AND (%@ !='2') THEN %@ End) as H2B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=2 AND (%@ ='2') THEN %@ End) as H2A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=3 AND (%@ !='2') THEN %@ End) as H3B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=3 AND (%@ ='2') THEN %@ End) as H3A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=4 AND (%@ !='2') THEN %@ End) as H4B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=4 AND (%@ ='2') THEN %@ End) as H4A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=5 AND (%@ !='2') THEN %@ End) as H5B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=5 AND (%@ ='2') THEN %@ End) as H5A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=6 AND (%@ !='2') THEN %@ End) as H6B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=6 AND (%@ ='2') THEN %@ End) as H6A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=7 AND (%@ !='2') THEN %@ End) as H7B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=7 AND (%@ ='2') THEN %@ End) as H7A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=8 AND (%@ !='2') THEN %@ End) as H8B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=8 AND (%@ ='2') THEN %@ End) as H8A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=9 AND (%@ !='2') THEN %@ End) as H9B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=9 AND (%@ ='2') THEN %@ End) as H9A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=10 AND (%@ !='2') THEN %@ End) as H10B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=10 AND (%@ ='2') THEN %@ End) as H10A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=11 AND (%@ !='2') THEN %@ End) as H11B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=11 AND (%@ ='2') THEN %@ End) as H11A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=12 AND (%@ !='2') THEN %@ End) as H12B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=12 AND (%@ ='2') THEN %@ End) as H12A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=13 AND (%@ !='2') THEN %@ End) as H13B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=13 AND (%@ ='2') THEN %@ End) as H13A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=14 AND (%@ !='2') THEN %@ End) as H14B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=14 AND (%@ ='2') THEN %@ End) as H14A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=15 AND (%@ !='2') THEN %@ End) as H15B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=15 AND (%@ ='2') THEN %@ End) as H15A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=16 AND (%@ !='2') THEN %@ End) as H16B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=16 AND (%@ ='2') THEN %@ End) as H16A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=17 AND (%@ !='2') THEN %@ End) as H17B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=17 AND (%@ ='2') THEN %@ End) as H17A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=18 AND (%@ !='2') THEN %@ End) as H18B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=18 AND (%@ ='2') THEN %@ End) as H18A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=19 AND (%@ !='2') THEN %@ End) as H19B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=19 AND (%@ ='2') THEN %@ End) as H19A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=20 AND (%@ !='2') THEN %@ End) as H20B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=20 AND (%@ ='2') THEN %@ End) as H20A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=21 AND (%@ !='2') THEN %@ End) as H21B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=21 AND (%@ ='2') THEN %@ End) as H21A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=22 AND (%@ !='2') THEN %@ End) as H22B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=22 AND (%@ ='2') THEN %@ End) as H22A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=23 AND (%@ !='2') THEN %@ End) as H23B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=23 AND (%@ ='2') THEN %@ End) as H23A,", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=0 AND (%@ !='2') THEN %@ End) as H0B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR]; // 식전
    strsql = [NSString stringWithFormat:@"%@ AVG(CASE WHEN cast(strftime('$', %@) as integer)=0 AND (%@ ='2') THEN %@ End) as H0A", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR]; // 식후
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND %@ = '' AND cast(%@ as integer) > 0 ", strsql, SUGAR_REGDATE, nDate, nDate, SUGAR_DRUGNAME, SUGAR_SUGAR];
   
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer)", strsql, SUGAR_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%H"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%d"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h0b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)]; // 식전
        NSString *h0a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)]; // 식후
        NSString *h1b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h1a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h2b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h2a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h3b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h3a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h4b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h4a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h5b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h5a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h6b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h6a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h7b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h7a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h8b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h8a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h9b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h9a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h10b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h10a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h11b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h11a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *h12b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *h12a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *h13b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *h13a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *h14b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *h14a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *h15b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        NSString *h15a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 31)];
        NSString *h16b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 32)];
        NSString *h16a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 33)];
        NSString *h17b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 34)];
        NSString *h17a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 35)];
        NSString *h18b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 36)];
        NSString *h18a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 37)];
        NSString *h19b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 38)];
        NSString *h19a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 39)];
        NSString *h20b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 40)];
        NSString *h20a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 41)];
        NSString *h21b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 42)];
        NSString *h21a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 43)];
        NSString *h22b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 44)];
        NSString *h22a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 45)];
        NSString *h23b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 46)];
        NSString *h23a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 47)];
        
//        h0b = [self replcaVal:h0b type:1];
        
        NSArray *before = @[h0b, h1b, h2b, h3b, h4b, h5b,
                            h6b, h7b, h8b, h9b, h10b, h11b,
                            h12b, h13b, h14b, h15b, h16b, h17b,
                            h18b, h19b, h20b, h21b, h22b, h23b];
        
        [arrResult addObject:before];
        
        NSArray *after = @[h0a, h1a, h2a, h3a, h4a, h5a,
                           h6a, h7a, h8a, h9a, h10a, h11a,
                           h12a, h13a, h14a, h15a, h16a, h17a,
                           h18a, h19a, h20a, h21a, h22a, h23a];
        
        [arrResult addObject:after];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0"];
        
        [arrResult addObject:before];
        
        NSArray *after = @[@"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0"];
        
        [arrResult addObject:after];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}


/**
 * 혈당 일 그래프 (투약)
 **/
- (NSArray*) getResultDrugDay:(NSString *)nDate
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=1 THEN %@ End) as H1A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=2 THEN %@ End) as H2A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=3 THEN %@ End) as H3A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=4 THEN %@ End) as H4A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=5 THEN %@ End) as H5A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=6 THEN %@ End) as H6A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=7 THEN %@ End) as H7A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=8 THEN %@ End) as H8A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=9 THEN %@ End) as H9A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=10 THEN %@ End) as H10A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=11 THEN %@ End) as H11A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=12 THEN %@ End) as H12A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=13 THEN %@ End) as H13A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=14 THEN %@ End) as H14A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=15 THEN %@ End) as H15A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=16 THEN %@ End) as H16A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=17 THEN %@ End) as H17A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=18 THEN %@ End) as H18A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=19 THEN %@ End) as H19A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=20 THEN %@ End) as H20A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=21 THEN %@ End) as H21A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=22 THEN %@ End) as H22A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=23 THEN %@ End) as H23A, ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('$', %@) as integer)=0 THEN %@ End) as H0A ", strsql, SUGAR_REGDATE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59'  AND cast(%@ as integer) <= 0 ", strsql, SUGAR_REGDATE, nDate, nDate, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer)", strsql, SUGAR_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%H"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%d"];
    
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h0 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *h1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *h2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
       
        NSArray *resut = @[h0, h1, h2, h3, h4, h5,
                           h6, h7, h8, h9, h10, h11,
                           h12, h13, h14, h15, h16, h17,
                           h18, h19, h20, h21, h22, h23];
        
        
        [arrResult addObject:resut];
    }
    
    if(arrResult.count == 0){
        
        NSArray *resut = @[@"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0"];
        
        [arrResult addObject:resut];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}


/**
 * 혈당 주간 그래프
 **/
- (NSArray*) getResultWeek:(NSString *)sDate eDate:(NSString *)eDate 
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=0 AND (%@ !='2') THEN %@ End), 0) as W0B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=0 AND (%@ ='2') THEN %@ End), 0) as W0A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=1 AND (%@ !='2') THEN %@ End), 0) as W1B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=1 AND (%@ ='2') THEN %@ End), 0) as W1A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=2 AND (%@ !='2') THEN %@ End), 0) as W2B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=2 AND (%@ ='2') THEN %@ End), 0) as W2A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=3 AND (%@ !='2') THEN %@ End), 0) as W3B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=3 AND (%@ ='2') THEN %@ End), 0) as W3A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=4 AND (%@ !='2') THEN %@ End), 0) as W4B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=4 AND (%@ ='2') THEN %@ End), 0) as W4A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=5 AND (%@ !='2') THEN %@ End), 0) as W5B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=5 AND (%@ ='2') THEN %@ End), 0) as W5A, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=6 AND (%@ !='2') THEN %@ End), 0) as W6B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=6 AND (%@ ='2') THEN %@ End), 0) as W6A", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND %@ = '' AND cast(%@ as integer) > 0 ", strsql, SUGAR_REGDATE, sDate, eDate, SUGAR_DRUGNAME, SUGAR_SUGAR];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%w"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w0b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w0a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w1b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w1a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w2b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w2a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w3b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *w3a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *w4b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *w4a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *w5b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *w5a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *w6b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *w6a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        
        NSArray *before = @[w0b, w1b, w2b, w3b, w4b, w5b, w6b];
        
        [arrResult addObject:before];
        
        NSArray *after = @[w0a, w1a, w2a, w3a, w4a, w5a, w6a];
        
        [arrResult addObject:after];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
        
        [arrResult addObject:before];
        
        NSArray *after = @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
                
        [arrResult addObject:after];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}

/**
 * 혈당 주간 그래프(투약)
 **/
- (NSArray*) getResultWeekDrug:(NSString *)sDate eDate:(NSString *)eDate
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=0  THEN %@ End) as W0, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=1  THEN %@ End) as W1, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=2  THEN %@ End) as W2, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=3  THEN %@ End) as W3, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=4  THEN %@ End) as W4, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=5  THEN %@ End) as W5, ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=6  THEN %@ End) as W6 ", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE %@ BETWEEN '%@ 00:00' AND '%@ 23:59' AND cast(%@ as integer) <= 0 ", strsql, SUGAR_REGDATE, sDate, eDate, SUGAR_SUGAR];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%w"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        NSArray *before = @[w1, w2, w3, w4, w5, w6, w7];
        
        [arrResult addObject:before];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
        
        [arrResult addObject:before];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}

/**
 * 혈당 월간 그래프
 **/
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for(int i = 0 ; i < 30 ; i++){
        strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=%d AND (%@ !='2') THEN %@ End), 0) as D%dB, ", strsql, SUGAR_REGDATE, i+1, SUGAR_BEFORE, SUGAR_SUGAR, i+1];
        strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=%d AND (%@ ='2') THEN %@ End), 0) as D%dA, ", strsql, SUGAR_REGDATE, i+1, SUGAR_BEFORE, SUGAR_SUGAR, i+1];
    }
    strsql = [NSString stringWithFormat:@"%@ ifnull(MIN(CASE WHEN cast(strftime('^', %@) as integer)=31 AND (%@ !='2') THEN %@ End), 0) as D31B, ", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ ifnull(MAX(CASE WHEN cast(strftime('^', %@) as integer)=31 AND (%@ ='2') THEN %@ End), 0) as D31A", strsql, SUGAR_REGDATE, SUGAR_BEFORE, SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE cast(strftime('#', %@) as integer)=%@ AND cast(strftime('$', %@) as integer)=%@ AND %@ = '' AND cast(%@ as integer) > 0",
              strsql, SUGAR_REGDATE, nYear, SUGAR_REGDATE, nMonth, SUGAR_DRUGNAME, SUGAR_SUGAR];
    
    
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer), cast(strftime('$', %@) as integer)", strsql, SUGAR_REGDATE, SUGAR_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d0b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d0a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d1b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d1a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d2b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d2a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d3b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d3a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d4b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d4a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d5b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d5a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d6b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d6a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d7b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d7a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d8b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d8a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d9b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d9a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d10b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d10a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d11b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d11a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d12b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d12a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d13b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d13a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d14b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d14a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d15b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        NSString *d15a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 31)];
        NSString *d16b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 32)];
        NSString *d16a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 33)];
        NSString *d17b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 34)];
        NSString *d17a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 35)];
        NSString *d18b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 36)];
        NSString *d18a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 37)];
        NSString *d19b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 38)];
        NSString *d19a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 39)];
        NSString *d20b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 40)];
        NSString *d20a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 41)];
        NSString *d21b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 42)];
        NSString *d21a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 43)];
        NSString *d22b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 44)];
        NSString *d22a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 45)];
        NSString *d23b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 46)];
        NSString *d23a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 47)];
        NSString *d24b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 48)];
        NSString *d24a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 49)];
        NSString *d25b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 50)];
        NSString *d25a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 51)];
        NSString *d26b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 52)];
        NSString *d26a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 53)];
        NSString *d27b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 54)];
        NSString *d27a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 55)];
        NSString *d28b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 56)];
        NSString *d28a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 57)];
        NSString *d29b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 58)];
        NSString *d29a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 59)];
        NSString *d30b = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 60)];
        NSString *d30a = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 61)];
        
        
        NSArray *before =@[d0b, d1b, d2b, d3b, d4b, d5b,
                           d6b, d7b, d8b, d9b, d10b, d11b,
                           d12b, d13b, d14b, d15b, d16b, d17b,
                           d18b, d19b, d20b, d21b, d22b, d23b,
                           d24b, d25b, d26b, d27b, d28b, d29b,
                           d30b];
        
        [arrResult addObject:before];
        
        
        NSArray *after =@[d0a, d1a, d2a, d3a, d4a, d5a,
                          d6a, d7a, d8a, d9a, d10a, d11a,
                          d12a, d13a, d14a, d15a, d16a, d17a,
                          d18a, d19a, d20a, d21a, d22a, d23a,
                          d24a, d25a, d26a, d27a, d28a, d29a,
                          d30a];
        
        [arrResult addObject:after];
    }
    
    if(arrResult.count == 0){
        
        NSArray *before = @[@"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0", @"0", @"0", @"0", @"0", @"0",
                            @"0"];
        
        [arrResult addObject:before];
        
        NSArray *after = @[@"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0"];
        
        [arrResult addObject:after];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}


/**
 * 혈당 월간 그래프 (투약)
 **/
- (NSArray*) getResultMonthDrug:(NSString *)nYear nMonth:(NSString *)nMonth
{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT "];
    for(int i = 0 ; i < 30 ; i++){
        strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=%d  THEN %@ End) as D%d, ", strsql, SUGAR_REGDATE, i+1,  SUGAR_SUGAR, i+1];
    }
    strsql = [NSString stringWithFormat:@"%@ COUNT(CASE WHEN cast(strftime('^', %@) as integer)=31 THEN %@ End) as D31", strsql, SUGAR_REGDATE,  SUGAR_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE cast(strftime('#', %@) as integer)=%@ AND cast(strftime('$', %@) as integer)=%@ AND cast(%@ as integer) <= 0",
              strsql, SUGAR_REGDATE, nYear, SUGAR_REGDATE, nMonth, SUGAR_SUGAR];
    
    strsql = [NSString stringWithFormat:@"%@ GROUP BY cast(strftime('#', %@) as integer), cast(strftime('$', %@) as integer)", strsql, SUGAR_REGDATE, SUGAR_REGDATE];
    
    strsql = [strsql stringByReplacingOccurrencesOfString:@"^" withString:@"%d"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"#" withString:@"%Y"];
    strsql = [strsql stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d25 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d26 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d27 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d28 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d29 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d30 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d31 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        
        
        
        
        NSArray *after =@[d1, d2,d3,d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15,
                          d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,];
        
        [arrResult addObject:after];
    }
    
    if(arrResult.count == 0){
        
        NSArray *after = @[@"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0", @"0", @"0", @"0", @"0", @"0",
                           @"0"];
        
        [arrResult addObject:after];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
    
}


/**
 * 혈당 투약 그래프
 **/
- (NSArray*) getResultMediTime:(NSString *)sDate eDate:(NSString *)eDate{
    sqlite3_stmt *statement;
    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@ ", SUGAR_IDX, SUGAR_SUGAR, SUGAR_REGDATE, SUGAR_DRUGNAME];
    strsql = [NSString stringWithFormat:@"%@ FROM %@ ", strsql, TABLE_SUGAR];
    strsql = [NSString stringWithFormat:@"%@ WHERE cast(%@ as integer) <= 0 AND %@ BETWEEN '%@ 00:00' AND '%@ 23:59' ", strsql, SUGAR_SUGAR, SUGAR_REGDATE, sDate, eDate];
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) desc, cast(%@ as BIGINT) asc", strsql, SUGAR_REGDATE, SUGAR_IDX];
    
    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {
        NSString *idx       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *sugar       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *regdate     = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *drugname   = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        
        NSDictionary *after =[NSDictionary dictionaryWithObjectsAndKeys:
                              idx, @"idx", sugar, @"sugar", regdate, @"regdate",drugname, @"drugname",
                              nil];
        
        [arrResult addObject:after];
    }
    
    sqlite3_finalize(statement);
    
    return arrResult;
}


#pragma mark - sugar method

-(NSString *)replcaVal:(NSString *)value type:(int)type{
    NSString *returnVal = @"";
    int val = [value intValue];
    int max = type == 1 ? 150 : 240;
    
    if(val == 0){
        return @"0";
    }
    
    if(60 > val){
        returnVal = @"60";
    }else { //if(max < val)
        returnVal = [NSString stringWithFormat:@"%d",max];
    }
    
    return returnVal;
}

@end
