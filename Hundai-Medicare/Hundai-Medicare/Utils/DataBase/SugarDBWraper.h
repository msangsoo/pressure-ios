//
//  SugarDBWraper.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"

@interface SugarDBWraper : MedicareDataBase

- (void) DeleteDb:(NSString*)idx;
- (void) UpdateDb:(NSString*)idx sugar:(NSString*)sugar drugname:(NSString*)drugname before:(NSString*)before reg_de:(NSString*)reg_de;
- (void) insert:(NSArray*)datas;
- (NSArray*) getResult;
- (NSArray*) getBottomResult:(NSString *)sDate eDate:(NSString *)eDate eatState:(int)eatSate;
- (NSArray*) getResultMain:(NSString *)nDate;

- (NSArray*) getResultDay:(NSString *)nDate;
- (NSArray*) getResultWeek:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth;
- (NSArray*) getResultDrugDay:(NSString *)nDate;
- (NSArray*) getResultWeekDrug:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultMonthDrug:(NSString *)nYear nMonth:(NSString *)nMonth;

- (NSArray*) getResultMediTime:(NSString *)sDate eDate:(NSString *)eDate;
@end
