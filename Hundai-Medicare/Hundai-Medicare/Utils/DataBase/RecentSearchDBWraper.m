//
//  RecentSearchDBWraper.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 23/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "RecentSearchDBWraper.h"

@implementation RecentSearchDBWraper

-(id) init
{
    // Table Create
    self = [super init];

    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];

    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}

- (void) insert:(NSString*)keyword
{
    if(keyword == nil && keyword.length == 0)
    {
        return;
    }
    NSString *sqlHeader  = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (%@, %@) VALUES ", TABLE_RECENT_SEARCH, RECENT_SEARCH_KEYWORD, RECENT_SEARCH_TIMESTAMP];

    NSString* date = [Utils getNowDate:@"yyyy-MM-dd HH:mm:ss"];
    NSString *value = [NSString stringWithFormat:@"('%@', '%@')", keyword, date];

    NSString *sql = [NSString stringWithFormat:@"%@ %@", sqlHeader, value];

    NSLog(@"sql : %@", sql);

    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"InsertDb Error");
    }else {
        NSLog(@"InsertDb OK");
    }
}

- (NSMutableArray*) getKeywords:(int)limit
{
    sqlite3_stmt *statement;

    NSString *strsql  = [NSString stringWithFormat:@"SELECT %@ FROM %@ ",
                         RECENT_SEARCH_KEYWORD, TABLE_RECENT_SEARCH];
    strsql = [NSString stringWithFormat:@"%@ ORDER BY datetime(%@) DESC LIMIT %d",strsql, RECENT_SEARCH_TIMESTAMP, limit];

    const char *sql = [strsql UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        return nil;
    }

    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    while (sqlite3_step(statement)==SQLITE_ROW) {

        NSString *keyword = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];

        [arrResult addObject:keyword];
    }

    sqlite3_finalize(statement);

    return arrResult;
}
-(void) truncateDb
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@", TABLE_RECENT_SEARCH];
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"TABLE_RECENT_SEARCH Delete Error");
    }else {
        NSLog(@"TABLE_RECENT_SEARCH Delete OK");
    }
}
- (void) deleteDb:(NSString*)keyword
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@=='%@'", TABLE_RECENT_SEARCH, RECENT_SEARCH_KEYWORD, keyword];
    NSLog(@"sql:%@", sql);

    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Error");
    }else {
        NSLog(@"DeleteDb OK");
    }
}

@end
