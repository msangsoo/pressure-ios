//
//  FoodCalorieDBWraper.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodDetailDBWraper.h"


@implementation FoodDetailDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}

-(NSMutableArray *) getUserResult:(NSString *)idx{
    
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT code, kind, name, tb_data_food_detail.forpeople, gram, unit, calorie, carbohydrate, protein, fat, sugars, salt, cholesterol, saturated, ctransquantic"];
    [sql appendString:@" From tb_data_food_calorie "];
    [sql appendString:@" INNER JOIN tb_data_food_detail ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_detail.idx = '%@'", idx]];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getUserResult '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *code = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *kind = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,1)];
        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,2)];
        NSString *forpeople = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *gram = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *unit = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *calorie = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *carbohydrate = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *protein = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *fat = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *sugars = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *salt = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *cholesterol = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *saturated = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *ctransquantic = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        
        gram            = [Utils getNoneZeroString:[gram floatValue] * [forpeople floatValue]];
        calorie         = [Utils getNoneZeroString:[calorie floatValue] * [forpeople floatValue]];
        carbohydrate    = [Utils getNoneZeroString:[carbohydrate floatValue] * [forpeople floatValue]];
        protein         = [Utils getNoneZeroString:[protein floatValue] * [forpeople floatValue]];
        fat             = [Utils getNoneZeroString:[fat floatValue] * [forpeople floatValue]];
        sugars          = [Utils getNoneZeroString:[sugars floatValue] * [forpeople floatValue]];
        salt            = [Utils getNoneZeroString:[salt floatValue] * [forpeople floatValue]];
        cholesterol     = [Utils getNoneZeroString:[cholesterol floatValue] * [forpeople floatValue]];
        saturated       = [Utils getNoneZeroString:[saturated floatValue] * [forpeople floatValue]];
        ctransquantic   = [Utils getNoneZeroString:[ctransquantic floatValue] * [forpeople floatValue]];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                code, @"code", kind, @"kind", name, @"name", forpeople, @"forpeople",
                                gram, @"gram", unit, @"unit", calorie, @"calorie", carbohydrate, @"carbohydrate",
                                protein, @"protein", fat, @"fat", sugars, @"sugars", salt, @"salt",
                                cholesterol, @"cholesterol", saturated, @"saturated", ctransquantic, @"ctransquantic",
                                nil];
        
        [arrRtn addObject:dicTemp];
    }
    
    return arrRtn;
}

// 건 바이 등록
- (BOOL) insertFoodDetailDb:(NSDictionary*)dicDetail {
    
    sqlite3_stmt *updateStmt = nil;
    
    // 삭제
    NSString *delSql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@=%@", TABLE_FOOD_DETAIL, FOODDETAIL_IDX, [dicDetail objectForKey:@"idx"]];
    if (sqlite3_exec(database, [delSql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Delete FoodMain failed %@", updateStmt);
    }
    
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_DETAIL, FOODDETAIL_IDX, FOODDETAIL_FOODCODE, FOODDETAIL_FORPEOPLE, FOODDETAIL_REGTYPE];
    
    NSString *idx               = [dicDetail objectForKey:@"idx"];
    NSString *foodcode          = [dicDetail objectForKey:@"foodcode"];
    NSString *forpeople         = [dicDetail objectForKey:@"forpeople"];
    NSString *regdate           = [dicDetail objectForKey:@"regdate"];
    
    NSString *queryValue = [NSString stringWithFormat:@" %@ ('%@', '%@', '%@', '%@'); ",
                            queryHead, idx, foodcode, forpeople, regdate];
    
    NSLog(@"query:%@",queryValue);
    if (sqlite3_exec(database, [queryValue UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"INSERT FoodSub failed %@",updateStmt);
    }
    return YES;
}


- (void) insert:(NSArray*)arrDetail
{
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_DETAIL, FOODDETAIL_IDX, FOODDETAIL_FOODCODE, FOODDETAIL_FORPEOPLE, FOODDETAIL_REGTYPE];
     
    
    for (int i=0; i < [arrDetail count]; i++) {
        NSDictionary *dicDetail     = [arrDetail objectAtIndex:i];
        
        if ([[dicDetail objectForKey:@"regdate"] length] < 12) continue;
        NSString *regdate = [[dicDetail objectForKey:@"regdate"] substringToIndex:12];
        regdate = [Utils getTimeFormat:regdate beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        
        NSString *idx               = [dicDetail objectForKey:@"idx"];
        NSString *foodcode          = [dicDetail objectForKey:@"foodcode"];
        NSString *forpeople         = [dicDetail objectForKey:@"forpeople"];
        
        NSString *queryValue = [NSString stringWithFormat:@" ('%@', '%@', '%@', '%@'); ", idx, foodcode, forpeople, regdate];
        NSString *sql = [NSString stringWithFormat:@"%@ %@", queryHead, queryValue];
        
        NSLog(@"sql:%@",sql);
        sqlite3_stmt *updateStmt = nil;
        if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"INSERT FoodSub failed %@",updateStmt);
        }
    }
}


// 일괄등록
- (BOOL) initFoodDetailDb:(NSArray*)arrDetail idx:(NSString*)idx regdate:(NSString *)regdate{
    
    
    // 삭제
    NSString *delSql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@='%@' ", TABLE_FOOD_DETAIL, FOODDETAIL_IDX, idx];
    if (sqlite3_exec(database, [delSql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Delete FoodMain failed %@", updateStmt);
    }
    
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_DETAIL, FOODDETAIL_IDX, FOODDETAIL_FOODCODE, FOODDETAIL_FORPEOPLE, FOODDETAIL_REGTYPE];
    NSString *time = [Utils getTimeFormat:regdate beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
    
    for (int i=0; i < [arrDetail count]; i++) {
        NSDictionary *dicDetail     = [arrDetail objectAtIndex:i];
        
        
        NSString *foodcode          = [dicDetail objectForKey:@"code"];
        NSString *forpeople         = [dicDetail objectForKey:@"forpeople"];
        
        NSString *queryValue = [NSString stringWithFormat:@" %@ ('%@', '%@', '%@', '%@'); ",
                                queryHead, idx, foodcode, forpeople, time];
        
        NSLog(@"query:%@",queryValue);
        sqlite3_stmt *updateStmt = nil;
        if (sqlite3_exec(database, [queryValue UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"INSERT FoodSub failed %@",updateStmt);
        }
    }
    return YES;
}


@end
