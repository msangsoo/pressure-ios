//
//  WalkDBWraper.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 3. 31..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"

@interface WalkDBWraper : MedicareDataBase
- (void) DeleteDb:(NSString*)idx;
- (void) insert:(NSArray*)datas;
- (void) UpdateDb:(NSString*)idx
           calory:(NSString*)calory
         distance:(NSString*)distance
             step:(NSString*)step
        heartrate:(NSString*)heartrate
           stress:(NSString*)stress
          regtype:(NSString*)regtype
           reg_de:(NSString*)reg_de;

- (NSArray*) getResult;
- (NSArray*) getResultMain:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultDay:(NSString *)nDate walk:(Boolean)walk;
- (NSArray*) getResultWeek:(NSString *)nDate eDate:(NSString *)eDate walk:(Boolean)walk;
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth walk:(Boolean)walk;
@end
