//
//  FoodCalorieDBWraper.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "CHCSVParser.h"
#import "Utils.h"
#import "Model.h"

@interface FoodMainDBWraper : MedicareDataBase

//서버에서 받은 3개월데이터 넣기.
- (BOOL) insert:(NSArray*)arrMain;
- (BOOL) insertFoodMainDb:(NSDictionary*)dicMain;
- (BOOL) updateFoodMain:(NSDictionary*)dicMain foodData:(NSArray*)arrFood;
- (void)updateImage:(NSString *)idx filename:(NSString *)filename;

-(NSMutableArray *) getMealSum:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getFoodMainResult:(NSString*)nDate;
- (NSArray*) getDayResult:(NSString*)nDate;
- (NSArray*) getWeekResult:(NSString*)sDate eDate:(NSString *)eDate;
- (NSArray*) getMonthResult:(NSString*)nYear nMonth:(NSString*)nMonth;
- (NSArray*) getRadial:(NSString*)sDate eDate:(NSString*)eDate totTakeCal:(float)totTakeCal recomCal:(float)recomCal dayCnt:(int)dayCnt;
- (NSArray*) getRadial:(NSString*)sDate eDate:(NSString*)eDate totTakeCal:(float)totTakeCal recomCal:(float)recomCal;
- (NSString*) getResultMain:(NSString*)sDate eDate:(NSString*)eDate;

@end
