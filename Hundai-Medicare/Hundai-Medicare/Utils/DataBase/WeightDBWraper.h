//
//  WeightDBWraper.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"
#import "TimeUtils.h"

@interface WeightDBWraper : MedicareDataBase


- (void) DeleteDb:(NSString*)idx;
- (void) UpdateDb:(NSString*)idx fat:(NSString*)fat obesity:(NSString*)obesity bodywater:(NSString*)bodywater muscle:(NSString*)muscle weight:(NSString*)weight reg_de:(NSString*)reg_de;
- (void) insert:(NSArray*)datas isServerReg:(Boolean)isServerReg;
- (void) insert:(NSArray*)datas;

- (NSArray *)getResult;
- (NSArray *)getBottomResult;
- (NSString *)getAvgWeight:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray*) getResultDay:(NSString *)nDate type:(int)type;
- (NSArray*) getResultWeek:(NSString *)sDate eDate:(NSString *)eDate type:(int)type;
- (NSArray*) getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth type:(int)type;
- (NSArray*) getResultYear:(NSString *)nYear type:(int)type;
- (NSArray*) getResult40Week:(NSArray *)weeks;
@end
