//
//  FoodCalorieDBWraper.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 3..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodMainDBWraper.h"


@implementation FoodMainDBWraper

-(id) init
{
    // Table Create
    self = [super init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:DBFILE_NAME];
    
    if (sqlite3_open([filePath UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        NSLog(@"Error");
        return nil;
    }
    return self;
}
  
- (BOOL) insert:(NSArray*)arrMain {
    
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_MAIN, FOODMAIN_IDX, FOODMAIN_AMOUNTTIME, FOODMAIN_MEALTTYPE, FOODMAIN_CALORIE, FOODMAIN_PICTURE, FOODMAIN_REGTYPE];
    
    for (int i=0; i < [arrMain count]; i++) {
        NSDictionary *calRow = [arrMain objectAtIndex:i];
        
        
        if ([[calRow objectForKey:@"regdate"] length] < 12) continue;
        
        NSString *regdate = [[calRow objectForKey:@"regdate"] substringToIndex:12];
        regdate = [Utils getTimeFormat:regdate beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
        
        
        NSString *idx               = [calRow objectForKey:@"idx"];
        NSString *amounttime        = [calRow objectForKey:@"amounttime"];
        NSString *mealtype          = [calRow objectForKey:@"mealtype"];
        NSString *calorie           = [calRow objectForKey:@"calorie"];
        NSString *picture           = [calRow objectForKey:@"picture"];
        
        NSString *queryValue = [NSString stringWithFormat:@" %@ ('%@', '%@', '%@', '%@', '%@', '%@'); ",
                                  queryHead, idx, amounttime, mealtype, calorie, picture, regdate];
        
        NSLog(@"query:%@",queryValue);
        sqlite3_stmt *updateStmt = nil;
        if (sqlite3_exec(database, [queryValue UTF8String], nil,nil,nil) != SQLITE_OK) {
            NSLog(@"INSERT FoodMain failed %@",updateStmt);
        }
    }
    return YES;
}

- (BOOL) insertFoodMainDb:(NSDictionary*)dicMain {
    
    sqlite3_stmt *updateStmt = nil;
    
    // 삭제
    NSString *delSql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@=%@", TABLE_FOOD_MAIN, FOODMAIN_IDX, [dicMain objectForKey:@"idx"]];
    if (sqlite3_exec(database, [delSql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"Delete FoodMain failed %@", updateStmt);
    }
    
    NSString *time = [Utils getTimeFormat:[dicMain objectForKey:@"regdate"] beTime:@"yyyyMMddHHmm" afTime:@"yyyy-MM-dd HH:mm"];
    
    NSString *queryHead = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES ",
                           TABLE_FOOD_MAIN, FOODMAIN_IDX, FOODMAIN_AMOUNTTIME, FOODMAIN_MEALTTYPE, FOODMAIN_CALORIE, FOODMAIN_PICTURE, FOODMAIN_REGTYPE];
    
    NSString *idx               = [dicMain objectForKey:@"idx"];
    NSString *amounttime        = [dicMain objectForKey:@"amounttime"];
    NSString *mealtype          = [dicMain objectForKey:@"mealtype"];
    NSString *calorie           = [dicMain objectForKey:@"calorie"];
    NSString *picture           = [dicMain objectForKey:@"picture"];
    NSString *regdate           = time;
    
    NSString *queryValue = [NSString stringWithFormat:@" %@ ('%@', '%@', '%@', '%@', '%@', '%@'); ",
                            queryHead, idx, amounttime, mealtype, calorie, picture, regdate];
    
    NSLog(@"query:%@",queryValue);
    if (sqlite3_exec(database, [queryValue UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"INSERT FoodMain failed %@",updateStmt);
    }
    return YES;
}



- (BOOL) updateFoodMain:(NSDictionary*)dicMain foodData:(NSArray*)arrFood {
    
    // 메인테이블 소요시간,식사일 업데이트하고
    
    // 서브테이블 삭제후 다시 인서트
    
    return YES;
}

- (void)updateImage:(NSString *)idx filename:(NSString *)filename{
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@='%@' WHERE %@='%@'",TABLE_FOOD_MAIN,  FOODMAIN_PICTURE, filename, FOODMAIN_IDX, idx];
    
    NSLog(@"sql:%@", sql);
    if (sqlite3_exec(database, [sql UTF8String], nil,nil,nil) != SQLITE_OK) {
        NSLog(@"UpdateDb Error");
    }else {
        NSLog(@"UpdateDb OK");
    }
}



/*
 // 일단위 호출(칼로리 및 소요시간을 리턴함)
 */
- (NSArray*) getFoodMainResult:(NSString*)nDate {
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@" tb_data_food_main.idx, mealtype, amounttime, picture, tb_data_food_main.regdate, SUM(ifnull(tb_data_food_calorie.calorie, 0) * ifnull(tb_data_food_detail.forpeople,0)) as food_calorie "];
    [sql appendString:@", SUM(ifnull(tb_data_food_calorie.carbohydrate, 0) * ifnull(tb_data_food_detail.forpeople,0)) as food_car "];
    [sql appendString:@", SUM(ifnull(tb_data_food_calorie.protein, 0) * ifnull(tb_data_food_detail.forpeople,0)) as food_protein "];
    [sql appendString:@", SUM(ifnull(tb_data_food_calorie.fat, 0) * ifnull(tb_data_food_detail.forpeople,0)) as food_fat "];
    [sql appendString:@" From tb_data_food_main "];
    [sql appendString:@" LEFT JOIN tb_data_food_detail ON tb_data_food_main.idx = tb_data_food_detail.idx "];
    [sql appendString:@" LEFT JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_main.regdate BETWEEN '%@ 00:00' and '%@ 23:59' ", nDate, nDate]];
    [sql appendString:@" Group by tb_data_food_main.mealtype "];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getFoodMainResult '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *idx           = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *mealtype      = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *amounttime    = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *picture       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *regdate       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *calorie       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *food_car      = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *food_protein  = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *food_fat      = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                idx, @"idx", mealtype, @"mealtype", amounttime, @"amounttime", picture, @"picture",
                                regdate, @"regdate", calorie, @"calorie",
                                food_car, @"food_car", food_protein, @"food_protein", food_fat, @"food_fat", nil];
        
        [arrRtn addObject:dicTemp];
    }

    return arrRtn;
}

-(NSMutableArray *) getMealSum:(NSString *)sDate eDate:(NSString *)eDate{

    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@"ifnull(SUM(CASE WHEN tb_data_food_main.mealtype in ('a', 'd') THEN "];
    [sql appendString:@"tb_data_food_calorie.calorie * tb_data_food_detail.forpeople End),0) as breakfast,"];
    [sql appendString:@"ifnull(SUM(CASE WHEN tb_data_food_main.mealtype in ('b', 'e') THEN "];
    [sql appendString:@"tb_data_food_calorie.calorie * tb_data_food_detail.forpeople End),0) as lunch,"];
    [sql appendString:@"ifnull(SUM(CASE WHEN tb_data_food_main.mealtype in ('c', 'f') THEN "];
    [sql appendString:@"tb_data_food_calorie.calorie * tb_data_food_detail.forpeople End),0) as dinner,"];
    [sql appendString:@"ifnull(AVG(CASE WHEN tb_data_food_main.mealtype in ('a') THEN amounttime End),0) as break_time,"];
    [sql appendString:@"ifnull(AVG(CASE WHEN tb_data_food_main.mealtype in ('b') THEN amounttime End),0) as lunch_time,"];
    [sql appendString:@"ifnull(AVG(CASE WHEN tb_data_food_main.mealtype in ('c') THEN amounttime End),0) as dinner_time"];
    [sql appendString:@" From tb_data_food_main "];
    [sql appendString:@" INNER JOIN tb_data_food_detail ON tb_data_food_main.idx = tb_data_food_detail.idx "];
    [sql appendString:@" INNER JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_detail.regdate BETWEEN '%@ 00:00' and '%@ 23:59' ", sDate, eDate]];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getMealSum '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *breakfast     = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *lunch         = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *dinner        = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *break_time    = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *lunch_time    = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *dinner_time   = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                breakfast, @"breakfast", lunch, @"lunch", dinner, @"dinner",
                                break_time, @"break_time", lunch_time, @"lunch_time", dinner_time, @"dinner_time", nil];
        
        [arrRtn addObject:dicTemp];
    }
    
    return arrRtn;
}


- (NSArray*) getDayResult:(NSString*)nDate
{
    sqlite3_stmt *statement;
    NSArray *arrRtn2;
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=1  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H1, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=2  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H2, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=3  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H3, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=4  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H4, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=5  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H5, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=6  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H6, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=7  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H7, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=8  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H8, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=9  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H9, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=10  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H10, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=11  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H11, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=12  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H12, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=13  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H13, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=14  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H14, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=15  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H15, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=16  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H16, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=17  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H17, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=18  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H18, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=19  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H19, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=20  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H20, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=21  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H21, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=22  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H22, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=23  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H23, "];
    [sql appendString:@" IFNULL(SUM(CASE WHEN cast(strftime('%H', tb_data_food_main.regdate) as integer)=24  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as H24  "];
    [sql appendString:@" From tb_data_food_main "];
    [sql appendString:@" LEFT JOIN tb_data_food_detail ON tb_data_food_main.idx = tb_data_food_detail.idx "];
    [sql appendString:@" LEFT JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_main.regdate BETWEEN '%@ 00:00' and '%@ 23:59' ", nDate, nDate]];
    [sql appendString:@" Group by cast(strftime('%d', tb_data_food_main.regdate) as integer) "];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getDayResult '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *h1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *h2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *h3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *h4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *h5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *h6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *h7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *h8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *h9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *h10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *h11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *h12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *h13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *h14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *h15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *h16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *h17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *h18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *h19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *h20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *h21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *h22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *h23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *h24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        
        
        arrRtn2 = @[@([h1 intValue]), @([h2 intValue]), @([h3 intValue]), @([h4 intValue]), @([h5 intValue]), @([h6 intValue]),
                    @([h7 intValue]), @([h8 intValue]), @([h9 intValue]), @([h10 intValue]), @([h11 intValue]), @([h12 intValue]),
                    @([h13 intValue]), @([h14 intValue]), @([h15 intValue]), @([h16 intValue]), @([h17 intValue]), @([h18 intValue]),
                    @([h19 intValue]), @([h20 intValue]), @([h21 intValue]), @([h22 intValue]), @([h23 intValue]), @([h24 intValue])];
        
    }
    
    return arrRtn2;
}



- (NSArray*) getWeekResult:(NSString*)sDate eDate:(NSString *)eDate
{
    sqlite3_stmt *statement;
    NSArray *arrRtn2;
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=0  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W1, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=1  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W2, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=2  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W3, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=3  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W4, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=4  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W5, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=5  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W6, "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%w', tb_data_food_detail.regdate) as integer)=6  THEN CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)  End),0) as W7 "];
    [sql appendString:@" From tb_data_food_detail "];
    [sql appendString:@" LEFT JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_detail.regdate BETWEEN '%@ 00:00' and '%@ 23:59' ;", sDate, eDate]];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getWeekResult '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *w1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *w2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *w3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *w4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *w5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *w6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *w7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        
        
        arrRtn2 = @[@([w1 intValue]), @([w2 intValue]), @([w3 intValue]), @([w4 intValue]), @([w5 intValue]), @([w6 intValue]),
                    @([w7 intValue])];
    }
    
    
    return arrRtn2;
}

- (NSArray*) getMonthResult:(NSString*)nYear nMonth:(NSString*)nMonth
{
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    NSArray *arrRtn2;
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=1  THEN calorie * tb_data_food_detail.forpeople End), 0) as D1,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=2  THEN calorie * tb_data_food_detail.forpeople End), 0) as D2,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=3  THEN calorie * tb_data_food_detail.forpeople End), 0) as D3,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=4  THEN calorie * tb_data_food_detail.forpeople End), 0) as D4,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=5  THEN calorie * tb_data_food_detail.forpeople End), 0) as D5,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=6  THEN calorie * tb_data_food_detail.forpeople End), 0) as D6,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=7  THEN calorie * tb_data_food_detail.forpeople End), 0) as D7,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=8  THEN calorie * tb_data_food_detail.forpeople End), 0) as D8,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=9  THEN calorie * tb_data_food_detail.forpeople End), 0) as D9,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=10  THEN calorie * tb_data_food_detail.forpeople End), 0) as D10,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=11  THEN calorie * tb_data_food_detail.forpeople End), 0) as D11,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=12  THEN calorie * tb_data_food_detail.forpeople End), 0) as D12,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=13  THEN calorie * tb_data_food_detail.forpeople End), 0) as D13,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=14  THEN calorie * tb_data_food_detail.forpeople End), 0) as D14,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=15  THEN calorie * tb_data_food_detail.forpeople End), 0) as D15,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=16  THEN calorie * tb_data_food_detail.forpeople End), 0) as D16,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=17  THEN calorie * tb_data_food_detail.forpeople End), 0) as D17,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=18  THEN calorie * tb_data_food_detail.forpeople End), 0) as D18,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=19  THEN calorie * tb_data_food_detail.forpeople End), 0) as D19,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=20  THEN calorie * tb_data_food_detail.forpeople End), 0) as D20,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=21  THEN calorie * tb_data_food_detail.forpeople End), 0) as D21,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=22  THEN calorie * tb_data_food_detail.forpeople End), 0) as D22,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=23  THEN calorie * tb_data_food_detail.forpeople End), 0) as D23,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=24  THEN calorie * tb_data_food_detail.forpeople End), 0) as D24,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=25  THEN calorie * tb_data_food_detail.forpeople End), 0) as D25,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=26  THEN calorie * tb_data_food_detail.forpeople End), 0) as D26,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=27  THEN calorie * tb_data_food_detail.forpeople End), 0) as D27,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=28  THEN calorie * tb_data_food_detail.forpeople End), 0) as D28,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=29  THEN calorie * tb_data_food_detail.forpeople End), 0) as D29,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=30  THEN calorie * tb_data_food_detail.forpeople End), 0) as D30,  "];
    [sql appendString:@" ifnull(SUM(CASE  WHEN cast(strftime('%d', tb_data_food_detail.regdate) as integer)=31  THEN calorie * tb_data_food_detail.forpeople End), 0) as D31  "];
    [sql appendString:@" From tb_data_food_detail "];
    [sql appendString:@" LEFT JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:@" WHERE cast(strftime('%Y',tb_data_food_detail.regdate) as integer)" ];
    [sql appendString:[NSString stringWithFormat:@"=%d and cast(strftime('$',tb_data_food_detail.regdate) as integer)=%d", [nYear intValue], [nMonth intValue]]];
    NSString *sql2 = [NSString stringWithString:sql];
    sql2 = [sql2 stringByReplacingOccurrencesOfString:@"$" withString:@"%m"];
    const char *sql3 = [sql2 UTF8String];
    if (sqlite3_prepare_v2(database, sql3, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getMonthResult '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        NSString *d1 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
        NSString *d2 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)];
        NSString *d3 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)];
        NSString *d4 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)];
        NSString *d5 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 4)];
        NSString *d6 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 5)];
        NSString *d7 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 6)];
        NSString *d8 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 7)];
        NSString *d9 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 8)];
        NSString *d10 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 9)];
        NSString *d11 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 10)];
        NSString *d12 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 11)];
        NSString *d13 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 12)];
        NSString *d14 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 13)];
        NSString *d15 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 14)];
        NSString *d16 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 15)];
        NSString *d17 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 16)];
        NSString *d18 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 17)];
        NSString *d19 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 18)];
        NSString *d20 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 19)];
        NSString *d21 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 20)];
        NSString *d22 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 21)];
        NSString *d23 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 22)];
        NSString *d24 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 23)];
        NSString *d25 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 24)];
        NSString *d26 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 25)];
        NSString *d27 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 26)];
        NSString *d28 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 27)];
        NSString *d29 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 28)];
        NSString *d30 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 29)];
        NSString *d31 = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 30)];
        
        
        arrRtn2 = @[@([d1 intValue]), @([d2 intValue]), @([d3 intValue]), @([d4 intValue]), @([d5 intValue]), @([d6 intValue]),
                    @([d7 intValue]), @([d8 intValue]), @([d9 intValue]), @([d10 intValue]), @([d11 intValue]), @([d12 intValue]),
                    @([d13 intValue]), @([d14 intValue]), @([d15 intValue]), @([d16 intValue]), @([d17 intValue]), @([d18 intValue]),
                    @([d19 intValue]), @([d20 intValue]), @([d21 intValue]), @([d22 intValue]), @([d23 intValue]), @([d24 intValue]),
                    @([d25 intValue]), @([d26 intValue]), @([d27 intValue]), @([d28 intValue]), @([d29 intValue]), @([d30 intValue]), @([d31 intValue])];
        
    }
    
    
    return arrRtn2;
}



/*
 // 방사형그래프
 */
- (NSArray*) getRadial:(NSString*)sDate eDate:(NSString*)eDate totTakeCal:(float)totTakeCal recomCal:(float)recomCal dayCnt:(int)dayCnt
{
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    
    int nowYaer = [[Utils getNowDate:@"yyyy"] intValue];
//    int rBirth = [[NSString stringWithFormat:@"19%@",[APPSESSION_SHARED.birthday substringFromIndex:2]] intValue];
    int rAge = nowYaer - 1999;
 
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.protein as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as protein, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.fat as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as fat, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.carbohydrate as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as carbohydrate, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.salt as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as salt,  "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as calorie "];
    [sql appendString:@"FROM tb_data_food_detail "];
    [sql appendString:@"INNER JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@"WHERE tb_data_food_detail.regdate between '%@ 00:00' and '%@ 23:59' ", sDate, eDate]];
   
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getRadial '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    
    NSString *nowDate               = [Utils getNowDate:@"yyyy"];   // 현재년도
    NSString *mber_brthdy           = @"1991"; //APPSESSION_SHARED.mber_brthdy; // 엄마 생일
    
    NSString *birthYear = [NSString stringWithFormat:@"19%@", [mber_brthdy substringToIndex:2]];
    int fAge = ([[nowDate substringWithRange:NSMakeRange(0, 4)] intValue] - [birthYear intValue]) -1;  //만나이
    
    int isPregnancy =  1; // [[AppModal sharedApp] iPregnancyValue];   // 1초기, 2중기, 3후기, 55수유기, 99비임신기
    
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        float protein       = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)] floatValue];    // 단백질
        float fat           = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)] floatValue];    // 지방
        float carbohydrate  = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)] floatValue];    // 탄수화물
        float salt          = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)] floatValue];    // 나트륨
        
        // 먹은 음식이 없다면, 4가지가 모두 0인 음식은 없음.
        if (protein==0
            && fat==0
            && carbohydrate==0
            && salt==0) {
            break;
        }
        
        
        float gCalorie = 0.0f;           //그래프 칼로리수치
        float gCarbohydrate = 0.0f;      //그래프 탄수화물수치
        float gProtein = 0.0f;           //그래프 단백질수치
        float gFat     = 0.0f;           //그래프 지방 수치
        float gSalt    = 0.0f;           //그래프 나트륨수치
        
        // + --------------------------
        //칼로리
        // + --------------------------
        float tmpCal= (totTakeCal / recomCal) * 100;
        if (tmpCal >= 110){
            gCalorie = 100.0f; // 과잉
        }else if (tmpCal > 90 && tmpCal < 110){
            gCalorie = 66.6f;
        }else if (tmpCal <= 90) {
            gCalorie = 33.3f;
        }
        
        // + --------------------------
        //단백질 (나이별, 임신주기별)
        // + --------------------------
        if (fAge >=19 && fAge <=29) {
            if (isPregnancy==1) {
                if(protein < 45 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 45 * dayCnt && protein <= 55 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 55 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==2) {
                if(protein < 57 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 57 * dayCnt && protein <= 70 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 70 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==3) {
                if(protein < 70 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 70 * dayCnt && protein <= 85 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 85 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==55) {
                if(protein < 65 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 65 * dayCnt && protein <= 80 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 80 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==99) {
                if(protein < 45 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 45 * dayCnt && protein <= 55 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 55 * dayCnt){
                    gProtein = 100.0f;
                }
            }
        }else if (fAge >=30 && fAge <=49) {
            if (isPregnancy==1) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 50 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 50 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==2) {
                if(protein < 52 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 52 * dayCnt && protein <= 65 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 65 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==3) {
                if(protein < 65 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 65 * dayCnt && protein <= 80 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 80 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==55) {
                if(protein < 60 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 60 * dayCnt && protein <= 75 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 75 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==99) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 50 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 50 * dayCnt){
                    gProtein = 100.0f;
                }
            }
        }else if (fAge >=50 && fAge <=64) {
            if (isPregnancy==1) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 50 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 50 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==2) {
                if(protein < 52 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 52 * dayCnt && protein <= 65 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 65 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==3) {
                if(protein < 65 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 65 * dayCnt && protein <= 80 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 80 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==55) {
                if(protein < 60 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 60 * dayCnt && protein <= 75 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 75 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==99) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 50 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 50 * dayCnt ){
                    gProtein = 100.0f;
                }
            }
        }else if (fAge >=65 && fAge <=74) {
            if (isPregnancy==1) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 45 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 45 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==2) {
                if(protein < 52 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 52 * dayCnt && protein <= 60 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 60 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==3) {
                if(protein < 65 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 65 * dayCnt && protein <= 75 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 75 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==55) {
                if(protein < 60 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 60 * dayCnt && protein <= 70 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 70 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==99) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 45 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 45 * dayCnt){
                    gProtein = 100.0f;
                }
            }
        }else if (fAge >=75) {
            if (isPregnancy==1) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 45 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 45 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==2) {
                if(protein < 52 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 52 * dayCnt && protein <= 60 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 60 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==3) {
                if(protein < 65 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 65 * dayCnt && protein <= 75 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 75 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==55) {
                if(protein < 60 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 60 * dayCnt && protein <= 70 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 70 * dayCnt){
                    gProtein = 100.0f;
                }
            }else if(isPregnancy==99) {
                if(protein < 40 * dayCnt){
                    gProtein = 33.6f;
                }else if(protein >= 40 * dayCnt && protein <= 45 * dayCnt){
                    gProtein = 66.6f;
                }else if(protein > 45 * dayCnt){
                    gProtein = 100.0f;
                }
            }
        }
        
        // + --------------------------
        // 탄수화물
        // + --------------------------
        float tmpCarbohydrate = ((carbohydrate * 4)/totTakeCal) * 100;
        if(totTakeCal == 0.0f){
            gCarbohydrate = 33.3f;
        }
        else if (tmpCarbohydrate > 65){
            gCarbohydrate = 100.0f; // 과잉
        }else if (tmpCarbohydrate >= 55 && tmpCarbohydrate <=65){
            gCarbohydrate = 66.6f;
        }else if (tmpCarbohydrate < 55) {
            gCarbohydrate = 33.3f;
        }
        
        // + --------------------------
        // 지방
        // + --------------------------
        float tmpFat = ((fat * 9)/totTakeCal) * 100;
        if(totTakeCal == 0.0f){
            gFat = 33.3f;
        }
        else if (tmpFat > 30){
            gFat = 100.0f; // 과잉
        }else if (tmpFat >= 15 && tmpFat <=30){
            gFat = 66.6f;
        }else if (tmpFat < 15){
            gFat = 33.3f;
        }
        
        // + --------------------------
        // 나트륨
        // + --------------------------
        
        int tmpSalt = 1500 * dayCnt;  //충분섭취량
        /*if (sex == 2){   //여성
            if(fAge >= 19 && fAge <=29){
                tmpSalt = 1500;
            }else if(fAge >= 30 &&fAge <=49){
                tmpSalt = 1500;
            }else if(fAge >= 50 && fAge <=64){
                tmpSalt = 1500;
            }else if(fAge >= 65 && fAge <=74){
                tmpSalt = 1300;
            }else if(fAge >= 75) {
                tmpSalt = 1100;
            }
        }else{
            if(fAge >= 19 && fAge <=29){
                tmpSalt = 1500;
            }else if(fAge >= 30 && fAge <=49){
                tmpSalt = 1500;
            }else if(fAge >= 50 && fAge <=64){
                tmpSalt = 1500;
            }else if(fAge >= 65 && fAge <=74){
                tmpSalt = 1300;
            }else if(fAge >= 75) {
                tmpSalt = 1100;
            }
        }*/
        if (salt >= 2000 * dayCnt){
            gSalt = 100.0f; // 과잉
        }else if (salt >= tmpSalt && salt < 2000 * dayCnt){
            gSalt = 66.6f;
        }else if(salt < tmpSalt){
            gSalt = 33.3f;
        }
        
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%.2f", gCalorie], @"gCalorie",   // 열량
                                [NSString stringWithFormat:@"%.2f", gProtein], @"gProtein",   // 단백질
                                [NSString stringWithFormat:@"%.2f", gSalt], @"gSalt",         // 나트룸
                                [NSString stringWithFormat:@"%.2f", gFat], @"gFat",           // 지방
                                [NSString stringWithFormat:@"%.2f", gCarbohydrate], @"gCarbohydrate", nil];  // 탄수화물
        
        [arrRtn addObject:dicTemp];
    }
    
    return arrRtn;
}

/*
 // 방사형그래프
 */
- (NSArray*) getRadial:(NSString*)sDate eDate:(NSString*)eDate totTakeCal:(float)totTakeCal recomCal:(float)recomCal
{
    sqlite3_stmt *statement;
    NSMutableArray *arrRtn = [[NSMutableArray alloc] init];
    
    Model *_model = [Model instance];
    Tr_login *login = [_model getLoginData];
    
    int sex = [login.mber_sex intValue];
    int nowYaer = [[Utils getNowDate:@"yyyy"] intValue];
    int rBirth = [[login.mber_lifyea substringFromIndex:4] intValue];
    int rAge = nowYaer - rBirth;
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.protein as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as protein, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.fat as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as fat, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.carbohydrate as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as carbohydrate, "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.salt as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as salt,  "];
    [sql appendString:@"ifnull(SUM(CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as calorie "];
    [sql appendString:@"FROM tb_data_food_detail "];
    [sql appendString:@"INNER JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode = tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@"WHERE tb_data_food_detail.regdate between '%@ 00:00' and '%@ 23:59' ", sDate, eDate]];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getRadial '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    
    
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        float protein       = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)] floatValue];    // 단백질
        float fat           = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 1)] floatValue];    // 지방
        float carbohydrate  = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 2)] floatValue];    // 탄수화물
        float salt          = [[NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 3)] floatValue];    // 나트륨
        
        
        float gCalorie = 0.0f;           //그래프 칼로리수치
        float gCarbohydrate = 0.0f;      //그래프 탄수화물수치
        float gProtein = 0.0f;           //그래프 단백질수치
        float gFat     = 0.0f;           //그래프 지방 수치
        float gSalt    = 0.0f;           //그래프 나트륨수치
        
        // + --------------------------
        //칼로리
        // + --------------------------
        float tmpCal= (totTakeCal / recomCal) * 100;
        if (tmpCal > 110){
            gCalorie = 100.0f; // 과잉
        }else if (tmpCal >= 90 && tmpCal <=110){
            gCalorie = 66.6f;
        }else if (tmpCal < 90) {
            gCalorie = 33.3f;
        }
        
        // + --------------------------
        //단백질
        // + --------------------------
        float tmpProtein = ((protein * 4)/recomCal) * 100;
        if (tmpProtein > 20){
            gProtein = 100.0f; // 과잉
        }else if (tmpProtein >= 7 && tmpProtein <=20){
            gProtein = 66.6f;
        }else if (tmpProtein < 7) {
            gProtein = 33.3f;
        }
        
        // + --------------------------
        // 탄수화물
        // + --------------------------
        float tmpCarbohydrate = ((carbohydrate * 4)/recomCal) * 100;
        if (tmpCarbohydrate > 65){
            gCarbohydrate = 100.0f; // 과잉
        }else if (tmpCarbohydrate >= 55 && tmpCarbohydrate <=65){
            gCarbohydrate = 66.6f;
        }else if (tmpCarbohydrate < 55) {
            gCarbohydrate = 33.3f;
        }
        
        // + --------------------------
        // 지방
        // + --------------------------
        float tmpFat = ((fat * 9)/recomCal) * 100;
        if (tmpFat > 30){
            gFat = 100.0f; // 과잉
        }else if (tmpFat >= 15 && tmpFat <=30){
            gFat = 66.6f;
        }else if (tmpFat < 15){
            gFat = 33.3f;
        }
        
        // + --------------------------
        // 나트륨
        // + --------------------------
        int rBirth = [[login.mber_lifyea substringToIndex:4] intValue];        // 출생년도
        int nowYear = [[Utils getNowDate:@"yyyy"] intValue];                   // 현재년도
        int nAge = (nowYear - rBirth + 1);                                     // 나이
        
        int tmpSalt = 0;  //충분섭취량
        if (sex == 2){   //여성
            if(nAge >= 19 && nAge <=29){
                tmpSalt = 1500;
            }else if(nAge >= 30 && nAge <=49){
                tmpSalt = 1500;
            }else if(nAge >= 50 && nAge <=64){
                tmpSalt = 1500;
            }else if(nAge >= 65 && nAge <=74){
                tmpSalt = 1300;
            }else if(nAge >= 75) {
                tmpSalt = 1100;
            }
        }else{
            if(nAge >= 19 && nAge <=29){
                tmpSalt = 1500;
            }else if(nAge >= 30 && nAge <=49){
                tmpSalt = 1500;
            }else if(nAge >= 50 && nAge <=64){
                tmpSalt = 1500;
            }else if(nAge >= 65 && nAge <=74){
                tmpSalt = 1300;
            }else if(nAge >= 75) {
                tmpSalt = 1100;
            }
        }
        if (salt >= 2000){
            gSalt = 100.0f; // 과잉
        }else if (salt >= tmpSalt && salt <= 2000){
            gSalt = 66.6f;
        }else if(salt < tmpSalt){
            gSalt = 33.6f;
        }
        
        
        NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%f", gCalorie], @"gCalorie",
                                [NSString stringWithFormat:@"%f", gProtein], @"gProtein",
                                [NSString stringWithFormat:@"%f", gSalt], @"gSalt",
                                [NSString stringWithFormat:@"%f", gFat], @"gFat",
                                [NSString stringWithFormat:@"%f", gCarbohydrate], @"gCarbohydrate", nil];
        
        [arrRtn addObject:dicTemp];
    }
    
    return arrRtn;
}

- (NSString*) getResultMain:(NSString*)sDate eDate:(NSString*)eDate {
    
    sqlite3_stmt *statement;
    
    NSMutableString *sql = [[NSMutableString alloc] init];
    [sql setString:@"SELECT "];
    [sql appendString:@" IFNULL(SUM(CAST(tb_data_food_calorie.calorie as FLOAT) * CAST(tb_data_food_detail.forpeople as FLOAT)),0) as calorie "];
    [sql appendString:@" FROM tb_data_food_main "];
    [sql appendString:@" INNER JOIN tb_data_food_detail ON  tb_data_food_main.idx=tb_data_food_detail.idx "];
    [sql appendString:@" INNER JOIN tb_data_food_calorie ON tb_data_food_detail.foodcode=tb_data_food_calorie.code "];
    [sql appendString:[NSString stringWithFormat:@" WHERE tb_data_food_main.regdate BETWEEN '%@ 00:00' and '%@ 23:59' ", sDate, eDate]];
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"Error: failed getResultMain '%s'.", sqlite3_errmsg(database));
        return nil;
    }
    
    NSString *calorie = @"0";
    
    while (sqlite3_step(statement)==SQLITE_ROW) {
        
        calorie       = [NSString stringWithFormat:@"%s", (const char*)sqlite3_column_text(statement, 0)];
    }
    
    return calorie;
}

@end
