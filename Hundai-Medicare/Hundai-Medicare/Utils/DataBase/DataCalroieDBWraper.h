//
//  DataCalroieDBWraper.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicareDataBase.h"
#import "Utils.h"
#import "TimeUtils.h"

@interface DataCalroieDBWraper : MedicareDataBase

- (void)DeleteDb:(NSString* )idx;
- (void)insert:(NSDictionary *)data;
- (NSArray *)getResultDay:(NSString *)nDate;
- (NSArray *)getResultWeek:(NSString *)sDate eDate:(NSString *)eDate;
- (NSArray *)getResultMonth:(NSString *)nYear nMonth:(NSString *)nMonth;

@end
