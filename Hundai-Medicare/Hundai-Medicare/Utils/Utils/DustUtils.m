//
//  DustUtils.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 7..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "DustUtils.h"

@implementation DustUtils

-(id)init{
    self = [super init];
    _skytext = @"";
    _feelslike = @"";
    
    _MapKey = [[NSDictionary alloc] initWithObjectsAndKeys:
            @"서울시"       ,@"1",
            @"부산광역시"    ,@"2",
            @"대구광역시"    ,@"3",
            @"인천광역시"    ,@"4",
            @"광주광역시"    ,@"5",
            @"대전광역시"    ,@"6",
            @"울산광역시"    ,@"7",
            @"경기도"       ,@"8",
            @"강원도"       ,@"9",
            @"충청북도"      ,@"10",
            @"충청남도"      ,@"11",
            @"전라북도"      ,@"12",
            @"전라남도"      ,@"13",
            @"경상북도"      ,@"14",
            @"경상남도"      ,@"15",
            @"제주특별자치도"  ,@"16",
            nil];
    
    return self;
}

-(NSString *)getCityName:(NSString *)key{
    NSString *city = @"";
    
    city = [_MapKey objectForKey:key];
    
    return city;
}

-(NSString *)getDustCode:(NSString *)name{
    NSString *code = @"";
    
    if([name isEqualToString:@"서울"]){
       code = @"1";
    }else if([name isEqualToString:@"부산"]){
       code = @"2";
    }else if([name isEqualToString:@"대구"]){
        code = @"3";
    }else if([name isEqualToString:@"인천"]){
        code = @"4";
    }else if([name isEqualToString:@"광주"]){
        code = @"5";
    }else if([name isEqualToString:@"대전"]){
        code = @"6";
    }else if([name isEqualToString:@"울산"]){
        code = @"7";
    }else if([name isEqualToString:@"경기"]){
        code = @"8";
    }else if([name isEqualToString:@"강원"]){
        code = @"9";
    }else if([name isEqualToString:@"충청북"] || [name isEqualToString:@"충북"]){
        code = @"10";
    }else if([name isEqualToString:@"충청남"] || [name isEqualToString:@"충남"]){
        code = @"11";
    }else if([name isEqualToString:@"전라북"] || [name isEqualToString:@"전북"]){
        code = @"12";
    }else if([name isEqualToString:@"전라남"] || [name isEqualToString:@"전남"]){
        code = @"13";
    }else if([name isEqualToString:@"경상북"] || [name isEqualToString:@"경북"]){
        code = @"14";
    }else if([name isEqualToString:@"경상남"] || [name isEqualToString:@"경남"]){
        code = @"15";
    }else if([name isEqualToString:@"제주"]){
        code = @"16";
    }
    return code;
}

-(NSString *)getDustQyStatus:(float)value {
    NSString *status = @"";
    
    if(0 <= value && value < 31){
        status = @"좋음";
    }else if(32 <= value && value < 81){
        status = @"보통";
    }else if(82 <= value && value < 151){
        status = @"나쁨";
    }else{
        status = @"매우나쁨";
    }
    
    return status;
}

//날씨 테스트
- (void)getWeather:(NSString *)cityname {
    
    
    NSString *url = [NSString stringWithFormat:@"http://weather.service.msn.com/data.aspx?weadegreetype=C&culture=ko-KR&weasearchstr=%@&src=outlook", cityname];
    NSString* encodedUrl = [url stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:encodedUrl]];
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %ld", url, [responseCode statusCode]);
    }
    NSLog(@"oResponseData = %@", oResponseData);
    
    NSString *receivedDataString = [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",receivedDataString);
    
    NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:oResponseData];
    [myParser setDelegate:self];
    [myParser setShouldResolveExternalEntities: YES];
    [myParser parse];
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if([elementName isEqualToString:@"current"]) {
        _skytext = [attributeDict objectForKey:@"skytext"];
        _feelslike = [attributeDict objectForKey:@"feelslike"];
    }
}

-(NSString *)getSkytext {
    return self.skytext;
}

-(NSString *)getFeelslike {
    return self.feelslike;
}

@end
