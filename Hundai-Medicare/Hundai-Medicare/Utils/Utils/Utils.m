//
//  Utils.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "Utils.h"

@implementation Utils

#pragma mark - UserDefault data get/set

+ (NSString*)getUserDefault:(NSString *)key {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    }
    return @"";
}

+ (void)setUserDefault:(NSString*)key value:(NSString*)value {
    if (key != nil && value != nil) {
        [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)setUserDefault:(NSString*)key bools:(BOOL)value {
    if (key != nil) {
        [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)removeUserDefault:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (UIColor *)uintColor:(uint)color {
    return [UIColor colorWithRed:(CGFloat)((color & 0xFF0000) >> 16) / 255.0 green:(CGFloat)((color & 0x00FF00) >> 8) / 255.0 blue:(color & 0x0000FF) / 255.0 alpha:1.0];
}


+ (void)playSound :(NSString *)fileName :(NSString *) ext {
    SystemSoundID audioEffect;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:ext];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath: path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else {
        NSLog(@"error, file not found: %@", path);
    }
}

+ (NSString *)decimalNumber:(int)number {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *result = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSNumber numberWithInt:number]]];
    
    return result;
}

+ (void)imageSequenceAnimation:(NSString *)name withLength:(int)length withImageView:(UIImageView *)imageView {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingPosition:NSNumberFormatterPadBeforePrefix];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:4];
    
    NSMutableArray *images = [NSMutableArray new];
    
    for (int i = 0; i < length; i++) {
        NSNumber *number = [NSNumber numberWithInt:(i + 1)];
        NSString *imageName = [numberFormatter stringFromNumber:number];
        
        UIImage *sequence = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", name, imageName]];
        [images addObject:sequence];
    }
    
    imageView.animationImages = images;
    imageView.animationRepeatCount = 0;
    
    [imageView startAnimating];
}

+ (NSString*)dateToString:(NSDate *)date format:(NSString*)format {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    return dateString;
}

+ (NSString*)dateStringToString:(NSString *)dateStr {
    
    if ([dateStr isEqualToString:@""]
        || [dateStr isEqualToString:@"0000-00-00"]
        || [dateStr isEqualToString:@"(null)"]
        || dateStr == nil
        || dateStr == (id)[NSNull null]) {
        return @"";
    }
    
    NSString *fmt = @"yyyy-MM-dd";
    NSDateFormatter *oldDateFormat = [[NSDateFormatter alloc] init];
    [oldDateFormat setDateFormat:fmt];
    [oldDateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [oldDateFormat dateFromString:dateStr];
    
    return [Utils dateToString:date format:@"yyyy.MM.dd"];
}

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg completion:(void (^)(void))completion {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completion();
    }];
    [alert addAction:okAction];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg completion:(void (^)(void))completion fail:(void (^)(void))fail {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completion();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (fail) {
            fail();
        }
    }];
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (BOOL)emailIsValid:(NSString *)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)NSStringIsValidPass:(NSString *)checkString {
    NSString *stricterFilterString = @"^(?=.*\\d)(?=.*[~`!@#$%\\?\\^&*()-])(?=.*[a-zA-Z0-9]).{8,15}$";
    NSPredicate *passTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passTest evaluateWithObject:checkString];
}

+ (void)callTelephone:(id)sender withPhone:(NSString *)phone
              message:(NSString*)msg svcCode:(NSString*)svcCode delegate:(id)delegate {
    
    if ([Utils stringNullCheck:phone]) {
        
        NSString* message = @"통화 하시겠습니까?";
        NSString *ok = @"통화";
        NSString *no = @"취소";
        if (msg != nil) {
            message = [NSString stringWithFormat:@"%@", msg];
            ok = @"예";
            no = @"아니오";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:no style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            // 오늘날짜
//            NSDateFormatter *format = [[NSDateFormatter alloc] init];
//            [format setDateFormat:@"yyyyMMdd"];
//            NSString* nDate = [format stringFromDate:[NSDate date]];
            
            // 어떠한 서비스로 인증했는지 저장(인증코드, 날짜 저장)
//            [Utils setExpeServiceExpireCode:svcCode nDate:nDate];
            
            [delegate callbackMessage];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phone]] options:@{} completionHandler:nil];
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [sender presentViewController:alert animated:YES completion:nil];
    }
}

+ (void)callTelephone:(id)sender withPhone:(NSString *)phone message:(NSString*)msg {
    if ([Utils stringNullCheck:phone]) {
        
        NSString *message = @"통화 하시겠습니까?";
        NSString *ok = @"전화하기";
        NSString *no = @"취소";
        if (msg != nil) {
            message = [NSString stringWithFormat:@"%@", msg];
            ok = @"전화하기";
            no = @"취소";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:no style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phone]] options:@{} completionHandler:nil];
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [sender presentViewController:alert animated:YES completion:nil];
    }
}

+ (NSString*)stringEmptyReturn:(NSString *)str {
    if (str != nil && str != (id)[NSNull null] && [str isEqualToString:@"<null>"]) {
        return @"";
    }
    
    return str;
}

+ (BOOL)stringNullCheck:(NSString *)str {
    if (str != nil && str != (id)[NSNull null] && ![str isEqualToString:@""]) {
        return YES;
    }
    
    return NO;
}

+ (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}


+ (CGFloat)getTextViewHeight:(UITextView *)textview {
    CGFloat fixedWidth = textview.frame.size.width;
    CGSize newSize = [textview sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textview.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textview.frame = newFrame;
    
    return textview.frame.size.height;
}

// string to float
+ (float)getFloatVal:(NSString *)str {
    
    if(str.length < 1 || str == NULL || str == (id)[NSNull null]){
        str = @"0";
    }
    
    float returnFloat = [[str stringByReplacingOccurrencesOfString:@"[^0-9 \\.]"
                                                        withString:@""
                                                           options:NSRegularExpressionSearch
                                                             range:NSMakeRange(0, [str length])] floatValue];
    
    return returnFloat;
}


// string to nsinteger
+ (NSInteger)getNsIntVal:(NSString *)str {
    
    if(str.length < 1 || str == NULL || str == (id)[NSNull null]){
        str = @"0";
    }
    
    NSInteger returnVal = [[str stringByReplacingOccurrencesOfString:@"[^0-9]"
                                                          withString:@""
                                                             options:NSRegularExpressionSearch
                                                               range:NSMakeRange(0, [str length])] integerValue];
    
    return returnVal;
}

// 현재 시간.
+ (NSString *)getNowDate:(NSString *)format {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSString *nowDate = [dateFormatter stringFromDate:[NSDate date]];
    
    return nowDate;
}

// time format
+ (NSString *)getTimeFormat:(NSString *)time beTime:(NSString *)beTime afTime:(NSString *)afTime {
    NSString *result = @"";
    
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    [dtF setDateFormat:beTime];
    NSDate *d = [dtF dateFromString:time];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:afTime];
    result = [dateFormat stringFromDate:d];
    
    return result;
}

// 오전 오후.
+ (NSString *)getAmPmHour:(NSString *)hourStr {
    
    if(hourStr.length > 2){
        hourStr = [hourStr substringWithRange:NSMakeRange(0, 2)];
    }
    
    int hourInt = [hourStr intValue];
    
    if(hourInt < 11){
        return @"오전";
    }else{
        return @"오후";
    }
}

// 요일
+ (NSString *)getMonthDay:(NSString *)time {
    
    NSDateFormatter* strFormatter = [[NSDateFormatter alloc] init];
    [strFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *sourceDate = [strFormatter dateFromString:time];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    
    // 요일 관련 (일자를 가지고 요일 구하기)
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear|
                              NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                                         fromDate:sourceDate];
    NSInteger weekday = [comp weekday];
    NSString *strWeek = [NSString stringWithFormat:@"%@",
                         [[formatter weekdaySymbols] objectAtIndex:weekday-1]];
    
    
    return strWeek;
}

+ (NSString *)getCurrDateAdd:(NSString *)nDate {
    
    NSDate *today = [Utils getDateFromString:nDate];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"] ];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMinute:10];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    NSString *dateString = [dateFormat stringFromDate:newDate];
    
    return dateString;
}

+ (NSDate *)getDateFromString:(NSString *)string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *date = [dateFormatter dateFromString:string];
    return date;
}

+ (int)convertCharToUnicode:(char)c {
    return (int) c;
}

+ (char)convertUnicodeToChar:(NSString*) hexUnicode {
    return (char) [NSString stringWithFormat:@"%@", hexUnicode];
}

+ (NSString *)makeQuery:(NSString*)strSearch {
    NSArray *CHO_LIST = [[NSArray alloc] initWithObjects:@"ㄱ", @"ㄲ", @"ㄴ", @"ㄷ", @"ㄸ", @"ㄹ", @"ㅁ", @"ㅂ", @"ㅃ", @"ㅅ", @"ㅆ", @"ㅇ", @"ㅈ", @"ㅉ", @"ㅊ", @"ㅋ", @"ㅌ", @"ㅍ", @"ㅎ", nil];
    NSArray *CHO_SEARCH_LIST = [[NSArray alloc] initWithObjects: @true, @false, @true, @true, @false, @true, @true, @true, @false, @true, @false, @true, @true, @false, @true, @true, @true, @true, @true, nil ];
    
    static  int HANGUL_BEGIN_UNICODE = 0xAC00; // 가
    static  int HANGUL_END_UNICODE = 0xD7A3; // ?
    static  int HANGUL_CHO_UNIT = 588; //한글 초성글자간 간격
    static  int HANGUL_JUNG_UNIT = 28; //한글 중성글자간 간격
    
    
    strSearch = strSearch == nil ? @"" : [strSearch stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableString *retQuery = [[NSMutableString alloc] init];
    
    int nChoPosition;
    int nNextChoPosition;
    int StartUnicode;
    int EndUnicode;
    NSString * newString;
    
    int nQueryIndex = 0;
    NSMutableString *query =  [[NSMutableString alloc] init];
    for( int nIndex = 0 ; nIndex < [strSearch length]; nIndex++ ){
        nChoPosition = -1;
        nNextChoPosition = -1;
        StartUnicode = -1;
        EndUnicode = -1;
        
        newString = [strSearch substringWithRange:NSMakeRange(nIndex, 1)];
        if([newString isEqualToString:@"'"])
            continue;
        
        if( nQueryIndex != 0 ){
            [query appendString:@" AND "];
        }
        
        for( int nChoIndex = 0 ; nChoIndex < [CHO_LIST count]; nChoIndex++ ){
            if([newString isEqualToString:[CHO_LIST objectAtIndex:nChoIndex]]){
                nChoPosition = nChoIndex;
                nNextChoPosition = nChoPosition+1;
                for( ; nNextChoPosition < [CHO_SEARCH_LIST count] ; nNextChoPosition++ ){
                    if( [CHO_SEARCH_LIST objectAtIndex:nNextChoPosition])
                        break;
                }
                break;
            }
        }
        
        if( nChoPosition >= 0 ){ //초성이 있을 경우
            StartUnicode = HANGUL_BEGIN_UNICODE + nChoPosition*HANGUL_CHO_UNIT;
            EndUnicode = HANGUL_BEGIN_UNICODE + nNextChoPosition*HANGUL_CHO_UNIT;
        }
        else{
            int Unicode = [self convertCharToUnicode:[newString UTF8String]];
            if( Unicode >= HANGUL_BEGIN_UNICODE && Unicode <= HANGUL_END_UNICODE){
                int Jong = ((Unicode-HANGUL_BEGIN_UNICODE)%HANGUL_CHO_UNIT)%HANGUL_JUNG_UNIT;
                
                if( Jong == 0 ){// 초성+중성으로 되어 있는 경우
                    StartUnicode = Unicode;
                    EndUnicode = Unicode + HANGUL_JUNG_UNIT;
                }
                else{
                    StartUnicode = Unicode;
                    EndUnicode = Unicode;
                }
            }
        }
        
        if( StartUnicode > 0 && EndUnicode > 0 ){
            if( StartUnicode == EndUnicode )
                [query appendString:[NSString stringWithFormat:@"substr(name, %d,1)='%@'",(nIndex+1) ,newString ]];
            else
                [query appendString:[NSString stringWithFormat:@"(substr(name, %d,1)>='%c' AND substr(name,%d,1)<'%c')",
                                     (nIndex+1), [self convertUnicodeToChar:[NSString stringWithFormat:@"%d", StartUnicode]],
                                     (nIndex+1), [self convertUnicodeToChar:[NSString stringWithFormat:@"%d", EndUnicode]]]];
            
        }
        else{
            BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[newString characterAtIndex:0]];
            
            if(!isUppercase){ //영문 소문자
                [query appendString:[NSString stringWithFormat:@"(substr(name,%d,1)='%@' OR substr(name, %d, 1)='%@')", (nIndex+1), newString, (nIndex+1), [newString uppercaseString]]];
            }
            else if(isUppercase){ //영문 대문자
                [query appendString:[NSString stringWithFormat:@"(substr(name,%d,1)='%@' OR substr(name, %d, 1)='%@')", (nIndex+1), newString, (nIndex+1), [newString lowercaseString]]];
            }
            else //기타 문자
                [query appendString:[NSString stringWithFormat:@"substr(name, %d,1)='%@'", (nIndex+1), newString]];
        }
        
        nQueryIndex++;
    }
    
    if([query length] > 0 && strSearch != nil && [strSearch length] > 0) {
        
        [retQuery appendString:[NSString stringWithFormat:@"(%@)", query]];
        
        if( [newString rangeOfString:@" "].location != NSNotFound) {
            // 공백 구분 단어에 대해 단어 모두 포함 검색
            NSArray *tokens = [newString componentsSeparatedByString:@" "];
            [retQuery appendString:@" OR ("];
            for(int i=0, isize=[tokens count]; i<isize; i++) {
                NSString *token = [tokens objectAtIndex:i];
                if(i != 0) {
                    [retQuery appendString:@" AND "];
                }
                [retQuery appendString:@"name like '%"];
                [retQuery appendString:[NSString stringWithFormat:@"%@", token]];
                [retQuery appendString:@"%'"];
            }
            [retQuery appendString:@")"];
        } else {
            // LIKE 검색 추가
            [retQuery appendString:@" OR name like '%"];
            [retQuery appendString:[NSString stringWithFormat:@"%@", newString]];
            [retQuery appendString:@"%'"];
        }
    } else {
        [retQuery appendString:[NSString stringWithFormat:@"%@", query]];
    }
    return retQuery;
}

+ (BOOL)string:(NSString *)compareString containsChoseong:(NSString *)choseongString {
    NSArray *chosungArray = [[NSArray alloc]initWithObjects:@"ㄱ",@"ㄲ",@"ㄴ",@"ㄷ",@"ㄸ",@"ㄹ",@"ㅁ",@"ㅂ",@"ㅃ",@"ㅅ",@"ㅆ",@"ㅇ",@"ㅈ",@"ㅉ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ", nil];
    NSString *chosung = @"";
    
    for (int i=0; i<[compareString length];i++){
        NSInteger code = [compareString characterAtIndex:i];
        if(code >= 44032 && code <= 55203) {
            NSInteger uniCode = code - 44032;
            NSInteger chosungIndex = uniCode / 21 / 28;
            chosung = [NSString stringWithFormat:@"%@%@",chosung,[chosungArray objectAtIndex:chosungIndex]];
        }
        else {
            chosung = [NSString stringWithFormat:@"%@%@",chosung,[compareString substringWithRange:NSMakeRange(i,1)]];
        }
    }
    
    if ([chosung rangeOfString:choseongString].location != NSNotFound){
        return YES;
    }
    else {
        return NO;
    }
}

+ (NSString *)getZeroAppend:(int)value {
    
    NSString *retunValue = @"";
    if (value==0) {
        return @"00";
    }else if(value < 10){
        retunValue = [NSString stringWithFormat:@"0%d", value];
    }else{
        retunValue = [NSString stringWithFormat:@"%d", value];
    }
    return retunValue;
}

+ (NSString *)getNoneZeroString:(float)value {
    NSString *result = [NSString stringWithFormat:@"%.2f", value];
    
    NSRange subRange = [result rangeOfString:@".00"];
    if(subRange.location != NSNotFound){
        result = [result stringByReplacingOccurrencesOfString:@".00" withString:@""];
    }
    return result;
}

+ (NSString *)getConvertServertDate:(NSString*)nDate {
    
    nDate = [nDate stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    return nDate;
}

+ (NSString *)getConvertServertTime:(NSString*)nTime {
    
    if( [nTime rangeOfString:@"오전"].location != NSNotFound) {
        nTime = [nTime stringByReplacingOccurrencesOfString:@"오전 " withString:@""];
        nTime = [nTime stringByReplacingOccurrencesOfString:@":" withString:@""];
        
    }else if( [nTime rangeOfString:@"오후"].location != NSNotFound) {
        
        nTime = [nTime stringByReplacingOccurrencesOfString:@"오후 " withString:@""];
        NSArray *time      = [nTime componentsSeparatedByString:@":"];
        NSString *hour = @"";
        if([[time objectAtIndex:0] intValue] == 12){
            hour      = @"12";
        }else{
            hour      = [NSString stringWithFormat:@"%@", [Utils getZeroAppend:[[time objectAtIndex:0] intValue] + 12]];
        }
        
        NSString *minute    = [NSString stringWithFormat:@"%@", [Utils getZeroAppend:[[time objectAtIndex:1] intValue]]];
        
        nTime = [NSString stringWithFormat:@"%@%@", hour, minute];
    }
    return nTime;
}

+ (NSString *)getFoodNumberDatetime:(NSString*)nDate nTime:(NSString*)nTime {
    
    nDate = [nDate stringByReplacingOccurrencesOfString:@"." withString:@""];
    nDate = [nDate stringByReplacingOccurrencesOfString:@"-" withString:@""];
    nTime = [Utils getConvertServertTime:nTime];
    nTime = [nTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    nDate = [nDate stringByReplacingOccurrencesOfString:@" " withString:@""];
    nTime = [nTime stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [NSString stringWithFormat:@"%@%@", nDate, nTime];
}

+ (NSString *)getAmPmPlayHour:(NSString *)hourStr{
    
    int hour = [[hourStr substringWithRange:NSMakeRange(0, 2)] intValue];
    NSString *minute = [hourStr substringWithRange:NSMakeRange(3, 2)];
    
    if(hour == 12){
        return [NSString stringWithFormat:@"오후 %d:%@", hour, minute];
    }else if(hour > 12 && hour < 22){
        return [NSString stringWithFormat:@"오후 0%d:%@", hour-12, minute];
    }else if(hour > 21){
        return [NSString stringWithFormat:@"오후 %d:%@", hour-12, minute];
    }else{
        return [NSString stringWithFormat:@"오전 %@:%@", [Utils getZeroAppend:hour], minute];
    }
}

+ (NSString *)getNumberComma:(NSString *)number {
    
    if ([number length] > 0) {
        number = [number stringByReplacingOccurrencesOfString:@"," withString:@""];
    }
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([number rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        NSNumberFormatter * numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        return [NSString stringWithFormat:@"%@", [numFormatter stringFromNumber:[NSNumber numberWithInt:[number intValue]]]];
    }else {
        return number;
    }
}

+ (UIImage *)captureScreen {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+ (NSString*)generateRandomString:(int)length {
    NSString *characters = @"0123456789abcdefghijklmnopqrstuvwxyz";
    
    int rand;
    NSString *token = @"";
    for (int i=0; i < length; i++) {
        rand = arc4random() % 34;
        token = [NSString stringWithFormat:@"%@%@", token, [characters substringWithRange:NSMakeRange(rand, 1)] ];
    }
    
    NSString *date = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *now = [NSDate date];
    [formatter setDateFormat:@"yyMMddhhmmssmi"];
    date = [formatter stringFromDate:now];
    token = [NSString stringWithFormat:@"%@%@", token, date];
    
    for (int i=0; i < [date length]; i++) {
        rand = arc4random() % [date length]-1;
        token = [NSString stringWithFormat:@"%@%@", token, [date substringWithRange:NSMakeRange(rand, 1)] ];
    }
    //    NSLog(@"user token:%@", token);
    
    return token;
}

///NEW
// nexttime format
+ (NSString *)getNextTimeFormat:(NSString *)time beTime:(NSString *)beTime afTime:(NSString *)afTime{
    NSString *result = @"";
    
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    [dtF setDateFormat:beTime];
    NSDate *d = [dtF dateFromString:time];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:1];
    NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate: d options:0];
    NSLog(@"2 nextDate %@", nextDate);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:afTime];
    result = [dateFormat stringFromDate:nextDate];
    
    return result;
}

@end
