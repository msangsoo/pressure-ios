//
//  AlertUtil.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "AlertUtils.h"

@implementation AlertUtils

+ (void)BasicAlertShow:(UIViewController *)weak tag:(int)tag title:(NSString *)title msg:(NSString *)msg left:(NSString *)left right:(NSString *)right {
    BasicAlertVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"BasicAlert"];
    
    vc.delegate = weak;
    vc.tag = tag;
    
    vc.titleStr = title;
    vc.msgStr = msg;
    vc.leftStr = left;
    vc.rightStr = right;
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

+ (void)PointAlertShow:(UIViewController *)weak point:(NSString *)point pointTitle:(NSString *)pointTitle pointInfo:(NSString *)pointInfo {
    PointAlertVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"PointAlertVC"];
    
    vc.point = point;
    vc.pointTitle = pointTitle;
    vc.pointInfo = pointInfo;
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

+ (void)EmojiDefaultShow:(UIViewController *)weak emoji:(EmojiImgType)emoji title:(NSString *)title msg:(NSString *)msg left:(NSString *)left right:(NSString *)right {
    EmojiDefaultVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"EmojiDefaultVC"];
    
    vc.delegate = weak;
    
    vc.titleStr = title;
    vc.msgStr = msg;
    vc.leftStr = left;
    vc.rightStr = right;
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
    
    [vc emojiType:emoji];
}

+ (void)HealthMessageShow:(UIViewController *)weak type:(NSString *)type msg:(NSString *)msg {
    
    HealthMessageAlertVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"HealthMessageAlertVC"];
    
    vc.delegate = weak;
    vc.healthTypeStr = type;
    vc.msgStr = msg;
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

#pragma mark - control
+ (void)DateControlShow:(UIViewController *)viewControl dateType:(DateControlDateType)type tag:(int)tag selectDate:(NSString*)sDate {
    
    DateControlVC *vc = [CONTROLS_STORYBOARD instantiateViewControllerWithIdentifier:@"DateControlVC"];
    vc.tag = tag;
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:^{
        [vc dateControllerWithDelegate:viewControl dateType:type defaultDate:sDate];
    }];
}

@end
