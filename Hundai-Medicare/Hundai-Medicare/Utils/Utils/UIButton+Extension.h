//
//  UIButton+Extension.h
//  Hundai-Medicare
//
//  Created by YuriHan on 13/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (AssociatedObject)
@property (nonatomic, strong) NSIndexPath* indexPath;
@property (nonatomic, strong) id associatedModel;
@end

NS_ASSUME_NONNULL_END
