//
//  AlertUtil.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BasicAlertVC.h"
#import "PointAlertVC.h"
#import "EmojiDefaultVC.h"
#import "HealthMessageAlertVC.h"

#import "DateControlVC.h"

@interface AlertUtils : NSObject

+ (void)BasicAlertShow:(UIViewController *)weak tag:(int)tag title:(NSString *)title msg:(NSString *)msg left:(NSString *)left right:(NSString *)right;

+ (void)PointAlertShow:(UIViewController *)weak point:(NSString *)point pointTitle:(NSString *)pointTitle pointInfo:(NSString *)pointInfo;

+ (void)EmojiDefaultShow:(UIViewController *)weak emoji:(EmojiImgType)emoji title:(NSString *)title msg:(NSString *)msg left:(NSString *)left right:(NSString *)right;

+ (void)HealthMessageShow:(UIViewController *)weak type:(NSString *)type msg:(NSString *)msg;

+ (void)DateControlShow:(UIViewController *)viewControl dateType:(DateControlDateType)type tag:(int)tag selectDate:(NSString*)sDate;

@end

