//
//  TimeUtils.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 17..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeUtils : NSObject{
    NSString *nowTime;
    NSString *endTime;
    NSString *timeType;
}

@property (readonly, readonly) Boolean rightHidden;
@property (readonly, readonly) NSInteger monthMaxDay;
 
-(NSString *)getNowTime;
-(NSString *)getEndTime;
-(void)setTimeType:(NSString *)type;
+(NSString *)getDayKoreaformat:(NSString *)date;
+(NSString *)getDateAddDay:(int)day;
-(void)setTimeDate;
+(NSString*)getNowDateFormat:(NSString*)format;
-(void)getTime:(int)calTime;
+(NSString *)getDayformat:(NSString *)date format:(NSString *)format;
+(BOOL)getDateValidate:(NSString *)nDate;
+(NSString *)getDateToString:(NSDate *)nDate;
+(NSString *)getDateAdd:(NSString*)nDate day:(int)day;
+(NSDate *)getStringToDate:(NSString*)nDate;
+(NSString *)getDayKoreaTimeformat:(NSString *)date;
+(long)getTimeMilsFormat:(NSString*)format date:(NSString*)date;
@end
