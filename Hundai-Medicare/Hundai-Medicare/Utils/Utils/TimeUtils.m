//
//  TimeUtils.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 17..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "TimeUtils.h"
#import "Utils.h"

@implementation TimeUtils

-(id)init{
    self = [super init];
    timeType = PERIOD_DAY;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    nowTime = [dateFormatter stringFromDate:[NSDate date]];
    endTime = [dateFormatter stringFromDate:[NSDate date]];
    
    [self getTime:0];
    
    return self;
}

-(NSString *)getNowTime {
    return nowTime;
}

-(NSString *)getEndTime {
    return endTime;
}

-(void)setTimeType:(NSString *)type {
    timeType = type;
    [self setTimeDate];
}




-(void)setTimeDate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    nowTime = [dateFormatter stringFromDate:[NSDate date]];
    endTime = [dateFormatter stringFromDate:[NSDate date]];
    
    [self getTime:0];
}

-(void)getTime:(int)calTime {
    
    if(calTime > 0) {
        nowTime = endTime;
    }
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    if([timeType isEqualToString:PERIOD_DAY]) {
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"yyyy"] intValue];
        comp.month = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"MM"] intValue];
        comp.day = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"dd"] intValue];
        NSDate *firstdate = [cal dateFromComponents:comp];
        
        firstdate = [cal dateByAddingUnit:NSCalendarUnitDay value:calTime toDate:firstdate options:0];
        NSDate *enddate = [self getLastDate:firstdate];
        
        NSDateFormatter *dft = [[NSDateFormatter alloc] init];
        [dft setDateFormat:@"yyyy-MM-dd"];
        nowTime = [dft stringFromDate:firstdate];
        endTime = [dft stringFromDate:enddate];
        
        NSLog(@"%@", nowTime);
        NSLog(@"%@", endTime);
        
    }else if([timeType isEqualToString:PERIOD_WEEK]) {
        // Start of week:
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"yyyy"] intValue];    // <-- fill in your year here
        comp.month = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"MM"] intValue]; // <-- fill in your week number here
        comp.day = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"dd"] intValue];
        
        NSDate *firstdate = [cal dateFromComponents:comp];
        firstdate = [cal dateByAddingUnit:NSCalendarUnitDay value:calTime toDate:firstdate options:0];
        
        NSDate *enddate;
        NSTimeInterval interval; [cal rangeOfUnit:NSCalendarUnitWeekOfYear startDate:&firstdate interval:&interval forDate:firstdate];
        
        enddate = [firstdate dateByAddingTimeInterval:interval - 1];
        
        enddate = [self getLastDate:enddate];
        
        [cal rangeOfUnit:NSCalendarUnitDay  startDate:&enddate interval:NULL forDate:enddate];
        
        NSDateFormatter *dft = [[NSDateFormatter alloc] init];
        [dft setDateFormat:@"yyyy-MM-dd"];
        NSLog(@"%@", [dft stringFromDate:firstdate]);
        NSLog(@"%@", [dft stringFromDate:enddate]);
        
        nowTime = [dft stringFromDate:firstdate];
        endTime = [dft stringFromDate:enddate];
        
    }else if([timeType isEqualToString:PERIOD_MONTH]) {
        // Start of week:
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"yyyy"] intValue];    // <-- fill in your year here
        comp.month = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"MM"] intValue] ; // <-- fill in your week number here
        comp.day = 1;
        
        NSDate *firstdate = [cal dateFromComponents:comp];
        firstdate = [cal dateByAddingUnit:NSCalendarUnitMonth value:calTime toDate:firstdate options:0];
        
        NSRange currentRange = [cal rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:firstdate];
        NSInteger numberOfDays = currentRange.length;
        _monthMaxDay = currentRange.length;
        NSDate *enddate = [cal dateByAddingUnit:NSCalendarUnitDay value:(numberOfDays-1) toDate:firstdate options:0];
        
        enddate = [self getLastDate:enddate];
        
        NSDateFormatter *dft = [[NSDateFormatter alloc] init];
        [dft setDateFormat:@"yyyy-MM-dd"];
        NSLog(@"%@", [dft stringFromDate:firstdate]);
        NSLog(@"%@", [dft stringFromDate:enddate]);
        
        nowTime = [dft stringFromDate:firstdate];
        endTime = [dft stringFromDate:enddate];
        
    }else if([timeType isEqualToString:PERIOD_YEAR]) {
        // Start of week:
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year = [[Utils getTimeFormat:nowTime beTime:@"yyyy-MM-dd" afTime:@"yyyy"] intValue];    // <-- fill in your year here
        comp.month = 1; // <-- fill in your week number here
        comp.day = 1;
        
        NSDate *firstdate = [cal dateFromComponents:comp];
        firstdate = [cal dateByAddingUnit:NSCalendarUnitYear value:calTime toDate:firstdate options:0];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:firstdate];
        
        comp.year = [components year];    // <-- fill in your year here
        comp.month = 12;
        comp.day = 31;
        
        NSDate *enddate = [cal dateFromComponents:comp];
        
        enddate = [self getLastDate:enddate];
        
        NSDateFormatter *dft = [[NSDateFormatter alloc] init];
        [dft setDateFormat:@"yyyy-MM-dd"];
        NSLog(@"%@", [dft stringFromDate:firstdate]);
        NSLog(@"%@", [dft stringFromDate:enddate]);
        
        nowTime = [dft stringFromDate:firstdate];
        endTime = [dft stringFromDate:enddate];
    }
}

+(NSString *)getDayKoreaTimeformat:(NSString *)date {
    
    date = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    date = [date stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:@"yyyy.MM.dd hh:mm"];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.year   = [[date substringToIndex:4] intValue];
    comp.month  = [[date substringWithRange:NSMakeRange(4, 2)] intValue];
    comp.day    = [[date substringWithRange:NSMakeRange(6, 2)] intValue];
    comp.hour    = [[date substringWithRange:NSMakeRange(8, 2)] intValue];
    comp.minute    = [[date substringWithRange:NSMakeRange(10, 2)] intValue];
    
    NSDate *nowdate = [cal dateFromComponents:comp];
    NSString *stringFromDate = [dft stringFromDate:nowdate];
    
    return stringFromDate;
}

+(NSString *)getDayKoreaformat:(NSString *)date  {
    date = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    date = [date stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:@"yyyy년MM월dd일"];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.year   = [[date substringToIndex:4] intValue];
    comp.month  = [[date substringWithRange:NSMakeRange(4, 2)] intValue];
    comp.day    = [[date substringWithRange:NSMakeRange(6, 2)] intValue];
    
    NSDate *nowdate = [cal dateFromComponents:comp];
    NSString *stringFromDate = [dft stringFromDate:nowdate];
    
    return stringFromDate;
}

+(NSString *)getDayformat:(NSString *)date format:(NSString *)format {
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:format];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.year   = [[date substringToIndex:4] intValue];
    comp.month  = [[date substringWithRange:NSMakeRange(4, 2)] intValue];
    if ([date length] >=8) {
        comp.day    = [[date substringWithRange:NSMakeRange(6, 2)] intValue];
    }
    NSDate *nowdate = [cal dateFromComponents:comp];
    NSString *stringFromDate = [dft stringFromDate:nowdate];

    return stringFromDate;
}

+(NSString*)getNowDateFormat:(NSString*)format {
    
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:format];
    NSString *returnDate = [dft stringFromDate:[NSDate date]];
    
    return returnDate;
}

+(long)getTimeMilsFormat:(NSString*)format date:(NSString*)date {
    
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:format];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dft dateFromString:date];
    long timeInMiliseconds = (long)([dateFromString timeIntervalSince1970] * 1000.0);
    
    return timeInMiliseconds;
}


-(NSDate *)getLastDate:(NSDate *)enddate {
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:@"yyyy-MM-dd"];
    NSString *time = [dft stringFromDate:[NSDate date]];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.year = [[Utils getTimeFormat:time beTime:@"yyyy-MM-dd" afTime:@"yyyy"] intValue];
    comp.month = [[Utils getTimeFormat:time beTime:@"yyyy-MM-dd" afTime:@"MM"] intValue];
    comp.day = [[Utils getTimeFormat:time beTime:@"yyyy-MM-dd" afTime:@"dd"] intValue];
    
    NSDate *nowdate = [cal dateFromComponents:comp];
    
    NSTimeInterval nowTimeInter = [nowdate timeIntervalSince1970];
    NSTimeInterval endTimeInter = [enddate timeIntervalSince1970];

    if(nowTimeInter <= endTimeInter) {
        _rightHidden = YES;
        return [NSDate date];
    }else{
        _rightHidden = NO;
        return enddate;
    }
}

  

+(NSString *)getDateAddDay:(int)day {
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = day;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSDate *dte = [NSDate date];
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:dte options:0];
    
    return [self getDateToString:nextDate];
}



+(NSString *)getDateToString:(NSDate*)nDate {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSString *dateString = [dateFormat stringFromDate:nDate];
    
    return dateString;
}

+(NSString *)getDateAdd:(NSString*)nDate day:(int)day {
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = day;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[self getStringToDate:nDate] options:0];
    
    return [self getDateToString:nextDate];
}


+(BOOL)getDateValidate:(NSString *)nDate {
    
    if ([nDate length] != 8) {
        return NO;
    }
    NSString *y = [nDate substringToIndex:4];
    NSString *m = [nDate substringWithRange:NSMakeRange(4, 2)];
    NSString *d = [nDate substringWithRange:NSMakeRange(6, 2)];
    
    if ([y intValue] < 1900 || [y intValue] > 2050) {
        return NO;
    }else if(!([m intValue] >= 1 && [m intValue] < 13)) {
        return NO;
    }else if(!([d intValue] >= 1 && [d intValue] < 32)) {
        return NO;
    }
    return YES;
    
}

+(NSDate *)getStringToDate:(NSString*)nDate {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ko_KR"]];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSDate *dte = [dateFormat dateFromString:nDate];
    
    return dte;
}

@end
