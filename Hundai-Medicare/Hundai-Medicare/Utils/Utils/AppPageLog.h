//
//  AppPageLog.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 11. 27..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogDB.h"

@interface AppPageLog : NSObject

// Instance
+ (id)instance;

- (void)inTimeLog:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod;
- (void)inCountLog:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod;
- (void)terminateLogset;
- (void)becomeActiveLogset;
@end
