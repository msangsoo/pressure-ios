//
//  Utils.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject {
    
}

@property (strong) NSString *resultText;

@property (nonatomic, retain) id delegate;

+ (NSString *)getUserDefault:(NSString*)key;

+ (void)setUserDefault:(NSString*)key value:(NSString*)value;

+ (void)setUserDefault:(NSString*)key bools:(BOOL)value;

+ (void)removeUserDefault:(NSString *)key;

+ (UIColor *)uintColor:(uint)color;

+ (void)playSound:(NSString *)fileName :(NSString *) ext;

+ (NSString *)decimalNumber:(int)number;

+ (void)imageSequenceAnimation:(NSString *)name withLength:(int)length withImageView:(UIImageView *)imageView;

+ (NSString *)dateToString:(NSDate *)date format:(NSString*)format;

+ (NSString *)dateStringToString:(NSString *)dateStr;

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg;

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg completion:(void (^)(void))completion;

+ (void)basicAlertView:(id)view withTitle:(NSString *)title withMsg:(NSString *)msg completion:(void (^)(void))completion fail:(void (^)(void))fail;

+ (BOOL)emailIsValid:(NSString *)checkString;

+ (BOOL)NSStringIsValidPass:(NSString *)checkString;

+ (void)openDetailView:(id)sender postID:(NSString *)postID brdKey:(NSString *)brdKey title:(NSString *)title;

+ (void)openDetailExpoView:(id)sender postID:(NSString *)postID title:(NSString *)title;

+ (NSString *)tagImage:(NSString *)brdKey;

+ (BOOL)stringNullCheck:(NSString *)str;

+ (CGFloat)getLabelHeight:(UILabel*)label;

+ (CGFloat)getTextViewHeight:(UITextView *)textview;

+ (NSString *)stringEmptyReturn:(NSString *)str;

+ (NSString *)getNowDate:(NSString *)format;

+ (NSInteger)getNsIntVal:(NSString *)str;

+ (NSString *)getTimeFormat:(NSString *)time beTime:(NSString *)beTime afTime:(NSString *)afTime;

+ (NSString *)getAmPmHour:(NSString *)hourStr;

+ (NSString *)getMonthDay:(NSString *)time;

+ (NSString *)getCurrDateAdd:(NSString *)nDate;

+ (NSDate *)getDateFromString:(NSString *)string;

+ (NSString *) makeQuery:(NSString*)strSearch;

+ (BOOL)string:(NSString *)compareString containsChoseong:(NSString *)choseongString;

+ (NSString *)getNoneZeroString:(float)value;

+ (NSString *)getZeroAppend:(int)value;

+ (NSString *)getConvertServertDate:(NSString*)nDate;

+ (NSString *)getConvertServertTime:(NSString*)nTime;

+ (NSString *)getFoodNumberDatetime:(NSString*)nDate nTime:(NSString*)nTime;

+ (NSString *)getAmPmPlayHour:(NSString *)hourStr;

+ (NSString *)getNumberComma:(NSString *)number;

+ (UIImage *)captureScreen;

+ (NSString*)generateRandomString:(int)length;

+ (void)callTelephone:(id)sender withPhone:(NSString *)phone message:(NSString*)msg;

+ (void)callTelephone:(id)sender withPhone:(NSString *)phone message:(NSString*)msg svcCode:(NSString*)svcCode delegate:(id)delegate;

//NEW
+ (NSString *)getNextTimeFormat:(NSString *)time beTime:(NSString *)beTime afTime:(NSString *)afTime;


@end


@protocol UtilsDelegate
- (void)callbackMessage;
@end

