//
//  AppPageLog.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 11. 27..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "AppPageLog.h"

@implementation AppPageLog {
    NSDate *viewLoadDate;
    LogDB *logDB;
    NSString *currCodeL;
    NSString *currCodeM;
    NSString *currCodeS;
}

static AppPageLog *instance = nil;

+ (id)instance {
    if( instance == nil ) {
        instance = [self new];
        [instance initInstance];
    }
    return instance;
}

- (void)initInstance {
    logDB = [[LogDB alloc] init];
    viewLoadDate = [NSDate date];
    currCodeL = [NSString string];
    currCodeM = [NSString string];
    currCodeS = [NSString string];
    currCodeL = @"";
    currCodeM = @"";
    currCodeS = @"";
}

- (void)inTimeLog:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod {
    
    BOOL equalL=false, equalM=false, equalS=false;
    if (![l_cod isEqualToString:currCodeL]) {
        equalL = true;
    }else if (![m_cod isEqualToString:currCodeM]) {
        equalM = true;
    }else if (![s_cod isEqualToString:currCodeS]) {
        equalS = true;
    }
    
    if (equalL && equalM && equalS) {
        return;
    }
    
    [self inTimeLogDo:l_cod m_cod:m_cod s_cod:s_cod];
}

- (void)inTimeLogDo:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod{
    
    NSDate *viewDidDate = [[NSDate alloc] init];
    double interval = viewDidDate.timeIntervalSinceReferenceDate - self->viewLoadDate.timeIntervalSinceReferenceDate;
    
    //지연시간이 30분이상이라면 스킵.
    if (interval > 1800) {
        NSLog(@"잔류시간 30분이상 스킵됨 :%f", interval);
        return;
    }
    
    NSLog(@"페이지 잔류시간:%.f초  (l_cod:%@, m_cod:%@, s_cod:%@)",interval, currCodeL, currCodeM, currCodeS);
    [logDB insertDb:currCodeL m_cod:currCodeM s_cod:currCodeS time:(int)interval];
    
    currCodeL = [l_cod copy];
    currCodeM = [m_cod copy];
    currCodeS = [s_cod copy];
    self->viewLoadDate = [[NSDate alloc] init];
}
- (void)inCountLog:(NSString *)l_cod m_cod:(NSString *)m_cod s_cod:(NSString *)s_cod {
    NSLog(@"페이지 카운트 - l_cod:%@, m_cod:%@, s_cod:%@)", l_cod, m_cod, s_cod);
    [logDB insertDb:l_cod m_cod:m_cod s_cod:s_cod time:0];
}

- (void)terminateLogset {
    
    [self inTimeLogDo:currCodeL m_cod:currCodeM s_cod:currCodeS];
    
}

- (void)becomeActiveLogset {
    self->viewLoadDate = [[NSDate alloc] init];
}

@end
