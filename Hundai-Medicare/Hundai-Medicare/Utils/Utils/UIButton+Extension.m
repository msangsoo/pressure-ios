//
//  UIButton+Extension.m
//  Hundai-Medicare
//
//  Created by YuriHan on 13/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "UIButton+Extension.h"
#import <objc/runtime.h>

@implementation UIButton (AssociatedObject)
static char* kIndexPathKey = "indexPath";

- (NSIndexPath*)indexPath
{
    return objc_getAssociatedObject(self,&kIndexPathKey);
}

- (void)setIndexPath:(NSIndexPath*)indexPath
{
    objc_setAssociatedObject(self, &kIndexPathKey, indexPath, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

static char* kAssociatedModel = "associatedModel";
- (id)associatedModel
{
    return objc_getAssociatedObject(self,&kAssociatedModel);
}

- (void)setAssociatedModel:(id)associatedModel
{
    objc_setAssociatedObject(self, &kAssociatedModel, associatedModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
