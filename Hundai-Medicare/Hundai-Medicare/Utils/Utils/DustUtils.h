//
//  DustUtils.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 7..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DustUtils : NSObject<NSXMLParserDelegate>{
    
}

@property (strong, nonatomic) NSDictionary *MapKey;
@property (strong, nonatomic) NSString *skytext;
@property (strong, nonatomic) NSString *feelslike;

- (NSString *)getCityName:(NSString *)key;
- (NSString *)getDustCode:(NSString *)name;
- (NSString *)getDustQyStatus:(float)value;
- (void)getWeather:(NSString *)cityname;
- (NSString *)getSkytext;
- (NSString *)getFeelslike;

@end
