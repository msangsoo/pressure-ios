//
//  CaffeineDataStore.m
//  HealthKitDemo
//
//  Created by Andrew Dolce on 9/23/14.
//  Copyright (c) 2014 Intrepid Pursuits. All rights reserved.
//

#import "CaffeineDataStore.h"

NSString * const kCaffeineDataUpdateNotificiation = @"kCaffeineDataUpdateNotificiation";

@interface CaffeineDataStore ()

@property (strong, nonatomic) HKHealthStore *healthStore;

@end

@implementation CaffeineDataStore

- (instancetype)initWithHealthStore:(HKHealthStore *)healthStore {
    self = [super init];
    if (self) {
        self.healthStore = healthStore;
//        [self startObservingUpdates];
    }
    return self;
}

- (void)healthKitSamplesWithCopletion:(NSString *)startData endData:(NSString *)endData :(void (^)(NSArray *samples, NSError *error))completion {
    [self requestAuthorizationWithCompletion:^(BOOL success, NSError *error) {
        
        //         걷게 데이터 소팅 방식 설정 (내림 차순 조회)
        NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierStartDate ascending:YES];
        //         healthkit 조회 시간 설정 : 시작 / 종료 - 임시로 오늘 0시부터 현재 시간으로 설정
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year   = [[startData substringToIndex:4] intValue];
        comp.month  = [[startData substringWithRange:NSMakeRange(4, 2)] intValue];
        comp.day    = [[startData substringWithRange:NSMakeRange(6, 2)] intValue];
        if(startData.length > 13) {
            comp.hour = [[startData substringWithRange:NSMakeRange(8, 2)] intValue];
            comp.minute = [[startData substringWithRange:NSMakeRange(10, 2)] intValue];
            comp.second = [[startData substringWithRange:NSMakeRange(12, 2)] intValue];
        }else {
            comp.hour = 0;
            comp.minute = 0;
            comp.second = 0;
        }
        NSDate *startDate = [cal dateFromComponents:comp];
        
        comp.year   = [[endData substringToIndex:4] intValue];
        comp.month  = [[endData substringWithRange:NSMakeRange(4, 2)] intValue];
        comp.day    = [[endData substringWithRange:NSMakeRange(6, 2)] intValue];
        if(endData.length > 13) {
            comp.hour = [[endData substringWithRange:NSMakeRange(8, 2)] intValue];
            comp.minute = [[endData substringWithRange:NSMakeRange(10, 2)] intValue];
            comp.second = [[endData substringWithRange:NSMakeRange(12, 2)] intValue];
        }else {
            comp.hour = 23;
            comp.minute = 59;
            comp.second = 59;
        }
        NSDate *endDate = [cal dateFromComponents:comp];
        
        NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
        
        HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type
                                                               predicate:predicate
                                                                   limit:HKObjectQueryNoLimit // 숫자로 넣어됌
                                                         sortDescriptors:@[timeSortDescriptor]
                                                          resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
                                                              if (error) {
                                                                  NSLog(@"Error fetching samples from HealthKit: %@", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          }];
        [self.healthStore executeQuery:query];
    }];
}

- (void)fetchCaffieneSamplesWithCompletion:(void (^)(NSArray *samples, NSError *error))completion {
    [self requestAuthorizationWithCompletion:^(BOOL success, NSError *error) {
        
        HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type
                                                               predicate:nil
                                                                   limit:10 // 숫자로 넣어됌
                                                         sortDescriptors:nil
                                                          resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
                                                              if (error) {
                                                                  NSLog(@"Error fetching samples from HealthKit: %@", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          }];
        [self.healthStore executeQuery:query];
    }];
}

- (void)fetchRunningWithCompletion:(void (^)(NSArray *samples, NSError *error))completion {
    [self requestAuthorizationWithCompletion:^(BOOL success, NSError *error) {
        HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type
                                                               predicate:nil
                                                                   limit:10 // 숫자로 넣어됌
                                                         sortDescriptors:nil
                                                          resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
                                                              if (error) {
                                                                  NSLog(@"Error fetching samples from HealthKit: %@", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          }];
        [self.healthStore executeQuery:query];
    }];
}

- (void)healthKitRunningSamplesWithCopletion:(NSString *)startData endData:(NSString *)endData :(void (^)(NSArray *samples, NSError *error))completion {
    [self requestAuthorizationWithCompletion:^(BOOL success, NSError *error) {
        
        //         걷게 데이터 소팅 방식 설정 (내림 차순 조회)
        NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:NO];
        //         healthkit 조회 시간 설정 : 시작 / 종료 - 임시로 오늘 0시부터 현재 시간으로 설정
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        comp.year   = [[startData substringToIndex:4] intValue];
        comp.month  = [[startData substringWithRange:NSMakeRange(4, 2)] intValue];
        comp.day    = [[startData substringWithRange:NSMakeRange(6, 2)] intValue];
        if(startData.length > 13) {
            comp.hour = [[startData substringWithRange:NSMakeRange(8, 2)] intValue];
            comp.minute = [[startData substringWithRange:NSMakeRange(10, 2)] intValue];
            comp.second = [[startData substringWithRange:NSMakeRange(12, 2)] intValue];
        }else {
            comp.hour = 0;
            comp.minute = 0;
            comp.second = 0;
        }
        NSDate *startDate = [cal dateFromComponents:comp];
        
        comp.year   = [[endData substringToIndex:4] intValue];
        comp.month  = [[endData substringWithRange:NSMakeRange(4, 2)] intValue];
        comp.day    = [[endData substringWithRange:NSMakeRange(6, 2)] intValue];
        if(endData.length > 13) {
            comp.hour = [[endData substringWithRange:NSMakeRange(8, 2)] intValue];
            comp.minute = [[endData substringWithRange:NSMakeRange(10, 2)] intValue];
            comp.second = [[endData substringWithRange:NSMakeRange(12, 2)] intValue];
        }else {
            comp.hour = 23;
            comp.minute = 59;
            comp.second = 59;
        }
        NSDate *endDate = [cal dateFromComponents:comp];
        
        NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionStrictStartDate];
        
        HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type
                                                               predicate:predicate
                                                                   limit:HKObjectQueryNoLimit // 숫자로 넣어됌
                                                         sortDescriptors:@[timeSortDescriptor]
                                                          resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
                                                              if (error) {
                                                                  NSLog(@"Error fetching samples from HealthKit: %@", error);
                                                              }
                                                              if (completion) {
                                                                  completion(results, error);
                                                              }
                                                          }];
        [self.healthStore executeQuery:query];
    }];
}

- (void)saveCaffeineSample:(double)caffeineInMilligrams completion:(void (^)(BOOL success, NSError *error))completion {
    [self requestAuthorizationWithCompletion:^(BOOL success, NSError *error) {
        HKUnit *unit = [HKUnit unitFromString:@"mg"];
        HKQuantity *quantity = [HKQuantity quantityWithUnit:unit doubleValue:caffeineInMilligrams];
        HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDietaryCaffeine];
        NSDate *now = [NSDate date];
        HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:type quantity:quantity startDate:now endDate:now];
        [self.healthStore saveObject:sample withCompletion:completion];
    }];
}

- (void)startObservingUpdates {
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    HKObserverQuery *observationQuery = [[HKObserverQuery alloc] initWithSampleType:type predicate:nil updateHandler:^(HKObserverQuery *query, HKObserverQueryCompletionHandler completionHandler, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kCaffeineDataUpdateNotificiation object:self];
    }];
    [self.healthStore executeQuery:observationQuery];
}

#pragma mark - Authorization Request Helper
- (BOOL)isHealthDataAvailable {
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    return [_healthStore authorizationStatusForType:type];
}

- (void)requestAuthorizationWithCompletion:(void(^)(BOOL success, NSError *error))completion {
    if ([HKHealthStore isHealthDataAvailable]) {
        NSSet *types  = [NSSet setWithObjects:
                         [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount],
                         nil];
        [self.healthStore requestAuthorizationToShareTypes:nil readTypes:types completion:^(BOOL success, NSError *error) {
            if (error) {
                NSLog(@"Error requesting HealthKit permissions: %@", error);
            }
            if (completion) {
                completion(success, error);
            }
        }];
    } else if (completion) {
        completion(NO, nil);
    }
}

@end
