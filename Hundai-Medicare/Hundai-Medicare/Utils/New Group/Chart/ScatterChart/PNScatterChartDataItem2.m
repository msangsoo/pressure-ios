//
//  PNScatterChartDataItem.m
//  PNChartDemo
//
//  Created by Alireza Arabi on 12/4/14.
//  Copyright (c) 2014 kevinzhow. All rights reserved.
//

#import "PNScatterChartDataItem2.h"

@interface PNScatterChartDataItem2 ()

- (id)initWithX:(CGFloat)x AndWithY:(CGFloat)y;

@property (readwrite) CGFloat x; // should be within the x range
@property (readwrite) CGFloat y; // should be within the y range

@end

@implementation PNScatterChartDataItem2

+ (PNScatterChartDataItem2 *)dataItemWithX:(CGFloat)x AndWithY:(CGFloat)y
{
    return [[PNScatterChartDataItem2 alloc] initWithX:x AndWithY:y];
}

- (id)initWithX:(CGFloat)x AndWithY:(CGFloat)y
{
    if ((self = [super init])) {
        if (x==y) {
            y = 60.0f;
        }
        self.x = x;
        self.y = y;
    }
    
    return self;
}

@end
