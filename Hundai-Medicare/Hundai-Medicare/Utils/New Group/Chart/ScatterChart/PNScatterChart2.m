//
//  PNScatterChart2.m
//  PNChartDemo
//
//  Created by Alireza Arabi on 12/4/14.
//  Copyright (c) 2014 kevinzhow. All rights reserved.
//

#import "PNScatterChart2.h"
#import "PNChartLabel2.h"
#import "PNScatterChartData2.h"
#import "PNScatterChartDataItem2.h"

@interface PNScatterChart2 ()

@property (nonatomic, weak) CAShapeLayer *pathLayer;
@property (nonatomic, weak) NSMutableArray *verticalLineLayer;
@property (nonatomic, weak) NSMutableArray *horizentalLinepathLayer;

@property (nonatomic) CGPoint startPoint;

@property (nonatomic) CGPoint startPointVectorX;
@property (nonatomic) CGPoint endPointVecotrX;

@property (nonatomic) CGPoint startPointVectorY;
@property (nonatomic) CGPoint endPointVecotrY;

@property (nonatomic) CGFloat vectorX_Steps;
@property (nonatomic) CGFloat vectorY_Steps;

@property (nonatomic) CGFloat vectorX_Size;
@property (nonatomic) CGFloat vectorY_Size;

@property (nonatomic) NSMutableArray *axisX_labels;
@property (nonatomic) NSMutableArray *axisY_labels;

@property (nonatomic) int AxisX_partNumber ;
@property (nonatomic) int AxisY_partNumber ;

@property (nonatomic) CGFloat AxisX_step ;
@property (nonatomic) CGFloat AxisY_step ;

@property (nonatomic) CGFloat AxisX_Margin;
@property (nonatomic) CGFloat AxisY_Margin;

@property (nonatomic) BOOL isForUpdate;

@end


@implementation PNScatterChart2

#pragma mark initialization

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        [self setupDefaultValues];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame chartType:(int)et
{
    self = [super initWithFrame:frame];
    
    if (self) {
        eattype = et;
        [self setupDefaultValues];
    }
    return self;
}

- (void) setup
{
    [self vectorXSetup];
    [self vectorYSetup];
}

- (void)setupDefaultValues
{
    [super setupDefaultValues];
    
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds   = YES;
    _showLabel           = YES;
    _isForUpdate         = NO;
    self.userInteractionEnabled = YES;
    
    // Coordinate Axis Default Values
    _showCoordinateAxis = YES;
    _axisColor = [UIColor colorWithRed:0.84f green:0.84f blue:0.84f alpha:1.f];
    _axisWidth = 0.4f;
    
    // Initialization code
    _AxisX_Margin = 30 ;
    _AxisY_Margin = 30 ;
    
    //    self.frame = CGRectMake((SCREEN_WIDTH - self.frame.size.width) / 2, 200, self.frame.size.width, self.frame.size.height) ;
    self.backgroundColor = [UIColor clearColor];
    
    _startPoint.y = self.frame.size.height - self.AxisY_Margin ;
    _startPoint.x = self.AxisX_Margin ;
    
    _axisX_labels = [NSMutableArray array];
    _axisY_labels = [NSMutableArray array];
    
    _descriptionTextColor = [UIColor blackColor];
    _descriptionTextFont  = [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
    _descriptionTextShadowColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    _descriptionTextShadowOffset =  CGSizeMake(0, 0);
    _duration = 1.0;
    
}

#pragma mark calculating axis

- (void) setAxisXWithMinimumValue:(CGFloat)minVal andMaxValue:(CGFloat)maxVal toTicks:(int)numberOfTicks
{
    _AxisX_minValue = minVal ;
    _AxisX_maxValue = maxVal ;
    _AxisX_partNumber = numberOfTicks - 1;
    _AxisX_step = (float)((maxVal - minVal)/_AxisX_partNumber);
    
    NSString *LabelFormat = self.xLabelFormat ? : @"%1.f";
    CGFloat tempValue = minVal ;
    UILabel *label = [[UILabel alloc] init];
    if(numberOfTicks == 7){
        label.text = @"일";
    }else{
        label.text = [NSString stringWithFormat:LabelFormat,minVal] ;
    }
    label.adjustsFontSizeToFitWidth=YES;
    label.minimumScaleFactor=0.1;
    [_axisX_labels addObject:label];
    for (int i = 0 ; i < _AxisX_partNumber; i++) {
        tempValue = tempValue + _AxisX_step;
        UILabel *tempLabel = [[UILabel alloc] init];
        
        if(_AxisX_partNumber == 6){
            if(i == 0){
                tempLabel.text = @"월";
            }else if(i == 1){
                tempLabel.text = @"화";
            }else if(i == 2){
                tempLabel.text = @"수";
            }else if(i == 3){
                tempLabel.text = @"목";
            }else if(i == 4){
                tempLabel.text = @"금";
            }else if(i == 5){
                tempLabel.text = @"토";
            }
        }else{
            tempLabel.text = [NSString stringWithFormat:LabelFormat,tempValue] ;
        }
        tempLabel.adjustsFontSizeToFitWidth=YES;
        tempLabel.minimumScaleFactor=0.1;
        [_axisX_labels addObject:tempLabel];
    }
}

- (void) setAxisYWithMinimumValue:(CGFloat)minVal andMaxValue:(CGFloat)maxVal toTicks:(int)numberOfTicks
{
    _AxisY_minValue = minVal ;
    _AxisY_maxValue = maxVal ;
    _AxisY_partNumber = numberOfTicks - 1;
    _AxisY_step = (float)((maxVal - minVal)/_AxisY_partNumber);
    
    NSString *LabelFormat = self.yLabelFormat ? : @"%1.f";
    CGFloat tempValue = minVal ;
    UILabel *label = [[UILabel alloc] init];
    label.adjustsFontSizeToFitWidth=YES;
    label.minimumScaleFactor=0.1;
    label.text = [NSString stringWithFormat:LabelFormat,minVal] ;
    [_axisY_labels addObject:label];
    for (int i = 0 ; i < _AxisY_partNumber; i++) {
        tempValue = tempValue + _AxisY_step;
        UILabel *tempLabel = [[UILabel alloc] init];
        tempLabel.adjustsFontSizeToFitWidth=YES;
        tempLabel.minimumScaleFactor=0.1;
        tempLabel.text = [NSString stringWithFormat:LabelFormat,tempValue] ;
        [_axisY_labels addObject:tempLabel];
    }
}

- (NSArray*) getAxisMinMax:(NSArray*)xValues
{
    float min = [xValues[0] floatValue];
    float max = [xValues[0] floatValue];
    for (NSNumber *number in xValues)
    {
        if ([number floatValue] > max)
            max = [number floatValue];
        
        if ([number floatValue] < min)
            min = [number floatValue];
    }
    NSArray *result = @[[NSNumber numberWithFloat:min], [NSNumber numberWithFloat:max]];
    
    
    return result;
}

- (void)setAxisXLabel:(NSArray *)array {
    if(array.count == ++_AxisX_partNumber){
        [_axisX_labels removeAllObjects];
        for(int i=0;i<array.count;i++){
            UILabel *label = [[UILabel alloc] init];
            label.text = [array objectAtIndex:i];
            [_axisX_labels addObject:label];
        }
    }
}

- (void)setAxisYLabel:(NSArray *)array {
    if(array.count == ++_AxisY_partNumber){
        [_axisY_labels removeAllObjects];
        for(int i=0;i<array.count;i++){
            UILabel *label = [[UILabel alloc] init];
            label.text = [array objectAtIndex:i];
            [_axisY_labels addObject:label];
        }
    }
}

- (void) vectorXSetup
{
    _AxisX_partNumber += 1;
    _vectorX_Size = self.frame.size.width - (_AxisX_Margin)  ;
    _vectorX_Steps = (_vectorX_Size) / (_AxisX_partNumber) ;
    _endPointVecotrX = CGPointMake(_startPoint.x + _vectorX_Size, _startPoint.y) ;
    _startPointVectorX = _startPoint ;
}

- (void) vectorYSetup
{
    _AxisY_partNumber += 1;
    _vectorY_Size = self.frame.size.height - (_AxisY_Margin) - (_AxisY_Margin/2);
    _vectorY_Steps = (_vectorY_Size) / (_AxisY_partNumber);
    _endPointVecotrY = CGPointMake(_startPoint.x, _startPoint.y - _vectorY_Size) ;
    _startPointVectorY = _startPoint ;
}

- (void) showXLabel : (UILabel *) descriptionLabel InPosition : (CGPoint) point
{
    CGRect frame = CGRectMake(point.x, point.y, 30, 14);
    descriptionLabel.frame = frame;
    descriptionLabel.font = _descriptionTextFont;
    descriptionLabel.textColor = _descriptionTextColor;
    descriptionLabel.shadowColor = _descriptionTextShadowColor;
    descriptionLabel.shadowOffset = _descriptionTextShadowOffset;
    descriptionLabel.textAlignment = NSTextAlignmentCenter;
    descriptionLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:descriptionLabel];
}

- (void)setChartData:(NSArray *)data
{
    
    NSDictionary *dic =[NSDictionary dictionaryWithObject:data forKey:@"dic"];
    [NSTimer scheduledTimerWithTimeInterval:0.05
                                                      target:self
                                                    selector:@selector(handleTimer:)
                                                    userInfo:dic repeats:NO];
    
   
}


- (void)handleTimer:(NSTimer*)theTimer {
    
    NSDictionary *dic = (NSDictionary*)[theTimer userInfo];
    NSArray *data = [dic objectForKey:@"dic"];
    
    __block CGFloat yFinilizeValue , xFinilizeValue;
    __block CGFloat yValue , xValue;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.displayAnimated) {
            //            [NSThread sleepForTimeInterval:0.1];
        }
        // update UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            for (PNScatterChartData2 *chartData in data) {
                for (NSUInteger i = 0; i < chartData.itemCount; i++) {
                    yValue = chartData.getData(i).y;
                    xValue = chartData.getData(i).x;
                    NSLog(@"xValue:%f, yValue:%f", xValue, yValue);
                    if (!(xValue >= _AxisX_minValue && xValue <= _AxisX_maxValue) || !(yValue >= _AxisY_minValue && yValue <= _AxisY_maxValue)) {
                        NSLog(@"input is not in correct range.");
                        //                        continue;
                        //                        exit(0);
                    }
                    xFinilizeValue = [self mappingIsForAxisX:true WithValue:xValue];
                    yFinilizeValue = [self mappingIsForAxisX:false WithValue:yValue];
                    CAShapeLayer *shape = [self drawingPointsForChartData:chartData AndWithX:xFinilizeValue AndWithY:yFinilizeValue];
                    self.pathLayer = shape ;
                    [self.layer addSublayer:self.pathLayer];
                    
                    [self addAnimationIfNeeded];
                }
            }
        });
    });
}



// 수축기와 이완기 사이의 라인을 그림.
- (void)setChartLineData:(NSArray *)data outData:(NSArray*)oData
{
    __block CGFloat yValue1, xValue1, yValue2, xValue2;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //            for (PNScatterChartData *chartData in data) {
            for (int j=0; j<[data count]; j++) {
                PNScatterChartData2 *chartData1 = (PNScatterChartData2*)[data objectAtIndex:j];
                PNScatterChartData2 *chartData2 = (PNScatterChartData2*)[oData objectAtIndex:j];
                
                for (NSUInteger i = 0; i < chartData1.itemCount; i++) {
                    yValue1 = chartData1.getData(i).y;
                    xValue1 = chartData1.getData(i).x;
                    
                    yValue2 = chartData2.getData(i).y;
                    xValue2 = chartData2.getData(i).x;
                    
                    
                    //                    if (_beforeX==xValue && _beforeY != yValue) {
                    
                    CAShapeLayer *linesLayer = [CAShapeLayer layer];
                    linesLayer.frame = self.bounds;
                    linesLayer.fillColor = [UIColor blackColor].CGColor;
                    linesLayer.backgroundColor = [UIColor clearColor].CGColor;
                    [linesLayer setStrokeColor:[UIColor blackColor].CGColor];
                    [linesLayer setLineWidth:0.5];
                    
                    float sX = [self mappingIsForAxisX:true WithValue:xValue2];
                    float sY = [self mappingIsForAxisX:false WithValue:yValue2];
                    float eX = [self mappingIsForAxisX:true WithValue:xValue1];
                    float eY = [self mappingIsForAxisX:false WithValue:yValue1];
                    
                    CGMutablePathRef linesPath = CGPathCreateMutable();
                    CGPathMoveToPoint(linesPath, NULL, sX, sY);
                    CGPathAddLineToPoint(linesPath, NULL, eX, eY);
                    
                    linesLayer.path = linesPath;
                    [self.layer addSublayer:linesLayer];
                    //                        _beforeX = -99;
                    //                        _beforeY = -99;
                    //                    }else {
                    //                        _beforeX = xValue;
                    //                        _beforeY = yValue;
                    //                    }
                    
                    //
                    //                    xFinilizeValue = [self mappingIsForAxisX:true WithValue:xValue];
                    //                    yFinilizeValue = [self mappingIsForAxisX:false WithValue:yValue];
                    //                    CAShapeLayer *shape = [self drawingPointsForChartData:chartData AndWithX:xFinilizeValue AndWithY:yFinilizeValue];
                    //                    self.pathLayer = shape ;
                    //                    [self.layer addSublayer:self.pathLayer];
                    
                    //                    [self addAnimationIfNeeded];
                }
            }
        });
    });
}



- (void)setMedicineData:(NSArray *)data
{
    
    NSLog(@"setMedicineData:%@", data);
    __block CGFloat yFinilizeValue , xFinilizeValue;
    __block CGFloat yValue , xValue;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (PNScatterChartData2 *chartData in data) {
                for (NSUInteger i = 0; i < chartData.itemCount; i++) {
                    yValue = chartData.getData(i).y;
                    xValue = chartData.getData(i).x;
                    NSLog(@"xValue:%f, yValue:%f", xValue, yValue);
                    if (!(xValue >= _AxisX_minValue && xValue <= _AxisX_maxValue) || !(yValue >= _AxisY_minValue && yValue <= _AxisY_maxValue)) {
                        NSLog(@"input is not in correct range.");
                        //exit(0);
                    }
                    xFinilizeValue = [self mappingIsForAxisX:true WithValue:xValue];
                    yFinilizeValue = [self mappingIsForAxisX:false WithValue:yValue];
                    CAShapeLayer *shape = [self drawingPointsForDrug:chartData AndWithX:xFinilizeValue AndWithY:yFinilizeValue];
                    self.pathLayer = shape ;
                    [self.layer addSublayer:self.pathLayer];
                    
                    CALayer *shapeIcon = [self drawingPointsForDrugIcon:chartData AndWithX:xFinilizeValue AndWithY:yFinilizeValue];
                    [self.layer addSublayer:shapeIcon];
                    
                    [self addAnimationIfNeeded];
                }
            }
        });
    });
}

- (void)addAnimationIfNeeded{
    
    if (self.displayAnimated) {
        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        pathAnimation.duration = _duration;
        pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        //        pathAnimation.fromValue = @(0.0f);
        //        pathAnimation.toValue = @(0.1f);
        pathAnimation.fillMode = kCAFillModeForwards;
        self.layer.opacity = 1;
        [self.pathLayer addAnimation:pathAnimation forKey:@"fade"];
    }
}

- (CGFloat) mappingIsForAxisX : (BOOL) isForAxisX WithValue : (CGFloat) value{
    
    if (isForAxisX) {
        float temp = _startPointVectorX.x + (_vectorX_Steps / 2) ;
        CGFloat xPos = temp + (((value - _AxisX_minValue)/_AxisX_step) * _vectorX_Steps) ;
        return xPos;
    }
    else {
        float temp = _startPointVectorY.y - (_vectorY_Steps / 2) ;
        CGFloat yPos = temp - (((value - _AxisY_minValue) /_AxisY_step) * _vectorY_Steps);
        return yPos;
    }
    return 0;
}

#pragma mark - Update Chart Data

- (void)updateChartData:(NSArray *)data
{
    _chartData = data;
    
    // will be work in future.
}

#pragma drawing methods

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (_showCoordinateAxis) {
        CGContextSetStrokeColorWithColor(context, [_axisColor CGColor]);
        CGContextSetLineWidth(context, _axisWidth);
        //drawing x vector
        CGContextMoveToPoint(context, _startPoint.x, _startPoint.y);
        CGContextAddLineToPoint(context, _endPointVecotrX.x, _endPointVecotrX.y);
        //drawing y vector
        CGContextMoveToPoint(context, _startPoint.x, _startPoint.y);
        CGContextAddLineToPoint(context, _endPointVecotrY.x, _endPointVecotrY.y);
    }
    
    if (_showLabel) {
        //drawing x steps vector and putting axis x labels
        float temp = _startPointVectorX.x + (_vectorX_Steps / 2) ;
        for (int i = 0; i < _axisX_labels.count; i++) {
            UILabel *lb = [_axisX_labels objectAtIndex:i] ;
            [self showXLabel:lb InPosition:CGPointMake(temp - 15, _startPointVectorX.y + 10 )];
            temp = temp + _vectorX_Steps ;
            
        }
        
        
        
        
        //drawing y steps vector and putting axis x labels
        temp = _startPointVectorY.y - (_vectorY_Steps / 2) ;
        for (int i = 0; i < _axisY_labels.count; i++) {
            UIBezierPath *path = [UIBezierPath bezierPath];
            
            
            [path moveToPoint:CGPointMake(_startPointVectorY.x, temp)];
            [path addLineToPoint:CGPointMake( _endPointVecotrX.x, temp)];
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.path = [path CGPath];
            shapeLayer.strokeColor = [_axisColor CGColor];
            shapeLayer.lineWidth = _axisWidth;
            shapeLayer.fillColor = [_axisColor CGColor];
            [self.verticalLineLayer addObject:shapeLayer];
            if (eattype==0
                || eattype==1
                || eattype==2) {
                [self.layer addSublayer:shapeLayer];
            }
            UILabel *lb = [_axisY_labels objectAtIndex:i];
            [self showXLabel:lb InPosition:CGPointMake(_startPointVectorY.x - 30, temp - 5)];
            
            
            // 기준라인. 120, 80 그림
            if (i==0 && _presuLine) {
                //1.
                CAShapeLayer *linesLayer = [CAShapeLayer layer];
                linesLayer.frame = self.bounds;
                linesLayer.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer.fillColor = COLOR_MAIN_ORANGE.CGColor;
                [linesLayer setStrokeColor:COLOR_MAIN_ORANGE.CGColor];
                [linesLayer setLineWidth:0.5];
                [linesLayer setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:120]);
                CGPathAddLineToPoint(linesPath, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:120]);
                
                linesLayer.path = linesPath;
                [self.layer addSublayer:linesLayer];
                
                
                //2.
                CAShapeLayer *linesLayer2 = [CAShapeLayer layer];
                linesLayer2.frame = self.bounds;
                linesLayer2.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer2.fillColor = COLOR_NATIONS_BLUE.CGColor;
                [linesLayer2 setStrokeColor:COLOR_NATIONS_BLUE.CGColor];
                [linesLayer2 setLineWidth:0.5];
                [linesLayer2 setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath2 = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath2, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:80]);
                CGPathAddLineToPoint(linesPath2, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:80]);
                
                linesLayer2.path = linesPath2;
                [self.layer addSublayer:linesLayer2];
            }else if (i==0 && !_presuLine) {
                /*
                //1.
                CAShapeLayer *linesLayer = [CAShapeLayer layer];
                linesLayer.frame = self.bounds;
                linesLayer.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer.fillColor = [UIColor blueColor].CGColor;
                [linesLayer setStrokeColor:[UIColor blueColor].CGColor];
                [linesLayer setLineWidth:0.5];
                [linesLayer setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:90]);
                CGPathAddLineToPoint(linesPath, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:90]);
                
                linesLayer.path = linesPath;
                [self.layer addSublayer:linesLayer];
                
                
                //2.
                CAShapeLayer *linesLayer2 = [CAShapeLayer layer];
                linesLayer2.frame = self.bounds;
                linesLayer2.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer2.fillColor = PINK_BAR_COLOR.CGColor;
                [linesLayer2 setStrokeColor:PINK_BAR_COLOR.CGColor];
                [linesLayer2 setLineWidth:0.5];
                [linesLayer2 setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath2 = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath2, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:99]);
                CGPathAddLineToPoint(linesPath2, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:99]);
                
                linesLayer2.path = linesPath2;
                [self.layer addSublayer:linesLayer2];
                
                CAShapeLayer *linesLayer3 = [CAShapeLayer layer];
                linesLayer3.frame = self.bounds;
                linesLayer3.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer3.fillColor = [UIColor purpleColor].CGColor;
                [linesLayer3 setStrokeColor:[UIColor purpleColor].CGColor];
                [linesLayer3 setLineWidth:0.5];
                [linesLayer3 setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath3 = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath3, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:120]);
                CGPathAddLineToPoint(linesPath3, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:120]);
                
                linesLayer3.path = linesPath3;
                [self.layer addSublayer:linesLayer3];
                
                
                CAShapeLayer *linesLayer4 = [CAShapeLayer layer];
                linesLayer4.frame = self.bounds;
                linesLayer4.backgroundColor = [UIColor clearColor].CGColor;
                linesLayer4.fillColor = [UIColor redColor].CGColor;
                [linesLayer4 setStrokeColor:[UIColor redColor].CGColor];
                [linesLayer4 setLineWidth:0.5];
                [linesLayer4 setLineDashPattern:
                 [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
                  [NSNumber numberWithInt:5],nil]];
                
                CGMutablePathRef linesPath4 = CGPathCreateMutable();
                CGPathMoveToPoint(linesPath4, NULL, _startPointVectorY.x, [self mappingIsForAxisX:false WithValue:139]);
                CGPathAddLineToPoint(linesPath4, NULL, self.frame.size.width, [self mappingIsForAxisX:false WithValue:139]);
                
                linesLayer4.path = linesPath4;
                [self.layer addSublayer:linesLayer4];
                 */
            }
            
            
            temp = temp - _vectorY_Steps ;
//            식전  / 식후 차트 배경색
//            if (i < _axisY_labels.count-1 && eattype ==1) {
//
//                int ypoint = [lb.text intValue];
//                NSLog(@"ypoint:%d", ypoint);
//
//                float r = 0.0f, g = 0.0f, b = 0.0f;
//                if (ypoint >= 60.0f && 90.0f >= ypoint) {
//
//                    b = 1.0f;
//                    CGRect rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps);
//                    CGContextRef context = UIGraphicsGetCurrentContext();
//                    CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                    CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                    CGContextFillRect(context, rectangle);
//
//                }else if (90 < ypoint && 120  >= ypoint) {
//                    CGRect rectangle;
//                    int minuse = 0;
//                    g = 1.0f;
//                    r = 1.0f;
//                    if (ypoint==120) {
//                        minuse = _vectorY_Steps/2; //125까지만 그림.
//                        rectangle = CGRectMake(_startPointVectorY.x, temp + minuse, _endPointVecotrX.x, _vectorY_Steps - minuse);
//
//                        CGContextRef context = UIGraphicsGetCurrentContext();
//                        CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                        CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                        CGContextFillRect(context, rectangle);
//                    }else{
//                        rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps);
//                        rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps);
//                        CGContextRef context = UIGraphicsGetCurrentContext();
//                        CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                        CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                        CGContextFillRect(context, rectangle);
//                    }
//                }else if (120 < ypoint && 150  >= ypoint){
//
//                    r = 1.0f;
//                    CGRect rectangle;
//                    int minuse = 0;
//                    if (ypoint==130) {
//                        minuse = _vectorY_Steps/2; //125까지만 그림.
//                        rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps + minuse);
//
//                        CGContextRef context = UIGraphicsGetCurrentContext();
//                        CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                        CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                        CGContextFillRect(context, rectangle);
//                    }else{
//                        CGRect rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps);
//                        CGContextRef context = UIGraphicsGetCurrentContext();
//                        CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                        CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                        CGContextFillRect(context, rectangle);
//                    }
//                }
//
//            }else if (i < _axisY_labels.count-1 && eattype ==2) {
//                int lY = [lb.text intValue];
//                float r = 0.0f, g = 0.0f, b = 0.0f;
//                if (60.0f<= lY && 120 >= lY) {
//                    b = 1.0f;
//                }else if (120 < lY && 180 >= lY) {
//                    g = 1.0f;
//                    r = 1.0f;
//                }else{
//
//                    r = 1.0f;
//                }
//                CGRect rectangle = CGRectMake(_startPointVectorY.x, temp, _endPointVecotrX.x, _vectorY_Steps);
//                CGContextRef context = UIGraphicsGetCurrentContext();
//                CGContextSetRGBFillColor(context, r, g, b, 0.1f);
//                CGContextSetRGBStrokeColor(context, r, g, b, 0.1f);
//                CGContextFillRect(context, rectangle);
//            }
        }
    }
    CGContextDrawPath(context, kCGPathStroke);
    
    CGPoint bottomLeft     = CGPointMake(_startPointVectorX.x, _startPointVectorY.x);
    CGPoint bottomRight = CGPointMake(_endPointVecotrX.x, _endPointVecotrY.x);
    CGPoint topLeft        = CGPointMake(_startPointVectorX.x, _startPointVectorY.y);
    CGPoint topRight    = CGPointMake(_endPointVecotrX.x, _startPointVectorY.x);
    
    CAShapeLayer *shapeLay = [CAShapeLayer layer];
    UIBezierPath *path    = [UIBezierPath bezierPath];
    [path moveToPoint:bottomLeft];
    [path addLineToPoint:topLeft];
    [path moveToPoint:bottomLeft];
    [path addLineToPoint:bottomRight];
    [self.layer addSublayer:shapeLay];
}

- (CAShapeLayer*) drawingPointsForChartData : (PNScatterChartData2 *) chartData AndWithX : (CGFloat) X AndWithY : (CGFloat) Y
{
    if (chartData.inflexionPointStyle == PNScatterChartPointStyleCircle) {
        float radius = chartData.size;
        CAShapeLayer *circle = [CAShapeLayer layer];
        // Make a circular shape
        circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(X - radius, Y - radius, 2.0*radius, 2.0*radius)
                                                 cornerRadius:radius].CGPath;
        // Configure the appearence of the circle
        circle.fillColor = [chartData.fillColor CGColor];
        circle.strokeColor = [chartData.strokeColor CGColor];
        circle.lineWidth = 0.4;
        
        // Add to parent layer
        return circle;
    }
    else if (chartData.inflexionPointStyle == PNScatterChartPointStyleSquare) {
        float side = chartData.size;
        CAShapeLayer *square = [CAShapeLayer layer];
        // Make a circular shape
        square.path = [UIBezierPath bezierPathWithRect:CGRectMake(X - (side/2) , Y - (side/2), side, side)].CGPath ;
        // Configure the apperence of the circle
        square.fillColor = [chartData.fillColor CGColor];
        square.strokeColor = [chartData.strokeColor CGColor];
        square.lineWidth = 0.4;
        
        // Add to parent layer
        return square;
    }
    else {
        // you cann add your own scatter chart point here
    }
    return nil ;
}

- (CALayer*) drawingPointsForDrugIcon : (PNScatterChartData2 *) chartData AndWithX:(CGFloat) X AndWithY : (CGFloat) Y
{
    //투약 이미지
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [UIColor clearColor].CGColor;
    sublayer.shadowColor = [UIColor clearColor].CGColor;
    sublayer.borderColor = [UIColor clearColor].CGColor;
    sublayer.frame = CGRectMake(X-5, 5, 10, 12);
    sublayer.contents = (id) [UIImage imageNamed:@"icon_medi.png"].CGImage;
    sublayer.masksToBounds = YES;
    
    return sublayer;
}

- (CAShapeLayer*) drawingPointsForDrug : (PNScatterChartData2 *) chartData AndWithX:(CGFloat) X AndWithY : (CGFloat) Y
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    //    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(X - radius, Y - radius, 2.0*radius, 2.0*radius) cornerRadius:radius].CGPath;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(X, 20)];
    [path addLineToPoint:CGPointMake(X, self.vectorY_Size+10)];
    
    [shapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:3],nil]];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor blueColor] CGColor];
    shapeLayer.lineWidth = 0.4;
    shapeLayer.fillColor = [UIColor redColor].CGColor;
    shapeLayer.strokeColor = [UIColor redColor].CGColor;
    
    
    return shapeLayer;
    
}

- (void) drawLineFromPoint : (CGPoint) startPoint ToPoint : (CGPoint) endPoint WithLineWith : (CGFloat) lineWidth AndWithColor : (UIColor*) color{
    
    // call the same method on a background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.displayAnimated) {
            //            [NSThread sleepForTimeInterval:0.1];
        }
        // calculating start and end point
        __block CGFloat startX = [self mappingIsForAxisX:true WithValue:startPoint.x];
        __block CGFloat startY = [self mappingIsForAxisX:false WithValue:startPoint.y];
        __block CGFloat endX = [self mappingIsForAxisX:true WithValue:endPoint.x];
        __block CGFloat endY = [self mappingIsForAxisX:false WithValue:endPoint.y];
        // update UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            // drawing path between two points
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(startX, startY)];
            [path addLineToPoint:CGPointMake(endX, endY)];
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.path = [path CGPath];
            shapeLayer.strokeColor = [color CGColor];
            shapeLayer.lineWidth = 0.4;
            shapeLayer.fillColor = [color CGColor];
            // adding animation to path
            [self addStrokeEndAnimationIfNeededToLayer:shapeLayer];
            [self.layer addSublayer:shapeLayer];
        });
    });
}

- (void)addStrokeEndAnimationIfNeededToLayer:(CAShapeLayer *)shapeLayer{
    
    if (self.displayAnimated) {
        CABasicAnimation *animateStrokeEnd = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        animateStrokeEnd.duration  = _duration;
        //        animateStrokeEnd.fromValue = [NSNumber numberWithFloat:0.0f];
        //        animateStrokeEnd.toValue   = [NSNumber numberWithFloat:0.01f];
        [shapeLayer addAnimation:animateStrokeEnd forKey:nil];
    }
}

@end
