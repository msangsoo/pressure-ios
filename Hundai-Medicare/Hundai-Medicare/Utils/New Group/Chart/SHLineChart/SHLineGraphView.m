// SHLineGraphView.m
//
// Copyright (c) 2014 Shan Ul Haq (http://grevolution.me)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import "SHLineGraphView.h" 
#import "SHPlot.h"
#import <math.h>
#import <objc/runtime.h>


#define BOTTOM_MARGIN_TO_LEAVE 30.0
#define TOP_MARGIN_TO_LEAVE 30.0
#define INTERVAL_COUNT 10
#define PLOT_WIDTH (self.bounds.size.width - _leftMarginToLeave)

#define kAssociatedPlotObject @"kAssociatedPlotObject"

#define TAG_BUTTON 2000
#define TAG_LINE 3000

@implementation SHLineGraphView
{
  float _leftMarginToLeave;
}
- (instancetype)init {
    if((self = [super init])) {
        [self loadDefaultTheme];
    }
    return self;
}

- (void)awakeFromNib
{
    [self loadDefaultTheme];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadDefaultTheme];
        arrPoint =[[NSMutableArray alloc] init];
    }
    return self;
}

- (void)loadDefaultTheme {
    
    _themeAttributes = @{
                         kXAxisLabelColorKey : [UIColor blackColor],
                         kXAxisLabelFontKey : [UIFont fontWithName:MAIN_FONT_NAME size:10],
                         kXAxisLabelFontKey2 : [UIFont fontWithName:MAIN_FONT_NAME size:10],
                         kYAxisLabelColorKey : [UIColor blackColor],
                         kYAxisLabelFontKey : [UIFont fontWithName:MAIN_FONT_NAME size:10],
                         kYAxisLabelSideMarginsKey : @10,
                         kPlotBackgroundLineColorKey : [UIColor colorWithRed:0.80f green:0.80f blue:0.80f alpha:1.f],
                         kDotSizeKey : @3
                         };
    
}


- (void)addPlot:(SHPlot *)newPlot;
{
  if(nil == newPlot) {
    return;
  }
  
  if(_plots == nil){
    _plots = [NSMutableArray array];
  }
  [_plots addObject:newPlot];
}

- (void)setupTheView:(int)isFu cType:(int)type
{
    isFuture = isFu;
    cType = type;
    for(SHPlot *plot in _plots) {
        if (self != nil) {
            [self drawPlotWithPlot:plot];
        }
    }
}

#pragma mark - Actual Plot Drawing Methods

- (void)drawPlotWithPlot:(SHPlot *)plot {
    //draw y-axis labels. this has to be done first, so that we can determine the left margin to leave according to the
    //y-axis lables.
    
    if (self != nil) {
//        NSLog(@"check = 1");
        [self drawYLabels:plot];
    }
    if (self != nil) {
//        NSLog(@"check = 2");
        //draw x-labels
        [self drawXLabels:plot];
    }
     if (self != nil) {
//         NSLog(@"check = 3");
         //draw the grey lines
         [self drawLines:plot];
     }
    if (self != nil) {
//        NSLog(@"check = 4");
        /*
         actual plot drawing
         */
        [self drawPlot:plot];
    }
    if (self != nil) {
//        NSLog(@"check = 5");
        // 백그라운드 그리기.
        [self drawPlotGuide:plot];
    }
    if (self != nil) {
//        NSLog(@"check = 6");
        // 터치시 풍선말 추가.
        [self setupPopup];
    }
    
    
}

- (int)getIndexForValue:(NSNumber *)value forPlot:(SHPlot *)plot {
  for(int i=0; i< _xAxisValues.count; i++) {
    NSDictionary *d = [_xAxisValues objectAtIndex:i];
    __block BOOL foundValue = NO;
    [d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
      NSNumber *k = (NSNumber *)key;
      if([k doubleValue] == [value doubleValue] ) {
        foundValue = YES;
        *stop = foundValue;
      }
    }];
    if(foundValue){
      return i;
    }
  }
  return -1;
}


//백그라운드 그리기
- (void)drawPlotGuide:(SHPlot *)plot {
    
    if (isFuture == 0) {
        return;
    }
    
    double domax = [_yAxisMaxRange doubleValue];
    double domin = [_yAxisMinRange doubleValue];
    int interval = [self getYintervalMaxValue:domax minValue:domin];
    double yIntervalValue = domax / interval;
    
    double intervalInPx = (self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE) / (interval + 1);
    
    [arrPoint removeAllObjects];
    //logic to fill the graph path, ciricle path, background path.
    [plot.plottingGuideValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (self == nil) {
            return;
        }
        NSDictionary *dic = (NSDictionary *)obj;
        
        __block NSNumber *_key2 = nil;
        __block NSNumber *_value2 = nil;
        
        [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            _key2 = (NSNumber *)key;
            _value2 = (NSNumber *)obj;
        }];
        
        int xIndex = [self getIndexForValue:_key2 forPlot:plot];
        
        
        double h = self.bounds.size.height;
        //x value
        double height = h - BOTTOM_MARGIN_TO_LEAVE;
        
        double y2 = (((height -intervalInPx) * (domax - [_value2 doubleValue])) / (domax-domin))+ intervalInPx;
        
        (plot.xPoints[xIndex]).x = ceil((plot.xPoints[xIndex]).x);
        (plot.xPoints[xIndex]).y = ceil(y2);
        
        NSString *xVal = [NSString stringWithFormat:@"%f", ceil((plot.xPoints[xIndex]).x)];
        NSString *yVal = [NSString stringWithFormat:@"%f", ceil(y2)];
        [self->arrPoint addObject:[NSDictionary dictionaryWithObjectsAndKeys:xVal, @"x", yVal, @"y", nil]];
    }];
    
    double xIntervalInPx = PLOT_WIDTH / _xAxisValues.count;
    
    if (isFuture == 1) {
        // +-------------------------------------------------
        // 40주 일때
        // +-------------------------------------------------
        CAShapeLayer *linesLayer = [CAShapeLayer layer];
        linesLayer.frame = self.bounds;
        linesLayer.fillColor = [UIColor clearColor].CGColor;
        linesLayer.backgroundColor = [UIColor clearColor].CGColor;
        linesLayer.strokeColor = ((UIColor *)_themeAttributes[kPlotBackgroundLineColorKey]).CGColor;
        linesLayer.lineWidth = 0.4;
        
        
        CGMutablePathRef linesPath = CGPathCreateMutable();
        for(int i=0; i < 40; i++){
            CGPoint currentLabelPoint = CGPointMake((xIntervalInPx * i) + _leftMarginToLeave, self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE);
            
            CGPathMoveToPoint(linesPath, NULL, currentLabelPoint.x, currentLabelPoint.y);
            if (i==15 || i==27) {
                CGPathAddLineToPoint(linesPath, NULL, currentLabelPoint.x, 20);
            }
        }
        linesLayer.path = linesPath;
        [self.layer addSublayer:linesLayer];
        
        if (_xAxisValues != nil ) {
            
            CAShapeLayer *futLayer = [CAShapeLayer layer];
            futLayer.fillColor = RGBA(251, 138, 211, 0.5).CGColor;
            futLayer.backgroundColor = RGBA(251, 138, 211, 0.5).CGColor;
            [futLayer setStrokeColor:RGBA(251, 138, 211, 0.5).CGColor];
            
            float valx1 = [[[arrPoint objectAtIndex:0] objectForKey:@"x"] floatValue];
            float valy1 = [[[arrPoint objectAtIndex:0] objectForKey:@"y"] floatValue];
            
            float valx2 = [[[arrPoint objectAtIndex:1] objectForKey:@"x"] floatValue];
            float valy2 = [[[arrPoint objectAtIndex:1] objectForKey:@"y"] floatValue];
            
            float valx3 = [[[arrPoint objectAtIndex:2] objectForKey:@"x"] floatValue];
            float valy3 = [[[arrPoint objectAtIndex:2] objectForKey:@"y"] floatValue];
            
            float valx4 = [[[arrPoint objectAtIndex:3] objectForKey:@"x"] floatValue];
            float valy4 = [[[arrPoint objectAtIndex:3] objectForKey:@"y"] floatValue];
            
            float valx5 = [[[arrPoint objectAtIndex:4] objectForKey:@"x"] floatValue];
            float valy5 = [[[arrPoint objectAtIndex:4] objectForKey:@"y"] floatValue];
            
            float valx6 = [[[arrPoint objectAtIndex:5] objectForKey:@"x"] floatValue];
            float valy6 = [[[arrPoint objectAtIndex:5] objectForKey:@"y"] floatValue];
            
            float valx7 = [[[arrPoint objectAtIndex:6] objectForKey:@"x"] floatValue];
            float valy7 = [[[arrPoint objectAtIndex:6] objectForKey:@"y"] floatValue];
            
            CGMutablePathRef backPath = CGPathCreateMutable();
            CGPathMoveToPoint(backPath, NULL, _leftMarginToLeave, valy1); //  임신전 몸무게
            CGPathAddLineToPoint(backPath, NULL, valx2, valy2);
            CGPathAddLineToPoint(backPath, NULL, valx3, valy3);
            CGPathAddLineToPoint(backPath, NULL, valx4, valy4);
            CGPathAddLineToPoint(backPath, NULL, valx5, valy5);
            CGPathAddLineToPoint(backPath, NULL, valx6, valy6);
            CGPathAddLineToPoint(backPath, NULL, valx7, valy7);
            CGPathAddLineToPoint(backPath, NULL, _leftMarginToLeave, valy1); // 임신전 몸무게
            
            futLayer.path = backPath;
            
            [self.layer addSublayer:futLayer];
        }
        
    }else if (isFuture == 2) {
        // +-------------------------------------------------
        // 빅데이터 예측
        // +-------------------------------------------------
        
        if (_xAxisValues != nil ) {
            
            CAShapeLayer *futLayer = [CAShapeLayer layer];
            futLayer.fillColor = RGBA(251, 138, 211, 0.5).CGColor;
            futLayer.backgroundColor = RGBA(251, 138, 211, 0.5).CGColor;
            [futLayer setStrokeColor:RGBA(251, 138, 211, 0.5).CGColor];
            
            
            CGMutablePathRef backPath = CGPathCreateMutable();
            CGPathMoveToPoint(backPath, NULL, _leftMarginToLeave, 0);
            
            float x = [[[arrPoint objectAtIndex:0] objectForKey:@"x"] floatValue];
            float y = [[[arrPoint objectAtIndex:0] objectForKey:@"y"] floatValue];
            CGPathMoveToPoint(backPath, NULL, x, y);
            
            for(int i=1; i < [arrPoint count]; i++){
                float x = [[[arrPoint objectAtIndex:i] objectForKey:@"x"] floatValue];
                float y = [[[arrPoint objectAtIndex:i] objectForKey:@"y"] floatValue];
                CGPathAddLineToPoint(backPath, NULL, x, y);
            }
            
            futLayer.path = backPath;
            
            [self.layer addSublayer:futLayer];
        }
    }
    
}


- (void)drawPlot:(SHPlot *)plot {
    
    NSDictionary *theme = plot.plotThemeAttributes;
    //
    CAShapeLayer *backgroundLayer = [CAShapeLayer layer];
    backgroundLayer.frame = self.bounds;
    backgroundLayer.fillColor = [UIColor clearColor].CGColor;
    backgroundLayer.backgroundColor = [UIColor clearColor].CGColor;
    [backgroundLayer setStrokeColor:[UIColor clearColor].CGColor];
    [backgroundLayer setLineWidth:((NSNumber *)theme[kPlotStrokeWidthKey]).intValue];
    
    CGMutablePathRef backgroundPath = CGPathCreateMutable();
    
    //
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    circleLayer.frame = self.bounds;
    circleLayer.fillColor = ((UIColor *)theme[kPlotPointFillColorKey]).CGColor;
    circleLayer.backgroundColor = [UIColor clearColor].CGColor;
    [circleLayer setStrokeColor:((UIColor *)theme[kPlotPointFillColorKey]).CGColor];
    [circleLayer setLineWidth:((NSNumber *)theme[kPlotStrokeWidthKey]).intValue];
    
    CGMutablePathRef circlePath = CGPathCreateMutable();
    
    //
    CAShapeLayer *graphLayer = [CAShapeLayer layer];
    graphLayer.frame = self.bounds;
    graphLayer.fillColor = [UIColor clearColor].CGColor;
    graphLayer.backgroundColor = [UIColor clearColor].CGColor;
    [graphLayer setStrokeColor:((UIColor *)theme[kPlotStrokeColorKey]).CGColor];
    [graphLayer setLineWidth:((NSNumber *)theme[kPlotStrokeWidthKey]).intValue/3];

//    104, 180, 49
    
    CGMutablePathRef graphPath = CGPathCreateMutable();
    
    double domax = [_yAxisMaxRange doubleValue];
    double domin = [_yAxisMinRange doubleValue];
    int interval = [self getYintervalMaxValue:domax minValue:domin];
    double yIntervalValue = domax / interval;
    
    double intervalInPx = (self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE) / (interval + 1);
    //logic to fill the graph path, ciricle path, background path.
    [plot.plottingValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSDictionary *dic = (NSDictionary *)obj;
        
        __block NSNumber *_key = nil;
        __block NSNumber *_value = nil;
        
        [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            _key = (NSNumber *)key;
            _value = (NSNumber *)obj;
        }];
        
        int xIndex = [self getIndexForValue:_key forPlot:plot];
        
        double h = self.bounds.size.height;
        //x value
        double height = h - BOTTOM_MARGIN_TO_LEAVE;

        double y2 = (((height -intervalInPx) * (domax - [_value doubleValue])) / (domax-domin))+ intervalInPx;

        (plot.xPoints[xIndex]).x = ceil((plot.xPoints[xIndex]).x);
        (plot.xPoints[xIndex]).y = ceil(y2);
    }];
    
    //move to initial point for path and background.
    CGPathMoveToPoint(graphPath, NULL, _leftMarginToLeave, plot.xPoints[0].y);
    CGPathMoveToPoint(backgroundPath, NULL, _leftMarginToLeave, plot.xPoints[0].y);
    
    if (sizeof(plot.xPoints) > 0) {
        CGPoint pointF = plot.xPoints[0];
        CGPathMoveToPoint(graphPath, NULL, pointF.x, pointF.y);
    }
    
    NSInteger count = _xAxisValues.count;
    int l =0;
    for(int i=0; i< count; i++){
        
        CGPoint point = plot.xPoints[i];
        if (l==0) {
            if (point.y > 0) {
                CGPathMoveToPoint(graphPath, NULL, point.x, point.y);
                l++;
            }
        }
        if (point.y > 0) {
            
            CGPathAddLineToPoint(graphPath, NULL, point.x, point.y);
            CGPathAddLineToPoint(backgroundPath, NULL, point.x, point.y);
            CGFloat dotsSize = [_themeAttributes[kDotSizeKey] floatValue];
            CGPathAddEllipseInRect(circlePath, NULL, CGRectMake(point.x - dotsSize/2.0f, point.y - dotsSize/2.0f, dotsSize, dotsSize));
        }
    }
    
    
    //move to initial point for path and background.
//    CGPathAddLineToPoint(graphPath, NULL, _leftMarginToLeave + PLOT_WIDTH, plot.xPoints[count -1].y);
//    CGPathAddLineToPoint(backgroundPath, NULL, _leftMarginToLeave + PLOT_WIDTH, plot.xPoints[count - 1].y);
    
    //additional points for background.
    CGPathAddLineToPoint(backgroundPath, NULL, _leftMarginToLeave + PLOT_WIDTH, self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE);
    CGPathAddLineToPoint(backgroundPath, NULL, _leftMarginToLeave, self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE);
    CGPathCloseSubpath(backgroundPath);
    
    backgroundLayer.path = backgroundPath;
    graphLayer.path = graphPath;
    circleLayer.path = circlePath;
    
    //animation
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
//    animation.duration = 0.5;
//    animation.fromValue = @(0.0);
//    animation.toValue = @(0.5);
//    [graphLayer addAnimation:animation forKey:@"strokeEnd"];
    
    backgroundLayer.zPosition = 0;
    graphLayer.zPosition = 1;
    circleLayer.zPosition = 2;
    
    [self.layer addSublayer:graphLayer];
    
    if (isFuture != 2) {
        [self.layer addSublayer:circleLayer];
    }
    [self.layer addSublayer:backgroundLayer];
    
    for(int i=0; i< _xAxisValues.count+40; i++){
        UIView *v = [self viewWithTag:TAG_BUTTON + i];
        [v removeFromSuperview];
    }
    
    long column = (self.frame.size.width-60)/ _xAxisValues.count;
    for(int i=0; i< _xAxisValues.count; i++){
        
        NSDictionary *dic = [_xAxisValues objectAtIndex:i];
        
        __block NSNumber *_key = nil;
        [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            _key = key;
        }];
        
        CGPoint point = plot.xPoints[i];
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(point.x - column/2, 0, column, self.frame.size.height)];
//        TAG_BUTTON;
        v.tag = TAG_BUTTON + i;
//        [v setBackgroundColor:[UIColor redColor]];
        [self addSubview:v];
    }

    
    NSMutableArray *aText = [[NSMutableArray alloc] init];
    for(int i=0; i< [plot.plottingValues count]; i++){
        
        NSString *keyName = @"";
        
        for (NSString *key in [plot.plottingValues  objectAtIndex:i]) {
 
            keyName = @"";
            NSString *value = [[plot.plottingValues  objectAtIndex:i] objectForKey:key];
            NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:key, @"key", value, @"value", nil];
            [aText addObject:dic];
        }
    }
    
    if (self.valueVisible) {
        int j = 0;
        for(int i=0; i< _xAxisValues.count; i++){
            CGPoint point = plot.xPoints[i];
            
            if (point.y >0) {
                NSDictionary *dic = [aText objectAtIndex:j];
                NSString *value = [dic objectForKey:@"value"];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(point.x -14, point.y +4, 28, 15)];
                lbl.backgroundColor = [UIColor clearColor];
                lbl.textAlignment = NSTextAlignmentCenter;
                lbl.textColor = [UIColor darkTextColor];
                [lbl setText:value];
                [lbl setFont:[UIFont fontWithName:MAIN_FONT_NAME size:8]];
                objc_setAssociatedObject(lbl, kAssociatedPlotObject, plot, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                [self addSubview:lbl];
                
                j ++;
            }
        }
    }
    
    // 빅데이터 3군데 점찍기, 숫자찍기.
    if (isFuture == 2) {
        for(int i=0; i< _xAxisValues.count; i++){
            
            NSDictionary *dic = [_xAxisValues objectAtIndex:i];
            
            __block NSNumber *_key = nil;
            __block NSNumber *_value = nil;
            [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                _key = key;
                _value = obj;
            }];
            
            if (i==0) {
                
                CGPoint point = plot.xPoints[i];
                
                CGFloat dotsSize = 8.0f;
                UIView *dotV = [[UIView alloc] initWithFrame:CGRectMake(point.x - dotsSize/2.0f, point.y - dotsSize/2.0f, dotsSize, dotsSize)];
                [dotV setBackgroundColor:RGBA(104,180,49, 1)];
                dotV.layer.cornerRadius = 4; // this value vary as per your desire
                dotV.clipsToBounds = YES;
                [self addSubview:dotV];
                
                NSString *value = [[plot.plottingValues  objectAtIndex:i] objectForKey:[NSString stringWithFormat:@"%@",_key]];
                
                UILabel *v = [[UILabel alloc] initWithFrame:CGRectMake(point.x - dotsSize/2.0f, point.y-24, 30, 15)];
                v.font = [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
                v.text = value;
                [self addSubview:v];
            }
           else  if ( i == (int)_xAxisValues.count/2) {
               
               CGPoint point = plot.xPoints[i];
               
               CGFloat dotsSize = 8.0f;
               UIView *dotV = [[UIView alloc] initWithFrame:CGRectMake(point.x - dotsSize/2.0f, point.y - dotsSize/2.0f, dotsSize, dotsSize)];
               [dotV setBackgroundColor:RGBA(104,180,49, 1)];
               dotV.layer.cornerRadius = 4; // this value vary as per your desire
               dotV.clipsToBounds = YES;
               [self addSubview:dotV];
               
               NSString *value = [[plot.plottingValues  objectAtIndex:i] objectForKey:[NSString stringWithFormat:@"%@",_key]];
               
                UILabel *v = [[UILabel alloc] initWithFrame:CGRectMake(point.x - dotsSize/2.0f, point.y-24, 30, 15)];
                v.font = [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
                v.text = value;
                [self addSubview:v];
            }
            else if (i == _xAxisValues.count-1) {
                
                CGPoint point = plot.xPoints[i];
                
                CGFloat dotsSize = 8.0f;
                UIView *dotV = [[UIView alloc] initWithFrame:CGRectMake(point.x - dotsSize/2.0f, point.y - dotsSize/2.0f, dotsSize, dotsSize)];
                [dotV setBackgroundColor:RGBA(104,180,49, 1)];
                dotV.layer.cornerRadius = 4; // this value vary as per your desire
                dotV.clipsToBounds = YES;
                [self addSubview:dotV];
                
                NSString *value = [[plot.plottingValues  objectAtIndex:i] objectForKey:[NSString stringWithFormat:@"%@",_key]];
                UILabel *v = [[UILabel alloc] initWithFrame:CGRectMake((point.x - dotsSize/2.0f)-10, point.y-24, 30, 15)];
                v.font = [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
                v.text = value;
                [self addSubview:v];
            }
            
        }
    }
    
    
    [self setupVirtualTouchBar:plot];
}


// X 라벨찍기
- (void)drawXLabels:(SHPlot *)plot {
    
    int xIntervalCount = _xAxisValues.count;
    
    double xIntervalInPx = PLOT_WIDTH / _xAxisValues.count;
    double termWidth = 0.0f;
    if(xIntervalCount==40) termWidth =5;
    //initialize actual x points values where the circle will be
    plot.xPoints = calloc(sizeof(CGPoint), xIntervalCount);
    
    for(int i=0; i < xIntervalCount; i++){
        CGPoint currentLabelPoint = CGPointMake((xIntervalInPx * i) + _leftMarginToLeave, self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE);
        CGRect xLabelFrame = CGRectMake(currentLabelPoint.x , currentLabelPoint.y, xIntervalInPx+termWidth, BOTTOM_MARGIN_TO_LEAVE);
        
        plot.xPoints[i] = CGPointMake((int) xLabelFrame.origin.x + (xLabelFrame.size.width /2) , (int) 0);
        
        
        UILabel *xAxisLabel = [[UILabel alloc] initWithFrame:xLabelFrame];
        xAxisLabel.backgroundColor = [UIColor clearColor];
        xAxisLabel.numberOfLines = 1;
        xAxisLabel.minimumFontSize = 7;
        xAxisLabel.adjustsFontSizeToFitWidth = YES;
        xAxisLabel.font = (UIFont *)_themeAttributes[kXAxisLabelFontKey];
        
        xAxisLabel.textColor = (UIColor *)_themeAttributes[kXAxisLabelColorKey];
        xAxisLabel.textAlignment = NSTextAlignmentCenter;
        
        NSDictionary *dic = [_xAxisValues objectAtIndex:i];
        __block NSString *xLabel = nil;
        [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            xLabel = (NSString *)obj;
        }];
        
        xAxisLabel.text = [NSString stringWithFormat:@"%@", xLabel];
        if (xIntervalCount==40) {
            // 40주일때는 짝수만 그림
            if (i%2!=0) {
                [self addSubview:xAxisLabel];
            }
        }else{
            [self addSubview:xAxisLabel];
        }
        
        if (i == xIntervalCount-1) {
            xLabelFrame = CGRectMake(xLabelFrame.origin.x + 12,  xLabelFrame.origin.y-2, xLabelFrame.size.width+3, xLabelFrame.size.height+3);
            UILabel *xAxisLabel = [[UILabel alloc] initWithFrame:xLabelFrame];
            xAxisLabel.backgroundColor = [UIColor clearColor];
            xAxisLabel.numberOfLines = 1;
//            xAxisLabel.minimumFontSize = 7;
            xAxisLabel.adjustsFontSizeToFitWidth = YES;
            xAxisLabel.font = (UIFont *)_themeAttributes[kXAxisLabelFontKey2];
            
            xAxisLabel.textColor = (UIColor *)_themeAttributes[kXAxisLabelColorKey];
            xAxisLabel.textAlignment = NSTextAlignmentCenter;
            
            if (cType==10) {
                xAxisLabel.text = @"시";
            }else if (cType==11) {
                xAxisLabel.text = @"";
            }else if (cType==12) {
                xAxisLabel.text = @"(일)";
            }else if (cType==13) {
                if (isFuture) {
                    xAxisLabel.text = @"주";
                }else{
                    xAxisLabel.text = @"(월)";
                }
            }
            [self addSubview:xAxisLabel];
        }
    }
}


// Y라벨찍기
- (void)drawYLabels:(SHPlot *)plot {
    double domax = [_yAxisMaxRange doubleValue];
    double domin = [_yAxisMinRange doubleValue];
    
    int interval = [self getYintervalMaxValue:domax minValue:domin];
    
    double yIntervalValue = domax / interval;
    double intervalInPx = (self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE) / (interval + 1);
    
    NSMutableArray *labelArray = [NSMutableArray array];
    float maxWidth = 0;
    
    for(int i= interval + 1; i >= 0; i--){
        CGPoint currentLinePoint = CGPointMake(_leftMarginToLeave, i * intervalInPx);
        CGRect lableFrame = CGRectMake(0, currentLinePoint.y - (intervalInPx / 2), 100, intervalInPx + 20);
        
        if(i != 0) {
            UILabel *yAxisLabel = [[UILabel alloc] initWithFrame:lableFrame];
            yAxisLabel.backgroundColor = [UIColor clearColor];
            yAxisLabel.font = (UIFont *)_themeAttributes[kYAxisLabelFontKey];
            yAxisLabel.textColor = (UIColor *)_themeAttributes[kYAxisLabelColorKey];
            yAxisLabel.textAlignment = NSTextAlignmentCenter;
            
//            float val = (yIntervalValue * (interval - (i-1)));
            float val  = domax - (i-1);
            yAxisLabel.text = [NSString stringWithFormat:@"%.0f", val];
            
          
            [yAxisLabel sizeToFit];
            CGRect newLabelFrame = CGRectMake(0, currentLinePoint.y - (yAxisLabel.layer.frame.size.height / 2), yAxisLabel.frame.size.width, yAxisLabel.layer.frame.size.height + 5);
            yAxisLabel.frame = newLabelFrame;
            
            if(newLabelFrame.size.width > maxWidth) {
                maxWidth = newLabelFrame.size.width;
            }
            
            [labelArray addObject:yAxisLabel];
            [self addSubview:yAxisLabel];
        }else {
            UILabel *yUnitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lableFrame.origin.y-5, 20, 20)];
            yUnitLabel.backgroundColor = [UIColor clearColor];
            yUnitLabel.font = (UIFont *)_themeAttributes[kYAxisLabelFontKey];
            yUnitLabel.textColor = (UIColor *)_themeAttributes[kYAxisLabelColorKey];
            yUnitLabel.textAlignment = NSTextAlignmentCenter;
            yUnitLabel.text = [NSString stringWithFormat:@"%@", _yAxisSuffix];
            
            [self addSubview:yUnitLabel];
        }
    }
    
    _leftMarginToLeave = maxWidth + [_themeAttributes[kYAxisLabelSideMarginsKey] doubleValue];
    
    for( UILabel *l in labelArray) {
        CGSize newSize = CGSizeMake(_leftMarginToLeave, l.frame.size.height);
        CGRect newFrame = l.frame;
        newFrame.size = newSize;
        l.frame = newFrame;
    }
}

// 라인그리기.
- (void)drawLines:(SHPlot *)plot {

    CAShapeLayer *linesLayer = [CAShapeLayer layer];
    linesLayer.frame = self.bounds;
    linesLayer.fillColor = [UIColor clearColor].CGColor;
    linesLayer.backgroundColor = [UIColor clearColor].CGColor;
    linesLayer.strokeColor = ((UIColor *)_themeAttributes[kPlotBackgroundLineColorKey]).CGColor;
    linesLayer.lineWidth = 0.4;
    
    double domax = [_yAxisMaxRange doubleValue];
    double domin = [_yAxisMinRange doubleValue];
    int interval = [self getYintervalMaxValue:domax minValue:domin];
    
    CGMutablePathRef linesPath = CGPathCreateMutable();
    CGMutablePathRef cirPath = CGPathCreateMutable();
    
    double intervalInPx = (self.bounds.size.height - BOTTOM_MARGIN_TO_LEAVE) / (interval + 1);
    for(int i= interval + 1; i > 0; i--){
        
        CGPoint currentLinePoint = CGPointMake(_leftMarginToLeave, (i * intervalInPx));
        
        // 가로라인그리기.
        if (i==interval +1) {
            CGPathMoveToPoint(linesPath, NULL, currentLinePoint.x, currentLinePoint.y);
            CGPathAddLineToPoint(linesPath, NULL, currentLinePoint.x, (currentLinePoint.y - (self.bounds.size.height-(self.bounds.size.height/7)))+17   );
            
        }
        
        CGPathMoveToPoint(linesPath, NULL, currentLinePoint.x, currentLinePoint.y);
        CGPathAddLineToPoint(linesPath, NULL, currentLinePoint.x + PLOT_WIDTH, currentLinePoint.y);
    }
    
    linesLayer.path = linesPath;
    [self.layer addSublayer:linesLayer];
}


- (int)getYintervalMaxValue:(NSInteger)maxValue
                   minValue:(NSInteger)minValue {
    
   
    int max = (int) maxValue;
    int min = (int) minValue;
    
    int r = max - min;
    
    return r;
     /*
    if (r <= 50) {
        return (int)r / 5;
    }else if (r <= 100) {
        return (int)r / 10;
    }else if(r <= 150){
        return (int)r / 15;
    }else if(r <= 200){
        return (int)r / 20;
    }else if(r <= 250){
        return (int)r / 25;
    }else{
        return (int)r / 30;
    }
     */
}

#pragma mark - UIButton event methods



#pragma mark - Theme Key Extern Keys

NSString *const kXAxisLabelColorKey         = @"kXAxisLabelColorKey";
NSString *const kXAxisLabelFontKey          = @"kXAxisLabelFontKey";
NSString *const kXAxisLabelFontKey2          = @"kXAxisLabelFontKey2";
NSString *const kYAxisLabelColorKey         = @"kYAxisLabelColorKey";
NSString *const kYAxisLabelFontKey          = @"kYAxisLabelFontKey";
NSString *const kYAxisLabelSideMarginsKey   = @"kYAxisLabelSideMarginsKey";
NSString *const kPlotBackgroundLineColorKey = @"kPlotBackgroundLineColorKey";
NSString *const kDotSizeKey                 = @"kDotSizeKey";




#pragma mark 팝업 레이어
- (void)setupPopup {
    
    UIColor *popColor = RGB(96, 123, 227);
    
    vBoardBack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 28)];
    [vBoardBack setBackgroundColor:RGBA(96, 123, 227, 1)];
    vBoard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, vBoardBack.frame.size.width, 28)];
    vBoardBack.backgroundColor = [UIColor clearColor];
    vBoard.backgroundColor = popColor;
    vLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, 0, vBoard.frame.size.width-6, vBoard.frame.size.height)];
    vLabel.font = [UIFont fontWithName:MAIN_FONT_NAME size:12.0];
    [vLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    vLabel.textColor = [UIColor whiteColor];
    vLabel.numberOfLines = 1;
    vLabel.minimumScaleFactor = 0.5;
    vLabel.adjustsFontSizeToFitWidth = YES;
    vLabel.textAlignment = NSTextAlignmentCenter;
    vBoard.layer.cornerRadius = 5;
    vBoard.layer.masksToBounds = true;
    
    [vBoard addSubview:vLabel];
    
    UIView *pLine = [[UIView alloc] initWithFrame:CGRectMake(vBoardBack.frame.size.width/2, 0, 1, vBoardBack.frame.size.height)];
    pLine.backgroundColor = popColor;
    
      dispatch_async(dispatch_get_main_queue(), ^{
          [self->vBoardBack addSubview:pLine];
          [self->vBoardBack addSubview:self->vBoard];
          [self addSubview:self->vBoardBack];
          [self bringSubviewToFront:self->vBoardBack];
          self->vBoardBack.hidden = YES;
      });
    
}

- (void)setupVirtualTouchBar:(SHPlot *)plot
{
    CGMutablePathRef graphPath = CGPathCreateMutable();
    
    NSInteger count = _xAxisValues.count;
    int l =0;
    for(int i=0; i< count; i++){
        
        CGPoint point = plot.xPoints[i];
        if (l==0) {
            if (point.y > 0) {
                CGPathMoveToPoint(graphPath, NULL, point.x, point.y);
                l++;
            }
        }
        if (point.y > 0) {
            
            UIView *vLine =[[UIView alloc] initWithFrame:CGRectMake(point.x, 0, 1, point.y)];
            vLine.backgroundColor = RGBA(96, 123, 227, 0.5);
            vLine.tag = TAG_LINE + i;
            [self addSubview:vLine];
            [self sendSubviewToBack:vLine];
            vLine.hidden = YES;
        }
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if (isFuture == 2) {
        return;
    }
    [self hidePopView];
    
    UITouch *touch= [touches anyObject];
    UIView * v = [touch view];
    
    long tag = v.tag;
    
    for(SHPlot *plot in _plots) {
        
        [plot.plottingValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSDictionary *dic = (NSDictionary *)obj;
            
            __block NSNumber *_key = nil;
            __block NSNumber *_value = nil;
            
            [dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                _key = (NSNumber *)key;
                _value = (NSNumber *)obj;
                
                int kk = [_key intValue]-1;
                if (kk == tag - TAG_BUTTON) {
                    
                    if (_value > 0) {
                        
                        // line visible
                        UIView *v = [self viewWithTag:tag+1000];
                        v.hidden = NO;
                        // pop visible
                        self->vBoardBack.hidden = NO;
                        self->vBoardBack.center = v.center;
                        self->vBoardBack.frame = CGRectMake(self->vBoardBack.frame.origin.x, 0, self->vBoardBack.frame.size.width, self->vBoardBack.frame.size.height);
                        
                        self->vLabel.text = [NSString stringWithFormat:@"%.2f%@", [_value floatValue], @"kg"];
                        
                    }
                }
            }];
            
            
        }];
        
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if (isFuture == 2) {
        return;
    }
    [self hidePopView];
    
    UITouch *touch= [touches anyObject];
    
    CGPoint pt = [touch locationInView:self];
    float currX = pt.x;
    NSLog(@"%f", pt.x);
    long column = (self.frame.size.width-60)/ _xAxisValues.count;
    for(SHPlot *plot in _plots) {
        
        NSInteger count = _xAxisValues.count;
        for(int i=0; i< count; i++){
            
            CGPoint point = plot.xPoints[i];
            if (point.x  <= currX &&  currX <= point.x+column) {
                if (point.y > 0) {
                    NSLog(@"point.x::%f", point.x);
                    
                    UIView *v = [self viewWithTag:i+TAG_BUTTON];
                    NSString *value = @"";
                    NSString *key = [NSString stringWithFormat:@"%d", i+1];
                    for (int k=0; k < [plot.plottingValues count]; k++) {
                        
                        if([[plot.plottingValues objectAtIndex:k] objectForKey:key]){
                            value = [[plot.plottingValues objectAtIndex:k] objectForKey:key];
                            continue;
                        }
                    }
                    if([value isEqualToString:@""] || [value length]==0){
                        return;
                    }
                    
                    UIView *vLine = [self viewWithTag:i+TAG_LINE];
                    vLine.hidden = NO;
                            
                    // pop visible
                    self->vBoardBack.hidden = NO;
                    self->vBoardBack.center = v.center;
                    self->vBoardBack.frame = CGRectMake(self->vBoardBack.frame.origin.x, 0, self->vBoardBack.frame.size.width, self->vBoardBack.frame.size.height);
                    
                    self->vLabel.text = [NSString stringWithFormat:@"%.2f%@", [value floatValue], @"kg"];
                    break;
                }
            }
            
        }
        
    }
    
    
    
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"touchesEnded");
}

- (void)hidePopView {
    for (int i=0; i < 40; i++) {
        UIView *v = [self viewWithTag:i+TAG_LINE];
        if(v){
            v.hidden = YES;
        }
    }
    vBoardBack.hidden = YES;
}

@end
