//
//  SimpleBarChart.m
//  YouLogReading
//
//  Created by Mohammed Islam on 9/18/13.
//  Copyright (c) 2013 KSI Technology, LLC. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

#import "SimpleBarChart.h"
#define TAG_BAR     2000
@interface SimpleBarChart ()

@end

@implementation SimpleBarChart

@synthesize
delegate	= _delegate,
dataSource	= _dataSource;

- (id)initWithFrame:(CGRect)frame
{
    if (!(self = [super initWithFrame:frame]))
        return self;
    
    self.animationDuration	= 0.0;
    self.hasGrids			= YES;
    self.incrementValue		= 0.0f;
    self.barWidth			= 10.0;
    self.barAlpha			= 1.0;
    self.chartBorderColor	= [UIColor blackColor];
    self.gridColor			= [UIColor grayColor];
    self.hasYLabels			= YES;
    self.yLabelFont			= [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
    self.yLabelColor		= [UIColor blackColor];
    self.yLabelType			= SimpleBarChartYAxisLabelTypeInteger;
    self.xLabelFont			= [UIFont fontWithName:MAIN_FONT_NAME size:10.0];
    self.xLabelColor		= [UIColor blackColor];
    self.xLabelType			= SimpleBarChartXLabelTypeVerticle;
    self.barTextFont		= [UIFont fontWithName:MAIN_FONT_NAME size:9.0];
    self.barTextColor		= [UIColor grayColor];
    self.barTextType		= SimpleBarChartBarTextTypeTop;
    
    _barPathLayers			= [[NSMutableArray alloc] init];
    _barHeights				= [[NSMutableArray alloc] init];
    _barLabels				= [[NSMutableArray alloc] init];
    _barTexts                = [[NSMutableArray alloc] init];
    _barManual                = [[NSMutableArray alloc] init];
    
    // Grid
    _gridLayer				= [CALayer layer];
    [self.layer addSublayer:_gridLayer];
    
    _barLayer				= [CALayer layer];
    [self.layer addSublayer:_barLayer];
    
    _borderLayer			= [CALayer layer];
    [self.layer addSublayer:_borderLayer];
    
    _yLabelView				= [[UIView alloc] init];
    _yLabelView.alpha		= 1.0;
    [self addSubview:_yLabelView];
    
    _xLabelView				= [[UIView alloc] init];
    _xLabelView.alpha		= 1.0;
    [self addSubview:_xLabelView];
    
    _barTextView			= [[UIView alloc] init];
    _barTextView.alpha		= 1.0;
    [self addSubview:_barTextView];
    
    
    
    return self;
}


- (void)reloadData
{
    if (_dataSource)
    {
        // Collect some data
        _numberOfBars = [_dataSource numberOfBarsInBarChart:self];
        [_barHeights removeAllObjects];
        [_barLabels removeAllObjects];
        [_barTexts removeAllObjects];
        [_barManual removeAllObjects];
        
        for (NSInteger i = 0; i < _numberOfBars; i++)
        {
            [_barHeights addObject:[NSNumber numberWithFloat:[_dataSource barChart:self valueForBarAtIndex:i]]];
            
            if (_dataSource && [_dataSource respondsToSelector:@selector(barChart:xLabelForBarAtIndex:)])
                [_barLabels addObject:[_dataSource barChart:self xLabelForBarAtIndex:i]];
            
            if (_dataSource && [_dataSource respondsToSelector:@selector(barChart:textForBarAtIndex:)])
                [_barTexts addObject:[_dataSource barChart:self textForBarAtIndex:i]];
            
            if (_dataSource && [_dataSource respondsToSelector:@selector(barChart:manualTextForBarAtIndex:)])
                [_barManual addObject:[_dataSource barChart:self manualTextForBarAtIndex:i]];
        }
        
        _maxHeight            = [_barHeights valueForKeyPath:@"@max.self"];
        _minHeight            = [_barHeights valueForKeyPath:@"@min.self"];
        
        if (_barManual.count > 0) {
            
            long long maxheight = [_maxHeight longLongValue];
            long long minheight = [_minHeight longLongValue];

            for (int k=0; k < [_barManual count]; k++) {

                long long itemVal1 = [[_barHeights objectAtIndex:k] longLongValue];
                long long itemVal2 = [[_barManual objectAtIndex:k] longLongValue];
                long long itemValSum = itemVal1 + itemVal2;
                
                if(itemValSum > maxheight ) {
                    _maxHeight = [NSNumber numberWithLong:itemValSum];
                    maxheight  = [_maxHeight longLongValue];
                }
                if (itemValSum < minheight) {
                    _minHeight = [NSNumber numberWithLong:itemValSum];
                    minheight = [_minHeight longLongValue];
                }
            }
        }
        
        _maxHeight            = [NSNumber numberWithInt:[_maxHeight intValue] + ([_maxHeight intValue]/3)];
        // Round up to the next increment value
        CGFloat remainder	= fmod(_maxHeight.floatValue / self.incrementValue, 1) * self.incrementValue;
        NSLog(@"incrementValue: %f, remainder: %f, maxHeight, %f", self.incrementValue, remainder, _maxHeight.floatValue);
        _topValue			= (self.incrementValue - remainder) + _maxHeight.floatValue;
        
        // Find max height of the x Labels based on the angle of rotation of the text
        switch (self.xLabelType)
        {
            case SimpleBarChartXLabelTypeVerticle:
            default:
                _xLabelRotation = 90.0;
                break;
                
            case SimpleBarChartXLabelTypeHorizontal:
                _xLabelRotation = 0.0;
                break;
                
            case SimpleBarChartXLabelTypeAngled:
                _xLabelRotation = 45.0;
                break;
        }
        
        for (NSString *label in _barLabels)
        {
            CGSize labelSize = [label sizeWithFont:self.xLabelFont];
            CGFloat labelHeightWithAngle = sin(DEGREES_TO_RADIANS(_xLabelRotation)) * labelSize.width;
            
            if (labelSize.height > labelHeightWithAngle)
            {
                _xLabelMaxHeight = MAX(_xLabelMaxHeight, labelSize.height);
            }
            else
            {
                _xLabelMaxHeight = MAX(_xLabelMaxHeight, labelHeightWithAngle);
            }
        }
        
        // Begin Drawing
        [self setupYAxisLabels];
        [self setupXAxisLabels];
        
        _gridLayer.frame		= CGRectMake(_yLabelView.frame.origin.x + _yLabelView.frame.size.width,
                                             0.0,
                                             self.bounds.size.width - (_yLabelView.frame.origin.x + _yLabelView.frame.size.width),
                                             self.bounds.size.height - (_xLabelMaxHeight > 0.0 ? (_xLabelMaxHeight + 5.0) : 0.0));
        _barLayer.frame			= _gridLayer.frame;
        _borderLayer.frame		= _gridLayer.frame;
        _barTextView.frame		= _gridLayer.frame;
        
        // Draw dem stuff
        [self setupBorders];
        [self drawBorders];
        
        @autoreleasepool {
            [self setupBars];
            [self animateBarAtIndex:0];
        }
        
        if (self.hasGrids)
        {
            [self setupGrid];
            [self drawGrid];
        }
        
        [self setupBarTexts];
        
        [self setupVirtualTouchBar];
    }
}

#pragma mark Borders
// 테두리 보더
- (void)setupBorders
{
    if (_borderPathLayer != nil)
    {
        [_borderPathLayer removeFromSuperlayer];
        _borderPathLayer = nil;
    }
    
    CGPoint bottomLeft 	= CGPointMake(CGRectGetMinX(_borderLayer.bounds), CGRectGetMinY(_borderLayer.bounds));
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(_borderLayer.bounds), CGRectGetMinY(_borderLayer.bounds));
    CGPoint topLeft		= CGPointMake(CGRectGetMinX(_borderLayer.bounds), CGRectGetMaxY(_borderLayer.bounds));
    CGPoint topRight	= CGPointMake(CGRectGetMaxX(_borderLayer.bounds), CGRectGetMaxY(_borderLayer.bounds));
    
    UIBezierPath *path	= [UIBezierPath bezierPath];
    [path moveToPoint:bottomLeft];
    [path addLineToPoint:topLeft];
    [path moveToPoint:bottomLeft];
    [path addLineToPoint:bottomRight];
//    [path addLineToPoint:topLeft];
//    [path addLineToPoint:bottomLeft];
//    [path addLineToPoint:bottomRight];
    
    
    
    _borderPathLayer					= [CAShapeLayer layer];
    _borderPathLayer.frame				= _borderLayer.bounds;
    _borderPathLayer.bounds				= _borderLayer.bounds;
    _borderPathLayer.geometryFlipped	= YES;
    _borderPathLayer.path				= path.CGPath;
    _borderPathLayer.strokeColor		= self.chartBorderColor.CGColor;
    _borderPathLayer.fillColor			= nil;
    _borderPathLayer.lineWidth			= 0.4f;
    _borderPathLayer.lineJoin			= kCALineJoinBevel;
    
    [_borderLayer addSublayer:_borderPathLayer];
}

- (void)drawBorders
{
    if (self.animationDuration == 0.0)
        return;
    
    [_borderPathLayer removeAllAnimations];
    
    CABasicAnimation *pathAnimation	= [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration			= 0.0;
    pathAnimation.fromValue			= [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue			= [NSNumber numberWithFloat:1.0f];
    [_borderPathLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
}

#pragma mark Bars
// 바차트 드로잉.
- (void)setupBars
{
    [self setupPopup];
    
    // Clear all bars for each drawing
    for (CAShapeLayer *layer in _barPathLayers)
    {
        if (layer != nil)
        {
            [layer removeFromSuperlayer];
        }
    }
    [_barPathLayers removeAllObjects];
    
    CGFloat barHeightRatio	= _barLayer.bounds.size.height / _topValue;
    CGFloat	xPos			= _barLayer.bounds.size.width / (_numberOfBars + 1);
    
    for (NSInteger i = 0; i < _numberOfBars; i++)
    {
        CGPoint bottom					= CGPointMake(xPos, _barLayer.bounds.origin.y);
        CGPoint top						= CGPointMake(xPos, ((NSNumber *)[_barHeights objectAtIndex:i]).floatValue * barHeightRatio);
        xPos							+= _barLayer.bounds.size.width / (_numberOfBars + 1);
        
        UIBezierPath *path				= [UIBezierPath bezierPath];
        [path moveToPoint:bottom];
        [path addLineToPoint:top];
        
        UIColor *barColor				= [UIColor darkGrayColor];
        if (_dataSource && [_dataSource respondsToSelector:@selector(barChart:colorForBarAtIndex:)])
            barColor = [_dataSource barChart:self colorForBarAtIndex:i];
        
        CAShapeLayer *barPathLayer        = [CAShapeLayer layer];
        barPathLayer.frame                = _barLayer.bounds;
        barPathLayer.bounds                = _barLayer.bounds;
        barPathLayer.geometryFlipped    = YES;
        barPathLayer.path                = path.CGPath;
        barPathLayer.strokeColor        = [barColor colorWithAlphaComponent:self.self.barAlpha].CGColor;
        barPathLayer.fillColor            = nil;
        barPathLayer.lineWidth            = _barWidth;
        barPathLayer.lineJoin            = kCALineJoinBevel;
        barPathLayer.hidden                = YES;
        barPathLayer.shadowOffset        = self.barShadowOffset;
        barPathLayer.shadowColor        = self.barShadowColor.CGColor;
        barPathLayer.shadowOpacity        = self.barShadowAlpha;
        barPathLayer.shadowRadius        = self.barShadowRadius;
        
        [_barLayer addSublayer:barPathLayer];
        [_barPathLayers addObject:barPathLayer];
        
    }
    
    
    if (_barManual.count > 0) {
        CGFloat    xPos2            = _barLayer.bounds.size.width / (_numberOfBars + 1);
        for (NSInteger i = 0; i < _numberOfBars; i++)
        {
            CGPoint top1                            = CGPointMake(xPos2, ((NSNumber *)[_barHeights objectAtIndex:i]).floatValue * barHeightRatio);
            
            CGPoint bottom2                         = CGPointMake(xPos2, _barLayer.bounds.origin.y);
            CGPoint top2                            = CGPointMake(xPos2, ((NSNumber *)[_barManual objectAtIndex:i]).floatValue * barHeightRatio);
            xPos2                                   += _barLayer.bounds.size.width / (_numberOfBars + 1);
            
            bottom2 = CGPointMake(bottom2.x, top1.y);
            top2    = CGPointMake(top2.x, top1.y + top2.y);
            
            UIBezierPath *path2                     = [UIBezierPath bezierPath];
            [path2 moveToPoint:bottom2];
            [path2 addLineToPoint:top2];
            
            UIColor *barColor2                  = RGB(245, 162, 0);
            
            CAShapeLayer *barPathLayer2         = [CAShapeLayer layer];
            barPathLayer2.frame                 = _barLayer.bounds;
            barPathLayer2.bounds                = _barLayer.bounds;
            barPathLayer2.geometryFlipped       = YES;
            barPathLayer2.path                  = path2.CGPath;
            barPathLayer2.strokeColor           = [barColor2 colorWithAlphaComponent:self.self.barAlpha].CGColor;
            barPathLayer2.fillColor             = nil;
            barPathLayer2.lineWidth             = _barWidth;
            barPathLayer2.lineJoin              = kCALineJoinBevel;
            barPathLayer2.hidden                = YES;
            barPathLayer2.shadowOffset          = self.barShadowOffset;
            barPathLayer2.shadowColor           = self.barShadowColor.CGColor;
            barPathLayer2.shadowOpacity         = self.barShadowAlpha;
            barPathLayer2.shadowRadius          = self.barShadowRadius;
            
            [_barLayer addSublayer:barPathLayer2];
            [_barPathLayers addObject:barPathLayer2];
            
        }
    }
    
    
    
}


- (void)animateBarAtIndex:(NSInteger)index
{
    if (index >= _barPathLayers.count)
    {
        // Last bar, begin drawing grids
        [self displayAxisLabels];
        return;
    }
    
    __block NSInteger i				= index + 1;
    __weak SimpleBarChart *weakSelf = self;
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.0];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [CATransaction setCompletionBlock:^{
        [weakSelf animateBarAtIndex:i];
    }];
    
    CAShapeLayer *barPathLayer		= [_barPathLayers objectAtIndex:index];
    barPathLayer.hidden				= NO;
    [self drawBar:barPathLayer];
    
    [CATransaction commit];
}

- (void)drawBar:(CAShapeLayer *)barPathLayer
{
    if (self.animationDuration == 0.0)
        return;
    
    [barPathLayer removeAllAnimations];
    
    CABasicAnimation *pathAnimation	= [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.fromValue			= [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue			= [NSNumber numberWithFloat:1.0f];
    [barPathLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
}

#pragma mark Grid

// 가로 라인 드로잉.
- (void)setupGrid
{
    if (_gridPathLayer != nil)
    {
        [_gridPathLayer removeFromSuperlayer];
        _gridPathLayer = nil;
    }
    
    CGFloat gridUnit		= _gridLayer.bounds.size.height / _topValue;
    CGFloat gridSeperation	= gridUnit * self.incrementValue;
    
    CGFloat yPos			= gridSeperation;
    UIBezierPath *path		= [UIBezierPath bezierPath];
    while (yPos < _gridLayer.bounds.size.height || [self floatsAlmostEqualBetweenValue1:yPos value2:_gridLayer.bounds.size.height andPrecision:0.001])
    {
        if (yPos==_gridLayer.bounds.size.height) {
            break;
        }
        CGPoint left	= CGPointMake(0.0, yPos);
        CGPoint right	= CGPointMake(_gridLayer.bounds.size.width, yPos);
        yPos			+= gridSeperation;
        
        [path moveToPoint:left];
        [path addLineToPoint:right];
    }
    
    _gridPathLayer					= [CAShapeLayer layer];
    _gridPathLayer.frame			= _gridLayer.bounds;
    _gridPathLayer.bounds			= _gridLayer.bounds;
    _gridPathLayer.geometryFlipped	= YES;
    _gridPathLayer.path				= path.CGPath;
    _gridPathLayer.strokeColor		= self.gridColor.CGColor;
    _gridPathLayer.fillColor		= nil;
    _gridPathLayer.lineWidth		= 0.2f;
    _gridPathLayer.lineJoin			= kCALineJoinBevel;
    
    [_gridLayer addSublayer:_gridPathLayer];
}

// From http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
- (BOOL)floatsAlmostEqualBetweenValue1:(CGFloat)value1 value2:(CGFloat)value2 andPrecision:(CGFloat)precision
{
    if (value1 == value2)
        return YES;
    CGFloat relativeError = fabs((value1 - value2) / value2);
    if (relativeError <= precision)
        return YES;
    return NO;
}

- (void)drawGrid
{
    if (self.animationDuration == 0.0)
        return;
    
    [_gridPathLayer removeAllAnimations];
    
    CABasicAnimation *pathAnimation	= [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration			= self.animationDuration;
    pathAnimation.fromValue			= [NSNumber numberWithFloat:1.0f];
    pathAnimation.toValue			= [NSNumber numberWithFloat:1.0f];
    pathAnimation.timingFunction	= [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [_gridPathLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
}

#pragma mark Axis Labels
// 좌측 세로 라벨텍스트 드로잉.
- (void)setupYAxisLabels
{
    if (!self.hasYLabels)
        return;
    
    if (_yLabelView.alpha > 0.0)
    {
        _yLabelView.alpha = 0.0;
        [[_yLabelView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat yLabelFrameHeight	= self.bounds.size.height - (_xLabelMaxHeight > 0.0 ? (_xLabelMaxHeight + 5.0) : 0.0);
    CGFloat gridUnit			= yLabelFrameHeight / _topValue;
    CGFloat gridSeperation		= gridUnit * self.incrementValue;
    
    CGFloat yPos				= 0.0;
    CGFloat maxVal				= _topValue;
    CGFloat maxWidth			= 0.0;
    
    while (yPos < yLabelFrameHeight)
    {
        NSString *stringFormat	= (_yLabelType == SimpleBarChartYAxisLabelTypeFloat) ? @"%.1f" : @"%.0f";
        NSString *yLabelString	= [NSString stringWithFormat:stringFormat, maxVal];
        CGSize yLabelSize		= [yLabelString sizeWithFont:self.yLabelFont];
        CGRect yLabelFrame		= CGRectMake(0.0,
                                             0.0,
                                             yLabelSize.width,
                                             yLabelSize.height);
        
        if ([yLabelString isEqualToString:@"nan"]) {
            yLabelString = @"";
        }
        UILabel *yLabel			= [[UILabel alloc] initWithFrame:yLabelFrame];
        yLabel.font				= self.yLabelFont;
        yLabel.backgroundColor	= [UIColor clearColor];
        yLabel.textColor		= self.yLabelColor;
        yLabel.textAlignment	= NSTextAlignmentRight;
        yLabel.center			= CGPointMake(yLabel.center.x, yPos);
        yLabel.text				= yLabelString;
        [_yLabelView addSubview:yLabel];
        
        if (maxWidth > 0.0) {
            yLabel.frame = CGRectMake(yLabel.frame.origin.x,
                                      yLabel.frame.origin.y,
                                      maxWidth,
                                      yLabel.frame.size.height);
        }
        
        maxWidth				= MAX(maxWidth, yLabelSize.width);
        maxVal					-= self.incrementValue;
        yPos					+= gridSeperation;
    }
    
    // Y라벨이 가려서 맨뒤로 보냄
    [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self sendSubviewToBack:_yLabelView];
    }];
    _yLabelView.frame		= CGRectMake(0.0,
                                         0.0,
                                         self.hasYLabels ? maxWidth + 5.0 : 0.0,
                                         yLabelFrameHeight);
}

- (void)setupXAxisLabels
{
    if (_barLabels.count == 0)
        return;
    
    if (_xLabelView.alpha > 0.0)
    {
        _xLabelView.alpha = 0.0;
        [[_xLabelView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat xLabelFrameWidth	= self.bounds.size.width - (_yLabelView.frame.origin.x + _yLabelView.frame.size.width);
    CGFloat	xPos				= xLabelFrameWidth / (_numberOfBars + 1);
    
    for (NSInteger i = 0; i < _numberOfBars; i++)
    {
        NSString *xLabelText	= [_barLabels objectAtIndex:i];
        CGSize xLabelSize		= [xLabelText sizeWithFont:self.xLabelFont];
        CGRect xLabelFrame		= CGRectMake(0.0,
                                             0.0,
                                             xLabelSize.width,
                                             xLabelSize.height);
        UILabel *xLabel			= [[UILabel alloc] initWithFrame:xLabelFrame];
        xLabel.font				= self.xLabelFont;
        xLabel.backgroundColor	= [UIColor clearColor];
        xLabel.textColor		= self.xLabelColor;
        xLabel.textAlignment	= NSTextAlignmentRight;
        xLabel.text				= xLabelText;
        xLabel.transform		= CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-_xLabelRotation));
        
        // Position the label appropriately
        switch (self.xLabelType)
        {
            case SimpleBarChartXLabelTypeVerticle:
            default:
                xLabel.center = CGPointMake(xPos, (xLabelSize.width / 2.0));
                break;
                
            case SimpleBarChartXLabelTypeHorizontal:
                xLabel.center = CGPointMake(xPos, _xLabelMaxHeight / 2.0);
                break;
                
            case SimpleBarChartXLabelTypeAngled:
            {
                CGFloat labelHeightWithAngle	= sin(DEGREES_TO_RADIANS(_xLabelRotation)) * xLabelSize.width;
                xLabel.center					= CGPointMake(xPos - (labelHeightWithAngle / 2.0), labelHeightWithAngle / 2.0);
                break;
            }
        }
        
        [_xLabelView addSubview:xLabel];
        
        xPos					+= xLabelFrameWidth / (_numberOfBars + 1);
    }
    
    // X라벨이 가려서 맨뒤로 보냄
    [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self sendSubviewToBack:_xLabelView];
    }];
    _xLabelView.frame			= CGRectMake(_yLabelView.frame.origin.x + _yLabelView.frame.size.width,
                                             self.bounds.size.height - _xLabelMaxHeight,
                                             xLabelFrameWidth,
                                             _xLabelMaxHeight);
}

- (void)setupBarTexts
{
    if (!self.valueVisible) {
        return;
    }
    if (_barTexts.count == 0)
        return;
    
    if (_barTextView.alpha > 0.0)
    {
        _barTextView.alpha = 0.0;
        [[_barTextView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat	xPos				= _barLayer.bounds.size.width / (_numberOfBars + 1);
    
    for (NSInteger i = 0; i < _numberOfBars; i++)
    {
        NSString *barLabelText	= [_barTexts objectAtIndex:i];
        CGSize barTextSize		= [barLabelText sizeWithFont:self.barTextFont];
        CGRect barTextFrame		= CGRectMake(0.0,
                                             0.0,
                                             barTextSize.width,
                                             barTextSize.height);
        UILabel *barText		= [[UILabel alloc] initWithFrame:barTextFrame];
        barText.font			= self.barTextFont;
        barText.backgroundColor	= [UIColor clearColor];
        barText.textColor		= self.barTextColor;
        barText.textAlignment	= NSTextAlignmentCenter;
        barText.text			= barLabelText;
        
        CGFloat barHeight		= (_barLayer.bounds.size.height / _topValue) * ((NSNumber *)[_barHeights objectAtIndex:i]).floatValue;
        if(isnan(barHeight)) barHeight = 0.0f;
        // Position the label appropriately
        switch (self.barTextType)
        {
            case SimpleBarChartBarTextTypeTop:
            default:
                barText.center = CGPointMake(xPos, _barLayer.bounds.size.height - (barHeight - (barTextSize.height / 2.0))-10);
                break;
                
            case SimpleBarChartBarTextTypeRoof:
                barText.center = CGPointMake(xPos, _barLayer.bounds.size.height - (barHeight + (barTextSize.height / 2.0)));
                break;
                
            case SimpleBarChartBarTextTypeMiddle:
            {
                CGFloat minBarHeight	= (_barLayer.bounds.size.height / _topValue) * _minHeight.floatValue;
                barText.center			= CGPointMake(xPos, _barLayer.bounds.size.height - (minBarHeight / 2.0));
                break;
            }
        }
        
        if(![barText.text isEqualToString:@"0"]){
            [_barTextView addSubview:barText];
        }
        
        xPos += _barLayer.bounds.size.width / (_numberOfBars + 1);
    }
}

- (void)displayAxisLabels
{
    if (self.hasYLabels || _barTexts.count > 0 || _barLabels.count > 0)
    {
        
        _yLabelView.alpha	= 1.0;
        _xLabelView.alpha	= 1.0;
        _barTextView.alpha	= 1.0;
        
        if (_delegate && [_delegate respondsToSelector:@selector(animationDidEndForBarChart:)])
            [_delegate animationDidEndForBarChart:self];
        
    }
    else
    {
        if (_delegate && [_delegate respondsToSelector:@selector(animationDidEndForBarChart:)])
            [_delegate animationDidEndForBarChart:self];
    }
}





#pragma mark 팝업 레이어
- (void)setupPopup {
    
    UIColor *popColor = RGB(96, 123, 227);
    
    CGPoint yPoint        = CGPointMake(CGRectGetMinX(_borderLayer.bounds), CGRectGetMaxY(_borderLayer.bounds));
    vBoardBack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, yPoint.y)];
    [vBoardBack setBackgroundColor:RGBA(96, 123, 227, 0.5)];
    vBoard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, vBoardBack.frame.size.width, 28)];
    vBoardBack.backgroundColor = [UIColor clearColor];
    vBoard.backgroundColor = popColor;
    vLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, 0, vBoard.frame.size.width-6, vBoard.frame.size.height)];
    vLabel.font = [UIFont fontWithName:MAIN_FONT_NAME size:12.0];
    [vLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    vLabel.textColor = [UIColor whiteColor];
    vLabel.numberOfLines = 1;
    vLabel.minimumScaleFactor = 0.5;
    vLabel.adjustsFontSizeToFitWidth = YES;
    vLabel.textAlignment = NSTextAlignmentCenter;
    vBoard.layer.cornerRadius = 5;
    vBoard.layer.masksToBounds = true;
    
    [vBoard addSubview:vLabel];
    
    UIView *pLine = [[UIView alloc] initWithFrame:CGRectMake(vBoardBack.frame.size.width/2, 0, 1, vBoardBack.frame.size.height)];
    pLine.backgroundColor = popColor;
    [vBoardBack addSubview:pLine];
    
    [vBoardBack addSubview:vBoard];
    [self addSubview:vBoardBack];
    [self sendSubviewToBack:vBoardBack];
    vBoardBack.hidden = YES;
}

- (void)setupVirtualTouchBar
{
    CGRect bRec = _barLayer.bounds;
    bRec.size.width = bRec.size.width + 50;
    UIView *backView = [[UIView alloc] initWithFrame:bRec];
    [self addSubview:backView];
    
    CGFloat barHeightRatio    = _barLayer.bounds.size.height / _topValue;
    CGFloat    xPos            = _barLayer.bounds.size.width / (_numberOfBars + 1);
    
    for (NSInteger i = 0; i < _numberOfBars; i++)
    {
        
        CGPoint bottom                    = CGPointMake(xPos, _barLayer.bounds.origin.y);
        CGPoint top                       = CGPointMake(xPos, ((NSNumber *)[_barHeights objectAtIndex:i]).floatValue * barHeightRatio);
        xPos                              += _barLayer.bounds.size.width / (_numberOfBars + 1);
        
        NSString *barLabelText    = [_barTexts objectAtIndex:i];
//        if ([barLabelText intValue] > 0) {
        
            UIView *v;
            v =[[UIView alloc] initWithFrame:CGRectMake(bottom.x, 0, _barWidth, self.frame.size.height)];
            v.tag = TAG_BAR +i;
            [backView addSubview:v];
//            [v setBackgroundColor:[UIColor redColor]];
            v.center = CGPointMake(bottom.x+_barLayer.frame.origin.x, self.frame.size.height/2);
//        }
    }
    
    
//    [backView setBackgroundColor:RGBA(0, 100, 0, 0.5)];
//    [self bringSubviewToFront:backView];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch= [touches anyObject];
    UIView * v = [touch view];
    
    if(v.tag >= TAG_BAR) {
        
        long val1 = [[_barTexts objectAtIndex:v.tag-TAG_BAR] longLongValue];
        long val2 = [[_barManual objectAtIndex:v.tag-TAG_BAR] longLongValue];
        long long sumVal = val1 + val2;
        
        if (sumVal >0) {
            
            vBoardBack.hidden = NO;
            vBoardBack.center = v.center;
            vBoardBack.frame = CGRectMake(vBoardBack.frame.origin.x, 0, vBoardBack.frame.size.width, vBoardBack.frame.size.height);
            
            NSString *barLabelText    = [NSString stringWithFormat:@"%lld", sumVal];
            //천단위 컴마
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *number = [f numberFromString:barLabelText];
            
            barLabelText=  [numberFormatter stringFromNumber:number];
            
            vLabel.text = [NSString stringWithFormat:@"%@%@", barLabelText, self.unit];
            NSLog(@"touchesBegan view.tag:%ld", v.tag);
        }else{
            vBoardBack.hidden = YES;
        }
        
    }else{
        vBoardBack.hidden = YES;
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
   
    UITouch *touch= [touches anyObject];
    UIView * v = [touch view];
    CGPoint pt = [touch locationInView:self];
    NSLog(@"%f", pt.x);
    
    CGFloat barHeightRatio    = _barLayer.bounds.size.height / _topValue;
    CGFloat    xPos            = _barLayer.bounds.size.width / (_numberOfBars + 1);
    
    for (NSInteger i = 0; i < _numberOfBars; i++)
    {
        CGPoint bottom                    = CGPointMake(xPos, _barLayer.bounds.origin.y);
        CGPoint top                       = CGPointMake(xPos, ((NSNumber *)[_barHeights objectAtIndex:i]).floatValue * barHeightRatio);
        xPos                              += _barLayer.bounds.size.width / (_numberOfBars + 1);
        
        
        long val1 = [[_barTexts objectAtIndex:i] longLongValue];
        long val2 = [[_barManual objectAtIndex:i] longLongValue];
        long long sumVal = val1 + val2;
        
        if (sumVal>0) {
            NSString *barLabelText    = [NSString stringWithFormat:@"%lld", sumVal];
            
            if ([barLabelText intValue] > 0) {
                
                float from  = bottom.x + _barLayer.frame.origin.x;
                float to    = from + _barWidth;
                
                if (pt.x >= from && pt.x <= to ) {
                    
                    UIView *tv = [self viewWithTag:TAG_BAR +i];
                    
                    vBoardBack.hidden = NO;
                    vBoardBack.center = tv.center;
                    vBoardBack.frame = CGRectMake(vBoardBack.frame.origin.x, 0, vBoardBack.frame.size.width, vBoardBack.frame.size.height);
                    
                    //천단위 컴마
                    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
                    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterDecimalStyle;
                    NSNumber *number = [f numberFromString:barLabelText];
                    
                    barLabelText=  [numberFormatter stringFromNumber:number];
                    
                    vLabel.text = [NSString stringWithFormat:@"%@%@", barLabelText, self.unit];
                    
                    break;
                }
            }else{
                
                vBoardBack.hidden = YES;
            }
        }
        
    }
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"touchesEnded");
   
}

@end
