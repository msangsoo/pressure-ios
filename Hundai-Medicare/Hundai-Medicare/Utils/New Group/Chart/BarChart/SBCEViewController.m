//
//  SBCEViewController.m
//  SimpleBarChartExample
//
//  Created by Mohammed Islam on 1/17/14.
//  Copyright (c) 2014 KSI Technology. All rights reserved.
//

#import "SBCEViewController.h"
#import "NSMutableArray+TTMutableArray.h"


@interface SBCEViewController ()

@end

@implementation SBCEViewController

- (SimpleBarChart*)loadChart:(CGRect)frame items:(NSArray*)items itemOther:(NSArray*)itemOther chartType:(CHARTVIEW_TYPE)chartType xCount:(long)xCount {
    
    _valManual = itemOther;
    
  return [self loadChart:frame items:items chartType:chartType xCount:xCount];
}

- (SimpleBarChart*)loadChart:(CGRect)frame items:(NSArray*)items chartType:(CHARTVIEW_TYPE)chartType xCount:(long)xCount
{
//	[super loadView];
    
    xItemsDay   = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17, @18, @19, @20, @21, @22, @23, @24];
    xItemsWeek  = @[@"일", @"월",@"화",@"수",@"목",@"금",@"토"];;
    xItemsMonth = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17, @18, @19, @20, @21, @22, @23, @24, @25, @26, @27, @28, @29, @30, @31];
    xItemsYear  = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12];
    
    _xCount = (int)xCount;
    
    _values							= items;
    int max = [[_values valueForKeyPath:@"@max.intValue"] intValue] == 0 ? 50 : [[_values valueForKeyPath:@"@max.intValue"] intValue];
    int incrementValue = [self setGraphSection:max];    // Y 구간간격
    if([[_values valueForKeyPath:@"@max.intValue"] intValue] == 0){
        incrementValue = 0;
    }
    
    if (_valManual!= nil && _valManual.count > 0) {
        max = [[_valManual valueForKeyPath:@"@max.intValue"] intValue] < max ? max : [[_valManual valueForKeyPath:@"@max.intValue"] intValue];
        incrementValue = [self setGraphSection:max];    // Y 구간간격
//        if([[_valManual valueForKeyPath:@"@max.intValue"] intValue] == 0
//           && [[_values valueForKeyPath:@"@max.intValue"] intValue] == 0){
//            incrementValue = 0;
//        }
    }
    
	_barColors						= @[DARKBLUE_BAR_COLOR];
	_currentBarColor				= 0;
    
	_chart							= [[SimpleBarChart alloc] initWithFrame:frame];
//	_chart.center					= CGPointMake(frame.size.width / 2.0, frame.size.height / 2.0);
	_chart.delegate					= self;
	_chart.dataSource				= self;
	_chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
	_chart.animationDuration		= 0.0;
	_chart.barShadowColor			= [UIColor clearColor];
	_chart.barShadowAlpha			= 0.5;
	_chart.barShadowRadius			= 1.0;
    
    if(chartType == CHART_TYPE_DAY){
        _chart.barWidth					=  frame.size.width / (float)(xItemsDay.count + 10);
    }else if(chartType == CHART_TYPE_WEEK){
        _chart.barWidth					=  frame.size.width / (float)(xItemsWeek.count + 10);
    }else{
        _chart.barWidth					=  frame.size.width / (float)(xItemsMonth.count + 10);
    }
    
    model = [Model instance];
    model.chartType = chartType;
    
	_chart.xLabelType				= SimpleBarChartXLabelTypeHorizontal;
	_chart.incrementValue			= incrementValue;
	_chart.barTextType				= SimpleBarChartBarTextTypeTop;
	_chart.barTextColor				= [UIColor blackColor];
	_chart.gridColor				= [UIColor grayColor];


    return _chart;
  
}

- (float)setGraphSection:(float)value {
    
    int div = value/10;
    if (div <= 10) {
        div = (((int)(div/1))*1)+1;
    }else if (div <= 100) {
        div = (((int)(div/10))*10)+10;
    }else if (div <= 1000) {
        div = (((int)(div/100))*100)+100;
    }else if (div <= 10000) {
        div = (((int)(div/1000))*1000)+1000;
    }else if (div <= 100000) {
        div = (((int)(div/10000))*10000)+ 10000;
    }
    return div;
}

- (void)changeClicked
{
	NSMutableArray *valuesCopy = _values.mutableCopy;
	[valuesCopy shuffle];

	_values = valuesCopy;

	if (_chart.xLabelType == SimpleBarChartXLabelTypeVerticle)
		_chart.xLabelType = SimpleBarChartXLabelTypeHorizontal;
	else
		_chart.xLabelType = SimpleBarChartXLabelTypeVerticle;

	_currentBarColor = ++_currentBarColor % _barColors.count;

	[_chart reloadData];
}

- (void)reloadData {
    
    [_chart reloadData];
}


#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
	return _xCount;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    
    return [[_values objectAtIndex:index] floatValue];
	
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
	return [[_values objectAtIndex:index] stringValue];
}

- (CGFloat)barChart:(SimpleBarChart *)barChart manualValueForBarAtIndex:(NSUInteger)index
{
    if (_valManual != nil
            && [_valManual count]-1 >= index) {
        return [[_valManual objectAtIndex:index] floatValue];
    }else{
        return 0.0f;
    }
}

- (NSString *)barChart:(SimpleBarChart *)barChart manualTextForBarAtIndex:(NSUInteger)index
{
    if (_valManual != nil
            && [_valManual count]-1 >= index) {
        
        return [[_valManual objectAtIndex:index] stringValue];
    }else{
        return @"0";
    }
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    
    if (model.chartType == CHART_TYPE_DAY) {
        
        if (index%2==0) {
            return [[xItemsDay objectAtIndex:index] stringValue];
        }
        return @"";
        
    }else if (model.chartType== CHART_TYPE_WEEK) {
        
        return [xItemsWeek objectAtIndex:index];
        
    }else if (model.chartType== CHART_TYPE_MONTH) {
        
        if (index%2==0) {
            return [[xItemsMonth objectAtIndex:index] stringValue];
        }
        return @"";
        
    }else  {
        
        return [[xItemsYear objectAtIndex:index] stringValue];
    }
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
	return [_barColors objectAtIndex:_currentBarColor];
}


@end
