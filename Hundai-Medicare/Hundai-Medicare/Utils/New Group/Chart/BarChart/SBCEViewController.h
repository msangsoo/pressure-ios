//
//  SBCEViewController.h
//  SimpleBarChartExample
//
//  Created by Mohammed Islam on 1/17/14.
//  Copyright (c) 2014 KSI Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"

@interface SBCEViewController : UIView <SimpleBarChartDataSource, SimpleBarChartDelegate>
{
    NSArray *_values;
    NSArray *_valManual; 

	SimpleBarChart *_chart;
    Model *model;
    
	NSArray *_barColors;
	NSInteger _currentBarColor;
    
    NSArray *xItemsDay;
    NSArray *xItemsWeek;
    NSArray *xItemsMonth;
    NSArray *xItemsYear;
    
    int _xCount;
}
- (SimpleBarChart*)loadChart:(CGRect)frame items:(NSArray*)items chartType:(int)chartType xCount:(long)xCount;
- (SimpleBarChart*)loadChart:(CGRect)frame items:(NSArray*)items itemOther:(NSArray*)itemOther chartType:(int)chartType xCount:(long)xCount;
- (void)reloadData;
@end
