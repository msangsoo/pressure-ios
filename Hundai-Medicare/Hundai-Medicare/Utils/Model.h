//
//  Model.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 4..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDelegate.h"
#import "ActivityIndicatorView.h"
#import "Tr_login.h"
#import "Tr_getInformation.h"
#import "WeightDBWraper.h"

typedef enum {
    CHART_TYPE_DAY = 0,
    CHART_TYPE_WEEK,
    CHART_TYPE_MONTH,
    CHART_TYPE_YEAR
} CHARTVIEW_TYPE;


@interface Model : NSObject {
    ActivityIndicatorView *_activityIndicatorView;
    BOOL _isRqActive;
    NSTimer *_timerIndicator;
    Tr_login *login;
    Tr_getInformation *getInformation;
}
@property (strong, nonatomic) NSString * mber_id;

@property (nonatomic, strong) NSString *_noticeGubun;
@property (nonatomic, assign) int chartType;

@property (strong, nonatomic) NSArray *checkMedicalArray;
@property (strong, nonatomic) NSMutableArray *checkLevelArray;
@property (strong, nonatomic) NSMutableArray *checkResultArray;

// Instance
+ (id)instance;

// indicator
- (void)indicatorActive;
- (void)doIndicatorActivity:(NSString*)message;
- (void)indicatorHidden;

// login&information
- (void)setLoginTypeData:(Tr_login *)data;
- (void)setLoginData:(NSDictionary *)data;
- (Tr_login *)getLoginData;
- (void)setInformation:(NSDictionary *)data;
- (Tr_getInformation *)getInformation;

//token
- (NSString *)getToken;

// 목표칼로리
- (int)getDefaultGoalCalori;
- (NSString *)getStepTargetCalulator:(int)sex height:(float)height weight:(float)weight calrori:(int)calrori ;

// 혈압 메세지
- (NSString *)getPresuMsg:(NSString *)systolic diastolic:(NSString *)diastolic regdate:(NSString *)regdate;
- (NSString *)getpresuGroup:(float)systolic diastolic:(float)diastolic;

//혈당 메세지
-(NSString *)getSugarMessage:(NSString *)sugar eatType:(NSString *)eatType regdate:(NSString *)regdate;

//체중 메세지
- (NSString *)getWeightMessage:(NSString *)weight fat:(NSString *)fat;

//표준 칼로리 구하기 (식사)
- (NSString *)getDefaultCalori;

// 식사 권장칼로리
- (int)getPregnancyRecommendCal;

@end
