//
//  Tr_getInformation.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 7..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "Tr_getInformation.h"

@implementation Tr_getInformation

- (id)initWithData:(NSDictionary *)data {
    self = [super init];
    
    self.CMPNY_NM       = [data objectForKey:@"CMPNY_NM"];
    self.CMPNY_ARS      = [data objectForKey:@"CMPNY_ARS"];
    self.CMPNY_IMAGE    = [data objectForKey:@"CMPNY_IMAGE"];
    self.loginURL       = [data objectForKey:@"loginURL"];
    self.apiURL         = [data objectForKey:@"apiURL"];
    self.NOTICE_YN      = [data objectForKey:@"NOTICE_YN"];
    self.appVersion     = [data objectForKey:@"appVersion"];
    self.updateURL      = [data objectForKey:@"updateURL"];
    
    return self;
}

@end
