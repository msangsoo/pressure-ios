//
//
//  Created by insystems on 2017. 5. 26..
//  Copyright © 2017년 insystems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tr_login : NSObject {
    
}

@property (strong, nonatomic) NSString *mber_sn;
@property (strong, nonatomic) NSString *mber_actqy;
@property (strong, nonatomic) NSString *mber_bdwgh;
@property (strong, nonatomic) NSString *mber_bdwgh_app;
@property (strong, nonatomic) NSString *mber_bdwgh_goal;
@property (strong, nonatomic) NSString *mber_grad;
@property (strong, nonatomic) NSString *mber_height;
@property (strong, nonatomic) NSString *mber_hp;
@property (strong, nonatomic) NSString *mber_hp_newyn;
@property (strong, nonatomic) NSString *mber_lifyea;
@property (strong, nonatomic) NSString *mber_nm;
@property (strong, nonatomic) NSString *mber_sex;

@property (strong, nonatomic) NSString *age;
@property (strong, nonatomic) NSString *accml_sum_amt;
@property (strong, nonatomic) NSString *add_reg_yn;

@property (strong, nonatomic) NSString *cm_yn;

@property (strong, nonatomic) NSString *day_health_amt;
@property (strong, nonatomic) NSString *daily_yn;
@property (strong, nonatomic) NSString *day_basic_goal;
@property (strong, nonatomic) NSString *default_basic_goal;
@property (strong, nonatomic) NSString *disease_nm;
@property (strong, nonatomic) NSString *disease_txt;
@property (strong, nonatomic) NSString *smkng_yn;

@property (strong, nonatomic) NSString *gclife_id;
@property (strong, nonatomic) NSString *goal_mvm_calory;
@property (strong, nonatomic) NSString *goal_mvm_stepcnt;
@property (strong, nonatomic) NSString *goal_water_ntkqy;

@property (strong, nonatomic) NSString *heart_yn;
@property (strong, nonatomic) NSString *health_yn;
@property (strong, nonatomic) NSString *health_alert_yn;

@property (strong, nonatomic) NSString *job_yn;

@property (strong, nonatomic) NSString *log_yn;

@property (strong, nonatomic) NSString *mber_zone;
@property (strong, nonatomic) NSString *medicine_yn;
@property (strong, nonatomic) NSString *mission_alert_yn;
@property (strong, nonatomic) NSString *mission_walk_start_de;
@property (strong, nonatomic) NSString *mission_walk_end_de;

@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *notice_yn;

@property (strong, nonatomic) NSString *pm_yn;

@property (strong, nonatomic) NSString *reply_yn;

@property (strong, nonatomic) NSString *seq;
@property (strong, nonatomic) NSString *sugar_typ;
@property (strong, nonatomic) NSString *sugar_alert_yn;
@property (strong, nonatomic) NSString *sugar_occur_de;
@property (strong, nonatomic) NSString *st_yn;

@property (strong, nonatomic) NSString *point_total_amt;

@property (strong, nonatomic) NSString *tot_basic_goal;

- (id)initWithData:(NSDictionary*)data;
@end
