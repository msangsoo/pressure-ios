//
//  Tr_getInformation.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 7..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tr_getInformation : NSObject {
}

@property (strong, nonatomic) NSString *CMPNY_NM;
@property (strong, nonatomic) NSString *CMPNY_ARS;
@property (strong, nonatomic) NSString *CMPNY_IMAGE;
@property (strong, nonatomic) NSString *loginURL;
@property (strong, nonatomic) NSString *apiURL;
@property (strong, nonatomic) NSString *NOTICE_YN;
@property (strong, nonatomic) NSString *appVersion;
@property (strong, nonatomic) NSString *updateURL;

- (id)initWithData:(NSDictionary *)data;

@end
