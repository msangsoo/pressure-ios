//
//
//  Created by insystems on 2017. 5. 26..
//  Copyright © 2017년 insystems. All rights reserved.
//

#import "Tr_login.h"

@implementation Tr_login

- (id)initWithData:(NSDictionary *)data {
    self = [super init];
    
    self.mber_sn                = [data objectForKey:@"mber_sn"];
    self.mber_actqy             = [data objectForKey:@"mber_actqy"];
    self.mber_bdwgh             = [data objectForKey:@"mber_bdwgh"];
    self.mber_bdwgh_app         = [data objectForKey:@"mber_bdwgh_app"];
    self.mber_bdwgh_goal        = [data objectForKey:@"mber_bdwgh_goal"];
    self.mber_grad              = [data objectForKey:@"mber_grad"];
    self.mber_hp_newyn          = [data objectForKey:@"mber_hp_newyn"];
    self.mber_hp                = [data objectForKey:@"mber_hp"];
    self.mber_height            = [data objectForKey:@"mber_height"];
    self.mber_lifyea            = [data objectForKey:@"mber_lifyea"];
    self.mber_nm                = [data objectForKey:@"mber_nm"];
    self.mber_sex               = [data objectForKey:@"mber_sex"];
    
    self.age                    = [data objectForKey:@"age"];
    self.add_reg_yn             = [data objectForKey:@"add_reg_yn"];
    self.accml_sum_amt          = [data objectForKey:@"accml_sum_amt"];
    
    self.cm_yn                  = [data objectForKey:@"cm_yn"];
    
    self.day_health_amt         = [data objectForKey:@"day_health_amt"];
    self.daily_yn               = [data objectForKey:@"daily_yn"];
    self.day_basic_goal         = [data objectForKey:@"cm_yn"];
    self.default_basic_goal     = [data objectForKey:@"default_basic_goal"];
    self.disease_nm             = [data objectForKey:@"disease_nm"];
    self.disease_txt            = [data objectForKey:@"disease_txt"];
    
    self.gclife_id              = [data objectForKey:@"gclife_id"];
    self.goal_mvm_calory        = [data objectForKey:@"goal_mvm_calory"];
    self.goal_mvm_stepcnt       = [data objectForKey:@"goal_mvm_stepcnt"];
    self.goal_water_ntkqy       = [data objectForKey:@"goal_water_ntkqy"];
    
    self.health_alert_yn        = [data objectForKey:@"health_alert_yn"];
    self.health_yn              = [data objectForKey:@"health_yn"];
    self.heart_yn               = [data objectForKey:@"heart_yn"];
    
    self.job_yn                 = [data objectForKey:@"job_yn"];
    
    self.log_yn                 = [data objectForKey:@"log_yn"];
    
    self.mber_zone              = [data objectForKey:@"mber_zone"];
    self.medicine_yn            = [data objectForKey:@"medicine_yn"];
    self.mission_alert_yn       = [data objectForKey:@"mission_alert_yn"];
    self.mission_walk_start_de  = [data objectForKey:@"mission_walk_start_de"];
    self.mission_walk_end_de    = [data objectForKey:@"mission_walk_end_de"];
    
    self.nickname           = [data objectForKey:@"nickname"];
    self.notice_yn           = [data objectForKey:@"notice_yn"];
    
    self.pm_yn           = [data objectForKey:@"pm_yn"];
    
    self.reply_yn           = [data objectForKey:@"reply_yn"];
    
    self.seq           = [data objectForKey:@"seq"];
    self.smkng_yn           = [data objectForKey:@"smkng_yn"];
    self.sugar_typ           = [data objectForKey:@"sugar_typ"];
    self.sugar_alert_yn           = [data objectForKey:@"sugar_alert_yn"];
    self.sugar_occur_de           = [data objectForKey:@"sugar_occur_de"];
    self.st_yn           = [data objectForKey:@"st_yn"];
    
    self.point_total_amt           = [data objectForKey:@"point_total_amt"];
    
    self.tot_basic_goal           = [data objectForKey:@"tot_basic_goal"];
    
    return self;
}

@end
