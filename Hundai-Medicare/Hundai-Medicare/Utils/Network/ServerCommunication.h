//
//  ServerCommunication.h
//  HealthCare
//
//  Created by jangkun lee on 13. 3. 5..
//  Copyright (c) 2013년 jangkun lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONKit.h"
#import "ASIFormDataRequest.h"
#import "NetworkCheck.h"

@protocol ServerCommunicationDelegate <NSObject>
-(void)delegateResultString:(id)result CODE:(NSString*)code;
-(void)delegateResultData:(id)result;
-(void)delegatePush:(NSInteger)status;
-(void)delegateError;
@end

@interface ServerCommunication : NSObject {
    
    Model *_model;
    ASIFormDataRequest *httpRequest;
    NetworkCheck *networkCheck;
    NSString *insures_code;
    id <ServerCommunicationDelegate> delegate;
}

@property ( assign ) id < ServerCommunicationDelegate > delegate;

- (void)serverCommunicationData:(NSDictionary *)parametter;
- (void)writeImage:(UIImage *)image ;
- (void)writeImage2:(UIImage *)image;
- (void)writeImage:(UIImage *)image key:(NSString *)key sn:(NSString *)sn;
- (void)reWirteImage:(UIImage *)image THREAD:(NSString* )thread CONTENT:(NSString *)content;
- (void)pushTest:(NSString *)user_name;

@end
