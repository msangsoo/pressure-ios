//
//  MANetwork.h
//  MediCare
//
//  Created by yunyungjeong on 2016. 6. 14..
//  Copyright © 2016년 SungChangmin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MANetwork : NSObject


+ (void)PostDataAsync:(NSString *)URLString paramString:(id)parameters success:(void (^)(id resultSet))completeBlock failure:(void(^)(NSError * error))errorBlock;

+ (NSDictionary *)POSTSync:(NSString *)strFullUrl viewState:(NSString *)strViewState eventValidation:(NSString*)strEventValidation paramString:(NSDictionary *)parameters fileName:(NSMutableArray*)arrayAddFileName  imageList:(NSMutableArray *)arrayImageList;

+ (NSDictionary *)POSTSync1:(NSString *)strFullUrl viewState:(NSString *)strViewState eventValidation:(NSString*)strEventValidation paramString:(NSDictionary *)parameters imageList:(NSMutableArray *)arrayImageList;

+ (void)PostMultipartDataAsync:(NSString *)URLString paramString:(id)parameters success:(void (^)(id resultSet))completeBlock failure:(void(^)(NSError * error))errorBlock;

+ (NSData *)resizedImageData:(UIImage *)image;

+(NSDictionary*)POSTSync:(NSString *)URLString parameters:(id)parameters;

+(NSDictionary*)GetSync:(NSString *)fullUrl;

+(NSString *) randomStringWithLength: (int) len;
@end
