//
//  MANetwork.m
//  MediCare
//
//  Created by yunyungjeong on 2016. 6. 14..
//  Copyright © 2016년 SungChangmin. All rights reserved.
//
#import "UIImage+Resize.h"
#import "MANetwork.h"
#import "AFNetworking.h"
#import "SBJson.h"
#import "Reachability.h"

#define EUCKRStringEncoding -2147482590

@implementation MANetwork

+ (void)PostDataAsync:(NSString *)URLString paramString:(id)parameters success:(void (^)(id resultSet))completeBlock failure:(void(^)(NSError * error))errorBlock
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    UIView *parentView = [[UIView alloc] init];
    [parentView setFrame:[[[[UIApplication sharedApplication] windows] firstObject] frame]];
    [parentView setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:parentView];
    [parentView setBackgroundColor:[UIColor clearColor]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [view setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [view.layer setCornerRadius:4];
    [view setClipsToBounds:YES];
    [parentView addSubview:view];
//    [view setBackgroundColor:UIColorFromRGB(0x000000, 0.6)];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    [activityIndicator setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    activityIndicator.hidesWhenStopped = NO;
    [activityIndicator startAnimating];
    
    [parentView addSubview:activityIndicator];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:parameters options:0 error:&err];
    NSLog(@"err:%@",err);
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    parameters = @{@"strJson":myString};
    
    
    NSString* string = [NSString stringWithFormat:@"%@",URLString];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    if ([parameters[@"DOCNO"]isEqualToString:@"DZ002"]||[parameters[@"DOCNO"]isEqualToString:@"DZ003"]||[parameters[@"DOCNO"]isEqualToString:@"DZ004"]||[parameters[@"DOCNO"]isEqualToString:@"DZ005"]||[parameters[@"DOCNO"]isEqualToString:@"DZ006"]) {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
    NSLog(@"parameters:\n%@",parameters);
    [manager POST:string parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
        
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([responseObject isKindOfClass:[NSDate class]]) {
            NSData * data = (NSData *)responseObject;
            NSError* error;
            NSDictionary *itemDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//
            NSLog(@"\n=================Start================\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,itemDict);
            completeBlock((NSDictionary*) itemDict);
        } else if ([responseObject isKindOfClass:[NSMutableDictionary class]]) {
            
            NSString *str = [NSString stringWithFormat:@"%@", responseObject[@"d"]];
            NSError *jsonError;
            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,json);
            completeBlock((NSDictionary *) json);
            
        } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSString *str = [NSString stringWithFormat:@"%@", responseObject[@"d"]];
            NSError *jsonError;
            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,json);
            completeBlock((NSDictionary *) json);
        } else if ([responseObject isKindOfClass:[NSArray class]]) {
            NSLog(@"\n===================Start==============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            completeBlock((NSArray*) responseObject);
        } else {
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            completeBlock((NSDictionary*) responseObject);
        }
        [parentView removeFromSuperview];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"error:%@",error);
        if (error.code ==-1001) {
            NSLog(@"시간 초과");
            
        }
        errorBlock((NSError*) error);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [parentView removeFromSuperview];
    }];
    
    
}

+ (void)PostMultipartDataAsync:(NSString *)URLString paramString:(id)parameters success:(void (^)(id resultSet))completeBlock failure:(void(^)(NSError * error))errorBlock
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    UIView *parentView = [[UIView alloc] init];
    [parentView setFrame:[[[[UIApplication sharedApplication] windows] firstObject] frame]];
    [parentView setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:parentView];
    [parentView setBackgroundColor:[UIColor clearColor]];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [view setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [view.layer setCornerRadius:4];
    [view setClipsToBounds:YES];
    [parentView addSubview:view];
    //    [view setBackgroundColor:UIColorFromRGB(0x000000, 0.6)];

    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    [activityIndicator setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    activityIndicator.hidesWhenStopped = NO;
    [activityIndicator startAnimating];

    [parentView addSubview:activityIndicator];

    NSError * err = nil;
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:parameters options:0 error:&err];
//    NSLog(@"err:%@",err);
//    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    parameters = @{@"strJson":myString};

//    NSURL *url = [NSURL URLWithString:URLString];
//    //3. request 객체
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:url];
//    [request setHTTPMethod:@"POST"];
//    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
//
//    [request addValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
//    [request setValue:@"windows-949,utf-8" forHTTPHeaderField:@"Accept-Charset"];
//    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
//    [request setValue:@"max-age=0" forHTTPHeaderField:@"Cache-Control"];
//    [request addValue:@"" forHTTPHeaderField:@"Origin"];
//
//    //4. 멀티 파트를 위한 기초 설정 스트링 정의
//    //Boundary String..
//    NSString *boundary = @"----WebKitFormBoundaryTGNE4W5X9ZSBF0rC";
//
//    //5. Body 객체 생성 start ======================
//    NSMutableData *body = [NSMutableData data];
//    //시작
//    // -- 파일 명 이미지 객체 파라미터
//
//
//    NSMutableDictionary* multipart = [@{} mutableCopy];
//    for(NSString* k in parameters)
//    {
//        if([parameters[k] isKindOfClass:[UIImage class]])
//        {
//            multipart[k] = parameters[k];
//        }
//    }
//    for(NSString* k in multipart)
//    {
//        [parameters removeObjectForKey:k];
//    }
//
//
//
//    int index=0;
//    for (NSString *k in parameters) {
//        if([parameters[k] isKindOfClass:[UIImage class]])
//        {
//            UIImage* image = parameters[k];
//            NSString * strImageName = [NSString stringWithFormat:@"%@.jpg",[self randomStringWithLength:8]];
//            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",arrayAddFileName[index], strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[ NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[MANetwork resizedImageData:image]];
//        }
//        index++;
//    }
//
//
//    // -- Hidden Param  __VIEWSTATE
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n%@", strViewState] dataUsingEncoding:NSUTF8StringEncoding]];
//    // -- Hidden Param  __EVENTVALIDATION
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__EVENTVALIDATION\"\r\n\r\n%@", strEventValidation] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    // -- Hidden Param  ImageButton1.x
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imgBtn.x\"\r\n\r\n%@", @"27"] dataUsingEncoding:NSUTF8StringEncoding]];
//    // -- Hidden Param  ImageButton1.y
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imgBtn.y\"\r\n\r\n%@", @"22"] dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:NSUTF8StringEncoding]];
//        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:EUCKRStringEncoding]];
//    }];
//
//
//    //-- 끝
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    //Contents Type
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
//
//
//    //      NSLog(@"send Data:%@", [boundary string);
//    [request setHTTPBody:body];
//    //5. Body 객체 생성 end ======================
//
//
//    //    NSLog(@"send Data:%@", [request description]);
//
//    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    //    [connection start];
//    //
//    //    if (connection == nil)
//    //        NSLog(@"Connect error");
//    //    else
//    //        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//
//    NSURLResponse* response;
//    NSError* error = nil;
//    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
//
//    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:rq delegate:self];
//    //    [connection start];
//    NSString *resultStr = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
//    NSLog(@"resultStr:%@",resultStr);
//
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//
//    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@""];
//    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@""];
//    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@""];
//
//    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version='1.0' encoding='UTF-8'?>" withString:@""];
//    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns='http://tempuri.org/'>" withString:@""];
//
//
//
//    //    <?xml version='1.0' encoding='UTF-8'?><string xmlns='http://tempuri.org/'>
//
//    //    NSLog(@"Response Data : %@", resultStr);
//
//    SBJsonParser *parser = [SBJsonParser new];
//    NSDictionary *responseBody = [parser objectWithString:resultStr];
//    //    NSLog(@"responseBody:%@",responseBody);
//    //     [[AppModal sharedApp] stopLoading];
//    return responseBody;

    NSString* string = [NSString stringWithFormat:@"%@",URLString];

//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];

    NSMutableDictionary* multipart = [@{} mutableCopy];

    for(NSString* k in parameters)
    {
        if([parameters[k] isKindOfClass:[UIImage class]])
        {
            multipart[k] = parameters[k];
        }
    }
    for(NSString* k in multipart)
    {
        [parameters removeObjectForKey:k];
    }



    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:string parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        for(NSString* k in parameters)
//        {
//            [formData appendPartWithFormData:[[NSString stringWithFormat:@"%@",parameters[k]] dataUsingEncoding:NSUTF8StringEncoding] name:k];
//        }

        for(NSString* k in multipart)
        {
            UIImage* image = multipart[k];
            [formData appendPartWithFileData:[MANetwork resizedImageData:image] name:k fileName:[NSString stringWithFormat:@"%@.jpeg",k] mimeType:@"image/jpeg"];
        }
        } error:&err];

    if(err != nil)
    {
        NSLog(@"%@",err);
        return;
    }

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
//    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"content-type"];

    NSLog(@"parameters:\n%@",parameters);

    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if(error != nil)
        {
            NSLog(@"error:%@",error);
            if (error.code ==-1001) {
                NSLog(@"시간 초과");
            }
            errorBlock((NSError*) error);
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [parentView removeFromSuperview];

        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([responseObject isKindOfClass:[NSDate class]]) {
            NSData * data = (NSData *)responseObject;
            NSError* error;
            NSDictionary *itemDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            //
            NSLog(@"\n=================Start================\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,itemDict);
            completeBlock((NSDictionary*) itemDict);
        } else if ([responseObject isKindOfClass:[NSMutableDictionary class]]) {

            NSString *str = [NSString stringWithFormat:@"%@", responseObject[@"d"]];
            NSError *jsonError;
            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,json);
            completeBlock((NSDictionary *) json);

        } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
//            NSString *str = [NSString stringWithFormat:@"%@", responseObject];
//            NSError *jsonError;
//            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
//            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
//                                                                 options:NSJSONReadingMutableContainers
//                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            completeBlock((NSDictionary *) responseObject);
        } else if ([responseObject isKindOfClass:[NSArray class]]) {
            NSLog(@"\n===================Start==============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            completeBlock((NSArray*) responseObject);
        } else {
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            completeBlock((NSDictionary*) responseObject);
        }
        [parentView removeFromSuperview];
    }];
    [uploadTask resume];
}

+(void)postJsonStringRequest:(NSString *)restApi
                       prams:(id)parameters
                     success:(void (^)(id resObject))success
                     failure:(void (^)(NSError *error))failure
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    UIView *parentView = [[UIView alloc] init];
    [parentView setFrame:[[[[UIApplication sharedApplication] windows] firstObject] frame]];
    [parentView setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:parentView];
    [parentView setBackgroundColor:[UIColor clearColor]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [view setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    [view.layer setCornerRadius:4];
    [view setClipsToBounds:YES];
    [parentView addSubview:view];
    //    [view setBackgroundColor:UIColorFromRGB(0x000000, 0.6)];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    [activityIndicator setCenter:CGPointMake([[[[UIApplication sharedApplication] windows] firstObject] bounds].size.width/2, [[[[UIApplication sharedApplication] windows] firstObject] bounds].size.height/2)];
    activityIndicator.hidesWhenStopped = NO;
    [activityIndicator startAnimating];
    [parentView addSubview:activityIndicator];
    
    NSString *string = [NSString stringWithFormat:@"%@",restApi];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    
    if ([parameters[@"DOCNO"]isEqualToString:@"DM001"]||[parameters[@"DOCNO"]isEqualToString:@"DM002"]||[parameters[@"DOCNO"]isEqualToString:@"DM003"]||[parameters[@"DOCNO"]isEqualToString:@"DM004"]||[parameters[@"DOCNO"]isEqualToString:@"DM005"]||[parameters[@"DOCNO"]isEqualToString:@"DM006"]) {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
    
    NSLog(@"parameters:\n%@",parameters);
    
    [manager POST:string parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([responseObject isKindOfClass:[NSDate class]]) {
            NSData * data = (NSData *)responseObject;
            NSError* error;
            NSDictionary *itemDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSLog(@"\n=================Start================\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,itemDict);
            success((NSDictionary*) itemDict);
        } else if ([responseObject isKindOfClass:[NSMutableDictionary class]]) {
            
            NSString *str = [NSString stringWithFormat:@"%@", responseObject[@"d"]];
            NSError *jsonError;
            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,json);
            success((NSDictionary *) json);
            
        } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSString *str = [NSString stringWithFormat:@"%@", responseObject[@"d"]];
            NSError *jsonError;
            NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,json);
            success((NSDictionary *) json);
        } else if ([responseObject isKindOfClass:[NSArray class]]) {
            NSLog(@"\n===================Start==============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            success((NSArray*) responseObject);
        } else {
            NSLog(@"\n====================Start=============\n파라미터 :\n%@\n조회 결과 :\n%@\n=================================",parameters,responseObject);
            success((NSDictionary*) responseObject);
        }
        [parentView removeFromSuperview];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [parentView removeFromSuperview];
    }];
}

+(NSDictionary *)GetSync:(NSString *)fullUrl {
    
    if(![self connectedToNetwork])
    {
        [self onShowNeworkErr];
        NSLog(@"Connect error");
        return nil;
    }
    
    
    //        [[AppModal sharedApp] startLoading];
    //    NSURL *url = [NSURL URLWithString:HOST_URL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:fullUrl]];
    //    [request setHTTPMethod:@"GET"];
    
    //    //    NSData* json = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    //    NSData *jsonData = [[NSString stringWithFormat:@"strJson=%@", parameters] dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    [request setHTTPBody:jsonData];
    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:[NSString stringWithFormat:@"%ld", (long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    //
    NSURLResponse* response;
    NSError* error = nil;
    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:rq delegate:self];
    //    [connection start];
    NSString *resultStr = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@""];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version='1.0' encoding='UTF-8'?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns='http://tempuri.org/'>" withString:@""];
    
    
    
    //    <?xml version='1.0' encoding='UTF-8'?><string xmlns='http://tempuri.org/'>
    
    //    NSLog(@"Response Data : %@", resultStr);
    
    SBJsonParser *parser = [SBJsonParser new];
    NSDictionary *responseBody = [parser objectWithString:resultStr];
    NSLog(@"responseBody:%@",responseBody);
    //     [[AppModal sharedApp] stopLoading];
    return responseBody;
    
}

/**
 이미지 업로드 : 싱크로드 방식 method
 
 
 
 */
+ (NSDictionary *)POSTSync:(NSString *)strFullUrl viewState:(NSString *)strViewState eventValidation:(NSString*)strEventValidation paramString:(NSDictionary *)parameters fileName:(NSMutableArray*)arrayAddFileName  imageList:(NSMutableArray *)arrayImageList
{
    if(![self connectedToNetwork])
    {
        [self onShowNeworkErr];
        NSLog(@"Connect error");
        return nil;
    }
    //        [[AppModal sharedApp] startLoading];
    //1. URL 객체 구성
    NSURL *url = [NSURL URLWithString:strFullUrl];
    //3. request 객체
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    [request addValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
    [request setValue:@"windows-949,utf-8" forHTTPHeaderField:@"Accept-Charset"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"max-age=0" forHTTPHeaderField:@"Cache-Control"];
    [request addValue:@"" forHTTPHeaderField:@"Origin"];
    
    //4. 멀티 파트를 위한 기초 설정 스트링 정의
    //Boundary String..
    NSString *boundary = @"----WebKitFormBoundaryTGNE4W5X9ZSBF0rC";
    
    //5. Body 객체 생성 start ======================
    NSMutableData *body = [NSMutableData data];
    //시작
    // -- 파일 명 이미지 객체 파라미터
    
   
    
 
    
    int index=0;
    for (NSData *dataImage in arrayImageList) {
        NSString * strImageName = [NSString stringWithFormat:@"%@.jpg",[self randomStringWithLength:8]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",arrayAddFileName[index], strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:dataImage];
        index++;
    }
    
    
    // -- Hidden Param  __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n%@", strViewState] dataUsingEncoding:NSUTF8StringEncoding]];
    // -- Hidden Param  __EVENTVALIDATION
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__EVENTVALIDATION\"\r\n\r\n%@", strEventValidation] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // -- Hidden Param  ImageButton1.x
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imgBtn.x\"\r\n\r\n%@", @"27"] dataUsingEncoding:NSUTF8StringEncoding]];
    // -- Hidden Param  ImageButton1.y
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imgBtn.y\"\r\n\r\n%@", @"22"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:EUCKRStringEncoding]];
    }];
    
    
    //-- 끝
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Contents Type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    //      NSLog(@"send Data:%@", [boundary string);
    [request setHTTPBody:body];
    //5. Body 객체 생성 end ======================
    
    
    //    NSLog(@"send Data:%@", [request description]);
    
    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //    [connection start];
    //
    //    if (connection == nil)
    //        NSLog(@"Connect error");
    //    else
    //        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURLResponse* response;
    NSError* error = nil;
    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:rq delegate:self];
    //    [connection start];
    NSString *resultStr = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"resultStr:%@",resultStr);
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@""];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version='1.0' encoding='UTF-8'?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns='http://tempuri.org/'>" withString:@""];
    
    
    
    //    <?xml version='1.0' encoding='UTF-8'?><string xmlns='http://tempuri.org/'>
    
    //    NSLog(@"Response Data : %@", resultStr);
    
    SBJsonParser *parser = [SBJsonParser new];
    NSDictionary *responseBody = [parser objectWithString:resultStr];
    //    NSLog(@"responseBody:%@",responseBody);
    //     [[AppModal sharedApp] stopLoading];
    return responseBody;
    
    
}
+(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}


/**
 진료 의뢰서 method
 */
+ (NSDictionary *)POSTSync1:(NSString *)strFullUrl viewState:(NSString *)strViewState eventValidation:(NSString*)strEventValidation paramString:(NSDictionary *)parameters imageList:(NSMutableArray *)arrayImageList
{
    if(![self connectedToNetwork])
    {
        [self onShowNeworkErr];
        NSLog(@"Connect error");
        return nil;
    }
    //        [[AppModal sharedApp] startLoading];
    //1. URL 객체 구성
    NSURL *url = [NSURL URLWithString:strFullUrl];
    //3. request 객체
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    [request addValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
    [request setValue:@"windows-949,utf-8" forHTTPHeaderField:@"Accept-Charset"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"max-age=0" forHTTPHeaderField:@"Cache-Control"];
    [request addValue:@"" forHTTPHeaderField:@"Origin"];
    
    //4. 멀티 파트를 위한 기초 설정 스트링 정의
    //Boundary String..
    NSString *boundary = @"----WebKitFormBoundaryTGNE4W5X9ZSBF0rC";
    
    //5. Body 객체 생성 start ======================
    NSMutableData *body = [NSMutableData data];
    //시작
    // -- 파일 명 이미지 객체 파라미터
    NSArray * arrayFileName = @[@"UPFILE1",@"UPFILE2",@"UPFILE3",@"UPFILE4",@"UPFILE5"];
    
    int index=0;
    for (NSData *dataImage in arrayImageList) {
          NSString * strImageName = [NSString stringWithFormat:@"%@.jpg",[self randomStringWithLength:8]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",arrayFileName[index], strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:dataImage];
        index++;
    }
    
    
    // -- Hidden Param  __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n%@", strViewState] dataUsingEncoding:NSUTF8StringEncoding]];
    // -- Hidden Param  __EVENTVALIDATION
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__EVENTVALIDATION\"\r\n\r\n%@", strEventValidation] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // -- Hidden Param  ImageButton1.x
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ImageButton1.x\"\r\n\r\n%@", @"27"] dataUsingEncoding:NSUTF8StringEncoding]];
    // -- Hidden Param  ImageButton1.y
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ImageButton1.y\"\r\n\r\n%@", @"22"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key,obj] dataUsingEncoding:EUCKRStringEncoding]];
    }];
    
    
    //-- 끝
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Contents Type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    //      NSLog(@"send Data:%@", [boundary string);
    [request setHTTPBody:body];
    //5. Body 객체 생성 end ======================
    
    
    //    NSLog(@"send Data:%@", [request description]);
    
    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //    [connection start];
    //
    //    if (connection == nil)
    //        NSLog(@"Connect error");
    //    else
    //        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURLResponse* response;
    NSError* error = nil;
    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    //    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:rq delegate:self];
    //    [connection start];
    NSString *resultStr = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"resultStr:%@",resultStr);
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@""];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version='1.0' encoding='UTF-8'?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns='http://tempuri.org/'>" withString:@""];
    
    
    
    //    <?xml version='1.0' encoding='UTF-8'?><string xmlns='http://tempuri.org/'>
    
    //    NSLog(@"Response Data : %@", resultStr);
    
    SBJsonParser *parser = [SBJsonParser new];
    NSDictionary *responseBody = [parser objectWithString:resultStr];
    //    NSLog(@"responseBody:%@",responseBody);
    //     [[AppModal sharedApp] stopLoading];
    return responseBody;
    
    
}


/**
 네트워크 오류 메세지 method
 
 
 
 */
+ (void)onShowNeworkErr
{
    NSString * strMsg =@"이동 통신망 또는 무선 네트워크 접속이 연결되지 않았거나 원할하지 않습니다.\n확인 후 다지 접속해 주세요.";
    
    UIApplication * delegate = [[UIApplication sharedApplication]delegate];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:nil message:strMsg delegate:delegate cancelButtonTitle:nil otherButtonTitles:@"확인", nil];
    [alert show];
}
/**
 네트워크 체크 method
 
 
 
 */
+ (BOOL) connectedToNetwork
{
    /*
     www.apple.com URL을 통해 인터넷 연결 상태를 확인합니다.
     ReachableViaWiFi && ReachableViaWWAN
     : Wi-fi와 3G 둘다 안된다면 인터넷이 연결되지 않은 상태입니다.
     */
    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
        internet = NO;
    } else {
        internet = YES;
    }
    return internet;
}



+ (NSData *)resizedImageData:(UIImage *)image {
    if (image == nil)
        return nil;
    
//    NSUInteger size200k = 1024 * 200;
//    NSUInteger dataSize = 1024 * 201;
    
//    CGSize standardSize = CGSizeMake(960, 1357);
    CGFloat quality = 0.1;
    NSData *data = nil;
    
//    if (image.size.width * image.size.height > standardSize.width * standardSize.height) {
//        if (image.size.width > image.size.height)
//            standardSize = CGSizeMake(standardSize.height, standardSize.width);
//        CGSize targetSize = CGSizeMake(standardSize.width, floor(standardSize.width * image.size.height / image.size.width));
//        
//        image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:targetSize interpolationQuality:kCGInterpolationDefault];
//    }
    
//    while(dataSize > size200k) {
        data = UIImageJPEGRepresentation(image, quality);
//        dataSize = data.length;
//        quality -= 0.05;
//        NSLog(@"%lu", dataSize);
//    }
    return data;
}

@end
