//
//  API.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 8. 27..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#define     SERVER_DOMAIN           @"https://wkd.walkie.co.kr"
#define     SERVER_PATH             [NSString stringWithFormat:@"%@/KBT/ws.asmx/getJson", SERVER_DOMAIN]

#define     SERVER_DOMAIN2          @"http://m.shealthcare.co.kr"
#define     SERVER_PATH2            [NSString stringWithFormat:@"%@/HL_MED/WebService/MED_MOBILE_CALL.asmx/MED_mobile_Call", SERVER_DOMAIN2]
#define     SERVER_UPLOADURL        [NSString stringWithFormat:@"%@/HL_MED/WebService/HLMED_PROFILE_upload.ashx", SERVER_DOMAIN2]

#define     FOOD_UPLOAD_PATH        [NSString stringWithFormat:@"%@/HL_MED/WebService/MED_food_upload.ashx", SERVER_DOMAIN2]
#define     HOSPITAL_UPLOAD_PATH    [NSString stringWithFormat:@"%@/HL_MED/WebService/hwg_hospital_upload.ashx", SERVER_DOMAIN2]
//http://m.shealthcare.co.kr/HL_MED/WebService/MED_food_upload.ashx
//CODE



//SIGN
#define SIGN_CODE @"KM001"
#define SIGN_LOG_CODE @"KM002"
#define SIGN_LOG_CONFIRM_CODE @"KM003"

//LUCKY BOX
#define LUCKY_OPEN_CODE  @"KG001"
#define LUCKY_RESULT_CODE @"KG002"
#define LUCKY_VACU_CODE @"KG003"
#define LUCKY_GIVEADD_CODE @"KG004"
#define LUCKY_EXPEADD_CODE @"KG005"

//인증초기화
#define AUTH_INIT_CODE  @"KG006"

//GROW
#define GROW_RESULT @"KR001"

//NOTI
#define NOTI_LIST_CODE @"KA001"
#define NOTI_LIST_CODE2 @"KA004"
#define NOTI_DETAIL_CODE @"KA002"
#define NOTI_NEW_CODE @"KA003"

//USERINFO
#define USER_INFO_CODE @"KI001"

//KEYWORD
#define KEYWORD_INFO_CODE @"KK001"

// 체험 서비스 이용내역
#define EXP_USE_HIS_CODE @"KH001"
#define EXP_USE_INSERT_CODE @"KH002"
