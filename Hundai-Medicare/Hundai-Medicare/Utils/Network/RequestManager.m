//
//  RequestManager.m
//  Showping
//
//  Created by ShinDongkwon on 2016. 6. 20..
//  Copyright © 2016년 ShinDongkwon. All rights reserved.
//

#import "RequestManager.h"
//#import "Utils.h"

@implementation RequestManager


+ (RequestManager *)sharedInstance {
    static RequestManager *instance = nil;
    
    if (instance == nil) {
        instance = [[RequestManager alloc] init];
    }
    
    return instance;
}

- (void)sendRequest:(id)delegate withParameters:(NSDictionary *)parameters success:(NSString *)success failure:(NSString *)failure {
    
    NSMutableDictionary * mutableDict = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    
    [mutableDict addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"300",@"insures_code",@"ios",@"app_code", nil]];
   
    
    NSString *param = [NSString stringWithFormat:@"json=%@", [mutableDict JSONRepresentation]];
    
    [self sendRequest:self url:SERVER_PATH withParameters:param success:@"responseList:" failure:nil];
}

- (void)sendRequest:(id)delegate url:(NSString *)url withParameters:(NSString *)parameters success:(NSString *)success failure:(NSString *)failure {
    
//    model = [HanwhaModel instance];
//    [model indicatorActive];
    
    self.delegate = delegate;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    NSData* jsonDataObj = [NSJSONSerialization dataWithJSONObject:parameters
                                                          options:NSJSONWritingPrettyPrinted error:nil];

    
    NSLog(@"========================================");
    NSLog(@"request send : %@", parameters);
    
    
    [manager POST:url parameters:jsonDataObj progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // success..
        NSLog(@"========================================");
        NSLog(@"request success : %@", responseObject);
        
        if ([responseObject objectForKey:@"returncode"] && [[responseObject objectForKey:@"returncode"] intValue] == 201) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                           message:[responseObject objectForKey:@"message"] delegate:nil
                                                 cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if (self.delegate != nil && success != nil) {
            [self.delegate requestSuccess:responseObject withAction:success];
        }
//        [model indicatorHidden];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // failure..
        NSLog(@"========================================");
        NSLog(@"request failure : @%@", error);
        
        NSString *errorMsg = [NSString stringWithFormat:@"error : %@", error];
        if (self.delegate != nil && failure != nil) {
            [self.delegate requestFailure:errorMsg withAction:failure];
        }
//        [model indicatorHidden];
        
    }];
}

- (void) uplodeImages:(id)delegate image:(UIImage *)image withName:(NSString *)name withURL:(NSString *)url withParameters:(NSDictionary *)parameters success:(NSString *)success failure:(NSString *)failure {
    
    
//    model = [HanwhaModel instance];
//    [model indicatorActive];
    
    
    self.delegate = delegate;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSLog(@"========================================");
    NSLog(@"request send : %@", parameters);
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (image) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.6f) name:name fileName:@"test.jpg" mimeType:@"image/jpeg"];
        }
        
        // etc.
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        // success..
        NSLog(@"========================================");
        NSLog(@"request success : %@", responseObject);
        
        if (self.delegate != nil && success != nil) {
            [self.delegate requestSuccess:responseObject withAction:success];
        }
//        [model indicatorHidden];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        // failure..
        NSLog(@"========================================");
        NSLog(@"request failure : @%@", error);
        
        NSString *errorMsg = [NSString stringWithFormat:@"error : %@", error];
        if (self.delegate != nil && failure != nil) {
            [self.delegate requestFailure:errorMsg withAction:failure];
        }
//        [model indicatorHidden];
    }];
}

@end
