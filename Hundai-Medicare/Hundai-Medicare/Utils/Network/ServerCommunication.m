 //
//  ServerCommunication.m
//  HealthCare
//
//  Created by jangkun lee on 13. 3. 5..
//  Copyright (c) 2013년 jangkun lee. All rights reserved.
//

#import "ServerCommunication.h"
#import "API.h"

@interface ServerCommunication ( Private )
-(void)responseServerCommunication:(ASIHTTPRequest*)result;
-(void)responseError:(NSError*)err;
@end

@implementation ServerCommunication

@synthesize delegate;

-(id)init {
    if ((self = [super init])) {
        _model = [Model instance];
        networkCheck = [[NetworkCheck alloc] init];
    }
    insures_code = @"304";
    return self;
}


#pragma mark - 공통파랑미터(필요시 추가)
- (NSDictionary*)addDefaultParams:(NSDictionary*)params {
    Tr_login *login = [_model getLoginData];
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic addEntriesFromDictionary:params];
    [dic setValue:insures_code forKey:@"insures_code"];
    [dic setValue:@"ios" forKey:@"app_code"];
//    [dic setValue:@"1871" forKey:@"mber_sn"];   //1871
//    [dic setValue:@"20" forKey:@"mber_grad"];   //20     10:정회원 또는 20:준회원
   
    if (![params objectForKey:@"mber_sn"]) {
        
        NSString *mber_sn = login.mber_sn;
        if (mber_sn==nil || [mber_sn length]==0) {
            mber_sn = @"";
        }
        [dic setValue:mber_sn forKey:@"mber_sn"];   //login.mber_sn
    }
    
    if (![params objectForKey:@"mber_grad"]) {
        [dic setValue:login.mber_grad forKey:@"mber_grad"];   //mber_grad     10:정회원 또는 20:준회원
    }

    return dic;
}

#pragma mark - send
-(void)serverCommunicationData:(NSDictionary*)parametter {
    
    if (![networkCheck connectedToNetwork])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"네트워크 상태가 고르지 않습니다.\n앱을 다시 시작하여 주세요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
        [alertView release];
        
        return;
    }
    
    [_model indicatorActive];
    
    httpRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_PATH2]];
    
    [httpRequest setDidFinishSelector:@selector(responseServerCommunication:)];
    [httpRequest setDidFailSelector:@selector(responseError:)];
    
    [httpRequest addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded;charset=utf-8"];
    [httpRequest setRequestMethod:@"POST"];
    
    [httpRequest setTimeOutSeconds:30];
    [httpRequest setDelegate:self];
    
    parametter = [self addDefaultParams:parametter];
    
    NSData* data = [NSJSONSerialization dataWithJSONObject:parametter
                                                   options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    if (json) {
        json    = [json stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }

    NSLog(@"Server Url : %@", SERVER_PATH2);
    NSLog(@"Request Json : %@", json);
    
    [httpRequest setPostValue:json forKey:@"json"];
    
    [httpRequest startAsynchronous ];
}


-(void)responseServerMberCheck:(ASIHTTPRequest *)result
{
    NSString *resultStr = [ result responseString ];
    resultStr = [ resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@"" ];
    resultStr = [ resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@"" ];
    resultStr = [ resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@"" ];
    id json = [ resultStr objectFromJSONString ];
    
    if(delegate != nil && [delegate conformsToProtocol:@protocol(ServerCommunicationDelegate)]) {
        if([delegate respondsToSelector:@selector(delegateResultString:CODE:)]) {
            
            [ delegate delegateResultString:@"yes" CODE:@"mber_check" ];
        }
    }
    
    
    [_model indicatorHidden];
}


#pragma mark - response
-(void)responseServerCommunication:(ASIHTTPRequest *)result {
    
    NSString *resultStr = [result responseString];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"<string xmlns=\"http://tempuri.org/\">" withString:@""];
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"</string>" withString:@"" ];
    id json = [resultStr objectFromJSONString];
    NSLog(@" ===================================== ");
    NSLog(@"Return json : %@", json);
    NSLog(@" ===================================== ");
    if(delegate != nil && [delegate conformsToProtocol:@protocol(ServerCommunicationDelegate)]) {
        if([delegate respondsToSelector:@selector(delegateResultData:)]) {
            
            NSError *jsonError;
            NSData *objectData = [resultStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonReturn = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            
            [delegate delegateResultData:jsonReturn];
            
        }
    }
    
    [_model indicatorHidden];
}

-(void)writeImage:(UIImage*)image {
    [_model indicatorActive];
    
    Tr_login *login = [_model getLoginData];

    NSString *urlString = [NSString stringWithFormat:@"%@?cmpny_code=%@&mber_sn=%@",
                            SERVER_UPLOADURL, insures_code, login.mber_sn];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	
    NSString *boundary = @"----WebKitFormBoundaryBeNfEjTuFHIPBBiO";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data;charset=utf-8;boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    NSString *filename = [ NSString stringWithFormat:@"IMAGES%@", login.mber_sn];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"img_file\"; filename=\"%@.jpg\"\r\n", filename ]  dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    
	[request setHTTPBody:body];
    
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:0x80000000 + kCFStringEncodingDOSKorean];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                         options:kNilOptions
                                                           error:&error];
    
    NSLog(@"file_name:%@", [json objectForKey:@"file_name"]);
    NSLog(@"file_url:%@", [json objectForKey:@"file_url"]);
    NSLog(@"data_yn:%@", [json objectForKey:@"data_yn"]);
                          
    if ([[json objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            if([delegate respondsToSelector:@selector(delegateResultData:)]) {
                [ delegate delegateResultData:json];
            }
    }else {
        UIAlertView *alertView = [ [ UIAlertView alloc ] initWithTitle:@"" message:@"인터넷 접속상태가 고르지 않습니다.\n잠시후에 다시 시도해 주세요" delegate:nil cancelButtonTitle:@"예" otherButtonTitles:nil ];
        [ alertView show ];
        [ alertView release ];
    }
}

-(void)writeImage:(UIImage*)image key:(NSString *)key sn:(NSString *)sn {
    [_model indicatorActive];
    
    // 식사 사진
    NSString *targetUrl = FOOD_UPLOAD_PATH;
    NSString *urlString = [NSString stringWithFormat:@"%@?cmpny_code=%@&%@=%@&mber_sn=%@",
                           targetUrl, @"304", key, sn, [_model getLoginData].mber_sn];
    // 병원방문 사진
    if([key isEqualToString:@"req_sn"]) {
        targetUrl = HOSPITAL_UPLOAD_PATH;
        urlString = [NSString stringWithFormat:@"%@?%@=%@", targetUrl, key, sn];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"----WebKitFormBoundaryBeNfEjTuFHIPBBiO";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    NSString *filename = [ NSString stringWithFormat:@"%@", sn];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"%@.png\"\r\n", filename ]  dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:0x80000000 + kCFStringEncodingDOSKorean];
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                         options:kNilOptions
                                                           error:&error];
    
    NSLog(@"json:%@", [json objectForKey:@"file_name"]);
    NSLog(@"json:%@", [json objectForKey:@"sn"]);
    NSLog(@"json:%@", [json objectForKey:@"data_yn"]);
    
    if ([[json objectForKey:@"data_yn"] isEqualToString:@"Y"])
    {
        if([delegate respondsToSelector:@selector(delegateResultData:)]) {
            [ delegate delegateResultData:json];
        }
    }
    else
    {
        UIAlertView *alertView = [ [ UIAlertView alloc ] initWithTitle:@"" message:@"인터넷 접속상태가 고르지 않습니다.\n잠시후에 다시 시도해 주세요" delegate:nil cancelButtonTitle:@"예" otherButtonTitles:nil ];
        [ alertView show ];
        [ alertView release ];
    }
}

-(void)reWirteImage:(UIImage*)image THREAD:(NSString*)thread CONTENT:(NSString *)content {
    [_model indicatorActive];
    
    NSDateFormatter *dateFormatter = [ [ NSDateFormatter alloc ] init ];
    [ dateFormatter setDateFormat:@"yyyyMMddHHmmss" ];
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:SERVER_PATH ];
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	
	NSString *boundary = @"----WebKitFormBoundaryBeNfEjTuFHIPBBiO";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n/wEPDwUKLTYwNDI4OTMzMWQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFDEltYWdlQnV0dG9uMQxX3FUfcrO6IUEx2U9H9jVpd1Epn4Q/FtVzzmKi3qNa" ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"__EVENTVALIDATION\"\r\n\r\n/wEWBgKSw7v3BAL064mfCQLW4bf/BALRqJuDDALE0o/aCwLSwpnTCClDcBSTfS9WCTE9leezjQt6AlZh/Sofg3A+nY9HR0Fz" ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ImageButton1.x\"\r\n\r\n22"] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ImageButton1.y\"\r\n\r\n17"] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"content\"\r\n\r\n%@", content ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_number\"\r\n\r\n%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"" ] ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"thread\"\r\n\r\n%@", thread ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"depth\"\r\n\r\n0" ] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    NSString *filename = [ NSString stringWithFormat:@"%@%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"" ], [ dateFormatter stringFromDate:[ NSDate date ] ] ];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"upfile\"; filename=\"%@.png\"\r\n", filename ]  dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
	[body appendData:[NSData dataWithData:imageData]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:0x80000000 + kCFStringEncodingDOSKorean]];
    
	[request setHTTPBody:body];
    
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [ [ NSString alloc ] initWithData:returnData encoding:0x80000000 + kCFStringEncodingDOSKorean ];
    
    if ( [ returnString isEqualToString:@"DATA_YN=Y" ] )
    {
        if(delegate != nil && [delegate conformsToProtocol:@protocol(ServerCommunicationDelegate)]) {
            if([delegate respondsToSelector:@selector(delegateResultString:CODE:)]) {
                [ delegate delegateResultString:@"Y" CODE:nil ];
            }
        }
    }
    else
    {
        if(delegate != nil && [delegate conformsToProtocol:@protocol(ServerCommunicationDelegate)]) {
            if([delegate respondsToSelector:@selector(delegateResultString:CODE:)]) {
                [ delegate delegateResultString:@"N" CODE:nil ];
            }
        }
        
        UIAlertView *alertView = [ [ UIAlertView alloc ] initWithTitle:@"" message:@"인터넷 접속상태가 고르지 않습니다.\n잠시후에 다시 시도해 주세요" delegate:nil cancelButtonTitle:@"예" otherButtonTitles:nil ];
        [ alertView show ];
        [ alertView release ];
    }
}

#pragma mark - response error
-(void)responseError:(NSError*)err {
    UIAlertView *alertView = [ [ UIAlertView alloc ] initWithTitle:@"" message:@"인터넷 접속상태가 고르지 않습니다.\n잠시후에 다시 시도해 주세요" delegate:nil cancelButtonTitle:@"예" otherButtonTitles:nil ];
    [ alertView show ];
    [ alertView release ];
    
    [_model indicatorHidden];
}

#pragma mark - push
-(void)pushTest:(NSString*)user_name {
    httpRequest = [ ASIFormDataRequest requestWithURL:[ NSURL URLWithString:@"http://112.216.110.218:8088/PushService/insertdevice" ] ];
    [ httpRequest setDidFinishSelector:@selector(responsePush:) ];
    [ httpRequest setDidFailSelector:@selector(responseError:) ];
    [ httpRequest setRequestMethod:@"POST" ];
    [ httpRequest setTimeOutSeconds:30 ];
    [ httpRequest setDelegate:self ];
    [ httpRequest setPostValue:user_name forKey:@"user_name" ];
    [ httpRequest setPostValue:@"iphone" forKey:@"device_type" ];
    [ httpRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"" ] forKey:@"device_id" ];
    [ httpRequest startAsynchronous ];
}

-(void)responsePush:(ASIHTTPRequest*)result {
    NSLog(@"status = %d", result.responseStatusCode);
    if(delegate != nil && [delegate conformsToProtocol:@protocol(ServerCommunicationDelegate)]) {
        if([delegate respondsToSelector:@selector(delegatePush:)]) {
            [ delegate delegatePush:result.responseStatusCode];
        }
    }
}

@end
