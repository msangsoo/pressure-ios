//
//  RequestManager.h
//  Showping
//
//  Created by ShinDongkwon on 2016. 6. 20..
//  Copyright © 2016년 ShinDongkwon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "SBJson.h"
//#import "HanwhaModel.h"
#import "API.h"

@protocol RequestManagerDelegate <NSObject>

@optional
- (void)requestSuccess:(NSDictionary *)response withAction:(NSString *)success;
- (void)requestFailure:(NSString *)message withAction:(NSString *)failure;

@end

@interface RequestManager : NSObject {
    
//    HanwhaModel *model;
}

@property (weak, nonatomic) id <RequestManagerDelegate> delegate;

+ (RequestManager *)sharedInstance;
- (void)sendRequest:(id)delegate withParameters:(NSDictionary *)parameters success:(NSString *)success failure:(NSString *)failure;
- (void)sendRequest:(id)delegate url:(NSString *)url withParameters:(NSString *)parameters success:(NSString *)success failure:(NSString *)failure;
- (void) uplodeImages:(id)delegate image:(UIImage *)image withName:(NSString *)name withURL:(NSString *)url withParameters:(NSDictionary *)parameters success:(NSString *)success failure:(NSString *)failure;

@end
