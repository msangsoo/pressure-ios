//
//  NetworkCheck.h
//  HealthCare
//
//  Created by jangkun lee on 13. 4. 12..
//  Copyright (c) 2013년 jangkun lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkCheck : NSObject

- (BOOL)connectedToNetwork;

@end
