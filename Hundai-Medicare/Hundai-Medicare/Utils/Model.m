//
//  Model.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 4..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#define INDICATOR_TIMEOUT 15

@implementation Model

static Model *instance = nil;

+(id)instance {
    if( instance == nil ) {
        instance = [self new];
        [instance initInstance];
        [instance initSet];
        [instance initMedi];
    }
    return instance;
}

-(void)initSet {
    self._noticeGubun = @"";
}

-(void)initInstance {
    if (!_activityIndicatorView) {
        _activityIndicatorView = [CONTROLS_STORYBOARD instantiateViewControllerWithIdentifier:@"ActivityIndicator"];
        [_activityIndicatorView._activityIndicator stopAnimating];
        _isRqActive = NO;
    }
}

#pragma mark - health check init
-(void)initMedi {
    _checkMedicalArray = @[@"골다공증 발생 위험도",@"폐암 발생 위험도",@"뇌졸중 발생 위험도",@"대장암 발생 위험도",@"당뇨병 발생 위험도",@"자궁경부암 발생 위험도",@"심장병 발생 위험도",@"유방암 발생 위험도",@"난소암 발생 위험도"];
    _checkLevelArray = [[NSMutableArray alloc] initWithObjects:@"체크하기",@"체크하기",@"체크하기",@"체크하기",@"체크하기",@"체크하기",@"체크하기",@"체크하기",@"체크하기", nil];
    _checkResultArray = [[NSMutableArray alloc] initWithObjects:@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"],@[@"0",@"0"], nil];
}

-(void)setCheckLevelArray:(NSMutableArray *)checkLevelArray {
    [_checkLevelArray removeAllObjects];
    _checkLevelArray = [[NSMutableArray arrayWithArray:checkLevelArray] mutableCopy];
}
- (void)setCheckResultArray:(NSMutableArray *)checkResultArray {
    [_checkResultArray removeAllObjects];
    _checkResultArray = [[NSMutableArray arrayWithArray:checkResultArray] mutableCopy];
}

#pragma mark - indicatorActive
- (void)indicatorTimeOut:(id)sender {
    [_timerIndicator invalidate];
    _timerIndicator = nil;
    
    [self indicatorHidden];
}

- (void)indicatorActive {
    [self doIndicatorActivity:nil];
}

- (void)doIndicatorActivity:(NSString *)message {
    [self indicatorHidden];
    
    [_timerIndicator invalidate];
    _timerIndicator = nil;
    
    _isRqActive = YES;
    
    _timerIndicator = [NSTimer scheduledTimerWithTimeInterval:INDICATOR_TIMEOUT target:self selector:@selector(indicatorTimeOut:) userInfo:nil repeats:NO];
    
    [_activityIndicatorView._activityIndicator startAnimating];
    [_activityIndicatorView doInit];
    if (message != nil && [message length] > 0) {
        [_activityIndicatorView setLoadingMessage:message];
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.window addSubview:_activityIndicatorView.view];
    [appDelegate.window bringSubviewToFront:_activityIndicatorView.view];
}

- (void)indicatorHidden {
    if (_isRqActive) {
        [_activityIndicatorView._activityIndicator stopAnimating];
        [_activityIndicatorView.view removeFromSuperview];
        
        [_timerIndicator invalidate];
        _timerIndicator = nil;
    }
    _isRqActive = NO;
}

#pragma mark - login & informaion method
- (void)setLoginTypeData:(Tr_login *)data {
    login = data;
}

- (void)setLoginData:(NSDictionary *)data {
    login = [[Tr_login alloc] initWithData:data];
}

- (Tr_login *)getLoginData {
    return login;
}

- (void)setLogOut {
    login = nil;
    self.mber_id = nil;
}

- (void)setInformation:(NSDictionary *)data {
    getInformation = [[Tr_getInformation alloc] initWithData:data];
}

- (Tr_getInformation *)getInformation {
    return getInformation;
}

#pragma mark - token
- (NSString *)getToken {
    NSString *token = [Utils getUserDefault:APP_TOKEN];
    if ([token length] ==0) {
        token = @"";
    }
    return token;
}

#pragma mark - user info
/// 목표칼로리
- (int)getDefaultGoalCalori {
    
    //목표칼로리, 목표걸음수 계산
    Tr_login *login = [self getLoginData];
//    float bmi = 0.f;  // [login.mber_bmi floatValue];
    float rHeight = [login.mber_height floatValue] * 0.01f;
    float bmi = [[NSString stringWithFormat:@"%.1f", ([login.mber_bdwgh_app floatValue] / (rHeight * rHeight))] floatValue];
    int sex =  [login.mber_sex intValue];
    int height =  [login.mber_height intValue];
    
    float nWeight = 0.0f;                                   //현재 체중
    float bdwghGoal = [login.mber_bdwgh_goal floatValue];   //목표체중
    WeightDBWraper *db = [[WeightDBWraper alloc] init];
    NSArray *tempArr = [db getBottomResult];
    if(tempArr.count > 0){
        nWeight = [login.mber_bdwgh_app floatValue];
    }else{
        nWeight = [login.mber_bdwgh floatValue];
    }
    
    int bmiLevel = 0;
    //목표칼로리, 목표걸음수 계산
    if (bdwghGoal > nWeight) {
        bmiLevel = 0;
    } else if (bdwghGoal == nWeight) {
        bmiLevel = 1;
    } else if (bdwghGoal < nWeight) {
        bmiLevel = 2;
    }
    
    int targetCal = 0;  //목표소모칼로리/1일 (kcal)
    
    float mWeight;   //표준체중
    float mHeight = height * 0.01f;
    mHeight = [[NSString stringWithFormat:@"%.2f", mHeight] floatValue];
    
    if (sex == 1) {
        //남성
        mWeight = [[NSString stringWithFormat:@"%.1f", (mHeight * mHeight) * 22] floatValue];
    } else {
        //여성
        mWeight = [[NSString stringWithFormat:@"%.1f", (mHeight * mHeight) * 21] floatValue];
    }
    // 체중 증량 필요
    if (bmiLevel == 0) {
        switch (sex) {
            case 1:
                targetCal = 300;
                break;
            case 2:
                targetCal = 250;
                break;
        }
    }
    // 체중 유지 필요 : BMI 기준 정상체중군(18.5≤BMI≤22.9)이지만 현재 체중≤표준 체중.
    else if (bmiLevel == 1 && nWeight <= mWeight) {
        switch (sex) {
            case 1:
                targetCal = 400;
                break;
            case 2:
                targetCal = 300;
                break;
        }
    }
    // 체중 감량 필요 :  BMI 기준 정상체중군(18.5≤BMI≤22.9)이지만 현재체중>표준체중
    else if ((bmiLevel == 1 && nWeight > mWeight) || bmiLevel > 1) {
        switch (sex) {
            case 1:
                targetCal = 500;
                //                    switch (actqy) {
                //                        case 1:
                //                            targetCal = 450;
                //                            break;
                //                        case 2:
                //                            targetCal = 500;
                //                            break;
                //                        case 3:
                //                            targetCal = 600;
                //                            break;
                //                    }
                break;
            case 2:
                targetCal = 400;
                //                    switch (actqy) {
                //                        case 1:
                //                            targetCal = 375;
                //                            break;
                //                        case 2:
                //                            targetCal = 400;
                //                            break;
                //                        case 3:
                //                            targetCal = 500;
                //                            break;
                //                    }
                break;
        }
    }
    
    return targetCal;
}

/**
 * 목표 걸음수 계산
 */
- (NSString *)getStepTargetCalulator:(int)sex height:(float)height weight:(float)weight calrori:(int)calrori {
    
    double stepTarget = 0.0f;
    NSString *rtnValue;
    if (sex == 1) {
        stepTarget = ((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60 / (height / 100 / (-0.5625 * 3.5 + 4.2294))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
    } else {
        stepTarget = ((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60 / (height / 100 / (-0.5133 * 3.5 + 4.1147))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
    }
    rtnValue = [NSString stringWithFormat:@"%.0f", stepTarget];
    
    if (height==0 || weight==0){
        return @"0";
    }else{
        return rtnValue;
    }
}

#pragma mark - 혈압 메세지
- (NSString *)getPresuMsg:(NSString *)systolic diastolic:(NSString *)diastolic regdate:(NSString *)regdate {
    float float_sys = [systolic floatValue];
    float float_dia = [diastolic floatValue];
    NSString *group_msg = [self getpresuGroup:float_sys diastolic:float_dia];
    NSString *msg = [NSString stringWithFormat:@"측정된 혈압 %1.f / %1.f mmHg은 %@에 해당합니다.", float_sys, float_dia, group_msg];
    
//    NSString *time = [Utils getTimeFormat:regdate beTime:@"yyyyMMddHHmm" afTime:@"yyyyMMddHHmmss"];
    NSString *time = [TimeUtils getNowDateFormat:@"yyyyMMddHHmmss"];
    
    // 고혈압 전단계
    NSString *afterTime     = [Utils getUserDefault:PRESU_AFTER_TIME];
    int afterCount    = [[Utils getUserDefault:PRESU_AFTER_COUNT] intValue];
    // 고혈압 1기
    NSString *oneStepTime   = [Utils getUserDefault:PRESU_ONESTEP_TIME];
    int oneStepCount  = [[Utils getUserDefault:PRESU_ONESTEP_COUNT] intValue];
    // 고혈압 2기
    NSString *twoStepTime   = [Utils getUserDefault:PRESU_TWOSTEP_TIME];
    int twoStepCount  = [[Utils getUserDefault:PRESU_TWOSTEP_COUNT] intValue];
    // 고혈압 마지막 저장 시간저장
    NSString *lastTimeSave  = [Utils getUserDefault:PRESU_LAST_SAVE_TIME];
    
    //전단계 시간 초기화
    if([afterTime isEqualToString:@""]){
        afterTime = time;
        [Utils setUserDefault:PRESU_AFTER_TIME value:afterTime];
    }
    //1기 시간 초기화
    if([oneStepTime isEqualToString:@""]) {
        oneStepTime = time;
        [Utils setUserDefault:PRESU_AFTER_TIME value:oneStepTime];
    }
    //2기 시간 초기화
    if([twoStepTime isEqualToString:@""]) {
        twoStepTime = time;
        [Utils setUserDefault:PRESU_AFTER_TIME value:twoStepTime];
    }
    //2기 시간 초기화
    if([lastTimeSave isEqualToString:@""]) {
        lastTimeSave = time;
        [Utils setUserDefault:PRESU_AFTER_TIME value:lastTimeSave];
    }
    
    long currtime = [TimeUtils getTimeMilsFormat:@"yyyyMMddHHmmss" date:time];
    long after10time = [TimeUtils getTimeMilsFormat:@"yyyyMMddHHmmss" date:afterTime];
    /*int one10time = [oneStepTime intValue];
     int two10time = [twoStepTime intValue];
     int lasttime = [lastTimeSave intValue];*/
    
    long tenTime = 1000 * 60 * 10;
    //long dayOnehourTime = 1000 * 60 * 60 * 25;
    
//    NSString *curr10minadd = [Utils getCurrDateAdd:afterTime];
    
    BOOL isAfter10Minit = NO;
    if(currtime - after10time <= tenTime && currtime - after10time !=0 ) {
        isAfter10Minit = YES;
    }else {
        isAfter10Minit = NO;
    }
    
    //    if((currtime - after10time) <= tenTime && currtime - after10time !=0)
    //        isAfter10Minit = YES;
    //    else
    //        isAfter10Minit = NO;
    
    /*BOOL isOneStep10Minit = NO;
     if((currtime - one10time) <= tenTime)
     isOneStep10Minit = YES;
     else
     isOneStep10Minit = NO;
     
     BOOL isTwoStep10Minit = NO;
     if((currtime - two10time) <= tenTime)
     isTwoStep10Minit = YES;
     else
     isTwoStep10Minit = NO;*/
    
    BOOL isAfterLastOneDayOneHourTime = [[Utils getUserDefault:PRESU_DAY_FIRST] boolValue];
    /*if((currtime - lasttime) <= dayOnehourTime)
     isAfterLastOneDayOneHourTime = YES;
     else
     isAfterLastOneDayOneHourTime = NO;*/
    
    if(isAfterLastOneDayOneHourTime) {
        if(![msg isEqualToString:@""])
            msg = [NSString stringWithFormat:@"%@\n\n", msg];
        
        msg = [NSString stringWithFormat:@"%@혈압은 시간마다, 활동 상태에 따라 다르므로 아침 9~10시 사이와 저녁 9~10시 사이에 각각 2~3회 측정한 혈압의 평균이 비교적 정확한 자기 혈압입니다.", msg];
    }
    
    Tr_login *login = [self getLoginData];
    //float bmi = [login.mber_bmi floatValue];
    float rHeight = [login.mber_height floatValue] * 0.01f;
    float mber_beWeight;
    
    WeightDBWraper *db = [[WeightDBWraper alloc] init];
    NSArray *tempArr = [db getBottomResult];
    if(tempArr.count > 0) {
        mber_beWeight = [[[tempArr objectAtIndex:0] objectForKey:@"weight"] floatValue];
    }else{
        mber_beWeight = [login.mber_bdwgh floatValue];
    }
    
    float bmi = [[NSString stringWithFormat:@"%.1f", (mber_beWeight / (rHeight * rHeight))] floatValue];
    NSString *nowYear = [Utils getNowDate:@"yyyy"];
    int rAge = [[login.mber_lifyea substringToIndex:4] intValue];
    
    // 저혈압
    if(float_sys < 90 && float_dia < 60) {
        if(![msg isEqualToString:@""])
            msg = [NSString stringWithFormat:@"%@\n\n", msg];
        
        msg = [NSString stringWithFormat:@"%@낮은 혈압에 속하더라도 별다른 증상을 보이지 않을 수 있습니다. 그러나 정도가 심한 경우에는 실신을 일으킬 수 있으므로 규칙적인 생활과 충분한 휴식을 실천하는 것이 좋습니다.", msg];
        return msg;
    }
    // 정상
    if((float_sys >= 90 && float_sys < 120) && (float_dia >= 60 && float_dia < 80)) {
        if(![msg isEqualToString:@""])
            msg = [NSString stringWithFormat:@"%@\n\n", msg];
        
        msg = [NSString stringWithFormat:@"%@심혈관질환의 발병위험이 가장 낮은 최적의 혈압입니다.", msg];
        return msg;
    }
    if(float_sys >= 160.f || float_dia >= 100.f) {
        // 고혈압 2기 첫번째
        if(afterCount == 0 && !isAfter10Minit) {
            if(![msg isEqualToString:@""])
                msg = [NSString stringWithFormat:@"%@\n\n", msg];
            msg = [NSString stringWithFormat:@"%@혈압은 변동이 많고 여러 원인에 의해 상승할 수 있으므로 3~5분 정도 안정을 취하신 후 다시 한번 측정해 보세요.", msg];
            
            if (((float_sys >= 160) || (float_dia >= 100))) {
                NSString *systolicStr = @"140";
                NSString *diastolicStr = @"90";
                //80세 이상일 경우
                if([login.age intValue] >= 80) {
                    systolicStr = @"150";
                    diastolicStr = @"90";
                }
                //당뇨일 경우
                if([login.disease_nm isEqualToString:@"2"]) {
                    systolicStr = @"130";
                    diastolicStr = @"80";
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@고혈압 치료의 목표 혈압은 수축기혈압 %@mmHg 미만, 이완기혈압 %@mmHg 미만입니다. 심혈관질환 예방을 위해 적극적인 생활습관 개선이 필요합니다.", msg, systolicStr, diastolicStr];
            }
            
            isAfter10Minit = YES;
            afterCount = 1;
        }
        // 고혈압 2기 두번째
        else if(afterCount > 0 && isAfter10Minit) {
            if (((float_sys >= 160) || (float_dia >= 100)) && isAfter10Minit) {
                NSString *systolicStr = @"140";
                NSString *diastolicStr = @"90";
                //80세 이상일 경우
                if([login.age intValue] >= 80) {
                    systolicStr = @"150";
                    diastolicStr = @"90";
                }
                //당뇨일 경우
                if([login.disease_nm isEqualToString:@"2"]) {
                    systolicStr = @"130";
                    diastolicStr = @"80";
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@고혈압 치료의 목표 혈압은 수축기혈압 %@mmHg 미만, 이완기혈압 %@mmHg 미만입니다. 심혈관질환 예방을 위해 적극적인 생활습관 개선이 필요합니다.", msg, systolicStr, diastolicStr];
            }
            // 고혈압 2기 두번째 (bmi가 25 이상일 경우 (비만일 경우) 추가 메시지)
            if(bmi >= 25) {
                NSString *sex = @"90"; // 남성
                if([login.mber_sex isEqualToString:@"2"]) {
                    sex = @"85"; // 여성
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[체중감량]\n", msg];
                msg = [NSString stringWithFormat:@"%@① 효과 : 체중 1kg 감량 시 수축기혈압은 -1.1mmHg, 이완기혈압은 -0.9mmHg 감소효과가 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 목표 : 체질량지수(BMI) 25kg/㎡ 미만 및 허리둘레 %@cm 미만 유지를 목표로 합니다.\n", msg, sex];
                msg = [NSString stringWithFormat:@"%@③ 식사조절과 함께 신체활동량을 늘려주세요.", msg];
            }
            
            // 고혈압 2기 두번째 (흡연중일 경우 추가 메시지)
            if ([login.smkng_yn isEqualToString:@"Y"]) {
                
                if (![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[금연]\n", msg];
                msg = [NSString stringWithFormat:@"%@담배에 함유된 니코틴에 의해 일시적으로 혈압과 맥박이 상승됩니다. 흡연은 고혈압과 마찬가지로 심혈관질환의 강력한 위험인자이므로 완전한 금연이 필요합니다.", msg];
            }
            
            // 고혈압 2기 두번째 (활동량 1,2 번 선택인 경우 추가메시지)
            if([login.mber_actqy isEqualToString:@"1"] || [login.mber_actqy isEqualToString:@"2"] ){
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[운동]\n", msg];
                msg = [NSString stringWithFormat:@"%@① 효과 : 하루 30~50분, 일주일에 5일 이상 운동 시 수축기혈압은 -4.9mmHg, 이완기혈압은 -3.7mmHg 감소효과가 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 방법 : 유산소 운동을 기본으로 하되 무산소 운동을 일주일에 2~3회 정도 병행하면 좋습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@③ 주의 : 혈압이 조절되지 않는 경우 무거운 것을 들어올리는 것과 같은 무산소 운동은 피하세요.", msg];
            }
            
            // 고혈압 2기 두번째 (180/120 mmHg 이상인 경우 추가 메시지)
            if((float_sys >= 180) || (float_dia >= 120)) {
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[고혈압성 응급 주의]\n", msg];
                msg = [NSString stringWithFormat:@"%@고혈 압성 응급이란 혈압이 급격하게 상승하면서 장기에 손상이 동반되는 응급상황입니다. 하지만 장기의 손상이 없을 경우 고혈압성 응급에 해당하지 않을 수도 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@① 증상 : 망막 출혈 및 부종, 심한 두통, 호흡곤란, 코피, 불안증 등의 증상을 보일 수 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 대처 : 고혈압성 응급의 경우 해당 전문의의 치료가 필요합니다. 반드시 병원에 방문하세요.", msg];
            }
            // 고혈압 2기 두번째 (200/110 mmHg 이상인 경우 추가 메시지)
            if ((float_sys >= 200) || (float_dia >= 110)) {
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[운동 시 주의사항]\n", msg];
                msg = [NSString stringWithFormat:@"%@① 운동 전 : 수축기혈압 200mmHg 이상, 이완기혈압 110mmHg 이상이면 운동을 하지 않는 것이 좋습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 운동 중 : 수축기혈압 250mmHg 이상, 이완기혈압 115mmHg 이상일 경우 운동을 중지해야 합니다.\n", msg];
            }
        }else if(!isAfter10Minit) {
            [Utils setUserDefault:PRESU_AFTER_COUNT value:@"0"];
            msg = [self getPresuMsg:systolic diastolic:diastolic regdate:regdate];
        }
        [Utils setUserDefault:PRESU_AFTER_TIME value:time];
        [Utils setUserDefault:PRESU_AFTER_COUNT value:[NSString stringWithFormat:@"%d", (afterCount + 1)]];
        return msg;
    }
    
    if(((float_sys >= 140 && float_sys <= 159) || (float_dia >= 90 && float_dia <= 99)) && float_sys < 160 || (float_sys >= 140 && float_dia < 90)) {
        //고혈압 1기 첫번째
        if(afterCount == 0 && !isAfter10Minit) {
            if(![msg isEqualToString:@""])
                msg = [NSString stringWithFormat:@"%@\n\n", msg];
            msg = [NSString stringWithFormat:@"%@혈압은 변동이 많고 여러 원인에 의해 상승할 수 있으므로 3~5분 정도 안정을 취하신 후 다시 한번 측정해 보세요.", msg];
            
            // 고혈압 1기 첫번째 (80세이상, 당뇨일 경우 추가 메시지)
            if(((float_sys >= 140 && float_sys <= 159) || (float_dia >= 90 && float_dia <= 90))) {
                NSString *systolicStr = @"140";
                NSString *diastolicStr = @"90";
                //80세 이상일 경우
                if([login.age intValue] >= 80) {
                    systolicStr = @"150";
                    diastolicStr = @"90";
                }
                //당뇨일 경우
                if([login.disease_nm isEqualToString:@"2"]) {
                    systolicStr = @"130";
                    diastolicStr = @"80";
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@고혈압 치료의 목표 혈압은 수축기혈압 %@mmHg 미만, 이완기혈압 %@mmHg 미만입니다. 심혈관질환 예방을 위해 적극적인 생활습관 개선이 필요합니다.", msg, systolicStr, diastolicStr];
            }
            
            isAfter10Minit = YES;
            afterCount = 1;
        }
        //고혈압 1기 두번째
        else if(afterCount > 0 && isAfter10Minit) {
            // 고혈압 1기 두번째 (80세이상, 당뇨일 경우 추가 메시지)
            if(((float_sys >= 140 && float_sys <= 159) || (float_dia >= 90 && float_dia <= 90)) && isAfter10Minit) {
                NSString *systolicStr = @"140";
                NSString *diastolicStr = @"90";
                //80세 이상일 경우
                if([login.age intValue] >= 80) {
                    systolicStr = @"150";
                    diastolicStr = @"90";
                }
                //당뇨일 경우
                if([login.disease_nm isEqualToString:@"2"]) {
                    systolicStr = @"130";
                    diastolicStr = @"80";
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@고혈압 치료의 목표 혈압은 수축기혈압 %@mmHg 미만, 이완기혈압 %@mmHg 미만입니다. 심혈관질환 예방을 위해 적극적인 생활습관 개선이 필요합니다.", msg, systolicStr, diastolicStr];
            }
            
            // 고혈압 1기 두번째 (bmi가 25 이상일 경우 (비만일 경우) 추가 메시지)
            if(bmi >= 25) {
                NSString *sex = @"90"; // 남성
                if([login.mber_sex isEqualToString:@"2"]) {
                    sex = @"85"; // 여성
                }
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[체중감량]\n", msg];
                msg = [NSString stringWithFormat:@"%@① 효과 : 체중 1kg 감량 시 수축기혈압은 -1.1mmHg, 이완기혈압은 -0.9mmHg 감소효과가 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 목표 : 체질량지수(BMI) 25kg/㎡ 미만 및 허리둘레 %@cm 미만 유지를 목표로 합니다.\n", msg, sex];
                msg = [NSString stringWithFormat:@"%@③ 식사조절과 함께 신체활동량을 늘려주세요.", msg];
            }
            
            // 고혈압 1기 두번째 (흡연중일 경우 추가 메시지)
            if ([login.smkng_yn isEqualToString:@"Y"]) {
                
                if (![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[금연]\n", msg];
                msg = [NSString stringWithFormat:@"%@담배에 함유된 니코틴에 의해 일시적으로 혈압과 맥박이 상승됩니다. 흡연은 고혈압과 마찬가지로 심혈관질환의 강력한 위험인자이므로 완전한 금연이 필요합니다.", msg];
            }
            
            // 고혈압 1기 두번째 (활동량 1,2 번 선택인 경우 추가메시지)
            if([login.mber_actqy isEqualToString:@"1"] || [login.mber_actqy isEqualToString:@"2"] ){
                if(![msg isEqualToString:@""])
                    msg = [NSString stringWithFormat:@"%@\n\n", msg];
                msg = [NSString stringWithFormat:@"%@[운동]\n", msg];
                msg = [NSString stringWithFormat:@"%@① 효과 : 하루 30~50분, 일주일에 5일 이상 운동 시 수축기혈압은 -4.9mmHg, 이완기혈압은 -3.7mmHg 감소효과가 있습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@② 방법 : 유산소 운동을 기본으로 하되 무산소 운동을 일주일에 2~3회 정도 병행하면 좋습니다.\n", msg];
                msg = [NSString stringWithFormat:@"%@③ 주의 : 혈압이 조절되지 않는 경우 무거운 것을 들어올리는 것과 같은 무산소 운동은 피하세요.", msg];
            }
            
            
        }else if(!isAfter10Minit) {
            [Utils setUserDefault:PRESU_AFTER_COUNT value:@"0"];
            msg = [self getPresuMsg:systolic diastolic:diastolic regdate:regdate];
        }
        [Utils setUserDefault:PRESU_AFTER_TIME value:time];
        [Utils setUserDefault:PRESU_AFTER_COUNT value:[NSString stringWithFormat:@"%d", (afterCount + 1)]];
        return msg;
    }
    //고혈압 전단계
    if(((float_sys >= 120 && float_sys <= 139) || (float_dia >= 80 && float_dia <= 89)) && float_sys < 140) {
        if (afterCount == 0) {
            if(![msg isEqualToString:@""])
                msg = [NSString stringWithFormat:@"%@\n\n", msg];
            
            msg = [NSString stringWithFormat:@"%@혈압은 변동이 많고 여러 원인에 의해 상승할 수 있으므로 1~2분 정도 안정을 취하신 후 다시 한번 측정해 보세요.", msg];
            
            if(![msg isEqualToString:@""])
                msg = [NSString stringWithFormat:@"%@\n\n", msg];
            
            msg = [NSString stringWithFormat:@"%@일반적으로 가정혈압의 고혈압 기준은 135/85 mmHg로서 진료실혈압보다 약 5 mmHg 정도 낮습니다.\n", msg];
            msg = [NSString stringWithFormat:@"%@고혈압 예방을 위한 적극적인 관리가 필요합니다.\n", msg];
            msg = [NSString stringWithFormat:@"%@비만(특히 복부 비만), 고염분 섭취, 운동부족, 흡연, 과음을 조절하는 생활습관을 갖도록 노력하세요.", msg];
            
            isAfter10Minit = YES;
            afterCount = 1;
        }
        if(afterCount > 1 && isAfter10Minit) {
            if(![msg isEqualToString:@""])
                msg = [NSString stringWithFormat:@"%@\n\n", msg];
            
            msg = [NSString stringWithFormat:@"%@일반적으로 가정혈압의 고혈압 기준은 135/85 mmHg로서 진료실혈압보다 약 5 mmHg 정도 낮습니다.\n", msg];
            msg = [NSString stringWithFormat:@"%@고혈압 예방을 위한 적극적인 관리가 필요합니다.\n", msg];
            msg = [NSString stringWithFormat:@"%@비만(특히 복부 비만), 고염분 섭취, 운동부족, 흡연, 과음을 조절하는 생활습관을 갖도록 노력하세요.", msg];
            
        }else if(!isAfter10Minit) {
            [Utils setUserDefault:PRESU_AFTER_COUNT value:@"0"];
            msg = [self getPresuMsg:systolic diastolic:diastolic regdate:regdate];
        }
        [Utils setUserDefault:PRESU_AFTER_TIME value:time];
        [Utils setUserDefault:PRESU_AFTER_COUNT value:[NSString stringWithFormat:@"%d", (afterCount + 1)]];
        return msg;
    }
    [Utils setUserDefault:PRESU_LAST_SAVE_TIME value:time];
    
    
    return msg;
}

- (NSString *)getpresuGroup:(float)systolic diastolic:(float)diastolic {
    NSString *msg = @"";
    if(systolic < 90.f && diastolic < 60.f) {
        msg = @"저혈압";
    } if((systolic >= 90.f && systolic < 120.f) || (diastolic >= 60.f && diastolic < 80.f)) {
        msg = @"정상혈압";
    } if(((systolic >= 120.f && systolic <= 139.f) || (diastolic >= 80.f && diastolic <= 89.f)) && systolic < 140.f) {
        msg = @"고혈압 전단계";
    } if(((systolic >= 140 && systolic <= 159) || (diastolic >= 90 && diastolic <= 99)) && systolic < 160) {
        msg = @"고혈압 1기";
    } if(systolic >= 160.f || diastolic >= 100.f) {
        msg = @"고혈압 2기";
    } if(systolic >= 140 && diastolic < 90){
        msg = @"수축기 단독 고혈압";
    }
    
    return msg;
}

#pragma mark - 혈당 메세지
-(NSString *)getSugarMessage:(NSString *)sugar eatType:(NSString *)eatType regdate:(NSString *)regdate {
    NSString *msg = @"";
    NSString *time = [Utils getTimeFormat:regdate beTime:@"yyyyMMddHHmm" afTime:@"HH시 mm분"];
    
    float rSugar = [sugar floatValue];
    
    if(![eatType isEqualToString:@"2"]){
        if(rSugar <= 70){
            msg = [NSString stringWithFormat:@"%@ 식전 혈당은 %.0fmg/dL로 낮은 편입니다. 저혈당 증상이 동반될 경우 주스, 사탕, 설탕물 등 당분이 포함된 음료나 음식을 섭취하세요. *저혈당 증상: 식은땀, 불안감, 손떨림, 창백한 얼굴, 의식혼미, 어지럼증, 시력변화, 말하기 힘듦, 두통 등", time, rSugar];
        }else if(rSugar >= 71 && rSugar <= 99){
            msg = [NSString stringWithFormat:@"%@ 식전 혈당은 %.0fmg/dL로 관리가 잘 되고 있습니다. 규칙적인 운동과 식이 조절을 통해 꾸준히 관리하세요.", time, rSugar];
        }else if(rSugar >= 100 && rSugar <= 299){
            msg = [NSString stringWithFormat:@"%@ 식전 혈당은 %.0fmg/dL로 관리가 필요합니다. 정기적으로 혈당을 체크하고, 높은 혈당 수치가 계속 나타날 경우 전문의료진과 상담하세요.", time, rSugar];
        }else if(rSugar >= 300){
            msg = [NSString stringWithFormat:@"%@ 식전 혈당은 %.0fmg/dL로 높습니다. 높은 혈당 수치가 지속되면서 고혈당 증상이 동반될 경우 전문의료진과 상의하거나 병원에 내원하여 진료를 받아보실 것을 권유 드립니다.", time, rSugar];
        }
    }else{
        if(rSugar <= 70){
            msg = [NSString stringWithFormat:@"%@ 식후 혈당은 %.0fmg/dL로 낮은 편입니다. 저혈당 증상이 동반될 경우 주스, 사탕, 설탕물 등 당분이 포함된 음료나 음식을 섭취하세요. *저혈당 증상: 식은땀, 불안감, 손떨림, 창백한 얼굴, 의식혼미, 어지럼증, 시력변화, 말하기 힘듦, 두통 등", time, rSugar];
        }else if(rSugar >= 71 && rSugar <= 139){
            msg = [NSString stringWithFormat:@"%@ 식후 혈당은 %.0fmg/dL로 관리가 잘 되고 있습니다. 규칙적인 운동과 식이 조절을 통해 꾸준히 관리하세요.", time, rSugar];
        }else if(rSugar >= 140 && rSugar <= 299){
            msg = [NSString stringWithFormat:@"%@ 식후 혈당은 %.0fmg/dL로 관리가 필요합니다. 정기적으로 혈당을 체크하고, 높은 혈당 수치가 계속 나타날 경우 전문의료진과 상담하세요.", time, rSugar];
        }else if(rSugar >= 300){
            msg = [NSString stringWithFormat:@"%@ 식후 혈당은 %.0fmg/dL로 높습니다. 높은 혈당 수치가 지속되면서 고혈당 증상이 동반될 경우 전문의료진과 상의하거나 병원에 내원하여 진료를 받아보실 것을 권유 드립니다.", time, rSugar];
        }
    }
    
    return msg;
}

#pragma mark - 체중 메세지
- (NSString *)getWeightMessage:(NSString *)weight fat:(NSString *)fat {
    NSString *msg = @"";
    if([weight isEqualToString:@""]) {
        weight = @"0.0";
    }
    
    if([fat isEqualToString:@""]) {
        fat = @"0.0";
    }
    
    login = [self getLoginData];
    
    NSString *rWeight = [NSString stringWithFormat:@"%.1f", [weight floatValue]];
    float rHeight = [login.mber_height floatValue] * 0.01f;
    float bmi = [[NSString stringWithFormat:@"%.1f", ([rWeight floatValue] / (rHeight * rHeight))] floatValue];
    
    NSString *lavelstr = @"";
    if(bmi < 18.5) {
        lavelstr = @"저체중";
    }else if(bmi >= 18.5 && bmi <= 22.9){
        lavelstr = @"정상체중";
    }else if(bmi > 22.9 && bmi < 25.0){
        lavelstr = @"과체중";
    }else if(bmi >= 25.0){
        lavelstr = @"비만";
    }
    
    msg = [NSString stringWithFormat:@"측정된 체중 %@kg으로 계산된 BMI(체질량지수)는 %.1f으로 %@군에 해당합니다.\n\n", rWeight, bmi, lavelstr];
    
    //    if([fat floatValue] > 0){
    //        msg = [NSString stringWithFormat:@"%@%@", msg, [self getRatingMSg:weight fat:fat]];
    //    }
    
    if(bmi < 18.5){
        //저체중군
        NSString *tempmsg = @"적절한 운동과 균형 잡힌 음식섭취를 통해 정상체중을 회복 할 수 있도록 노력이 필요합니다.\n체중증가가 지방만이 아닌 제지방의 증가까지 병행하여 목표 활동량 달성 노력과 함께 근력운동을 추가하여 근육의 양과 크기를 증가시키는 것이 중요합니다.\n점진적으로 목표를 수정하여 활동량과 식사량을 늘려주세요.";
        msg = [NSString stringWithFormat:@"%@%@", msg, tempmsg];
        
    }else if(bmi >= 18.5 && bmi <= 22.9){
        //정상체중군
        NSString *tempmsg = @"적절한 운동과 식사조절을 통해 건강한 체중을 유지하는 것이 중요합니다.\n점진적으로 목표를 수정하여 활동량을 늘려주세요.";
        msg = [NSString stringWithFormat:@"%@%@", msg, tempmsg];
        
    }else if(bmi > 22.9 && bmi < 25.0){
        
        //과체중군
        NSString *tempmsg = @"적절한 체중 감량에는 시간이 걸립니다. 가능한 매일 활동 목표 달성을 위해 노력해야 합니다.\n추천되는 목표활동량은 최소한입니다. 점진적으로 목표를 수정하여 활동량을 늘려가야 합니다.\n체중 감량을 위해 평소보다 하루 500~1,000kcal정도의 에너지 섭취량을 줄이세요.";
        msg = [NSString stringWithFormat:@"%@%@", msg, tempmsg];
        
    }else if(bmi >= 25.0){
        //비만군
        NSString *tempmsg = @"적절한 체중 감량에는 시간이 걸립니다. 가능한 매일 활동 목표 달성을 위해 노력해야 합니다.\n추천되는 목표활동량은 최소한입니다. 점진적으로 목표를 수정하여 활동량을 늘려가야 합니다.\n체중 감량을 위해 평소보다 하루 500~1,000kcal정도의 에너지 섭취량을 줄이세요.";
        msg = [NSString stringWithFormat:@"%@%@", msg, tempmsg];
    }
    
    return msg;
}

//체지방 메세지 만들기
- (NSString *)getRatingMSg:(NSString *)weight fat:(NSString *)fat {
    NSString *msg = @"";
    
    login = [self getLoginData];
    int rating = [self getRating:fat];
    NSString *fencent = @"%";
    
    if(rating == 1){
        msg = [NSString stringWithFormat:@"체지방률 %@%@로서 평균보다 상당히 적은 상태입니다.\n\n", fat, fencent];
    }else if(rating == 2){
        msg = [NSString stringWithFormat:@"체지방률 %@%@로서 평균보다 적은 상태입니다.\n\n", fat, fencent];
    }else if(rating == 3){
        msg = [NSString stringWithFormat:@"체지방률 %@%@로서 평균적인 상태입니다.\n\n", fat, fencent];
    }else if(rating == 4){
        msg = [NSString stringWithFormat:@"체지방률 %@%@로서 평균보다 많은 상태입니다.\n\n", fat, fencent];
    }else if(rating == 5){
        msg = [NSString stringWithFormat:@"체지방률 %@%@로서 평균보다 상당히 많은 상태입니다.\n\n", fat, fencent];
    }
    
    return msg;
}

//체지방 등급 만들기
- (int)getRating:(NSString *)fat {
    int rating = 0;
    
    login = [self getLoginData];
    
    int sex = [login.mber_sex intValue];
    int nowYaer = [[Utils getNowDate:@"yyyy"] intValue];
    int rBirth = [[login.mber_lifyea substringToIndex:4] intValue];
    int rAge = nowYaer - rBirth;
    float bdfat = [fat floatValue];
    
    if(sex == 1) {
        if( ((rAge >= 19 && rAge <= 24) && (bdfat <= 8.0))                            // 1등급군에 해당
           || ((rAge >= 25 && rAge <= 29) && (bdfat <= 9.4))
           || ((rAge >= 30 && rAge <= 34) && (bdfat <= 10.6))
           || ((rAge >= 35 && rAge <= 39) && (bdfat <= 12.9))
           || ((rAge >= 40 && rAge <= 44) && (bdfat <= 12.8))
           || ((rAge >= 45 && rAge <= 49) && (bdfat <= 13.2))
           || ((rAge >= 50 && rAge <= 54) && (bdfat <= 14.3))
           || ((rAge >= 55 && rAge <= 59) && (bdfat <= 14.4))
           || ((rAge >= 60 && rAge <= 64) && (bdfat <= 16.1)) ) {
            rating = 1;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 8.1 && bdfat <= 11.7))      // 2등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 9.5 && bdfat <= 13.7))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 10.7 && bdfat <= 14.5))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 13.0 && bdfat <= 16.7))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 12.9 && bdfat <= 15.6))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 13.3 && bdfat <= 16.5))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 14.4 && bdfat <= 17.7))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 14.5 && bdfat <= 18.0))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 16.2 && bdfat <= 17.8)) ) {
            rating = 2;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 11.8 && bdfat <= 16.6))     // 3등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 13.8 && bdfat <= 18.3))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 14.6 && bdfat <= 18.8))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 16.8 && bdfat <= 21.1))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 15.7 && bdfat <= 20.0))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 16.6 && bdfat <= 20.3))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 17.8 && bdfat <= 21.8))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 18.1 && bdfat <= 21.5))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 17.9 && bdfat <= 22.5 )) ) {
            rating = 3;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 16.7 && bdfat <= 22.8))     // 4등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 18.4 && bdfat <= 24.4))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 18.9 && bdfat <= 23.0))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 21.2 && bdfat <= 25.1))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 20.1 && bdfat <= 24.0))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 20.4 && bdfat <= 24.8))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 21.9 && bdfat <= 25.9))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 21.6 && bdfat <= 25.1))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 22.6 && bdfat <= 27.5)) ) {
            rating = 4;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 22.9))                    // 5등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 24.5))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 23.1))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 25.2))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 24.1))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 24.9))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 26.0))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 25.2))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 27.6)) ) {
            rating =5;
        }
        return rating;
    }
    // 여자
    if(sex == 2) {
        if( ((rAge >= 19 && rAge <= 24) && (bdfat <= 19.0))                           // 1등급군에 해당
           || ((rAge >= 25 && rAge <= 29) && (bdfat <= 18.6))
           || ((rAge >= 30 && rAge <= 34) && (bdfat <= 18.9))
           || ((rAge >= 35 && rAge <= 39) && (bdfat <= 19.2))
           || ((rAge >= 40 && rAge <= 44) && (bdfat <= 19.8))
           || ((rAge >= 45 && rAge <= 49) && (bdfat <= 19.4))
           || ((rAge >= 50 && rAge <= 54) && (bdfat <= 19.7))
           || ((rAge >= 55 && rAge <= 59) && (bdfat <= 20.6))
           || ((rAge >= 60 && rAge <= 64) && (bdfat <= 21.2)) ) {
            rating = 1;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 19.1 && bdfat <= 22.3))     // 2등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 18.7 && bdfat <= 21.3))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 19.0 && bdfat <= 22.1))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 19.3 && bdfat <= 23.0))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 19.9 && bdfat <= 23.1))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 19.5 && bdfat <= 22.9))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 19.8 && bdfat <= 23.9))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 20.7 && bdfat <= 24.3))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 21.3 && bdfat <= 24.8)) ) {
            rating = 2;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 22.4 && bdfat <= 25.3))     // 3등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 21.4 && bdfat <= 24.9))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 22.2 && bdfat <= 24.8))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 23.1 && bdfat <= 27.0))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 23.2 && bdfat <= 28.0))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 23.0 && bdfat <= 27.7))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 24.4 && bdfat <= 27.8))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 24.4 && bdfat <= 28.9))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 24.9 && bdfat <= 29.2 )) ) {
            rating = 3;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 25.4 && bdfat <= 29.6))     // 4등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 25.0 && bdfat <= 29.6))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 24.9 && bdfat <= 28.6))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 27.1 && bdfat <= 32.8))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 28.1 && bdfat <= 33.1))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 27.8 && bdfat <= 31.4))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 27.9 && bdfat <= 34.6 ))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 29.0 && bdfat <= 36.0))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 29.3 && bdfat <= 34.7)) ) {
            rating = 4;
        } else if( ((rAge >= 19 && rAge <= 24) && (bdfat >= 29.7))                    // 5등급군에 해당
                  || ((rAge >= 25 && rAge <= 29) && (bdfat >= 29.7))
                  || ((rAge >= 30 && rAge <= 34) && (bdfat >= 28.7))
                  || ((rAge >= 35 && rAge <= 39) && (bdfat >= 32.9))
                  || ((rAge >= 40 && rAge <= 44) && (bdfat >= 33.2))
                  || ((rAge >= 45 && rAge <= 49) && (bdfat >= 31.5))
                  || ((rAge >= 50 && rAge <= 54) && (bdfat >= 34.7))
                  || ((rAge >= 55 && rAge <= 59) && (bdfat >= 36.1))
                  || ((rAge >= 60 && rAge <= 64) && (bdfat >= 34.8)) ) {
            rating = 5;
        }
        return rating;
    }
    
    return rating;
}

//표준 칼로리 구하기 (식사)
- (NSString *)getDefaultCalori {
    NSString *result = @"";
    
    login = [self getLoginData];
    
    //    int rActqy = [login.mber_actqy intValue];
    int rSex = [login.mber_sex intValue];
    float rHeight = [login.mber_height floatValue];
    float rWeight = [login.mber_bdwgh floatValue];
    float mWeight = 0.0f;
    rHeight = rHeight * 0.01f;
    
    WeightDBWraper *db = [[WeightDBWraper alloc] init];
    NSArray *tempArr = [db getBottomResult];
    if(tempArr.count > 0){
        rWeight = [[[tempArr objectAtIndex:0] objectForKey:@"weight"] floatValue];
    }else{
        rWeight = [login.mber_bdwgh floatValue];
    }
    
    float mHeight = [[NSString stringWithFormat:@"%.2f", rHeight] floatValue];
    
    if(rSex == 1){
        mWeight = [[NSString stringWithFormat:@"%.1f", (mHeight*mHeight)*22] floatValue];
    }else{
        mWeight = [[NSString stringWithFormat:@"%.1f", (mHeight*mHeight)*21] floatValue];
    }
    
    float bmi = rWeight/(rHeight * rHeight);
    float fat = 0.f;
    
    if(bmi < 18.5){
        //"저체중"
        /*
         if(rActqy == 1){
         fat = 35.f;
         }else if(rActqy == 2){
         fat = 40.f;
         }else if(rActqy == 3){
         fat = 45.f;
         }*/
        // 이번 프로잭트에서는 가벼운운동, 보통활동, 힘든운동을 받지 않기때문에 보통으로 고정(박서언대리 2018.4.20)
        fat = 40.f;
    }else if(bmi >= 18.5 && bmi <=22.9){
        //"정상";
        /*
         if(rActqy == 1){
         fat = 30.f;
         }else if(rActqy == 2){
         fat = 35.f;
         }else if(rActqy == 3){
         fat = 40.f;
         }*/
        // 이번 프로잭트에서는 가벼운운동, 보통활동, 힘든운동을 받지 않기때문에 보통으로 고정(박서언대리 2018.4.20)
        fat = 35.f;
    }else if(bmi >= 23.0) {
        //"과체중";
        /*
         if (rActqy == 1) {
         fat = 25.f;
         } else if (rActqy == 2) {
         fat = 30.f;
         } else if (rActqy == 3) {
         fat = 35.f;
         }*/
        // 이번 프로잭트에서는 가벼운운동, 보통활동, 힘든운동을 받지 않기때문에 보통으로 고정(박서언대리 2018.4.20)
        fat = 30.f;
    }
    
    int recomCal = (int)(mWeight * fat);  // 권장섭취량
    
    result = [NSString stringWithFormat:@"%d", recomCal];
    
    return result;
}

// 식사 권장칼로리
- (int)getPregnancyRecommendCal {
//    성인 여자 : 354 - 6.91 × 연령(세) + PA×[9.36 × 체중(kg) + 726 × 신장(m)]
//    성인 남자 : 662 -9.53 × 연령(세) + PA [15.91 × 체중(kg) + 539.6 × 신장(m)]
    
//    <계산식에 쓰이는 데이터>
//    성별 : 회원가입 시 정보
//    연령 : 회원가입 시 생년월일 정보
//    PA(신체활동계수) : 기본 정보 입력 시 활동량 정보
//    - 주로 앉아 있음 선택 시  1.0
//    - 약간활동적 선택 시  1.12
//    - 활동적 선택 시  1.25
//    체중 : 1순위- 건강관리 > 체중 기록 최신 데이터
//    2순위- 기본정보 입력 체중 값 (건강관리 > 체중 기록 값이 없을 경우)
//    신장 : 기본 정보 키 ※cm를 m로 바꿔서 계산되어야함
//
    int recommandCal = 0;
    
   login = [self getLoginData];
    
    NSString *mber_brthdy   =  login.mber_lifyea;                       // 생일
    NSString *mber_active   =  login.mber_actqy;                        // 가벼운운동,보통활동,힘들활동
    float  mber_height      =  [login.mber_height floatValue];          // 신장(키)
    float  mber_beWeight    =  [login.mber_bdwgh floatValue];           // 출산정체중
    NSString *nowDate       =  [TimeUtils getNowDateFormat:@"yyyy"];// now
    
    WeightDBWraper *db = [[WeightDBWraper alloc] init];
    NSArray *tempArr = [db getBottomResult];
    if(tempArr.count > 0) {
        mber_beWeight = [[[tempArr objectAtIndex:0] objectForKey:@"weight"] floatValue];
    }else{
        mber_beWeight = [login.mber_bdwgh floatValue];
    }
    
//    NSString *birthYear = [NSString stringWithFormat:@"%@", [mber_brthdy substringToIndex:4]];
//    int fAge = ([nowDate intValue] - [birthYear intValue]);  //만나이
    int fAge = [login.age intValue];
    
    // + ---------------------------------
    // 활동정도
    // + ---------------------------------
    float fActivie  = 0.0f;
    if ([mber_active isEqualToString:@"1"]) {
        fActivie    = 1.0;    //가벼운운동
    }else if ([mber_active isEqualToString:@"2"]) {
        fActivie    = 1.12;    //보통활동
    }else if ([mber_active isEqualToString:@"3"]) {
        fActivie    = 1.25;    //힘들활동
    }
    
    if ([login.mber_sex isEqualToString:@"1"]) { // 남자
        recommandCal = (int)(662-(9.53 * fAge) + (fActivie * ((15.91 * mber_beWeight) + (539.6 * (mber_height/100)))));
    }else { // 여자
        recommandCal = (int)(354-(6.91 * fAge) + (fActivie * ((9.36 * mber_beWeight) + (726 * (mber_height/100)))));
    }
    return recommandCal;
}

@end
