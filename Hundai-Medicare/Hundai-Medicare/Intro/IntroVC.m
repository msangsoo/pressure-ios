//
//  IntroVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "IntroVC.h"

@interface IntroVC() {
    NSArray *items;
    NSArray *testitems;
}

@end

@implementation IntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    items = [[NSArray alloc] initWithObjects:@"intro_1", @"intro_2", @"intro_3", nil];
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    
    CGSize frameSize = self.scrollView.frame.size;
    for (NSString *imageName in items) {
        CGSize contetnSize = self.scrollView.contentSize;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(contetnSize.width, 0, frameSize.width, frameSize.height)];
        imageView.layer.shouldRasterize = true;
        imageView.opaque = true;
        imageView.userInteractionEnabled = false;
        imageView.tag = 99;
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.image  = [UIImage imageNamed:imageName];
        [self.scrollView addSubview:imageView];
        [self.scrollView setContentSize:CGSizeMake(contetnSize.width + frameSize.width, frameSize.height)];
    }
    
    self.scrollView.delaysContentTouches = false;
    self.scrollView.delegate = self;
}

#pragma mark - Actions
- (IBAction)pressAgain:(id)sender {
    _againBtn.selected = !_againBtn.isSelected;
}

- (IBAction)nextBtn:(id)sender {
    if (_againBtn.isSelected) {
        [Utils setUserDefault:APP_INTRO value:@"Y"];
    }
    
    UIViewController *vc = LOGIN_STORYBOARD.instantiateInitialViewController;
    [self.navigationController pushViewController:vc animated:true];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _againBtn.hidden = true;
    _nextBtn.hidden = true;
    
    int index = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    _pageControl.currentPage = index;
    
    if (index == (items.count - 1)) {
        _againBtn.hidden = false;
        _nextBtn.hidden = false;
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _pageControl.numberOfPages = items.count;
    _pageControl.currentPage = 0;
    _pageControl.pageIndicatorTintColor = RGB(143, 143, 143);
    _pageControl.currentPageIndicatorTintColor = COLOR_NATIONS_BLUE;
    
    _againBtn.hidden = true;
    _nextBtn.hidden = true;
    
    _againBtn.titleLabel.font = FONT_NOTO_REGULAR(14);
    [_againBtn setImage:[UIImage imageNamed:@"btn_common_un_selected"] forState:UIControlStateNormal];
    [_againBtn setImage:[UIImage imageNamed:@"btn_common_on_selected"] forState:UIControlStateSelected];
    [_againBtn setTitle:@"  다음부터 안볼래요" forState:UIControlStateNormal];
    [_againBtn setTitle:@"  다음부터 안볼래요" forState:UIControlStateSelected];
    [_againBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_againBtn setBackgroundColor:[UIColor whiteColor]];

    _nextBtn.titleLabel.font = FONT_NOTO_BOLD(16);
    [_nextBtn setTitle:@"시작하기" forState:UIControlStateNormal];
    [_nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_nextBtn setBackgroundColor:COLOR_NATIONS_BLUE];
}

@end
