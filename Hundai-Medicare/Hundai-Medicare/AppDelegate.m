//
//  AppDelegate.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 31/01/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#define isOSVersionOver10  ([[[[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."] objectAtIndex:0] integerValue] >= 10)

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //Navigation
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor blackColor],
       NSFontAttributeName: [UIFont fontWithName:@"NotoSansKR-Bold" size:18]
       }];
    
    UIImage *backImg = [[UIImage imageNamed:@"btn_common_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UINavigationBar appearance] setBackIndicatorImage:backImg];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backImg];
    
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:false];
    
    //UITabBar Appearance
    [[UITabBarItem appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName : RGB(143, 144, 157),
       NSFontAttributeName: [UIFont fontWithName:@"NotoSansKR-Medium" size:10],
       NSKernAttributeName: [NSNumber numberWithFloat:-1.5f]
       } forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName : RGB(108, 131, 229),
       NSFontAttributeName: [UIFont fontWithName:@"NotoSansKR-Medium" size:10],
       NSKernAttributeName: [NSNumber numberWithFloat:-1.5f]
       } forState:UIControlStateSelected];
    
    
    // 원격 푸쉬 알림을 통한 진입인지 확인
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(remoteNotification){
        // 로그인 후 HomeViewController 나왔을 때 바로 열지도나 건강뉴스, 일반공지 화면으로 넘어갈 수 있도록 UserDefaults에 기록
//        [self notiSett:remoteNotification];
    }
    
    [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Initialize Remote Notification
- (void)initializeRemoteNotification {
    if(isOSVersionOver10) { // iOS10 이상
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ) {
                // 푸시 서비스 등록 성공
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
            else {
                // 푸시 서비스 등록 실패
            }}];
    }else {  // iOS10 하위버전
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }
}


#pragma mark - notication delegate
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"deviceToken = %@", deviceToken);
    NSLog(@"deviceToken = %@", deviceTokenString);
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:APP_TOKEN] == nil) {
        if(deviceTokenString.length > 1) {
            [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:APP_TOKEN];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"push fail Error : %@",error);
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    completionHandler(UNNotificationPresentationOptionAlert);
    //엑티브 상태일때 푸시 들어옴(임시막음)
//    NSDictionary *userInfo = notification.request.content.userInfo;
//    [self notiSett:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [self notiSett:userInfo];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSLog(@"터치하면 들어옴 response = %@", response);
    [self notiSett:response.notification.request.content.userInfo];
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    // Print message ID.
    NSDictionary *userInfo = notification.userInfo;
    
    [self notiSett:userInfo];
}

#pragma mark - notication
- (void)notiSett:(NSDictionary *)userInfo {
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
        
        if(userInfo){
            // 로그인 후 HomeViewController 나왔을 때 바로 열지도나 건강뉴스, 일반공지 화면으로 넘어갈 수 있도록 UserDefaults에 기록
            NSDictionary *aps = userInfo[@"aps"];
            if(aps) {
                NSString *pushType = aps[@"I_BUFFER2"];
                if([aps[@"I_BUFFER2"] isKindOfClass:[NSNumber class]]) {
                    pushType = [aps[@"I_BUFFER2"] stringValue];
                }
                NSDictionary *dic = nil;
                // 공지
                if([pushType isEqualToString:@"1"]) {
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowNoti", @"notiname", nil];
                }
                // 새건강정보(건강컬럼, 건강식단)
                else if([pushType isEqualToString:@"2"]
                        || [pushType isEqualToString:@"12"]) {
                    
                    NSString *url = aps[@"url"];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowHealthInfo", @"notiname",
                           url, @"url",
                           @"0", @"tab",
                           pushType, @"I_BUFFER2", nil];
                }
                // 댓글
                else if([pushType isEqualToString:@"4"]) {
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowCommunityReply", @"notiname", nil];
                }
                // 일일미션
                else if([pushType isEqualToString:@"5"]) {
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowDayMisssion", @"notiname", nil];
                }
                // 언급
                else if([pushType isEqualToString:@"6"]) {
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowCommunityAbout", @"notiname", nil];
                }
                // 게시글좋아요
                else if([pushType isEqualToString:@"7"]) {
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowCommunityLike", @"notiname", nil];
                }
                // 건강관리
                else if([pushType isEqualToString:@"8"]) {
                    NSString *index = aps[@"index"];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:@"ShouldShowHealth", @"notiname",
                           index, @"index", nil];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationPageIfNeed" object:nil userInfo:dic];
                
            }
        }
        
    }];
}

#pragma mark - landscape
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    UIViewController* topvc = self.window.rootViewController;
    
    while (true) {
        if(topvc.presentedViewController == nil) {
            break;
        }
        topvc = topvc.presentedViewController;
    }
    
    NSString *className = [NSString stringWithFormat:@"%@", topvc.class];
    if ([className isEqualToString:@"SugarGraphZoomVC"]     ||
        [className isEqualToString:@"PresuGraphZoomVC"]     ||
        [className isEqualToString:@"WeightGraphZoomVC"]    ||
        [className isEqualToString:@"WalkGraphZoomVC"]      ||
        [className isEqualToString:@"UIAlertController"]      ||
        [className isEqualToString:@"FoodGraphZoomVC"]) {
        
        return UIInterfaceOrientationMaskAll;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
