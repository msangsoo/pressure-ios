//
//  DuplicationAlertViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AlertType) {
    feedDeleteType,
    feedDupleType
};

@interface DuplicationAlertViewController : UIViewController

@property(copy) void (^touchedDoneButton)(void);
@property (weak, nonatomic) IBOutlet UILabel *dupleBodyLabel;
@property (weak, nonatomic) IBOutlet UIStackView *deleteTypeStackView;
@property (weak, nonatomic) IBOutlet UIStackView *dupleTypeStackView;
@property (weak, nonatomic) IBOutlet UIView *dimView;

@property (weak, nonatomic) IBOutlet UIButton *dupleDoneButton;

- (IBAction)touchedCancelButton:(UIButton *)sender;
- (IBAction)touchedDoneButton:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
