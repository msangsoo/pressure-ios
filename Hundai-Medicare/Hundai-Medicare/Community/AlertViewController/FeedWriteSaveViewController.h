//
//  FeedWriteSaveViewController.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 25/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedWriteSaveViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property(copy) void (^touchedCancelButton)(void);
@property(copy) void (^touchedDoneButton)(void);

- (IBAction)touchedCancelButton:(UIButton *)sender;
- (IBAction)touchedDoneButton:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
