//
//  NickNameInputAlertViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface NickNameInputAlertViewController : BaseViewController

@property(copy) void (^touchedDoneButton)(NSString *, BOOL);
@property(copy) void (^touchedCancelButton)(void);
@property(nonatomic, strong) NSString *nickName;
@property (weak, nonatomic) IBOutlet UIButton *publicButton;
@property (weak, nonatomic) IBOutlet UIButton *privateButton;
@property(nonatomic, assign) BOOL isPublic;
@property (weak, nonatomic) IBOutlet UIImageView *publicImageView;
@property (weak, nonatomic) IBOutlet UIImageView *privateImageView;
@property (weak, nonatomic) IBOutlet UITextField *nickNameInputTextField;
@property (weak, nonatomic) IBOutlet UILabel *sickNessLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;


- (IBAction)touchedPublicOrPrivateButton:(UIButton *)sender;

- (IBAction)touchedCancelButton:(UIButton *)sender;
- (IBAction)touchedDoneButton:(UIButton *)sender;

@end
