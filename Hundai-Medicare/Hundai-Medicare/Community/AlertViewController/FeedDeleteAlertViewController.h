//
//  FeedDeleteAlertViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedDeleteAlertViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property(copy) void (^touchedCancelButton)(void);
@property(copy) void (^touchedDoneButton)(void);

- (IBAction)touchedCancelButton:(UIButton *)sender;
- (IBAction)touchedDoneButton:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
