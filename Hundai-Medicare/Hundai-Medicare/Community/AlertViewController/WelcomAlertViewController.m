//
//  WelcomAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WelcomAlertViewController.h"

@interface WelcomAlertViewController ()
@property (weak, nonatomic) IBOutlet UILabel *feedCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;

@end

@implementation WelcomAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    NSMutableAttributedString *mAttrStr = [[NSMutableAttributedString alloc]init];
    NSAttributedString *nickName = [[NSAttributedString alloc]initWithString:self.nickName attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(18), NSForegroundColorAttributeName:RGB(105, 129, 236)}];
    NSAttributedString *welcomStr = [[NSAttributedString alloc]initWithString:@" 님 환영합니다." attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(18), NSForegroundColorAttributeName:RGB(143, 144, 158)}];

    [mAttrStr appendAttributedString:nickName];
    [mAttrStr appendAttributedString:welcomStr];
    self.nickNameLabel.attributedText = mAttrStr;

    self.doneButton.layer.borderColor = RGB(105, 129, 236).CGColor;
    self.doneButton.layer.borderWidth = 1.0;
    self.doneButton.layer.cornerRadius = self.doneButton.bounds.size.height/2.0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchedDoneButton:(UIButton *)sender {
    _touchedDoneButton();
}
@end
