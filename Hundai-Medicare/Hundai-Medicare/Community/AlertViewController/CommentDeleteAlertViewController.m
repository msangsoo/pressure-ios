//
//  CommentDeleteAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommentDeleteAlertViewController.h"

@interface CommentDeleteAlertViewController ()

@end

@implementation CommentDeleteAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_cancelButton borderRadiusButton:COLOR_GRAY_LIGHT];
    [_doneButton backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchedCancelButton:(UIButton *)sender {
    self.commentCancel();
}

- (IBAction)touchedDoneButton:(UIButton *)sender {
    self.commentDelete();
}
@end
