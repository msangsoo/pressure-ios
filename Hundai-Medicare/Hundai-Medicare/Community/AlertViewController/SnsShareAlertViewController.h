//
//  SnsShareAlertViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SnsShareAlertViewController : UIViewController

@property(copy) void (^touchedCancelButton)(void);
@property(copy) void (^touchedSmsButton)(void);
@property(copy) void (^touchedKakaoButton)(void);
//@property(copy) void (^touchedDeleteButton)(void);

- (IBAction)touchedCancelButton:(UIButton *)sender;
- (IBAction)touchedSmsButton:(UIButton *)sender;
- (IBAction)touchedKakaoButton:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
