//
//  SnsShareAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SnsShareAlertViewController.h"

@interface SnsShareAlertViewController ()
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation SnsShareAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.cancelButton.layer.borderColor = RGB(105, 129, 236).CGColor;
//    self.cancelButton.layer.borderWidth = 1.0;
//    self.cancelButton.layer.cornerRadius = self.cancelButton.bounds.size.height/2.0;
    [self.cancelButton borderRadiusButton:COLOR_GRAY_SUIT];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchedCancelButton:(UIButton *)sender {
    self.touchedCancelButton();
}

- (IBAction)touchedSmsButton:(UIButton *)sender {
    self.touchedSmsButton();
}

- (IBAction)touchedKakaoButton:(UIButton *)sender {
    self.touchedKakaoButton();
}
@end
