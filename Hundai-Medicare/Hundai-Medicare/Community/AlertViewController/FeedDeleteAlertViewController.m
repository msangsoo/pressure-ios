//
//  FeedDeleteAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FeedDeleteAlertViewController.h"

@interface FeedDeleteAlertViewController ()

@end

@implementation FeedDeleteAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_cancelButton borderRadiusButton:COLOR_GRAY_SUIT];
    [_doneButton backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchedCancelButton:(UIButton *)sender {
    self.touchedCancelButton();
}

- (IBAction)touchedDoneButton:(UIButton *)sender {
    self.touchedDoneButton();
}
- (IBAction)cancelButton:(id)sender {
}

- (IBAction)doneButton:(id)sender {
}
@end
