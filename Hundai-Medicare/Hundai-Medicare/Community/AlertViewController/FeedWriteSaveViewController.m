//
//  FeedWriteSaveViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 25/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "FeedWriteSaveViewController.h"

@implementation FeedWriteSaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_cancelButton borderRadiusButton:COLOR_GRAY_SUIT];
    [_doneButton backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)touchedCancelButton:(UIButton *)sender {
    self.touchedCancelButton();
}

- (IBAction)touchedDoneButton:(UIButton *)sender {
    self.touchedDoneButton();
}

@end
