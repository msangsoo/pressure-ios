//
//  FeedModifyAlertViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedModifyAlertViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *modifyButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property(copy) void (^touchedModifyButton)(void);
@property(copy) void (^touchedDeleteButton)(void);

- (IBAction)touchedModifybutton:(UIButton *)sender;
- (IBAction)touchedDeleteButton:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
