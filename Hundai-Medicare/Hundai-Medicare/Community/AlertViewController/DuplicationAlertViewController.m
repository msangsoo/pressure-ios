//
//  DuplicationAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "DuplicationAlertViewController.h"

@interface DuplicationAlertViewController ()

@end

@implementation DuplicationAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setupViews];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupViews {
    self.dupleDoneButton.layer.borderColor = RGB(105, 129, 236).CGColor;
    self.dupleDoneButton.layer.borderWidth = 1.0;
    self.dupleDoneButton.layer.cornerRadius = self.dupleDoneButton.bounds.size.height/2.0;
}

- (IBAction)touchedCancelButton:(UIButton *)sender {
}

- (IBAction)touchedDoneButton:(UIButton *)sender {
    self.touchedDoneButton();
}
@end
