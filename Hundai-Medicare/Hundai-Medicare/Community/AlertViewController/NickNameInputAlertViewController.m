//
//  NickNameInputAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NickNameInputAlertViewController.h"
#import "CommunityCommon.h"
#import "Masonry.h"
#import "WelcomAlertViewController.h"

@interface NickNameInputAlertViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewCenterYContraint;

@end

@implementation NickNameInputAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


        // Register keyboard events
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
//    self.isPublic = TRUE;

    [self setupViews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
}
- (void)dismissKeyboard:(UITapGestureRecognizer *)sender {
    [self.nickNameInputTextField resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupViews {
    UIImage *selectedImage = [UIImage imageNamed:@"commu_check_01"];
    UIImage *deSelectedImage = [UIImage imageNamed:@"commu_check_02"];

    [self.publicButton setSelected:self.isPublic];
    [self.privateButton setSelected:!self.isPublic];

    self.publicImageView.image = self.isPublic == TRUE ? selectedImage : deSelectedImage;
    self.privateImageView.image = self.isPublic == TRUE ? deSelectedImage : selectedImage;

    self.nickNameInputTextField.layer.borderColor = RGB(105, 129, 236).CGColor;
    self.nickNameInputTextField.layer.borderWidth = 1.0;
    self.cancelButton.layer.borderColor = RGB(132, 133, 148).CGColor;
    self.cancelButton.layer.borderWidth = 1.0;
    self.cancelButton.layer.cornerRadius = self.cancelButton.bounds.size.height/2.0;
    self.doneButton.layer.cornerRadius = self.doneButton.bounds.size.height/2.0;

//disease_nm - 1:고혈압, 2:당뇨, 3:고지혈증, 4:비만, 5:없음 6:기타
    NSMutableString *diseaseStr = [NSMutableString stringWithString:@"나의 질환 : "];
    if ([_model.getLoginData.disease_nm isEqualToString:@"1"]) {
        [diseaseStr appendString: @"고혈압"];
    } else if ([_model.getLoginData.disease_nm isEqualToString:@"2"]) {
        [diseaseStr appendString: @"당뇨"];
    } else if ([_model.getLoginData.disease_nm isEqualToString:@"3"]) {
        [diseaseStr appendString: @"고지혈증"];
    } else if ([_model.getLoginData.disease_nm isEqualToString:@"4"]) {
        [diseaseStr appendString: @"비만"];
    } else if ([_model.getLoginData.disease_nm isEqualToString:@"5"]) {
        [diseaseStr appendString: @"없음"];
    } else {
        [diseaseStr appendString:_model.getLoginData.disease_txt];
    }

    if(self.nickName != nil && ![self.nickName isEqualToString:@""]) {
        self.nickNameInputTextField.text = self.nickName;
    }
    self.sickNessLabel.text = diseaseStr;

}

- (void)keyboardWillShowNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    UIViewAnimationCurve curve = [[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    CGFloat duration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect keyboardFrame = [[userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame toView:self.view];

    [UIView animateWithDuration:duration delay:0.0f options:(UIViewAnimationOptions)curve animations:^{
        CGRect textFieldFrame = self.nickNameInputTextField.frame;
        textFieldFrame.origin.y = keyboardFrame.origin.y - CGRectGetHeight(textFieldFrame);
        self.bgViewCenterYContraint.constant = -textFieldFrame.size.height;
    }completion:nil];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    UIViewAnimationCurve curve = [[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    CGFloat duration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:duration delay:0.0f options:(UIViewAnimationOptions)curve animations:^{
//        self.bgView.frame = CGRectMake(self.bgView.frame.origin.x, CGRectGetMaxY(self.bgView.f), CGRectGetWidth(self.bgView.bounds), 50);
        self.bgViewCenterYContraint.constant = 0;
    }completion:nil];
}

- (IBAction)touchedPublicOrPrivateButton:(UIButton *)sender {
    if (sender.isSelected == TRUE) {
        return;
    }

    self.isPublic = !self.isPublic;

    UIImage *selectedImage = [UIImage imageNamed:@"commu_check_01"];
    UIImage *deSelectedImage = [UIImage imageNamed:@"commu_check_02"];

    [self.publicButton setSelected:self.isPublic];
    [self.privateButton setSelected:!self.isPublic];

    self.publicImageView.image = self.isPublic == TRUE ? selectedImage : deSelectedImage;
    self.privateImageView.image = self.isPublic == TRUE ? deSelectedImage : selectedImage;
}

- (IBAction)touchedCancelButton:(UIButton *)sender {
    _touchedCancelButton();
}

- (IBAction)touchedDoneButton:(UIButton *)sender {

    if (self.nickNameInputTextField.text == nil || self.nickNameInputTextField.text.length == 0) {
        return;
    }
    _touchedDoneButton(self.nickNameInputTextField.text, self.isPublic);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (string.length > 0 && textField.text.length >= 10) {
        return NO;
    }


    if (string.length > 0 && ![CommunityCommon checkValidateString:string]) {
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.nickName = textField.text;
}

@end
