//
//  FeedModifyAlertViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FeedModifyAlertViewController.h"

@interface FeedModifyAlertViewController ()

@end

@implementation FeedModifyAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(close:)];

    [_deleteButton borderRadiusButton:COLOR_NATIONS_BLUE];
    [_modifyButton borderRadiusButton:COLOR_NATIONS_BLUE];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)close:(UITapGestureRecognizer *)gesture {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchedModifybutton:(UIButton *)sender {
    self.touchedModifyButton();
}

- (IBAction)touchedDeleteButton:(UIButton *)sender {
    self.touchedDeleteButton();
}
@end
