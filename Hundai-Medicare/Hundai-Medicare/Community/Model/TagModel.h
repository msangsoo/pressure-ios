//
//  TagModel.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 23/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//@interface TagManager : NSObject
//@property (strong, nonatomic) NSArray* tags;
//-(int)getSeq:(NSString*)tag;
//-(NSString*)getTag:(int)seq;
//@end


@interface TagModel : NSObject
- (TagModel*) initWithJson:(NSDictionary *)json;
@property (assign, nonatomic) int seq;
@property (strong, nonatomic) NSString* tag;
@end

NS_ASSUME_NONNULL_END
