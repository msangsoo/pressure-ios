//
//  ProfileCommentModel.h
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef ProfileCommentModel_h
#define ProfileCommentModel_h

#import <Foundation/Foundation.h>

@interface ProfileCommentModel: NSObject

@property(nonatomic, strong)NSString *cmGubun;
@property(nonatomic, strong)NSString *cmSeq;
@property(nonatomic, strong)NSString *nick;
@property(nonatomic, strong)NSString *profilePick;
@property(nonatomic, strong)NSString *cmTitle;
@property(nonatomic, strong)NSString *cmContent;
@property(nonatomic, strong)NSString *cmImg1;
@property(nonatomic, strong)NSString *regDate;
@property(nonatomic, strong)NSArray *cmTag;
@property(nonatomic, strong)NSString *cmMeal;
@property(nonatomic, strong)NSString *hCnt;
@property(nonatomic, strong)NSString *myHeart;
@property(nonatomic, strong)NSString *rCnt;
@property(nonatomic, strong)NSString *mbrGrad;
@property(nonatomic, strong)NSString *rNum;

- (instancetype)initWithJson:(NSDictionary *) json;

@end

#endif /* ProfileCommentModel_h */
