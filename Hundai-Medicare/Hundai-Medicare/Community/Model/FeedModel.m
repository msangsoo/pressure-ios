//
//  FeedModel.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FeedModel.h"

@implementation FeedModel

- (instancetype) init {
    if (self = [super init]) {

        self.tPage = nil;
        self.cmSeq = nil;
        self.mberSn = nil;
        self.nick = nil;
        self.profilePic = nil;
        self.cmTitle = nil;
        self.cmContent = nil;
        self.cmImg1 = nil;
        self.cmImg2 = nil;
        self.cmImg3 = nil;
        self.regDate = nil;
        self.cmTag = [@[] mutableCopy];
        self.hCnt = 0;
        self.myHeart = nil;
        self.rCnt = 0;
        self.rNum = 0;
        self.mberGrad = nil;
    }
    return self;
}
/*
- (instancetype) initWithJson:(NSDictionary *)json {
    if (self = [super init]) {
        self.tPage = [NSString stringWithString:json[@"TPAGE"]];
        self.cmSeq = [NSString stringWithString:json[@"CM_SEQ"]];
        self.mberSn = [NSString stringWithString:json[@"MBER_SN"]];
        self.nick = [NSString stringWithString:json[@"NICK"]];
        self.profilePic = [NSString stringWithString:json[@"PROFILE_PIC"]];
        self.cmTitle = [NSString stringWithString:json[@"CM_TITLE"]];
        self.cmContent = [NSString stringWithString:json[@"CM_CONTENT"]];
        self.cmImg1 = [NSString stringWithString:json[@"CM_IMG1"]];
        self.cmImg2 = [NSString stringWithString:json[@"CM_IMG3"]];
        self.cmImg3 = [NSString stringWithString:json[@"CM_IMG3"]];
        self.regDate = [NSString stringWithString:json[@"REGDATE"]];
        self.cmTag = [NSArray arrayWithArray:json[@"CM_TAG"]];
        self.cmMeal = [[[[NSString stringWithString:json[@"CM_MEAL"]] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""]stringByReplacingOccurrencesOfString:@"|" withString:@"/"];
        self.hCnt = [json[@"HCNT"] integerValue];
        self.myHeart = [NSString stringWithString:json[@"MYHEART"]];
        self.rCnt = [json[@"RCNT"] integerValue];
        self.rNum = [json[@"RNUM"] integerValue];
        self.mberGrad = [NSString stringWithString:json[@"MBER_GRAD"]];
    }

    return self;
}
    */

- (instancetype) initWithJson:(NSDictionary *)json {
    if (self = [super init]) {
        if (json[@"TPAGE"] != nil) {
            self.tPage = [NSString stringWithString:json[@"TPAGE"]];
        }
        if (json[@"CM_SEQ"] != nil) {
            self.cmSeq = [NSString stringWithString:json[@"CM_SEQ"]];
        }
        if (json[@"MBER_SN"] != nil) {
            self.mberSn = [NSString stringWithString:json[@"MBER_SN"]];
        }
        if (json[@"OSEQ"] != nil) {
            self.oSeq = [NSString stringWithString:json[@"OSEQ"]];
        }
        if (json[@"NICK"] != nil) {
            self.nick = [NSString stringWithString:json[@"NICK"]];
        }
        if (json[@"PROFILE_PIC"] != nil) {
            self.profilePic = [NSString stringWithString:json[@"PROFILE_PIC"]];
        }
        if (json[@"CM_TITLE"] != nil) {
            self.cmTitle = [NSString stringWithString:json[@"CM_TITLE"]];
        }
        if (json[@"CM_CONTENT"] != nil) {
            self.cmContent = [NSString stringWithString:json[@"CM_CONTENT"]];
        }
        if (json[@"CM_IMG1"] != nil) {
            self.cmImg1 = [NSString stringWithString:json[@"CM_IMG1"]];
        }
        if (json[@"CM_IMG3"] != nil) {
            self.cmImg2 = [NSString stringWithString:json[@"CM_IMG3"]];
        }
        if (json[@"CM_IMG3"] != nil) {
            self.cmImg3 = [NSString stringWithString:json[@"CM_IMG3"]];
        }
        if (json[@"REGDATE"] != nil) {
            self.regDate = [NSString stringWithString:json[@"REGDATE"]];
        }
        if (json[@"CM_TAG"] != nil) {
            self.cmTag = [[NSArray arrayWithArray:json[@"CM_TAG"]] mutableCopy];
        }
        if (json[@"CM_MEAL"] != nil) {
            self.cmMeal = [[[[NSString stringWithString:json[@"CM_MEAL"]] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""]stringByReplacingOccurrencesOfString:@"|" withString:@"/"];
        }
        if (json[@"HCNT"] != nil) {
            self.hCnt = [json[@"HCNT"] integerValue];
        }
        if(json[@"MYHEART"] != nil) {
            self.myHeart = [NSString stringWithString:json[@"MYHEART"]];
        } else if(json[@"HYN"] != nil) {
            self.myHeart = [NSString stringWithString:json[@"HYN"]];
        }

        if (json[@"RCNT"] != nil) {
            self.rCnt = [json[@"RCNT"] integerValue];
        }
        if (json[@"RNUM"] != nil) {
            self.rNum = [json[@"RNUM"] integerValue];
        }
        if (json[@"MBER_GRAD"]) {
            self.mberGrad = [NSString stringWithString:json[@"MBER_GRAD"]];
        }
    }

    return self;
}
-(FeedModel*)copy
{
    FeedModel* dest = [[FeedModel alloc] init];
    dest.tPage = [self.tPage copy];
    dest.cmSeq = [self.cmSeq copy];
    dest.mberSn = [self.mberSn copy];
    dest.oSeq = [self.oSeq copy];
    dest.nick = [self.nick copy];
    dest.profilePic = [self.profilePic copy];
    dest.cmTitle = [self.cmTitle copy];
    dest.cmContent = [self.cmContent copy];
    dest.cmImg1 = [self.cmImg1 copy];
    dest.cmImg2 = [self.cmImg2 copy];
    dest.cmImg3 = [self.cmImg3 copy];
    dest.regDate = [self.regDate copy];
    dest.cmTag = [self.cmTag mutableCopy];
    dest.cmMeal = [self.cmMeal copy];
    dest.hCnt = self.hCnt;
    dest.myHeart = [self.myHeart copy];
    dest.rCnt = self.rCnt;
    dest.rNum = self.rNum;
    dest.mberGrad = [self.mberGrad copy];
    return dest;
}
@end


