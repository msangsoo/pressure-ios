//
//  ProfileModel.m
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ProfileModel.h"

@implementation ProfileModel

- (instancetype)init {
    if (self = [super init]) {
        self.mberSN = nil;
        self.mberNM = nil;
        self.mberSex = nil;
        self.profilePick = nil;
        self.diseaseOpen = nil;
        self.diseaseNM = nil;
        self.totPoint = nil;
        self.lv = nil;
        self.totPage = nil;
        self.totCnt = nil;
        self.mberGrad = nil;
        self.nickName = nil;
    }
    return self;
}

- (instancetype)initWithJson:(NSDictionary *) json {
    
    if (self = [super init]) {
        if (json[@"MBER_SN"]) {
            self.mberSN = [NSString stringWithString:json[@"MBER_SN"]];
        }
        if (json[@"MBER_NM"]) {
            self.mberNM = [NSString stringWithString:json[@"MBER_NM"]];
        }
        if (json[@"NICKNAME"]) {
            self.nickName = [NSString stringWithString:json[@"NICKNAME"]];
        }
        if (json[@"MBER_SEX"]) {
            self.mberSex = [NSString stringWithString:json[@"MBER_SEX"]];
        }
        if (json[@"PROFILE_PIC"]) {
            self.profilePick = [NSString stringWithString:json[@"PROFILE_PIC"]];
        }
        if (json[@"DISEASE_OPEN"]) {
            self.diseaseOpen = [NSString stringWithString:json[@"DISEASE_OPEN"]];
        }
        if (json[@"DISEASE_NM"]) {
            self.diseaseNM = [NSString stringWithString:json[@"DISEASE_NM"]];
        }
        if (json[@"TOT_POINT"]) {
            self.totPoint = [NSString stringWithString:json[@"TOT_POINT"]];
        }
        if (json[@"LV"]) {
            self.lv = [NSString stringWithString:json[@"LV"]];
        }
        if (json[@"TOT_PAGE"]) {
            self.totPage = [NSString stringWithString:json[@"TOT_PAGE"]];
        }
        if (json[@"TOT_CNT"]) {
            self.totCnt = [NSString stringWithString:json[@"TOT_CNT"]];
        }
        if (json[@"MBER_GRAD"]) {
            self.mberGrad = [NSString stringWithString:json[@"MBER_GRAD"]];
        }
    }

    return self;
}

@end
