//
//  FeedModel.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef FeedModel_h
#define FeedModel_h

#import <Foundation/Foundation.h>

/*
 AST_LENGTH : 배열의 원소 개수
 ADDR_MASS : 배열
 TPAGE : 총 페이지수
 CM_SEQ : 커뮤니티 기본키(회원키)
 MBER_SN : 회원키
 NICK : 닉네임(5글자이내)
 PROFILE_PIC : 프로필 사진
 CM_TITLE : 제목
 CM_CONTENT : 글 내용
 REGDATE : 등록일
 CM_TAG : 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 HCNT : 글 당 하트 수
 MYHEART : 내가 좋아요 한 글 여부(Y/N)
 RCNT : 글 당 댓글 수
 MBER_GRAD : 정회원 10, 준회원 20
 */
@interface FeedModel: NSObject

@property (nonatomic, strong) NSString *tPage;
@property (nonatomic, strong) NSString *cmSeq;
@property (nonatomic, strong) NSString *mberSn;
@property (nonatomic, strong) NSString *oSeq;
@property (nonatomic, strong) NSString *nick;
@property (nonatomic, strong) NSString *profilePic;
@property (nonatomic, strong) NSString *cmTitle;
@property (nonatomic, strong) NSString *cmContent;
@property (nonatomic, strong) NSString *cmImg1;
@property (nonatomic, strong) NSString *cmImg2;
@property (nonatomic, strong) NSString *cmImg3;
@property (nonatomic, strong) NSString *regDate;
@property (nonatomic, strong) NSMutableArray *cmTag;
@property (nonatomic, strong) NSString *cmMeal;
@property (nonatomic, assign) NSInteger hCnt;
@property (nonatomic, strong) NSString *myHeart;
@property (nonatomic, assign) NSInteger rCnt;
@property (nonatomic, assign) NSInteger rNum;
@property (nonatomic, strong) NSString *mberGrad;

@property (nonatomic, strong) UIImage *uploadImage;
@property (nonatomic, assign) BOOL isDeleteImage;

- (instancetype) initWithJson:(NSDictionary *)json;
- (FeedModel*) copy;
@end

#endif /* FeedModel_h */
