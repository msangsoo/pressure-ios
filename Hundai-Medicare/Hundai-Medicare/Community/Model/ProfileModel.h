//
//  ProfileModel.h
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef ProfileModel_h
#define ProfileModel_h

#import <Foundation/Foundation.h>

@interface ProfileModel: NSObject

@property(nonatomic, strong)NSString *mberSN;
@property(nonatomic, strong)NSString *mberNM;
@property(nonatomic, strong)NSString *mberSex;
@property(nonatomic, strong)NSString *profilePick;
@property(nonatomic, strong)NSString *diseaseOpen;
@property(nonatomic, strong)NSString *diseaseNM;
@property(nonatomic, strong)NSString *totPoint;
@property(nonatomic, strong)NSString *lv;
@property(nonatomic, strong)NSString *nickName;
@property(nonatomic, strong)NSString *totPage;
@property(nonatomic, strong)NSString *totCnt;
@property(nonatomic, strong)NSString *mberGrad;

- (instancetype)initWithJson:(NSDictionary *) json;

@end

#endif /* ProfileModel_h */
