//
//  CommentModel.h
//  Hundai-Medicare
//
//  Created by Paul P on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef CommentModel_h
#define CommentModel_h

#import <Foundation/Foundation.h>

/*
 Input 값
 SEQ: 회원일련번호(MBER_SN값)
 CM_SEQ: 커뮤니티 기본키
 PG_SIZE: 페이지사이즈 (페이지당 리스트 개수)
 PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)

 Output 값
 AST_LENGTH : 배열의 원소 개수
 ADDR_MASS :    배열
 TPAGE : 총 페이지수
 CC_SEQ : 암환자 커뮤니티 댓글 기본키
 CM_SEQ : 암환자 커뮤니티 기본키
 OSEQ : 댓글 글쓴이 회원일련번호
 NICK : 닉네임(5글자이내)
 PROFILE_PIC : 작성자 프로필 사진
 CM_CONTENT : 댓글내용
 MBER_GRAD : 정회원 10, 준회원 20
 REGDATE : 등록일

 "CC_SEQ": "6",
 "NICK": "인시스템즈",
 "SEQ": "1871",
 "PROFILE_PIC": "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/profile/th.jpg",
 "CC_CONTENT": "한글깨짐",
 "MBR_GRD": "10",
 "REGDATE": "2019-02-18 오후 8:03:00"

 */
@interface CommentModel: NSObject

@property (nonatomic, strong) NSString *tPage;
@property (nonatomic, strong) NSString *ccSeq;
@property (nonatomic, strong) NSString *cmSeq;
@property (nonatomic, strong) NSString *oSeq;
@property (nonatomic, strong) NSString *seq;
@property (nonatomic, strong) NSString *nick;
@property (nonatomic, strong) NSString *profilePic;
@property (nonatomic, strong) NSString *ccContent;
@property (nonatomic, strong) NSString *cmContent;
@property (nonatomic, strong) NSString *regDate;
@property (nonatomic, strong) NSString *mberGrad;

- (instancetype) initWithJson:(NSDictionary *)json;

@end

#endif /* CommentModel_h */
