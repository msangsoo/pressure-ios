//
//  TagModel.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 23/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "TagModel.h"

@implementation TagModel
- (TagModel*) initWithJson:(NSDictionary *)json {
    if (self = [super init]) {
        if (json[@"TAG_SEQ"]) {
            self.seq = [[NSString stringWithString:json[@"TAG_SEQ"]] intValue];
        }
        if (json[@"TAG_WORD"]) {
            self.tag = [NSString stringWithString:json[@"TAG_WORD"]];
        }
    }

    return self;
}
@end
