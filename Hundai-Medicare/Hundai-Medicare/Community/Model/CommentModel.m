//
//  CommentModel.m
//  Hundai-Medicare
//
//  Created by Paul P on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel

- (instancetype) init {
    if (self = [super init]) {

        self.tPage = nil;
        self.cmSeq = nil;
        self.ccSeq = nil;
        self.oSeq = nil;
        self.nick = nil;
        self.profilePic = nil;
        self.ccContent = nil;
        self.regDate = nil;
        self.mberGrad = nil;
    }
    return self;
}

- (instancetype) initWithJson:(NSDictionary *)json {
    if (self = [super init]) {

        if (json[@"TPAGE"] != nil) {
            self.tPage = [NSString stringWithString:json[@"TPAGE"]];
        }
        if (json[@"CM_SEQ"] != nil) {
            self.cmSeq = [NSString stringWithString:json[@"CM_SEQ"]];
        }
        if (json[@"CC_SEQ"] != nil) {
            self.ccSeq = [NSString stringWithString:json[@"CC_SEQ"]];
        }
        if (json[@"OSEQ"] != nil) {
            self.oSeq = [NSString stringWithString:json[@"OSEQ"]];
        }
        if (json[@"SEQ"] != nil) {
            self.seq = [NSString stringWithString:json[@"SEQ"]];
        }
        if (json[@"NICK"] != nil) {
            self.nick = [NSString stringWithString:json[@"NICK"]];
        }
        if (json[@"PROFILE_PIC"] != nil) {
            self.profilePic = [NSString stringWithString:json[@"PROFILE_PIC"]];
        }
        if (json[@"CC_CONTENT"]) {
            self.ccContent = [NSString stringWithString:json[@"CC_CONTENT"]];
        }
        if (json[@"CM_CONTENT"]) {
            self.cmContent = [NSString stringWithString:json[@"CM_CONTENT"]];
        }
        if (json[@"REGDATE"] != nil) {
            self.regDate = [NSString stringWithString:json[@"REGDATE"]];
        }
        if (json[@"MBER_GRAD"] != nil) {
            self.mberGrad = [NSString stringWithString:json[@"MBER_GRAD"]];
        }
        if (json[@"MBER_GRD"] != nil) {
            self.mberGrad = [NSString stringWithString:json[@"MBER_GRD"]];
        }
    }

    return self;
}

@end
