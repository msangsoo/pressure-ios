//
//  ProfileCommentModel.m
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ProfileCommentModel.h"

@implementation ProfileCommentModel

- (instancetype)init {
    if (self = [super init]) {
        self.cmGubun = nil;
        self.cmSeq = nil;
        self.nick = nil;
        self.profilePick = nil;
        self.cmTitle = nil;
        self.cmContent = nil;
        self.cmImg1 = nil;
        self.regDate = nil;
        self.cmTag = nil;
    }

    return self;
}

- (instancetype)initWithJson:(NSDictionary *)json {

    if (self = [super init]) {
        if (json[@"CM_GUBUN"]) {
            self.cmGubun = [NSString stringWithString:json[@"CM_GUBUN"]];
        }
        if (json[@"CM_SEQ"]) {
            self.cmSeq = [NSString stringWithString:json[@"CM_SEQ"]];
        }
        if (json[@"NICK"]) {
            self.nick = [NSString stringWithString:json[@"NICK"]];
        }
        if (json[@"PROFILE_PIC"]) {
            self.profilePick = [NSString stringWithString:json[@"PROFILE_PIC"]];
        }
        if (json[@"CM_TITLE"]) {
            self.cmTitle = [NSString stringWithString:json[@"CM_TITLE"]];
        }
        if (json[@"CM_CONTENT"]) {
            self.cmContent = [NSString stringWithString:json[@"CM_CONTENT"]];
        }
        if (json[@"CM_IMG1"]) {
            self.cmImg1 = [NSString stringWithString:json[@"CM_IMG1"]];
        }
        if (json[@"REGDATE"]) {
            self.regDate = [NSString stringWithString:json[@"REGDATE"]];
        }
        if (json[@"CM_TAG"]) {
            self.cmTag = [NSArray arrayWithArray:json[@"CM_TAG"]];
        }
        if (json[@"CM_MEAL"]) {
            self.cmMeal = [NSString stringWithString:json[@"CM_MEAL"]];
        }
        if (json[@"HCNT"]) {
            self.hCnt = [NSString stringWithString:json[@"HCNT"]];
        }
        if (json[@"MYHEART"]) {
            self.myHeart = [NSString stringWithString:json[@"MYHEART"]];
        }
        if (json[@"RCNT"]) {
            self.rCnt = [NSString stringWithString:json[@"RCNT"]];
        }
        if (json[@"MBR_GRAD"]) {
            self.mbrGrad = [NSString stringWithString:json[@"MBR_GRAD"]];
        }
        if (json[@"HCNT"]) {
            self.hCnt = [NSString stringWithString:json[@"HCNT"]];
        }
    }
    return self;
}

@end
