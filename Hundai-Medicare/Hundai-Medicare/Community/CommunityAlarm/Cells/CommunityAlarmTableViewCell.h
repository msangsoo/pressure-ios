//
//  CommunityAlarmTableViewCell.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunityAlarmTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel* desc;
@property (nonatomic, weak) IBOutlet UILabel* date;
@property (nonatomic,weak) IBOutlet RoundedImageView* userProfileImage;
@property (nonatomic, weak) IBOutlet UIButton* btnProfile;
@end

NS_ASSUME_NONNULL_END
