//
//  CommunityAlarmTableViewCell.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunityAlarmTableViewCell.h"

@implementation CommunityAlarmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat width = self.userProfileImage.bounds.size.width;
    self.userProfileImage.layer.cornerRadius = width/2.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
