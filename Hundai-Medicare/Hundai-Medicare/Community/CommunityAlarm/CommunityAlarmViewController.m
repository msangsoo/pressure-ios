//
//  CommunityAlarmViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//
#import "CommunityAlarmViewController.h"
#import "CommunityAlarmTableViewCell.h"
#import "CommunityCommon.h"
#import "CommunityNetwork.h"
#import "ProfileViewController.h"
#import "FeedDetailViewController.h"

@import SDWebImage;

@interface CommunityAlarmViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@property (nonatomic) NSArray* alarmInfos;
@end

@implementation CommunityAlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM04 m_cod:HM04002 s_cod:HM04002001];
    
    self.alarmInfos = @[];
    // Do any additional setup after loading the view.

    [CommunityNetwork requestAlarmList:_model.getLoginData.mber_sn  response:^(id response, BOOL isSuccess) {
        self.alarmInfos = response;
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self.tableView reloadData];
        });
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - action method
- (IBAction)close:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* info = self.alarmInfos[indexPath.row];
    CommunityAlarmTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"alarmCell"];
    
    cell.desc.text = info[@"MSG"];
    [cell.desc setFontStyle:info[@"NICK"] font:FONT_NOTO_BOLD(15) color:RGB(0, 22, 45)];
//    cell.date.text = [CommunityCommon convertDateToFormatShort:info[@"REGDATE"]];
    cell.date.text = [CommunityCommon convertDateToFormat:info[@"REGDATE"]];
    NSString* profileImage = info[@"PROFILE_PIC"];
    if ([info[@"CM_GUBUN"] isEqualToString:@"2"]) {
        UIImage *hundaiLogo = [UIImage imageNamed:@"commu_l_04"];
        [cell.userProfileImage setImage:hundaiLogo];
    }
    else if(profileImage != nil && profileImage.length != 0)
    {
//        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:profileImage]];
        profileImage = [profileImage stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] options:0];
    }else {
        UIImage *hundaiLogo = [UIImage imageNamed:@"commu_l_03"];
        [cell.userProfileImage setImage:hundaiLogo];
    }
    
    cell.userProfileImage.layer.borderColor = RGB(219, 219, 219).CGColor;
    cell.userProfileImage.layer.borderWidth = 1.0;
    if ([info[@"MBER_GRAD"] isEqualToString:@"10"]) { // 정회원 : "10"
        cell.userProfileImage.layer.borderColor = RGB(243, 163, 41).CGColor;
    }
    cell.btnProfile.indexPath = indexPath;

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.alarmInfos.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary* info = self.alarmInfos[indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    FeedDetailViewController *vc = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    vc.isShowKeypad = FALSE;
    vc.seq = _model.getLoginData.mber_sn;
    vc.feedSeq = info[@"CM_SEQ"];
    [self.navigationController pushViewController:vc animated:TRUE];
}


-(IBAction)showProfile:(UIButton*)sender
{
    NSDictionary* info = self.alarmInfos[sender.indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    vc.memSeq = _model.getLoginData.mber_sn;
    vc.seq = info[@"MBER_SN"];
    [self.navigationController pushViewController:vc animated:TRUE];
}

@end

