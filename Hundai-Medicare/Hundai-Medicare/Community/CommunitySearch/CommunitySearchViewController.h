//
//  CommunitySearchViewController.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "BaseViewController.h"

@interface CommunitySearchViewController : BaseViewController
-(IBAction)cancelSearch:(id)sender;
@end
