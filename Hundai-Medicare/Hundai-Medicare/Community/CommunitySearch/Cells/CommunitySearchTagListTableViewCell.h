//
//  CommunitySearchTagListTableViewCell.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 11/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import <UIKit/UIKit.h>
@import TagListView_ObjC;

NS_ASSUME_NONNULL_BEGIN

@interface CommunitySearchTagListTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet TagListView* tagListView;
@end

NS_ASSUME_NONNULL_END
