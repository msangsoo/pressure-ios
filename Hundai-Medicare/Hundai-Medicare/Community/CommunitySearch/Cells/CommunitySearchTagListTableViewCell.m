//
//  CommunitySearchTagListTableViewCell.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 11/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunitySearchTagListTableViewCell.h"

@implementation CommunitySearchTagListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
