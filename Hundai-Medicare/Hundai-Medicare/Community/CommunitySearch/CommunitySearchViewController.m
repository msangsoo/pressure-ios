//
//  CommunitySearchViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunitySearchViewController.h"
#import "CommunityCommon.h"
#import "TagModel.h"
#import "CommunitySearchTagListTableViewCell.h"
#import "CommunitySearchResultViewController.h"
#import "CommunitySearchResultTagsViewController.h"
#import "RecentSearchDBWraper.h"

@import IQKeyboardManager;

@interface CommunitySearchViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@property (nonatomic,weak) IBOutlet UITextField* textField;
@property (nonatomic,weak) IBOutlet UIButton* cancelSerch;
@property (nonatomic,weak) CommunitySearchTagListTableViewCell* tagListCell;
@property (nonatomic,strong) NSMutableArray* searchKeywords;
@property (nonatomic,strong) RecentSearchDBWraper* db;
@property (assign) BOOL isEditingMode;
@end

@implementation CommunitySearchViewController
typedef enum {
    Sections_tagList = 0,
    Sections_recentHeader,
    Sections_recent,
    Sections_count
} Sections;

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM04 m_cod:HM04004 s_cod:HM04004001];
    // Do any additional setup after loading the view.
    _tableView.estimatedRowHeight = 213;

    self.db = [[RecentSearchDBWraper alloc] init];
    self.searchKeywords = [self.db getKeywords:10];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSMutableArray* arr = [self.db getKeywords:10];
    if(arr.count != self.searchKeywords.count)
    {
        self.searchKeywords = arr;
        if(self.textField.text.length > 0)
        {
            [self.tableView reloadData];
        }
    }
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:@"resultSegue"]){

         CommunitySearchResultViewController* dvc = segue.destinationViewController;
         dvc.searchKeyword = sender;
         [self.db insert:sender];
     }
     else if([segue.identifier isEqualToString:@"resultTagSegue"]){
         CommunitySearchResultTagsViewController* dvc = segue.destinationViewController;
         dvc.searchKeyword = sender;
     }
 }

#pragma mark - action method

- (IBAction)close:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)removeRecentKeyword:(UIButton*)sender
{
    NSLog(@"%@",sender.indexPath);
//    UIViewController* vc = [[UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil] instantiateViewControllerWithIdentifier:@"write"];
//    UINavigationController* nvc = [[UINavigationController alloc] initWithRootViewController:vc];
//    [self presentViewController:nvc animated:YES completion:nil];
    [self.db deleteDb:self.searchKeywords[sender.indexPath.row]];
    [self.searchKeywords removeObjectAtIndex:sender.indexPath.row];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:Sections_recent] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(IBAction)removeAllRecentKeyword:(UIButton*)sender
{
    [self.db truncateDb];
    [self.searchKeywords removeAllObjects];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:Sections_recent] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(IBAction)textFieldChanged:(UITextField*)sender{
    if(self.textField.text.length > 15)
    {
        self.textField.text = [self.textField.text substringWithRange:NSMakeRange(0, 15)];
    }
    if(self.textField.text.length == 0)
    {
        self.cancelSerch.backgroundColor = UIColor.lightGrayColor;
    }
    else
    {
        self.cancelSerch.backgroundColor = RGB(105, 129, 236);
    }
}

-(IBAction)search:(UIButton*)sender{
    [self textFieldShouldReturn:self.textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField.text.length > 0)
    {
        [self performSegueWithIdentifier:@"resultSegue" sender:textField.text];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.isEditingMode = YES;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadData];
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.isEditingMode = NO;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadData];
    }];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(indexPath.section)
    {
        case Sections_tagList :
        {
            if(self.tagListCell != nil)
            {
                return self.tagListCell;
            }
            CommunitySearchTagListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"tagListCell"];
            self.tagListCell = cell;
            [cell.tagListView removeAllTags];
            for(TagModel* tag in communityTagList)
            {
                TagView* tv = [cell.tagListView addTag:tag.tag];
                tv.onTap = ^(TagView* tv){
                    NSString* keyword = [tv titleForState:UIControlStateNormal];
                    NSLog(@"%@",keyword);
                    [self performSegueWithIdentifier:@"resultTagSegue" sender:keyword];//[keyword substringFromIndex:1]];
                };
            }
            return cell;
        }
        case Sections_recentHeader :
        {
            return [tableView dequeueReusableCellWithIdentifier:@"recentCellHeader"];
        }
        case Sections_recent :
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"recentCell"];
            UILabel* keyword = [cell viewWithTag:1];
            keyword.text = self.searchKeywords[indexPath.row];
            UIButton* removeButton = [cell viewWithTag:2];
            removeButton.indexPath = indexPath;
            return cell;
        }
        default :
            break;
    }
    return [[UITableViewCell alloc] init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case Sections_tagList :
            return self.textField.text.length > 0 || self.isEditingMode ? 0 : 1;
        case Sections_recentHeader :
            return self.textField.text.length > 0 || self.isEditingMode ? 1 : 0;
        case Sections_recent :
            return self.textField.text.length > 0 || self.isEditingMode ? self.searchKeywords.count : 0;
        default:
            break;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return Sections_count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case Sections_tagList :
            return UITableViewAutomaticDimension;
        case Sections_recentHeader :
            return 44;
        case Sections_recent :
            return 45;
        default:
            break;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    switch (indexPath.section) {
        case Sections_tagList :
            return;
        case Sections_recentHeader :
            return;
        case Sections_recent :
            NSLog(@"%@",indexPath);
            [self.textField resignFirstResponder];
            [self performSegueWithIdentifier:@"resultSegue" sender:self.searchKeywords[indexPath.row]];
            return;
        default:
            break;
    }
}

-(IBAction)cancelSearch:(id)sender
{
    self.textField.text = @"";
    [self.textField resignFirstResponder];
    self.cancelSerch.backgroundColor = UIColor.lightGrayColor;
    [self.tableView reloadData];
}
@end
