//
//  CommunityMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunityMainVC.h"
#import "NickNameInputAlertViewController.h"
#import "WelcomAlertViewController.h"
#import "CommunityNetwork.h"
#import "CommunityAlarmViewController.h"
#import "TagModel.h"
#import "CommunityTotalListVC.h"
#import "CommunityNoticeListVC.h"
#import "CommunityWriteViewController.h"
#import "DuplicationAlertViewController.h"
typedef enum : NSUInteger {
    total = 10,
    notice,
} MainButtonType;

@interface CommunityMainVC ()<UIScrollViewDelegate> {


}

@property (nonatomic, weak)NickNameInputAlertViewController *nickNameInputAlertViewController;
@property (nonatomic, assign)BOOL showFloatingButton;
@property (nonatomic, assign) BOOL isTotalList;
@property (weak, nonatomic) IBOutlet UIButton *generalButton;
@property (weak, nonatomic) IBOutlet UIButton *noticeButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomLineLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *contentsScrollView;
@property (weak, nonatomic) IBOutlet UIView *totalContainerView;
@property (weak, nonatomic) IBOutlet UIView *noticeContainerView;

@end

@implementation CommunityMainVC

- (void)viewDidLoad {
    [super viewDidLoad];

//    [self.navigationItem setTitle:@"Community"];
//    self.title = @"커뮤니티";

        //        NSMutableArray *feedModels = [NSMutableArray array];
        //        for (NSDictionary *item in jsonArray) {
        //            FeedModel *feed = [[FeedModel alloc]initWithJson:item];
        //            [feedModels addObject:feed];
        //        }

    [CommunityNetwork requestTagList];

    _isTotalList = TRUE;
    [self setupNavigationBar];


    CommunityTotalListVC *totalVC = (CommunityTotalListVC *)self.childViewControllers.firstObject;
    totalVC.scrollViewWillBeginDragging = ^{
        NSLog(@"totalVC.scrollViewWillBeginDragging");
        [self scrollViewWillBeginDragging];
    };
    totalVC.scrollViewDidEndDragging = ^{
        NSLog(@"totalVC.scrollViewDidEndDragging");
        [self scrollViewDidEndDragging];
    };

    CommunityNoticeListVC *noticeVC = (CommunityNoticeListVC *)self.childViewControllers.lastObject;
    noticeVC.scrollViewWillBeginDragging = ^{
//        NSLog(@"noticeVC.scrollViewWillBeginDragging");
//        [self scrollViewWillBeginDragging];
    };
    noticeVC.scrollViewDidEndDragging = ^{
//        NSLog(@"noticeVC.scrollViewDidEndDragging");
//        [self scrollViewDidEndDragging];
    };
    
    [self.generalButton setTitle:@"전체" forState:UIControlStateNormal];
    [self.generalButton setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.generalButton setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    [self.noticeButton setTitle:@"공지사항" forState:UIControlStateNormal];
    [self.noticeButton setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.noticeButton setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // nickname 없을때 False
    self.hasNickName = TRUE;
    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
        self.hasNickName = FALSE;
        [self checkHaveNickname:FALSE];
        [self.floatingButton setHidden:TRUE];
    }else {
        if ([[Utils getUserDefault:NICKNAME_FIRST_INIT] isEqualToString:@"Y"]) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault removeObjectForKey:NICKNAME_FIRST_INIT];
            [userDefault synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
            WelcomAlertViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WelcomAlertViewController"];
            vc.nickName = _model.getLoginData.nickname;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc.touchedDoneButton = ^{
                [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            };
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
        }
    }
//    [self checkHaveNickname:FALSE];
}

- (void)requestSetNickname:(NSString *)nickName isPublic:(BOOL)isPublic {
    NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
    NSString *seq = _model.getLoginData.mber_sn;

    [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {

        NSMutableDictionary *responseDic = [NSMutableDictionary dictionary];
        responseDic[@"isSuccess"] = @(isSuccess);
        responseDic[@"response"] = responseObj;

        if (isSuccess == TRUE) {
            self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
            self.hasNickName = TRUE;
            [self.floatingButton setHidden:FALSE];
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
            WelcomAlertViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WelcomAlertViewController"];
            vc.nickName = nickName;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc.touchedDoneButton = ^{
                [NSNotificationCenter.defaultCenter postNotificationName:@"PostNicknameNotification" object:responseDic userInfo:nil];
                [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            };
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
            return;
        }
        self.hasNickName = FALSE;
        NSError *error = (NSError *)responseObj;
        NSLog(@"Error : %@", [error localizedDescription]);

        /*
        if ([error code] != 4444) {
            [Utils basicAlertView:self withTitle:@"오류" withMsg:[NSString stringWithFormat:@"%@",error.description]];
            return;
        }

        // 실패시 알럿 띄우기
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        DuplicationAlertViewController *vc = (DuplicationAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DuplicationAlertViewController"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.dimView.alpha = 0.1;
        vc.touchedDoneButton = ^ {
            [self.nickNameInputAlertViewController dismissViewControllerAnimated:TRUE completion:nil];
        };
        [self.nickNameInputAlertViewController presentViewController:vc animated:TRUE completion:nil];
         */
    }];
}

- (void)checkHaveNickname:(BOOL)haveNickname {
    if (!haveNickname) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];

        self.nickNameInputAlertViewController = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
        self.nickNameInputAlertViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.nickNameInputAlertViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.nickNameInputAlertViewController.touchedCancelButton = ^{
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            [self.floatingButton setHidden:TRUE];
        };
        self.nickNameInputAlertViewController.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
            [self requestSetNickname:nickName isPublic:isPublic];
        };
        [self.navigationController.tabBarController presentViewController:self.nickNameInputAlertViewController animated:TRUE completion:nil];
    }
}

- (void)setupNavigationBar {
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName:FONT_NOTO_BOLD(18)};
}

- (IBAction)actionShowAlarmVC:(UIBarButtonItem *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
    CommunityAlarmViewController *ac = [storyboard instantiateViewControllerWithIdentifier:@"alarm"];

    [self.navigationController pushViewController:ac animated:YES];
}

- (IBAction)actionShowSearchVC:(UIBarButtonItem *)sender {

}

- (IBAction)touchedChageGeneralOrNotice:(UIButton *)sender {
    [LogDB insertDb:HM04 m_cod:HM04003 s_cod:HM04003001];
    
    MainButtonType type = (MainButtonType)sender.tag;

    self.isTotalList = FALSE;
    int offSet = self.view.bounds.size.width;

    if (type == total) {
        self.isTotalList = TRUE;
        offSet = 0;
    }

    [UIView animateWithDuration:0.3 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contentsScrollView.contentOffset = CGPointMake(offSet, 0);
        self.bottomLineLeadingConstraint.active = self.isTotalList;
    } completion:nil];

    [self.generalButton setSelected:self.isTotalList];
    [self.noticeButton setSelected:!self.isTotalList];

    [self loadViewIfNeeded];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView == self.contentsScrollView)
    {
        CGFloat offset = scrollView.contentOffset.x;

        self.isTotalList = FALSE;
        if (offset < scrollView.bounds.size.width / 2.0) {
            self.isTotalList = TRUE;
        }

        [UIView animateWithDuration:0.3 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.bottomLineLeadingConstraint.active = self.isTotalList;
        } completion:nil];

        [self.generalButton setSelected:self.isTotalList];
        [self.noticeButton setSelected:!self.isTotalList];

        if (self.hasNickName) {
            [self.floatingButton setHidden:!self.isTotalList];
        }
        [self loadViewIfNeeded];
    }
}

- (void)scrollViewWillBeginDragging {
    self.showFloatingButton = FALSE;
    [self floatingButton:self.showFloatingButton];
}

- (void)scrollViewDidEndDragging {
    self.showFloatingButton = TRUE;
    [self floatingButton:self.showFloatingButton];
}

- (void)floatingButton:(BOOL)show {
//    __block CGFloat topConstant = show == TRUE ? -110 : 0;
//    [UIView animateWithDuration:1.4 animations:^{
//        [self.floatingButtonTopConstraint setConstant:topConstant];
//    } completion:^(BOOL finished) {
//
//    }];
//    dispatch_async(dispatch_get_main_queue(), ^(void){
    if(show)
    {
        [self.floatingButton setHidden:NO];
    }
    [UIView animateWithDuration:0.3 animations:^{
        if(show)
        {
            self.floatingButton.transform = CGAffineTransformIdentity;
            self.floatingButton.alpha = 1;
        }
        else
        {
            self.floatingButton.transform = CGAffineTransformMakeScale(0.01, 0.01);
            self.floatingButton.alpha = 0;
        }
    } completion:^(BOOL finished) {
        [self.floatingButton setHidden:!show];
    }];
//    });


}
- (IBAction)touchedWriteFeed:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
    CommunityWriteViewController *vc = (CommunityWriteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"write"];

    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    [self.navigationController.tabBarController presentViewController:navi animated:TRUE completion:nil];
}

@end
