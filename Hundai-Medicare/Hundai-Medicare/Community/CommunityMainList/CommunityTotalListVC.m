//
//  CommunityTotalListVC.m
//  Hundai-Medicare
//
//  Created by Paul P on 04/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunityTotalListVC.h"
#import "CommunityUserInfoCell.h"
#import "ContentsImageCell.h"
#import "ContentsMealCell.h"
#import "ContentsTextCell.h"
#import "ContentsMoreCell.h"
#import "ContentsTagCell.h"
#import "SnsShareCell.h"
#import "CommunityInfoCell.h"
#import "CommunityCommon.h"
#import "CommunityNetwork.h"
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileViewController.h"
#import "FeedModifyAlertViewController.h"
#import "CommunityWriteViewController.h"
#import "FeedDeleteAlertViewController.h"
#import "SnsShareAlertViewController.h"
#import "PhotoDetailViewController.h"
#import "AXPhotoViewer-Swift.h"
#import "FeedDetailViewController.h"
#import <KakaoLink/KakaoLink.h>
#import <KakaoMessageTemplate/KakaoMessageTemplate.h>
#import <MessageUI/MessageUI.h>
#import "TagModel.h"
#import "CommunitySearchResultTagsViewController.h"

@interface CommunityTotalListVC () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, MFMessageComposeViewControllerDelegate>

@property(nonatomic, strong)UIRefreshControl *refreshControl;

@end

@implementation CommunityTotalListVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _pageSize = 0;
    _page = 1;

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshNewFeed:) name:@"refreshNewFeed" object:nil];

    self.feedModels = [NSMutableArray array];
    self.isFullTextArray = [NSMutableArray array];
    [self setupTableView];
    [self setupRefreshView];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadFeedData:) name:@"UpdateFeedModel" object:nil];
    [self requestFeedList:_page isRefresh:FALSE];
}

- (void)setupTableView {
    self.tableView.estimatedRowHeight = 45;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)setupRefreshView {
    _refreshControl = [[UIRefreshControl alloc]init];
    [_refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.tableView reloadData];
}

- (void)reloadFeedData:(NSNotification *)noti {
    FeedModel *receiveFeed = (FeedModel *)(noti.object);
    for (FeedModel *feed in self.feedModels) {
        if ([feed.cmSeq isEqualToString:receiveFeed.cmSeq]) {
            feed.myHeart = receiveFeed.myHeart;
            feed.hCnt = receiveFeed.hCnt;
            feed.rCnt = receiveFeed.rCnt;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (void)refreshNewFeed:(NSNotification *)noti {
    _page = 1;
    [self requestFeedList:_page isRefresh:TRUE];
}


#pragma mark -------- CustomFunction --------
- (void)requestFeedList:(NSInteger)page isRefresh:(BOOL)isRefresh {

    NSString *seq = _model.getLoginData.mber_sn;
    NSString *tag = @"";
    NSDictionary *header = [NSDictionary dictionary];
    NSString *type = @"1";
    NSString *search = @"";

    self.isNewDataLoading = TRUE;
    [CommunityNetwork requestGetFeedList:header seq:seq page:page type:type tag:tag search:search response:^(id response, BOOL isSuccess) {

        NSLog(@"%@", response);
        self.isNewDataLoading = FALSE;
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }

        if(isSuccess == FALSE || ((NSArray *)response).count == 0) {
            return;
        }

        if (isRefresh && [self.feedModels count] > 0) {
            self.page = 1;
            [self.feedModels removeAllObjects];
            [self.isFullTextArray removeAllObjects];
        }

        for (NSDictionary *feedDic in response) {
            [self.feedModels addObject:[[FeedModel alloc]initWithJson:feedDic]];
            [self.isFullTextArray addObject:[NSNumber numberWithBool:FALSE]];
        }

        [self.tableView reloadData];
        if (isRefresh) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView setContentOffset:CGPointZero animated:YES];
            });
        }
    }];
}

#pragma mark UIRefresh
- (void)handleRefresh:(UIRefreshControl *)sender {
    [self requestFeedList:1 isRefresh:TRUE];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (IBAction)selectedButtonAtCommunityTableView:(UIButton *)sender
{
    NSIndexPath *indexPath = sender.indexPath;
    FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];
    NSInteger tag = sender.tag;

    switch(indexPath.row) {
        case userInfoCell: {

            if(tag == 30) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                vc.memSeq = _model.getLoginData.mber_sn;
                vc.seq = feed.mberSn;
                [self.navigationController pushViewController:vc animated:TRUE];
                break;
            } else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedModifyAlertViewController *vc = (FeedModifyAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedModifyAlertViewController"];
                vc.touchedModifyButton = ^{
                    [LogDB insertDb:HM01 m_cod:HM04009 s_cod:HM04009001];
                    
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion: ^{
                        [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] completion:^(BOOL success){
                            if(success)
                            {
                                // refresh..
                            }
                        }];
                    }];
                    NSLog(@"list : touchedModifyButton");
                };

                vc.touchedDeleteButton = ^{
                    [LogDB insertDb:HM01 m_cod:HM04010 s_cod:HM04010001];
                    
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion: ^{
                        FeedDeleteAlertViewController *vc = (FeedDeleteAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDeleteAlertViewController"];
                        vc.touchedCancelButton = ^{
                            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                        };
                        vc.touchedDoneButton = ^{
                            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:^{
                                [self deleteFeed:feed];
                            }];
                        };
                        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

                        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
                    }];
                    NSLog(@"list : touchedDeleteButton");
                };

                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

                [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
            }
        } break;
        case imageCell: {
            NSArray *photos = @[[[AXPhoto alloc]initWithAttributedTitle:nil attributedDescription:nil attributedCredit:nil imageData:nil image:nil url:[NSURL URLWithString:feed.cmImg1]]];

            AXPhotosDataSource *dataSource = [[AXPhotosDataSource alloc]initWithPhotos:photos];
//            AXPagingConfig *config = [[AXPagingConfig alloc]initWithLoadingViewClass:CustomLoadingView.self];
            AXPhotosViewController *vc = [[AXPhotosViewController alloc]initWithDataSource:dataSource];
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
        } break;
        case snsShareCell: {
            if (tag == 10) {
                [LogDB insertDb:HM01 m_cod:HM04005 s_cod:HM04005001];
                
                [CommunityNetwork requestRegistLike:@{} seq:_model.getLoginData.mber_sn feedSeq:feed.cmSeq response:^(id responseObj, BOOL isSuccess) {
                    if ([feed.myHeart isEqualToString:@"Y"]) {
                        feed.myHeart = @"N";
                        feed.hCnt -= 1;
                    } else {
                        feed.myHeart = @"Y";
                        feed.hCnt += 1;
                    }

                    [self.tableView reloadData];
                }];
            } else if (tag == 20) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedDetailViewController *vc = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                vc.isShowKeypad = TRUE;
                vc.seq = _model.getLoginData.mber_sn;
                vc.feedSeq = feed.cmSeq;
                [self.navigationController pushViewController:vc animated:TRUE];
            } else if (tag == 30) {
                [LogDB insertDb:HM01 m_cod:HM04007 s_cod:HM04007001];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                SnsShareAlertViewController *vc = (SnsShareAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SnsShareAlertViewController"];
                [CommunityNetwork requestShareEventLog:self->_model.getLoginData.mber_sn cmSeq:feed.cmSeq response:^(id response, BOOL isSuccess) {
                    if(isSuccess) {
                        NSLog(@"requestShareEventLog");
                    }
                }];
                vc.touchedCancelButton = ^{
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                };

                vc.touchedSmsButton = ^{
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:^{

                        NSString *message = [NSString stringWithFormat:@"https://insu.greenpio.com:452/HL/Share_sns/sns_contents.asp?seq=%@&cm_seq=%@", self->_model.getLoginData.mber_sn, feed.cmSeq];
                        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
                        messageController.messageComposeDelegate = self;
                        [messageController setBody:message];
                        [self.navigationController.tabBarController presentViewController:messageController animated:YES completion:nil];
                    }];
                };
                vc.touchedKakaoButton = ^{
                    KMTLinkObject *obj = [KMTLinkObject linkObjectWithBuilderBlock:^(KMTLinkBuilder * _Nonnull linkBuilder) {
                        linkBuilder.mobileWebURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://insu.greenpio.com:452/HL/Share_sns/sns_contents.asp?seq=%@&cm_seq=%@", self->_model.getLoginData.mber_sn, feed.cmSeq]];
                    }];
                    KMTTemplate *template = [KMTTextTemplate textTemplateWithText:@"건강관리는 메디케어서비스로" link:obj];
                    [[KLKTalkLinkCenter sharedCenter]sendDefaultWithTemplate:template success:^(NSDictionary<NSString *,NSString *> * _Nullable warningMsg, NSDictionary<NSString *,NSString *> * _Nullable argumentMsg) {
                        NSLog(@"%@", argumentMsg);
                    } failure:^(NSError * _Nonnull error) {
                        NSLog(@"error");
                    }];

                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                };

                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

                [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
            }
        } break;
        case infoCell: {
            FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
            FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
            feedDetailVC.seq = _model.getLoginData.mber_sn;
            feedDetailVC.feedSeq = feed.cmSeq;
            [self.navigationController pushViewController:feedDetailVC animated:TRUE];
        } break;
        default:
            break;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deleteFeed:(FeedModel *)feed {
    [CommunityNetwork requestDeleteFeedItem:nil seq:_model.getLoginData.mber_sn feedSeq:feed.cmSeq  response:^(id response, BOOL isSuccess) {
        if (isSuccess) {
            [self requestFeedList:1 isRefresh:TRUE];
            return;
        }
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"메시지 삭제를 실패 하였습니다."];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 15;
    }
    return 0;
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if([cell isKindOfClass:[SnsShareCell class]] || [cell isKindOfClass:[CommunityInfoCell class]]) {
        return nil;
    }

    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case moreCell: {
            ContentsTextCell *cell = (ContentsTextCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textCell inSection: indexPath.section]];
            cell.contentsLabel.numberOfLines = 0;
            cell.contentsLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            cell.showFullText = TRUE;
            self.isFullTextArray[indexPath.section] = [NSNumber numberWithBool:TRUE];
            [self.tableView reloadData];
            return;
        } break;
        default:
            break;
    }

    FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    feedDetailVC.seq = _model.getLoginData.mber_sn;
    feedDetailVC.feedSeq = feed.cmSeq;
    [self.navigationController pushViewController:feedDetailVC animated:TRUE];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];

    FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];

    switch(indexPath.row) {
        case userInfoCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[CommunityUserInfoCell reuseIdentifier] forIndexPath:indexPath];
            CommunityUserInfoCell *userInfoCell = (CommunityUserInfoCell *)cell;

            BOOL modifyIsHidden = FALSE;
            if (![feed.mberSn isEqualToString:_model.getLoginData.mber_sn] || (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0)) { // mberSn??
                modifyIsHidden = TRUE;
            }

            NSString* profileImage = [feed.profilePic stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];

            [userInfoCell.profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error != nil) {
                        //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
                        //                    make.height.equalTo(0);
                        //                }];
                }
            }];

            userInfoCell.modifiedButton.indexPath = indexPath;
            [userInfoCell.modifyImageView setHidden:modifyIsHidden];
            [userInfoCell.modifiedButton setHidden:modifyIsHidden];

            userInfoCell.profileButton.indexPath = indexPath;
            userInfoCell.nickNameLabel.text = feed.nick;
            userInfoCell.creationTimeLabel.text = [CommunityCommon convertDateToFormat:feed.regDate];
        } break;
        case imageCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ContentsImageCell reuseIdentifier] forIndexPath:indexPath];
            ContentsImageCell *imageCell = (ContentsImageCell *)cell;
            
            NSString* profileImage = [feed.cmImg1 stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
            
//            if(feed.cmImg1 != nil && ![feed.cmImg1 isEqualToString:@""]){
//                NSRange range = [feed.cmImg1 rangeOfString:@"/"options:NSBackwardsSearch];
//                NSString *string = [feed.cmImg1 substringFromIndex:range.location+1];
//
//                NSString* profileImage = [feed.cmImg1 stringByReplacingOccurrencesOfString:string withString:[NSString stringWithFormat:@"tm_%@", string]];
//
//                
//            }
            
            [imageCell.contentsImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error != nil) {
                    //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
                    //                    make.height.equalTo(0);
                    //                }];
                }
            }];
            imageCell.contentsButton.indexPath = indexPath;

            
        } break;
        case mealCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ContentsMealCell reuseIdentifier] forIndexPath:indexPath];
            ContentsMealCell *mealCell = (ContentsMealCell *)cell;
            mealCell.mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@",feed.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
        } break;
        case textCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ContentsTextCell reuseIdentifier] forIndexPath:indexPath];
            ContentsTextCell *textCell = (ContentsTextCell *)cell;
            textCell.contentsLabel.text = feed.cmContent;
            textCell.showFullText = FALSE;
            textCell.contentsLabel.numberOfLines = 2;
            if ([self.isFullTextArray[indexPath.section] boolValue] == TRUE) {
                textCell.contentsLabel.numberOfLines = 0;
            }
            CGFloat unitHeight = [@"A" heightForWidth:textCell.bounds.size.width usingFont:textCell.contentsLabel.font];
            CGFloat blockHeight = [feed.cmContent heightForWidth:textCell.bounds.size.width usingFont:textCell.contentsLabel.font];
            NSInteger numberOfLines = ceilf(blockHeight / unitHeight);


            self.numberOfLines = numberOfLines;
        } break;
        case moreCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ContentsMoreCell reuseIdentifier] forIndexPath:indexPath];
            ContentsMoreCell *moreCell = (ContentsMoreCell *)cell;
        } break;
        case tagCell: {
            ContentsTagCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContentsTagCell"];
            self.tagListCell = cell;

            NSMutableArray *tagList = [NSMutableArray array];
            for (NSString *tag in feed.cmTag) {
                TagModel *tagModel = [[TagModel alloc]init];
                tagModel.tag = tag;
                tagModel.seq = 0;
                [tagList addObject: tagModel];
            }
            [cell.tagListView removeAllTags];
            for(TagModel* tag in tagList) {
                TagView* tv = [cell.tagListView addTag:tag.tag];
                [tv setTextColor:COLOR_MAIN_ORANGE];
                [tv setTextFont:FONT_NOTO_BOLD(13)];
                tv.onTap = ^(TagView* tv){
                    NSString* keyword = [tv titleForState:UIControlStateNormal];
                    NSLog(@"%@",keyword);
//                    [self performSegueWithIdentifier:@"resultTagSegue" sender:keyword];//[keyword substringFromIndex:1]];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
                    CommunitySearchResultTagsViewController *search = (CommunitySearchResultTagsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"CommunitySearchResultTagsViewController"];
                    search.searchKeyword = keyword;
                    [self.navigationController pushViewController:search animated:TRUE];
                };
            }
            return cell;
        } break;
        case snsShareCell: {
            cell = (SnsShareCell *)[tableView dequeueReusableCellWithIdentifier:[SnsShareCell reuseIdentifier] forIndexPath:indexPath];
            SnsShareCell *shareCell = (SnsShareCell *)cell;
            [shareCell.likeButton setSelected:FALSE];
            if ([feed.myHeart isEqualToString:@"Y"]) {
                [shareCell.likeButton setSelected:TRUE];
            }
            shareCell.likeButton.indexPath = indexPath;
            shareCell.commentButton.indexPath = indexPath;
            shareCell.snsShareButton.indexPath = indexPath;
        } break;
        case infoCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[CommunityInfoCell reuseIdentifier] forIndexPath:indexPath];
            CommunityInfoCell *infoCell = (CommunityInfoCell *)cell;
//            if (feed.rCnt > 0) {
//                [infoCell.CommentArrowImageView setHidden:FALSE];
//            }
//            infoCell.likeCountLabel.text = [NSString stringWithFormat:@"%ld개", feed.hCnt];
//            infoCell.commentCountLabel.text = [NSString stringWithFormat:@"%ld개", feed.rCnt];
            infoCell.CommentButton.indexPath = indexPath;
        } break;
        default: break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];

    switch(indexPath.row) {
        case userInfoCell: {
            CommunityUserInfoCell *userInfoCell = (CommunityUserInfoCell *)cell;

            if ([feed.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
                userInfoCell.profileImageView.layer.borderColor = RGB(243, 163, 41).CGColor;
            } else {
                userInfoCell.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
            }
        } break;
        case imageCell: {
            ContentsImageCell *imageCell = (ContentsImageCell *)cell;
        } break;
        case mealCell: {
            ContentsMealCell *mealCell = (ContentsMealCell *)cell;
        } break;
        case textCell: {
            ContentsTextCell *textCell = (ContentsTextCell *)cell;
        } break;
        case moreCell: {
            ContentsMoreCell *moreCell = (ContentsMoreCell *)cell;

        } break;
        case snsShareCell: {
            SnsShareCell *shareCell = (SnsShareCell *)cell;
        } break;
        case infoCell: {
            CommunityInfoCell *infoCell = (CommunityInfoCell *)cell;
            [infoCell.CommentArrowImageView setHidden:TRUE];
            if (feed.rCnt > 0) {
                [infoCell.CommentArrowImageView setHidden:FALSE];
            }
            infoCell.likeCountLabel.text = [NSString stringWithFormat:@"%ld개", feed.hCnt];
            infoCell.commentCountLabel.text = [NSString stringWithFormat:@"%ld개", feed.rCnt];
        } break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedModel *feed = [self.feedModels objectAtIndex:indexPath.section];

    if (indexPath.row == userInfoCell) {
        return 90;
    } else if (indexPath.row == mealCell) {
        if (feed.cmMeal.length == 0) return 0;
    } else if (indexPath.row == moreCell) {
        ContentsTextCell *cell = (ContentsTextCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textCell inSection: indexPath.section]];
        // 본문이 두줄 넘는지 체크
        if (self.numberOfLines < 3 || [self.isFullTextArray[indexPath.section]boolValue] == TRUE) {
            return 0;
        }
        return 50;
    } else if (indexPath.row == snsShareCell) {
        return 55;
    } else if (indexPath.row == imageCell) {
        if (feed.cmImg1 == nil || [feed.cmImg1 isEqual:@""]) {
            return 0;
        }
    } else if (indexPath.row == tagCell) {
        if (feed.cmTag == nil || feed.cmTag.count == 0) {
            return 0;
        }
    }
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    FeedModel *feed = [_feedModels objectAtIndex:section];
//    if(feed.cmImg1) {
//
//    }
    return 8;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.feedModels.count;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.tableView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            if (!self.isNewDataLoading) {
                self.page += 1;
                [self requestFeedList:self.page isRefresh:FALSE];
            }
        }
    }
    if (!decelerate) {
        self.scrollViewDidEndDragging();
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.scrollViewDidEndDragging();
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.scrollViewWillBeginDragging();
}

@end
