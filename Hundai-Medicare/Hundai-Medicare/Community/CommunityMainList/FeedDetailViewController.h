//
//  FeedDetailViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 11/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "FeedModel.h"
#import "CommentModel.h"
#import "GrowingTextView-Swift.h"

@class ContentsTagCell;

@interface FeedDetailViewController : BaseViewController

@property (nonatomic, assign) BOOL isShowKeypad;
@property (nonatomic, assign) BOOL isNewDataLoading;

@property (nonatomic, strong) NSString *seq;
@property (nonatomic, strong) NSString *oseq;
@property (nonatomic, strong) NSString *feedSeq;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) FeedModel *feed;
@property (nonatomic, strong) NSMutableArray<CommentModel *> *comments;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *growBgView;
@property (weak, nonatomic) IBOutlet UIButton *commentSendButton;
@property (weak, nonatomic) IBOutlet GrowingTextView *growTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *growViewBottomConstraint;
@property (nonatomic, strong)ContentsTagCell *tagListCell;

@end
