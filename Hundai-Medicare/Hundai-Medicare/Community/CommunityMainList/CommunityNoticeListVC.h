//
//  CommunityNoticeListVC.h
//  Hundai-Medicare
//
//  Created by Paul P on 04/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FeedModel.h"
#import "BaseViewController.h"
#import "CommunityMainVC.h"
@import TagListView_ObjC;

@class ContentsTagCell;

@interface CommunityNoticeListVC : BaseViewController

@property (copy)void (^scrollViewWillBeginDragging)(void);
@property (copy)void (^scrollViewDidEndDragging)(void);
@property (nonatomic, assign)BOOL isNewDataLoading;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger page;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSMutableArray<FeedModel *> *feedModels;
@property (nonatomic, assign)NSInteger numberOfLines;
@property (nonatomic, strong)NSMutableArray *isFullTextArray;
@property (nonatomic, strong)ContentsTagCell *tagListCell;
@end
