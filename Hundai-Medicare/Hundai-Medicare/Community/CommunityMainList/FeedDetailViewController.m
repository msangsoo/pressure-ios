    //
    //  CommunityTotalListVC.m
    //  Hundai-Medicare
    //
    //  Created by Paul P on 04/03/2019.
    //  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
    //

#import "FeedDetailViewController.h"
#import "CommunityUserInfoCell.h"
#import "ContentsImageCell.h"
#import "ContentsMealCell.h"
#import "ContentsTextCell.h"
#import "ContentsMoreCell.h"
#import "SnsShareCell.h"
#import "CommunityInfoCell.h"
#import "CommentCell.h"
#import "CommentTextCell.h"
#import "CommunityCommon.h"
#import "CommunityNetwork.h"
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileViewController.h"
#import "FeedModifyAlertViewController.h"
#import "CommunityWriteViewController.h"
#import "FeedDeleteAlertViewController.h"
#import "SnsShareAlertViewController.h"
#import "PhotoDetailViewController.h"
#import "AXPhotoViewer-Swift.h"
#import "FeedDetailViewController.h"
#import "CommentDeleteAlertViewController.h"
#import <KakaoLink/KakaoLink.h>
#import <KakaoMessageTemplate/KakaoMessageTemplate.h>
#import <MessageUI/MessageUI.h>
#import "ContentsTagCell.h"
#import "TagModel.h"
#import "CommunitySearchResultTagsViewController.h"

@import IQKeyboardManager;

@interface FeedDetailViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, GrowingTextViewDelegate, UITextViewDelegate, MFMessageComposeViewControllerDelegate>

@property(nonatomic, strong)UIRefreshControl *refreshControl;

@end

@implementation FeedDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    self.title = @"댓글";

    _pageSize = 0;
    _page = 1;
    self.navigationController.navigationBar.tintColor = COLOR_MAIN_DARK;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName:FONT_NOTO_BOLD(18)};

    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:TRUE];

    self.comments = [NSMutableArray array];
    [self setupTableView];
    [self setupRefreshView];
    [self setupGrowTextView];

    [self requestGetDetailFeed:self.seq feedSeq:self.feedSeq];
}
- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (self.isShowKeypad == TRUE) {
        [self.growTextView becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:true];

    [[NSNotificationCenter defaultCenter]postNotificationName:@"UpdateFeedModel" object:[self.feed copy]];
}

- (void)setupNavigationBar {
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName:FONT_NOTO_BOLD(18)};
    self.title = @"게시글";
}

- (void)setupTableView {
    self.tableView.estimatedRowHeight = 45;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)setupRefreshView {
    _refreshControl = [[UIRefreshControl alloc]init];
    [_refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
}

- (void)setupGrowTextView {

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureHandler)];
    [self.view addGestureRecognizer:tapGesture];


//    if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//        var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
//        if #available(iOS 11, *) {
//            if keyboardHeight > 0 {
//                keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
//            }
//        }
//        textViewBottomConstraint.constant = keyboardHeight + 8
//        view.layoutIfNeeded()

}

- (void)tapGestureHandler{
    [self.view endEditing:TRUE];
}

- (void)keyboardWillChangeFrame:(NSNotification *)noti {
    NSValue *notiValue = (NSValue *)noti.userInfo[@"UIKeyboardFrameEndUserInfoKey"];
    CGRect endFrame = notiValue.CGRectValue;
    double keyboardHeight = [UIScreen mainScreen].bounds.size.height - endFrame.origin.y;
    if (@available(iOS 11, *)) {
        if (keyboardHeight > 0) {
            keyboardHeight = keyboardHeight - self.view.safeAreaInsets.bottom;
        }
    }
    self.growViewBottomConstraint.constant = keyboardHeight + 8;
    [self.view layoutIfNeeded];
}

#pragma mark - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
}


#pragma mark -------- CustomFunction --------
- (void)requestGetDetailFeed:(NSString *)seq feedSeq:(NSString *)feedSeq {
    NSDictionary *header = [NSDictionary dictionary];

    self.isNewDataLoading = TRUE;
    [CommunityNetwork requestGetDetailFeed:header seq:seq feedSeq:feedSeq response:^(id response, BOOL isSuccess) {
        NSLog(@"%@", response);
        self.isNewDataLoading = FALSE;
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }

        if(isSuccess == FALSE) {
            return;
        }

        self.feed = [[FeedModel alloc]initWithJson:response];
        self.feed.cmSeq = [NSString stringWithString:self.feedSeq];

//        NSArray *comments = (NSArray *)response[@"DATA"];

        if (self.comments.count > 0) {
            self.page = 1;
            [self.comments removeAllObjects];
        }

        [self requestGetComments:self.seq feedSeq:self.feedSeq page:self.page];

//        for (NSDictionary *comment in comments) {
//            [self.comments addObject:[[CommentModel alloc]initWithJson:comment]];
//        }
        [self resetGrowTextField];
//        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
//        });Y
    }];
}

#pragma mark UIRefresh
- (void)handleRefresh:(UIRefreshControl *)sender {
    [self requestGetDetailFeed:self.seq feedSeq:self.feedSeq];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (IBAction)selectedButtonAtCommunityTableView:(UIButton *)sender
{
    NSIndexPath *indexPath = sender.indexPath;
    NSInteger tag = sender.tag;

    if (indexPath.section == 0) {
//        CommentModel *comment= [self.comments objectAtIndex:indexPath.section];
        switch(indexPath.row) {
            case userInfoCell: {
                if(tag == 30) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                    ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                    vc.memSeq = _model.getLoginData.mber_sn;
                    vc.seq = self.feed.oSeq;
                    [self.navigationController pushViewController:vc animated:TRUE];
                    break;
                }
            } break;
            case snsShareCell: {
                if (tag == 10) {
                    [CommunityNetwork requestRegistLike:@{} seq:self.seq feedSeq:self.feedSeq response:^(id responseObj, BOOL isSuccess) {
                        if ([self.feed.myHeart isEqualToString:@"Y"]) {
                            self.feed.myHeart = @"N";
                            self.feed.hCnt -= 1;
                        } else {
                            self.feed.myHeart = @"Y";
                            self.feed.hCnt += 1;
                        }
                        [self.tableView reloadData];
                    }];
                }else if (tag == 30) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                    SnsShareAlertViewController *vc = (SnsShareAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SnsShareAlertViewController"];

                    [CommunityNetwork requestShareEventLog:self->_model.getLoginData.mber_sn cmSeq:self.feedSeq response:^(id response, BOOL isSuccess) {
                        if(isSuccess) {
                            NSLog(@"requestShareEventLog");
                        }
                    }];

                    vc.touchedCancelButton = ^{
                        [self dismissViewControllerAnimated:TRUE completion:nil];
                    };

                    vc.touchedSmsButton = ^{
                        [self dismissViewControllerAnimated:TRUE completion:^{

                            NSString *message = [NSString stringWithFormat:@"https://insu.greenpio.com:452/HL/Share_sns/sns_contents.asp?seq=%@&cm_seq=%@", self->_model.getLoginData.mber_sn, self.feedSeq];
                            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
                            messageController.messageComposeDelegate = self;
                            [messageController setBody:message];
                            [self presentViewController:messageController animated:YES completion:nil];
                        }];
                    };
                    vc.touchedKakaoButton = ^{
                        KMTLinkObject *obj = [KMTLinkObject linkObjectWithBuilderBlock:^(KMTLinkBuilder * _Nonnull linkBuilder) {
                            linkBuilder.mobileWebURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://insu.greenpio.com:452/HL/Share_sns/sns_contents.asp?seq=%@&cm_seq=%@", _model.getLoginData.mber_sn, self.feedSeq]];
                        }];
                        KMTTemplate *template = [KMTTextTemplate textTemplateWithText:@"건강관리는 메디케어서비스로" link:obj];
                        [[KLKTalkLinkCenter sharedCenter]sendDefaultWithTemplate:template success:^(NSDictionary<NSString *,NSString *> * _Nullable warningMsg, NSDictionary<NSString *,NSString *> * _Nullable argumentMsg) {
                            NSLog(@"%@", argumentMsg);
                        } failure:^(NSError * _Nonnull error) {
                            NSLog(@"error");
                        }];

                        [self dismissViewControllerAnimated:TRUE completion:nil];
                    };

                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

                    [self presentViewController:vc animated:TRUE completion:nil];
                }
            } break;
            default:
            break;
        }
    } else {
        CommentModel *comment= [self.comments objectAtIndex:indexPath.section - 1];
        switch (indexPath.row) {
            case commentCell: {
                if(tag == 30) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                    ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                    vc.memSeq = _model.getLoginData.mber_sn;
                    vc.seq = comment.oSeq;
                    [self.navigationController pushViewController:vc animated:TRUE];
                    break;
                } else {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];

                    CommentDeleteAlertViewController * vc = (CommentDeleteAlertViewController *)[storyboard instantiateViewControllerWithIdentifier: @"CommentDeleteAlertViewController"];
                    CommentModel *comment = [self.comments objectAtIndex:sender.indexPath.section-1];

                    vc.commentCancel = ^{
                        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                    };
    //                vc.commentDelete = ^{
    //                    [self deleteComment:comment];
    //                };
                    vc.commentDelete = ^{
                        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                        [self deleteComment:comment reloadIndexPath:sender.indexPath];
                    };

                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

                    [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
                }
            } break;
            case commentTextCell: {

            } break;
            default:
            break;
        }
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deleteComment:(CommentModel *)comment reloadIndexPath:(NSIndexPath *)indexPath {
//    CommentModel *comment = [self.comments objectAtIndex:sender.indexPath.section];
    
    [LogDB insertDb:HM01 m_cod:HM04008 s_cod:HM04008001];
    
    NSDictionary *header = [NSDictionary dictionary];
    [CommunityNetwork requestDeleteComment:header seq:self->_model.getLoginData.mber_sn commentSeq:comment.ccSeq response:^(id response, BOOL isSuccess) {
        if (isSuccess == FALSE) {
            NSError *error = (NSError *)response;
            NSLog(@"%@", error.localizedDescription);

            NSString *errorMsg;
            if (error.code == 44444) {
                errorMsg = @"이미 등록된 댓글 입니다.";
            } else if (error.code == 6666) {
                errorMsg = @"회원이 존재 하지 않습니다.";
            } else if (error.code == 7777) {
                errorMsg = @"사용중지 회원입니다.";
            }
            [Utils basicAlertView:self withTitle:@"알림" withMsg:errorMsg];

            return;
        }
            // 성공 알럿
        if (self.feed.rCnt > 0) {
            self.feed.rCnt -= 1;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.growTextView.text = @"";
            self.growTextView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"댓글을 입력해주세요." attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
            self.growTextView.textColor = RGB(143, 144, 158);
            self.growTextView.font = FONT_NOTO_REGULAR(13);

            [self requestGetDetailFeed:self.seq feedSeq:self.feedSeq];
        });
    }];
}

-  (void)resetGrowTextField {
    self.growTextView.text = @"";
    self.growTextView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"댓글을 입력해주세요." attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
    self.growTextView.textColor = RGB(143, 144, 158);
    self.growTextView.font = FONT_NOTO_REGULAR(13);
}

- (IBAction)touchedAddComment:(UIButton *)sender {
    [self.view endEditing:TRUE];

    if (self.growTextView.text.length == 0) {
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"댓글을 입력해주세요."];
        return;
    }

    [LogDB insertDb:HM01 m_cod:HM04006 s_cod:HM04006001];
    
    NSDictionary *header = [NSDictionary dictionary];
    NSLog(@"%@", [NSString stringWithFormat:@"%@",self.oseq]);
    if(![self.growTextView.text containsString:@"@"]){
        self.oseq = @"";
    }
    [CommunityNetwork requestAddComment:header seq:_model.getLoginData.mber_sn feedSeq:self.feedSeq relationMberSn:self.oseq conent:self.growTextView.text response:^(id response, BOOL isSuccess) {
//        0000 : 입력성공 4444 : 이미등록된 댓글 6666 : 회원이 존재하지 않음 7777 : 사용중지회원 9999 : 기타오류
        if (isSuccess == FALSE) {
            NSError *error = (NSError *)response;
            NSLog(@"%@", error.localizedDescription);

            NSString *errorMsg;
            if (error.code == 44444) {
                errorMsg = @"이미 등록된 댓글 입니다.";
            } else if (error.code == 6666) {
                errorMsg = @"회원이 존재 하지 않습니다.";
            } else if (error.code == 7777) {
                errorMsg = @"사용중지 회원입니다.";
            }
            [Utils basicAlertView:self withTitle:@"알림" withMsg:errorMsg];
            [self resetGrowTextField];
            return;
        }

        self.feed.rCnt += 1;
        // 성공 알럿
        [self requestGetDetailFeed:self.seq feedSeq:self.feedSeq];
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 15;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 15;
    }
    return 0;
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(indexPath.section == 0 || [cell isKindOfClass:[SnsShareCell class]] || [cell isKindOfClass:[CommunityInfoCell class]]) {
        return nil;
    }

    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    if (indexPath.section == 0) {
        switch(indexPath.row) {
            case userInfoCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[CommunityUserInfoCell reuseIdentifier] forIndexPath:indexPath];
                CommunityUserInfoCell *userInfoCell = (CommunityUserInfoCell *)cell;

                if ([self.feed.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
                    userInfoCell.profileImageView.layer.borderColor = RGB(243, 163, 41).CGColor;
                }
                else
                {
                    userInfoCell.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
                }

                NSString* profileImage = [self.feed.profilePic stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];

                [userInfoCell.profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error != nil) {
                            //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
                            //                    make.height.equalTo(0);
                            //                }];
                    }
                }];

                userInfoCell.profileButton.indexPath = indexPath;
                userInfoCell.nickNameLabel.text = self.feed.nick;
                userInfoCell.creationTimeLabel.text = [CommunityCommon convertDateToFormat:self.feed.regDate];
            } break;
            case imageCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[ContentsImageCell reuseIdentifier] forIndexPath:indexPath];
//                ContentsImageCell *imageCell = (ContentsImageCell *)cell;
//
//                NSString* profileImage = [self.feed.cmImg1 stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
//
//                [imageCell.contentsImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    if (error != nil) {
//                            //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
//                            //                    make.height.equalTo(0);
//                            //                }];
//                    }
//                }];
//                imageCell.contentsButton.indexPath = indexPath;
            } break;
            case mealCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[ContentsMealCell reuseIdentifier] forIndexPath:indexPath];
                if (self.feed.cmMeal != nil && self.feed.cmMeal.length > 0) {
                    ContentsMealCell *mealCell = (ContentsMealCell *)cell;
                    mealCell.mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", self.feed.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
                }
            } break;
            case textCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[ContentsTextCell reuseIdentifier] forIndexPath:indexPath];
                ContentsTextCell *textCell = (ContentsTextCell *)cell;
                textCell.contentsLabel.text = self.feed.cmContent;
            } break;
            case moreCell:
                cell = [tableView dequeueReusableCellWithIdentifier:[ContentsMoreCell reuseIdentifier] forIndexPath:indexPath]; break;
            case tagCell: {
                ContentsTagCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContentsTagCell"];
                self.tagListCell = cell;

                NSMutableArray *tagList = [NSMutableArray array];
                for (NSString *tag in self.feed.cmTag) {
                    TagModel *tagModel = [[TagModel alloc]init];
                    tagModel.tag = tag;
                    tagModel.seq = 0;
                    [tagList addObject: tagModel];
                }
                [cell.tagListView removeAllTags];
                for(TagModel* tag in tagList) {
                    TagView* tv = [cell.tagListView addTag:tag.tag];
                    [tv setTextColor:COLOR_MAIN_ORANGE];
                    [tv setTextFont:FONT_NOTO_BOLD(13)];
//                    tv.onTap = ^(TagView* tv){
//                        NSString* keyword = [tv titleForState:UIControlStateNormal];
//                        NSLog(@"%@",keyword);
//                            //                    [self performSegueWithIdentifier:@"resultTagSegue" sender:keyword];//[keyword substringFromIndex:1]];
//                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
//                        CommunitySearchResultTagsViewController *search = (CommunitySearchResultTagsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"CommunitySearchResultTagsViewController"];
//                        search.searchKeyword = keyword;
//                        [self.navigationController pushViewController:search animated:TRUE];
//                    };
                }
                return cell;
            } break;
            case snsShareCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[SnsShareCell reuseIdentifier] forIndexPath:indexPath];
                SnsShareCell *shareCell = (SnsShareCell *)cell;
                [shareCell.likeButton setSelected:FALSE];
                if ([self.feed.myHeart isEqualToString:@"Y"]) {
                    [shareCell.likeButton setSelected:TRUE];
                }
                shareCell.likeButton.indexPath = indexPath;
                shareCell.commentButton.indexPath = indexPath;
                shareCell.snsShareButton.indexPath = indexPath;
            } break;
            case infoCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[CommunityInfoCell reuseIdentifier] forIndexPath:indexPath];
                CommunityInfoCell *infoCell = (CommunityInfoCell *)cell;
                if (self.feed.rCnt > 0) {
                    [infoCell.CommentArrowImageView setHidden:FALSE];
                }
                infoCell.likeCountLabel.text = [NSString stringWithFormat:@"%ld개", self.feed.hCnt];
                infoCell.commentCountLabel.text = [NSString stringWithFormat:@"%ld개", self.feed.rCnt];
            } break;
        }
    } else {
        CommentModel *comment = [self.comments objectAtIndex:indexPath.section - 1];
        switch (indexPath.row) {
            case commentCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[CommentCell reuseIdentifier] forIndexPath:indexPath];
                CommentCell *commentCell = (CommentCell *)cell;
                if ([comment.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
                    commentCell.profileImageView.layer.borderColor = RGB(243, 163, 41).CGColor;
                }
                else
                {
                    commentCell.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
                }
                NSString* profileImage = [comment.profilePic stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
                [commentCell.profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] options:0];

                commentCell.touchedNickname = ^(NSString *nickName) {
                    if(self->_model.getLoginData.mber_sn == comment.oSeq) {
                        return;
                    }
                    NSLog(@"%@", nickName);
                    NSLog(@"%@", comment.oSeq);

                    NSMutableAttributedString *commentAttr = [[NSMutableAttributedString alloc]init];

                    NSAttributedString *nickAttr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"@%@", nickName] attributes:@{NSFontAttributeName:FONT_NOTO_BOLD(15), NSForegroundColorAttributeName:RGB(143, 144, 158)}];

                    NSAttributedString *trailAttr = [[NSAttributedString alloc]initWithString:@" " attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
                    [commentAttr appendAttributedString:nickAttr];
                    [commentAttr appendAttributedString:trailAttr];
                    self.growTextView.attributedText = commentAttr;
                    self.oseq = comment.oSeq;
                    self.growTextView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@""];
                    [self.growTextView becomeFirstResponder];
                };

                commentCell.touchedCommentDelete = ^(){
                    //코멘트 삭제
                };

                [commentCell.deleteButton setHidden:TRUE];
                NSString *seq = comment.oSeq;
                if (seq == nil) {
                    seq = comment.seq;
                }
                if ([seq isEqualToString:_model.getLoginData.mber_sn]) {
                    [commentCell.deleteButton setHidden:FALSE];
                }
                commentCell.nickNameButton.indexPath = indexPath;
                commentCell.deleteButton.indexPath = indexPath;
                commentCell.profileButton.indexPath = indexPath;
                commentCell.nickNameLabel.text = comment.nick;
                commentCell.creationTimeLabel.text = [CommunityCommon convertDateToFormat:comment.regDate];
            } break;
            case commentTextCell: {
                cell = [tableView dequeueReusableCellWithIdentifier:[CommentTextCell reuseIdentifier] forIndexPath:indexPath];
                CommentTextCell *commentTextCell = (CommentTextCell *)cell;

                if ((comment.ccContent == nil || [comment.ccContent isEqualToString:@""])  && (comment.cmContent == nil || [comment.cmContent isEqualToString:@""])) {
                    break;
                }

                NSString *totalComment = comment.ccContent;
                if (totalComment == nil || [totalComment isEqualToString:@""]) {
                    totalComment = comment.cmContent;
                }
                NSMutableAttributedString *commentAttr = [[NSMutableAttributedString alloc]init];
                if ([totalComment hasPrefix:@"@"] == TRUE) {
                    NSRange rangeOfSpace = [totalComment rangeOfString:@" "];
                    NSString *first = rangeOfSpace.location == NSNotFound ? totalComment : [totalComment substringToIndex:rangeOfSpace.location];
                    NSString *last = rangeOfSpace.location == NSNotFound ? nil :[totalComment substringFromIndex:rangeOfSpace.location + 1];
                    NSAttributedString *firstAttr = [[NSAttributedString alloc]initWithString:[first stringByReplacingOccurrencesOfString:@"@" withString:@""] attributes:@{NSFontAttributeName:FONT_NOTO_BOLD(15), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//                    commentTextCell.nickName = firstAttr;
                    NSAttributedString *lastAttr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", last] attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
                    [commentAttr appendAttributedString:firstAttr];
                    [commentAttr appendAttributedString:lastAttr];

                    commentTextCell.nickNameRange = [commentAttr.string rangeOfString:firstAttr.string];
                } else {
                    [commentAttr appendAttributedString:[[NSAttributedString alloc]initWithString:totalComment attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}]];
                }

                commentTextCell.commentTextLabel.attributedText = commentAttr; //comment.cmContent;
//                commentTextCell.touchedNickname = ^(NSString *nickName) {
//                    NSLog(@"%@", nickName);
//
//                    NSMutableAttributedString *commentAttr = [[NSMutableAttributedString alloc]init];
//
//                    NSAttributedString *nickAttr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"#%@", nickName] attributes:@{NSFontAttributeName:FONT_NOTO_BOLD(18), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//
//                    NSAttributedString *trailAttr = [[NSAttributedString alloc]initWithString:@" " attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//                    [commentAttr appendAttributedString:nickAttr];
//                    [commentAttr appendAttributedString:trailAttr];
//                    self.growTextView.attributedText = commentAttr;
//                    [self.growTextView becomeFirstResponder];
//                };
            } break;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch(indexPath.row) {
            case userInfoCell: {
//                CommunityUserInfoCell *userInfoCell = (CommunityUserInfoCell *)cell;
//
//                if ([self.feed.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
//                    userInfoCell.profileImageView.layer.borderColor = [UIColor colorWithRed:243/255.0 green:163/255.0 blue:41/255.0 alpha:1.0].CGColor;
//                }
//
//                userInfoCell.nickNameLabel.text = self.feed.nick;
//                userInfoCell.creationTimeLabel.text = [CommunityCommon convertDateToFormat:self.feed.regDate];
            } break;
            case imageCell: {
//                ContentsImageCell *imageCell = (ContentsImageCell *)cell;
//                [imageCell.contentsImageView sd_setImageWithURL:[NSURL URLWithString:self.feed.cmImg1] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    if (error != nil) {
//                            //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
//                            //                    make.height.equalTo(0);
//                            //                }];
//                    }
//                }];
//                imageCell.contentsButton.indexPath = indexPath;
            } break;
            case mealCell: {
//                ContentsMealCell *mealCell = (ContentsMealCell *)cell;
//                mealCell.mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", self.feed.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(11), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
            } break;
            case textCell: {
//                ContentsTextCell *textCell = (ContentsTextCell *)cell;
//                textCell.contentsLabel.text = self.feed.cmContent;
            } break;
            case moreCell: {
                ContentsMoreCell *moreCell = (ContentsMoreCell *)cell;
            } break;
            case snsShareCell: {
//                SnsShareCell *shareCell = (SnsShareCell *)cell;
//                [shareCell.likeButton setSelected:FALSE];
//                if ([self.feed.myHeart isEqualToString:@"Y"]) {
//                    [shareCell.likeButton setSelected:TRUE];
//                }
//                shareCell.likeButton.indexPath = indexPath;
//                shareCell.commentButton.indexPath = indexPath;
//                shareCell.snsShareButton.indexPath = indexPath;
            } break;
            case infoCell: {
//                CommunityInfoCell *infoCell = (CommunityInfoCell *)cell;
//                if (self.feed.rCnt > 0) {
//                    [infoCell.CommentArrowImageView setHidden:FALSE];
//                }
//                infoCell.likeCountLabel.text = [NSString stringWithFormat:@"%ld개", self.feed.hCnt];
//                infoCell.commentCountLabel.text = [NSString stringWithFormat:@"%ld개", self.feed.rCnt];
            } break;
            default:
            break;
        }
    } else {
        CommentModel *comment = [self.comments objectAtIndex:indexPath.section - 1];
        switch(indexPath.row) {
            case commentCell: {
//                CommentCell *commentCell = (CommentCell *)cell;
//                if ([self.feed.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
//                    commentCell.profileImageView.layer.borderColor = [UIColor colorWithRed:243/255.0 green:163/255.0 blue:41/255.0 alpha:1.0].CGColor;
//                }
//                [commentCell.profileImageView sd_setImageWithURL:[NSURL URLWithString:comment.profilePic] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    if (error != nil) {
//                            //                [cell mas_makeConstraints:^(MASConstraintMaker *make) {
//                            //                    make.height.equalTo(0);
//                            //                }];
//                    }
//                }];
//
//                [commentCell.deleteButton setHidden:TRUE];
//                NSString *seq = self.feed.mberSn;
//                if (seq == nil) {
//                    seq = self.feed.oSeq;
//                }
//                if ([comment.seq isEqualToString:seq]) {
//                    [commentCell.deleteButton setHidden:FALSE];
//                }
//                commentCell.deleteButton.indexPath = indexPath;
//                commentCell.nickNameLabel.text = comment.nick;
//                commentCell.creationTimeLabel.text = [CommunityCommon convertDateToFormat:comment.regDate];
            } break;
            case commentTextCell: {
//                CommentTextCell *commentTextCell = (CommentTextCell *)cell;

//                commentTextCell.commentTextLabel.text = @"안녕하세요.안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요";//comment.cmContent;

//                NSString *totalComment = @"#jeongho.pi 안녕하세요.안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요안녕하세요";
//                NSMutableAttributedString *commentAttr = [[NSMutableAttributedString alloc]init];
//                if ([totalComment hasPrefix:@"#"] == TRUE) {
//                    NSRange rangeOfSpace = [totalComment rangeOfString:@" "];
//                    NSString *first = rangeOfSpace.location == NSNotFound ? totalComment : [totalComment substringToIndex:rangeOfSpace.location];
//                    NSString *last = rangeOfSpace.location == NSNotFound ? nil :[totalComment substringFromIndex:rangeOfSpace.location + 1];
//                    NSAttributedString *firstAttr = [[NSAttributedString alloc]initWithString:[first stringByReplacingOccurrencesOfString:@"#" withString:@""] attributes:@{NSFontAttributeName:FONT_NOTO_BOLD(18), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//                    commentTextCell.nickName = firstAttr;
//                    NSAttributedString *lastAttr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", last] attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//                    [commentAttr appendAttributedString:firstAttr];
//                    [commentAttr appendAttributedString:lastAttr];
//
//                    commentTextCell.nickNameRange = [commentAttr.string rangeOfString:firstAttr.string];
//                } else {
//                    [commentAttr appendAttributedString:[[NSAttributedString alloc]initWithString:totalComment attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}]];
//                }
//
//                commentTextCell.commentTextLabel.attributedText = commentAttr; //comment.cmContent;
//                commentTextCell.touchedNickname = ^(NSString *nickName) {
//                    NSLog(@"%@", nickName);
//
//                    NSMutableAttributedString *commentAttr = [[NSMutableAttributedString alloc]init];
//
//                    NSAttributedString *nickAttr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"#%@", nickName] attributes:@{NSFontAttributeName:FONT_NOTO_BOLD(18), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//
//                    NSAttributedString *trailAttr = [[NSAttributedString alloc]initWithString:@" " attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(14), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
//                    [commentAttr appendAttributedString:nickAttr];
//                    [commentAttr appendAttributedString:trailAttr];
//                    self.growTextView.attributedText = commentAttr;
//                    [self.growTextView becomeFirstResponder];
//                };
//                [self.tableView layoutIfNeeded];
            } break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == userInfoCell) {
            return 90;
        } else if (indexPath.row == mealCell) {
            if (self.feed.cmMeal.length == 0) {
                return 0;
            }
        } else if (indexPath.row == moreCell) {
            return 0;
        } else if (indexPath.row == snsShareCell) {
            return 0;
        } else if (indexPath.row == infoCell) {
            return 0;
        } else if (indexPath.row == tagCell) {
            if (self.feed.cmTag == nil || self.feed.cmTag.count == 0) {
                return 0;
            }
        } else if (indexPath.row == imageCell) {
//            if (self.feed.cmImg1 == nil || [self.feed.cmImg1 isEqual:@""]) {
                return 0;
//            }
        }
    } else {
        if (indexPath.row == commentCell) {
            return 70;
        }
    }
    return UITableViewAutomaticDimension;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 8;
    }
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.comments.count + 1;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentSize.height < scrollView.frame.size.height) {
        return;
    }

    if (scrollView == self.tableView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height - 120) {
            if (!self.isNewDataLoading) {
                [self requestGetComments:self.seq feedSeq:self.feedSeq page:self.page];
            }
        }
    }
}

- (void)requestGetComments:(NSString *)seq feedSeq:(NSString *)feedSeq page:(NSInteger)page {
    NSDictionary *header = [NSDictionary dictionary];

    [CommunityNetwork requestGetCommentList:header seq:seq feedSeq:feedSeq page:page response:^(id response, BOOL isSuccess) {
        NSLog(@"GetComments : %@", response);
        if(isSuccess == FALSE || ((NSArray *)response).count == 0) {
            return;
        }
        self.page += 1;
        NSArray *comments = (NSArray *)response;
        for (NSDictionary *comment in comments) {
            [self.comments addObject:[[CommentModel alloc]initWithJson:comment]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

- (void)textViewDidChangeHeight:(GrowingTextView *)textView height:(CGFloat)height {
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)textViewDidChange:(UITextView *)textView {
    NSLog(@"textViewDidChange");
    if (textView.text.length == 0) {
        self.growTextView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"댓글을 입력해주세요." attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
        self.growTextView.textColor = RGB(143, 144, 158);
        self.growTextView.font = FONT_NOTO_REGULAR(13);
        
//        [[NSAttributedString alloc]initWithString:@"댓글을 입력해주세요." attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(13), NSForegroundColorAttributeName:RGB(143, 144, 158)}];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    NSLog(@"textViewDidEndEditing");
}

//func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
//    UIView.animate(withDuration: 0.2) {
//        self.view.layoutIfNeeded()
//    }
//}

@end
