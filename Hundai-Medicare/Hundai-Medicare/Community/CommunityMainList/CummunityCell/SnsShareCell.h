//
//  SnsShareCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 06/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+ReuseIdentifier.h"

NS_ASSUME_NONNULL_BEGIN

@interface SnsShareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *snsShareButton;

@end

NS_ASSUME_NONNULL_END
