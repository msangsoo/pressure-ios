//
//  CommentTextCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentTextCell : UITableViewCell

@property (nonatomic, assign) NSRange nickNameRange;
@property (nonatomic, strong) NSAttributedString *nickName;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;


@end

NS_ASSUME_NONNULL_END
