//
//  CommentUserInfoCell.m
//  Hundai-Medicare
//
//  Created by Paul P on 11/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    CGFloat width = self.profileImageView.bounds.size.width;
    self.profileImageView.layer.cornerRadius = width/2.0;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:191/255 green:191/255 blue:191/255 alpha:1.0].CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchedNickNameButton:(UIButton *)sender {
    if (self.nickNameLabel.text == nil || [self.nickNameLabel.text isEqualToString:@""]) {
        return;
    }
    self.touchedNickname(self.nickNameLabel.text);
}


//- (void)touchedNickName:(UITapGestureRecognizer *)tap {
//    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:self.nickName];
//
//    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
//    [textStorage addLayoutManager:layoutManager];
//
//    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.commentTextLabel.frame.size.width + 10, CGFLOAT_MAX)];
//    textContainer.lineFragmentPadding = 0;
//    textContainer.lineBreakMode = NSLineBreakByWordWrapping;
//    [layoutManager addTextContainer:textContainer];
//
//    NSRange glyphRange;
//    [layoutManager characterRangeForGlyphRange:self.nickNameRange actualGlyphRange:&glyphRange];
//
//    CGRect glyphRect = [layoutManager boundingRectForGlyphRange:glyphRange inTextContainer:textContainer];
//    CGPoint touchPoint = [tap locationOfTouch:0 inView:self.commentTextLabel];
//    if( CGRectContainsPoint(glyphRect, touchPoint) ) {
//        self.touchedNickname(self.nickName.string);
//    }
//}


- (IBAction)touchedDeleteButton:(UIButton *)sender {
    self.touchedCommentDelete();
}
@end
