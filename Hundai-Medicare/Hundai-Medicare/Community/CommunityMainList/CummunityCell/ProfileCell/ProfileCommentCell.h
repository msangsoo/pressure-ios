//
//  ProfileCommentCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+ReuseIdentifier.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfileCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mealLabel;
@property (weak, nonatomic) IBOutlet UILabel *conentsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImgeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *thumbImageButton;

@end

NS_ASSUME_NONNULL_END
