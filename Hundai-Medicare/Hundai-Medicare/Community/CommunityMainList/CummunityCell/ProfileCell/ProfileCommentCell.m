//
//  ProfileCommentCell.m
//  Hundai-Medicare
//
//  Created by Paul P on 24/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ProfileCommentCell.h"

@implementation ProfileCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.thumbImgeView.layer.cornerRadius = 10.0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
