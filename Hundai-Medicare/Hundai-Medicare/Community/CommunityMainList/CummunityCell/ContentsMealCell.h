//
//  ContentsMealCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 14/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContentsMealCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mealLabel;

@end

NS_ASSUME_NONNULL_END
;

