//
//  CommunityInfoCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 06/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+ReuseIdentifier.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunityInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

@property (weak, nonatomic) IBOutlet UIImageView *commentImageView;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *CommentButton;
@property (weak, nonatomic) IBOutlet UIImageView *CommentArrowImageView;
@end

NS_ASSUME_NONNULL_END
