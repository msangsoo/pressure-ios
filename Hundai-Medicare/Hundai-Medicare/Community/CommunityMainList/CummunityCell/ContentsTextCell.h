//
//  ContentsTextCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 06/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+ReuseIdentifier.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentsTextCell : UITableViewCell

//+ (NSString *)identifier;
@property (weak, nonatomic) IBOutlet UILabel *contentsLabel;
@property (nonatomic, assign)BOOL showFullText;
@end

NS_ASSUME_NONNULL_END
