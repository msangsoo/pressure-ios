//
//  ContentsTagCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 31/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
@import TagListView_ObjC;

NS_ASSUME_NONNULL_BEGIN

@interface ContentsTagCell : UITableViewCell
@property (weak, nonatomic) IBOutlet TagListView *tagListView;

@end

NS_ASSUME_NONNULL_END
