//
//  CommunityUserInfoCell.h
//  Hundai-Medicare
//
//  Created by Paul P on 06/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+ReuseIdentifier.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    profileButton,
    modifyButton,
} UserInfoButtonType;

@interface CommunityUserInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *creationTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *modifyImageView;
@property (weak, nonatomic) IBOutlet UIButton *modifiedButton;

- (IBAction)touchedPresentProfilePage:(UIButton *)sender;
- (IBAction)touchedFeedModify:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
