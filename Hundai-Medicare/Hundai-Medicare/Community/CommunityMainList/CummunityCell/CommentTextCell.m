//
//  CommentTextCell.m
//  Hundai-Medicare
//
//  Created by Paul P on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommentTextCell.h"

@implementation CommentTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchedNickName:)];
//    [self.commentTextLabel addGestureRecognizer:tapGesture];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)touchedNickName:(UITapGestureRecognizer *)tap {
//    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:self.nickName];
//
//    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
//    [textStorage addLayoutManager:layoutManager];
//
//    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.commentTextLabel.frame.size.width + 10, CGFLOAT_MAX)];
//    textContainer.lineFragmentPadding = 0;
//    textContainer.lineBreakMode = NSLineBreakByWordWrapping;
//    [layoutManager addTextContainer:textContainer];
//
//    NSRange glyphRange;
//    [layoutManager characterRangeForGlyphRange:self.nickNameRange actualGlyphRange:&glyphRange];
//
//    CGRect glyphRect = [layoutManager boundingRectForGlyphRange:glyphRange inTextContainer:textContainer];
//    CGPoint touchPoint = [tap locationOfTouch:0 inView:self.commentTextLabel];
//    if( CGRectContainsPoint(glyphRect, touchPoint) ) {
//        self.touchedNickname(self.nickName.string);
//    }
//}

@end
