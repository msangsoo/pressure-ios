//
//  CommunityUserInfoCell.m
//  Hundai-Medicare
//
//  Created by Paul P on 06/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunityUserInfoCell.h"

@implementation CommunityUserInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    CGFloat width = self.profileImageView.bounds.size.width;
    self.profileImageView.layer.cornerRadius = width/2.0;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:191/255 green:191/255 blue:191/255 alpha:1.0].CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchedPresentProfilePage:(UIButton *)sender {
}

- (IBAction)touchedFeedModify:(UIButton *)sender {
    
}
@end
