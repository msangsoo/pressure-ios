//
//  CommunityMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "CommunityNetwork.h"

@interface CommunityMainVC : BaseViewController

@property(nonatomic, assign)BOOL hasNickName;
@property(nonatomic, strong)NSString *nickName;
@property (weak, nonatomic) IBOutlet UIButton *floatingButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *floatingButtonTopConstraint;

@end
