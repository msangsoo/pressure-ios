//
//  PhotoDetailViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PhotoDetailViewController.h"
#import "AXPhotoViewer-Swift.h"



@interface PhotoDetailViewController ()<AXPhotosViewControllerDelegate, AXPhotoProtocol>

@end

@implementation PhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

//    NSArray *photos = @[[[AXPhoto alloc]initWithAttributedTitle:nil attributedDescription:nil attributedCredit:nil imageData:nil image:nil url:[NSURL URLWithString:self.imageUrl]]];
//
//    AXPhotosDataSource *dataSource = [[AXPhotosDataSource alloc]initWithPhotos:photos];
////    AXPagingConfig *config = [[AXPagingConfig alloc]initWithLoadingViewClass:CustomLoadingView.self];
//    AXPhotosViewController *vc = [[AXPhotosViewController alloc]initWithDataSource:dataSource];
//
//    [self.containerView addSubview:vc.view];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"%@", segue);
}


- (IBAction)touchedCloseButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
