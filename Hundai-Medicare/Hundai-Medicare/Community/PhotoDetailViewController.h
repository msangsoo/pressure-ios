//
//  PhotoDetailViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Hundai-Medicare-Bridging-Header.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoDetailViewController : BaseViewController


- (IBAction)touchedCloseButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong)NSString *imageUrl;

@end

NS_ASSUME_NONNULL_END
