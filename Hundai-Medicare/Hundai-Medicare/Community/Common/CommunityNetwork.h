//
//  CommunityNetwork.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef CommunityNetwork_h
#define CommunityNetwork_h

#import <Foundation/Foundation.h>
#import "CommunityCommon.h"
#import "MANetwork.h"

@class FeedModel;

typedef void (^Response)(id response, BOOL isSuccess);

@interface CommunityNetwork: NSObject


+ (instancetype)shared;

/*
 Input 값
 SEQ: 회원일련번호
 PG_SIZE: 페이지사이즈 (페이지당 리스트 개수)
 PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)
 CM_GUBUN: 커뮤니티 구분 1:커뮤니티 , 2: 공지사항
 CM_TAG: 해쉬태그 값으로 검색
 SWORD: 검색어 (제목 OR 내용 LIKE검색) (항목필수 아님,검색시 만 사용 하셔도 됩니다)


 Output값
 AST_LENGTH : 배열의 원소 개수
 ADDR_MASS : 배열
 TPAGE : 총 페이지수
 CM_SEQ : 커뮤니티 기본키(회원키)
 MBER_SN : 회원키
 NICK : 닉네임(5글자이내)
 PROFILE_PIC : 프로필 사진
 CM_TITLE : 제목
 CM_CONTENT : 글 내용
 REGDATE : 등록일
 CM_TAG : 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 HCNT : 글 당 하트 수
 MYHEART : 내가 좋아요 한 글 여부(Y/N)
 RCNT : 글 당 댓글 수
 MBER_GRAD : 정회원 10, 준회원 20
 */
+ (void)requestGetFeedList:(NSDictionary *)header seq:(NSString *)seq page:(NSInteger)page type:(NSString *)type tag:(NSString *)tag search:(NSString *)word response:(Response)response;

/*
 input값
 SEQ : 회원일련번호(MBER_SN값)
 CM_SEQ : 커뮤니티 기본키

 output값
 OSEQ : 회원일련번호
 RESULT_CODE : 결과코드

 0000 : 삭제성공
 4444 : 삭제 실패(해당글이 없음)
 6666 : 회원이 존재하지 않음
 7777 : 사용중지 회원
 9999 : 기타오류
 */
+ (void)requestDeleteFeedItem:(NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response;

/*
 input 값
 SEQ : 회원일련번호(MBER_SN값)
 NICK : 닉네임(5글자이내)

 output값
 OSEQ : 회원일련번호
 RESULT_CODE : 결과코드

 0000 : 등록가능 닉네임
 4444 : 이미 등록된 닉네임
 6666 : 회원이 존재하지 않음
 7777 : 사용중지 회원
 9999 : 기타오류
 */
//+ (void)requestConfirmDuplecateNickname:(NSDictionary *)header seq:(NSString *)seq nickName:(NSString *)nickName;

/*
 Input 값
 SEQ: 회원일련번호(MBER_SN값)
 NICK: 닉네임(5글자이내)
 DISEASE_OPEN: 질환명 공개여부(Y/N)

 Output 값
 OSEQ: 회원일련번호
 ONICK: 닉네임(5글자이내)
 RESULT_CODE: 결과코드

 0000 : 닉네임 수정 성공
 1000 : 닉네임조회 성공
 4444 : 이미 등록된 닉네임
 5555 : 닉네임 수정 실패
 7777 : 사용중지 회원
 9999 : 기타오류

 NICK 이나 DISEASE_OPEN 중 한 항목만 입력하면 입력한 항목만 수정 되도록 수정했습니다.(둘다 NULL값이면 업데이트 되지않습니다.)
 */
+ (void)requestSetWithConfirmNickname:(NSDictionary *)header seq:(NSString *)seq nickName:(NSString *)nickName diseaseOpen:(NSString *)open response:(Response)response;


/*
 input 값
 SEQ : 회원일련번호(MBER_SN값)
 CM_SEQ : 커뮤니티 기본키

 output값
 CM_SEQ : 암환자 커뮤니티 기본키
 NICK : 닉네임(5글자이내)
 CM_TITLE : 제목
 CM_CONTENT : 글내용
 CM_GUBUN : 말머리 구분 1:암 진단 후, 2: 암치료방법, 3: 암 극복후기, 4:기타
 CM_IMG1 : 이미지1
 CM_IMG2 : 이미지2
 CM_IMG3 : 이미지3
 REGDATE : 등록일
 PROFILE_PIC : 작성자 프로필 사진
 CM_TAG : 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 HCNT : 글 당 추천 수
 HYN : 본인 추천여부(Y/N)
 RCNT : 글 당 댓글 수
 OSEQ : 글쓴이 회원일련번호
 MBER_GRAD : 정회원 10, 준회원 20
 RESULT_CODE : 결과코드
 AST_LENGTH : 배열의 원소 개수
 AST_MASS :
 CC_SEQ : 암환자 커뮤니티 댓글 기본키
 NICK : 닉네임(5글자이내)
 PROFILE_PIC : 프로필 사진
 SEQ : 회원일련번호
 CC_CONTENT : 댓글내용
 MBR_GRD : 정회원 10, 준회원 20
 REGDATE : 등록일


 0000 : 조회성공
 4444 : 등록된 글이 없습니다.
 6666 : 회원이 존재하지 않음
 9999 : 기타오류


 * 게시글에서 댓글을 계속해서 불러올때는 댓글리스트API를 사용합니다.
 */

+ (void)requestGetDetailFeed: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response;

/*
 input 값
 SEQ : 회원일련번호(MBER_SN값)
 CM_SEQ : 커뮤니티 기본키

 output값
 OSEQ : 회원일련번호
 RESULT_CODE : 결과코드

 0000 : 입력성공
 1000 : 삭제성공
 6666 : 회원이 존재하지 않음
 9999 : 기타오류
 */

+ (void)requestRegistLike: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response;

/*
 input 값
 SEQ : 회원일련번호(MBER_SN값)
 CC_SEQ : 커뮤니티 댓글기본키

 output 값
 OSEQ : 회원일련번호
 RESULT_CODE : 결과코드

 0000 : 삭제성공
 4444 : 삭제실패
 6666 : 회원이 존재하지 않음
 9999 : 기타오류
 */
+ (void)requestDeleteComment: (NSDictionary *)header seq:(NSString *)seq commentSeq:(NSString *)commentSeq response:(Response)response;


/*
 Input 값
 SEQ :회원일련번호(MBER_SN값)
 CM_SEQ :커뮤니티 기본키
 T_MBER_SN : 언급 회원 일련번호
 CC_CONTENT :댓글내용

 Output값
 OSEQ : 회원일련번호
 RESULT_CODE : 결과코드

 0000 : 입력성공
 4444 : 이미등록된 댓글
 6666 : 회원이 존재하지 않음
 7777 : 사용중지회원
 9999 : 기타오류
 */
+ (void)requestAddComment: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq relationMberSn:(NSString *)relationMberSn conent:(NSString *)content response:(Response)response;

/*
 Input 값
 SEQ: 회원일련번호(MBER_SN값)
 CM_SEQ: 커뮤니티 기본키
 PG_SIZE: 페이지사이즈 (페이지당 리스트 개수)
 PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)

 Output 값
 AST_LENGTH : 배열의 원소 개수
 ADDR_MASS :    배열
 TPAGE : 총 페이지수
 CC_SEQ : 암환자 커뮤니티 댓글 기본키
 CM_SEQ : 암환자 커뮤니티 기본키
 OSEQ : 댓글 글쓴이 회원일련번호
 NICK : 닉네임(5글자이내)
 PROFILE_PIC : 작성자 프로필 사진
 CM_CONTENT : 댓글내용
 MBER_GRAD : 정회원 10, 준회원 20
 REGDATE : 등록일
 */
+ (void)requestGetCommentList: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq page:(NSInteger)page response:(Response)response;

/*
 Input 값
 CM_SEQ: 커뮤니티 고유번호(게시물키,없으면 항목있고 NULL값)
 SEQ: 회원일련번호
 CM_TITLE: 글제목
 CM_GUBUN: 커뮤니티 구분 1:커뮤니티 , 2: 공지사항
 CM_CONTENT: 글내용
 FILE: 이미지
 PROFILE_PIC: 프로필 사진
 CM_TAG: 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 DEL_FILE: 삭제할 이미지 (,로구분) ex)CM_IMG1, CM_IMG2

 Output 값
 RESULT_CODE : 결과코드

 0000 : 입력(수정)성공
 3333 : 동일한 게시물 존재함
 4444 : 필수입력값없음
 8888 : 회원정보없음
 9999 : 기타오류

 // CM_SEQ 값이없으면 등록, 있으면 수정
 // 원본이미지를 넣으면 tm_가 붙은 썸네일 이미지가 생성됩니다. EX)song.jpg(원본), tm_song_jpg(썸네일)

 URL주소
 https://m.shealthcare.co.kr/HL_MED_COMMUNITY/comm_ins.aspx (테스트 할 수 있는 페이지)
 https://m.shealthcare.co.kr/HL_MED_COMMUNITY/HL_upload.ashx (처리페이지, 업로드를 받는 페이지)
 */

+ (void)requestModifyFeed: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq
                    title:(NSString *)title type:(int)type content:(NSString *)content delFiles:(NSString *)delFiles;

/*
 Input 값
 SEQ: 대상 고객 회원일련번호
 ME_SEQ: 조회 고객 회원 일련번호
 PG_SIZE: 페이지사이즈 (페이지당 리스트 개수) (PG_SIZE 미 입력 및 9999값은 페이징 없이 전체 출력입니다.)
 PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)

 Output 값
 MBER_SN: 대상 고객 회원일련번호
 MBER_NM: 대상 고객명
 MBER_SEX: 성별(1:남자,2:여자)
 PROFILE_PIC: 프로필 사진
 DISEASE_OPEN: 질환명 공개여부(Y/N)
 DISEASE_NM: 질환명 (비공개시  비공개 로 표시 됨)
 POINT_TOTAL_AMT: 총 누적 포인트
 LV: 레벨 (1000포인트 당 1레벨 씩 획득 최초 1레벨)
 TOT_PAGE: 총 페이지 수
 TOT_CNT: 총 레코드 수
 RESULT_CODE: 오류코드
 DATA_LENGTH: DATA 배열의 원소 개수
 DATA: 배열
 CM_GUBUN: 커뮤니티 구분 1:커뮤니티 , 2: 공지사항
 CM_SEQ: 커뮤니티 기본키
 NICK: 닉네임(5글자이내)
 PROFILE_PIC: 프로필 사진
 CM_TITLE: 제목
 CM_CONTENT: 글 내용
 REGDATE: 등록일 EX)2016-05-20 오전 11:15:00
 CM_TAG: 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 CM_MEAL : 식사일지  공유 값 ( | 로 구분 )
 HCNT: 해당 글 좋아요 수
 MYHEART: 내가 좋아요 한 글 여부(Y/N)
 MBER_GRAD : 정회원 10, 준회원 20
 RCNT: 해당 글 댓글 수

 0000: 프로필 조회 성공
 8888: 조회 회원이 존재하지 않음
 9999: 기타오류

 */

+ (void)requestProfile: (NSDictionary *)header seq:(NSString *)seq meSeq:(NSString *)meSeq page:(NSInteger)page response:(Response)response;
+ (void)requestTagList;

+ (void)requestAlarmList:(NSString*)seq response:(Response)response;
+ (void)requestSearchListByNick:(NSString*)nick response:(Response)response;

/*
Input 값
 CM_SEQ: 커뮤니티 고유번호(게시물키,없으면 항목있고 NULL값)
 SEQ: 회원일련번호
 CM_TITLE: 글제목
 CM_GUBUN: 커뮤니티 구분 1:커뮤니티 , 2: 공지사항
 CM_CONTENT: 글내용
 FILE: 이미지
 PROFILE_PIC: 프로필 사진
 CM_TAG: 해쉬태그 값 (,로 구분 ) EX)#일상,#운동
 CM_MEAL : 식사일지 공유 값 ( | 로 구분 )
 DEL_FILE: 삭제할 이미지 (,로구분) ex)CM_IMG1, CM_IMG2

 Output 값
 OCM_SEQ : 커뮤니티 고유번호
 CM_POINT : 지급포인트(첫 작성만 지급)
 Defualt:0
 RESULT_CODE : 결과코드

 0000 : 입력(수정)성공
 4444 : 수정 시 수정글 존재하지 않음.
 5555 : 동일한게시물존재
 6666 : 필수 값 입력 없음.(커뮤니티 구분,제목,내용)
 8888 : 회원정보없음
 9999 : 기타오류
// CM_SEQ 값이없으면 등록, 있으면 수정 // 원본이미지를 넣으면 tm_가 붙은 썸네일 이미지가 생성됩니다. EX)song.jpg(원본), tm_song_jpg(썸네일)
URL주소 https://m.shealthcare.co.kr/HL_MED_COMMUNITY/comm_ins.aspx (테스트 할 수 있는 페이지) https://m.shealthcare.co.kr/HL_MED_COMMUNITY/HL_upload.ashx (처리페이지, 업로드를 받는 페이지)
 */
+ (void)requestPostUpload:(FeedModel*)feed response:(Response)response;

/*
 {"DOCNO":"DB016","SEQ": "1871","CM_SEQ":"537"}
*/
+ (void)requestShareEventLog:(NSString*)seq cmSeq:(NSString *)cmSeq response:(Response)response;

@end

#endif /* CommunityNetwork_h */
