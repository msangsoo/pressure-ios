//
//  CommunityNetwork.m
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunityNetwork.h"
#import "FeedModel.h"
#import "TagModel.h"

@implementation CommunityNetwork

+ (instancetype)shared
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

/*
 "DOCNO":"DB001"
 ,"SEQ":"1871"
 ,"PG_SIZE":"10"
 ,"PG":"1"
 ,"CM_GUBUN":"1"
 ,"CM_TAG":"#일상"
 ,"SWORD":""
 */
+ (void)requestGetFeedList:(NSDictionary *)header seq:(NSString *)seq page:(NSInteger)page type:(NSString *)type tag:(NSString *)tag search:(NSString *)word response:(Response)response {

    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB001";
    mDic[@"SEQ"] = seq;
    mDic[@"PG_SIZE"] = @"10";
    mDic[@"PG"] = @(page).stringValue;
    mDic[@"CM_GUBUN"] = type;
    mDic[@"CM_TAG"] = tag;
    mDic[@"SWORD"] = word;


///////////
    /*
    NSArray *jsonArray = [CommunityCommon loadJsonData:@"FeedList"];

    BOOL isSuccess = TRUE;
//
//    NSDictionary *responseDic = @{@"data": jsonArray};

    response(jsonArray, isSuccess);
    return;
*/
//////////
    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        NSArray *jsonArray = [NSArray array];
        BOOL isSuccess = TRUE;
        if([resultSet[@"DATA_LENGTH"] isEqualToString:@"0"] == false && resultSet[@"DATA"] != nil) {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }
        
//        jsonArray = [CommunityCommon loadJsonData:@"FeedList"];

        response(jsonArray, isSuccess);

    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestDeleteFeedItem:(NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response {

    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB011";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = feedSeq;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        /*
         0000 : 삭제성공
         4444 : 삭제 실패(해당글이 없음)
         6666 : 회원이 존재하지 않음
         7777 : 사용중지 회원
         9999 : 기타오류
         */

        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSError *error = nil;
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"]) {
            isSuccess = TRUE;
        } else {
            if ([resultCode isEqualToString:@"4444"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"삭제 싪패"}];
            } else if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"7777"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"사용중지 획원"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }

        response(error, isSuccess);

    } failure:^(NSError *error) {
        response(nil, TRUE);
        NSAssert(error, error.localizedDescription);
    }];
}

//+ (void)requestConfirmDuplecateNickname:(NSDictionary *)header seq:(NSString *)seq nickName:(NSString *)nickName {
//    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
//    mDic[@"DOCNO"] = @"DB009";
//    mDic[@"SEQ"] = seq;
//    mDic[@"NICK"] = nickName;
//
//    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
//    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
//        NSLog(@"%s: %@", __FUNCTION__, resultSet);
//    } failure:^(NSError *error) {
//        NSAssert(error, error.localizedDescription);
//    }];
//}

+ (void)requestSetWithConfirmNickname:(NSDictionary *)header seq:(NSString *)seq nickName:(NSString *)nickName diseaseOpen:(NSString *)open response:(Response)response {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB008";
    mDic[@"SEQ"] = seq;
    mDic[@"NICK"] = nickName;
    mDic[@"DISEASE_OPEN"] = open;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
        NSString *resultCode = resultSet[@"RESULT_CODE"];
        //0000 : 닉네임 수정 성공, 1000 : 닉네임조회 성공, 4444 : 이미 등록된 닉네임, 5555 : 닉네임 수정 실패, 7777 : 사용중지 회원, 9999 : 기타오류
        NSError *error = nil;
        NSString *errMsg = @"";
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"]) {
            isSuccess = TRUE;
        } else {
            if ([resultCode isEqualToString:@"4444"]) {
//                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"이미 등록된 닉네임"}];
                errMsg =@"이미 사용중인 닉네임입니다.\n다른 닉네임을 입력해주세요.";
            } else if ([resultCode isEqualToString:@"5555"]) {
//                error = [NSError errorWithDomain:url code:5555 userInfo:@{NSLocalizedDescriptionKey:@"닉네임 수정 실패"}];
            } else if ([resultCode isEqualToString:@"7777"]) {
//                error = [NSError errorWithDomain:url code:7777 userInfo:@{NSLocalizedDescriptionKey:@"사용중지 획원"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
//                error = [NSError errorWithDomain:url code:9999 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }
        if ([errMsg length] > 0) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:errMsg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"확인", nil];
            [alert show];
        }

        response(error, isSuccess);
    } failure:^(NSError *error) {
        response(nil, TRUE);
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestGetDetailFeed: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB007";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = feedSeq;

//    mDic[@"SEQ"] = @"1871";
//    mDic[@"CM_SEQ"] = @"18";

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSError *error = nil;
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"] || [resultCode isEqualToString:@"1000"]) {
            isSuccess = TRUE;
            response(resultSet, isSuccess);
            return;
        } else {
            if ([resultCode isEqualToString:@"4444"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"등록된 글이 없습니다."}];
            }
            else if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }

        response(error, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestRegistLike: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq response:(Response)response {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB006";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = feedSeq;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
        NSString *resultCode = resultSet[@"RESULT_CODE"];
        /*
         0000 : 입력성공
         1000 : 삭제성공
         6666 : 회원이 존재하지 않음
         9999 : 기타오류
         */
        NSError *error = nil;
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"] || [resultCode isEqualToString:@"1000"]) {
            isSuccess = TRUE;
        } else {
            if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }

        response(error, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestDeleteComment: (NSDictionary *)header seq:(NSString *)seq commentSeq:(NSString *)commentSeq response:(Response)response {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB005";
    mDic[@"SEQ"] = seq;
    mDic[@"CC_SEQ"] = commentSeq;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
            ////        0000 : 삭제성공 4444 : 삭제실패 6666 : 회원이 존재하지 않음 9999 : 기타오류

        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSError *error = nil;
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"] || [resultCode isEqualToString:@"1000"]) {
            isSuccess = TRUE;
            response(resultSet, isSuccess);
            return;
        } else {
            if ([resultCode isEqualToString:@"4444"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"삭제실패"}];
            } else if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }

        response(error, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestAddComment: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq relationMberSn:(NSString *)relationMberSn conent:(NSString *)content response:(Response)response
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB004";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = feedSeq;
    mDic[@"T_MBER_SN"] = relationMberSn;
    mDic[@"CC_CONTENT"] = content;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
        ////        0000 : 입력성공 4444 : 이미등록된 댓글 6666 : 회원이 존재하지 않음 7777 : 사용중지회원 9999 : 기타오류

        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSError *error = nil;
        BOOL isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"] || [resultCode isEqualToString:@"1000"]) {
            isSuccess = TRUE;
            response(resultSet, isSuccess);
            return;
        } else {
            if ([resultCode isEqualToString:@"4444"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"이미등록된 댓글"}];
            } else if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"7777"]) {
                error = [NSError errorWithDomain:url code:7777 userInfo:@{NSLocalizedDescriptionKey:@"사용중지회원"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:9999 userInfo:@{NSLocalizedDescriptionKey:@"기타 오류"}];
            }
        }

        response(error, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestGetCommentList: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq page:(NSInteger)page response:(Response)response
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB003";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = feedSeq;
    mDic[@"PG_SIZE"] = @(10).stringValue;
    mDic[@"PG"] = @(page).stringValue;


//SEQ: 회원일련번호(MBER_SN값)
//CM_SEQ: 커뮤니티 기본키
//PG_SIZE: 페이지사이즈 (페이지당 리스트 개수)
//PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)

    __block BOOL isSuccess = FALSE;
    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        if ([resultSet[@"DATA_LENGTH"]isEqualToString:@"0"]) {
            response(NULL, isSuccess);
            return;
        }
        NSArray *jsonArray = [NSArray array];
        if(resultSet[@"DATA"] != nil) {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }

        isSuccess = TRUE;

        response(jsonArray, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestModifyFeed: (NSDictionary *)header seq:(NSString *)seq feedSeq:(NSString *)feedSeq
                    title:(NSString *)title type:(int)type content:(NSString *)content delFiles:(NSString *)delFiles
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
//    mDic[@"DOCNO"] = @"DB002";
//    mDic[@"CM_SEQ"] = feedSeq;
//    mDic[@"CM_TITLE"] = title;
//    mDic[@"CM_GUBUN"] = @(type).stringValue;
//    mDic[@"CM_CONTENT"] = content;
//    mDic[@"FILE"]
//    mDic[@"SEQ"] = seq;
//    mDic[@"PROFILE_PIC"] = profile;
//    mDic[@"DEL_FILE"] = delFiles

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestProfile: (NSDictionary *)header seq:(NSString *)seq meSeq:(NSString *)meSeq page:(NSInteger)page response:(Response)response {

    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB012";
    mDic[@"SEQ"] = seq;
    mDic[@"ME_SEQ"] = meSeq;
    mDic[@"PG_SIZE"] = @(10).stringValue;
    mDic[@"PG"] = @(page).stringValue;


    /*
    SEQ: 대상 고객 회원일련번호
    ME_SEQ: 조회 고객 회원 일련번호
    PG_SIZE: 페이지사이즈 (페이지당 리스트 개수) (PG_SIZE 미 입력 및 9999값은 페이징 없이 전체 출력입니다.)
    PG: 현재 페이지 (총 페이지:TPAGE까지 페이징 됩니다)
     */

    __block BOOL isSuccess = FALSE;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

            ////        0000 : 입력성공 4444 : 이미등록된 댓글 6666 : 회원이 존재하지 않음 7777 : 사용중지회원 9999 : 기타오류

        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSError *error = nil;
        isSuccess = FALSE;

        if ([resultCode isEqualToString:@"0000"]) {
            isSuccess = TRUE;
            response(resultSet, isSuccess);
            return;
        } else {
            if ([resultCode isEqualToString:@"8888"]) {
                error = [NSError errorWithDomain:url code:8888 userInfo:@{NSLocalizedDescriptionKey:@"조회 회원이 존재하지 않음"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:9999 userInfo:@{NSLocalizedDescriptionKey:@"기타오류"}];
            }
        }

        response(error, isSuccess);

    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestTagList
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB015";

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        NSArray *jsonArray = [NSArray array];
        if(resultSet[@"DATA"] != nil) {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }
        NSMutableArray* modelArray = [NSMutableArray array];
        for(NSDictionary* json in jsonArray)
            {
            [modelArray addObject:[[TagModel alloc] initWithJson:json]];
            }
        communityTagList = modelArray;
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestAlarmList:(NSString*)seq response:(Response)response
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB014";
    mDic[@"SEQ"] = seq;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        BOOL isSuccess = TRUE;
        NSArray *jsonArray = [NSArray array];
        if(resultSet[@"DATA"] != nil) {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }
        response(jsonArray, isSuccess);
            //        NSLog(@"%@",jsonArray);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestSearchListByNick:(NSString*)nick response:(Response)response
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB013";
    mDic[@"NICK"] = nick;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        BOOL isSuccess = TRUE;
        NSArray *jsonArray = [NSArray array];
        if([resultSet[@"DATA_LENGTH"] isEqualToString:@"0"] == false && resultSet[@"DATA"] != nil)
        {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }
        response(jsonArray, isSuccess);
        //        NSLog(@"%@",jsonArray);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

+ (void)requestPostUpload:(FeedModel*)feed response:(Response)response
{
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB002";
    if(feed.cmSeq != nil)
    {
        mDic[@"CM_SEQ"] = feed.cmSeq;
    }
    else
    {
        mDic[@"CM_SEQ"] = @"";
    }
    mDic[@"SEQ"] = feed.mberSn;
    mDic[@"CM_GUBUN"] = @"1";
    mDic[@"CM_TITLE"] = [MANetwork randomStringWithLength:10];

    if(feed.cmMeal != nil)
    {
        mDic[@"CM_MEAL"] = feed.cmMeal;
    }
    else
    {
        mDic[@"CM_MEAL"] = @"";
    }
    mDic[@"CM_CONTENT"] = feed.cmContent;
    if(feed.profilePic != nil)
    {
        mDic[@"PROFILE_PIC"] = feed.profilePic;
    }
    else
    {
        mDic[@"PROFILE_PIC"] = @"";
    }
    mDic[@"CM_TAG"] = [feed.cmTag componentsJoinedByString:@","];
    if(feed.uploadImage != nil)
    {
        mDic[@"FILE"] = feed.uploadImage;
    }
    else
    {
        mDic[@"FILE"] = @"";
    }
    if([mDic[@"CM_SEQ"] isEqualToString:@""] == false && [feed.cmImg1 isEqualToString:@""] == false && feed.uploadImage == nil && feed.isDeleteImage == YES)
    {
        mDic[@"DEL_FILE"] = @"CM_IMG1";
    }
    else
    {
        mDic[@"DEL_FILE"] = @"";
    }


    NSString *url = [mainUrl stringByAppendingPathComponent:communityModifyUrl];
//    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
    [MANetwork PostMultipartDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);

        BOOL isSuccess = TRUE;
        NSString *resultCode = resultSet[@"RESULT_CODE"];

        NSArray *jsonArray = [NSArray array];
        if(resultSet[@"DATA"] != nil) {
            jsonArray = (NSArray *)resultSet[@"DATA"];
        }
//        response(jsonArray, isSuccess);
        //        NSLog(@"%@",jsonArray);
        NSError *error = nil;
        if ([resultCode isEqualToString:@"0000"]) {
            isSuccess = TRUE;
            if ([resultSet[@"DOCNO"] isEqualToString:@"DB002"]) {
                response(resultSet, isSuccess);
            }else {
                response(jsonArray, isSuccess);
            }
            return;
        } else {

            if ([resultCode isEqualToString:@"4444"]) {
                error = [NSError errorWithDomain:url code:4444 userInfo:@{NSLocalizedDescriptionKey:@"수정글이 존재하지 않음."}];
            } else if ([resultCode isEqualToString:@"5555"]) {
                error = [NSError errorWithDomain:url code:5555 userInfo:@{NSLocalizedDescriptionKey:@"동일한게시물존재"}];
            } else if ([resultCode isEqualToString:@"6666"]) {
                error = [NSError errorWithDomain:url code:6666 userInfo:@{NSLocalizedDescriptionKey:@"필수 값 입력 없음."}];
            } else if ([resultCode isEqualToString:@"8888"]) {
                error = [NSError errorWithDomain:url code:8888 userInfo:@{NSLocalizedDescriptionKey:@"회원정보없음"}];
            } else if ([resultCode isEqualToString:@"9999"]) {
                error = [NSError errorWithDomain:url code:9999 userInfo:@{NSLocalizedDescriptionKey:@"기타오류"}];
            }
            response(error, isSuccess);
        }

    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}

// {"DOCNO":"DB016","SEQ": "1871","CM_SEQ":"537"}
+ (void)requestShareEventLog:(NSString*)seq cmSeq:(NSString *)cmSeq response:(Response)response {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    mDic[@"DOCNO"] = @"DB016";
    mDic[@"SEQ"] = seq;
    mDic[@"CM_SEQ"] = cmSeq;

    NSString *url = [mainUrl stringByAppendingPathComponent:communityBaseUrl];
    [MANetwork PostDataAsync:url paramString:mDic success:^(id resultSet) {
        NSLog(@"%s: %@", __FUNCTION__, resultSet);
        BOOL isSuccess = TRUE;
        response(nil, isSuccess);
    } failure:^(NSError *error) {
        NSAssert(error, error.localizedDescription);
    }];
}
@end

