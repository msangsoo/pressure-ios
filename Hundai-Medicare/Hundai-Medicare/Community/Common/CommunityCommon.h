//
//  CommunityCommon.h
//  Hundai-Medicare
//
//  Created by Paul P on 10/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#ifndef CommunityCommon_h
#define CommunityCommon_h

#import <Foundation/Foundation.h>

typedef enum: NSUInteger {
    userInfoCell,
    imageCell,
    mealCell,
    textCell,
    moreCell,
    tagCell,
    snsShareCell,
    infoCell
} CommunityCellType;

typedef enum : NSUInteger {
    commentCell,
    commentTextCell
} CommentCellType;

typedef enum: NSUInteger {
    profileCommentCell,
    profileDescCell
} ProfileCellType;

extern NSString *mainUrl;
extern NSString *communityBaseUrl;
extern NSString *communityModifyUrl;

extern NSArray* communityTagList;

@interface CommunityCommon: NSObject
+ (NSArray *)loadJsonData:(NSString *)path;
+ (BOOL)checkValidateString:(NSString *)string;
+ (NSString *)convertFromNumberToCurrencyNoUnit:(NSString *)number;
+ (NSString *)convertDateToFormat:(NSString *)dateString;
+ (NSString *)convertDateToFormatShort:(NSString *)dateString;

@end

@interface UILabel (setColor)
- (void)setColor:(NSString*)text color:(UIColor*)color;
- (void)setFontStyle:(NSString*)text font:(UIFont*)font;
- (void)setFontStyle:(NSString*)text font:(UIFont*)font color:(UIColor*)color;
@end

#include <math.h>


@interface NSString (HeightCalc)
- (CGFloat)heightForWidth:(CGFloat)width usingFont:(UIFont *)font;
@end

@implementation NSString (HeightCalc)

- (CGFloat)heightForWidth:(CGFloat)width usingFont:(UIFont *)font
{
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize labelSize = (CGSize){width, FLT_MAX};
    CGRect r = [self boundingRectWithSize:labelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:context];
    return r.size.height;
}
@end

#endif /* CommunityCommon_h */
