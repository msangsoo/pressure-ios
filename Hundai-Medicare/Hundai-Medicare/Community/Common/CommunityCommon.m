//
//  CommunityCommon.m
//  Hundai-Medicare
//
//  Created by Paul P on 11/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunityCommon.h"
#import "SBJson.h"

NSString *mainUrl = @"https://m.shealthcare.co.kr";
NSString *communityBaseUrl = @"HL_MED_COMMUNITY/ws.asmx/getJson";
NSString *communityModifyUrl = @"HL_MED_COMMUNITY/HL_upload.ashx";

//static NSString *communityAlarmUrl = @"HL_MED_COMMUNITY/ws.asmx/getJson";

NSArray* communityTagList = nil;

@implementation CommunityCommon

+ (NSArray *)loadJsonData:(NSString *)path
{
    NSString *filePath = [[NSBundle mainBundle]pathForResource:path ofType:@"json"];
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSString *jsonStr = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];

    SBJsonParser *parser = [SBJsonParser new];
    NSArray *jsonArray = [parser objectWithString:jsonStr];
//    NSLog(@"%s%@", __FUNCTION__, jsonArray);

    return jsonArray;
}

+ (BOOL)checkValidateString:(NSString *)string {
    if (!string) {
        return YES;
    }

    NSString *regex = @"^[ㄱ-힣0-9a-zA-Z]*$";
    NSRange r = [string rangeOfString:regex options:NSRegularExpressionSearch];

    if(r.length == 0) {
        return NO;
    }
    return YES;
}

+(NSString *)convertFromNumberToCurrencyNoUnit:(NSString *)number {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *numberFromString = [NSNumber numberWithInt:[number intValue]];
    NSString *balance = [formatter stringFromNumber:numberFromString];
    return balance;
}


/*
 if let date = comment["insert_dt"].string?.date(formatString: "yyyy-MM-dd HH:mm:ss")
 {
 let diffSec = Int((Date().timeIntervalSince1970 - date.timeIntervalSince1970)/60)
 if diffSec < 60
 {
 time = "방금 전"
 }
 else if diffSec < 60
 {
 time = "\(diffSec)분 전"
 }
 else if diffSec < 60*12
 {
 time = "\(diffSec/60)시간 전"
 }
 else if date.day == Date().day
 {
 time = date.string(custom: "HH:mm")
 }
 else
 {
 time = date.string(custom: "M월 d일 HH:mm")
 }
 }
 */

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];

    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];

    return [difference day];
}

+ (NSString *)convertDateToFormat:(NSString *)dateString {
        //2019-03-05 오전 11:24:00
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd a hh:mm:ss"];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ko_KR"]];

    NSDate *date = [dateFormat dateFromString:dateString];
    int diffSec = ([[NSDate date]timeIntervalSince1970] - date.timeIntervalSince1970);
//    NSDateComponents *localCalendar = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
//    NSDateComponents *dateCalendar = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];

    NSString *time = @"";
    if (diffSec < 60) {
        //time = [NSString stringWithFormat:@"%@초 전", @(MAX(diffSec,0))];
        time = @"1분 전";
    } else if (diffSec < 60*60) {
        time = [NSString stringWithFormat:@"%@분 전", @(diffSec/60)];
    } else if (diffSec < 60*60*24) {
        time = [NSString stringWithFormat:@"%@시간 전", @(diffSec/60/60)];
    }else
    {
        NSInteger days = [CommunityCommon daysBetweenDate:date andDate:[NSDate date]];
        if(days == 1)
        {
            time = @"어제";
        }
        else// if(days < 10)
        {
            time = [NSString stringWithFormat:@"%@일 전", @(days)];
        }
//        else
//        {
//            //time = [NSString stringWithFormat:@"%@달 전", @(days/30)];
//            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc]init];
//            [dateFormat2 setDateFormat:@"yy년 M월d일 aH시m분"];
//            [dateFormat2 setLocale:[NSLocale localeWithLocaleIdentifier:@"ko_KR"]];
//            time = [dateFormat2 stringFromDate:date];
//        }
    }

    return time;
}

+ (NSString *)convertDateToFormatShort:(NSString *)dateString {
    //2019-03-05 오전 11:24:00
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd a hh:mm:ss"];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ko_KR"]];

    NSDate *date = [dateFormat dateFromString:dateString];
    int diffSec = ([[NSDate date]timeIntervalSince1970] - date.timeIntervalSince1970);
    //    NSDateComponents *localCalendar = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    //    NSDateComponents *dateCalendar = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];

    NSString *time = @"";
    if (diffSec < 60) {
        time = [NSString stringWithFormat:@"%@초 전", @(MAX(diffSec,0))];
    } else if (diffSec < 60*60) {
        time = [NSString stringWithFormat:@"%@분 전", @(diffSec/60)];
    } else if (diffSec < 60*60*24) {
        time = [NSString stringWithFormat:@"%@시간 전", @(diffSec/60/60)];
    }else
    {
        NSInteger days = [CommunityCommon daysBetweenDate:date andDate:[NSDate date]];
        if(days == 1)
        {
            time = @"어제";
        }
        else if(days <= 30)
        {
            time = [NSString stringWithFormat:@"%@일 전", @(days)];
        }
        else
        {
            time = [NSString stringWithFormat:@"%@달 전", @(days/30)];
//            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc]init];
//            [dateFormat2 setDateFormat:@"yy년 M월d일 aH시m분"];
//            [dateFormat2 setLocale:[NSLocale localeWithLocaleIdentifier:@"ko_KR"]];
//            time = [dateFormat2 stringFromDate:date];
        }
    }

    return time;
}

@end

@implementation UILabel (setColor)
- (void)setColor:(NSString*)text color:(UIColor*)color
{
    NSDictionary* attribute = @{NSForegroundColorAttributeName: color};
    NSString* lblText = self.attributedText.string;
    if(lblText == nil)
    {
        lblText = @"";
    }
    NSAttributedString* tmp = self.attributedText;
    if(tmp == nil)
    {
        tmp = [[NSAttributedString alloc] init];
    }

    NSMutableAttributedString* attrText = [[NSMutableAttributedString alloc] initWithAttributedString:tmp];

    NSRange frange = [lblText.lowercaseString rangeOfString:text.lowercaseString];
    if(frange.location != NSNotFound)
    {
        [attrText addAttributes:attribute range:frange];
        self.attributedText = attrText;
    }
}
- (void)setFontStyle:(NSString*)text font:(UIFont*)font
{
    [self setFontStyle:text font:font color:nil];
}
- (void)setFontStyle:(NSString*)text font:(UIFont*)font color:(UIColor*)color
{
    NSMutableDictionary* attribute = [@{NSFontAttributeName: font} mutableCopy];
    if(color != nil)
    {
        [attribute setValue:color forKey:NSForegroundColorAttributeName];
    }
    NSString* lblText = self.attributedText.string;
    if(lblText == nil)
    {
        lblText = @"";
    }
    NSAttributedString* tmp = self.attributedText;
    if(tmp == nil)
    {
        tmp = [[NSAttributedString alloc] init];
    }

    NSMutableAttributedString* attrText = [[NSMutableAttributedString alloc] initWithAttributedString:tmp];

    NSRange frange = [lblText.lowercaseString rangeOfString:text.lowercaseString];
    if(frange.location != NSNotFound)
    {
        [attrText addAttributes:attribute range:frange];
        self.attributedText = attrText;
    }
}

@end
