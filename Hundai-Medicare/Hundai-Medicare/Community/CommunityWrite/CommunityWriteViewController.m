//
//  CommunityWriteViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunityWriteViewController.h"
#import "CommunityCommon.h"
#import "TagModel.h"
#import "CommunitySearchTagListTableViewCell.h"
#import "FeedModel.h"
#import "CommunityNetwork.h"
#import "FeedWriteCancelViewController.h"
#import "FeedWriteSaveViewController.h"

@import SDWebImage;
@import IQKeyboardManager;
@import MobileCoreServices;

@interface CommunityWriteViewController () <UITextFieldDelegate, UITableViewDataSource, UIScrollViewDelegate, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@property (nonatomic,weak) UITextView* textView;
@property (nonatomic,weak) CommunitySearchTagListTableViewCell* tagListCell;
@property (nonatomic,weak) UITableViewCell* writeCell;
@property (nonatomic,strong) UIImage* selectedImage;
@property (nonatomic, copy) void (^completion)(BOOL);
@property (nonatomic, copy) void (^shareCompletion)(BOOL, id);
@end

@implementation CommunityWriteViewController
typedef enum {
    Sections_tagList = 0,
    Sections_contentHeader,
    Sections_contentMeal,
    Sections_contentImage,
    Sections_contentText,
    Sections_count
} Sections;

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM04 m_cod:HM04001 s_cod:HM04001001];
    
    // Do any additional setup after loading the view.
    _tableView.estimatedRowHeight = 213;

    if(self.feedModel == nil)
    {
        self.feedModel = [[FeedModel alloc] init];
        self.feedModel.mberSn = [[_model getLoginData] mber_sn];
//        self.feedModel.cmMeal = @"asdasdasdasdasdasdasdasdasdasda한글sdasdasdasdasdasdasdasdasdasdasdasdasdasd";
    }
    else
    {
        if(self.feedModel.cmImg1 != nil && self.feedModel.cmImg1.length != 0)
        {
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:self.feedModel.cmImg1] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {

                if (image && finished) {
                    self.selectedImage = image;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIView performWithoutAnimation:^{
                            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:Sections_contentImage] withRowAnimation:UITableViewRowAnimationNone];
                        }];
                    });
                }
            }];
        }
    }
//    self.feedModel.cmMeal = @"asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd";
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    if([segue.identifier isEqualToString:@"resultSegue"]){
//        CommunitySearchResultViewController* dvc = segue.destinationViewController;
//        dvc.searchKeyword = sender;
//
//    }
    if([segue.identifier isEqualToString:@"cancelSegue"])
    {
        FeedWriteCancelViewController* vc = segue.destinationViewController;
        __weak FeedWriteCancelViewController* weakVc = vc;
        __weak CommunityWriteViewController* weakSelf = self;

        vc.touchedDoneButton = ^{
            [weakVc dismissViewControllerAnimated:true completion:^{
                [weakSelf _close:NO];
            }];

        };
        vc.touchedCancelButton = ^{
            [weakVc dismissViewControllerAnimated:true completion:nil];
        };
    }
    else if([segue.identifier isEqualToString:@"saveSegue"])
    {
        FeedWriteSaveViewController* vc = segue.destinationViewController;
        __weak FeedWriteSaveViewController* weakVc = vc;
        __weak CommunityWriteViewController* weakSelf = self;
        vc.touchedDoneButton = ^{
            [weakVc dismissViewControllerAnimated:true completion:^{
                [weakSelf _save];
            }];
        };
        vc.touchedCancelButton = ^{
            [weakVc dismissViewControllerAnimated:true completion:nil];
        };
    }
    else if([segue.identifier isEqualToString:@"emptySegue"])
    {
        FeedWriteSaveViewController* vc = segue.destinationViewController;
        __weak FeedWriteSaveViewController* weakVc = vc;
        vc.touchedDoneButton = ^{
            [weakVc dismissViewControllerAnimated:true completion:nil];
        };
    }
}

#pragma mark - action method

- (void)_close:(BOOL)success {
    if(self.navigationController != nil && self.navigationController.viewControllers.firstObject != self)
    {
        [self.navigationController popViewControllerAnimated:true];
        return;
    }
    [self dismissViewControllerAnimated:true completion:^{
//        refreshNewFeed
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshNewFeed" object:nil];
        if(self.completion)
        {
            self.completion(success);
        }
    }];
}

- (void)_shareClose:(BOOL)success response:(id)response {
    if(self.navigationController != nil && self.navigationController.viewControllers.firstObject != self)
    {
        [self.navigationController popViewControllerAnimated:true];
        return;
    }
    [self dismissViewControllerAnimated:true completion:^{
        //        refreshNewFeed
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshNewFeed" object:nil];
        if(self.shareCompletion)
        {
            self.shareCompletion(success, response);
        }
    }];
}

- (IBAction)close:(UIButton*)sender {
    if (self.textView.text == nil || self.textView.text.length == 0) {
        [self dismissViewControllerAnimated:true completion:^{
            [self _close:NO];
        }];
        return;
    }

    [self performSegueWithIdentifier:@"cancelSegue" sender:nil];
}

- (void)_save {
    if(self.feedModel.cmTag == nil)
    {
        self.feedModel.cmTag = [@[] mutableCopy];
    }
    [self.feedModel.cmTag removeAllObjects];
    for(TagView* tagView in self.tagListCell.tagListView.tagViews)
    {
        if(tagView.selected)
        {
            NSString* tag = [tagView titleForState:UIControlStateNormal];
            [self.feedModel.cmTag addObject:tag];
        }
    }
    self.feedModel.cmContent = self.textView.text;
    [CommunityNetwork requestPostUpload:self.feedModel response:^(id response, BOOL isSuccess) {
        if(isSuccess)
        {
            if (self.isShare) {
                [self _shareClose:true response:response];
            }else {
                [self _close:YES];
            }
        }
        else
        {
            //..
        }
    }];
}
- (IBAction)save:(UIButton*)sender {

    NSString* trimmedString = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(trimmedString.length == 0)
    {
        [self performSegueWithIdentifier:@"emptySegue" sender:nil];
        return;
    }
    [self performSegueWithIdentifier:@"saveSegue" sender:nil];
}


-(IBAction)textFieldChanged:(UITextField*)sender{
//    [UIView performWithoutAnimation:^{
//        [self.tableView reloadData];
//    }];
}

-(IBAction)addPhoto:(id)sender
{
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *man = [UIAlertAction
                          actionWithTitle:@"Camera"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action) {
                              if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                  [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
                              }
                          }];
    UIAlertAction *wman = [UIAlertAction
                           actionWithTitle:@"Library"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                   [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                               }
                           }];

    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {

                             }];
    [actionController addAction:man];
    [actionController addAction:wman];
    [actionController addAction:cancel];
    [self presentViewController:actionController animated:YES completion:nil];
}

- (void)showImagePickerControllerWithSourceType: (UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
//    picker.allowsEditing = YES;

    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.selectedImage = chosenImage;
    self.feedModel.uploadImage = chosenImage;
    self.feedModel.isDeleteImage = NO;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:Sections_contentImage] withRowAnimation:UITableViewRowAnimationNone];
    }];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)removePhoto:(UIButton*)sender{
    self.selectedImage = nil;
    self.feedModel.isDeleteImage = YES;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:Sections_contentImage] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)textViewDidChange:(UITextView *)textView
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [UIView setAnimationsEnabled:NO];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        [UIView setAnimationsEnabled:YES];
        [IQKeyboardManager.sharedManager reloadLayoutIfNeeded];
    });
}
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self.textView resignFirstResponder];
//}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(indexPath.section)
    {
        case Sections_tagList :
        {
            if(self.tagListCell != nil)
            {
                return self.tagListCell;
            }
            CommunitySearchTagListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"tagListCell"];
            self.tagListCell = cell;

            for(TagModel* tag in communityTagList)
            {
                TagView* tv = [cell.tagListView addTag:tag.tag];
                tv.onTap = ^(TagView* tv){
                    tv.selected = !tv.selected;
                    tv.backgroundColor = tv.selected ? RGB(0x70, 0x80, 0xe8) : [UIColor whiteColor];
                    [tv setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
                };
                if([self.feedModel.cmTag containsObject:tag.tag])
                {
                    tv.selected = !tv.selected;
                    tv.backgroundColor = tv.selected ? RGB(0x70, 0x80, 0xe8) : [UIColor whiteColor];
                    [tv setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
                }
            }
            return cell;
        }
        case Sections_contentHeader :
        {
            return [tableView dequeueReusableCellWithIdentifier:@"contentHeaderCell"];
        }
        case Sections_contentMeal :
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"mealCell"];
            UILabel* mealLabel = [cell viewWithTag:1];
//            mealLabel.text = self.feedModel.cmMeal;
            mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@ ",self.feedModel.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(11), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
            return cell;
        }
        case Sections_contentImage :
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"contentImageCell"];
            UIImageView* imgView = [cell viewWithTag:1];
            imgView.image = self.selectedImage;
            return cell;
        }
        case Sections_contentText :
        {
            if(self.writeCell != nil)
            {
                return self.writeCell;
            }
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"contentTextCell"];
            self.writeCell = cell;
            self.textView = [cell viewWithTag:1];
            self.textView.text = self.feedModel.cmContent;
            return cell;
        }
        default :
            break;
    }
    return [[UITableViewCell alloc] init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == Sections_contentImage)
    {
        if(self.selectedImage == nil)
        {
            return 0;
        }
    }
    else if(section == Sections_contentMeal)
    {
        if(self.feedModel.cmMeal == nil || self.feedModel.cmMeal.length == 0)
        {
            return 0;
        }
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return Sections_count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

+(CommunityWriteViewController*)showContentWriteViewController:(UIViewController*)parentVC feed:(FeedModel*)feedModel completion:(void(^)(BOOL success))completion;
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
    CommunityWriteViewController *vc = (CommunityWriteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"write"];

    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.feedModel = feedModel;
    vc.completion = completion;
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    [parentVC presentViewController:navi animated:TRUE completion:nil];

    return vc;
}

+(CommunityWriteViewController*)showContentWriteViewController:(UIViewController*)parentVC feed:(FeedModel*)feedModel isShare:(BOOL)isShare completion:(void(^)(BOOL success, id response))completion;
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
    CommunityWriteViewController *vc = (CommunityWriteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"write"];
    
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.feedModel = feedModel;
    vc.isShare = isShare;
    vc.shareCompletion = completion;
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    [parentVC presentViewController:navi animated:TRUE completion:nil];
    
    return vc;
}
@end
