//
//  CommunityWriteViewController.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class FeedModel;

@interface CommunityWriteViewController : BaseViewController
@property (nonatomic,strong) FeedModel* feedModel;
@property (nonatomic,assign) BOOL isShare;
+(CommunityWriteViewController*)showContentWriteViewController:(UIViewController*)parentVC feed:(FeedModel*)feedModel completion:(void(^)(BOOL success))completion;
+(CommunityWriteViewController*)showContentWriteViewController:(UIViewController*)parentVC feed:(FeedModel*)feedModel isShare:(BOOL)isShare completion:(void(^)(BOOL success, id response))completion;
@end

NS_ASSUME_NONNULL_END
