//
//  ProfileViewController.m
//  Hundai-Medicare
//
//  Created by Paul P on 14/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ProfileViewController.h"
#import "Masonry.h"
#import "ProfileCommentCell.h"
#import "ProfileDescCell.h"
#import "CommunityCommon.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"
#import "NickNameInputAlertViewController.h"
#import "DuplicationAlertViewController.h"
#import "AXPhotoViewer-Swift.h"

@import MobileCoreServices;
@import SDWebImage;

@interface ProfileViewController ()<UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ServerCommunicationDelegate>
{

}

@property(nonatomic, strong)NickNameInputAlertViewController *nickNameInputAlerVC;
@property(nonatomic, strong)UIRefreshControl *refreshControl;
@property(nonatomic, assign) NSInteger numberOfLines;
@property(nonatomic, assign)BOOL isExtends;
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _pageSize = 0;
    _page = 1;

    self.commentArray = [NSMutableArray array];
    
    [self setupNavigationBar];
    [self setupViews];
    [self setupRefreshView];
    [self setupTableView];

    [self requestGetProfile:self.seq meSeq:self.memSeq page:1 isRefresh:FALSE];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)gotoBack:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)setupNavigationBar {
    self.title = @"프로필";
    self.navigationController.navigationBar.tintColor = COLOR_MAIN_DARK;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName:FONT_NOTO_BOLD(18)};
}

- (void)setupViews {
    double imageViewWidth = self.profileImageView.bounds.size.width;
    self.profileImageView.layer.cornerRadius = imageViewWidth / 2.0;
    self.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
}

- (void)bindProfileInfo {
    if (self.profileModel.profilePick != nil) {

        NSString* profileImage = [self.profileModel.profilePick stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];

        if ([self.profileModel.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
            self.profileImageView.layer.borderColor = RGB(243, 163, 41).CGColor;
        }
        else
        {
            self.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
        }

        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error != nil) {
            }
        }];
    }

    self.levelLabel.text = [NSString stringWithFormat:@"Lv.%@", [self calculatorLevel:self.profileModel.totPoint]];
    NSString *sex = @"여자";
    if ([self.profileModel.mberSex isEqualToString:@"1"]) {
        sex = @"남자";
    }
    NSString *disease = @"";
    if ([_model.getLoginData.mber_sn isEqualToString:self.profileModel.mberSN]) {
//        if (self.profileModel.diseaseNM != nil && ![self.profileModel.diseaseNM isEqualToString:@""] ) {
//            disease = [NSString stringWithFormat:@" / %@", self.profileModel.diseaseNM];//[self profileDisease:self.profileModel]];
//        }
        if ([self.profileModel.diseaseOpen isEqualToString:@"Y"]) {
            if (self.profileModel.diseaseNM != nil && ![self.profileModel.diseaseNM isEqualToString:@""] ) {
                disease = [NSString stringWithFormat:@" / %@", self.profileModel.diseaseNM];//[self profileDisease:self.profileModel]];
            }
        } else {
            disease = @" / 비공개";
        }
    } else {
        if ([self.profileModel.diseaseOpen isEqualToString:@"Y"]) {
            if (self.profileModel.diseaseNM != nil && ![self.profileModel.diseaseNM isEqualToString:@""] ) {
                disease = [NSString stringWithFormat:@" / %@", self.profileModel.diseaseNM];//[self profileDisease:self.profileModel]];
            }
        } else {
            disease = @" / 비공개";
        }
    }

    self.diseaseLabel.text = [NSString stringWithFormat:@"%@%@", sex, disease];
    self.nickNameLabel.text = self.profileModel.nickName;

    [self.commentCountLabel setHidden:TRUE];
    if (self.profileModel.totCnt > 0) {
        [self.commentCountLabel setHidden:FALSE];
        self.commentCountLabel.text = [NSString stringWithFormat:@"작성글 %@개", self.profileModel.totCnt];
    }

    if (self.profileModel.totPoint > 0) {
        self.pointLabel.text = [NSString stringWithFormat:@"누적포인트 %@ P", [CommunityCommon convertFromNumberToCurrencyNoUnit:self.profileModel.totPoint]];
    }

    BOOL hideModifyButton = TRUE;
    if (self.profileModel.mberSN != nil && [self.profileModel.mberSN isEqualToString:_model.getLoginData.mber_sn]) {
        hideModifyButton = FALSE;
    }
    [self.profileModifyButton setHidden:hideModifyButton];
    [self.uploadImageButton setHidden:hideModifyButton];
}

- (NSString *)calculatorLevel:(NSString *)totPoint {
    NSInteger point = [[totPoint stringByReplacingOccurrencesOfString:@"," withString:@""]integerValue];
    NSInteger level = point / 1000;
    if (level == 0 || level == 1) {
        return @"1";
    }
    return [NSString stringWithFormat:@"%ld", level];
}

- (NSString *)profileDisease:(ProfileModel *)profileModel {
        //disease_nm - 1:고혈압, 2:당뇨, 3:고지혈증, 4:비만, 5:없음 6:기타
    if ( profileModel.diseaseNM == nil || [profileModel.diseaseNM isEqualToString:@""]) {
        return @"없음";
    }
    NSMutableString *diseaseStr = [NSMutableString string];
    if ([profileModel.diseaseNM isEqualToString:@"1"]) {
        [diseaseStr appendString: @"고혈압"];
    } else if ([profileModel.diseaseNM isEqualToString:@"2"]) {
        [diseaseStr appendString: @"당뇨"];
    } else if ([profileModel.diseaseNM isEqualToString:@"3"]) {
        [diseaseStr appendString: @"고지혈증"];
    } else if ([profileModel.diseaseNM isEqualToString:@"4"]) {
        [diseaseStr appendString: @"비만"];
    } else if ([profileModel.diseaseNM isEqualToString:@"5"]) {
        [diseaseStr appendString: @"없음"];
    } else {
        [diseaseStr appendString:profileModel.diseaseNM];
    }
    return diseaseStr;
}

- (void)setupTableView {
    self.profileTableView.estimatedRowHeight = 100;
    self.profileTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)setupRefreshView {
    _refreshControl = [[UIRefreshControl alloc]init];
    [_refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.profileTableView  addSubview:_refreshControl];
}

- (void)handleRefresh:(UIRefreshControl *)sender {
    NSString *seq = _model.getLoginData.seq;
    [self requestGetProfile:seq meSeq:self.feed.oSeq page:1 isRefresh:TRUE];
}

- (void)requestGetProfile:(NSString *)seq meSeq:(NSString *)meSeq page:(NSInteger)page isRefresh:(BOOL)isRefresh {
    NSDictionary *header = [NSDictionary dictionary];
    
    self.isNewDataLoading = TRUE;
    [CommunityNetwork requestProfile:header seq:seq meSeq:meSeq page:page response:^(id response, BOOL isSuccess) {
        self.isNewDataLoading = FALSE;

        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }

        if (isSuccess == FALSE) {
            return;
        }

        if (isRefresh && [self.commentArray count] > 0) {
            self.page = 0;
            [self.commentArray removeAllObjects];
        }

        self.profileModel = [[ProfileModel alloc]initWithJson:response];

        NSString *dataLength = response[@"DATA_LENGTH"];
        if ([dataLength intValue] != 0) {
            NSArray *comments = (NSArray *)response[@"DATA"];
            if (comments != nil && comments.count > 0) {
                for (NSDictionary *comment in comments) {
                    [self.commentArray addObject:[[ProfileCommentModel alloc]initWithJson:comment]];
                }

                self.page += 1;

                [self.profileTableView reloadData];
            }
        }

        [self bindProfileInfo];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == profileDescCell) {
        return 45;
    }
    else if (indexPath.row == profileCommentCell) {
        return self.isExtends == true ? 100 : UITableViewAutomaticDimension;
    }

    return UITableViewAutomaticDimension;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [UITableViewCell new];
    ProfileCommentModel *comment = [self.commentArray objectAtIndex:indexPath.section];
    switch (indexPath.row) {
        case profileCommentCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ProfileCommentCell reuseIdentifier] forIndexPath:indexPath];
            ProfileCommentCell *profileCell = (ProfileCommentCell *)cell;

            BOOL thumbImageHidden = TRUE;
            if (comment.cmImg1 != nil && ![comment.cmImg1 isEqualToString:@""]) {
                NSString* profileImage = [comment.cmImg1 stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];

                [profileCell.thumbImgeView sd_setImageWithURL:[NSURL URLWithString:profileImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                }];
                thumbImageHidden = FALSE;
            }
            [profileCell.thumbImgeView setHidden:thumbImageHidden];

            BOOL mealHidden = TRUE;
            if (comment.cmMeal != nil && ![comment.cmMeal isEqualToString:@""]) {
                profileCell.mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", comment.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(11), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
                profileCell.contentTopConstraint.constant = 25;
            } else {
                profileCell.contentTopConstraint.constant = 15;
            }
            [profileCell.mealLabel setHidden:mealHidden];

            profileCell.conentsLabel.numberOfLines = 0;
            profileCell.conentsLabel.lineBreakMode = NSLineBreakByTruncatingTail;

            profileCell.conentsLabel.text = comment.cmContent;
            profileCell.thumbImageButton.indexPath = indexPath;


            CGFloat unitHeight = [@"A" heightForWidth:profileCell.bounds.size.width usingFont:profileCell.conentsLabel.font];
            CGFloat blockHeight = [comment.cmContent heightForWidth:profileCell.bounds.size.width usingFont:profileCell.conentsLabel.font];
            self.numberOfLines = ceilf(blockHeight / unitHeight);
            self.isExtends = self.numberOfLines < 3 && ![comment.cmImg1 isEqualToString:@""];

            break;
        }
        case profileDescCell: {
            cell = [tableView dequeueReusableCellWithIdentifier:[ProfileDescCell reuseIdentifier] forIndexPath:indexPath];
            ProfileDescCell *descCell = (ProfileDescCell *)cell;
            descCell.creationLabel.text = [CommunityCommon convertDateToFormat:comment.regDate];
            descCell.likeCountLabel.text = [NSString stringWithFormat:@"%@개", comment.hCnt];
            if (comment.rCnt > 0) {
                descCell.commentCountLabel.text = [NSString stringWithFormat:@"%@개", comment.rCnt];
            }
        } break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileCommentModel *comment = [self.commentArray objectAtIndex:indexPath.section];

    switch (indexPath.row) {
        case profileCommentCell: {
            ProfileCommentCell *profileCell = (ProfileCommentCell *)cell;
//            [profileCell.thumbImgeView sd_setImageWithURL:[NSURL URLWithString:comment.cmImg1] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                if (error != nil) {
//
//                }
//            }];
//            profileCell.mealLabel.attributedText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@", comment.cmMeal]   attributes:@{NSFontAttributeName:FONT_NOTO_REGULAR(11), NSForegroundColorAttributeName:RGB(255, 255, 255), NSBackgroundColorAttributeName:RGB(143, 144, 157)}];
//            profileCell.conentsLabel.text = comment.cmContent;
        } break;
        case profileDescCell: {
//            ProfileDescCell *descCell = (ProfileDescCell *)cell;
//            descCell.creationLabel.text = comment.regDate;
//            descCell.likeCountLabel.text = [NSString stringWithFormat:@"%@개", comment.hCnt];
//            if (comment.rCnt > 0) {
//                descCell.commentCountLabel.text = [NSString stringWithFormat:@"작성글 %@개", comment.rCnt];
//            }
        } break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileCommentModel *comment = [self.commentArray objectAtIndex:indexPath.section];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    feedDetailVC.feedSeq = comment.cmSeq;
    feedDetailVC.seq = _model.getLoginData.mber_sn;
    [self.navigationController pushViewController:feedDetailVC animated:TRUE];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.commentArray.count;
}

- (IBAction)touchedProfileModifyButton:(UIButton *)sender {
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *man = [UIAlertAction
                          actionWithTitle:@"Camera"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action) {
                              if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                  [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
                              }
                          }];
    UIAlertAction *wman = [UIAlertAction
                           actionWithTitle:@"Library"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                   [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                               }
                           }];

    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {

                             }];
    [actionController addAction:man];
    [actionController addAction:wman];
    [actionController addAction:cancel];
    [self presentViewController:actionController animated:YES completion:nil];
}

- (void)showImagePickerControllerWithSourceType: (UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;

    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        self.profileImageView.image = chosenImage;
        [server writeImage:chosenImage];
    }];
}

- (IBAction)touchedNickModifyButton:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    self.nickNameInputAlerVC = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
//    NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
    self.nickNameInputAlerVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.nickNameInputAlerVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    self.nickNameInputAlerVC.nickName = self.nickNameLabel.text;
    self.nickNameInputAlerVC.isPublic = [self.profileModel.diseaseOpen isEqualToString:@"Y"] ? TRUE : FALSE;
    self.nickNameInputAlerVC.touchedCancelButton = ^{
        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
    };
    _nickNameInputAlerVC.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
        [self requestSetNickname:nickName isPublic:isPublic];
    };
    [self.navigationController.tabBarController presentViewController:_nickNameInputAlerVC animated:TRUE completion:nil];
}

- (void)requestSetNickname:(NSString *)nickName isPublic:(BOOL)isPublic {
    NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
    NSString *seq = _model.getLoginData.mber_sn;

    [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
        NSMutableDictionary *responseDic = [NSMutableDictionary dictionary];
        responseDic[@"isSuccess"] = @(isSuccess);
        responseDic[@"response"] = responseObj;
//        [NSNotificationCenter.defaultCenter postNotificationName:@"PostNicknameNotification" object:responseDic];
        if (isSuccess == TRUE) {
            // 성공 알럿 띄우기
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion: ^{
                self.profileModel.nickName = nickName;
                self.profileModel.diseaseOpen = diseaseOpen;
                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.nickNameLabel.text = self.profileModel.mberNM;
                    [self bindProfileInfo];
                });
            }];
            return;
        }

        NSError *error = (NSError *)responseObj;
        NSLog(@"Error : %@", [error localizedDescription]);
/*
        if ([error code] != 4444) {
            [Utils basicAlertView:self withTitle:@"오류" withMsg:[NSString stringWithFormat:@"%@",error.description]];
            return;
        }
            // 실패시 알럿 띄우기
        
//        self.definesPresentationContext = FALSE;d
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        DuplicationAlertViewController *vc = (DuplicationAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DuplicationAlertViewController"];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.dimView.alpha = 0.1;
        vc.touchedDoneButton = ^{
            [self.nickNameInputAlerVC dismissViewControllerAnimated:TRUE completion:nil];
        };

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.nickNameInputAlerVC presentViewController:vc animated:TRUE completion:nil];
        });
         */
    }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    if (scrollView.contentSize.height < scrollView.frame.size.height) {
//        return;
//    }

    if (scrollView == self.profileTableView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height - 120) {
            if (!self.isNewDataLoading) {
                /*
                 vc.seq = _model.getLoginData.mber_sn;
                 vc.feedSeq = feed.cmSeq;
                 */
//                [self requestGetComments:_model.getLoginData.mber_sn feedSeq:self.feed.cmSeq page:self.page];
                [self requestGetProfile:self.seq meSeq:self.feed.oSeq page:self.page isRefresh:FALSE];
            }
        }
    }
}

- (void)requestGetComments:(NSString *)seq feedSeq:(NSString *)feedSeq page:(NSInteger)page {
    NSDictionary *header = [NSDictionary dictionary];

    [CommunityNetwork requestGetCommentList:header seq:seq feedSeq:feedSeq page:page response:^(id response, BOOL isSuccess) {
        NSLog(@"GetComments : %@", response);
        if(isSuccess == FALSE || ((NSArray *)response).count == 0) {
            return;
        }
        self.page += 1;
        NSArray *comments = (NSArray *)response;
        for (NSDictionary *comment in comments) {
            [self.commentArray addObject:[[ProfileCommentModel alloc]initWithJson:comment]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.profileTableView reloadData];
        });
    }];
}

- (IBAction)selecedTableViewCell:(UIButton *)sender {
    ProfileCommentModel *comment = [self.commentArray objectAtIndex:sender.indexPath.section];
    switch (sender.indexPath.row) {
        case profileCommentCell: {
            NSArray *photos = @[[[AXPhoto alloc]initWithAttributedTitle:nil attributedDescription:nil attributedCredit:nil imageData:nil image:nil url:[NSURL URLWithString: comment.cmImg1]]];

            AXPhotosDataSource *dataSource = [[AXPhotosDataSource alloc]initWithPhotos:photos];
                //            AXPagingConfig *config = [[AXPagingConfig alloc]initWithLoadingViewClass:CustomLoadingView.self];
            AXPhotosViewController *vc = [[AXPhotosViewController alloc]initWithDataSource:dataSource];
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
        } break;

        default:
        break;
    }
}


@end
