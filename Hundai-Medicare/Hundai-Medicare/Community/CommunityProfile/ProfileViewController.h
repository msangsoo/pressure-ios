//
//  ProfileViewController.h
//  Hundai-Medicare
//
//  Created by Paul P on 14/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedModel.h"
#import "ProfileModel.h"
#import "ProfileCommentModel.h"
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : BaseViewController



@property (nonatomic, strong)NSString *seq;
@property (nonatomic, strong)NSString *memSeq;
@property (nonatomic, assign) BOOL isNewDataLoading;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *diseaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (nonatomic, strong)NSMutableArray<ProfileCommentModel*> *commentArray;
@property (nonatomic, strong)ProfileModel *profileModel;
@property (weak, nonatomic) IBOutlet UIButton *uploadImageButton;
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) FeedModel *feed;
@property (weak, nonatomic) IBOutlet UIButton *profileModifyButton;

- (IBAction)touchedProfileModifyButton:(UIButton *)sender;
- (IBAction)touchedNickModifyButton:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
