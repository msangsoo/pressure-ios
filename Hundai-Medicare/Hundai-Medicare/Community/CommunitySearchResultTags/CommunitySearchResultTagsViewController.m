//
//  CommunitySearchResultTagsViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 16/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunitySearchResultTagsViewController.h"
#import "CommunitySearchResultArticleTableViewCell.h"
#import "CommunityNetwork.h"
#import "ProfileViewController.h"
#import "FeedDetailViewController.h"
#import "FeedModel.h"

@import SDWebImage;

@interface CommunitySearchResultTagsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@property (nonatomic,strong) NSMutableArray* feedModels;
@property (nonatomic) BOOL isNewDataLoading;
@property (nonatomic) int page;
@property (nonatomic) BOOL isFinishedLoading;

-(void)setNewSearchText:(NSString*)searchKeyword;
@end
@implementation CommunitySearchResultTagsViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setNewSearchText:self.searchKeyword];

}

-(void)setNewSearchText:(NSString*)searchKeyword
{
    self.searchKeyword = searchKeyword;
    self.title = [self.searchKeyword substringFromIndex:1];
    self.page = 1;
    self.feedModels = [@[] mutableCopy];
    [self getSearchFeedList];
}

-(void)getSearchFeedList
{
    NSString *seq = @"";
    NSDictionary *header = [NSDictionary dictionary];
    NSString *type = @"1";

    self.isNewDataLoading = TRUE;
    [CommunityNetwork requestGetFeedList:header seq:seq page:self.page type:type tag:self.searchKeyword search:@"" response:^(id response, BOOL isSuccess) {

        NSLog(@"%@", response);
        self.isNewDataLoading = FALSE;
        //        if ([self.refreshControl isRefreshing]) {
        //            [self.refreshControl endRefreshing];
        //        }
        if(isSuccess == FALSE) {
            return;
        }

        if(((NSArray*)response).count == 0)
        {
            self.isFinishedLoading = YES;
            return;
        }
        for (NSDictionary *feedDic in response) {
            [self.feedModels addObject:[[FeedModel alloc]initWithJson:feedDic]];
        }

        self.page += 1;
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self.tableView reloadData];
            if(self.page == 2)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
                    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];

                });
            }
        });
    }];
}
/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - action method
- (IBAction)close:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedModel* info = self.feedModels[indexPath.row];
    CommunitySearchResultArticleTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"articleCell"];
    //        {
    //            "TPAGE": "6",
    //            "CM_SEQ": "282",
    //            "MBER_SN": "1871",
    //            "NICK": "닉넴임임당",
    //            "PROFILE_PIC": "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/profile/1871_20190322055704.jpg",
    //            "CM_TITLE": "1553230762396",
    //            "CM_CONTENT": "테스트",
    //            "CM_IMG1": "",
    //            "CM_IMG2": "",
    //            "CM_IMG3": "",
    //            "REGDATE": "2019-03-22 오후 1:59:00",
    //            "CM_TAG": [
    //                       "#일상",
    //                       "#혈당"
    //                       ],
    //            "CM_MEAL": "",
    //            "HCNT": "0",
    //            "MYHEART": "N",
    //            "RCNT": "8",
    //            "RNUM": "37",
    //            "MBER_GRAD": "10"
    //        }
    [cell.tagListView removeAllTags];
    for(NSString* tag in info.cmTag)
    {
        TagView* tv = [cell.tagListView addTag:tag];
        tv.onTap = ^(TagView* tv){
            NSString* keyword = [tv titleForState:UIControlStateNormal];
            NSLog(@"%@",keyword);
            [self setNewSearchText:keyword];
        };
        if([self.searchKeyword isEqualToString:tag])
        {
            [tv setTextColor:COLOR_MAIN_ORANGE];
            [tv setTextFont:FONT_NOTO_BOLD(16)];
        }
    }

    cell.userName.text = info.nick;
    cell.writeTime.text = info.regDate;
    cell.article.text = info.cmContent;
    cell.like.text = [NSString stringWithFormat:@"%ld",(long)info.hCnt];
    cell.comment.text = [NSString stringWithFormat:@"%ld",(long)info.rCnt];
    if(info.profilePic != nil && info.profilePic.length != 0)
    {
//        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:info.profilePic]];
        NSString* profileImage = [info.profilePic stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] options:0];
    }
    cell.userProfileImage.layer.borderColor = RGB(219, 219, 219).CGColor;
    cell.userProfileImage.layer.borderWidth = 1.0;
    if ([info.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
        cell.userProfileImage.layer.borderColor = RGB(243, 163, 41).CGColor;
    }
    if(info.cmImg1 != nil && info.cmImg1.length != 0)
    {
        cell.articleImage.hidden = NO;
        NSString* profileImage = [info.cmImg1 stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
        [cell.articleImage sd_setImageWithURL:[NSURL URLWithString:profileImage]];
    }
    else
    {
        cell.articleImage.hidden = YES;
    }
    cell.btnProfile.indexPath = indexPath;

    if(self.isNewDataLoading == NO && self.isFinishedLoading == NO && indexPath.row == self.feedModels.count-1)
    {
        [self getSearchFeedList];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feedModels.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return 135;
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    FeedModel* model = self.feedModels[indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    FeedDetailViewController *vc = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    vc.seq = _model.getLoginData.mber_sn;
    vc.feedSeq = model.cmSeq;
    [self.navigationController pushViewController:vc animated:TRUE];
}

-(IBAction)showProfile:(UIButton*)sender
{
    FeedModel* model = self.feedModels[sender.indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    vc.memSeq = _model.getLoginData.mber_sn;
    vc.seq = model.mberSn;
    [self.navigationController pushViewController:vc animated:TRUE];
}
@end
