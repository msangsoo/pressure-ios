//
//  CommunitySearchResultTagsViewController.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 16/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunitySearchResultTagsViewController : BaseViewController
@property (nonatomic,strong) NSString* searchKeyword;
@end

NS_ASSUME_NONNULL_END
