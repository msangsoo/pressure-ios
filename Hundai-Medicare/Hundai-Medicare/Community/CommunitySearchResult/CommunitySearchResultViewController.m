//
//  CommunitySearchResultViewController.m
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "CommunitySearchResultViewController.h"
#import "CommunitySearchViewController.h"
#import "CommunitySearchResultUserTableViewCell.h"
#import "CommunitySearchResultArticleTableViewCell.h"
#import "CommunitySearchResultTagsViewController.h"
#import "FeedDetailViewController.h"
#import "ProfileViewController.h"
#import "RecentSearchDBWraper.h"
#import "CommunityNetwork.h"
#import "FeedModel.h"

@import SDWebImage;
@import IQKeyboardManager;

@interface CommunitySearchResultViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@property (nonatomic,weak) IBOutlet UITextField* textField;
@property (nonatomic,weak) IBOutlet UIButton* boardSearchResult;
@property (nonatomic,weak) IBOutlet UIButton* userSearchResult;
@property (nonatomic,weak) IBOutlet UIView* boardSearchResult_bottom;
@property (nonatomic,weak) IBOutlet UIView* userSearchResult_bottom;
@property (nonatomic,strong) NSArray* userInfos;
@property (nonatomic,strong) NSMutableArray* feedModels;
@property (nonatomic) BOOL isNewDataLoading;
@property (nonatomic) int page;
@property (nonatomic) BOOL isFinishedLoading;
@end

@implementation CommunitySearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textField.text = self.searchKeyword;
//    _tableView.estimatedRowHeight = 213;
    self.userInfos = @[];
    self.feedModels = [@[] mutableCopy];
    self.page = 1;

    [CommunityNetwork requestSearchListByNick:self.searchKeyword response:^(id response, BOOL isSuccess) {
        self.userInfos = response;
    }];

    [self getSearchFeedList];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

-(void)getSearchFeedList
{
    NSString *seq = @"";
    NSString *tag = @"";
    NSDictionary *header = [NSDictionary dictionary];
    NSString *type = @"1";

    self.isNewDataLoading = TRUE;
    [CommunityNetwork requestGetFeedList:header seq:seq page:self.page type:type tag:tag search:self.searchKeyword response:^(id response, BOOL isSuccess) {

        NSLog(@"%@", response);
        self.isNewDataLoading = FALSE;
//        if ([self.refreshControl isRefreshing]) {
//            [self.refreshControl endRefreshing];
//        }
        if(isSuccess == FALSE) {
            return;
        }

        if(((NSArray*)response).count == 0)
        {
            self.isFinishedLoading = YES;
            if(self.boardSearchResult.selected == YES)
            {
                [self.tableView reloadData];
            }
            return;
        }
        for (NSDictionary *feedDic in response) {
            [self.feedModels addObject:[[FeedModel alloc]initWithJson:feedDic]];
        }

        self.page += 1;
        if(self.boardSearchResult.selected == YES)
        {
            [self.tableView reloadData];
        }
    }];
}

 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:@"resultTagSegue"]){
         CommunitySearchResultTagsViewController* dvc = segue.destinationViewController;
         dvc.searchKeyword = sender;
     }
 }


#pragma mark - action method
- (IBAction)close:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)boardSearchResult:(UIButton*)sender
{
    self.boardSearchResult.selected = YES;
    self.userSearchResult.selected = NO;

    self.boardSearchResult_bottom.backgroundColor = RGB(0x69, 0x81, 0xEC);
    self.userSearchResult_bottom.backgroundColor = RGB(0xBA, 0xBA, 0xBA);

    [self.tableView reloadData];
}

-(IBAction)userSearchResult:(UIButton*)sender
{
    self.boardSearchResult.selected = NO;
    self.userSearchResult.selected = YES;

    self.boardSearchResult_bottom.backgroundColor = RGB(0xBA, 0xBA, 0xBA);
    self.userSearchResult_bottom.backgroundColor = RGB(0x69, 0x81, 0xEC);

    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField.text.length > 0)
    {
//        [self performSegueWithIdentifier:@"resultSegue" sender:textField.text];
        self.searchKeyword = textField.text;
        self.userInfos = @[];
        self.feedModels = [@[] mutableCopy];
        self.page = 1;

        [CommunityNetwork requestSearchListByNick:self.searchKeyword response:^(id response, BOOL isSuccess) {
            self.userInfos = response;
            if(self.userSearchResult.selected == YES)
            {
                [self.tableView reloadData];
            }
        }];

        [self getSearchFeedList];
    }
    return YES;
}

#pragma mark UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.boardSearchResult.selected) // 게시글
    {
        FeedModel* info = self.feedModels[indexPath.row];
        CommunitySearchResultArticleTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"articleCell"];
//        {
//            "TPAGE": "6",
//            "CM_SEQ": "282",
//            "MBER_SN": "1871",
//            "NICK": "닉넴임임당",
//            "PROFILE_PIC": "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/profile/1871_20190322055704.jpg",
//            "CM_TITLE": "1553230762396",
//            "CM_CONTENT": "테스트",
//            "CM_IMG1": "",
//            "CM_IMG2": "",
//            "CM_IMG3": "",
//            "REGDATE": "2019-03-22 오후 1:59:00",
//            "CM_TAG": [
//                       "#일상",
//                       "#혈당"
//                       ],
//            "CM_MEAL": "",
//            "HCNT": "0",
//            "MYHEART": "N",
//            "RCNT": "8",
//            "RNUM": "37",
//            "MBER_GRAD": "10"
//        }
        [cell.tagListView removeAllTags];
        for(NSString* tag in info.cmTag)
        {
            TagView* tv = [cell.tagListView addTag:tag];
            tv.onTap = ^(TagView* tv){
                NSString* keyword = [tv titleForState:UIControlStateNormal];
                NSLog(@"%@",keyword);
                [self performSegueWithIdentifier:@"resultTagSegue" sender:keyword];
            };
        }

        cell.userName.text = info.nick;
        cell.writeTime.text = info.regDate;
        cell.article.text = info.cmContent;
        cell.like.text = [NSString stringWithFormat:@"%ld",(long)info.hCnt];
        cell.comment.text = [NSString stringWithFormat:@"%ld",(long)info.rCnt];
        if(info.profilePic != nil && info.profilePic.length != 0)
        {
//            [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:info.profilePic]];
            NSString* profileImage = [info.profilePic stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
            [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] options:0];
        }
        cell.userProfileImage.layer.borderColor = RGB(219, 219, 219).CGColor;
        cell.userProfileImage.layer.borderWidth = 1.0;
        if ([info.mberGrad isEqualToString:@"10"]) { // 정회원 : "10"
            cell.userProfileImage.layer.borderColor = RGB(243, 163, 41).CGColor;
        }
        if(info.cmImg1 != nil && info.cmImg1.length != 0)
        {
            cell.articleImage.hidden = NO;

        NSString* profileImage = [info.cmImg1 stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
            [cell.articleImage sd_setImageWithURL:[NSURL URLWithString:profileImage]];
        }
        else
        {
            cell.articleImage.hidden = YES;
        }
        if(self.isNewDataLoading == NO && self.isFinishedLoading == NO && indexPath.row == self.feedModels.count-1)
        {
            [self getSearchFeedList];
        }
        cell.btnProfile.indexPath = indexPath;
        return cell;
    }
    else
    {
        NSDictionary* info = self.userInfos[indexPath.row];
        CommunitySearchResultUserTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"userInfoCell"];
        //    {
        //        "MBER_SN": "1871",
        //        "NICKNAME": "닉넴임임당",
        //        "PROFILE_PIC": "https://m.shealthcare.co.kr/HL_MED_COMMUNITY/Upload/profile/1871_20190322055704.jpg",
        //        "MBER_GRAD": "10"
        //    }

        cell.nickNameLabel.text = info[@"NICKNAME"];
        NSString* profileImage = info[@"PROFILE_PIC"];
        if(profileImage != nil && profileImage.length != 0)
        {
            profileImage = [profileImage stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
            [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:[UIImage imageNamed:@"commu_l_03"] options:0];
        }
        cell.profileImageView.layer.borderColor = RGB(219, 219, 219).CGColor;
        cell.profileImageView.layer.borderWidth = 1.0;
        if ([info[@"MBER_GRAD"] isEqualToString:@"10"]) { // 정회원 : "10"
            cell.profileImageView.layer.borderColor = RGB(243, 163, 41).CGColor;
        }

        return cell;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.boardSearchResult.selected) // 게시글
    {
        return self.feedModels.count;
    }
    else // 사용자
    {
        return self.userInfos.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.boardSearchResult.selected) // 게시글
    {
        return 1;
    }
    else // 사용자
    {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.boardSearchResult.selected) // 게시글
    {
        //return 135;
        return UITableViewAutomaticDimension;
    }
    else // 사용자
    {
        return 60;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if(self.boardSearchResult.selected) // 게시글
    {
        FeedModel* model = self.feedModels[indexPath.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        FeedDetailViewController *vc = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
        vc.seq = _model.getLoginData.mber_sn;
        vc.feedSeq = model.cmSeq;
        [self.navigationController pushViewController:vc animated:TRUE];
    }
    else // 사용자
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        vc.memSeq = _model.getLoginData.mber_sn;
        vc.seq = self.userInfos[indexPath.row][@"MBER_SN"];
        [self.navigationController pushViewController:vc animated:TRUE];
    }
}

-(IBAction)showProfile:(UIButton*)sender
{
    FeedModel* model = self.feedModels[sender.indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
    ProfileViewController *vc = (ProfileViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    vc.memSeq = _model.getLoginData.mber_sn;
    vc.seq = model.mberSn;
    [self.navigationController pushViewController:vc animated:TRUE];
}

-(IBAction)cancelSearch:(id)sender
{
    CommunitySearchViewController* csv =  self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2];
    [self.navigationController popViewControllerAnimated:true];
    [csv cancelSearch:sender];
}


@end

