//
//  CommunitySearchResultArticleTableViewCell.m
//  Hundai-Medicare
//
//  Created by Choi Chef on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunitySearchResultArticleTableViewCell.h"

@implementation CommunitySearchResultArticleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat width = self.userProfileImage.bounds.size.width;
    self.userProfileImage.layer.cornerRadius = width/2.0;

    self.articleImage.layer.cornerRadius = 8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
