//
//  CommunitySearchResultArticleTableViewCell.h
//  Hundai-Medicare
//
//  Created by Choi Chef on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"
@import TagListView_ObjC;

NS_ASSUME_NONNULL_BEGIN

@interface CommunitySearchResultArticleTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel* userName;
@property (nonatomic,weak) IBOutlet UILabel* writeTime;
@property (nonatomic,weak) IBOutlet RoundedImageView* userProfileImage;
@property (nonatomic,weak) IBOutlet RoundedImageView* articleImage;
@property (nonatomic,weak) IBOutlet UILabel* article;
@property (nonatomic,weak) IBOutlet TagListView* tagListView;
@property (nonatomic,weak) IBOutlet UILabel* like;
@property (nonatomic,weak) IBOutlet UILabel* comment;
@property (nonatomic,weak) IBOutlet UIButton* btnProfile;

@end

NS_ASSUME_NONNULL_END
