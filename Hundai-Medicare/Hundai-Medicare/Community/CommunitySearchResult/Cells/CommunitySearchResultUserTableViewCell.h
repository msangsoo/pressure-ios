//
//  CommunitySearchResultUserTableViewCell.h
//  Hundai-Medicare
//
//  Created by Choi Chef on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommunitySearchResultUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@end

NS_ASSUME_NONNULL_END
