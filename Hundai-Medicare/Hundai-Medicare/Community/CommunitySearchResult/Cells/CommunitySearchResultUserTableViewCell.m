//
//  CommunitySearchResultUserTableViewCell.m
//  Hundai-Medicare
//
//  Created by Choi Chef on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommunitySearchResultUserTableViewCell.h"

@implementation CommunitySearchResultUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    CGFloat width = self.profileImageView.bounds.size.width;

    self.profileImageView.layer.cornerRadius = width/2.0;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:220/255 green:220/255 blue:220/255 alpha:1.0].CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
