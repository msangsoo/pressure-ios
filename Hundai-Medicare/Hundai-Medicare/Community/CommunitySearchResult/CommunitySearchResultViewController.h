//
//  CommunitySearchResult.h
//  Hundai-Medicare
//
//  Created by YuriHan. on 07/03/2019.
//  Copyright © 2019 DreamFactory. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunitySearchResultViewController : BaseViewController
@property (nonatomic,strong) NSString* searchKeyword;
@end

NS_ASSUME_NONNULL_END
