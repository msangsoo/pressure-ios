//
//  RoundedImageView.m
//  Hundai-Medicare
//
//  Created by Choi Chef on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "RoundedImageView.h"

@implementation RoundedImageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        self.borderWidth = 0;
        self.radius = 0;
        self.borderColor = UIColor.blackColor;

        [self customInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self customInit];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}


- (void)prepareForInterfaceBuilder {

    [self customInit];
}

- (void)customInit {

    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.radius;
    self.layer.borderWidth = self.borderWidth;

    if (self.radius > 0) {
        self.layer.masksToBounds = YES;
    }

    if (self.borderWidth > 0)
    {
        self.layer.borderColor = self.borderColor.CGColor;
    }

}

@end
