//
//  RoundedImageView.h
//  Hundai-Medicare
//
//  Created by Choi Chef on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface RoundedImageView : UIImageView
@property (nonatomic,assign) IBInspectable CGFloat borderWidth;
@property (nonatomic,assign) IBInspectable CGFloat radius;
@property (nonatomic,copy) IBInspectable UIColor* borderColor;
@end

NS_ASSUME_NONNULL_END
