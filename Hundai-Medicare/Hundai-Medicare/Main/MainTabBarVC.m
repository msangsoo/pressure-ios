//
//  MainTabBarVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CaffeineDataStore.h"
#import "MainTabBarVC.h"

@interface MainTabBarVC ()
{
    int static_total_step;
    UIBarButtonItem *naviLeftBtn;
    UIView *bubbleView;
}
@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;
@end

@implementation MainTabBarVC {
    UIView *oneVw;
    UIView *twoVw;
    UIView *threeVw;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TabBarMove:) name:@"TabBarMove" object:nil];
    
    _model = [Model instance];
    server = [[ServerCommunication alloc] init];
    
    oneVw = [[UIView alloc] initWithFrame:CGRectMake((UIScreen.mainScreen.bounds.size.width / 3) * 0, 0, UIScreen.mainScreen.bounds.size.width / 3, 3)];
    [oneVw setBackgroundColor:RGB(108, 131, 229)];
    [oneVw setHidden:false];
    [self.tabBar addSubview:oneVw];
    
    twoVw = [[UIView alloc] initWithFrame:CGRectMake((UIScreen.mainScreen.bounds.size.width / 3) * 1, 0, UIScreen.mainScreen.bounds.size.width / 3, 3)];
    [twoVw setBackgroundColor:RGB(108, 131, 229)];
    [twoVw setHidden:true];
    [self.tabBar addSubview:twoVw];
    
    threeVw = [[UIView alloc] initWithFrame:CGRectMake((UIScreen.mainScreen.bounds.size.width / 3) * 2, 0, UIScreen.mainScreen.bounds.size.width / 3, 3)];
    [threeVw setBackgroundColor:RGB(108, 131, 229)];
    [threeVw setHidden:true];
    [self.tabBar addSubview:threeVw];

    
    // let's create several View Controllers with different colours each
    UIViewController *vc1 = [self createViewcontrollerWithColor:[UIColor clearColor] viewIdentifier:@"FoodNC"];
    vc1.view.tag = 0;
    UIViewController *vc2 = [self createViewcontrollerWithColor:[UIColor clearColor] viewIdentifier:@"PresuNC"];
    vc2.view.tag = 1;
    UIViewController *vc3 = [self createViewcontrollerWithColor:[UIColor clearColor] viewIdentifier:@"MyNC"];
    vc3.view.tag = 2;

    UITabBarItem *tabBarItem1, *tabBarItem2, *tabBarItem3;
    
    
    tabBarItem1 = [[UITabBarItem alloc] init];
    tabBarItem1.tag = 0;
    tabBarItem1.title = @"식사";
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"tap_btn_01ov"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem1.image = [[UIImage imageNamed:@"tap_btn_01"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem2 = [[UITabBarItem alloc] init];
    tabBarItem2.tag = 1;
    tabBarItem2.title = @"혈압";
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"tap_btn_02ov"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.image = [[UIImage imageNamed:@"tap_btn_02"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarItem3 = [[UITabBarItem alloc] init];
    tabBarItem3.tag = 2;
    tabBarItem3.title = @"프로필";
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"tap_btn_03ov"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.image = [[UIImage imageNamed:@"tap_btn_03"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    

    vc1.tabBarItem = tabBarItem1;
    vc2.tabBarItem = tabBarItem2;
    vc3.tabBarItem = tabBarItem3;

    NSArray *controllers;
    controllers = @[vc1, vc2, vc3];
    
    [self setViewControllers:controllers animated:YES];

//    [self LogCheck];
}

- (UIViewController *)createViewcontrollerWithColor:(UIColor *)color viewIdentifier:(NSString *)viewIdentifier{

    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:viewIdentifier];
    vc.view.backgroundColor = color;
    
    return vc;
}

- (UIViewController *)createViewcontrollerAndStoryboardWithColor:(NSString *)name color:(UIColor *)color viewIdentifier:(NSString *)viewIdentifier {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:name bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:viewIdentifier];
    vc.view.backgroundColor = color;
    return vc;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    NSLog(@"tabBarController viewController  = %ld", viewController.view.tag);
    
    int tag = (int)viewController.view.tag;
    
    [oneVw setHidden:true];
    [twoVw setHidden:true];
    [threeVw setHidden:true];
    
    switch (tag) {
        case 0:
            [oneVw setHidden:false];
            break;
        case 1:
            [twoVw setHidden:false];
            break;
        case 2:
            [threeVw setHidden:false];
            break;
        default:
            break;
    }

    
    
    
    return true;
}


#pragma mark - NSNotificationCenter
- (void)TabBarMove:(NSNotification *)noti {
    int index = [noti.object[@"index"] intValue];

    [self setSelectedIndex:index];
    
    [oneVw setHidden:true];
    [twoVw setHidden:true];
    [threeVw setHidden:true];

    
    switch (index) {
        case 0:
            [oneVw setHidden:false];
            break;
        case 1:
            [twoVw setHidden:false];
            break;
        case 2:
            [threeVw setHidden:false];
            break;
        default:
            break;
    }
}

#pragma mark - ServerCommunicationDelegate
#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}



@end
