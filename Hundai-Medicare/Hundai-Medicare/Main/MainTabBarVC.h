//
//  MainTabBarVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HealthMainVC.h"
#import "MedicareMainVC.h"
#import "MyMainVC.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "ServerCommunication.h"
#import "WelcomAlertViewController.h"

#import "Model.h"

@interface MainTabBarVC : UITabBarController<UITabBarDelegate, UITabBarControllerDelegate, ServerCommunicationDelegate> {
    Model *_model;
    ServerCommunication *server;
}

@end
