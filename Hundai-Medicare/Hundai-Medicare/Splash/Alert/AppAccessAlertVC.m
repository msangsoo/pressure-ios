//
//  AppAccessAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 22/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "AppAccessAlertVC.h"

@interface AppAccessAlertVC ()

@end

@implementation AppAccessAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    [self dismiss:^{
        self.confirmAction();
    }];
}

- (IBAction)pressCancel:(UIButton *)sender {
    [self dismiss:^{
        exit(-1);
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_BOLD(24);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"앱 접근권한 안내"];
    
    _msgLbl.font = FONT_NOTO_REGULAR(16);
    _msgLbl.textColor = COLOR_GRAY_SUIT;
    _msgLbl.numberOfLines = 3;
    [_msgLbl setNotoText:@"아래와 같이 안내해 드립니다.\n선택적 접근권한의 경우 허용에 동의하지 않으셔도 앱 사용이 가능합니다."];
    
    _NecessaryLbl.font = FONT_NOTO_REGULAR(18);
    _NecessaryLbl.textColor = COLOR_NATIONS_BLUE;
    [_NecessaryLbl setNotoText:@"필수적 접근권한"];;
    
    _NecessaryInfoLbl.font = FONT_NOTO_REGULAR(16);
    _NecessaryInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_NecessaryInfoLbl setNotoText:@"애플 건강 걸음 수(걸음 수 카운트)"];
    
    _selectLbl.font = FONT_NOTO_REGULAR(18);
    _selectLbl.textColor = COLOR_NATIONS_BLUE;
    [_selectLbl setNotoText:@"선택적 접근권한"];
    
    _selectInfoLbl.font = FONT_NOTO_REGULAR(16);
    _selectInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_selectInfoLbl setNotoText:@"사진첩 및 카메라(식사 사진 등록)"];
    
    [_exitBtn setTitle:@"종료" forState:UIControlStateNormal];
    [_exitBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

@end
