//
//  AppAccessAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 22/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppAccessAlertVC : UIViewController

@property (copy) void (^confirmAction)(void);

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@property (weak, nonatomic) IBOutlet UILabel *NecessaryLbl;
@property (weak, nonatomic) IBOutlet UILabel *NecessaryInfoLbl;

@property (weak, nonatomic) IBOutlet UILabel *selectLbl;
@property (weak, nonatomic) IBOutlet UILabel *selectInfoLbl;

@property (weak, nonatomic) IBOutlet UIButton *exitBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end
