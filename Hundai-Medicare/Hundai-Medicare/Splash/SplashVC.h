//
//  SplashVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

#import "AppAccessAlertVC.h"
#import "CaffeineDataStore.h"
#import "MedicareDataBase.h"
#import "FoodCalorieDBWraper.h"
#import "TimeUtils.h"

@interface SplashVC : BaseViewController <UNUserNotificationCenterDelegate, ServerCommunicationDelegate> {
    MedicareDataBase *mediDb;
    FoodCalorieDBWraper *foodDb;
    CaffeineDataStore *caffeineDataStore;
}

@end
