//
//  SplashVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SplashVC.h"

@interface SplashVC () {
    BOOL didBackCheck;
}

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //초기화 해야할꺼
    [Utils setUserDefault:HEALTH_POINT_ALERT value:@"0"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidBeacome)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    
    didBackCheck = false;
    
    mediDb = [[MedicareDataBase alloc] init];
    foodDb = [[FoodCalorieDBWraper alloc] init];
    
    [self pageMoveSet];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - noticationcenter method
- (void)notificationDidBeacome {
    if (didBackCheck) {
        didBackCheck = false;
        
    }
}

- (void)notificationDidBackground {
    didBackCheck = true;
}


#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
}

#pragma mark - viewPageMove
- (void)pageMoveSet {
//    if ([[Utils getUserDefault:AUTO_LOGIN] isEqualToString:@"Y"]) {
        [self goLogin];
//    }
}

- (void)goLogin {
    UIViewController *vc = MAIN_STORYBOARD.instantiateInitialViewController;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - viewInit
- (void)viewInit {
    
}

@end
