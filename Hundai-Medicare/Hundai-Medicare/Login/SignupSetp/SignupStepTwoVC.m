//
//  SignupStepTwoVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SignupStepTwoVC.h"

@interface SignupStepTwoVC () {
    NSString *mber_actqy;
    NSString *disease_nm;
    NSString *disease_txt;
    NSString *smkng_yn;
    NSString *sugar_typ;
    NSString *job_yn;
}

@end

@implementation SignupStepTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.strTitle){
        [self.navigationItem setTitle:self.strTitle];
    }else{
        [self.navigationItem setTitle:@"기본정보입력(2/2)"];
    }
    
    [self viewInit];
    
    if (self.strTitle) {
        [self viewEdit];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}

-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"login_add_aft"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            Tr_login *login = [_model getLoginData];
            
            login.mber_height = _mber_height;
            login.mber_bdwgh = _mber_bdwgh;
            login.mber_bdwgh_goal = _mber_bdwgh_goal;
            
            login.mber_actqy = mber_actqy;
            login.disease_nm = disease_nm;
            login.disease_txt = disease_txt;
            login.smkng_yn = smkng_yn;
            login.sugar_typ = sugar_typ;
            login.job_yn = job_yn;
            
            [Utils basicAlertView:self withTitle:@"" withMsg:@"메디케어서비스 앱 회원이 되신것을 환영합니다!!" completion:^{
                [self apiLogin];
            }];
        }else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"다시 시도해 주세요."];
        }
        
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"login"]) {
        if ([[result objectForKey:@"log_yn"] isEqualToString:@"Y"]) {
            [_model setLoginData:result];
            
            //전체 데어터 삭제
            MedicareDataBase *mediDB = [[MedicareDataBase alloc] init];
            [mediDB allTableDelete];
            
            [self userDefaultInit];
            
            // 메인으로 이동
            UIViewController *vc = MAIN_STORYBOARD.instantiateInitialViewController;
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:vc animated:true];
        }
    }
}

- (void)apiLogin {
    
    if ([Utils getUserDefault:USER_ID] == nil
        || [[Utils getUserDefault:USER_ID] length]==0) {
        return;
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"login", @"api_code",
                                [Utils getUserDefault:USER_ID], @"mber_id",
                                [Utils getUserDefault:USER_PASS], @"mber_pwd",
                                DIVECE_MODEL, @"phone_model",
                                APP_VERSION, @"app_ver",
                                @"", @"pushk",
                                [Utils getUserDefault:APP_TOKEN], @"token",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}


#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    
}

#pragma mark - Actions
- (IBAction)pressActqy:(UIButton *)sender {
    _actqy1Btn.selected = false;
    _actqy2Btn.selected = false;
    _actqy3Btn.selected = false;
    if (sender.tag == 1) {
        _actqy1Btn.selected = true;
    }else if (sender.tag == 2) {
        _actqy2Btn.selected = true;
    }else if (sender.tag == 3) {
        _actqy3Btn.selected = true;
    }
    
    [self nextVailCheck];
}

- (IBAction)pressJob:(UIButton *)sender {
    _jobYesBtn.selected = false;
    _jobNoBtn.selected = false;
    
    if (sender.tag == 1) {
        _jobYesBtn.selected = true;
    }else if (sender.tag == 2) {
        _jobNoBtn.selected = true;
    }
    
    [self nextVailCheck];
}

- (IBAction)pressDisease:(UIButton *)sender {
    _disease1Btn.selected = false;
    _disease2Btn.selected = false;
    _disease3Btn.selected = false;
    _disease4Btn.selected = false;
    _disease5Btn.selected = false;
    _disease6Btn.selected = false;
    _diseaseTf.hidden = true;
    
    if (sender.tag == 1) {
        _disease1Btn.selected = true;
    }else if (sender.tag == 2) {
        _disease2Btn.selected = true;
    }else if (sender.tag == 3) {
        _disease3Btn.selected = true;
    }else if (sender.tag == 4) {
        _disease4Btn.selected = true;
    }else if (sender.tag == 5) {
        _disease5Btn.selected = true;
    }else if (sender.tag == 6) {
        _disease6Btn.selected = true;
        _diseaseTf.hidden = false;
    }
    
    [self nextVailCheck];
}

- (IBAction)pressSmoke:(UIButton *)sender {
    _smokeYesBtn.selected = false;
    _smokeNoBtn.selected = false;
    
    if (sender.tag == 1) {
        _smokeYesBtn.selected = true;
    }else if (sender.tag == 2) {
        _smokeNoBtn.selected = true;
    }
    
    [self nextVailCheck];
    
}

- (IBAction)pressNext:(id)sender {
    if (self.disease6Btn.isSelected) {
        if ([self.diseaseTf.text length] < 1) {
            [AlertUtils BasicAlertShow:self tag:99 title:@"" msg:@"보유질환을 입력해주세요." left:@"확인" right:@""];
            return;
        }
    }
    if (_nextBtn.isSelected) {
        [self apiLoginAdd];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:true];
    return true;
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self nextVailCheck];
}

#pragma mark - vaildate
- (void)nextVailCheck {
    
    _nextBtn.selected = false;
    _nextBtn.userInteractionEnabled = false;
    
    if (!_actqy1Btn.isSelected && !_actqy2Btn.isSelected && !_actqy3Btn.isSelected) {
        return;
    }
    
    if (!_jobYesBtn.isSelected && !_jobNoBtn.isSelected) {
        return;
    }
    
    if (!_disease1Btn.isSelected && !_disease2Btn.isSelected && !_disease3Btn.isSelected && !_disease4Btn.isSelected && !_disease5Btn.isSelected && !_disease6Btn.isSelected ) {
        return;
    }
    
    if (!_smokeYesBtn.isSelected && !_smokeNoBtn.isSelected) {
        return;
    }
    
    if (_disease6Btn.isSelected && (!_diseaseTf.hidden && [_diseaseTf.text length] == 0)) {
        return;
    }
    
    _nextBtn.userInteractionEnabled = true;
    _nextBtn.selected = true;
}

#pragma mark - user userdefault init
- (void)userDefaultInit {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *alarmListData = [defaults objectForKey:ALARM_LIST_DATA];
    NSMutableArray *items = [NSKeyedUnarchiver unarchiveObjectWithData:alarmListData];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    
    for (AlarmObject *alarm in items) {
        for (UILocalNotification *event in eventArray) {
            NSDictionary *userInfoCurrent = event.userInfo;
            NSString *uid = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
            if ([uid isEqualToString:[NSString stringWithFormat:@"%i", alarm.notificationID]]) {
                [app cancelLocalNotification:event];
            }
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:MAIN_NOTI_IDX];
    [userDefault removeObjectForKey:MAIN_NOTI_COMFIRM];
    [userDefault removeObjectForKey:ALARM_LIST_DATA];
    
    [userDefault removeObjectForKey:HEALTH_MSG_NEW];
    [userDefault removeObjectForKey:HEALTH_POINT_ALERT];
    [userDefault removeObjectForKey:HEALTH_TIP_DATA];
    [userDefault removeObjectForKey:HEALTH_WEIGHT_TIP_TODAY_TIME];
    [userDefault removeObjectForKey:HEALTH_PRESU_TIP_TODAY_TIME];
    [userDefault removeObjectForKey:HEALTH_SUGAR_TIP_TODAY_TIME];
    
    [userDefault removeObjectForKey:PRESU_AFTER_TIME];
    [userDefault removeObjectForKey:PRESU_AFTER_COUNT];
    [userDefault removeObjectForKey:PRESU_ONESTEP_TIME];
    [userDefault removeObjectForKey:PRESU_ONESTEP_COUNT];
    [userDefault removeObjectForKey:PRESU_TWOSTEP_TIME];
    [userDefault removeObjectForKey:PRESU_TWOSTEP_COUNT];
    [userDefault removeObjectForKey:PRESU_LAST_SAVE_TIME];
    [userDefault removeObjectForKey:PRESU_DAY_FIRST];
    
    [userDefault removeObjectForKey:@"Food_Lately_Input"];
    
    [userDefault synchronize];
}

#pragma mark - api
- (void)apiLoginAdd {
    
    [Utils setUserDefault:AUTO_LOGIN value:@"Y"];
    
    mber_actqy = @"1";
    disease_nm = @"5";
    disease_txt = @"";
    smkng_yn = @"1";
    sugar_typ = @"";
    job_yn = @"1";
    
    if (_actqy2Btn.isSelected) {
        mber_actqy = @"2";
    }else if (_actqy3Btn.isSelected) {
        mber_actqy = @"3";
    }
    
    if (_jobNoBtn.isSelected) {
        job_yn = @"2";
    }

    if (_disease1Btn.isSelected) {
        disease_nm = @"1";
    }else if (_disease2Btn.isSelected) {
        disease_nm = @"2";
    }else if (_disease3Btn.isSelected) {
        disease_nm = @"3";
    }else if (_disease4Btn.isSelected) {
        disease_nm = @"4";
    }else if (_disease6Btn.isSelected) {
        disease_nm = @"6";
        disease_txt = _diseaseTf.text;
    }
    
    if (_smokeNoBtn.isSelected) {
        smkng_yn = @"2";
    }

    Tr_login *login = [_model getLoginData];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"login_add_aft", @"api_code",
                                _mber_height, @"mber_height",
                                _mber_bdwgh, @"mber_bdwgh",
                                _mber_bdwgh_goal, @"mber_bdwgh_goal",
                                mber_actqy, @"mber_actqy",
                                disease_nm, @"disease_nm",
                                disease_txt, @"disease_txt",
                                smkng_yn, @"smkng_yn",
                                sugar_typ, @"sugar_typ",
                                job_yn, @"job_yn",
                                login.mber_sn, @"mber_sn",
                                [Utils getUserDefault:USER_ID], @"mbar_id",
                                APP_VERSION, @"app_ver",
                                DIVECE_MODEL, @"phone_model",
                                [_model getToken], @"token",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _actqyLbl.font = FONT_NOTO_MEDIUM(18);
    _actqyLbl.textColor = COLOR_NATIONS_BLUE;
    [_actqyLbl setNotoText:@"활동량 선택"];
    
    _actqy1Btn.tag = 1;
    _actqy1Btn.titleLabel.numberOfLines = 3;
    _actqy1Btn.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [_actqy1Btn setAttributedTitle:[self actqyString:@"주로 앉아 있음" msg:@"하루 중 대부분을 앉아서 보내며 운동량이 거의 없음" type: 0] forState:UIControlStateNormal];
    [_actqy1Btn setAttributedTitle:[self actqyString:@"주로 앉아 있음" msg:@"하루 중 대부분을 앉아서 보내며 운동량이 거의 없음" type: 1] forState:UIControlStateSelected];
    [_actqy1Btn setBackgroundImage:[UIImage setBackgroundImageByColor:[UIColor whiteColor] withFrame:_actqy1Btn.bounds] forState:UIControlStateNormal];
    [_actqy1Btn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_actqy1Btn.bounds] forState:UIControlStateSelected];
    
    _actqy2Btn.tag = 2;
    _actqy2Btn.titleLabel.numberOfLines = 3;
    _actqy2Btn.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [_actqy2Btn setAttributedTitle:[self actqyString:@"약간 활동적" msg:@"하루중 상당 부분 서서 보내거나(교사, 판매원) 가벼운 운동을 꾸준히 하는편                          " type: 0] forState:UIControlStateNormal];
    [_actqy2Btn setAttributedTitle:[self actqyString:@"약간 활동적" msg:@"하루중 상당 부분 서서 보내거나(교사, 판매원) 가벼운 운동을 꾸준히 하는편                          " type: 1] forState:UIControlStateSelected];
    [_actqy2Btn setBackgroundImage:[UIImage setBackgroundImageByColor:[UIColor whiteColor] withFrame:_actqy2Btn.bounds] forState:UIControlStateNormal];
    [_actqy2Btn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_actqy2Btn.bounds] forState:UIControlStateSelected];
    
    _actqy3Btn.tag = 3;
    _actqy3Btn.titleLabel.numberOfLines = 3;
    _actqy3Btn.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [_actqy3Btn setAttributedTitle:[self actqyString:@"활동적" msg:@"하루 중 상당 부분을 신체 활동을 하면서 보내거나 (트레이너, 우체부)강도 높은 운동을 꾸준히 함" type: 0] forState:UIControlStateNormal];
    [_actqy3Btn setAttributedTitle:[self actqyString:@"활동적" msg:@"하루 중 상당 부분을 신체 활동을 하면서 보내거나 (트레이너, 우체부)강도 높은 운동을 꾸준히 함" type: 1] forState:UIControlStateSelected];
    [_actqy3Btn setBackgroundImage:[UIImage setBackgroundImageByColor:[UIColor whiteColor] withFrame:_actqy3Btn.bounds] forState:UIControlStateNormal];
    [_actqy3Btn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_actqy3Btn.bounds] forState:UIControlStateSelected];
    
    ///////////////
    /// JOB
    /////////////////
    _jobLbl.font = FONT_NOTO_MEDIUM(18);
    _jobLbl.textColor = COLOR_NATIONS_BLUE;
    [_jobLbl setNotoText:@"직업"];
    
    _jobYesBtn.tag = 1;
    _jobYesBtn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_jobYesBtn setTitle:@"  있음" forState:UIControlStateNormal];
    [_jobYesBtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _jobNoBtn.tag = 2;
    _jobNoBtn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_jobNoBtn setTitle:@"  없음" forState:UIControlStateNormal];
    [_jobNoBtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    ////////////////
    /// disease
    ///////////////
    _diseaseLbl.font = FONT_NOTO_MEDIUM(18);
    _diseaseLbl.textColor = COLOR_NATIONS_BLUE;
    [_diseaseLbl setNotoText:@"보유질환"];
    
    _disease1Btn.tag = 1;
    _disease1Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease1Btn setTitle:@"  고혈압" forState:UIControlStateNormal];
    [_disease1Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _disease2Btn.tag = 2;
    _disease2Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease2Btn setTitle:@"  당뇨" forState:UIControlStateNormal];
    [_disease2Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _disease3Btn.tag = 3;
    _disease3Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease3Btn setTitle:@"  고지혈증" forState:UIControlStateNormal];
    [_disease3Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _disease4Btn.tag = 4;
    _disease4Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease4Btn setTitle:@"  비만    " forState:UIControlStateNormal];
    [_disease4Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _disease5Btn.tag = 5;
    _disease5Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease5Btn setTitle:@"  없음" forState:UIControlStateNormal];
    [_disease5Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _disease6Btn.tag = 6;
    _disease6Btn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_disease6Btn setTitle:@"  기타        " forState:UIControlStateNormal];
    [_disease6Btn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _diseaseTf.layer.borderWidth = 1;
    _diseaseTf.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    [_diseaseTf setPlaceholder:@"질환명을 입력해주세요."];
    _diseaseTf.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 30)];
    _diseaseTf.hidden = true;
    _diseaseTf.delegate = self;
    [_diseaseTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    ///////////////
    /// SMOKE
    /////////////////
    _smokeLbl.font = FONT_NOTO_MEDIUM(18);
    _smokeLbl.textColor = COLOR_NATIONS_BLUE;
    [_smokeLbl setNotoText:@"현재 흡연 하십니까?"];
    
    _smokeYesBtn.tag = 1;
    _smokeYesBtn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_smokeYesBtn setTitle:@"  네" forState:UIControlStateNormal];
    [_smokeYesBtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    _smokeNoBtn.tag = 2;
    _smokeNoBtn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_smokeNoBtn setTitle:@"  아니요" forState:UIControlStateNormal];
    [_smokeNoBtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    
    
    //Default
    _actqy1Btn.selected = true;
    _jobYesBtn.selected = true;
    _disease5Btn.selected = true;
    _smokeNoBtn.selected = true;
    _nextBtn.selected = true;
    
    ////////////////
    /// next
    ///////////////
    [_nextBtn setTitle:@"다  음" forState:UIControlStateNormal];
    [_nextBtn mediBaseButton];
    
}


// 수정일대만 실행
- (void)viewEdit {
    
    _actqy1Btn.selected = NO;
    _actqy2Btn.selected = NO;
    _actqy3Btn.selected = NO;
    
    _jobYesBtn.selected = NO;
    _jobNoBtn.selected = NO;
    
    _disease1Btn.selected = NO;
    _disease2Btn.selected = NO;
    _disease3Btn.selected = NO;
    _disease4Btn.selected = NO;
    _disease5Btn.selected = NO;
    _disease6Btn.selected = NO;
    _diseaseTf.hidden = YES;
    
    _smokeYesBtn.selected = NO;
    _smokeNoBtn.selected = NO;
    
    if([self.strActqy isEqualToString:@"1"]){
        _actqy1Btn.selected = YES;
    }else if([self.strActqy isEqualToString:@"2"]){
        _actqy2Btn.selected = YES;
    }else if([self.strActqy isEqualToString:@"3"]){
        _actqy3Btn.selected = YES;
    }
    
    if ([self.strJobYN isEqualToString:@"Y"]) {
        _jobYesBtn.selected = YES;
    }else {
        _jobNoBtn.selected = YES;
    }
    
    if (self.strDiseaseNm) {
        NSArray *arrDiseaseNm = [self.strDiseaseNm componentsSeparatedByString:@","];
        for (int i=0; i < arrDiseaseNm.count; i++) {
            NSString *trDisese = [[arrDiseaseNm objectAtIndex:i] stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([trDisese isEqualToString:@"1"]) {
                _disease1Btn.selected = YES;
            }else if ([trDisese isEqualToString:@"2"]) {
                _disease2Btn.selected = YES;
            }else if ([trDisese isEqualToString:@"3"]) {
                _disease3Btn.selected = YES;
            }else if ([trDisese isEqualToString:@"4"]) {
                _disease4Btn.selected = YES;
            }else if ([trDisese isEqualToString:@"5"]) {
                _disease5Btn.selected = YES;
            }else if ([trDisese isEqualToString:@"6"]) {
                _disease6Btn.selected = YES;
                _diseaseTf.hidden = NO;
                _diseaseTf.text = self.strDiseaseTxt;
            }
        }
    }
    
    if ([self.strSmoke isEqualToString:@"Y"]) {
        _smokeYesBtn.selected = YES;
    }else {
        _smokeNoBtn.selected = YES;
    }
    
    [_nextBtn setTitle:@"완  료" forState:UIControlStateNormal];
    [_nextBtn addTarget:self
                action:@selector(eventsEdit:)
      forControlEvents:UIControlEventTouchUpInside];
}
// 수정시 콜백
- (void)eventsEdit:(id)sender {
    
    if(self.delegate){
        
        NSString * Actqy = @"";
        NSString * JobYN = @"";
        NSString * DiseaseNm = @"";
        NSString * DiseaseTxt = @"";
        NSString * Smoke = @"";
        
        Actqy       = _actqy1Btn.selected?@"1":_actqy2Btn.selected?@"2":@"3";
        JobYN       = _jobYesBtn.selected?@"Y":@"N";
        DiseaseTxt  = _diseaseTf.text;
        Smoke       = _smokeYesBtn.selected?@"Y":@"N";
        
        if (_disease1Btn.selected) {
            DiseaseNm = @"1";
        }
        else if (_disease2Btn.selected) {
            DiseaseNm = @"2";
        }
        else if (_disease3Btn.selected) {
            DiseaseNm = @"3";
        }
        else if (_disease4Btn.selected) {
            DiseaseNm = @"4";
        }
        else if (_disease5Btn.selected) {
            DiseaseNm = @"5";
        }
        else if (_disease6Btn.selected) {
            DiseaseNm = @"6";
        }
        
        // 전달.
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                             Actqy, @"actqy",
                             JobYN, @"job_yn",
                             DiseaseTxt, @"disease_txt",
                             DiseaseNm, @"disease_nm",
                             Smoke, @"smkng_yn",
                             nil];
        [self.delegate SignupStepEditDone:dic];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
 

#pragma mark - string method
- (NSMutableAttributedString *)actqyString:(NSString *)title msg:(NSString *)msg type:(int)type {
    CGFloat number = -1.5f;
    
    UIColor *titleColor = COLOR_MAIN_DARK;
    UIColor *msgColor = COLOR_GRAY_SUIT;
    if (type == 1) {
        titleColor = [UIColor whiteColor];
        msgColor = [UIColor whiteColor];
    }
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:title];
    [titleStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, title.length)];
    [titleStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_MEDIUM(16)
                     range:NSMakeRange(0, title.length)];
    [titleStr addAttribute:NSForegroundColorAttributeName
                     value:titleColor
                     range:NSMakeRange(0, title.length)];
    [attstr appendAttributedString:titleStr];
    
    NSMutableAttributedString *spaceStr = [[NSMutableAttributedString alloc] initWithString:@"\n"];
    [attstr appendAttributedString:spaceStr];
    
    NSMutableAttributedString *msgStr = [[NSMutableAttributedString alloc] initWithString:msg];
    [msgStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, msg.length)];
    [msgStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(14)
                     range:NSMakeRange(0, msg.length)];
    [msgStr addAttribute:NSForegroundColorAttributeName
                     value:msgColor
                     range:NSMakeRange(0, msg.length)];
    [attstr appendAttributedString:msgStr];
    
    if ([title isEqualToString:@"주로 앉아 있음"]) {
        UIColor *msg2Color = [UIColor whiteColor];
        if (type == 1) {
            msg2Color = COLOR_NATIONS_BLUE;
        }
        
        NSMutableAttributedString *msgStr2 = [[NSMutableAttributedString alloc] initWithString:msg];
        [msgStr2 addAttribute:NSKernAttributeName
                       value:[NSNumber numberWithFloat:number]
                       range:NSMakeRange(0, msg.length)];
        [msgStr2 addAttribute:NSFontAttributeName
                       value:FONT_NOTO_REGULAR(14)
                       range:NSMakeRange(0, msg.length)];
        [msgStr2 addAttribute:NSForegroundColorAttributeName
                       value:msg2Color
                       range:NSMakeRange(0, msg.length)];
        [attstr appendAttributedString:msgStr2];
        [attstr appendAttributedString:msgStr2];
    }
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentLeft];
    [attstr addAttribute:NSParagraphStyleAttributeName
                   value:style
                   range:NSMakeRange(0, attstr.length)];
    
    return attstr;
}

@end
