//
//  SignupStepOneVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SignupStepOneVC.h"

@interface SignupStepOneVC ()

@end

@implementation SignupStepOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"기본정보입력(1/2)"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - Actions
- (IBAction)pressNext:(UIButton *)sender {
    if (_nextBtn.isSelected) {
        SignupStepTwoVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupStepTwoVC"];
        vc.mber_height = _heightTf.text;
        vc.mber_bdwgh = _weightTf.text;
        vc.mber_bdwgh_goal = _targetTf.text;
        [self.navigationController pushViewController:vc animated:true];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _heightTf) {
        if(range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        NSRange subRange = [_heightTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *weightArr = [_heightTf.text componentsSeparatedByString:@"."];
            NSString *weightIndex = [weightArr objectAtIndex:1];
            if(weightIndex.length > 0 && ![string isEqualToString:@""]) {
                return NO;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
    }else if(textField == _weightTf){
        if(range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        NSRange subRange = [_weightTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *weightArr = [_weightTf.text componentsSeparatedByString:@"."];
            NSString *weightIndex = [weightArr objectAtIndex:1];
            if(weightIndex.length > 1 && ![string isEqualToString:@""]) {
                return NO;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
    }else if(textField == _targetTf){
        if(range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        NSRange subRange = [_targetTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *weightArr = [_targetTf.text componentsSeparatedByString:@"."];
            NSString *weightIndex = [weightArr objectAtIndex:1];
            if(weightIndex.length > 1 && ![string isEqualToString:@""]) {
                return NO;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self nextBtnVaildate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:true];
    return true;
}

#pragma mark - vaildate
- (void)nextBtnVaildate {
    
    _nextBtn.userInteractionEnabled = false;
    _nextBtn.selected = false;
    
    if (
        [_heightTf.text isEqualToString:@""] ||
        [_heightTf.text intValue] < 90 ||
         [_heightTf.text intValue] > 230) {
        _nextBtn.selected = false;
        return;
    }
    
    if ([_weightTf.text isEqualToString:@""] ||
        [_weightTf.text intValue] < 30 ||
         [_weightTf.text intValue] > 200) {
        _nextBtn.selected = false;
        return;
    }
    
    if ([_targetTf.text isEqualToString:@""] ||
        [_targetTf.text intValue] < 30 ||
         [_targetTf.text intValue] > 200) {
        _nextBtn.selected = false;
        return;
    }
    
    _nextBtn.userInteractionEnabled = true;
    _nextBtn.selected = true;
}

#pragma mark - viewInit
- (void)viewInit {
    _wordLbl.numberOfLines = 3;
    _wordLbl.font = FONT_NOTO_REGULAR(18);
    _wordLbl.textColor = COLOR_NATIONS_BLUE;
    [_wordLbl setNotoText:@"앱 이용에 필요한 기본정 보를\n입력해주세요."];
    
    _heightLbl.font = FONT_NOTO_REGULAR(16);
    _heightLbl.textColor = COLOR_MAIN_DARK;
    [_heightLbl setNotoText:@"키"];
    
    _heightInfoLbl.font = FONT_NOTO_REGULAR(12);
    _heightInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_heightInfoLbl setNotoText:@"소수점 첫째 자리까지 입력 가능합니다."];
    
    _heightTf.font = FONT_NOTO_REGULAR(16);
    _heightTf.textColor = COLOR_MAIN_DARK;
    _heightTf.placeholder = @"";
    _heightTf.delegate = self;
    [_heightTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _weightLbl.font = FONT_NOTO_REGULAR(16);
    _weightLbl.textColor = COLOR_MAIN_DARK;
    [_weightLbl setNotoText:@"체중"];
    
    _weightInfoLbl.font = FONT_NOTO_REGULAR(12);
    _weightInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_weightInfoLbl setNotoText:@"소수점 둘째 자리까지 입력 가능합니다."];
    
    _weightTf.font = FONT_NOTO_REGULAR(16);
    _weightTf.textColor = COLOR_MAIN_DARK;
    _weightTf.placeholder = @"";
    _weightTf.delegate = self;
    [_weightTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _targetLbl.font = FONT_NOTO_REGULAR(16);
    _targetLbl.textColor = COLOR_MAIN_DARK;
    [_targetLbl setNotoText:@"목표체중"];
    
    _targetInfoLbl.font = FONT_NOTO_REGULAR(12);
    _targetInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_targetInfoLbl setNotoText:@"소수점 둘째 자리까지 입력 가능합니다."];
    
    _targetTf.font = FONT_NOTO_REGULAR(16);
    _targetTf.textColor = COLOR_MAIN_DARK;
    _targetTf.placeholder = @"";
    _targetTf.delegate = self;
    [_targetTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_nextBtn setTitle:@"다  음" forState:UIControlStateNormal];
    _nextBtn.userInteractionEnabled = false;
    [_nextBtn mediBaseButton];
    
}

@end
