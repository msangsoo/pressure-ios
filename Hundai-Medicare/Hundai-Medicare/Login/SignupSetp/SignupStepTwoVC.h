//
//  SignupStepTwoVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "MedicareDataBase.h"
#import "AlarmObject.h"

@interface SignupStepTwoVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, BasicAlertVCDelegate>

@property (nonatomic, strong) NSString *mber_height;
@property (nonatomic, strong) NSString *mber_bdwgh;
@property (nonatomic, strong) NSString *mber_bdwgh_goal;

// 수정일때 넘어오는 데이터가 있을때 추가 +------
@property (nonatomic, retain) id delegate;
@property (nonatomic, strong) NSString *strTitle; // 값이 있으면 수정으로 간주함.
@property (nonatomic, strong) NSString *strActqy;
@property (nonatomic, strong) NSString *strJobYN;
@property (nonatomic, strong) NSString *strDiseaseNm;
@property (nonatomic, strong) NSString *strDiseaseTxt;
@property (nonatomic, strong) NSString *strSmoke;

@property (weak, nonatomic) IBOutlet UILabel *actqyLbl;
@property (weak, nonatomic) IBOutlet UIButton *actqy1Btn;
@property (weak, nonatomic) IBOutlet UIButton *actqy2Btn;
@property (weak, nonatomic) IBOutlet UIButton *actqy3Btn;

@property (weak, nonatomic) IBOutlet UILabel *jobLbl;
@property (weak, nonatomic) IBOutlet UIButton *jobYesBtn;
@property (weak, nonatomic) IBOutlet UIButton *jobNoBtn;

@property (weak, nonatomic) IBOutlet UILabel *diseaseLbl;
@property (weak, nonatomic) IBOutlet UIButton *disease1Btn;
@property (weak, nonatomic) IBOutlet UIButton *disease2Btn;
@property (weak, nonatomic) IBOutlet UIButton *disease3Btn;
@property (weak, nonatomic) IBOutlet UIButton *disease4Btn;
@property (weak, nonatomic) IBOutlet UIButton *disease5Btn;
@property (weak, nonatomic) IBOutlet UIButton *disease6Btn;
@property (weak, nonatomic) IBOutlet UITextField *diseaseTf;

@property (weak, nonatomic) IBOutlet UILabel *smokeLbl;
@property (weak, nonatomic) IBOutlet UIButton *smokeYesBtn;
@property (weak, nonatomic) IBOutlet UIButton *smokeNoBtn;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

@protocol SignupStepTwoVCDelegate
- (void)SignupStepEditDone:(NSDictionary*)dic;
@end
