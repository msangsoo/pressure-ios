//
//  SignupStepOneVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "SignupStepTwoVC.h"

@interface SignupStepOneVC : BaseViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *wordLbl;

@property (weak, nonatomic) IBOutlet UILabel *heightLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightInfoLbl;
@property (weak, nonatomic) IBOutlet UITextField *heightTf;

@property (weak, nonatomic) IBOutlet UILabel *weightLbl;
@property (weak, nonatomic) IBOutlet UILabel *weightInfoLbl;
@property (weak, nonatomic) IBOutlet UITextField *weightTf;

@property (weak, nonatomic) IBOutlet UILabel *targetLbl;
@property (weak, nonatomic) IBOutlet UILabel *targetInfoLbl;
@property (weak, nonatomic) IBOutlet UITextField *targetTf;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end
