//
//  LoginMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginMainVC.h"

@interface LoginMainVC () {
    int get_type_data;
}

@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;

@end

@implementation LoginMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:false];
    [self.navigationItem setTitle:@"로그인"];
    
    _walkDB         = [[WalkDBWraper alloc] init];
    _dataCalroieDB  = [[DataCalroieDBWraper alloc] init];
    _sugarDB        = [[SugarDBWraper alloc] init];
    _weightDB       = [[WeightDBWraper alloc] init];
    _presuDB        = [[PresuDBWraper alloc] init];
    _foodMainDB     = [[FoodMainDBWraper alloc] init];
    _foodDetailDB   = [[FoodDetailDBWraper alloc] init];
    _messageDB      = [[MessageDBWraper alloc] init];
    
    _emailTf.delegate = self;
    _passTf.delegate = self;
    get_type_data = 0;
    
    
    [self viewInit];
    [self pressAutoLogin:nil];
    
#if DEBUG
    _emailTf.text = @"moon@theinsystems.com";
    _passTf.text = @"1111";
//    _emailTf.text = @"csw300@csw.com";
//    _passTf.text = @"qwer1234!";
#endif
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)LoginSignupAddAuthAgree:(BOOL)agree {
    if (agree) {
        [self apiLogin];
    }
}

- (void)delegateResultData:(NSDictionary *)result {
    
    if ([[result objectForKey:@"api_code"] isEqualToString:@"login"]) {
        if ([[result objectForKey:@"log_yn"] isEqualToString:@"Y"]) {
            
            
            [_model setLoginData:result];
            
            
            if ([[result objectForKey:@"offerinfo_yn"] isEqualToString:@"N"]) {
                // 개인정보정책 변경안내 약관띄움
                LoginSignupAddAuthVC *vc = [LOGIN_STORYBOARD instantiateViewControllerWithIdentifier:@"LoginSignupAddAuthVC"];
                vc.delegate = self;
                [self.navigationController pushViewController:vc animated:true];
                return;
            }
            
            
#if DEBUG
//            [self goMain];
//            return;
#endif
            
            NSString *lastLoginID   = [Utils getUserDefault:USER_ID];
            // 기존앱 사용자의 아이디가 다르거나, 로그인 한적이 없는 사용자는 모두 지우고 다시 받음.
            if (lastLoginID == nil || lastLoginID.length < 1 || ![lastLoginID isEqualToString:_emailTf.text]) {
                //전체 데어터 삭제
                MedicareDataBase *mediDB = [[MedicareDataBase alloc] init];
                [mediDB allTableDelete];
                
                [self userDefaultInit];
                // + -----------------------
                //혈당 요청.
                // + -----------------------
                get_type_data = 2;
                [self doFirstDataType:@"2"];
            }else {
                [self healthDataInsert];
                [_model indicatorHidden];
                [self goMain];
            }
        }else {
            [AlertUtils EmojiDefaultShow:self emoji:error title:@"로그인 정보가 확인되지 않습니다.\n아이디 또는 비밀번호를 확인해주세요." msg:@"" left:@"확인" right:@""];
        }
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_hedctdata"]
                  && get_type_data == 2) {
            
        if([result objectForKey:@"data_list"] != nil) {
            //혈당 넣기
            [_sugarDB insert:[result objectForKey:@"data_list"]];
        }
        
        // + -----------------------
        //혈압 요청.
        // + -----------------------
        get_type_data = 3;
        [self doFirstDataType:@"3"];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_hedctdata"]
                  && get_type_data == 3) {
            
        if([result objectForKey:@"data_list"] != nil) {
            //혈압 넣기
            [_presuDB insert:[result objectForKey:@"data_list"]];
        }
        
        // + -----------------------
        //체중 요청.
        // + -----------------------
        get_type_data = 4;
        [self doFirstDataType:@"4"];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_hedctdata"]
              && get_type_data == 4) {
        
        if([result objectForKey:@"data_list"] != nil) {
            //체중계 넣기
            [_weightDB insert:[result objectForKey:@"data_list"]];
        }
        
        // + -----------------------
        //걷기 요청.
        // + -----------------------
        get_type_data = 7;
        [self doFirstDataType:@"7"];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_hedctdata"]
              && get_type_data == 7) {
        
        if([result objectForKey:@"data_list"] != nil) {
            //걷기칼로리 넣기
            [_dataCalroieDB insert:[result objectForKey:@"data_list"]];
        }
        
        // + -----------------------
        //건강메시지 요청.
        // + -----------------------
        get_type_data = 0;
        [self doFirstDataHeathMessage];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"infra_message"]) {
        
        if([result objectForKey:@"message_list"] != nil) {
            //메시지 넣기
            [_messageDB insert:[result objectForKey:@"message_list"]];
        }
        
        // + -----------------------
        //식사메인 요청.
        // + -----------------------
        [self doFoodMaindata];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_meal_input_data"]) {
        
        if([result objectForKey:@"data_list"] != nil) {
            //식사메인 넣기
            [_foodMainDB insert:[result objectForKey:@"data_list"]];
        }
        
        // + -----------------------
        //식사상세 요청.
        // + -----------------------
        [self doFoodDetaildata];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_meal_input_food_data"]) {
        if([result objectForKey:@"data_list"] != nil) {
            //식사상세 넣기
            [_foodDetailDB insert:[result objectForKey:@"data_list"]];
        }
        
        NSMutableArray *msgArr = [[NSMutableArray alloc] initWithArray: [_messageDB getResult:@"1"]];
        [msgArr addObjectsFromArray:[_messageDB getResult:@"2"]];
        if(msgArr.count > 0){
            [Utils setUserDefault:HEALTH_MSG_NEW value:@"N"];
        }else{
            [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
        }
        
        [_model indicatorHidden];
        [self goMain];
    }
}

#pragma mark - Actions
- (IBAction)pressAutoLogin:(id)sender {
    _autoLoginBtn.selected = !_autoLoginBtn.isSelected;
    
    _autoLoginLbl.textColor = COLOR_GRAY_SUIT;
    if (_autoLoginBtn.isSelected) {
        _autoLoginLbl.textColor = COLOR_NATIONS_BLUE;
    }
}

- (IBAction)LoginDidTap:(id)sender {
    [self apiLogin];
}

- (IBAction)pressIdFind:(id)sender {
    [self goIdFind];
}

- (IBAction)pressPassFind:(id)sender {
    [self goPassFind];
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == _emailTf) {
//        _idTfLeftConst.constant = 10;
//        [_emailImgVw setHidden:true];
    }else if (textField == _passTf) {
//        _passTfLeftConst.constant = 10;
//        [_passImgVw setHidden:true];
    }
    [self loginBtnActive];
    
    [self.view layoutIfNeeded];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _emailTf) {
        if (textField.text.length > 0) {
            
        }else {
//            _idTfLeftConst.constant = 55;
//            [_emailImgVw setHidden:false];
        }
    }else if (textField == _passTf) {
        if (textField.text.length > 0) {
            
        }else {
//            _passTfLeftConst.constant = 55;
//            [_passImgVw setHidden:false];
        }
    }
    [self loginBtnActive];
    
    [self.view layoutIfNeeded];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self loginBtnActive];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:true];
    return true;
}

- (void)loginBtnActive {
    
    if ([_emailTf.text length] > 4 && [_passTf.text length] > 3) {
        _loginBtn.selected = YES;
    }else {
        _loginBtn.selected = NO;
    }
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
    
}

#pragma mark - api
- (void)apiLogin {
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                  @"login", @"api_code",
                  _emailTf.text, @"mber_id",
                  _passTf.text, @"mber_pwd",
                  DIVECE_MODEL, @"phone_model",
                  APP_VERSION, @"app_ver",
                  @"", @"pushk",
                  [Utils getUserDefault:APP_TOKEN], @"token",
                  nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - user data 3개월치 가져오기
- (void)doFirstDataType:(NSString*)dataType {
    
    TimeUtils *time = [[TimeUtils alloc] init];
    [time setTimeType:PERIOD_MONTH];
    NSString *endday    = [time getEndTime];
    [time getTime:-3];
    NSString *beginday  = [time getNowTime];
    beginday = [beginday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    endday = [endday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    Tr_login *login = [_model getLoginData];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"get_hedctdata", @"api_code",
                                dataType, @"get_data_typ",
                                login.mber_sn, @"mber_sn",
                                beginday, @"begin_day",
                                endday, @"end_day",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - user data 전체 건강메시지 가져오기
- (void)doFirstDataHeathMessage {
    
    Tr_login *login = [_model getLoginData];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"infra_message", @"api_code",
                                login.mber_sn, @"mber_sn",
                                @"1", @"pageNumber",
                                nil];
    
    [server serverCommunicationData:parameters];
}

#pragma mark - user data 식사 메인 데이터 가져오기
- (void)doFoodMaindata {
    TimeUtils *time = [[TimeUtils alloc] init];
    [time setTimeType:PERIOD_MONTH];
    NSString *endday    = [time getEndTime];
    [time getTime:-3];
    NSString *beginday  = [time getNowTime];
    beginday = [beginday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    endday = [endday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    Tr_login *login = [_model getLoginData];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"get_meal_input_data", @"api_code",
                                login.mber_sn, @"mber_sn",
                                beginday, @"begin_day",
                                endday, @"end_day",
                                nil];
    
    [server serverCommunicationData:parameters];
}

#pragma mark - user data 식사 상세 데이터 가져오기
- (void)doFoodDetaildata {
    TimeUtils *time = [[TimeUtils alloc] init];
    [time setTimeType:PERIOD_MONTH];
    NSString *endday    = [time getEndTime];
    [time getTime:-3];
    NSString *beginday  = [time getNowTime];
    beginday = [beginday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    endday = [endday stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    Tr_login *login = [_model getLoginData];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"get_meal_input_food_data", @"api_code",
                                login.mber_sn, @"mber_sn",
                                beginday, @"begin_day",
                                endday, @"end_day",
                                nil];
    
    [server serverCommunicationData:parameters];
}

#pragma mark - user userdefault init
- (void)userDefaultInit {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *alarmListData = [defaults objectForKey:ALARM_LIST_DATA];
    NSMutableArray *items = [NSKeyedUnarchiver unarchiveObjectWithData:alarmListData];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    
    for (AlarmObject *alarm in items) {
        for (UILocalNotification *event in eventArray) {
            NSDictionary *userInfoCurrent = event.userInfo;
            NSString *uid = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
            if ([uid isEqualToString:[NSString stringWithFormat:@"%i", alarm.notificationID]]) {
                [app cancelLocalNotification:event];
            }
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [userDefault removeObjectForKey:AUTO_LOGIN];
    [userDefault removeObjectForKey:USER_ID];
    [userDefault removeObjectForKey:USER_PASS];
    [userDefault removeObjectForKey:MAIN_NOTI_IDX];
    [userDefault removeObjectForKey:MAIN_NOTI_COMFIRM];
    [userDefault removeObjectForKey:ALARM_LIST_DATA];
    
    [userDefault removeObjectForKey:HEALTH_MSG_NEW];
    [userDefault removeObjectForKey:HEALTH_POINT_ALERT];
    [userDefault removeObjectForKey:HEALTH_TIP_DATA];
    [userDefault removeObjectForKey:HEALTH_WEIGHT_TIP_TODAY_TIME];
    [userDefault removeObjectForKey:HEALTH_PRESU_TIP_TODAY_TIME];
    [userDefault removeObjectForKey:HEALTH_SUGAR_TIP_TODAY_TIME];
    
    [userDefault removeObjectForKey:PRESU_AFTER_TIME];
    [userDefault removeObjectForKey:PRESU_AFTER_COUNT];
    [userDefault removeObjectForKey:PRESU_ONESTEP_TIME];
    [userDefault removeObjectForKey:PRESU_ONESTEP_COUNT];
    [userDefault removeObjectForKey:PRESU_TWOSTEP_TIME];
    [userDefault removeObjectForKey:PRESU_TWOSTEP_COUNT];
    [userDefault removeObjectForKey:PRESU_LAST_SAVE_TIME];
    [userDefault removeObjectForKey:PRESU_DAY_FIRST];
    
    [userDefault removeObjectForKey:@"Food_Lately_Input"];
    
    [userDefault synchronize];
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_MEDIUM(24);
    _word1Lbl.textColor = COLOR_GRAY_SUIT;
    [_word1Lbl setNotoText:@"안녕하세요!"];
    
    _word2Lbl.font = FONT_NOTO_REGULAR(14);
    _word2Lbl.textColor = COLOR_GRAY_SUIT;
    [_word2Lbl setNotoText:@"가입하신 정보로 로그인 해주세요."];
    
    _autoLoginLbl.font = FONT_NOTO_LIGHT(14);
    _autoLoginLbl.textColor = COLOR_GRAY_SUIT;
    [_autoLoginLbl setNotoText:@"자동로그인"];
    
    [_emailVw setBackgroundColor:RGB(237, 237, 237)];
    [_emailVw makeToRadius:true];
    
    _emailTf.font = FONT_NOTO_BOLD(14);
    [_emailTf setPlaceholder:@"아이디(이메일주소)"];
    _emailTf.textColor = COLOR_NATIONS_BLUE;
    _emailTf.delegate = self;
    
    [_passVw setBackgroundColor:RGB(237, 237, 237)];
    [_passVw makeToRadius:true];
    
    _passTf.font = FONT_NOTO_BOLD(14);
    [_passTf setPlaceholder:@"비밀번호"];
    _passTf.textColor = COLOR_NATIONS_BLUE;
    _passTf.delegate = self;
    
    _loginBtn.titleLabel.font = FONT_NOTO_REGULAR(16);
    [_loginBtn setTitle:@"로그인" forState:UIControlStateNormal];
    [_loginBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(143, 143, 143) withFrame:_loginBtn.bounds] forState:UIControlStateNormal];
    [_loginBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_loginBtn.bounds] forState:UIControlStateSelected];
    [_loginBtn makeToRadius:true];
    
    [_idFindBtn setTitle:@"아이디찾기" forState:UIControlStateNormal];
    [_idFindBtn setTitleColor:RGB(143, 143, 143) forState:UIControlStateNormal];
    _idFindBtn.titleLabel.font = FONT_NOTO_REGULAR(14);
    
    [_passFindBtn setTitle:@"비밀번호찾기" forState:UIControlStateNormal];
    [_passFindBtn setTitleColor:RGB(143, 143, 143) forState:UIControlStateNormal];
    _passFindBtn.titleLabel.font = FONT_NOTO_REGULAR(14);
    
}

#pragma mark - Page Move Method
- (void)goMain {
    [Utils setUserDefault:AUTO_LOGIN value:@"N"];
    if(self.autoLoginBtn.isSelected) {
        [Utils setUserDefault:AUTO_LOGIN value:@"Y"];
    }
    
    [Utils setUserDefault:USER_ID value:_emailTf.text];
    [Utils setUserDefault:USER_PASS value:_passTf.text];
    
    Tr_login *login = [_model getLoginData];
    if ([login.add_reg_yn isEqualToString:@"Y"]) {
        //기본정보 입력 페이지
        UIViewController *vc = SIGNUPSTEP_STORYBOARD.instantiateInitialViewController;
        [self.navigationController pushViewController:vc animated:true];
    }else {
        UIViewController *vc = MAIN_STORYBOARD.instantiateInitialViewController;
        [self.navigationController pushViewController:vc animated:true];
    }
}

- (void)goIdFind {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginFindIdVC"];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)goPassFind {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginFindPassVC"];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - health kit
- (void)healthDataInsert {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:LAST_WALK_SAVE_DATE] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:LAST_WALK_SAVE_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_WALK_SAVE_DATE];
    NSDate *todayDate = [NSDate date];
    
    NSString *saveDateStr = [TimeUtils getDateToString:saveDate];
    NSString *todayDateStr = [TimeUtils getDateToString:todayDate];
    
    if ([saveDateStr intValue] >= [todayDateStr intValue]) {
        return;
    }
    
    saveDateStr = [TimeUtils getDateAdd:saveDateStr day:-1];
    todayDateStr = [TimeUtils getDateAdd:todayDateStr day:-1];
    
    Tr_login *login = [_model getLoginData];
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    self.caffeineDataStore = [[CaffeineDataStore alloc] initWithHealthStore:healthStore];
    
    [self.caffeineDataStore healthKitSamplesWithCopletion:saveDateStr endData:todayDateStr :^(NSArray *samples, NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                //서버에 데이터 전송
                
                float avgStepDistance =  60.f; //[_login.mber_height floatValue] - 100.f;
                if(avgStepDistance < 0.f)
                    avgStepDistance = 60.f;
                NSDateFormatter *dft = [[NSDateFormatter alloc] init];
                [dft setDateFormat:@"yyyyMMddHHmm"];
                
                if(samples.count > 0) {
                    
                    NSString *beforeRegdate = @"";
                    NSString *lastItemRegdate = @"";
                    CGFloat stepVal = 0.f;
                    CGFloat distanceVal = 0.f;
                    CGFloat calorieVal = 0.f;
                    
                    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
                    long i = 0;
                    NSString *tempidx = [Utils getNowDate:@"yyMMddHHmmss"];
                    
                    for(HKQuantitySample *sample in samples) {
                        
                        [dft setDateFormat:@"yyyyMMddHH"];
                        NSString *itemRegdate = [dft stringFromDate:sample.endDate];
                        
                        if ([beforeRegdate isEqualToString:itemRegdate]) {
                            
                            NSString *step = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            stepVal += [step floatValue];
                            distanceVal += [[NSString stringWithFormat:@"%1.f", ((avgStepDistance * [step floatValue]) / 10.f)] floatValue];
                            calorieVal += [[NSString stringWithFormat:@"%1.f", ([login.mber_bdwgh floatValue] * ([step floatValue] / 10000.f) * 5.5f)] floatValue];
                            
                            lastItemRegdate = [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]];
                        }else {
                            
                            if ([beforeRegdate isEqualToString:@""]) {
                                
                            }else {
                                NSString *idx = [NSString stringWithFormat:@"%lld",[tempidx longLongValue] + i];
                                NSDictionary *writeData =[NSDictionary dictionaryWithObjectsAndKeys:
                                                          idx, @"idx",
                                                          [NSString stringWithFormat:@"%1.f", calorieVal], @"calorie",
                                                          [NSString stringWithFormat:@"%1.f", distanceVal], @"distance",
                                                          [NSString stringWithFormat:@"%1.f", stepVal], @"step",
                                                          @"0", @"heartRate",
                                                          @"0", @"stress",
                                                          @"U", @"regtype",
                                                          [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]], @"regdate",
                                                          nil];
                                [ast_mass addObject:writeData];
                                NSLog(@"writeData = %@", writeData);
                            }
                            
                            stepVal = [[NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]] floatValue];
                            distanceVal = [[NSString stringWithFormat:@"%1.f", ((avgStepDistance * stepVal) / 10.f)] floatValue];
                            calorieVal = [[NSString stringWithFormat:@"%1.f", ([login.mber_bdwgh floatValue] * (stepVal / 10000.f) * 5.5f)] floatValue];
                        }
                        
                        beforeRegdate = itemRegdate;
                        i++;
                        
                        lastItemRegdate = [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]];
                    }
                    
                    NSString *idx = [NSString stringWithFormat:@"%lld",[tempidx longLongValue] + i];
                    NSDictionary *writeData =[NSDictionary dictionaryWithObjectsAndKeys:
                                              idx, @"idx",
                                              [NSString stringWithFormat:@"%1.f", calorieVal], @"calorie",
                                              [NSString stringWithFormat:@"%1.f", distanceVal], @"distance",
                                              [NSString stringWithFormat:@"%1.f", stepVal], @"step",
                                              @"0", @"heartRate",
                                              @"0", @"stress",
                                              @"U", @"regtype",
                                              lastItemRegdate, @"regdate",
                                              nil];
                    [ast_mass addObject:writeData];
                    NSLog(@"writeData = %@", writeData);
                    
                    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                @"mvm_info_input_data", @"api_code",
                                                ast_mass, @"ast_mass",
                                                login.mber_sn, @"mber_sn",
                                                @"304", @"insures_code",
                                                nil];
                    
                    [self->server serverCommunicationData:parameters];
                    
                }else {
                    
                }
            });
        });
    }];
}


@end
