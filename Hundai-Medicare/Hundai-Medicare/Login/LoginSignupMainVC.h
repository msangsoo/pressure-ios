//
//  LoginSignupMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "LoginSignupNotAuthVC.h"

@interface LoginSignupMainVC : BaseViewController<EmojiDefaultVCDelegate>

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;

@property (weak, nonatomic) IBOutlet UILabel *authLbl;
@property (weak, nonatomic) IBOutlet UILabel *inputLbl;

@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UIButton *btnNotMember;

@end
