//
//  LoginSignupMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginSignupMainVC.h"

@interface LoginSignupMainVC ()

@end

@implementation LoginSignupMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:false];
    [self.navigationItem setTitle:@"회원가입"];
    
    [self viewInit];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}

#pragma mark - Actions
- (IBAction)pressSignup:(id)sender {
    [AlertUtils EmojiDefaultShow:self emoji:happy title:@"현대해상 보험 가입 고객이 아닌 경우\n준회원으로 가입되며 건강상담, 진료예약 등의 메디케어 서비스와 관련된 일부 기능이 제한됩니다." msg:@"준회원으로 이용 중 메디케어서비스가 제공되는\n보험에 가입하신 경우 마이페이지에서 정회원으로 전환이 가능합니다.\n\n※ 현대해상 가입 고객(정회원)이 준회원으로 추가 가입 하실 경우 준회원으로 이용하신 앱 데이터는 정회원 계정으로 이관이 불가능합니다." left:@"확인" right:@""];
    
//    UIViewController *vc = [[UIStoryboard storyboardWithName:@"SignupStep" bundle:nil] instantiateViewControllerWithIdentifier:@"SignupStepTwoVC"];
//    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
    LoginSignupNotAuthVC *vc = [LOGIN_STORYBOARD instantiateViewControllerWithIdentifier:@"LoginSignupNotAuthVC"];
    vc.mber_gred = @"20";
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(24);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    [_word1Lbl setNotoText:@"환영합니다."];
    
    _word2Lbl.font = FONT_NOTO_REGULAR(21);
    _word2Lbl.textColor = COLOR_NATIONS_BLUE;
    [_word2Lbl setNotoText:@"현대해상 보험 가입 고객에게 제공되는\n건강관리 앱 메디케어서비스입니다."];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(14);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"앱 이용을 위한 고객 인증 방법을 선택해주세요."];
    
    _authLbl.font = FONT_NOTO_REGULAR(14);
    _authLbl.textColor = COLOR_NATIONS_BLUE;
    [_authLbl setNotoText:@"간편인증"];
    
    _inputLbl.font = FONT_NOTO_REGULAR(14);
    _inputLbl.textColor = COLOR_GRAY_SUIT;
    [_inputLbl setNotoText:@"고객정보 직접 입력"];
    
    _infoLbl.font = FONT_NOTO_REGULAR(14);
    _infoLbl.textColor = [UIColor whiteColor];
    [_infoLbl setNotoText:@"현대해상 보험 가입 고객이 아닙니다."];
    
    _btnNotMember.layer.cornerRadius = 7;
    _btnNotMember.layer.masksToBounds = true;
}

@end
