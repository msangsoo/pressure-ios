//
//  LoginIntroVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginIntroVC.h"

@interface LoginIntroVC ()

@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;

@end

@implementation LoginIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self viewSetUp];
    //    [self versionCheck];
    if ([[Utils getUserDefault:AUTO_LOGIN] isEqualToString:@"Y"]) {
        [self apiLogin];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}

- (void)viewDidAppear:(BOOL)animated {
    
}

/*
 - (void)versionCheck {
 NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
 @"get_app_info", @"api_code",
 [Utils getUserDefault:APP_TOKEN], @"HP_SEQ",
 @"A", @"DEVICE_KIND",
 APP_VERSION, @"APP_VER",
 nil];
 
 if(parameters != nil) {
 if(server.delegate == nil) {
 server.delegate = self;
 }
 [_model indicatorActive];
 [server serverCommunicationData:parameters];
 }else {
 NSLog(@"server parameters error = %@", parameters);
 }
 }
 */

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}

-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)LoginSignupAddAuthAgree:(BOOL)agree {
    if (agree) {
        [self apiLogin];
    }
}

- (void)delegateResultData:(NSDictionary *)result {
    if ([[result objectForKey:@"api_code"] isEqualToString:@"login"]) {
        
        
        if ([[result objectForKey:@"log_yn"] isEqualToString:@"Y"]) {
            [_model setLoginData:result];
            
            if ([[result objectForKey:@"offerinfo_yn"] isEqualToString:@"N"]) {
                // 개인정보정책 변경안내 약관띄움
                LoginSignupAddAuthVC *vc = [LOGIN_STORYBOARD instantiateViewControllerWithIdentifier:@"LoginSignupAddAuthVC"];
                vc.delegate = self;
                [self.navigationController pushViewController:vc animated:true];
                return;
            }
            [self healthDataInsert];
            /*[NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
             
             }];*/
            [self goMain];
            
        }
    }
    /*
     else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_app_info"]) {
     float svrVer = [[result objectForKey:@"now_app_ver"] floatValue];
     float appVer = [APP_VERSION floatValue];
     
     if (svrVer > appVer) {
     
     NSString *message = @"더욱 향상된 서비스를 위한 쉬운방법!\n지금 바로 업데이트해 주세요.";
     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"업데이트"
     message:message
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction *alertCancelAction = [UIAlertAction actionWithTitle:@"취소"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * _Nonnull action) {
     
     }];
     UIAlertAction *alertDoneAction = [UIAlertAction actionWithTitle:@"업데이트"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * _Nonnull action) {
     
     NSURL* url = [[NSURL alloc] initWithString:APP_STORE_URL];
     [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
     exit(0);
     }];
     
     }];
     [alertController addAction:alertCancelAction];
     [alertController addAction:alertDoneAction];
     [self presentViewController:alertController animated:YES completion:nil];
     return;
     }else {
     if ([[Utils getUserDefault:AUTO_LOGIN] isEqualToString:@"Y"]) {
     [self apiLogin];
     }
     }
     }*/
}

#pragma mark - api
- (void)apiLogin {
    
    if ([Utils getUserDefault:USER_ID] == nil
        || [[Utils getUserDefault:USER_ID] length]==0) {
        return;
    }
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"login", @"api_code",
                                [Utils getUserDefault:USER_ID], @"mber_id",
                                [Utils getUserDefault:USER_PASS], @"mber_pwd",
                                DIVECE_MODEL, @"phone_model",
                                APP_VERSION, @"app_ver",
                                @"", @"pushk",
                                [Utils getUserDefault:APP_TOKEN], @"token",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - Page Move Method
- (void)goMain {
    Tr_login *login = [_model getLoginData];
    if ([login.add_reg_yn isEqualToString:@"Y"]) {
        //기본정보 입력 페이지
        UIViewController *vc = SIGNUPSTEP_STORYBOARD.instantiateInitialViewController;
        [self.navigationController pushViewController:vc animated:false];
    }else {
        HomeMainVC *vc = MAIN_STORYBOARD.instantiateInitialViewController;
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [self.navigationController pushViewController:vc animated:false];
    }
}

#pragma mark - setUp
- (void)viewSetUp {
    _titleLbl.font = FONT_NOTO_BOLD(28);
    [_titleLbl setNotoText:@"메디케어 서비스"];
    
    _subTitleLbl.font = FONT_NOTO_REGULAR(16);
    [_subTitleLbl setNotoText:@"고객님의 건강을 지켜드립니다."];
    
    [_firstUserBtn setTitle:@"새 사용자" forState:UIControlStateNormal];
    [_firstUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_firstUserBtn.titleLabel setFont:FONT_NOTO_REGULAR(16)];
    [_firstUserBtn setBackgroundColor:[UIColor whiteColor]];
    [_firstUserBtn.layer setCornerRadius:_firstUserBtn.frame.size.height / 2];
    [_firstUserBtn.layer setMasksToBounds:true];
    
    [_secondUserBtn setTitle:@"기존 사용자" forState:UIControlStateNormal];
    [_secondUserBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_secondUserBtn.titleLabel setFont:FONT_NOTO_REGULAR(16)];
    [_secondUserBtn setBackgroundColor:[UIColor clearColor]];
    [_secondUserBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_secondUserBtn.layer setBorderWidth:1.5];
    [_secondUserBtn.layer setCornerRadius:_secondUserBtn.frame.size.height / 2];
    [_secondUserBtn.layer setMasksToBounds:true];
}

#pragma mark - health kit
- (void)healthDataInsert {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:LAST_WALK_SAVE_DATE] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:LAST_WALK_SAVE_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_WALK_SAVE_DATE];
    NSDate *todayDate = [NSDate date];
    
    NSString *saveDateStr = [TimeUtils getDateToString:saveDate];
    NSString *todayDateStr = [TimeUtils getDateToString:todayDate];
    
    if ([saveDateStr intValue] >= [todayDateStr intValue]) {
        return;
    }
    
    saveDateStr = [TimeUtils getDateAdd:saveDateStr day:-1];
    todayDateStr = [TimeUtils getDateAdd:todayDateStr day:-1];
    
    Tr_login *login = [_model getLoginData];
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    self.caffeineDataStore = [[CaffeineDataStore alloc] initWithHealthStore:healthStore];
    
    [self.caffeineDataStore healthKitSamplesWithCopletion:saveDateStr endData:todayDateStr :^(NSArray *samples, NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                //서버에 데이터 전송
                
                float avgStepDistance =  60.f; //[_login.mber_height floatValue] - 100.f;
                if(avgStepDistance < 0.f)
                    avgStepDistance = 60.f;
                NSDateFormatter *dft = [[NSDateFormatter alloc] init];
                [dft setDateFormat:@"yyyyMMddHHmm"];
                
                if(samples.count > 0) {
                    
                    NSString *beforeRegdate = @"";
                    NSString *lastItemRegdate = @"";
                    CGFloat stepVal = 0.f;
                    CGFloat distanceVal = 0.f;
                    CGFloat calorieVal = 0.f;
                    
                    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
                    long i = 0;
                    NSString *tempidx = [Utils getNowDate:@"yyMMddHHmmss"];
                    
                    for(HKQuantitySample *sample in samples) {
                        
                        [dft setDateFormat:@"yyyyMMddHH"];
                        NSString *itemRegdate = [dft stringFromDate:sample.endDate];
                        
                        if ([beforeRegdate isEqualToString:itemRegdate]) {
                            
                            NSString *step = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            stepVal += [step floatValue];
                            distanceVal += [[NSString stringWithFormat:@"%1.f", ((avgStepDistance * [step floatValue]) / 10.f)] floatValue];
                            calorieVal += [[NSString stringWithFormat:@"%1.f", ([login.mber_bdwgh floatValue] * ([step floatValue] / 10000.f) * 5.5f)] floatValue];
                            
                            lastItemRegdate = [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]];
                        }else {
                            
                            if ([beforeRegdate isEqualToString:@""]) {
                                
                            }else {
                                NSString *idx = [NSString stringWithFormat:@"%lld",[tempidx longLongValue] + i];
                                NSDictionary *writeData =[NSDictionary dictionaryWithObjectsAndKeys:
                                                          idx, @"idx",
                                                          [NSString stringWithFormat:@"%1.f", calorieVal], @"calorie",
                                                          [NSString stringWithFormat:@"%1.f", distanceVal], @"distance",
                                                          [NSString stringWithFormat:@"%1.f", stepVal], @"step",
                                                          @"0", @"heartRate",
                                                          @"0", @"stress",
                                                          @"U", @"regtype",
                                                          [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]], @"regdate",
                                                          nil];
                                [ast_mass addObject:writeData];
                                NSLog(@"writeData = %@", writeData);
                            }
                            
                            stepVal = [[NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]] floatValue];
                            distanceVal = [[NSString stringWithFormat:@"%1.f", ((avgStepDistance * stepVal) / 10.f)] floatValue];
                            calorieVal = [[NSString stringWithFormat:@"%1.f", ([login.mber_bdwgh floatValue] * (stepVal / 10000.f) * 5.5f)] floatValue];
                        }
                        
                        beforeRegdate = itemRegdate;
                        i++;
                        
                        lastItemRegdate = [NSString stringWithFormat:@"%@00", [dft stringFromDate:sample.endDate]];
                    }
                    
                    NSString *idx = [NSString stringWithFormat:@"%lld",[tempidx longLongValue] + i];
                    NSDictionary *writeData =[NSDictionary dictionaryWithObjectsAndKeys:
                                              idx, @"idx",
                                              [NSString stringWithFormat:@"%1.f", calorieVal], @"calorie",
                                              [NSString stringWithFormat:@"%1.f", distanceVal], @"distance",
                                              [NSString stringWithFormat:@"%1.f", stepVal], @"step",
                                              @"0", @"heartRate",
                                              @"0", @"stress",
                                              @"U", @"regtype",
                                              lastItemRegdate, @"regdate",
                                              nil];
                    [ast_mass addObject:writeData];
                    NSLog(@"writeData = %@", writeData);
                    
                    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                @"mvm_info_input_data", @"api_code",
                                                ast_mass, @"ast_mass",
                                                login.mber_sn, @"mber_sn",
                                                @"304", @"insures_code",
                                                nil];
                    
                    [self->server serverCommunicationData:parameters];
                    
                }else {
                    
                }
            });
        });
    }];
}

@end
