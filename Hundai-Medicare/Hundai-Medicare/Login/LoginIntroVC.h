//
//  LoginIntroVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "HomeMainVC.h"
#import "LoginSignupAddAuthVC.h"

#import "CaffeineDataStore.h"

@interface LoginIntroVC : BaseViewController<ServerCommunicationDelegate, LoginSignupAddAuthVCDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UIButton *firstUserBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondUserBtn;

@end
