//
//  LoginMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "TimeUtils.h"
#import "AlarmObject.h"

#import "CaffeineDataStore.h"

//db
#import "MedicareDataBase.h"
#import "SugarDBWraper.h"
#import "WeightDBWraper.h"
#import "DataCalroieDBWraper.h"
#import "PresuDBWraper.h"
#import "WalkDBWraper.h"
#import "FoodMainDBWraper.h"
#import "FoodDetailDBWraper.h"
#import "MessageDBWraper.h"
#import "LoginSignupAddAuthVC.h"


@interface LoginMainVC : BaseViewController<UITextFieldDelegate, ServerCommunicationDelegate, EmojiDefaultVCDelegate, LoginSignupAddAuthVCDelegate> {
    WalkDBWraper *_walkDB;
    DataCalroieDBWraper *_dataCalroieDB;
    SugarDBWraper *_sugarDB;
    WeightDBWraper *_weightDB;
    PresuDBWraper *_presuDB;
    FoodMainDBWraper *_foodMainDB;
    FoodDetailDBWraper *_foodDetailDB;
    MessageDBWraper *_messageDB;
}

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;

@property (weak, nonatomic) IBOutlet UILabel *autoLoginLbl;
@property (weak, nonatomic) IBOutlet UIButton *autoLoginBtn;

@property (weak, nonatomic) IBOutlet UIView *emailVw;
@property (weak, nonatomic) IBOutlet UIImageView *emailImgVw;
@property (weak, nonatomic) IBOutlet UITextField *emailTf;

@property (weak, nonatomic) IBOutlet UIView *passVw;
@property (weak, nonatomic) IBOutlet UIImageView *passImgVw;
@property (weak, nonatomic) IBOutlet UITextField *passTf;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (weak, nonatomic) IBOutlet UIButton *idFindBtn;
@property (weak, nonatomic) IBOutlet UIButton *passFindBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *idTfLeftConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passTfLeftConst;

#pragma mark - Actions
- (IBAction)LoginDidTap:(id)sender;

@end
