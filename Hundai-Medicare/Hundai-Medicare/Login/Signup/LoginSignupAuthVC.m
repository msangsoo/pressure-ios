//
//  LoginSgnupAuthVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginSignupAuthVC.h"

@interface LoginSignupAuthVC () {
    NSString *result_key;
}

@end

@implementation LoginSignupAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"회원가입(1/2)"];
    
    result_key = @"";
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_sms_proc"]) {
        if([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            result_key = [result objectForKey:@"result_key"];
            
            _numberTf.text = @"";
            _numberBtn.selected = false;
            _nextBtn.selected = false;
            
            _numberLbl.hidden = false;
            _numberVw.hidden = false;
            _nextBtn.hidden = false;
            
            _phoneTf.enabled = false;
            _phoneBtn.enabled = false;
            
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"4444"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"고객님의 정보가 확인되지 않습니다.\n휴대폰 번호를 확인해주시기 바랍니다."];
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"5555"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"휴대폰 번호 중복으로 간편인증이 불가능합니다.\n고객정보를 직접 입력해 주시기 바랍니다."];
        }
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"mber_sms_check"]) {
        if([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
//            [Utils basicAlertView:self withTitle:@"" withMsg:@"인증이 완료되었습니다.\n회원가입 페이지로 이동합니다." completion:^{
                LoginSignupIdVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginSignupIdVC"];
                vc.mber_hp = [result objectForKey:@"mber_hp"];
                vc.m_name = [result objectForKey:@"m_name"];
                vc.m_sex = [result objectForKey:@"m_sex"];
                vc.m_birth = [result objectForKey:@"m_birth"];
                vc.mber_gred = @"10";
                vc.marketing = self.auth4Btn.selected?@"Y":@"N";
            
                [self.navigationController pushViewController:vc animated:true];
//            }];
        }else if([[result objectForKey:@"result_code"] isEqualToString:@"6666"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"이미 가입하신 고객입니다.\n로그인 후 이용해주시기 바랍니다. " completion:^{

            }];
        }else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"인증번호가 일치하지 않습니다.\n인증번호를 확인해주세요."];
        }
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == _phoneTf) {
        if(textField.text.length > 10) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }else if (textField == _numberTf) {
        if(textField.text.length > 5) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if(textField == _phoneTf) {
        if(_phoneTf.text.length < 10) {
            _phoneBtn.selected = false;
        }else {
            if (_auth1Btn.isSelected) {
                _phoneBtn.selected = true;
                
            }else {
                _phoneBtn.selected = false;
            }
        }
    }else if(textField == _numberTf) {
        if(_numberTf.text.length < 5) {
            _numberBtn.selected = false;
        }else {
            _numberBtn.selected = true;
        }
    }
}

#pragma mark - Actions
- (IBAction)pressPhone:(id)sender {
    if (_phoneBtn.isSelected) {
        [self apiSmsProc];
        
        _auth1Btn.userInteractionEnabled = NO;
        _auth2Btn.userInteractionEnabled = NO;
        _auth3Btn.userInteractionEnabled = NO;
        _auth4Btn.userInteractionEnabled = NO;
    }
}

- (IBAction)pressNumber:(id)sender {
    if (_numberBtn.isSelected) {
        if ([result_key isEqualToString:_numberTf.text]) {
            [self apiSmsCheck];
        }else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"인증번호가 일치하지 않습니다.\n인증번호를 확인해주세요."];
        }
    }
}

- (IBAction)pressAuth:(UIButton *)sender {
    if (sender.tag == 1) {
        [_auth2Btn setSelected:true];
        [_auth3Btn setSelected:true];
        [_auth4Btn setSelected:true];
    }else if (sender.tag == 2) {
        _auth2Btn.selected = !_auth2Btn.isSelected;
    }else if (sender.tag == 3) {
        _auth3Btn.selected = !_auth3Btn.isSelected;
    }else if (sender.tag == 4) {
        _auth4Btn.selected = !_auth4Btn.isSelected;
    }
    
    [_auth1Btn setSelected:false];
    if (_auth2Btn.isSelected && _auth3Btn.isSelected && _auth4Btn.isSelected) {
        [_auth1Btn setSelected:true];
    }
    
    if(_phoneTf.text.length > 9
       && _auth2Btn.isSelected
       && _auth3Btn.isSelected
       && _auth4Btn.isSelected
       ) {
        _phoneBtn.selected = true;
    }else{
        _phoneBtn.selected = false;
    }
}

- (IBAction)pressNext:(id)sender {
    if (_nextBtn.isSelected) {
//        LoginSignupIdVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginSignupIdVC"];
//        [self.navigationController pushViewController:vc animated:true];
    }else {
        [Utils basicAlertView:self withTitle:@"" withMsg:@"인증문자를 받지 못하셨나요? 5초 후에도 수신이 안되신다면 재발송 버튼을 눌러주세요." completion:^{
            [self apiSmsProc];
        } fail:nil];
    }
}

- (IBAction)pressPermission:(UIButton *)sender {
    
    NSString *titleStr = @"";
    NSString *urlStr = @"";
    if (sender.tag == 1) {
        titleStr = @"개인정보 제공 동의";
        urlStr = PERSONAL_TERMS_1_URL;
        
    }else if (sender.tag == 2) {
        titleStr = @"개인민감정보 제공 동의";
        urlStr = PERSONAL_TERMS_2_URL;
        
    }else if (sender.tag == 3) {
        titleStr = @"개인정보 제3자 제공 동의";
        urlStr = PERSONAL_TERMS_4_URL;
        
    }else if (sender.tag == 4) {
        titleStr = @"마케팅 활동 동의(선택)";
        urlStr = PERSONAL_ASSO_TERMS_5_URL;
    }
    
    CommonWebVC *vc = [COMMON_STORYBOARD instantiateViewControllerWithIdentifier:@"CommonWebVC"];
    vc.title = titleStr;
    vc.paramURL = urlStr;
    [self.navigationController pushViewController:vc animated:true];
}


#pragma mark - api
- (void)apiSmsProc {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_sms_proc", @"api_code",
                                [_model getToken], @"token",
                                _phoneTf.text, @"mber_hp",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiSmsCheck {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_sms_check", @"api_code",
                                [_model getToken], @"token",
                                _phoneTf.text, @"mber_hp",
                                result_key, @"result_key",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _auth1Lbl.font = FONT_NOTO_REGULAR(14);
    _auth1Lbl.textColor = COLOR_MAIN_DARK;
    [_auth1Lbl setNotoText:@"개인정보 수집 전체 동의"];
    
    _auth2Lbl.font = FONT_NOTO_REGULAR(14);
    _auth2Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth2Lbl setNotoText:@"개인정보 제공 동의(필수)"];
    
    _auth3Lbl.font = FONT_NOTO_REGULAR(14);
    _auth3Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth3Lbl setNotoText:@"개인정보 제공 동의(필수)"];
    
    _auth4Lbl.font = FONT_NOTO_REGULAR(14);
    _auth4Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth4Lbl setNotoText:@"개인정보 제3자 제공 동의(필수)"];
    
    _auth1Btn.tag = 1;
    _auth2Btn.tag = 2;
    _auth3Btn.tag = 3;
    _auth4Btn.tag = 4;
    
    [_auth1Btn setSelected:false];
    [_auth2Btn setSelected:false];
    [_auth3Btn setSelected:false];
    [_auth4Btn setSelected:false];
    
    _phoneLbl.font = FONT_NOTO_REGULAR(14);
    _phoneLbl.textColor = COLOR_MAIN_DARK;
    [_phoneLbl setNotoText:@"휴대폰 번호"];
    
    _phoneInfoLbl.font = FONT_NOTO_REGULAR(11);
    _phoneInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_phoneInfoLbl setNotoText:@"보험가입시 제공하신 휴대폰 번호를 입력해주세요."];
    
    _phoneTf.font = FONT_NOTO_REGULAR(14);
    [_phoneTf setPlaceholder:@"휴대폰번호('-'제외)"];
    [_phoneTf setKeyboardType:UIKeyboardTypePhonePad];
    _phoneTf.delegate = self;
    [_phoneTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_phoneBtn setTitle:@"인증요청" forState:UIControlStateNormal];
    [_phoneBtn mediBaseButton];
    
    _numberLbl.font = FONT_NOTO_REGULAR(14);
    _numberLbl.textColor = COLOR_MAIN_DARK;
    [_numberLbl setNotoText:@"인증번호"];
    
    _numberTf.font = FONT_NOTO_REGULAR(14);
    [_numberTf setPlaceholder:@"인증번호"];
    [_numberTf setKeyboardType:UIKeyboardTypePhonePad];
    _numberTf.delegate = self;
    [_numberTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_numberBtn setTitle:@"인증하기" forState:UIControlStateNormal];
    [_numberBtn mediBaseButton];
    
    
    [_nextBtn setTitle:@"인증번호 재요청" forState:UIControlStateNormal];
    [_nextBtn mediBaseButton];
    _nextBtn.selected = true;
    
    _numberLbl.hidden = true;
    _numberVw.hidden = true;
    _nextBtn.hidden = true;
}

@end
