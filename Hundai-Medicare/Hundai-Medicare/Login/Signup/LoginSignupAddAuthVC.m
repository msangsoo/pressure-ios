//
//  LoginSignupNotAuthVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginSignupAddAuthVC.h"

@interface LoginSignupAddAuthVC ()

@end

@implementation LoginSignupAddAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    login = [_model getLoginData];
    _confirmBtn.selected = false;
    
    [self.navigationItem setTitle:@"개인정보정책 변경 안내"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}


#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
   
    if ([[result objectForKey:@"api_code"] isEqualToString:@"offer_info_mod"]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
        if([[result objectForKey:@"offer_yn"] isEqualToString:@"Y"]) {
            [self.delegate LoginSignupAddAuthAgree:YES];
        }else {
            [self.delegate LoginSignupAddAuthAgree:NO];
        }
    }
}


- (IBAction)pressAuth:(UIButton *)sender {
    if (sender.tag == 100) {
        [_auth2Btn setSelected:!_auth1Btn.selected];
        [_auth3Btn setSelected:!_auth1Btn.selected];
        [_auth4Btn setSelected:!_auth1Btn.selected];
    }else {
        if (sender.tag == 101) {
            _auth2Btn.selected = !_auth2Btn.isSelected;
        }else if (sender.tag == 102) {
            _auth3Btn.selected = !_auth3Btn.isSelected;
        }else if (sender.tag == 103) {
            _auth4Btn.selected = !_auth4Btn.isSelected;
        }
    }
    
    [_auth1Btn setSelected:false];
    if (_auth2Btn.isSelected && _auth3Btn.isSelected && _auth4Btn.isSelected ) {
        [_auth1Btn setSelected:true];
    }
    [self vaildateCheck];
}

- (IBAction)pressAuthInfo:(UIButton *)sender {
    NSString *titleStr = @"";
    NSString *urlStr = @"";
    if (sender.tag == 1) {
        titleStr = @"개인정보 제공 동의(필수)";
        urlStr = PERSONAL_TERMS_1_URL;
        
    }else if (sender.tag == 2) {
        titleStr = @"개인민감정보 제공 동의(필수)";
        urlStr = PERSONAL_TERMS_2_URL;
        
    }else if (sender.tag == 3) {
        titleStr = @"개인정보 제3자 제공 동의(필수)";
        urlStr = PERSONAL_TERMS_4_URL;
        
    }
    
    CommonWebVC *vc = [COMMON_STORYBOARD instantiateViewControllerWithIdentifier:@"CommonWebVC"];
    vc.title = titleStr;
    vc.paramURL = urlStr;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    
    if (_confirmBtn.isSelected) {
    
        [self apiAgree];
    }
}


#pragma mark - vaildate
- (void)vaildateCheck {
    _confirmBtn.selected = false;
    
    if (!_auth1Btn.isSelected
        || !_auth2Btn.isSelected
        || !_auth3Btn.isSelected) {
        return;
    }
    _confirmBtn.selected = true;
}

#pragma mark - api
- (void)apiAgree {
    
    [_model indicatorActive];

    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                  @"offer_info_mod", @"api_code",
                  @"Y", @"offer_yn",
                  login.mber_sn, @"mber_sn",
                  nil];

    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}




#pragma mark - viewInit
- (void)viewInit {
    
    _wordLbl.numberOfLines = 3;
    _wordLbl.font = FONT_NOTO_REGULAR(17);
    _wordLbl.textColor = COLOR_NATIONS_BLUE;
   
    [_wordLbl setNotoText:@"개인정보정책이 변경되었습니다.\n개인정보정책에 동의해주세요."];
    
    _auth1Lbl.font = FONT_NOTO_REGULAR(14);
    _auth1Lbl.textColor = COLOR_MAIN_DARK;
    [_auth1Lbl setNotoText:@"전체 동의"];
    
    _auth2Lbl.font = FONT_NOTO_REGULAR(14);
    _auth2Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth2Lbl setNotoText:@"개인정보 제공 동의(필수)"];
    
    _auth3Lbl.font = FONT_NOTO_REGULAR(14);
    _auth3Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth3Lbl setNotoText:@"개인민감정보 제공 동의(필수)"];
    
    _auth4Lbl.font = FONT_NOTO_REGULAR(14);
    _auth4Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth4Lbl setNotoText:@"개인정보 제3자 제공 동의(필수)"];
    
    [_auth1Btn setSelected:false];
    [_auth2Btn setSelected:false];
    [_auth3Btn setSelected:false];
    [_auth4Btn setSelected:false];
    
    [_confirmBtn setTitle:@"다  음" forState:UIControlStateNormal];
    [_confirmBtn setTitle:@"다  음" forState:UIControlStateSelected];
    [_confirmBtn mediBaseButton];
    
}

@end
