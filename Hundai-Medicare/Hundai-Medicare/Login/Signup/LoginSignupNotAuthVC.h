//
//  LoginSignupNotAuthVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "CommonWebVC.h"
#import "LoginSignupIdVC.h"


@interface LoginSignupNotAuthVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, EmojiDefaultVCDelegate> {
    Tr_login *login;
    NSString *mber_no;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, strong) NSString *mber_gred;

@property (weak, nonatomic) IBOutlet UILabel *wordLbl;

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UITextField *nameTf;

@property (weak, nonatomic) IBOutlet UILabel *birthLbl;
@property (weak, nonatomic) IBOutlet UITextField *birthTf;

@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;

@property (weak, nonatomic) IBOutlet UILabel *sexLbl;
@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (weak, nonatomic) IBOutlet UILabel *maleLbl;
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
@property (weak, nonatomic) IBOutlet UILabel *femaleLbl;

@property (weak, nonatomic) IBOutlet UILabel *auth1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth4Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth5Lbl;

@property (weak, nonatomic) IBOutlet UIButton *auth1Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth2Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth3Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth4Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth5Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth5BtnDummy;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

// 정회원전환일때
@property (nonatomic, strong) NSString *strTitle; // 이데이터 있으면, 전회원전환임
@property (nonatomic, strong) NSString *strTitleMsg;

@end

@protocol LoginSignupNotAuthVCDelegate
- (void)doubleIDPop:(NSDictionary*)dic;
- (void)memberTransComplete:(NSDictionary*)dic;
@end
