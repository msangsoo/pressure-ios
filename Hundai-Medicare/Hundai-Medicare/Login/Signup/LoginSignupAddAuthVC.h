//
//  LoginSignupNotAuthVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "API.h"
#import "BaseViewController.h"
#import "CommonWebVC.h"


@interface LoginSignupAddAuthVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate>
{
    Tr_login *login;
}
@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UILabel *wordLbl;


@property (weak, nonatomic) IBOutlet UILabel *auth1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth4Lbl;

@property (weak, nonatomic) IBOutlet UIButton *auth1Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth2Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth3Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth4Btn;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
- (IBAction)pressAuthInfo:(UIButton *)sender;

@end

@protocol LoginSignupAddAuthVCDelegate
- (void)LoginSignupAddAuthAgree:(BOOL)agree;
@end
