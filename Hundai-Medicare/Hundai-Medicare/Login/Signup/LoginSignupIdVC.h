//
//  LoginSignupIdVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface LoginSignupIdVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSString *mber_hp;
@property (nonatomic, strong) NSString *m_name;
@property (nonatomic, strong) NSString *m_sex;
@property (nonatomic, strong) NSString *m_birth;
@property (nonatomic, strong) NSString *mber_gred;
@property (nonatomic, strong) NSString *marketing;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *wordEnd;

@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UITextField *emailTf;

@property (weak, nonatomic) IBOutlet UILabel *passLbl;
@property (weak, nonatomic) IBOutlet UITextField *passTf;

@property (weak, nonatomic) IBOutlet UILabel *passConfirmLbl;
@property (weak, nonatomic) IBOutlet UITextField *passConfirmTf;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contairVwHeightConst;

@end
