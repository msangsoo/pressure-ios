//
//  LoginSgnupAuthVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "LoginSignupIdVC.h"
#import "CommonWebVC.h"

@interface LoginSignupAuthVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *auth1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *auth4Lbl;

@property (weak, nonatomic) IBOutlet UIButton *auth1Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth2Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth3Btn;
@property (weak, nonatomic) IBOutlet UIButton *auth4Btn;

@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneInfoLbl;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;

@property (weak, nonatomic) IBOutlet UIView *numberVw;
@property (weak, nonatomic) IBOutlet UILabel *numberLbl;
@property (weak, nonatomic) IBOutlet UITextField *numberTf;
@property (weak, nonatomic) IBOutlet UIButton *numberBtn;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end
