//
//  LoginSignupNotAuthVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginSignupNotAuthVC.h"

@interface LoginSignupNotAuthVC ()

@end

@implementation LoginSignupNotAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    login = [_model getLoginData];
    
    if (self.strTitle) {
        [self.navigationItem setTitle:self.strTitle];
    }else {
        [self.navigationItem setTitle:@"회원가입(1/2)"];
    }
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - textfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == _phoneTf) {
        if(textField.text.length > 10) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }else if(textField == _birthTf ) {
        if(textField.text.length > 7) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }else if(textField == _nameTf ) {
        if(textField.text.length > 30) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    
    return YES;
}


#pragma mark - textfield delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _nameTf) {
        if ([_nameTf.text length] < 2) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"2글자 이상 입력해주세요."];
            [self vaildateCheck];
             return;
        }
    }else if (textField == _birthTf) {
        if (![TimeUtils getDateValidate:_birthTf.text]
            && [_birthTf.text length] > 0) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"생년월일 형식을 확인해주세요."];
            [self vaildateCheck];
            return;
        }
        long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue];
        long iDate = [_birthTf.text longLongValue];
        if (nDate < iDate) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"생년월일은 미래날자를 입력할 수 없습니다."];
            [self vaildateCheck];
            return;
        }
    }else if (textField == _phoneTf) {
        if ([_phoneTf.text length] < 10 &&
             [_birthTf.text length] > 0
            ) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"휴대폰 번호를 정확히 입력해주세요."];
            [self vaildateCheck];
            return;
        }
    }
    
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if ([self.strTitle length] > 0) {
        
        if ([[result objectForKey:@"api_code"] isEqualToString:@"mber_find_yn"]) {
            //정회원전환
            if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
                
                mber_no = [result objectForKey:@"mber_no"];
                //바로전환
                [self goOriginalMemberShip];
                
            }else if([[result objectForKey:@"data_yn"] isEqualToString:@"YY"]) {
                
                if (self.delegate) {

                    [self.navigationController popViewControllerAnimated:YES];
                    
                    NSString *site_memid = [result objectForKey:@"site_memid"];
                    NSString *app_memid  = [result objectForKey:@"app_memid"];
                    NSString *mber_no  = [result objectForKey:@"mber_no"];
                    NSDictionary *dic    = [NSDictionary dictionaryWithObjectsAndKeys:
                                            site_memid, @"site_memid",
                                            app_memid,  @"app_memid",
                                            mber_no,  @"mber_no",
                                            nil];
                    [self.delegate doubleIDPop:dic];
                }
            }else if([[result objectForKey:@"data_yn"] isEqualToString:@"N"]) {
                [AlertUtils EmojiDefaultShow:self emoji:sad title:@"" msg:@"서비스 대상자가 아닙니다.\n보험 가입 시 제공하셨던 정보가 맞는지\n확인해주시기 바랍니다." left:@"확인" right:@""];
            }else if([[result objectForKey:@"data_yn"] isEqualToString:@"YN"]) {
                [AlertUtils EmojiDefaultShow:self emoji:sad title:@"" msg:@"이미 가입하신 고객입니다.\n로그인 후 이용해주시기 바랍니다." left:@"확인" right:@""];
            }
            
        }else if ([[result objectForKey:@"api_code"] isEqualToString:@"asstb_mber_keep_member"]) {
            
            if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
                
                [Utils basicAlertView:self withTitle:@"" withMsg:@"정회원 인증이 완료되었습니다.\n메디케어서비스 고객이 되신 것을 환영합니다." completion:^{
                    if (self.delegate) {
                        [self.delegate memberTransComplete:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }
            
        }
    }else {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]
           || [[result objectForKey:@"data_yn"] isEqualToString:@"YY"]) {
            [self goSignup];
        }else if([[result objectForKey:@"data_yn"] isEqualToString:@"N"]) {
            [AlertUtils EmojiDefaultShow:self emoji:sad title:@"" msg:@"서비스 대상자가 아닙니다.\n보험 가입 시 제공하셨던 정보가 맞는지\n확인해주시기 바랍니다." left:@"확인" right:@""];
        }else if([[result objectForKey:@"data_yn"] isEqualToString:@"YN"]) {
            [AlertUtils EmojiDefaultShow:self emoji:sad title:@"" msg:@"이미 가입하신 고객입니다.\n로그인 후 이용해주시기 바랍니다." left:@"확인" right:@""];
        }
    }
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
    
}

#pragma mark - Actions
- (IBAction)pressSex:(UIButton *)sender {
    if (sender.tag == 0) {
        [_maleBtn setSelected:true];
        [_femaleBtn setSelected:false];
    }else {
        [_maleBtn setSelected:false];
        [_femaleBtn setSelected:true];
    }
    
    [self vaildateCheck];
}

- (IBAction)pressAuth:(UIButton *)sender {
    if (sender.tag == 0) {
        [_auth2Btn setSelected:true];
        [_auth3Btn setSelected:true];
        [_auth4Btn setSelected:true];
        [_auth5Btn setSelected:true];
    }else {
        if (sender.tag == 1) {
            _auth2Btn.selected = !_auth2Btn.isSelected;
        }else if (sender.tag == 2) {
            _auth3Btn.selected = !_auth3Btn.isSelected;
        }else if (sender.tag == 3) {
            _auth4Btn.selected = !_auth4Btn.isSelected;
        }else if (sender.tag == 4) {
            _auth5Btn.selected = !_auth5Btn.isSelected;
        }
    }
    
    
    [_auth1Btn setSelected:false];
    if ([self.mber_gred isEqualToString:@"10"]
        || [self.strTitle length] > 0) {
        if (_auth2Btn.isSelected && _auth3Btn.isSelected && _auth4Btn.isSelected ) {
            [_auth1Btn setSelected:true];
        }
    }else {
        if (_auth2Btn.isSelected && _auth3Btn.isSelected && _auth4Btn.isSelected ) {
            [_auth1Btn setSelected:true];
        }
    }
    
    [self vaildateCheck];
}

- (IBAction)pressAuthInfo:(UIButton *)sender {
    NSString *titleStr = @"";
    NSString *urlStr = @"";
    if (sender.tag == 1) {
        titleStr = @"개인정보 제공 동의(필수)";
        
        urlStr = PERSONAL_TERMS_1_URL;
        
    }else if (sender.tag == 2) {
        titleStr = @"개인민감정보 제공 동의(필수)";
        
        urlStr = PERSONAL_TERMS_2_URL;
        
    }else if (sender.tag == 3) {
        titleStr = @"개인정보 제3자 제공 동의(필수)";
        
        urlStr = PERSONAL_TERMS_4_URL;
        
    }else if (sender.tag == 4) {
        titleStr = @"마케팅 활동 동의(선택)";
        
        urlStr = PERSONAL_ASSO_TERMS_5_URL;
    }
    
    CommonWebVC *vc = [COMMON_STORYBOARD instantiateViewControllerWithIdentifier:@"CommonWebVC"];
    vc.title = titleStr;
    vc.paramURL = urlStr;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    
    if (_confirmBtn.isSelected) {
        if ([self.strTitle length] >0) {
            // 정회원전환 인증
            [self apiMberFind];
        }else {
            if ([_mber_gred isEqualToString:@"10"]) {
                [self apiMberFind];
            }else {
                [self goSignup];
            }
        }
    }
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)textField {
    [self vaildateCheck];
}

#pragma mark - vaildate
- (void)vaildateCheck {
    _confirmBtn.selected = false;
    
    if ([_mber_gred isEqualToString:@"10"]) {
        if (!_auth1Btn.isSelected
            || !_auth2Btn.isSelected
            || !_auth3Btn.isSelected) {
            return;
        }
    }else{
        if (!_auth1Btn.isSelected
            || !_auth2Btn.isSelected
            || !_auth3Btn.isSelected) {
            return;
        }
    }
    
    if (!_maleBtn.isSelected && !_femaleBtn.isSelected) {
        return;
    }
    
    if (_birthTf.text.length != 8) {
        return;
    }
    
    if (_nameTf.text.length < 2) {
        return;
    }
    
    if (_phoneTf.text.length < 10) {
        return;
    }
    if ([_birthTf.text length]==8) {
        long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue];
        long iDate = [_birthTf.text longLongValue];
        if (nDate < iDate) {
            return;
        }
    }
    if ([_birthTf.text length] >= 8) {
        if (![TimeUtils getDateValidate:_birthTf.text]) {
            return;
        }
    }
    
    _confirmBtn.selected = true;
}

#pragma mark - api
- (void)apiMberFind {
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                  @"mber_find_yn", @"api_code",
                  _nameTf.text, @"mber_nm",
                  _birthTf.text, @"mber_lifyea",
                  _phoneTf.text, @"mber_hp",
                  _maleBtn.isSelected ? @"1" : @"2", @"mber_sex",
                  login.mber_sn, @"mber_sn",
                  nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)goOriginalMemberShip {
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"asstb_mber_keep_member", @"api_code",
                                login.mber_sn, @"mber_sn",
                                mber_no, @"mber_no",
                                login.mber_nm, @"memname",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}


#pragma mark - page move
- (void)goSignup {
    LoginSignupIdVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginSignupIdVC"];
    vc.m_name = self.nameTf.text;
    vc.m_birth = self.birthTf.text;
    vc.mber_hp = self.phoneTf.text;
    vc.m_sex = self.maleBtn.isSelected ? @"1" : @"2";
    vc.mber_gred = self.mber_gred;
    vc.marketing = _auth5Btn.selected?@"Y":@"N";
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - viewInit
- (void)viewInit {
    if ([self.mber_gred isEqualToString:@""] || self.mber_gred == nil) {
        self.mber_gred = @"10";
    }
    
    _wordLbl.numberOfLines = 3;
    _wordLbl.font = FONT_NOTO_REGULAR(17);
    _wordLbl.textColor = COLOR_NATIONS_BLUE;
    if (self.strTitleMsg) {
        [_wordLbl setNotoText:self.strTitleMsg];
    }else {
        if ([self.mber_gred isEqualToString:@"10"]) {
            [_wordLbl setNotoText:@"고객 인증을 위해\n보험 가입 시 제공하셨던 정보를\n입력해주세요."];
        }else {
            [_wordLbl setNotoText:@"회원가입에 필요한\n기본정보를 입력해주시기 바랍니다."];
        }
    }
    
    _nameLbl.font = FONT_NOTO_REGULAR(14);
    _nameLbl.textColor = COLOR_MAIN_DARK;
    [_nameLbl setNotoText:@"이름"];
    
    _nameTf.font = FONT_NOTO_REGULAR(14);
    _nameTf.textColor = COLOR_MAIN_DARK;
    _nameTf.placeholder = @"2글자 이상 입력 해 주세요.";
    _nameTf.delegate = self;
    [_nameTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _birthLbl.font = FONT_NOTO_REGULAR(14);
    _birthLbl.textColor = COLOR_MAIN_DARK;
    [_birthLbl setNotoText:@"생년월일"];
    
    _birthTf.font = FONT_NOTO_REGULAR(14);
    _birthTf.textColor = COLOR_MAIN_DARK;
    _birthTf.placeholder = @"생년월일 8자를 입력 해 주세요.예)19830101";
    _birthTf.delegate = self;
    [_birthTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _phoneLbl.font = FONT_NOTO_REGULAR(14);
    _phoneLbl.textColor = COLOR_MAIN_DARK;
    [_phoneLbl setNotoText:@"휴대폰번호"];
    
    _phoneTf.font = FONT_NOTO_REGULAR(14);
    _phoneTf.textColor = COLOR_MAIN_DARK;
    _phoneTf.placeholder = @"휴대폰번호('-'제외)";
    _phoneTf.delegate = self;
    [_phoneTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _sexLbl.font = FONT_NOTO_REGULAR(14);
    _sexLbl.textColor = COLOR_MAIN_DARK;
    [_sexLbl setNotoText:@"성별"];
    
    [_maleBtn setSelected:false];
    
    _maleLbl.font = FONT_NOTO_REGULAR(14);
    _maleLbl.textColor = COLOR_GRAY_SUIT;
    [_maleLbl setNotoText:@"남성"];
    
    [_femaleBtn setSelected:false];
    
    _femaleLbl.font = FONT_NOTO_REGULAR(14);
    _femaleLbl.textColor = COLOR_GRAY_SUIT;
    [_femaleLbl setNotoText:@"여성"];
    
    _auth1Lbl.font = FONT_NOTO_REGULAR(14);
    _auth1Lbl.textColor = COLOR_MAIN_DARK;
    [_auth1Lbl setNotoText:@"개인정보 수집 전체 동의"];
    
    _auth2Lbl.font = FONT_NOTO_REGULAR(14);
    _auth2Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth2Lbl setNotoText:@"개인정보 제공 동의(필수)"];
    
    _auth3Lbl.font = FONT_NOTO_REGULAR(14);
    _auth3Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth3Lbl setNotoText:@"개인민감정보 제공 동의(필수)"];
    
    _auth4Lbl.font = FONT_NOTO_REGULAR(14);
    _auth4Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth4Lbl setNotoText:@"개인정보 제3자 제공 동의(필수)"];
    
    _auth5Lbl.font = FONT_NOTO_REGULAR(14);
    _auth5Lbl.textColor = COLOR_GRAY_SUIT;
    [_auth5Lbl setNotoText:@"마케팅 활용 동의(선택)"];
    
    [_auth1Btn setSelected:false];
    [_auth2Btn setSelected:false];
    [_auth3Btn setSelected:false];
    [_auth4Btn setSelected:false];
    [_auth5Btn setSelected:false];
    
    [_confirmBtn setTitle:@"다  음" forState:UIControlStateNormal];
    [_confirmBtn setTitle:@"다  음" forState:UIControlStateSelected];
    [_confirmBtn mediBaseButton];
    
    if (self.mber_gred == nil || [self.mber_gred isEqualToString:@""]) {
        self.mber_gred = @"10";
    }
    
    if ([self.mber_gred isEqualToString:@"10"]
        || [self.strTitle length] > 0) {
        _auth5Btn.hidden = YES;
        _auth5BtnDummy.hidden = YES;
        _auth5Lbl.hidden = YES;
        _constViewHeight.constant = 732;
    }
}

@end
