//
//  LoginSignupIdVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginSignupIdVC.h"

@interface LoginSignupIdVC () {
    BOOL emailAuthCheck;
    BOOL passAuthCheck;
}

@end

@implementation LoginSignupIdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"회원가입(2/2)"];
    
    emailAuthCheck = false;
    passAuthCheck = false;
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
    
    if (IS_IPHONEX) {
        self.contairVwHeightConst.constant = UIScreen.mainScreen.bounds.size.height - 88;
    }else {
        if (UIScreen.mainScreen.bounds.size.height - 64 < 590) {
            self.contairVwHeightConst.constant = 590;
        }else {
            self.contairVwHeightConst.constant = UIScreen.mainScreen.bounds.size.height - 64;
        }
    }
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_check_id"]) {
        //TODO:4444처리가 안되어 있음 : 5/24 유영진 통화함, 서버에서 확정해서 알려주기로 함.
        if ([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            emailAuthCheck = true;
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"4444"]){
            emailAuthCheck = false;
            [Utils basicAlertView:self withTitle:@"" withMsg:@"이미 사용중인 아이디입니다."];
        }else{
            emailAuthCheck = false;
            [Utils basicAlertView:self withTitle:@"" withMsg:@"사용할 수 없는 아이디입니다."];
        }
        
        _confirmBtn.selected = false;
        if (emailAuthCheck && passAuthCheck) {
            _confirmBtn.selected = true;
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"mber_reg"]) {
        
        if ([[result objectForKey:@"result_code"] isEqualToString:@"0000"] ||
            [[result objectForKey:@"result_code"] isEqualToString:@"2222"] ) {
            
            [Utils basicAlertView:self withTitle:@"" withMsg:@"아이디 생성이 완료되었습니다." completion:^{
                
                // 회원가입완료 후 아이디/패스워드 저장
                [Utils setUserDefault:USER_ID value:self->_emailTf.text];
                [Utils setUserDefault:USER_PASS value:self->_passTf.text];
                // 자동로그인
                [Utils setUserDefault:AUTO_LOGIN value:@"Y"];
                
                [self apiLogin];
            }];
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"1111"]
                  || [[result objectForKey:@"result_code"] isEqualToString:@"3333"]
                  || [[result objectForKey:@"result_code"] isEqualToString:@"6666"]
                  || [[result objectForKey:@"result_code"] isEqualToString:@"7777"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"이미 가입하신 고객입니다.\n로그인 후 이용해주시기 바랍니다."];
            
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"5555"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"정회원 대상자이십니다.\n정회원으로 가입해주시기 바랍니다."];
            
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"4444"]) {
//            [Utils basicAlertView:self withTitle:@"" withMsg:@"이미 사용중인 닉네임이 있습니다.\n다른 닉네임을 입력해주세요."];
            
        }else if ([[result objectForKey:@"result_code"] isEqualToString:@"5566"]
                  || [[result objectForKey:@"result_code"] isEqualToString:@"8888"]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"서비스 대상자가 아닙니다.\n보험 가입 시 제공하셨던 정보가 맞는지 확인해주시기 바랍니다."];
            
        }else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"회원가입이 실패 했습니다."];
        }
    }
    else if ([[result objectForKey:@"api_code"] isEqualToString:@"login"]) {
        if ([[result objectForKey:@"log_yn"] isEqualToString:@"Y"]) {
            [_model setLoginData:result];
            
            // 추가정보페이지로 이동.
            UIViewController *vc = SIGNUPSTEP_STORYBOARD.instantiateInitialViewController;
            vc.navigationItem.hidesBackButton = YES;
            [self.navigationController pushViewController:vc animated:true];
        }
    }
}

- (void)apiLogin {
    
    if ([Utils getUserDefault:USER_ID] == nil
        || [[Utils getUserDefault:USER_ID] length]==0) {
        return;
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"login", @"api_code",
                                [Utils getUserDefault:USER_ID], @"mber_id",
                                [Utils getUserDefault:USER_PASS], @"mber_pwd",
                                DIVECE_MODEL, @"phone_model",
                                APP_VERSION, @"app_ver",
                                @"", @"pushk",
                                [Utils getUserDefault:APP_TOKEN], @"token",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}


#pragma mark - textfield delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _emailTf) {
        emailAuthCheck = false;
        if (![Utils emailIsValid:_emailTf.text]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"아이디는 이메일 형식으로 입력해주세요."];
        }else {
            [self apiEmailCheck];
        }
    }else if (textField == _passTf) {
        passAuthCheck = false;
        if (![Utils NSStringIsValidPass:_passTf.text]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"영문, 숫자, 특수문자가 모두 포함된 8자리 이상으로 입력해주세요."];
        }else {
            if ([_passTf.text isEqualToString:_passConfirmTf.text]) {
                passAuthCheck = true;
            }else {
                if (![_passConfirmTf.text isEqualToString:@""]) {
                    [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호가 일치하지 않습니다."];
                }
            }
        }
    }else if (textField == _passConfirmTf) {
        passAuthCheck = false;
        if (![Utils NSStringIsValidPass:_passConfirmTf.text]) {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"영문, 숫자, 특수문자가 모두 포함된 8자리 이상으로 입력해주세요."];
        }else {
            if ([_passTf.text isEqualToString:_passConfirmTf.text] && ![_passTf.text isEqualToString:@""]) {
                passAuthCheck = true;
            }else {
                if (![_passTf.text isEqualToString:@""]) {
                    [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호가 일치하지 않습니다."];
                }
            }
        }
    }
    
    _confirmBtn.selected = false;
    if (emailAuthCheck && passAuthCheck) {
        _confirmBtn.selected = true;
    }
}

- (void)textFieldDidChange:(UITextField *)textField {
   
}

#pragma mark - Actions
- (IBAction)pressConfirm:(id)sender {
    if (_confirmBtn.isSelected) {
        [self apiMberReg];
    }
}

#pragma mark - api
- (void)apiEmailCheck {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_check_id", @"api_code",
                                _emailTf.text, @"mber_id",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiMberReg {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_reg", @"api_code",
                                _emailTf.text, @"mber_id",
                                _passTf.text, @"mber_pwd",
                                _mber_hp, @"mber_hp",
                                _m_name, @"mber_nm",
                                _m_sex, @"mber_sex",
                                _m_birth, @"mber_lifyea",
                                _mber_gred, @"mber_grad",
                                @"", @"pushk",
                                APP_VERSION, @"app_ver",
                                DIVECE_MODEL, @"phone_model",
                                @"I", @"device_kind",
                                [_model getToken], @"token",
                                self.marketing, @"Marketing_yn",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_BOLD(24);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    [_word1Lbl setNotoText:[NSString stringWithFormat:@"%@", _m_name]];
    
    _wordEnd.font = FONT_NOTO_THIN(24);
    _wordEnd.textColor = COLOR_NATIONS_BLUE;
    [_wordEnd setNotoText:[NSString stringWithFormat:@"님 환영합니다."]];
    
    _word2Lbl.font = FONT_NOTO_REGULAR(18);
    _word2Lbl.textColor = COLOR_NATIONS_BLUE;
    _word2Lbl.numberOfLines = 2;
    [_word2Lbl setNotoText:@"현대해상 보험 가입 고객에게 제공되는\n건강관리 앱 메디케어서비스입니다."];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(18);
    _word3Lbl.textColor = COLOR_NATIONS_BLUE;
    [_word3Lbl setNotoText:@"앱에서 사용하실 아이디를 생성해 주세요."];
    
    _emailLbl.font = FONT_NOTO_REGULAR(14);
    _emailLbl.textColor = COLOR_MAIN_DARK;
    [_emailLbl setNotoText:@"아이디 (이메일)"];
    
    _emailTf.font = FONT_NOTO_REGULAR(14);
    _emailTf.textColor = COLOR_MAIN_DARK;
    [_emailTf setPlaceholder:@"이메일형식의 아이디를 입력 해 주세요."];
    _emailTf.delegate = self;
    [_emailTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _passLbl.font = FONT_NOTO_REGULAR(14);
    _passLbl.textColor = COLOR_MAIN_DARK;
    [_passLbl setNotoText:@"비밀번호"];
    
    _passTf.font = FONT_NOTO_REGULAR(14);
    _passTf.textColor = COLOR_MAIN_DARK;
    [_passTf setPlaceholder:@"영문,숫자,특수문자 포함 8자리 이상입력해주세요."];
    _passTf.delegate = self;
    [_passTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _passConfirmLbl.font = FONT_NOTO_REGULAR(14);
    _passConfirmLbl.textColor = COLOR_MAIN_DARK;
    [_passConfirmLbl setNotoText:@"비밀번호 확인"];
    
    _passConfirmTf.font = FONT_NOTO_REGULAR(14);
    _passConfirmTf.textColor = COLOR_MAIN_DARK;
    [_passConfirmTf setPlaceholder:@"영문,숫자,특수문자 포함 8자리 이상입력해주세요."];
    _passConfirmTf.delegate = self;
    [_passConfirmTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [_confirmBtn setTitle:@"확 인" forState:UIControlStateNormal];
    [_confirmBtn setTitle:@"확 인" forState:UIControlStateSelected];
    [_confirmBtn mediBaseButton];
}

@end
