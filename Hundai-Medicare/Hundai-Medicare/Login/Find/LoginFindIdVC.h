//
//  LoginFindIdVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "LoginFindPassVC.h"

@interface LoginFindIdVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;

@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet UIView *findVw;

@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word4Lbl;

@property (weak, nonatomic) IBOutlet UIButton *passFindBtn;
@property (weak, nonatomic) IBOutlet UIButton *findConfirmBtn;

@end
