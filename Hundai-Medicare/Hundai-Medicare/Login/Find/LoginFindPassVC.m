//
//  LoginFindPassVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginFindPassVC.h"

@interface LoginFindPassVC ()

@end

@implementation LoginFindPassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"비밀번호찾기"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
    [self confirmBtnActive];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"login_pwd"]) {
        if([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            [AlertUtils EmojiDefaultShow:self emoji:mail title:@"이메일로 임시 비밀 번호를 발송해드렸습니다.\n로그인 후 내정보>계정설정에서 비밀번호를 변경해주시기 바랍니다." msg:@"" left:@"확인" right:@""];
        }else {
            NSString *msg = @"ID 또는 휴대폰 번호가 확인되지 않습니다.";
            [AlertUtils BasicAlertShow:self tag:1001 title:@"" msg:msg left:@"" right:@"확인"];
        }
    }
}

#pragma mark - textfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == _phoneTf) {
        if(textField.text.length > 10) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }else if(textField == _emailTf ) {
        if(textField.text.length > 30) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self confirmBtnActive];
}

- (void)confirmBtnActive {
    _confirmBtn.selected = false;
    if(_phoneTf.text.length >= 10 && _emailTf.text.length >= 6 && [Utils emailIsValid:_emailTf.text]) {
        _confirmBtn.selected = true;
    }
}

#pragma mark - Actions
- (IBAction)pressConfirm:(id)sender {
    if (_confirmBtn.isSelected) {
        [self.view endEditing:true];
        [self apiPassFind];
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.view.tag == 100) {
        [self popBack:@"LoginMainVC"];
        
    }else if (alertView.tag==1001) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:15885656"] options:@{} completionHandler:nil];
    }
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
     [self popBack:@"LoginMainVC"];
}

#pragma mark - api
- (void)apiPassFind {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                      @"login_pwd", @"api_code",
                      _emailTf.text, @"mber_id",
                      _phoneTf.text, @"mber_hp",
                      _phoneTf.text, @"sms_snd_hp",
                      [_model getToken], @"token",
                      nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(18);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    _word1Lbl.numberOfLines = 3;
    [_word1Lbl setNotoText:@"아래 정보를 입력해주세요.\n메일로 임시비밀번호를\n발송 해 드립니다."];
    
    _emailTf.font = FONT_NOTO_BOLD(14);
    _emailTf.textColor = COLOR_NATIONS_BLUE;
    _emailTf.placeholder = @"아이디(이메일주소)";
    _emailTf.text = _email;
    _emailTf.delegate = self;
    [_emailTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _phoneTf.font = FONT_NOTO_BOLD(14);
    _phoneTf.textColor = COLOR_NATIONS_BLUE;
    _phoneTf.placeholder = @"휴대전화번호('-'제외)";
    _phoneTf.text = _phone;
    _phoneTf.delegate = self;
    [_phoneTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _confirmBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_confirmBtn setTitle:@"확  인" forState:UIControlStateNormal];
    [_confirmBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_confirmBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(237, 237, 237) withFrame:_confirmBtn.bounds] forState:UIControlStateNormal];
    [_confirmBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_confirmBtn.bounds] forState:UIControlStateSelected];
}

@end
