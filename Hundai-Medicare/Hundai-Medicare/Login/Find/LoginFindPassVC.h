//
//  LoginFindPassVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface LoginFindPassVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, BasicAlertVCDelegate, EmojiDefaultVCDelegate>

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;

@property (weak, nonatomic) IBOutlet UITextField *emailTf;

@property (weak, nonatomic) IBOutlet UITextField *phoneTf;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end
