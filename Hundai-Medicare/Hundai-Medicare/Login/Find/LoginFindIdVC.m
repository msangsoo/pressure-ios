//
//  LoginFindIdVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "LoginFindIdVC.h"

@interface LoginFindIdVC ()

@end

@implementation LoginFindIdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"아아디찾기"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"login_id"]) {
        _findVw.hidden = false;
        if ([[result objectForKey:@"mber_id"] length] > 0) {
            [_word2Lbl setNotoText:@"아이디를 찾았습니다."];
            [_word4Lbl setNotoText:[result objectForKey:@"mber_id"]];
            _confirmBtn.selected = false;
            _word3Lbl.hidden = false;
            _passFindBtn.hidden = false;
            _findConfirmBtn.hidden = false;
        }else {
            [_word2Lbl setNotoText:@"가입된 정보가 없습니다."];
            _word4Lbl.text = @"";
            _word3Lbl.hidden = true;
            _passFindBtn.hidden = true;
            _findConfirmBtn.hidden = true;
        }
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == _phoneTf) {
        if(textField.text.length > 10) {
            if([string isEqualToString:@""]) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if(textField == _phoneTf) {
        if(_phoneTf.text.length < 10)
            [_confirmBtn setSelected:NO];
        else
            [_confirmBtn setSelected:YES];
    }
}

#pragma mark - Actions
- (IBAction)pressConfirm:(id)sender {
    if (_confirmBtn.isSelected) {
        [self.view endEditing:true];
        [self apiLoginFind];
    }
}

- (IBAction)pressPassFind:(id)sender {
    LoginFindPassVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginFindPassVC"];
    vc.email = _word4Lbl.text;
    vc.phone = _phoneTf.text;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressFindConfirm:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - api
- (void)apiLoginFind {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                      @"login_id", @"api_code",
                      [_model getToken], @"token",
                      _phoneTf.text, @"mber_hp",
                      nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(18);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    _word1Lbl.numberOfLines = 2;
    [_word1Lbl setNotoText:@"가입 시 등록하신\n휴대폰번호를 입력 해 주세요."];
    
    _phoneTf.font = FONT_NOTO_BOLD(14);
    _phoneTf.textColor = COLOR_NATIONS_BLUE;
    _phoneTf.placeholder = @"휴대전화번호('-'제외)";
    
    _phoneTf.delegate = self;
    [_phoneTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _confirmBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_confirmBtn setTitle:@"확  인" forState:UIControlStateNormal];
    [_confirmBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_confirmBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(237, 237, 237) withFrame:_confirmBtn.bounds] forState:UIControlStateNormal];
    [_confirmBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_confirmBtn.bounds] forState:UIControlStateSelected];
    
    _findVw.hidden = true;
    
    _word2Lbl.font = FONT_NOTO_REGULAR(24);
    _word2Lbl.textColor = [UIColor blackColor];
    [_word2Lbl setNotoText:@"아이디를 찾았습니다."];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(18);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"고객님의 아이디입니다."];
    
    _word4Lbl.font = FONT_NOTO_REGULAR(18);
    _word4Lbl.textColor = COLOR_NATIONS_BLUE;
    _word4Lbl.text = @"";
    
    _passFindBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_passFindBtn setTitle:@"비밀번호 찾기" forState:UIControlStateNormal];
    [_passFindBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_passFindBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(143, 143, 143) withFrame:_passFindBtn.bounds] forState:UIControlStateNormal];
    
    _findConfirmBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_findConfirmBtn setTitle:@"확  인" forState:UIControlStateNormal];
    [_findConfirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_findConfirmBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_findConfirmBtn.bounds] forState:UIControlStateNormal];
    
}

@end
