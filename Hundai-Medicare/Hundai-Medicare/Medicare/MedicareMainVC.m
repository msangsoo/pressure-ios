//
//  MedicareMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MedicareMainVC.h"

@interface MedicareMainVC ()

@end

@implementation MedicareMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"메디케어서비스"];
    
    self.webView.scrollView.bounces = false;
    self.webView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://m.shealthcare.co.kr/HL_MED/info/m_info_ios.asp"]];
    [self.webView loadRequest:url];
}

- (void)reloadWebView {
    if (![self.webView.request.URL.absoluteString isEqualToString:@"http://m.shealthcare.co.kr/HL_MED/info/m_info_ios.asp"]) {
        NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://m.shealthcare.co.kr/HL_MED/info/m_info_ios.asp"]];
        [self.webView loadRequest:url];
    }
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webViewDidStartLoad = %@", webView);
    [_model indicatorActive];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"request = %@",request);
    
    NSString *requestStr = [[request URL] absoluteString];
    NSLog(@"requestStr:%@",requestStr);
    
    if ([requestStr hasPrefix:@"tel:"]) {
        NSString *urlStrings = [requestStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        urlStrings = [urlStrings stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        urlStrings = [urlStrings stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", urlStrings]] options:@{} completionHandler:nil];
        
        return false;
    }
    
    return true;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webViewDidFinishLoad = %@", webView);
    [_model indicatorHidden];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

@end
