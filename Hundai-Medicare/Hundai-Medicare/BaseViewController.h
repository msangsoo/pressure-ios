//
//  BaseViewController.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppPageLog.h"
#import "ServerCommunication.h"
#import "MANetwork.h"

typedef enum {
    EAT_ALL = 0,
    EAT_BEFORE,
    EAT_AFTER
} EAT_BEFORE_AFTER;

@interface BaseViewController : UIViewController {
    Model *_model;
    ServerCommunication *server;
    AppPageLog *pageLog;
}

- (void)popBack:(NSString *)viewClass;

@end
