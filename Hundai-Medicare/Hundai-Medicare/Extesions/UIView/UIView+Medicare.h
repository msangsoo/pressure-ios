//
//  UIView+Medicare.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Medicare)

- (void)makeToRadius:(BOOL)mask;

@end
