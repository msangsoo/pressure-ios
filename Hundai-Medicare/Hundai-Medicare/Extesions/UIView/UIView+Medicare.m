//
//  UIView+Medicare.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "UIView+Medicare.h"

@implementation UIView (Medicare)

- (void)makeToRadius:(BOOL)mask {
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = mask;
}

@end
