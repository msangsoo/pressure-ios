//
//  UILabel.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Medicare)

- (void)setNotoText:(NSString *)text;

- (void)setHealthBottom:(NSString *)val unit:(NSString *)unit fontsize:(CGFloat)fontsize;

@end
