//
//  UILabel.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "UILabel+Medicare.h"

@implementation UILabel (Medicare)

- (void)setNotoText:(NSString *)text {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", text]];
    
    [attstr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, attstr.length)];
    
    [attstr addAttribute:NSFontAttributeName
                   value:self.font
                   range:NSMakeRange(0, attstr.length)];
//.   위아래자간 설정
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//    [style1 setLineSpacing:0.01];
    [style setAlignment:self.textAlignment];
    [attstr addAttribute:NSParagraphStyleAttributeName
                        value:style
                        range:NSMakeRange(0, attstr.length)];
    
    
    [self setAttributedText:attstr];
    self.adjustsFontSizeToFitWidth = true;
    self.lineBreakMode = NSLineBreakByTruncatingTail;
}

#pragma mark - Health Text
- (void)setHealthBottom:(NSString *)val unit:(NSString *)unit fontsize:(CGFloat)fontsize {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:val];
    [firstStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(fontsize)
                     range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_ORANGE
                     range:NSMakeRange(0, firstStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:unit];
    [secondStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_LIGHT(fontsize)
                     range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_ORANGE
                     range:NSMakeRange(0, secondStr.length)];
    [attstr appendAttributedString:secondStr];
    
    [self setAttributedText:attstr];
}

@end
