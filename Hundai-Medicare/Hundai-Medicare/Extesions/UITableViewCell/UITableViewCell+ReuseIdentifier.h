//
//  UITableViewCell+ReuseIdentifier.h
//  Hundai-Medicare
//
//  Created by Paul P on 11/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableViewCell (ReuseIdentifier)

+ (NSString *)reuseIdentifier;

@end

NS_ASSUME_NONNULL_END
