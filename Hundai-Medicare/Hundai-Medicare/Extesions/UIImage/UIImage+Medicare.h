//
//  UIImage.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Medicare)

+ (UIImage *)setBackgroundImageByColor:(UIColor *)backgroundColor withFrame:(CGRect)rect;

@end
