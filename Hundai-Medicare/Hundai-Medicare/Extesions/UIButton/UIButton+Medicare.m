//
//  UIButton+Medicare.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "UIButton+Medicare.h"

@implementation UIButton (Medicare)

- (void)mediBaseButton {
    [self setTitleColor:RGB(143, 144, 158) forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(234, 234, 234) withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:self.bounds] forState:UIControlStateSelected];
    
    self.titleLabel.font = FONT_NOTO_BOLD(14);
}

- (void)mediTabButton {
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:self.bounds] forState:UIControlStateSelected];
    
    self.titleLabel.font = FONT_NOTO_REGULAR(16);
}

- (void)mediServiceTabButton {
    [self setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(223, 223, 223) withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:self.bounds] forState:UIControlStateSelected];
    
    self.titleLabel.font = FONT_NOTO_REGULAR(14);
}

- (void)mediHealthTabButton {
    [self setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(237, 237, 237) withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:self.bounds] forState:UIControlStateSelected];
    
    self.titleLabel.font = FONT_NOTO_MEDIUM(14);
}

- (void)mediHealthSecondTabButton {
    [self setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(143, 144, 156) withFrame:self.bounds] forState:UIControlStateSelected];
    
    self.titleLabel.font = FONT_NOTO_MEDIUM(14);
}

- (void)borderRadiusButton:(UIColor *)color {
    self.titleLabel.font = FONT_NOTO_BOLD(16);
    [self setTitleColor:color forState:UIControlStateNormal];
    self.layer.borderWidth = 1;
    self.layer.borderColor = color.CGColor;
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = true;
}

- (void)backgroundRadiusButton:(UIColor *)color {
    self.titleLabel.font = FONT_NOTO_BOLD(16);
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundColor:color];
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = true;
}

- (void)foodKindSelectedBtn {
    self.titleLabel.font = FONT_NOTO_LIGHT(16);
    [self setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:[UIColor whiteColor] withFrame:self.bounds] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(143, 143, 143) withFrame:self.bounds] forState:UIControlStateSelected];
}

@end
