//
//  UIButton+Medicare.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 12/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Medicare)

- (void)mediBaseButton;
- (void)mediTabButton;
- (void)mediServiceTabButton;
- (void)mediHealthTabButton;
- (void)mediHealthSecondTabButton;
- (void)borderRadiusButton:(UIColor *)color;
- (void)backgroundRadiusButton:(UIColor *)color;
- (void)foodKindSelectedBtn;

@end
