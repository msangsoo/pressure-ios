//
//  HomeMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HomeMainVC.h"

#import "CaffeineDataStore.h"

@interface HomeMainVC () {
    Tr_login *login;
    
    NSArray *mission_point_list;
    NSArray *quiz_day;
    NSArray *health_call_list;
    
    int static_total_step;
    
    UIView *bubbleView;
    
    BOOL jumpToOther;
}

@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;

@end

@implementation HomeMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:NICKNAME_FIRST_INIT];
    [userDefault synchronize];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.title = @"홈";
    
    jumpToOther = false;
    
    quiz_day = [[NSArray alloc] init];
    
    DustUtil = [[DustUtils alloc] init];
    
    login = [_model getLoginData];
    
    [CommunityNetwork requestTagList];
    
    [self viewInit];
    
    [self todayPointConfirmAlarm];
    
    [self LogCheck];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(PointAlertClose:)
                                                 name:@"PointAlertClose"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jumpToOtherViewControllerIfNeeded:)
                                                 name:@"notificationPageIfNeed"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
    
    if (!jumpToOther) {
        [self textRefresh];
        
        [self apiMainCall];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    //    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    jumpToOther = false;
    
    UIImage *notiImg = [UIImage imageNamed:@"icon_noti"]; //icon_noti icon_on_noti
    notiImg = [notiImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.notiImgVw.image = notiImg;
    [bubbleView removeFromSuperview];
}

- (void)jumpToOtherViewControllerIfNeeded:(NSNotification *)notification {
    
    [NSTimer scheduledTimerWithTimeInterval:0.3 repeats:NO block:^(NSTimer * _Nonnull timer) {

        NSDictionary *userInfo = notification.userInfo;
        NSString *notiname = [userInfo objectForKey: @"notiname"];
        
        self->jumpToOther = true;
        
        // 1. 공지사항
        if ([notiname isEqualToString:@"ShouldShowNoti"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"0", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
            CommunityAlarmViewController *ac = [storyboard instantiateViewControllerWithIdentifier:@"alarm"];
            [self.navigationController setNavigationBarHidden:NO];
            [self.navigationController pushViewController:ac animated:YES];
        }
        
        // 새건강정보
        else if ([notiname isEqualToString:@"ShouldShowHealthInfo"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"0", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            
            NSString *I_BUFFER2 = [userInfo objectForKey:@"I_BUFFER2"];
            
            HealthInfoDisInfoDetailViewController *vc = [HEALTHINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"HealthInfoDisInfoDetail"];
            
            if([I_BUFFER2 isEqualToString:@"2"])
            {
                // 건강칼럼
                [Utils setUserDefault:@"health_info_title" value:@"건강컬럼"];
                NSString *urlStr = [NSString stringWithFormat:@"%@", [[userInfo objectForKey:@"url"] copy]];
                vc.type = @"web";
                vc.targetUrl = [NSString stringWithFormat:@"%@", urlStr];
                vc.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else{
                //12 건강식단
                [Utils setUserDefault:@"health_info_title" value:@"건강식단"];
                NSString *urlStr = [NSString stringWithFormat:@"%@", [[userInfo objectForKey:@"url"] copy]];
                vc.type = @"webimg";
                vc.targetUrl = [NSString stringWithFormat:@"%@", urlStr];
                vc.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
            
        }
        
        // 4. 게시글 댓글
        else if ([notiname isEqualToString:@"ShouldShowCommunityReply"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"0", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
            CommunityAlarmViewController *ac = [storyboard instantiateViewControllerWithIdentifier:@"alarm"];
            [self.navigationController setNavigationBarHidden:NO];
            [self.navigationController pushViewController:ac animated:YES];
        }
        
        // 5. 일일미션
        else if ([notiname isEqualToString:@"ShouldShowDayMisssion"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"4", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
        }
        
        // 6. 언급
        else if ([notiname isEqualToString:@"ShouldShowCommunityAbout"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"0", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
            CommunityAlarmViewController *ac = [storyboard instantiateViewControllerWithIdentifier:@"alarm"];
            [self.navigationController setNavigationBarHidden:NO];
            [self.navigationController pushViewController:ac animated:YES];
        }
        
        // 7. 게시글 좋아요
        else if ([notiname isEqualToString:@"ShouldShowCommunityLike"]) {
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"0", @"index", @"1", @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityYuriHan" bundle:nil];
            CommunityAlarmViewController *ac = [storyboard instantiateViewControllerWithIdentifier:@"alarm"];
            [self.navigationController setNavigationBarHidden:NO];
            [self.navigationController pushViewController:ac animated:YES];
        }
        
        // 8. 건강관리
        else if ([notiname isEqualToString:@"ShouldShowHealth"]) {
            NSString *index = [userInfo objectForKey:@"index"];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1", @"index", index, @"tab", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
        }
    }];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_main_call"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            [self mainCallViewSetup:result];
        }
        
        if (![[result objectForKey:@"misson_goal_alert_yn"] isEqualToString:@"Y"]) {
            [self healthToDayStepCheck];
        }
        
        [self apiDust];
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"asstb_kbtg_alimi_view_on"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            if ([[result objectForKey:@"chlmReadern"] count] > 0) {
                NSDictionary *item = [[result objectForKey:@"chlmReadern"] firstObject];
                if ([[item objectForKey:@"kbta_idx"] length] > 0) {
                    [self notiNewViewInit:[[result objectForKey:@"chlmReadern"] firstObject]];
                }
            }
        }
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"get_dust"]) {
        [_dustStateLbl setNotoText:[DustUtil getDustQyStatus:[[result objectForKey:@"dusn_qy"] floatValue]]];
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"menu_log_hist"]) {
        [LogDB LogDataDelete];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:APP_FIRST_SERVICE_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_misson_goal_alert"]){
        if ([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            NSString *msg = [result objectForKey:@"misson_goal_txt"];
            [AlertUtils BasicAlertShow:self tag:1019 title:@"" msg:msg left:@"확인" right:@""];
        }
    }
}

#pragma mark - NSNotificationCenter
- (void)PointAlertClose:(NSNotification *)noti {
    [self healthToDayStepCheck];
}

#pragma mark - Actions
- (IBAction)pressNoti:(UIButton *)sender {
    [Utils setUserDefault:MAIN_NOTI_COMFIRM value:@"Y"];
    UIViewController *vc = [NOTI_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressNotiClose:(id)sender {
    [bubbleView removeFromSuperview];
}

- (IBAction)pressPremium:(UIButton *)sender {
    UIViewController *vc = [FREEMIUM_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressDust:(id)sender {
    UIViewController *vc = [HOME_STORYBOARD instantiateViewControllerWithIdentifier:@"DustAlertVC"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressMission:(UIButton *)sender {
    MissionMainVC *vc = [MISSION_STORYBOARD instantiateInitialViewController];
    vc.mission_point_list = mission_point_list;
    vc.quiz_day = [quiz_day firstObject];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressPoint:(UIButton *)sender {
    UIViewController *vc = [MYPOINT_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
    
    //    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"4", @"index", @"1", @"tab", nil];
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
}

- (IBAction)pressCoinText:(UIButton *)sender {
    
}

- (IBAction)pressKeyword:(UIButton *)sender {
    UIViewController *vc = [KEYWORD_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressQuiz:(UIButton *)sender {
    QuizMainVC *vc = [QUIZ_STORYBOARD instantiateInitialViewController];
    vc.item = [quiz_day firstObject];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressCall:(UIButton *)sender {
    if ([login.mber_grad isEqualToString:@"10"]) {
        [LogDB insertDb:HM01 m_cod:HM01003 s_cod:HM01003003];
        
        MainCallAlertVC *vc = [HOME_STORYBOARD instantiateViewControllerWithIdentifier:@"MainCallAlertVC"];
        vc.delegate = self;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
    }else {
        [AlertUtils EmojiDefaultShow:self emoji:error title:@"" msg:@"현대해상 메디케어서비스 미 가입자는\n상담하기를 이용할 수 없습니다.\n\n서비스 가입 및 문의는\n1588-5656에서 가능합니다." left:@"취소" right:@"전화하기"];
    }
}

- (IBAction)pressHealthInfo:(UIButton *)sender {
    UIViewController *vc = [HEALTHINFO_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
    if (tag == 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:15885656"]] options:@{} completionHandler:nil];
    }
}

#pragma mark - MainCallAlertVCDelegate
- (void)selectedCall:(int)index {
    NSString *call = health_call_list[index] ? health_call_list[index][@"call_hp"] : @"";
    call = [call stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([call length] > 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", call]] options:@{} completionHandler:nil];
    }else {
        if (index == 2) {
            [AlertUtils EmojiDefaultShow:self emoji:error title:@"" msg:@"전담간호사 정보가 없습니다.\n\n전담간호사는 암 진단 후\n고객님을 방문하는 간호사 입니다." left:@"확인" right:@""];
        }else {
            [AlertUtils EmojiDefaultShow:self emoji:error title:@"" msg:@"하이플래너 정보가 없습니다.\n\n현대해상 고객센터\n1588-5656 문의 바랍니다." left:@"취소" right:@"전화하기"];
        }
    }
}

#pragma mark - api
- (void)apiMainCall {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMMdd"];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_main_call", @"api_code",
                                login.mber_sn, @"mber_sn",
                                [format stringFromDate:[NSDate date]], @"input_de",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiDust {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"get_dust", @"api_code",
                                @"", @"dust_nm",
                                login.mber_zone, @"dust_sn",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiWalkMission {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_misson_goal_alert", @"api_code",
                                @"1", @"misson_work_typ",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - noti sett
- (void)notiNewViewInit:(NSDictionary *)dic {
    NSString *kbta_idx = [dic objectForKey:@"kbta_idx"];
    if ([[Utils getUserDefault:MAIN_NOTI_IDX] isEqualToString:kbta_idx]) {
        if (![[Utils getUserDefault:MAIN_NOTI_COMFIRM] isEqualToString:@"Y"]) {
            [self bubbleHealthMsg:[dic objectForKey:@"kbt"]];
        }
    }else {
        [Utils setUserDefault:MAIN_NOTI_IDX value:kbta_idx];
        [Utils setUserDefault:MAIN_NOTI_COMFIRM value:@"N"];
        [self bubbleHealthMsg:[dic objectForKey:@"kbt"]];
    }
}

- (void)alimiViewInit:(NSDictionary *)dic {
    NSString *notice_sn = [dic objectForKey:@"notice_sn"];
    if ([[Utils getUserDefault:MAIN_NOTI_IDX] isEqualToString:notice_sn]) {
        if (![[Utils getUserDefault:MAIN_NOTI_COMFIRM] isEqualToString:@"Y"]) {
            [self bubbleHealthMsg:[dic objectForKey:@"alimi_subject"]];
        }
    }else {
        [Utils setUserDefault:MAIN_NOTI_IDX value:notice_sn];
        [Utils setUserDefault:MAIN_NOTI_COMFIRM value:@"N"];
        [self bubbleHealthMsg:[dic objectForKey:@"alimi_subject"]];
    }
}

- (void)bubbleHealthMsg:(NSString *)msg {
    CGFloat yPos = IS_IPHONEX ? 80 : 55;
    bubbleView = [[UIView alloc] initWithFrame:CGRectMake(15 ,yPos ,140 ,28)];
    
    //    bubbleView.layer.borderWidth = 1;
    //    bubbleView.layer.borderColor = COLOR_MAIN_DARK.CGColor;
    //    bubbleView.layer.cornerRadius = 14;
    //    bubbleView.layer.masksToBounds = true;
    //    bubbleView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backImgVw = [[UIImageView alloc] initWithFrame:bubbleView.bounds];
    [backImgVw setBackgroundColor:[UIColor clearColor]];
    UIImage *stretchImage = [[UIImage imageNamed:@"bg_main_bubble"] stretchableImageWithLeftCapWidth:0 topCapHeight:2];
    [backImgVw setImage:stretchImage];
    [bubbleView addSubview:backImgVw];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 124, 20)];
    lbl.textColor = COLOR_MAIN_DARK;
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.lineBreakMode = NSLineBreakByTruncatingTail;
    lbl.numberOfLines = 1;
    lbl.font = FONT_NOTO_MEDIUM(12);
    lbl.textColor = COLOR_MAIN_DARK;
    [lbl setNotoText:msg];
    //    [lbl sizeToFit];
    [bubbleView addSubview:lbl];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:bubbleView.bounds];
    [btn setTitle:@"" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:self action:@selector(pressNoti:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:btn];
    
    //    UIButton *closebtn = [[UIButton alloc] initWithFrame:CGRectMake(125, 4.5, 25, 20)];
    //    [closebtn setImage:[UIImage imageNamed:@"path"] forState:UIControlStateNormal];
    //    [closebtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    //    [closebtn addTarget:self action:@selector(pressNotiClose:) forControlEvents:UIControlEventTouchUpInside];
    //    [bubbleView addSubview:closebtn];
    [self.navigationController.view addSubview:bubbleView];
    
    UIImage *notiImg = [UIImage imageNamed:@"icon_on_noti"]; //icon_noti icon_on_noti
    notiImg = [notiImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.notiImgVw.image = notiImg;
}

#pragma mark - viewInit
- (void)textRefresh {
    [DustUtil getWeather:[DustUtil getCityName:login.mber_zone]];
    [_cityLbl setNotoText:[DustUtil getCityName:login.mber_zone]];
    [_feelLbl setNotoText:[NSString stringWithFormat:@"%@°",[DustUtil getFeelslike]]];
    
    _coinOneImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinTwoImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinThreeImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinFourImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinFiveImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinSixImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    _coinSevenImgVw.image = [UIImage imageNamed:@"icon_1_0_coin"];
    
    [_pointLbl setNotoText:[NSString stringWithFormat:@"%@님의 포인트(가용/누적)", login.mber_nm]];
    
    [self setUsePointText:@"0" totalPoint:@"0"];
    
    _keywordOneLbl.text = @"-";
    _keywordTwoLbl.text = @"-";
    
    _quizOneLbl.text = @"-";
    _quizTwoLbl.text = @"-";
    
    _callOneLbl.text = @"-";
    _callTwoLbl.text = @"-";
    
    _infoOneLbl.text = @"-";
    _infoTwoLbl.text = @"-";
}

- (void)mainCallViewSetup:(NSDictionary *)dic {
    /* day_tip_list */
    NSMutableArray *day_tip_list = [dic objectForKey:@"day_tip_list"];
    
    if (day_tip_list.count == 1) {
        [day_tip_list addObjectsFromArray:[dic objectForKey:@"day_tip_list"]];
    }
    
    if (self.bannerVw.subviews) {
        for (UIView *view in self.bannerVw.subviews) {
            [view removeFromSuperview];
        }
    }
    
    bannerView = [[BannerView alloc] initWithFrame: self.bannerVw.bounds];
    [self.bannerVw addSubview:bannerView];
    [bannerView setData:[day_tip_list copy]];
    [bannerView setBilboard];
    [bannerView startAnimation];
    
    mission_point_list = [dic objectForKey:@"mission_point_list"];
    NSArray *coinImgVwArr = @[_coinOneImgVw, _coinTwoImgVw, _coinThreeImgVw, _coinFourImgVw, _coinFiveImgVw, _coinSixImgVw, _coinSevenImgVw];
    NSArray *coinLblwArr = @[_coinOneLbl, _coinTwoLbl, _coinThreeLbl, _coinFourLbl, _coinFiveLbl, _coinSixLbl, _coinSevenLbl];
    NSArray *coinBtnArr = @[_coinOneBtn, _coinTwoBtn, _coinThreeBtn, _coinFourBtn, _coinFiveBtn, _coinSixBtn, _coinSevenBtn];
    
    for (UIView *view in coinImgVwArr) {
        view.hidden = true;
    }
    for (UIView *view in coinLblwArr) {
        view.hidden = true;
    }
    for (UIView *view in coinBtnArr) {
        view.hidden = true;
    }
    
    int count = 0;
    for (NSDictionary *item in mission_point_list) {
        int point_max_day_amt = [item[@"point_max_day_amt"] intValue];
        int accml_amt = [item[@"accml_amt"] intValue];
        
        if (point_max_day_amt > 8) {
            point_max_day_amt = 8;
        }
        
        if (accml_amt < 0) {
            accml_amt = 0;
        }
        
        UILabel *coinLbl = coinLblwArr[count];
        coinLbl.font = FONT_NOTO_BOLD(10);
        coinLbl.textColor = COLOR_GRAY_SUIT;
        [coinLbl setNotoText:item[@"point_nm"]];
        coinLbl.hidden = false;
        
        coinLbl.textColor = COLOR_GRAY_SUIT;
        if (point_max_day_amt <= accml_amt) {
            accml_amt = point_max_day_amt;
            coinLbl.textColor = COLOR_MAIN_ORANGE;
        }
        
        UIImageView *coinImgVw = coinImgVwArr[count];
        coinImgVw.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%d_%d_coin", point_max_day_amt, accml_amt]];
        coinImgVw.hidden = false;
        
        UIButton *coinBtn = coinBtnArr[count];
        coinBtn.tag = count;
        coinBtn.hidden = false;
        
        count ++;
    }
    
    [_pointLbl setNotoText:[NSString stringWithFormat:@"%@님의 포인트(가용/누적)", login.mber_nm]];
    
    [self setUsePointText:[dic objectForKey:@"user_point_amt"] totalPoint:[dic objectForKey:@"total_accml_amt"]];
    //    login.point_total_amt
    
    NSArray *keyword_list = [dic objectForKey:@"keyword_list"];
    count = 0;
    for (NSDictionary *dic in keyword_list) {
        if (count < 2) {
            [@[_keywordOneLbl, _keywordTwoLbl][count] setNotoText:[NSString stringWithFormat:@"%d위.%@",(count + 1),  dic[@"keyword_nm"]]];
        }
        count ++;
    }
    
    quiz_day = [dic objectForKey:@"quiz_day"];
    if (quiz_day.count > 0) {
        [_quizOneLbl setNotoText:[NSString stringWithFormat:@"%@", quiz_day[0][@"quiz_question"]]];
        [_quizTwoLbl setNotoText:@""];
    }
    
    NSArray *health_info_list = [dic objectForKey:@"health_info_list"];
    count = 0;
    for (NSDictionary *dic in health_info_list) {
        if (count < 2) {
            [@[_infoOneLbl, _infoTwoLbl][count] setNotoText:[NSString stringWithFormat:@"%@", dic[@"health_nm"]]];
        }
        count ++;
    }
    
    health_call_list = [dic objectForKey:@"health_call_list"];
    
    count = 0;
    for (NSDictionary *dic in health_call_list) {
        if (count == 0) {
            [_callOneLbl setNotoText:[NSString stringWithFormat:@"%@/",dic[@"call_nm"]]];
        }else if (count == 1) {
            [_callOneLbl setNotoText:[NSString stringWithFormat:@"%@%@/",_callOneLbl.text, dic[@"call_nm"]]];
        }else if (count == 2) {
            [_callTwoLbl setNotoText:[NSString stringWithFormat:@"%@",dic[@"call_nm"]]];
        }
        count ++;
    }
    
    NSArray *alimi_list = [dic objectForKey:@"alimi_list"];
    if ([alimi_list count] > 0) {
        NSDictionary *alimiItem = [alimi_list firstObject];
        if ([[alimiItem objectForKey:@"alimi_yn"] isEqualToString:@"Y"]) {
            [self alimiViewInit:alimiItem];
        }
    }
    
    if ([[dic objectForKey:@"misson_goal_alert_yn"] isEqualToString:@"Y"]) {
        [AlertUtils PointAlertShow:self point:[dic objectForKey:@"misson_goal_point"] pointTitle:[NSString stringWithFormat:@"안녕하세요?\n오늘도 메디케어 서비스와 함께\n건강한 하루보내세요.\n\n로그인 포인트(%@)가\n지급되었습니다.",[dic objectForKey:@"misson_goal_point"]] pointInfo:@""];
    }
}

- (void)viewInit {
    _premiumImgVw.hidden = true;
    _premiumBtn.hidden = true;
#if DEBUG
    _premiumImgVw.hidden = false;
    _premiumBtn.hidden = false;
#endif
    
    _cityLbl.font = FONT_NOTO_REGULAR(14);
    _cityLbl.textColor = COLOR_MAIN_DARK;
    [_cityLbl setNotoText:@"성남시"];
    
    _feelLbl.font = FONT_NOTO_BOLD(14);
    _feelLbl.textColor = COLOR_MAIN_DARK;
    [_feelLbl setNotoText:@"-4C"];
    
    _dustLbl.font = FONT_NOTO_REGULAR(14);
    _dustLbl.textColor = COLOR_GRAY_SUIT;
    [_dustLbl setNotoText:@"미세먼지"];
    
    _dustStateLbl.font = FONT_NOTO_MEDIUM(14);
    _dustStateLbl.textColor = COLOR_MAIN_DARK;
    [_dustStateLbl setNotoText:@"보통"];
    
    [_bannerVw makeToRadius:true];
    
    [self setMissionLblText];
    
    _coinOneImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinTwoImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinThreeImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinFourImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinFiveImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinSixImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    _coinSevenImgVw.image = [UIImage imageNamed:@"icon_1_un_coin"];
    
    _coinOneLbl.font = FONT_NOTO_BOLD(11);
    _coinOneLbl.textColor = COLOR_GRAY_SUIT;
    [_coinOneLbl setNotoText:@"로그인"];
    
    _coinTwoLbl.font = FONT_NOTO_BOLD(11);
    _coinTwoLbl.textColor = COLOR_GRAY_SUIT;
    [_coinTwoLbl setNotoText:@"건강기록"];
    
    _coinThreeLbl.font = FONT_NOTO_BOLD(11);
    _coinThreeLbl.textColor = COLOR_GRAY_SUIT;
    [_coinThreeLbl setNotoText:@"일일퀴즈"];
    
    _coinFourLbl.font = FONT_NOTO_BOLD(11);
    _coinFourLbl.textColor = COLOR_GRAY_SUIT;
    [_coinFourLbl setNotoText:@"건강정보"];
    
    _coinFiveLbl.font = FONT_NOTO_BOLD(11);
    _coinFiveLbl.textColor = COLOR_GRAY_SUIT;
    [_coinFiveLbl setNotoText:@"걸음 수"];
    
    _coinSixLbl.font = FONT_NOTO_BOLD(11);
    _coinSixLbl.textColor = COLOR_GRAY_SUIT;
    [_coinSixLbl setNotoText:@"글쓰기"];
    
    _coinSevenLbl.font = FONT_NOTO_BOLD(11);
    _coinSevenLbl.textColor = COLOR_GRAY_SUIT;
    [_coinSevenLbl setNotoText:@"댓글쓰기"];
    
    _pointLbl.font = FONT_NOTO_REGULAR(14);
    _pointLbl.textColor = [UIColor whiteColor];
    [_pointLbl setNotoText:@"홍길동님의 포인트(가용/누적)"];
    
    _pointValLbl.text = @"";
    
    _keywordLbl.font = FONT_NOTO_REGULAR(18);
    _keywordLbl.textColor = COLOR_NATIONS_BLUE;
    [_keywordLbl setNotoText:@"상담 키워드"];
    
    _keywordOneLbl.font = FONT_NOTO_REGULAR(12);
    _keywordOneLbl.textColor = COLOR_MAIN_DARK;
    _keywordOneLbl.minimumScaleFactor = 1.0;
    _keywordOneLbl.numberOfLines = 2;
    _keywordOneLbl.text = @"";
    
    _keywordTwoLbl.font = FONT_NOTO_REGULAR(12);
    _keywordTwoLbl.textColor = COLOR_MAIN_DARK;
    _keywordTwoLbl.minimumScaleFactor = 1.0;
    _keywordTwoLbl.numberOfLines = 2;
    _keywordTwoLbl.text = @"";
    
    _quizLbl.font = FONT_NOTO_REGULAR(18);
    _quizLbl.textColor = COLOR_NATIONS_BLUE;
    [_quizLbl setNotoText:@"일일퀴즈"];
    
    _quizOneLbl.font = FONT_NOTO_REGULAR(12);
    _quizOneLbl.textColor = COLOR_MAIN_DARK;
    _quizOneLbl.minimumScaleFactor = 1.0;
    _quizOneLbl.numberOfLines = 2;
    _quizOneLbl.text = @"";
    
    _quizTwoLbl.font = FONT_NOTO_REGULAR(12);
    _quizTwoLbl.textColor = COLOR_GRAY_SUIT;
    _quizTwoLbl.minimumScaleFactor = 1.0;
    _quizTwoLbl.numberOfLines = 2;
    _quizTwoLbl.text = @"";
    
    _callLbl.font = FONT_NOTO_REGULAR(18);
    _callLbl.textColor = COLOR_NATIONS_BLUE;
    [_callLbl setNotoText:@"전화하기"];
    
    _callOneLbl.font = FONT_NOTO_REGULAR(12);
    _callOneLbl.textColor = COLOR_MAIN_DARK;
    _callOneLbl.minimumScaleFactor = 1.0;
    _callOneLbl.numberOfLines = 2;
    _callOneLbl.text = @"";
    
    _callTwoLbl.font = FONT_NOTO_REGULAR(12);
    _callTwoLbl.textColor = COLOR_MAIN_DARK;
    _callTwoLbl.minimumScaleFactor = 1.0;
    _callTwoLbl.numberOfLines = 2;
    _callTwoLbl.text = @"";
    
    _infoLbl.font = FONT_NOTO_REGULAR(18);
    _infoLbl.textColor = COLOR_NATIONS_BLUE;
    [_infoLbl setNotoText:@"건강정보"];
    
    _infoOneLbl.font = FONT_NOTO_REGULAR(12);
    _infoOneLbl.minimumScaleFactor = 1.0;
    _infoOneLbl.numberOfLines = 2;
    _infoOneLbl.textColor = COLOR_MAIN_DARK;
    _infoOneLbl.text = @"";
    
    _infoTwoLbl.font = FONT_NOTO_REGULAR(12);
    _infoTwoLbl.minimumScaleFactor = 1.0;
    _infoTwoLbl.numberOfLines = 2;
    _infoTwoLbl.textColor = COLOR_MAIN_DARK;
    _infoTwoLbl.text = @"";
    
    /// 940:520
    if (IS_IPHONEX) {
        self.bottomVwHeightConst.constant = ((DEVICE_WIDTH - 20.f) / 940.f) * 620.f ;
    }else {
        self.bottomVwHeightConst.constant = ((DEVICE_WIDTH - 20.f) / 940.f) * 520.f;
    }
}

- (void)setMissionLblText {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:@"일일미션    "];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(18)
                     range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_NATIONS_BLUE
                     range:NSMakeRange(0, firstStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:@"완료하고"];
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, @"완료하고".length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_THIN(12)
                      range:NSMakeRange(0, @"완료하고".length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:COLOR_GRAY_SUIT
                      range:NSMakeRange(0, @"완료하고".length)];
    [attstr appendAttributedString:secondStr];
    
    NSMutableAttributedString *thirdStr = [[NSMutableAttributedString alloc] initWithString:@"포인트"];
    [thirdStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @"포인트".length)];
    [thirdStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(12)
                     range:NSMakeRange(0, @"포인트".length)];
    [thirdStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_DARK
                     range:NSMakeRange(0, @"포인트".length)];
    [attstr appendAttributedString:thirdStr];
    
    NSMutableAttributedString *fourStr = [[NSMutableAttributedString alloc] initWithString:@"받으세요."];
    [fourStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, @"받으세요.".length)];
    [fourStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_THIN(12)
                    range:NSMakeRange(0, @"받으세요.".length)];
    [fourStr addAttribute:NSForegroundColorAttributeName
                    value:COLOR_GRAY_SUIT
                    range:NSMakeRange(0, @"받으세요.".length)];
    [attstr appendAttributedString:fourStr];
    
    self.missionLbl.attributedText = attstr;
}

#pragma mark - text method
- (void)setUsePointText:(NSString *)point totalPoint:(NSString *)totalPoint {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *pointStr = [[NSMutableAttributedString alloc] initWithString:point];
    [pointStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, point.length)];
    [pointStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(24)
                     range:NSMakeRange(0, point.length)];
    [pointStr addAttribute:NSForegroundColorAttributeName
                     value:RGB(246, 195, 46)
                     range:NSMakeRange(0, point.length)];
    [attstr appendAttributedString:pointStr];
    
    NSMutableAttributedString *underStr = [[NSMutableAttributedString alloc] initWithString:@" / "];
    [underStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @" / ".length)];
    [underStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(12)
                     range:NSMakeRange(0, @" / ".length)];
    [underStr addAttribute:NSForegroundColorAttributeName
                     value:RGB(246, 195, 46)
                     range:NSMakeRange(0, @" / ".length)];
    [attstr appendAttributedString:underStr];
    
    NSMutableAttributedString *totalPointStr = [[NSMutableAttributedString alloc] initWithString:totalPoint];
    [totalPointStr addAttribute:NSKernAttributeName
                          value:[NSNumber numberWithFloat:number]
                          range:NSMakeRange(0, totalPoint.length)];
    [totalPointStr addAttribute:NSFontAttributeName
                          value:FONT_NOTO_REGULAR(24)
                          range:NSMakeRange(0, totalPoint.length)];
    [totalPointStr addAttribute:NSForegroundColorAttributeName
                          value:RGB(246, 195, 46)
                          range:NSMakeRange(0, totalPoint.length)];
    [attstr appendAttributedString:totalPointStr];
    
    NSMutableAttributedString *LastStr = [[NSMutableAttributedString alloc] initWithString:@" P"];
    [LastStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, @" P".length)];
    [LastStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_THIN(24)
                    range:NSMakeRange(0, @" P".length)];
    [LastStr addAttribute:NSForegroundColorAttributeName
                    value:[UIColor whiteColor]
                    range:NSMakeRange(0, @" P".length)];
    [attstr appendAttributedString:LastStr];
    
    self.pointValLbl.attributedText = attstr;
}

#pragma mark - Marque Label Method
-(void)marqueLabel:(MarqueeLabel*)_marqueLabel {
    _marqueLabel.marqueeType = MLContinuous;
    _marqueLabel.fadeLength = 0.0f;
    _marqueLabel.animationDelay = 0.f;
    _marqueLabel.scrollDuration = 10.0f;
    _marqueLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    _marqueLabel.leadingBuffer = 5.0f;
    _marqueLabel.trailingBuffer = 5.f;
}

#pragma mark - health kit
- (void)healthToDayStepCheck {
    timeUtils = [[TimeUtils alloc] init];
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    self.caffeineDataStore = [[CaffeineDataStore alloc] initWithHealthStore:healthStore];
    
    static_total_step = 0;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self.caffeineDataStore healthKitSamplesWithCopletion:[[self->timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] endData:[[self->timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] :^(NSArray *samples, NSError *error) {
            if(samples.count> 0) {
                NSDateFormatter *dft = [[NSDateFormatter alloc] init];
                [dft setDateFormat:@"HH"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    int hourCount = 1;
                    
                    for(HKQuantitySample *sample in samples) {
                        NSLog(@"countUnit = %f, endDate = %@", [sample.quantity doubleValueForUnit:[HKUnit countUnit]], sample.endDate);
                        
                        while (hourCount < [[dft stringFromDate:sample.endDate] intValue]) {
                            NSLog(@"hourCount = %d : sample.endDate = %d" ,hourCount ,[[dft stringFromDate:sample.endDate] intValue]);
                            hourCount ++;
                        }
                        
                        if(hourCount == [[dft stringFromDate:sample.endDate] intValue]) {
                            self->static_total_step = [[NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]] intValue] + self->static_total_step;
                            
                            NSLog(@"self->static_total_step = %d", self->static_total_step);
                            if (self->static_total_step >= 10000) {
                                
                                NSString *nowDate = [TimeUtils getNowDateFormat:@"yyyyMMdd"];
                                if ([Utils getUserDefault:MAIN_WORK_MISSION_DATE]) {
                                    NSString *oldDate = [Utils getUserDefault:MAIN_WORK_MISSION_DATE];
                                    if (![oldDate isEqualToString:nowDate]) {
                                        [self apiWalkMission];
                                    }
                                }
                                [Utils setUserDefault:MAIN_WORK_MISSION_DATE value:nowDate];
                                return;
                            }
                        }
                    }
                });
            }
            
        }];
    });
}

- (void)todayPointConfirmAlarm {
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    
    for (int i=0; i<[eventArray count]; i++) {
        UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
        if ([uid isEqualToString:TODAY_POINT_CONFIRM_ALARM]) {
            //            NSLog(@"uid : %@", uid);
            [app cancelLocalNotification:oneEvent];
        }
    }
    
    // 알림 시간 설정
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    
    dateComponents.hour = 10;
    
    dateComponents.minute = 0;
    
    dateComponents.second = 0;
    
    // Schedule the notification
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.fireDate = [calendar dateFromComponents:dateComponents];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.repeatInterval = NSCalendarUnitDay;
    localNotification.alertTitle = @"";
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:TODAY_POINT_CONFIRM_ALARM forKey:@"notificationID"];
    localNotification.userInfo = userInfo;
    localNotification.alertBody = @"어제 획득한 포인트를 확인해보세요.";
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //    NSLog(@"dateComponents : %@", dateComponents);
}

#pragma mark - Log
- (void)LogCheck {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:APP_FIRST_SERVICE_DATE]) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyyMMdd"];
        int nowDate = [[format stringFromDate:[NSDate date]] intValue];
        int beforeDate = [[format stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:APP_FIRST_SERVICE_DATE]] intValue];
        if (nowDate > beforeDate) {
            NSArray *logArr = [LogDB todayAllSelect];
            if (logArr.count > 0) {
                NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            @"menu_log_hist", @"api_code",
                                            [NSString stringWithFormat:@"%d", (int)logArr.count], @"DATA_LENGTH",
                                            logArr, @"DATA",
                                            nil];
                
                NSLog(@"log parameters = %@", parameters);
                if(parameters != nil) {
                    if(server == nil) {
                        server = [[ServerCommunication alloc] init];
                        server.delegate = self;
                    }
                    [_model indicatorActive];
                    [server serverCommunicationData:parameters];
                }else {
                    NSLog(@"server parameters error = %@", parameters);
                }
            }
        }
    }else {
        [LogDB LogDataDelete];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:APP_FIRST_SERVICE_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1019) {
        [self apiMainCall];
    }
}
@end
