//
//  FreemiumHistoryListVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FreemiumHistoryDetailVC.h"

@interface FreemiumHistoryListVC : UIViewController

@property (strong, nonatomic) NSString *type;

@property (weak, nonatomic) IBOutlet UIStackView *serviceVw;
@property (weak, nonatomic) IBOutlet UIStackView *cancerVw;

@property (weak, nonatomic) IBOutlet UIButton *service1Btn;
@property (weak, nonatomic) IBOutlet UIButton *service2Btn;
@property (weak, nonatomic) IBOutlet UIButton *service3Btn;
@property (weak, nonatomic) IBOutlet UIButton *service4Btn;
@property (weak, nonatomic) IBOutlet UIButton *service5Btn;

@property (weak, nonatomic) IBOutlet UIButton *cancer1Btn;
@property (weak, nonatomic) IBOutlet UIButton *cancer2Btn;
@property (weak, nonatomic) IBOutlet UIButton *cancer3Btn;

@end
