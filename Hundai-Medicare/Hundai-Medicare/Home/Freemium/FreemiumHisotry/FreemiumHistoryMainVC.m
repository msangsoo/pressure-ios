//
//  FreemiumHistoryMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FreemiumHistoryMainVC.h"

@interface FreemiumHistoryMainVC ()

@end

@implementation FreemiumHistoryMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"서비스 이력보기"];
    
    [self viewInit];
}

#pragma mark - Actions
- (IBAction)pressServiceTap:(UIButton *)sender {
    NSString *serviceType = @"";
    if (sender.tag == 0) {
        serviceType = @"service";
    }else {
        serviceType = @"cancer";
    }
    
    FreemiumHistoryListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FreemiumHistoryListVC"];
    vc.type = serviceType;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - viewInit
- (void)viewInit {
    self.serviceBtn.tag = 0;
    self.cancerBtn.tag = 1;
}

@end
