//
//  FreemiumHistoryMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FreemiumHistoryListVC.h"

@interface FreemiumHistoryMainVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancerBtn;

@end
