//
//  FreemiumHistoryDetailVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FreemiumHistoryDetailVC.h"

@interface FreemiumHistoryDetailVC ()

@property (strong,nonatomic) NSString *strRemainText;
@property (strong,nonatomic) NSMutableAttributedString *strAtbtTipText;
@property (strong,nonatomic) NSMutableArray *arrayMutMaster;

@end

@implementation FreemiumHistoryDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"세부 이력보기"];
    
    UINib *cellNib ;
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell01" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell01"];
    
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell02" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell02"];
    
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell03" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell03"];
    
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell04" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell04"];
    
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell05" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell05"];
    
    cellNib = [UINib nibWithNibName:@"ServiceDetailCell06" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ServiceDetailCell06"];
    
    _strRemainText = @"";
    _strAtbtTipText = [[NSMutableAttributedString alloc] init];
    _arrayMutMaster = [[NSMutableArray alloc] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self viewInit];
    
    [self apiDetail];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"self.arrayMutMaster.count:%ld",self.arrayMutMaster.count);
    
    return self.arrayMutMaster.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([_strApiCode isEqualToString:@"DZ002"]) {
        return 270;
    }else if ([_strApiCode isEqualToString:@"DZ003"]) {
        return 224;
    }else if ([_strApiCode isEqualToString:@"DZ004"]) {
        return 188;
    }else if ([_strApiCode isEqualToString:@"DZ005"]) {
        return 188;
    }else if ([_strApiCode isEqualToString:@"DZ006"]) {
        return 188;
    }else if([_strApiCode isEqualToString:@"DZ007"]) {
        return 270;
    }else if([_strApiCode isEqualToString:@"DZ009"]) {
        
        return 302;
    }else if([_strApiCode isEqualToString:@"DZ008"] ){
        if(indexPath.row == 0) {
            return 188;
        }else{
            return 227;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([_strApiCode isEqualToString:@"DZ002"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell01" forIndexPath:indexPath];
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        cell.lblCount.text = [NSString stringWithFormat:@"%d회", (int)indexPath.row+1];
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        cell.strSO_NAME.text = dicRow[@"SO_NAME"];
        cell.strUSE_MEMO.text = dicRow[@"USE_MEMO"];
        cell.strSTATE_NAME.text = dicRow[@"STATE_NAME"];
        return cell;
        
    }else if ([_strApiCode isEqualToString:@"DZ003"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell02" forIndexPath:indexPath];
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        cell.lblCount.text = [NSString stringWithFormat:@"면역세포보관(지원횟수 %d회)",(int)indexPath.row+1];
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        cell.strSO_NAME.text = dicRow[@"SO_NAME"];
        cell.strUSE_MEMO.text = dicRow[@"USE_MEMO"];
        return cell;
        
    }else if ([_strApiCode isEqualToString:@"DZ004"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell03" forIndexPath:indexPath];
        // 서비스구분 (A:맞춤형 운동영양서비스,B: 항암약선요리강습,C:재활운동기기 제공(워키디커피) )
        
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        if ([dicRow[@"SGUBUN"]isEqualToString:@"A"])  {
            cell.lblCount.text = @"";
            cell.strREQDATE.text = dicRow[@"REQDATE"];
            cell.strUSEDATE.text = dicRow[@"ENDDATE"];
            cell.strSO_NAME.text = dicRow[@"STATE_NAME"];
            cell.lblCellTitle3.text = @"서비스상태";
        }else if ([dicRow[@"SGUBUN"]isEqualToString:@"B"]) {
            cell.lblCount.text = [NSString stringWithFormat:@"항암 약선요리 강습(지원횟수 1회)"];
            
            cell.strREQDATE.text = dicRow[@"REQDATE"];
            cell.strUSEDATE.text = dicRow[@"USEDATE"];
            cell.strSO_NAME.text = dicRow[@"USE_MEMO"];
            cell.lblCellTitle3.text = @"이용내역";
        }else if ([dicRow[@"SGUBUN"]isEqualToString:@"C"]) {
            cell.lblCount.text = [NSString stringWithFormat:@"재활운동기기 제공(지원횟수 1회)"];
            
            cell.strREQDATE.text = dicRow[@"REQDATE"];
            cell.strUSEDATE.text = dicRow[@"USEDATE"];
            cell.strSO_NAME.text = dicRow[@"USE_MEMO"];
            cell.lblCellTitle3.text = @"제공제품";
        }
        
        
        return cell;
    }else if ([_strApiCode isEqualToString:@"DZ005"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell03" forIndexPath:indexPath];
        
        
        NSDictionary *dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        if (indexPath.row == 0) {
            cell.lblCount.text = [NSString stringWithFormat:@"면역력 검사(지원횟수 각1회)"];
        }else if (indexPath.row == 1) {
            cell.lblCount.text = [NSString stringWithFormat:@"세포건강도 검사(지원횟수 각1회)"];
        }
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        cell.strSO_NAME.text = dicRow[@"SO_NAME"];
        return cell;
    }else if ([_strApiCode isEqualToString:@"DZ006"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell03" forIndexPath:indexPath];
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        cell.lblCount.text = [NSString stringWithFormat:@"PET-CT(지원횟수 1회)"];
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        cell.strSO_NAME.text = dicRow[@"SO_NAME"];
        return cell;
    }else if ([_strApiCode isEqualToString:@"DZ007"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell01" forIndexPath:indexPath];
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        cell.lblCount.text = [NSString stringWithFormat:@"%d회",(int)indexPath.row+1];
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        cell.strSO_NAME.text = dicRow[@"SO_NAME"];
        cell.strUSE_MEMO.text = dicRow[@"USE_MEMO"];
        cell.strSTATE_NAME.text = dicRow[@"STATE_NAME"];
        return cell;
        
    }else if([_strApiCode isEqualToString:@"DZ009"]) {
        ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell04" forIndexPath:indexPath];
        NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
        
        cell.strREQDATE.text = dicRow[@"REQDATE"];
        cell.strCONFDATE.text = dicRow[@"CONFDATE"];
        cell.strGOODS_NAME.text = self.serviceCountTitleLbl.text;
        cell.strENDDATE.text = dicRow[@"ENDDATE"];
        cell.strRTNDATE.text = dicRow[@"RTNDATE"];
        cell.strUSEDATE.text = dicRow[@"USEDATE"];
        
        return cell;
    }else if([_strApiCode isEqualToString:@"DZ008"]) {
        
        NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
        [fmt setNumberStyle:NSNumberFormatterDecimalStyle];
        
        if(indexPath.row == 0) {
            ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell05" forIndexPath:indexPath];
            NSDictionary * dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
            
            cell.strREQDATE.text = [NSString stringWithFormat:@"%@ 원",[fmt stringFromNumber:[NSNumber numberWithInt:[dicRow[@"TOTAL_MONEY"] intValue]]]];
            cell.strUSEDATE.text = [NSString stringWithFormat:@"%@ 원",[fmt stringFromNumber:[NSNumber numberWithInt:[dicRow[@"UCOST"] intValue]]]];
            int money = [dicRow[@"TOTAL_MONEY"] intValue] - [dicRow[@"UCOST"] intValue];
            cell.strSO_NAME.text = [NSString stringWithFormat:@"%@ 원",[fmt stringFromNumber:[NSNumber numberWithInt:money]]];
            return cell;
        }else {
            ServiceDetailCell01 *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceDetailCell06" forIndexPath:indexPath];
            NSDictionary *dicRow = [self.arrayMutMaster objectAtIndex:indexPath.row];
            
            cell.lblCount.text = [NSString stringWithFormat:@"%ld회",indexPath.row];
            cell.strREQDATE.text = dicRow[@"USEDATE"];
            cell.strUSEDATE.text = [NSString stringWithFormat:@"%@ - %@ (%@)",dicRow[@"DEPARTADDRES"],dicRow[@"ARRIVALADDRES"],dicRow[@"ODIVISION"]];
            cell.strSO_NAME.text = [NSString stringWithFormat:@"%@km",dicRow[@"DISTANCE"]];
            cell.strUSE_MEMO.text = [NSString stringWithFormat:@"%@ 원",[fmt stringFromNumber:[NSNumber numberWithInt:[dicRow[@"UCOST"] intValue]]]];
            
            return cell;
        }
        
    }
    
    return nil;
}

#pragma mark - api
- (void)apiDetail {
    NSString *SEQ = @"";
    Tr_login *login = [_model getLoginData];
    SEQ = login.seq;
    
#if DEBUG
    SEQ = @"0001013000015";
#endif
    
    NSDictionary *params = @{@"DOCNO": _strApiCode,
                             @"SEQ": SEQ
                             };
    
    [MANetwork PostDataAsync:@"https://m.shealthcare.co.kr/HL_MED_COMMUNITY/ws.asmx/getJson" paramString:params success:^(id resultSet) {
        NSLog(@"%@", resultSet);
        
        if ([resultSet[@"DATA"][0][@"RESULT_CODE"] isEqualToString:@"0000"]) {
            if ([self->_strApiCode isEqualToString:@"DZ002"]) { //심리 케어
                self.arrayMutMaster = resultSet[@"DATA"];
                self.titleLbl.text = @"심리케어(지원횟수 5회)";
                NSString * strTextRed = [NSString stringWithFormat:@"%d회",(5-[resultSet[@"AST_LENGTH"]intValue])];
                NSString * strText = [NSString stringWithFormat:@"서비스 이용 가능 횟수: %@",strTextRed];
                NSMutableAttributedString *strAtbtTipText = [[NSMutableAttributedString alloc]initWithString:strText];
                NSRange  range =[strText rangeOfString:strTextRed];
                [strAtbtTipText addAttribute:NSForegroundColorAttributeName value:COLOR_MAIN_RED range:range];
                
                self.serviceCountTitleLbl.attributedText = strAtbtTipText;
                
            }else if ([self->_strApiCode isEqualToString:@"DZ003"]) {
                self.arrayMutMaster = resultSet[@"DATA"];
                self.titleLbl.text = @"";
                
            }else if ([self->_strApiCode isEqualToString:@"DZ004"]) {
                
                self.serviceCountTitleLbl.text = @"서비스 잔여 기간:";
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init] ;
                [dateFormat setDateFormat:@"yyyy-MM-dd"];
                
                NSDate *dateToday = [NSDate date];
                
                //순서는 A,C,B순서로 나와야 함
                
                NSArray * arrayCode = @[@"A",@"C",@"B"];
                for (int index = 0;index< [arrayCode count]; index++) {
                    for (NSDictionary * dicRow in resultSet[@"DATA"]) {
                        if ([dicRow[@"SGUBUN"]isEqualToString:arrayCode[index]]) {
                            [self.arrayMutMaster addObject:dicRow];
                        }
                    }
                }
                
                NSDate *dateFrom =[dateFormat dateFromString:self.arrayMutMaster[0][@"ENDDATE"]];
                
                NSLog(@"Date1  : %@", [dateFormat stringFromDate:dateToday]);
                NSLog(@"Date2  : %@", [dateFormat stringFromDate:dateFrom]);
                
                //date1과 date2의 차이를 dateComp변수에 저장
                NSDateComponents *dateComp;
                
                //[dateFrom dateByAddingTimeInterval:+60*60*24]
                dateComp = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:dateToday  toDate:dateFrom options:0];
                
                self.titleLbl.text = @"맞춤형 운동·영양관리(지원기간 5년)";
                NSString * strTextRed = [NSString stringWithFormat:@"%d일",(int)[dateComp day ]];
                NSString * strText = [NSString stringWithFormat:@"서비스 잔여 기간: %@",strTextRed];
                self->_strAtbtTipText = [[NSMutableAttributedString alloc]initWithString:strText];
                NSRange  range =[strText rangeOfString:strTextRed];
                [self->_strAtbtTipText addAttribute:NSForegroundColorAttributeName value:COLOR_MAIN_RED range:range];
                
                self.serviceCountTitleLbl.attributedText = self->_strAtbtTipText;
                
            }else if ([self->_strApiCode isEqualToString:@"DZ005"]) {
                self.arrayMutMaster = resultSet[@"DATA"];
                self.titleLbl.text = @"";
                
            }else if ([self->_strApiCode isEqualToString:@"DZ006"]) {
                self.arrayMutMaster = resultSet[@"DATA"];
                
            }else if ([self->_strApiCode isEqualToString:@"DZ007"]){
                
                self.arrayMutMaster = resultSet[@"DATA"];
                
                self.titleLbl.text = @"방문 재활지원서비스(지원횟수 4회)";
                
                NSString * strTextRed = [NSString stringWithFormat:@"%d회",(4 - [resultSet[@"AST_LENGTH"]intValue])];
                NSString * strText = [NSString stringWithFormat:@"서비스 이용 가능 횟수: %@",strTextRed];
                NSMutableAttributedString *strAtbtTipText = [[NSMutableAttributedString alloc]initWithString:strText];
                NSRange  range =[strText rangeOfString:strTextRed];
                [strAtbtTipText addAttribute:NSForegroundColorAttributeName value:COLOR_MAIN_RED range:range];
                
                self.serviceCountTitleLbl.attributedText = strAtbtTipText;
                
            }else if ([self->_strApiCode isEqualToString:@"DZ009"]) {
                self.arrayMutMaster = resultSet[@"DATA"];
                
                self.titleLbl.text = @"스마트 재활지원서비스";
                
                self.serviceCountTitleLbl.text = resultSet[@"GOODS_NAME"];
                self.serviceCountTitleLbl.hidden = YES;
            }else if ([self->_strApiCode isEqualToString:@"DZ008"]) {
                int totalUCOST = 0;
                for (int i = 0 ; i < [resultSet[@"AST_LENGTH"] intValue] ; i ++) {
                    totalUCOST += [resultSet[@"DATA"][i][@"UCOST"] intValue];
                }
                NSDictionary *dict1 = @{@"TOTAL_MONEY": resultSet[@"TOTAL_MONEY"], @"UCOST": [NSString stringWithFormat:@"%d",totalUCOST]};
                [resultSet[@"DATA"] insertObject:dict1 atIndex:0];
                self.arrayMutMaster = resultSet[@"DATA"];
                
                self.titleLbl.text = @"이송차량 지원서비스";
            }
            
            [self.tableView reloadData];
            
        }else if ([resultSet[@"DATA"][0][@"RESULT_CODE"] isEqualToString:@"4444"]) {
            self.arrayMutMaster = resultSet[@"DATA"];
            
            self.titleLbl.text = @"";
            NSString *strMsg = @"등록된 이력이 없습니다.";
            [Utils basicAlertView:self withTitle:@"" withMsg:strMsg];
        }
        
    } failure:^(NSError *error) {
        NSString *strMsg = @"등록된 이력이 없습니다.";
        [Utils basicAlertView:self withTitle:@"" withMsg:strMsg];
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    self.titleLbl.text = @"";
    self.serviceCountTitleLbl.text = @"";
}

@end
