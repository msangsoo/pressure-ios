//
//  ServiceDetailCell01.h
//  MediCare
//
//  Created by yunyungjeong on 2016. 6. 24..
//  Copyright © 2016년 SungChangmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetailCell01 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *strREQDATE;
@property (weak, nonatomic) IBOutlet UILabel *strUSEDATE;
@property (weak, nonatomic) IBOutlet UILabel *strSO_NAME;
@property (weak, nonatomic) IBOutlet UILabel *strUSE_MEMO;
@property (weak, nonatomic) IBOutlet UILabel *strSTATE_NAME;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCellTitle3;
@property (weak, nonatomic) IBOutlet UILabel *strCONFDATE;
@property (weak, nonatomic) IBOutlet UILabel *strGOODS_NAME;
@property (weak, nonatomic) IBOutlet UILabel *strENDDATE;
@property (weak, nonatomic) IBOutlet UILabel *strRTNDATE;

@end

