//
//  FreemiumHistoryDetailVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "ServiceDetailCell01.h"

@interface FreemiumHistoryDetailVC : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSString *strApiCode;
@property (nonatomic, assign) BOOL isCertiSuccess;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *serviceCountTitleLbl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
