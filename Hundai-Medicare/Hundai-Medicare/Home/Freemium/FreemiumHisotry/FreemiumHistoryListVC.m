//
//  FreemiumHistoryListVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FreemiumHistoryListVC.h"

@interface FreemiumHistoryListVC ()

@end

@implementation FreemiumHistoryListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"서비스 이력보기"];
    
    [self viewInit];
}

#pragma mark - Actions
- (IBAction)pressService:(UIButton *)sender {
    
    FreemiumHistoryDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FreemiumHistoryDetailVC"];
    
    switch (sender.tag) {
        case 10:
            vc.strApiCode = @"DZ002";
            break;
        case 11:
            vc.strApiCode = @"DZ003";
            break;
        case 12:
            vc.strApiCode = @"DZ004";
            break;
        case 13:
            vc.strApiCode = @"DZ005";
            break;
        case 14:
            vc.strApiCode = @"DZ006";
            break;
        case 15:
            vc.strApiCode = @"DZ007";
            break;
        case 16:
            vc.strApiCode = @"DZ009";
            break;
        case 17:
            vc.strApiCode = @"DZ008";
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - viewInit
- (void)viewInit {
    self.serviceVw.hidden = true;
    self.cancerVw.hidden = true;
    
    if ([self.type isEqualToString:@"service"]) {
        self.serviceVw.hidden = false;
    }else {
        self.cancerVw.hidden = false;
    }
    
    self.service1Btn.tag = 10;
    self.service2Btn.tag = 11;
    self.service3Btn.tag = 12;
    self.service4Btn.tag = 13;
    self.service5Btn.tag = 14;
    self.cancer1Btn.tag = 15;
    self.cancer2Btn.tag = 16;
    self.cancer3Btn.tag = 17;
}

@end
