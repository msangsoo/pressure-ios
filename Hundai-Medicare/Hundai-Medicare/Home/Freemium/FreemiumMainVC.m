//
//  FreemiumMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 08/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FreemiumMainVC.h"

@interface FreemiumMainVC ()

@end

@implementation FreemiumMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Premium"];
    
    [self viewInit];
    
    [self navigationSetup];
    
    coachingVC = [NUTRITIONCOACHING_STORYBOARD instantiateInitialViewController];
    [self addChildViewController:coachingVC];
    coachingVC.view.bounds = _vwMain.bounds;
    coachingVC.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:coachingVC.view];
    [coachingVC.view setHidden:false];
    
    mediaVC = [FITMEDIA_STORYBOARD instantiateInitialViewController];
    [self addChildViewController:mediaVC];
    mediaVC.view.bounds = _vwMain.bounds;
    mediaVC.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:mediaVC.view];
    [mediaVC.view setHidden:true];
    
    self.tabBarVwLeftConst.constant = 0;
    [self.coachingBtn setSelected:true];
    [self.mediaBtn setSelected:false];
    [coachingVC viewSetup];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    if (self.coachingBtn.isSelected) {
        [self.coachingBtn setSelected:true];
        [self.mediaBtn setSelected:false];
        
        [coachingVC.view setHidden:false];
        [mediaVC.view setHidden:true];
    }else {
        [self.coachingBtn setSelected:false];
        [self.mediaBtn setSelected:true];
        
        [coachingVC.view setHidden:true];
        [mediaVC.view setHidden:false];
    }
}

#pragma mark - Actions
- (IBAction)pressHistory:(id)sender {
    UIViewController *vc = [FREEMIUMHISTORY_STORYBOARD instantiateInitialViewController];
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressTab:(UIButton *)sender {
    if(sender.tag == 1 && !self.coachingBtn.isSelected) {
        [self.coachingBtn setSelected:true];
        [self.mediaBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [coachingVC.view setHidden:false];
        [mediaVC.view setHidden:true];
        
        [coachingVC viewSetup];
    }else if(sender.tag == 2 && !self.mediaBtn.isSelected) {
        [self.coachingBtn setSelected:false];
        [self.mediaBtn setSelected:true];
        
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [coachingVC.view setHidden:true];
        [mediaVC.view setHidden:false];
        
        [mediaVC viewSetup];
    }
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"서비스이력"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressHistory:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

#pragma mark - viewInit
- (void)viewInit {
    self.coachingBtn.tag = 1;
    [self.coachingBtn setTitle:@"영양 코칭" forState:UIControlStateNormal];
    [self.coachingBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.coachingBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.mediaBtn.tag = 2;
    [self.mediaBtn setTitle:@"맞춤 동영상" forState:UIControlStateNormal];
    [self.mediaBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.mediaBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
}

@end
