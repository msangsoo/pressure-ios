//
//  FitMediaMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FitMediaMainVC.h"

#define AutoSizingRatio [[UIScreen mainScreen] bounds].size.width/self.view.frame.size.width

@interface FitMediaMainVC () {
    NSDictionary *item;
}

@property (strong, nonatomic) NSDate *datePre;
@property (strong, nonatomic) NSDate *dateNext;
@property (strong, nonatomic) NSDate *dateLast;

@property (strong, nonatomic) NSString *strPreDate;
@property (strong, nonatomic) NSString *strNextDate;

@property (strong, nonatomic) NSString *strWDATE;

@property (strong, nonatomic) NSString *strTabGubun;

@property (strong, nonatomic) NSDictionary *dicCode;
@property (strong, nonatomic) NSArray *arrayCodeKey;
@property (strong, nonatomic) NSArray *arrayCodeVal;
@property (assign, nonatomic) NSInteger iSelCodeIndex;

@end

@implementation FitMediaMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    item = [[NSDictionary alloc] init];
    _dicCode = @{@"A":@"10",@"B":@"11",@"C":@"12",@"D":@"13",@"E":@"14",@"F":@"15",@"G":@"16",@"H":@"17",@"I":@"18"};
    _arrayCodeKey = @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I"];
    _arrayCodeVal = @[@"전혀 힘들지 않다.",@"극도로 가볍다.",@"매우 가볍다.",@"가볍다.",@"다소 힘들다.",@"힘들다(무겁다).",@"매우 힘들다.",@"극도로 힘들다.",@"최대이다."];
    
    [self viewInit];
    
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        _strWDATE = _strPreDate;
    }else {
        _strWDATE = _strNextDate;
    }
    
    [self viewSetup];
}

- (IBAction)pressTab:(UIButton *)sender {
    if (sender.tag == 1) {
        self.strTabGubun = @"E";
        
        self.healthBtn.selected = true;
        self.recipeBtn.selected = false;
    }else {
        self.strTabGubun = @"R";
        
        self.healthBtn.selected = false;
        self.recipeBtn.selected = true;
    }
    
    [self viewSetup];
}

- (IBAction)pressPlay:(id)sender {
    if ([self->item[@"RESULT_CODE"]isEqualToString:@"4444"] || [self->item[@"MV_URL"]isEqualToString:@""]) {
        [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"등록된 맞춤 동영상이 없습니다." left:@"확인" right:@""];
    }else {
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"Wi-fi가 아닌 이동통신망에 접속된 상태에서 동영상 재생시 별도의 데이터 통화료가 부과될 수 있습니다." left:@"취소" right:@"확인"];
    }
}

- (IBAction)pressInfo:(id)sender {
    FitMediaCautionAlertVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FitMediaCautionAlertVC"];
    vc.strTitle = self.infoLbl.text;
    [self.navigationController presentViewController:vc animated:true completion:nil];
}

- (IBAction)pressThing:(id)sender {
    FitMediaThingAlertVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FitMediaThingAlertVC"];
    vc.delegate = self;
    vc.selectedIndex = (int)self.iSelCodeIndex;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 100) {
        if (tag == 2) {
            FitMediaDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FitMediaDetailVC"];
            if ([_strTabGubun isEqualToString:@"E"]) {
                vc.strTabText = @"재활 운동";
            }else{
                vc.strTabText = @"특별 레시피";
            }
            vc.dicRow = self->item;
            [self.navigationController pushViewController:vc animated:true];
        }
    }
}

#pragma mark - FitMediaThingAlertVCDelegate
- (void)selected:(int)index {
    NSString *SEQ = @"";
    Tr_login *login = [_model getLoginData];
    SEQ = login.seq;
    
#if DEBUG
    SEQ = @"0001013000015";
#endif
    
    _iSelCodeIndex = index;
    NSDictionary *params = @{@"DOCNO": @"DY002",
                             @"SEQ": SEQ,
                             @"MOTION_CODE": _arrayCodeKey[_iSelCodeIndex],
                             @"ML_SEQ": self->item[@"ML_SEQ"]
                             };
    //    NSLog(@"params:%@",params);
    [MANetwork PostDataAsync:@"https://m.shealthcare.co.kr/HL_MED_COMMUNITY/ws.asmx/getJson" paramString:params success:^(id resultSet) {
        if ([resultSet[@"RESULT_CODE"]isEqualToString:@"0000"]) {  //성공
            [self.feelLbl setNotoText:self.arrayCodeVal[self.iSelCodeIndex]];
        }else{
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self.dateLbl setNotoText:[NSString stringWithFormat:@"%@년 %@월",[_strWDATE  substringToIndex:4], [_strWDATE  substringFromIndex:4]]];
    
    if ([self.strTabGubun isEqualToString:@"E"]) {
         [self.infoDescLbl setNotoText:@"신체를 균형 상태로 만들고, 피로에 대한 저항력을 키워가는데 중점을 두고 운동을 실천하세요."];
        
        [self.infoLbl setNotoText:@"운동 수행 전 주의사항"];
    }else {
        [self.infoDescLbl setNotoText:@"입맛 돋우고 암을 이기는 『맞춤식 특별 레시피』 쉐프에 도전하세요!"];
        
        [self.infoLbl setNotoText:@"암을 이기는 식생활"];
    }
    
    [self apiContent];
}

#pragma mark - api
- (void)apiContent {
    NSString *SEQ = @"";
    Tr_login *login = [_model getLoginData];
    SEQ = login.seq;
    
#if DEBUG
    SEQ = @"0001013000015";
#endif
    
    NSDictionary *params = @{@"DOCNO": @"DY001",
                             @"SEQ": SEQ,
                             @"MMONTH": _strWDATE,
                             @"MV_GUBUN": _strTabGubun
                             };
    
    [MANetwork PostDataAsync:@"https://m.shealthcare.co.kr/HL_MED_COMMUNITY/ws.asmx/getJson" paramString:params success:^(id resultSet) {
        
        self.feelLbl.text = @"";
        
        if ([resultSet[@"RESULT_CODE"]isEqualToString:@"0000"]) {  //성공
            
            self->item = resultSet;
            
            [self.thumnailImgVw sd_setImageWithURL:[NSURL URLWithString:self->item[@"MV_THUMBNAIL"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                self.thumnailImgVw.image = image;
            }];
            [self.mediaVw bringSubviewToFront:self.playBtn];
            
            self.iSelCodeIndex = [self.dicCode[self->item[@"MOTION_CODE"]] intValue] - 10;
            
            [self.feelLbl setNotoText:([self->item[@"MOTION_TITLE"]isEqualToString:@""] ? @"기록없음" : self->item[@"MOTION_TITLE"])];
            
            [self.infoMediaLbl setNotoText:self->item[@"MV_TITLE"]];
            
            
            //좌우 화살 표 처리
            self.strPreDate = self->item[@"LMONTH"];
            self.strNextDate = self->item[@"NMONTH"];
            
            if ([self.strNextDate isEqualToString:@""]) {
                self.rightBtn.hidden = true;
            }else {
                self.rightBtn.hidden = false;
            }
            
            if ([self.strPreDate isEqualToString:@""]) {
                self.leftBtn.hidden = true;
            }else {
                self.leftBtn.hidden = false;
            }
            
            //탭 구분에 따른 설명, 운동자 각도
            if ([self.strTabGubun isEqualToString:@"E"]) {          //재활 운동
                
                self.feelVw.hidden = false;
                self.accountVw.hidden = true;
                
                self.contentHeightConst.constant = self.feelVw.frame.origin.y + self.feelVw.frame.size.height + 20.f;
            }else if ([self.strTabGubun isEqualToString:@"R"]) {        //특별 레시피
                
                [self.accountLbl setNotoText:self->item[@"MV_DESCRIPTION"]];
                self.accountLbl = [self getResizeLabel:self.accountLbl];
                [self.accountLbl layoutIfNeeded];
                
                self.accountVwHeightConst.constant = self.accountLbl.frame.size.height + 40;
                [self.accountVw layoutIfNeeded];
                
                self.contentHeightConst.constant = self.accountVw.frame.origin.y + self.accountVw.frame.size.height + 20.f;
                
                self.feelVw.hidden = true;
                self.accountVw.hidden = false;
            }
            
            if ([self->item[@"MV_URL"] isEqualToString:@""]) {
                self.playBtn.userInteractionEnabled = false;
            }else {
                self.playBtn.userInteractionEnabled = true;
            }
        }else if ([resultSet[@"RESULT_CODE"]isEqualToString:@"4444"]) {
            
            [Utils basicAlertView:self withTitle:@"" withMsg:@"등록된 맞춤 동영상이 없습니다."];
            
            self->item = resultSet;
            
            if ([self->item[@"MV_URL"] isEqualToString:@""]) {
                self.playBtn.userInteractionEnabled = false;
            }else{
                self.playBtn.userInteractionEnabled = true;
            }
            
            self.feelLbl.text = @"";
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    self.healthBtn.tag = 1;
    [self.healthBtn setTitle:@"재활 운동" forState:UIControlStateNormal];
    [self.healthBtn mediHealthSecondTabButton];
    self.healthBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    
    self.recipeBtn.tag = 2;
    [self.recipeBtn setTitle:@"특별 레시피" forState:UIControlStateNormal];
    [self.recipeBtn mediHealthSecondTabButton];
    self.recipeBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    
    self.infoDescLbl.font = FONT_NOTO_REGULAR(16);
    self.infoDescLbl.textColor = COLOR_GRAY_SUIT;
    self.infoDescLbl.text = @"";
    
    self.infoLbl.font = FONT_NOTO_REGULAR(14);
    self.infoLbl.text = @"";
    
    self.infoMediaLbl.font = FONT_NOTO_MEDIUM(18);
    self.infoMediaLbl.textColor = [UIColor whiteColor];
    self.infoMediaLbl.text = @"";
    
    self.feelVw.layer.borderWidth = 1;
    self.feelVw.layer.borderColor = RGB(218, 218, 218).CGColor;
    
    self.feelLbl.font = FONT_NOTO_MEDIUM(18);
    self.feelLbl.textColor = COLOR_GRAY_SUIT;
    [self.feelLbl setNotoText:@"기록 없음"];
    
    self.accountTitleLbl.font = FONT_NOTO_REGULAR(14);
    self.accountTitleLbl.textColor = [UIColor whiteColor];
    [self.accountTitleLbl setNotoText:@"설명"];
    
    self.accountLbl.font = FONT_NOTO_REGULAR(14);
    self.accountLbl.textColor = COLOR_GRAY_SUIT;
    self.accountLbl.text = @"";
    
    self.strTabGubun = @"E";
    self.healthBtn.selected = true;
    
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMM"];
    _strWDATE =[formatter stringFromDate:[NSDate date]];
    
    self.leftBtn.hidden = true;
    self.rightBtn.hidden = true;
    
    self.feelVw.hidden = false;
    self.accountVw.hidden = true;
}

#pragma mark - Custom Method
- (UILabel *)getResizeLabel:(UILabel *)label {
    CGFloat height = 0.f;
    CGSize newSize = [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width * AutoSizingRatio, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin
                                           attributes: @{ NSFontAttributeName: label.font } context: nil].size;
    
    height = ceilf(newSize.height);
    NSLog(@"height:%f",height);
    
    CGRect frame= label.frame;
    frame.size.height = height;
    label.frame = frame;
    
    return label;
}

@end
