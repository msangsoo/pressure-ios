//
//  FitMediaDetailVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FitMediaDetailVC.h"

#define AutoSizingRatio [[UIScreen mainScreen] bounds].size.width/self.view.frame.size.width

@interface FitMediaDetailVC ()

@property (nonatomic,strong) AVPlayer *avPlayer;
@property (nonatomic,strong) AVPlayerItem *avPlayerItem;
@property (nonatomic,strong) AVPlayerViewController *mController;
@property (nonatomic,strong) AVPlayerLayer *avPlayerLayer;

@end

@implementation FitMediaDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%@(%@)", _strTabText, self.dicRow[@"MV_TITLE"]]];
    
    [self viewInit];
    
    NSURL *urlMovie = [NSURL URLWithString:self.dicRow[@"MV_URL"]];
    _mController = [[AVPlayerViewController alloc] init];
    _avPlayer = [AVPlayer playerWithURL:urlMovie];
    _mController.player = _avPlayer;
    _mController.videoGravity = AVLayerVideoGravityResizeAspect;
    _mController.showsPlaybackControls = YES;
    [_mController shouldAutorotate];
    [self addChildViewController:_mController];
    [self.mediaVw addSubview:_mController.view];
    
   _mController.view.hidden = true;
}

- (void)viewDidAppear:(BOOL)animated {
    self.contentHeightConst.constant = self.infoDescLbl.frame.origin.y + self.infoDescLbl.frame.size.height + 10.f;
    
    _mController.view.frame = CGRectMake(0, 0, self.mediaVw.frame.size.width, self.mediaVw.frame.size.height);
    _mController.view.hidden = false;
    
    [_avPlayer play];
    [_mController.contentOverlayView addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
    
    _avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.avPlayerItem];
}

#pragma mark - NSNotificationCenter
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
}

#pragma mark - viewInit
- (void)viewInit {
    self.infoTitleLbl.font = FONT_NOTO_MEDIUM(18);
    self.infoTitleLbl.textColor = [UIColor whiteColor];
    self.infoTitleLbl.text = @"";
    [self.infoTitleLbl setNotoText: self.dicRow[@"MV_TITLE"]];
    
    self.infoDescLbl.font = FONT_NOTO_REGULAR(18);
    self.infoDescLbl.textColor = COLOR_GRAY_SUIT;
    self.infoDescLbl.text = @"";
    [self.infoDescLbl setNotoText: self.dicRow[@"MV_CONTENT"]];
    [self.infoDescLbl layoutIfNeeded];
}

#pragma mark - Custom Method
- (UILabel *)getResizeLabel:(UILabel *)label {
    CGFloat height = 0.f;
    CGSize newSize = [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width * AutoSizingRatio, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin
                                           attributes: @{ NSFontAttributeName: label.font } context: nil].size;
    
    height = ceilf(newSize.height);
    NSLog(@"height:%f",height);
    
    CGRect frame= label.frame;
    frame.size.height = height;
    label.frame = frame;
    
    return label;
}

@end
