//
//  FitMediaCautionAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FitMediaCautionAlertVC.h"

@interface FitMediaCautionAlertVC ()

@end

@implementation FitMediaCautionAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *closeImg = [[UIImage imageNamed:@"btn_white_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.rightBtn setImage:closeImg forState:UIControlStateNormal];
    [self.rightBtn setTintColor:COLOR_NATIONS_BLUE];
    
    [self viewInit];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - viewInit
- (void)viewInit {
    self.titleLbl.text = [_strTitle substringToIndex:_strTitle.length - 1];
    
    if ([self.titleLbl.text isEqualToString:@"운동 수행 전 주의사항"]) {
        _sv1.hidden = false;
        _sv2.hidden = true;
        
    }else if ([self.titleLbl.text isEqualToString:@"암을 이기는 식생활"]) {
        _sv1.hidden = true;
        _sv2.hidden = false;
    }
}

@end
