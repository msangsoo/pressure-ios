//
//  FitMediaCautionAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FitMediaCautionAlertVC : UIViewController

@property(nonatomic, strong) NSString *strTitle;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *sv1;
@property (weak, nonatomic) IBOutlet UIScrollView *sv2;

@end
