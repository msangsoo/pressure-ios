//
//  FitMediaThingAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FitMediaThingAlertVC.h"

@interface FitMediaThingAlertVC () {
    NSArray *items;
}

@end

@implementation FitMediaThingAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    items = @[@"전혀 힘들지 않다.",@"극도로 가볍다.",@"매우 가볍다.",@"가볍다.",@"다소 힘들다.",@"힘들다(무겁다).",@"매우 힘들다.",@"극도로 힘들다.",@"최대이다."];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismiss:nil];
}

- (IBAction)pressSeleted:(UIButton *)sender {
    self.select1Btn.selected = false;
    self.select2Btn.selected = false;
    self.select3Btn.selected = false;
    self.select4Btn.selected = false;
    self.select5Btn.selected = false;
    self.select6Btn.selected = false;
    self.select7Btn.selected = false;
    self.select8Btn.selected = false;
    self.select9Btn.selected = false;
    
    if (sender.tag == 0) {
        self.select1Btn.selected = true;
        self.selectedIndex = 0;
    }else if (sender.tag == 1) {
        self.select2Btn.selected = true;
        self.selectedIndex = 1;
    }else if (sender.tag == 2) {
        self.select3Btn.selected = true;
        self.selectedIndex = 2;
    }else if (sender.tag == 3) {
        self.select4Btn.selected = true;
        self.selectedIndex = 3;
    }else if (sender.tag == 4) {
        self.select5Btn.selected = true;
        self.selectedIndex = 4;
    }else if (sender.tag == 5) {
        self.select6Btn.selected = true;
        self.selectedIndex = 5;
    }else if (sender.tag == 6) {
        self.select7Btn.selected = true;
        self.selectedIndex = 6;
    }else if (sender.tag == 7) {
        self.select8Btn.selected = true;
        self.selectedIndex = 7;
    }else if (sender.tag == 8) {
        self.select9Btn.selected = true;
        self.selectedIndex = 8;
    }
}

- (IBAction)pressNext:(id)sender {
    if (self.selectedIndex >= 0) {
        [self dismiss:^{
            if (self.delegate) {
                [self.delegate selected:self.selectedIndex];
            }
        }];
    }else {
        [self dismiss:^{
            if (self.delegate) {
                [self.delegate selected:-1];
            }
        }];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    
    int index = 0;
    NSArray *selectBtns = @[_select1Btn, _select2Btn, _select3Btn, _select4Btn, _select5Btn, _select6Btn, _select7Btn, _select8Btn, _select9Btn];
    for (NSString *lblString in items) {
        UIButton *btn = selectBtns[index];
        
        btn.tag = index;
        [btn setTitle:lblString forState:UIControlStateNormal];
        [btn foodKindSelectedBtn];
        [btn addTarget:self action:@selector(pressSeleted:) forControlEvents:UIControlEventTouchUpInside];
        
        btn.selected = false;
        if (index == self.selectedIndex) {
            btn.selected = true;
        }
        
        index ++;
    }
    
    self.cancelBtn.tag = 1;
    [self.cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [self.cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    self.confirmBtn.tag = 2;
    [self.confirmBtn setTitle:@"다음" forState:UIControlStateNormal];
    [self.confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

@end
