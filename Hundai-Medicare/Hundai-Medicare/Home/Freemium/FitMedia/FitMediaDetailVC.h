//
//  FitMediaDetailVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface FitMediaDetailVC : UIViewController<AVAudioPlayerDelegate>

@property(nonatomic, strong) NSDictionary *dicRow;
@property (strong, nonatomic) NSString *strTabText;

@property (weak, nonatomic) IBOutlet UIView *mediaVw;
@property (weak, nonatomic) IBOutlet UILabel *infoTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoDescLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeightConst;

@end
