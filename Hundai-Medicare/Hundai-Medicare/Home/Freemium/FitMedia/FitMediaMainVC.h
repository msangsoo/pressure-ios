//
//  FitMediaMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "FitMediaDetailVC.h"
#import "FitMediaCautionAlertVC.h"
#import "FitMediaThingAlertVC.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface FitMediaMainVC : BaseViewController<FitMediaThingAlertVCDelegate, BasicAlertVCDelegate> {
    
}

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Tab */
@property (weak, nonatomic) IBOutlet UIButton *healthBtn;
@property (weak, nonatomic) IBOutlet UIButton *recipeBtn;

/* Content */
@property (weak, nonatomic) IBOutlet UILabel *infoDescLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoMediaLbl;
@property (weak, nonatomic) IBOutlet UIView *mediaVw;
@property (weak, nonatomic) IBOutlet UIImageView *thumnailImgVw;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;

/* Bottom */
@property (weak, nonatomic) IBOutlet UIView *feelVw;
@property (weak, nonatomic) IBOutlet UILabel *feelLbl;

@property (weak, nonatomic) IBOutlet UIView *accountVw;
@property (weak, nonatomic) IBOutlet UILabel *accountTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accountVwHeightConst;

/* Content */
- (void)viewSetup;

@end
