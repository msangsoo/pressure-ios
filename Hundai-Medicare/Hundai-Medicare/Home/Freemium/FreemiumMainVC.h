//
//  FreemiumMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 08/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "NutritionCoachingMainVC.h"
#import "FitMediaMainVC.h"

@interface FreemiumMainVC : BaseViewController {
    NutritionCoachingMainVC *coachingVC;
    FitMediaMainVC *mediaVC;
}

@property (weak, nonatomic) IBOutlet UIButton *coachingBtn;
@property (weak, nonatomic) IBOutlet UIButton *mediaBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

@end
