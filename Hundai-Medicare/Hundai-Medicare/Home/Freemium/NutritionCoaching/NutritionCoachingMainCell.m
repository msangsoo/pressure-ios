//
//  NutritionCoachingMainCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NutritionCoachingMainCell.h"

@implementation NutritionCoachingMainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.scrollView.delegate = self;
    
    [self viewInit];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int index = scrollView.contentOffset.x / scrollView.frame.size.width;
    self.countLbl.text = [NSString stringWithFormat:@"%d", index + 1];
}

#pragma mark - viewInit
- (void)viewInit {
    self.titleLbl.font = FONT_NOTO_REGULAR(18);
    self.titleLbl.textColor = COLOR_MAIN_DARK;
    self.titleLbl.text = @"";
    
    self.msgLbl.font = FONT_NOTO_REGULAR(16);
    self.msgLbl.textColor = COLOR_GRAY_SUIT;
    self.msgLbl.text = @"";
}

@end
