//
//  NutritionCoachingMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "NutritionCoachingMainCell.h"

#import "TimeUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AXPhotoViewer-Swift.h"

@interface NutritionCoachingMainVC : BaseViewController<UITableViewDataSource, UITableViewDelegate> {
}

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Content */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)viewSetup;

@end
