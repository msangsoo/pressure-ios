//
//  NutritionCoachingMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NutritionCoachingMainVC.h"

@interface NutritionCoachingMainVC () {
    NSMutableArray *items;
}

@property (strong, nonatomic) NSDate *datePre;
@property (strong, nonatomic) NSDate *dateNext;
@property (strong, nonatomic) NSDate *dateLast;

@property (strong, nonatomic) NSString *strSDATE;
@property (strong, nonatomic) NSString *strEDATE;

@end

@implementation NutritionCoachingMainVC

- (void)viewDidLoad {
    [super viewDidLoad];

    items = [[NSMutableArray alloc] init];
    
    [self viewInit];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [self updateMondaySaturday:_datePre];
    }else {
        [self updateMondaySaturday:_dateNext];
    }
    
    [self viewSetup];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *item = items[indexPath.row];
    
    UILabel *frameLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, DEVICE_WIDTH - 30, 0)];
    frameLbl.font = FONT_NOTO_REGULAR(16);
    frameLbl.numberOfLines = 0;
    frameLbl.contentMode = NSLineBreakByWordWrapping;
    [frameLbl setNotoText:item[@"HABITS_MEMO"]];
    [frameLbl layoutIfNeeded];
    [frameLbl sizeToFit];
    
    NSArray *arrayImageName = @[@"HB_IMG1",@"HB_IMG2",@"HB_IMG3",@"HB_IMG4",@"HB_IMG5"];
    int idx_image = 0;
    for (NSString *imageName in arrayImageName) {
        NSString *strImageUrl = item[imageName];
        if(![strImageUrl isEqualToString:@""] && [strImageUrl length] > 5) {
            idx_image++;
        }
    }
    
    if (idx_image == 0) {
        return 50 + 20 + frameLbl.frame.size.height;
    }else {
        return 50 + 20 + frameLbl.frame.size.height + 200;
    }
    
    return 300.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NutritionCoachingMainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NutritionCoachingMainCell" forIndexPath:indexPath];
    
    NSDictionary *item = items[indexPath.row];
    
    NSString *HABITS_GUBUN = item[@"HABITS_GUBUN"];
    if ([HABITS_GUBUN isEqualToString:@"R"]) {
        [cell.titleLbl setNotoText:@"고쳐야 할 식습관"];
    }else {
        [cell.titleLbl setNotoText:@"유지해야 할 식습관"];
    }
    
    [cell.msgLbl setNotoText:item[@"HABITS_MEMO"]];
    
    cell.bottomLineVw.hidden = true;
    if ((items.count - 1) == indexPath.row) {
        cell.bottomLineVw.hidden = false;
    }
    
    NSArray *arrayImageName = @[@"HB_IMG1",@"HB_IMG2",@"HB_IMG3",@"HB_IMG4",@"HB_IMG5"];
    int idx_image = 0;
    
    for (UIView *view in cell.scrollView.subviews) {
        [view removeFromSuperview];
    }
    
    for (NSString *imageName in arrayImageName) {
        NSString *strImageUrl = item[imageName];
        if(![strImageUrl isEqualToString:@""] && [strImageUrl length] > 5) {
            UIImageView *ivPhotoScroll = [[UIImageView alloc] initWithFrame:CGRectMake(DEVICE_WIDTH * idx_image, 0, DEVICE_WIDTH, 200)];
            idx_image++;
            
            ivPhotoScroll.contentMode = UIViewContentModeScaleAspectFill;
            ivPhotoScroll.clipsToBounds = YES;
            ivPhotoScroll.tag = 100;
            [cell.scrollView addSubview:ivPhotoScroll];
            
            [ivPhotoScroll sd_setImageWithURL:[NSURL URLWithString:strImageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                ivPhotoScroll.image = image;
            }];
        }
    }
    
    if (idx_image == 0) {
        cell.pageVw.hidden = true;
    }else {
        cell.pageVw.hidden = false;
        cell.totLbl.text = [NSString stringWithFormat:@"/%d", idx_image];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImageDetail:)];
    [cell.scrollView addGestureRecognizer:tapGesture];
    
    cell.scrollView.contentSize = CGSizeMake(DEVICE_WIDTH * idx_image, 200);
    cell.scrollView.scrollsToTop = NO;
    cell.scrollView.directionalLockEnabled = YES;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - api
- (void)apiList {
    NSString *SEQ = @"";
    Tr_login *login = [_model getLoginData];
    SEQ = login.seq;
    
#if DEBUG
    SEQ = @"0001013000015";
#endif

    NSDictionary *params = @{@"DOCNO": @"DX005",
                             @"SEQ": SEQ,
                             @"SDATE": _strSDATE,
                             @"EDATE": _strEDATE
                             };
    
    [MANetwork PostDataAsync:@"https://m.shealthcare.co.kr/HL_MED_COMMUNITY/ws.asmx/getJson" paramString:params success:^(id resultSet) {
         NSLog(@"%@", resultSet);
        
         self->items = [[NSMutableArray alloc] init];
         NSDictionary *dicRow = [resultSet[@"DATA"] lastObject];
         
         [self updateValidUI:dicRow];
         
         [self->items addObjectsFromArray:resultSet[@"DATA"]];
         
         if(self->items.count == 1) {
             NSDictionary *dicRow = resultSet[@"DATA"][0];
             if ([dicRow[@"HABITS_GUBUN"] isEqualToString:@"K"]) {
                 NSDictionary *params = @{@"HABITS_GUBUN":@"R"
                                          ,@"HABITS_MEMO":@"내용이 없습니다."
                                          ,@"HB_IMG1":@""
                                          ,@"HB_IMG2":@""
                                          ,@"HB_IMG3":@""
                                          ,@"LDATE":@""
                                          ,@"NDATE":@""
                                          };
                 [self->items addObject:params];
                 
             }else if ([dicRow[@"HABITS_GUBUN"] isEqualToString:@"R"]) {
                 NSDictionary *params = @{@"HABITS_GUBUN":@"K"
                                          ,@"HABITS_MEMO":@"내용이 없습니다."
                                          ,@"HB_IMG1":@""
                                          ,@"HB_IMG2":@""
                                          ,@"HB_IMG3":@""
                                          ,@"LDATE":@""
                                          ,@"NDATE":@""
                                          };
                 [self->items insertObject:params atIndex:0];
             }else {
                 NSDictionary *params = @{@"HABITS_GUBUN":@"K"
                                          ,@"HABITS_MEMO":@"내용이 없습니다."
                                          ,@"HB_IMG1":@""
                                          ,@"HB_IMG2":@""
                                          ,@"HB_IMG3":@""
                                          ,@"LDATE":@""
                                          ,@"NDATE":@""
                                          };
                 NSDictionary *params2 = @{@"HABITS_GUBUN":@"R"
                                          ,@"HABITS_MEMO":@"내용이 없습니다."
                                          ,@"HB_IMG1":@""
                                          ,@"HB_IMG2":@""
                                          ,@"HB_IMG3":@""
                                          ,@"LDATE":@""
                                          ,@"NDATE":@""
                                          };
                 

                 self->items = [[NSMutableArray alloc] init];
                 [self->items addObject:params];
                 [self->items addObject:params2];
             }
         }
        
         [self.tableView reloadData];
         
     } failure:^(NSError *error) {
         self->items = [[NSMutableArray alloc]init];
         if ([self->items count] == 0) {
             
         }else {
             
         }
         [self.tableView reloadData];
     }];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self apiList];
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    [self updateMondaySaturday:[NSDate date]];
}

#pragma mark - Custom Method
- (void)showImageDetail:(UITapGestureRecognizer *)sender {
    
    UIScrollView *svSender = (UIScrollView *)sender.view;
    NSMutableArray *arrayMImages = [[NSMutableArray alloc]init];
    
    for (id object in svSender.subviews) {
        if ([object isKindOfClass:[UIImageView class]]) {
            UIImageView *ivImage = (UIImageView *)object;
            if (ivImage.tag == 100) {
                [arrayMImages addObject:[[AXPhoto alloc] initWithAttributedTitle:nil attributedDescription:nil attributedCredit:nil imageData:nil image:ivImage.image url:nil]];
            }
        }
    }
    
    AXPhotosDataSource *dataSource = [[AXPhotosDataSource alloc]initWithPhotos: [arrayMImages copy]];
    AXPhotosViewController *vc = [[AXPhotosViewController alloc] initWithDataSource: dataSource];
    [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
}

- (void)updateMondaySaturday:(NSDate *)toDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSCalendarUnitWeekday;
    NSDateComponents *com  = [calendar components:unitFlags fromDate:toDate];
    NSInteger iWeek = [com weekday];
    
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate * monday = [toDate dateByAddingTimeInterval:-secondsPerDay*(iWeek-1)];
    NSDate * satday = [toDate dateByAddingTimeInterval:secondsPerDay*(7-iWeek)];
    
    [formatter setDateFormat:@"yyyyMMdd"];
    _strSDATE = [formatter stringFromDate:monday];
    _strEDATE = [formatter stringFromDate:satday];
    
    [formatter setDateFormat:@"yyyy년 MM월 dd일"];
    NSString  *strStartDay = [formatter stringFromDate:monday];
    NSString  *strEndDay = [formatter stringFromDate:satday];
    
    [self.dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@",strStartDay,strEndDay]];
}

- (void)updateValidUI:(NSDictionary *)dicRow {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    
    _dateLast = [formatter dateFromString:dicRow[@"WDATE"]];
    
    if ([dicRow[@"NDATE"] isEqualToString:@""]) {
        self.rightBtn.hidden = true;
    }else{
        _dateNext = [formatter dateFromString:dicRow[@"NDATE"]];
        self.rightBtn.hidden = false;
    }
    
    if ([dicRow[@"LDATE"] isEqualToString:@""]) {
        self.leftBtn.hidden = true;
    }else{
        _datePre =     [formatter dateFromString:dicRow[@"LDATE"]];
        self.leftBtn.hidden = false;
    }
    
//    if ([dicRow[@"EVAL_YN"] isEqualToString:@"Y"]) {
//        _isEvalYN = YES;
//    }else{
//        _isEvalYN = NO;
//
//    }
    
}

@end
