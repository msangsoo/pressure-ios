//
//  MainCallAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MainCallAlertVC.h"

@interface MainCallAlertVC ()

@end

@implementation MainCallAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}
    
- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressCall:(UIButton *)sender {
    NSString *callMsg = @"";
    if (sender.tag == 1) {
        callMsg = @"";
    }else if (sender.tag == 2) {
        callMsg = @"";
    }else {
        callMsg = @"";
    }
    
    [self dismiss:^{
        if (self.delegate) {
            [self.delegate selectedCall:(int)sender.tag];
        }
    }];
}

- (IBAction)pressCancel:(UIButton *)sender {
    [self dismiss:nil];
}

#pragma mark - viewInit
- (void)viewInit {
    _wordLbl.font = FONT_NOTO_MEDIUM(18);
    _wordLbl.textColor = COLOR_MAIN_DARK;
    _wordLbl.numberOfLines = 2;
    [_wordLbl setNotoText:@"어디로 전화연결\n하시겠습니까?"];
    
    _healthBtn.tag = 0;
    _healthBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_healthBtn setTitle:@"     헬스콜센터         " forState:UIControlStateNormal];
    [_healthBtn makeToRadius:true];
    
    _plannerBtn.tag = 1;
    _plannerBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_plannerBtn setTitle:@"     담당 하이플래너" forState:UIControlStateNormal];
    [_plannerBtn makeToRadius:true];
    
    _nurseBtn.tag = 2;
    _nurseBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_nurseBtn setTitle:@"     전담간호사           " forState:UIControlStateNormal];
    [_nurseBtn makeToRadius:true];
    
    [_cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
}

@end
