//
//  DustAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DustAlertVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end
