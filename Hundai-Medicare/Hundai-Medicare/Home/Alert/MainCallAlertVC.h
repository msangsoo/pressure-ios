//
//  MainCallAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utils.h"

@interface MainCallAlertVC : UIViewController

@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *wordLbl;

@property (weak, nonatomic) IBOutlet UIButton *healthBtn;
@property (weak, nonatomic) IBOutlet UIButton *plannerBtn;
@property (weak, nonatomic) IBOutlet UIButton *nurseBtn;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end

@protocol MainCallAlertVCDelegate
- (void)selectedCall:(int)index;
@end
