//
//  DustAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "DustAlertVC.h"

@interface DustAlertVC ()

@end

@implementation DustAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    [self dismiss:nil];
}

#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_MEDIUM(18);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    _word1Lbl.numberOfLines = 1;
    [_word1Lbl setNotoText:@"미세먼지 안내"];
    
    _word2Lbl.font = FONT_NOTO_MEDIUM(16);
    _word2Lbl.textColor = COLOR_GRAY_SUIT;
    _word2Lbl.numberOfLines = 0;
    [_word2Lbl setNotoText:@"앱의 미세먼지 데이터는\n실시간 관측된 자료이며\n측정소 현지 사정이나\n데이터의 수신상태에 따라\n표시되지 않을 수 있습니다.\n\n미세먼지 데이터 출처:\n환경부/환국환경공단'또는\n'Airkorea'"];
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn borderRadiusButton:COLOR_NATIONS_BLUE];
}

@end
