//
//  HomeMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "QuizMainVC.h"
#import "MissionMainVC.h"
#import "MainCallAlertVC.h"

#import "TimeUtils.h"
#import "BannerView.h"
#import "MarqueeLabel.h"
#import "DustUtils.h"

#import "CommunityNetwork.h"
#import "CommunityAlarmViewController.h"
#import "HealthInfoDisInfoDetailViewController.h"

@interface HomeMainVC : BaseViewController<ServerCommunicationDelegate, EmojiDefaultVCDelegate, MainCallAlertVCDelegate> {
    TimeUtils *timeUtils;
    DustUtils *DustUtil;
    BannerView *bannerView;
}

@property (weak, nonatomic) IBOutlet UIImageView *notiImgVw;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImgVw;
@property (weak, nonatomic) IBOutlet UIButton *premiumBtn;

/* Dust */
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;
@property (weak, nonatomic) IBOutlet UILabel *feelLbl;
@property (weak, nonatomic) IBOutlet UILabel *dustLbl;
@property (weak, nonatomic) IBOutlet UILabel *dustStateLbl;

/* Banner */
@property (weak, nonatomic) IBOutlet UIView *bannerVw;

/* Marquee */
@property (strong, nonatomic) IBOutlet UILabel *marqueeLbl;

/* Coin */
@property (weak, nonatomic) IBOutlet UILabel *missionLbl;

@property (weak, nonatomic) IBOutlet UIImageView *coinOneImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinOneLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinTwoImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinTwoLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinThreeImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinThreeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinFourImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinFourLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinFiveImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinFiveLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinSixImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinSixLbl;
@property (weak, nonatomic) IBOutlet UIImageView *coinSevenImgVw;
@property (weak, nonatomic) IBOutlet UILabel *coinSevenLbl;

@property (weak, nonatomic) IBOutlet UIButton *coinOneBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinTwoBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinThreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinFourBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinFiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinSixBtn;
@property (weak, nonatomic) IBOutlet UIButton *coinSevenBtn;

/* Middle */
@property (weak, nonatomic) IBOutlet UILabel *pointLbl;
@property (weak, nonatomic) IBOutlet UILabel *pointValLbl;

/* Bottom */
@property (weak, nonatomic) IBOutlet UILabel *keywordLbl;
@property (weak, nonatomic) IBOutlet UILabel *keywordOneLbl;
@property (weak, nonatomic) IBOutlet UILabel *keywordTwoLbl;

@property (weak, nonatomic) IBOutlet UILabel *quizLbl;
@property (weak, nonatomic) IBOutlet UILabel *quizOneLbl;
@property (weak, nonatomic) IBOutlet UILabel *quizTwoLbl;

@property (weak, nonatomic) IBOutlet UILabel *callLbl;
@property (weak, nonatomic) IBOutlet UILabel *callOneLbl;
@property (weak, nonatomic) IBOutlet UILabel *callTwoLbl;

@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoOneLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoTwoLbl;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomVwHeightConst;

@end
