//
//  MissionMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "QuizMainVC.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "WelcomAlertViewController.h"

@interface MissionMainVC : BaseViewController<UICollectionViewDataSource>

@property (nonatomic, strong) NSDictionary *quiz_day;
@property (nonatomic, strong) NSArray *mission_point_list;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *pointLbl;

@property (weak, nonatomic) IBOutlet UILabel *missionLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *pointStdLbl;

@property (weak, nonatomic) IBOutlet UIStackView *missionContentVw;

@property (weak, nonatomic) IBOutlet UILabel *loginLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthHistoryLbl;
@property (weak, nonatomic) IBOutlet UILabel *quizLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *walkLbl;
@property (weak, nonatomic) IBOutlet UILabel *writeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentLbl;

@property (weak, nonatomic) IBOutlet UILabel *loginStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthHistoryStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *quizStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthInfoStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *walkStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *writeStdLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentStdLbl;

@property (weak, nonatomic) IBOutlet UILabel *loginPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthHistoryPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *quizPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthInfoPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *walkPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *writePointLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentPointLbl;

@property (weak, nonatomic) IBOutlet UIButton *action1Btn;
@property (weak, nonatomic) IBOutlet UIButton *action2Btn;
@property (weak, nonatomic) IBOutlet UIButton *action3Btn;
@property (weak, nonatomic) IBOutlet UIButton *action4Btn;
@property (weak, nonatomic) IBOutlet UIButton *action5Btn;
@property (weak, nonatomic) IBOutlet UIButton *action6Btn;
@property (weak, nonatomic) IBOutlet UIButton *action7Btn;

@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;

@property (weak, nonatomic) IBOutlet UIButton *myPointBtn;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentVwHeightConst;

@end
