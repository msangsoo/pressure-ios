//
//  MissionMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MissionMainVC.h"

@interface MissionMainVC ()

@end

@implementation MissionMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01001 s_cod:HM01001001];
    
    [self.navigationItem setTitle:@"미션"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    
    [self missionLabelSetup];
    
    if (IS_IPHONEX) {
        self.contentVwHeightConst.constant = UIScreen.mainScreen.bounds.size.height - 88;
    }else {
        if (UIScreen.mainScreen.bounds.size.height - 64 < 630) {
            self.contentVwHeightConst.constant = 630;
        }else {
            self.contentVwHeightConst.constant = UIScreen.mainScreen.bounds.size.height - 64;
        }
    }
}

#pragma mark - Actions
- (IBAction)pressPoint:(id)sender {
    [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02007001];
    
    UIViewController *vc = [MYPOINT_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressActionBtn:(UIButton *)sender {
    NSDictionary *item = _mission_point_list[(int)sender.tag];
    NSString *point_code = [item objectForKey:@"point_code"];
    if ([point_code isEqualToString:@"1006"]) {
        //        NSLog(@"로그인");
    }else if ([point_code isEqualToString:@"1007"]) {
        //        NSLog(@"건강기록");
        [LogDB insertDb:HM02 m_cod:HM02001 s_cod:HM02001001];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1", @"index", @"1", @"tab", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
        [self.navigationController popViewControllerAnimated:true];
    }else if ([point_code isEqualToString:@"1008"]) {
        //        NSLog(@"일일퀴즈");
        [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02002001];
        
        QuizMainVC *vc = [QUIZ_STORYBOARD instantiateInitialViewController];
        vc.item = self.quiz_day;
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
        
    }else if ([point_code isEqualToString:@"1009"]) {
        //        NSLog(@"건강정보");
        [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02003001];
        
        UIViewController *vc = [HEALTHINFO_STORYBOARD instantiateInitialViewController];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
    }else if ([point_code isEqualToString:@"1010"]) {
        //        NSLog(@"10,000보");
        [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02004001];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1", @"index", @"2", @"tab", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
        [self.navigationController popViewControllerAnimated:true];
    }else if ([point_code isEqualToString:@"1011"]) {
        //        NSLog(@"커뮤니티 게시");
        [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02005001];
        
        if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
            
            NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc.touchedCancelButton = ^{
                [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            };
            vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
                NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
                NSString *seq = self->_model.getLoginData.mber_sn;
                
                [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                    if (isSuccess == TRUE) {
                        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                        
                        [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                        
                        self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"index", nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
                        [self.navigationController popViewControllerAnimated:true];
                        
                    }else {
                        
                    }
                }];
            };
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
        }else {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"index", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            [self.navigationController popViewControllerAnimated:true];
        }
        
    }else if ([point_code isEqualToString:@"1012"]) {
        //        NSLog(@"커뮤니티 댓글");
        [LogDB insertDb:HM02 m_cod:HM02002 s_cod:HM02006001];
        
        if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
            
            NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc.touchedCancelButton = ^{
                [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
            };
            vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
                NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
                NSString *seq = self->_model.getLoginData.mber_sn;
                
                [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                    if (isSuccess == TRUE) {
                        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                        
                        [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                        
                        self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"index", nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
                        [self.navigationController popViewControllerAnimated:true];
                        
                    }else {
                        
                    }
                }];
            };
            [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
        }else {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"index", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TabBarMove" object:dic];
            [self.navigationController popViewControllerAnimated:true];
        }
    }
}

#pragma mark - viewInit
- (void)missionLabelSetup {
    int dayPointGet = 0;
    int dayTotalPointGet = 0;
    
    NSArray *titleArr = @[_loginLbl, _healthHistoryLbl, _quizLbl, _healthInfoLbl, _walkLbl, _writeLbl, _commentLbl];
    NSArray *pointArr = @[_loginPointLbl, _healthHistoryPointLbl, _quizPointLbl, _healthInfoPointLbl, _walkPointLbl, _writePointLbl, _commentPointLbl];
    NSArray *actionArr = @[_action1Btn, _action2Btn, _action3Btn, _action4Btn, _action5Btn, _action6Btn, _action7Btn];
    
    int count = 0;
    for (NSDictionary *item in _mission_point_list) {
        
        int point_max_day_amt = [item[@"point_max_day_amt"] intValue];
        int accml_amt = [item[@"accml_amt"] intValue];
        
        if (accml_amt < 0) {
            accml_amt = 0;
        }
        
        if (point_max_day_amt <= accml_amt) {
            accml_amt = point_max_day_amt;
        }
        
        dayPointGet += accml_amt;
        dayTotalPointGet += point_max_day_amt;
        
        UILabel *titleArrLbl = titleArr[count];
        [titleArrLbl setNotoText:item[@"point_nm"]];
        if (accml_amt > 0) {
            titleArrLbl.textColor = COLOR_MAIN_DARK;
        }else {
            titleArrLbl.textColor = COLOR_GRAY_SUIT;
        }
        
        UILabel *pointArrLbl = pointArr[count];
        pointArrLbl.attributedText = [self setPointLabelText:accml_amt totalPoint:point_max_day_amt];
        
        UIButton *actionBtn = actionArr[count];
        actionBtn.hidden = false;
        
        count++;
    }

    [_pointLbl setNotoText:[NSString stringWithFormat:@"%dP", dayPointGet]];
    [_word1Lbl setNotoText:[NSString stringWithFormat:@"건강활동에 따라 포인트를 드립니다. (매일 최대 %dP)", dayTotalPointGet]];
}

- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(16);
    _word1Lbl.textColor = COLOR_NATIONS_BLUE;
    [_word1Lbl setNotoText:@"건강활동에 따라 포인트를 드립니다. (매일 최대 15P)"];
    
    _word2Lbl.font = FONT_NOTO_REGULAR(20);
    _word2Lbl.textColor = COLOR_MAIN_DARK;
    [_word2Lbl setNotoText:@"오늘 달성한 포인트 : "];
    
    _pointLbl.font = FONT_NOTO_REGULAR(20);
    _pointLbl.textColor = COLOR_MAIN_ORANGE;
    [_pointLbl setNotoText:@"6P"];
    
    _missionLbl.font = FONT_NOTO_MEDIUM(14);
    _missionLbl.textColor = [UIColor whiteColor];
    [_missionLbl setNotoText:@"활동"];
    
    _dayStdLbl.font = FONT_NOTO_MEDIUM(14);
    _dayStdLbl.textColor = [UIColor whiteColor];
    [_dayStdLbl setNotoText:@"일 기준"];
    
    _pointStdLbl.font = FONT_NOTO_MEDIUM(14);
    _pointStdLbl.textColor = [UIColor whiteColor];
    [_pointStdLbl setNotoText:@"포인트(획득/최대)"];
    
    _missionContentVw.layer.borderWidth = 1;
    _missionContentVw.layer.borderColor = RGB(237, 237, 237).CGColor;
    
    //////////////////////////////////////////////////////
    _loginLbl.font = FONT_NOTO_MEDIUM(16);
    _loginLbl.textColor = COLOR_GRAY_SUIT;
    [_loginLbl setNotoText:@"로그인"];
    
    _loginStdLbl.font = FONT_NOTO_MEDIUM(14);
    _loginStdLbl.textColor = COLOR_GRAY_SUIT;
    [_loginStdLbl setNotoText:@"1회"];
    
    [_loginPointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _healthHistoryLbl.font = FONT_NOTO_MEDIUM(16);
    _healthHistoryLbl.textColor = COLOR_GRAY_SUIT;
    [_healthHistoryLbl setNotoText:@"건강기록"];
    
    _healthHistoryStdLbl.font = FONT_NOTO_MEDIUM(14);
    _healthHistoryStdLbl.textColor = COLOR_GRAY_SUIT;
    [_healthHistoryStdLbl setNotoText:@"건강기록 당 4P"];
    
    [_healthHistoryPointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _quizLbl.font = FONT_NOTO_MEDIUM(16);
    _quizLbl.textColor = COLOR_GRAY_SUIT;
    [_quizLbl setNotoText:@"일일퀴즈"];
    
    _quizStdLbl.font = FONT_NOTO_MEDIUM(14);
    _quizStdLbl.textColor = COLOR_GRAY_SUIT;
    [_quizStdLbl setNotoText:@"1회"];
    
    [_quizPointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _healthInfoLbl.font = FONT_NOTO_MEDIUM(16);
    _healthInfoLbl.textColor = COLOR_GRAY_SUIT;
    [_healthInfoLbl setNotoText:@"건강정보"];
    
    _healthInfoStdLbl.font = FONT_NOTO_MEDIUM(14);
    _healthInfoStdLbl.textColor = COLOR_GRAY_SUIT;
    [_healthInfoStdLbl setNotoText:@"1회"];
    
    [_healthInfoPointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _walkLbl.font = FONT_NOTO_MEDIUM(16);
    _walkLbl.textColor = COLOR_GRAY_SUIT;
    [_walkLbl setNotoText:@"걸음 수"];
    
    _walkStdLbl.font = FONT_NOTO_MEDIUM(14);
    _walkStdLbl.textColor = COLOR_GRAY_SUIT;
    [_walkStdLbl setNotoText:@"10,000보"];
    
    [_walkPointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _writeLbl.font = FONT_NOTO_MEDIUM(16);
    _writeLbl.textColor = COLOR_GRAY_SUIT;
    [_writeLbl setNotoText:@"커뮤니티 게시"];
    
    _writeStdLbl.font = FONT_NOTO_MEDIUM(14);
    _writeStdLbl.textColor = COLOR_GRAY_SUIT;
    [_writeStdLbl setNotoText:@"1회"];
    
    [_writePointLbl setNotoText:@"0P/0P"];
    //////////////////////////////////////////////////////
    _commentLbl.font = FONT_NOTO_MEDIUM(16);
    _commentLbl.textColor = COLOR_GRAY_SUIT;
    [_commentLbl setNotoText:@"커뮤니티 댓글"];
    
    _commentStdLbl.font = FONT_NOTO_MEDIUM(14);
    _commentStdLbl.textColor = COLOR_GRAY_SUIT;
    [_commentStdLbl setNotoText:@"댓글 당 1P"];
    
    _commentPointLbl.attributedText = [self setPointLabelText:0 totalPoint:0];
    //////////////////////////////////////////////////////
    
    self.action1Btn.tag = 0;
    self.action2Btn.tag = 1;
    self.action3Btn.tag = 2;
    self.action4Btn.tag = 3;
    self.action5Btn.tag = 4;
    self.action6Btn.tag = 5;
    self.action7Btn.tag = 6;
    
    self.action1Btn.hidden = true;
    self.action2Btn.hidden = true;
    self.action3Btn.hidden = true;
    self.action4Btn.hidden = true;
    self.action5Btn.hidden = true;
    self.action6Btn.hidden = true;
    self.action7Btn.hidden = true;
    
    [self.action1Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action2Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action3Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action4Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action5Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action6Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.action7Btn addTarget:self action:@selector(pressActionBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(14);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"* 포인트는 녹십자라이프케어몰에서 사용 가능합니다.\n*걸음 수 미션 포인트는 1만보 이상 달성 후\n   앱의 홈화면 또는 건강관리 화면에 진입하셔야 지급됩니다."];
    
    [_myPointBtn borderRadiusButton:COLOR_NATIONS_BLUE];
    [_myPointBtn setTitle:@"내 포인트 보기" forState:UIControlStateNormal];
}

- (NSMutableAttributedString *)setPointLabelText:(int)point totalPoint:(int)totalPoint {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    UIColor *pointColor = COLOR_GRAY_SUIT;
    if (point > 0) {
        pointColor = COLOR_MAIN_ORANGE;
    }
    
    NSString *porintString = [NSString stringWithFormat:@"%dP", point];
    NSMutableAttributedString *pointStr = [[NSMutableAttributedString alloc] initWithString:porintString];
    [pointStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, porintString.length)];
    [pointStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(14)
                     range:NSMakeRange(0, porintString.length)];
    [pointStr addAttribute:NSForegroundColorAttributeName
                     value:pointColor
                     range:NSMakeRange(0, porintString.length)];
    [attstr appendAttributedString:pointStr];
    
    NSString *totalPointString = [NSString stringWithFormat:@"   /   %dP", totalPoint];
    NSMutableAttributedString *totalPointStr = [[NSMutableAttributedString alloc] initWithString:totalPointString];
    [totalPointStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, totalPointString.length)];
    [totalPointStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(14)
                     range:NSMakeRange(0, totalPointString.length)];
    [totalPointStr addAttribute:NSForegroundColorAttributeName
                     value:RGB(222, 222, 222)
                     range:NSMakeRange(0, totalPointString.length)];
    [attstr appendAttributedString:totalPointStr];
    
    return attstr;
}

@end
