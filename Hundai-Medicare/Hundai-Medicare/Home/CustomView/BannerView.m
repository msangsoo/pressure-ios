
//
//  BannerView.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "BannerView.h"

#define animationTimer  6.f
#define delayTimer      1.f

@implementation BannerView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        labelHeight = frame.size.height;
        labelWidth = frame.size.width;
        
        m_stringIndex = 0;
        b_animation = YES;
        m_animationCounting = 0;
        
        firstLabel = [[UILabel alloc] initWithFrame: CGRectMake(7, 0, labelWidth - 10, frame.size.height ) ];
        firstLabel.numberOfLines = 1;
        firstLabel.minimumScaleFactor = 0.9;
        firstLabel.adjustsFontSizeToFitWidth = YES;
        [firstLabel setLineBreakMode: NSLineBreakByClipping];
        [firstLabel setTextAlignment: NSTextAlignmentLeft];
        [firstLabel setFont: FONT_NOTO_REGULAR(13)];
        [firstLabel setTextColor: COLOR_MAIN_DARK];
        [self addSubview:firstLabel];
        
        secondLabel = [[UILabel alloc] initWithFrame: CGRectMake(labelWidth + 7, 0, labelWidth - 10, frame.size.height)];
        secondLabel.numberOfLines = 1;
        secondLabel.minimumScaleFactor = 0.9;
        secondLabel.adjustsFontSizeToFitWidth = YES;
        [secondLabel setLineBreakMode: NSLineBreakByClipping];
        [secondLabel setTextAlignment: NSTextAlignmentLeft];
        [secondLabel setFont: FONT_NOTO_REGULAR(13)];
        [secondLabel setTextColor: COLOR_MAIN_DARK];
        [self addSubview:secondLabel];
    }
    
    return self;
}

- (void)setData:(NSArray *)stringData {
    if (stringArr) {
        stringArr = nil;
    }
    
//    UILabel *widthLbl = [[UILabel alloc] initWithFrame:CGRectMake(labelWidth + 7, 0, labelWidth - 10, 20)];
//    widthLbl.numberOfLines = 1;
//    widthLbl.adjustsFontSizeToFitWidth = YES;
//    [widthLbl setLineBreakMode: NSLineBreakByWordWrapping];
//    [widthLbl setTextAlignment: NSTextAlignmentLeft];
//    [widthLbl setFont: FONT_NOTO_REGULAR(13)];
//    [widthLbl setText:@"수축기혈압, 확장기혈압 모두 120mmHg와 80mmHg 미만일 때를 정상혈압으로 분류해요."];
//    [widthLbl sizeToFit];
    
    UILabel *widthLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(labelWidth + 7, 0, labelWidth - 10, 20)];
    widthLbl2.numberOfLines = 1;
    widthLbl2.adjustsFontSizeToFitWidth = YES;
    [widthLbl2 setLineBreakMode: NSLineBreakByWordWrapping];
    [widthLbl2 setTextAlignment: NSTextAlignmentLeft];
    [widthLbl2 setFont: FONT_NOTO_REGULAR(13)];
    
    CGFloat lblWidth = 0.f;
    
    for (NSDictionary *item in stringData) {
        NSString *itemSteing = item[@"tip_nm"];
        [widthLbl2 setText:itemSteing];
        [widthLbl2 sizeToFit];
        
        if (lblWidth < widthLbl2.frame.size.width) {
            lblWidth = widthLbl2.frame.size.width;
        }
    }
    
    if (lblWidth < labelWidth - 10) {
        lblWidth = labelWidth - 10;
    }else if ((483 - 30) < lblWidth) {
        lblWidth = (483 - 30);
    }
    
    labelWidth = lblWidth;
    
    [firstLabel setFrame:CGRectMake(7, 0, labelWidth - 10, firstLabel.frame.size.height)];
    [secondLabel setFrame:CGRectMake(labelWidth + 7, 0, labelWidth - 10, firstLabel.frame.size.height)];
    
    stringArr = [[NSArray alloc] initWithArray: stringData];
}

- (void)setBilboard {
    if ([stringArr count] > 1) {
        [firstLabel setText:[NSString stringWithFormat:@"%@", [stringArr objectAtIndex:stringArr.count-1][@"tip_nm"] ]];
    }else if ( [stringArr count ] == 1 ) {
        [firstLabel setText:[NSString stringWithFormat:@"%@", [stringArr objectAtIndex:0][@"tip_nm"]]];
    }
}

- (void)startAnimation {
    if ([stringArr count] > 1) {
        b_animation = YES;
        m_animationCounting++;
        
        [firstLabel setText:[NSString stringWithFormat:@"%@",[stringArr objectAtIndex:0][@"tip_nm"]]];
        [secondLabel setText:[NSString stringWithFormat:@"%@",[stringArr objectAtIndex:1][@"tip_nm"]]];
        [NSTimer scheduledTimerWithTimeInterval:delayTimer target:self selector:@selector(dealyTime:) userInfo:nil repeats:NO];
    }else if ([stringArr count] == 1) {
        [firstLabel setText:[NSString stringWithFormat:@"%@",[stringArr objectAtIndex:0][@"tip_nm"]]];
    }
}

-(void)stopAnimation {
    b_animation = NO;
}

#pragma mark - selector ( timer )
- (void)dealyTime:(NSTimer *)timer {
    [timer invalidate];
    
    if  (b_animation) {
        if (m_animationCounting > 1) {
            m_animationCounting--;
        }else {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationTimer];
            [UIView setAnimationDidStopSelector:@selector(animationTime)];
            [UIView setAnimationDelegate:self ];
            [firstLabel setFrame:CGRectMake((-1 * labelWidth) - 10, 0, labelWidth - 10, self.frame.size.height)];
            [secondLabel setFrame:CGRectMake(7, 0, labelWidth - 10, self.frame.size.height)];
            [UIView commitAnimations ];
        }
    }else {
        m_animationCounting--;
    }
}

- (void)animationTime {
    m_animationCounting--;
    if ( b_animation ) {
        if ( m_animationCounting == 0 ) {
            
            [firstLabel setText:[NSString stringWithFormat:@"%@",[stringArr objectAtIndex:m_stringIndex][@"tip_nm"]]];
            [firstLabel setFrame:CGRectMake(7, 0, labelWidth - 10, self.frame.size.height)];
            m_stringIndex++;
            m_animationCounting++;
            if ( m_stringIndex + 1 > [ stringArr count ] ) {
                m_stringIndex = 0;
            }
            [secondLabel setText:[NSString stringWithFormat:@"%@",[stringArr objectAtIndex:m_stringIndex][@"tip_nm"]]];
            [secondLabel setFrame:CGRectMake( labelWidth + 7, 0, labelWidth - 10, self.frame.size.height ) ];
            [NSTimer scheduledTimerWithTimeInterval:delayTimer target:self selector:@selector(dealyTime:) userInfo:nil repeats:NO ];
        }
    }
}

@end
