//
//  BannerView.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerView : UIView {
    CGFloat labelWidth;
    CGFloat labelHeight;
    CGFloat animationTime;
    
    NSArray *stringArr;
    
    UILabel *firstLabel;
    UILabel *secondLabel;
    
    BOOL b_animation;
    
    NSString *onViewMsg;
    
    NSInteger m_animationCounting;
    NSInteger m_stringIndex;
}

- (void)setData:(NSArray *)stringData;
- (void)setBilboard;
- (void)startAnimation;
- (void)stopAnimation;

@end
