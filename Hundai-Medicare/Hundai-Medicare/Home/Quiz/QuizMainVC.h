//
//  QuizMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface QuizMainVC : BaseViewController<ServerCommunicationDelegate>

@property (nonatomic, strong) NSDictionary *item;

@property (weak, nonatomic) IBOutlet UIView *questionVw;
@property (weak, nonatomic) IBOutlet UILabel *questionTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *questionLbl;

@property (weak, nonatomic) IBOutlet UIView *anserVw;
@property (weak, nonatomic) IBOutlet UILabel *anserTitleLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *anserTitleRightLbl;
@property (weak, nonatomic) IBOutlet UIImageView *anserOXImgVw;
@property (weak, nonatomic) IBOutlet UILabel *anserResultLbl;

@end
