//
//  QuizMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "QuizMainVC.h"

@interface QuizMainVC ()

@end

@implementation QuizMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01003 s_cod:HM01003002];
    
    [self.navigationItem setTitle:@"일일퀴즈"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"quiz_point_input"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            if ([[result objectForKey:@"misson_alert_yn"] isEqualToString:@"Y"]) {
                [AlertUtils PointAlertShow:self point:[result objectForKey:@"misson_goal_point"] pointTitle:@"" pointInfo:[result objectForKey:@"misson_goal_txt"]];
            }
        }
    }
}

#pragma mark - Actions
- (IBAction)pressOX:(UIButton *)sender {
    if (sender.tag == 1) {
        
    }else {
        
    }
    
    [self apiQuiz];
    
    _anserOXImgVw.image = [UIImage imageNamed:@"btn_quiz_o"];
    if ([[_item objectForKey:@"quiz_replay"] isEqualToString:@"0"]) {
        _anserOXImgVw.image = [UIImage imageNamed:@"btn_quiz_x"];
    }

    [_anserResultLbl setNotoText:[_item objectForKey:@"quiz_explain"]];
    
    _questionVw.hidden = true;
    _anserVw.hidden = false;
}

#pragma mark - api
- (void)apiQuiz {
    Tr_login *login = [_model getLoginData];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMMdd"];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"quiz_point_input", @"api_code",
                                login.mber_sn, @"mber_sn",
                                [_item objectForKey:@"quiz_sn"], @"quiz_sn",
                                [format stringFromDate:[NSDate date]], @"quiz_input_de",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _questionTitleLbl.font = FONT_NOTO_BLACK(24);
    _questionTitleLbl.textColor = COLOR_NATIONS_BLUE;
    [_questionTitleLbl setNotoText:@"오늘의 Quiz"];
    
    _questionLbl.font = FONT_NOTO_BOLD(18);
    _questionLbl.textColor = COLOR_GRAY_SUIT;
    [_questionLbl setNotoText:[_item objectForKey:@"quiz_question"]];
    
    _anserTitleLeftLbl.font = FONT_NOTO_BLACK(24);
    _anserTitleLeftLbl.textColor = COLOR_NATIONS_BLUE;
    [_anserTitleLeftLbl setNotoText:@"정답은"];
    
    _anserTitleRightLbl.font = FONT_NOTO_BLACK(24);
    _anserTitleRightLbl.textColor = COLOR_NATIONS_BLUE;
    [_anserTitleRightLbl setNotoText:@"입니다."];
    
    _anserResultLbl.font = FONT_NOTO_BOLD(18);
    _anserResultLbl.textColor = COLOR_GRAY_SUIT;
    _anserResultLbl.text = @"";
    
    _questionVw.hidden = false;
    _anserVw.hidden = true;
}

@end
