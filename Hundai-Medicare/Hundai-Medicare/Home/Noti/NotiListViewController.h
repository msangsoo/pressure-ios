//
//  NotiListViewController.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 5..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "BaseViewController.h"
#import "NotiListBasicCell.h"
#import "NotiDetailViewController.h"

#import "FeedDetailViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"

@interface NotiListViewController : BaseViewController<ServerCommunicationDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSString *DOCNO;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTv;

@property (weak, nonatomic) IBOutlet UILabel *lblTab1Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTab2Title;

@property (weak, nonatomic) IBOutlet UIView *vwTab1;
@property (weak, nonatomic) IBOutlet UIView *vwTab2;

@end
