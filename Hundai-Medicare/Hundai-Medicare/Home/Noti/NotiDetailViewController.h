//
//  NotiDetailViewController.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 10. 14..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KakaoLink/KakaoLink.h>
#import <KakaoMessageTemplate/KakaoMessageTemplate.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "BaseViewController.h"

@interface NotiDetailViewController : BaseViewController< MFMessageComposeViewControllerDelegate, ServerCommunicationDelegate>

@property (nonatomic, strong) IBOutlet NSDictionary *items;


@property (weak, nonatomic) NSString *DOCNO;

@property (weak, nonatomic) IBOutlet UIView *vwTopArea;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *shareVw;
@property (weak, nonatomic) IBOutlet UILabel *shareLbl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextView *contentTv;
@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constDownload;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTopAreaHeight;


- (IBAction)pressPdfdownload:(id)sender;

@end

