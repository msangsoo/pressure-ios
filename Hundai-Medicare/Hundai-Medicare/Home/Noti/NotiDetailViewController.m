//
//  NotiDetailViewController.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 10. 14..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NotiDetailViewController.h"


@interface NotiDetailViewController () {
    int KBTA_IDX;
}

@end

@implementation NotiDetailViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"알리미"];
    
    if (_items.count > 0) {
        KBTA_IDX = [[_items objectForKey:@"kbta_idx"] intValue];
        [self viewInit];
    }else {
        [self apiDetail];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    if (!self.contentTv.isHidden) {
        [self.contentTv setContentOffset:CGPointZero];
    }
}

#pragma mark - server delegate
- (void)delegateResultString:(id)result CODE:(NSString*)code {
}
- (void)delegatePush:(NSInteger)status {
}
- (void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"asstb_kbtg_alimi_view"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            self.items = [[result objectForKey:@"chlmReadern"] firstObject];
            [self viewInit];
        }else {
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"데이터 통신에 실패했습니다." completion:^{
                [self.navigationController popViewControllerAnimated:true];
            }];
        }
    }
}

#pragma mark - action method
- (IBAction)pressShare:(id)sender {
    
//    [AppUtil ShareAlertOpen:self];
}

#pragma mark - share delegate
//- (void)ShareAlertView:(ShareAlertViewController *)alertView clickButtonTag:(NSInteger)tag {
//    if (tag == 0) {
//        [self kakaoShare];
//    }else {
//        int idx = [[self.items objectForKey:@"KBTA_IDX"] intValue];
//        UIImage *imageCapture = [UIImage imageNamed:@"share_noti"];
//        NSString *shareUrl = [NSString stringWithFormat:@"http://wkd.walkie.co.kr/KBT/kbon.html?pakagename=com.greencare.kyobototalcare&tabletname=com.greencare.kyobototaltablets&urlschema=kyobototalcare&service=notice&param1=%d&param2=", idx];
//        MFMessageComposeViewController* composer = [[MFMessageComposeViewController alloc] init];
//        composer.body = [NSString stringWithFormat:@"[교보생명 상품부가서비스]\n%@\n%@", self.titleLbl.text, shareUrl];
//        if (composer != nil) {
//            composer.messageComposeDelegate = self;
//            // MMS
//            if([MFMessageComposeViewController respondsToSelector:@selector(canSendAttachments)] && [MFMessageComposeViewController canSendAttachments]) {
//                NSData* attachment = UIImageJPEGRepresentation(imageCapture, 1.0);
//                NSString* uti = (NSString*)kUTTypeMessage;
//                [composer addAttachmentData:attachment typeIdentifier:uti filename:@"capture.jpg"];
//            }
//            [self presentViewController:composer animated:YES completion:nil];
//        }
//    }
//}

#pragma mark - msg delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    switch (result) {
        case MessageComposeResultCancelled:
        case MessageComposeResultFailed:
        {
            //            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - kakao
//- (void)kakaoShare {
//
//    int idx = [[self.items objectForKey:@"KBTA_IDX"] intValue];
//    NSString *templateId = KAKAO_TEMPLETKEY2;   //알리미, 서비스소개, 체험, 럭키박스 링크용 키
//    NSDictionary *templateArgs = @{@"imgUrl": @"https://wkd.walkie.co.kr/KBT/Contents/share/share_alimi.png",
//                                   @"imgWidth": @"500",
//                                   @"imgHeight": @"250",
//                                   @"title": @"[교보생명 상품부가서비스]",
//                                   @"description": self.titleLbl.text,
//                                   @"btTitle": @"자세히보기",
//                                   @"params": [NSString stringWithFormat:@"service=notice&param1=%d", idx]
//                                   };
//
//    [[KLKTalkLinkCenter sharedCenter] sendCustomWithTemplateId:templateId templateArgs:templateArgs success:^(NSDictionary<NSString *,NSString *> * _Nullable warningMsg, NSDictionary<NSString *,NSString *> * _Nullable argumentMsg) {
//        // 성공
//        NSLog(@"templateArgs:%@", templateArgs);
//    } failure:^(NSError * _Nonnull error) {
//        // 에러
//
//        NSLog(@"error:%@", error);
//    }];
//}

#pragma mark - api
- (void)apiDetail {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                  @"asstb_kbtg_alimi_view", @"api_code",
                  [NSString stringWithFormat:@"%d", KBTA_IDX], @"kbta_idx",
                  nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.titleLbl.font = FONT_NOTO_REGULAR(17);
    self.titleLbl.textColor = [UIColor blackColor];
    [self.titleLbl setNotoText:[self.items objectForKey:@"kbt"]];
    
    self.shareVw.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    self.shareVw.layer.cornerRadius = self.shareVw.frame.size.height / 2;
    self.shareVw.layer.borderWidth = 1;
    self.shareVw.layer.masksToBounds = true;
    
    self.shareLbl.font = FONT_NOTO_REGULAR(13);
    self.shareLbl.textColor = COLOR_NATIONS_BLUE;
    [self.shareLbl setNotoText:@"공유하기"];
    
    self.contentTv.font = FONT_NOTO_REGULAR(15);
    self.contentTv.textColor = [UIColor blackColor];
    self.contentTv.text = @"";
    
    
    [self.contentTv setHidden:true];
    [self.webView setHidden:true];
    
    if ([self.DOCNO isEqualToString:@"KA004"]) {
        
        [self.webView setHidden:false];
        self.webView.scalesPageToFit = YES;

        self.vwTopArea.hidden = YES;
        self.vwTopArea.frame = CGRectMake(self.vwTopArea.frame.origin.x, self.vwTopArea.frame.origin.y, self.vwTopArea.frame.size.width, 0);
        self.constTopAreaHeight.constant = 0;
        
        NSString *kaimg = [self.items objectForKey:@"kaimg"];
        
        NSString *imageHTML = [[NSString alloc] initWithFormat:@"%@%@%@", @"<!DOCTYPE html>"
                               "<html lang=\"ko\">"
                               "<head>"
                               "<meta charset=\"UTF-8\">"
                               "<style type=\"text/css\">"
                               "html{margin:0;padding:0;}"
                               "body {"
                               "margin: 0;"
                               "padding: 0;"
                               "color: #ffffff;"
                               "font-size: 90%;"
                               "line-height: 1.6;"
                               "background: #ffffff;"
                               "}"
                               "img{"
                               "top: 0;"
                               "bottom: 0;"
                               "left: 0;"
                               "right: 0;"
                               "margin: 0;"
                               "max-width: 100%;"
                               "max-height: 100%;"
                               "}"
                               "</style>"
                               "</head>"
                               "<body id=\"page\">"
                               "<img src='", kaimg ,@"' width='100%'/> </body></html>"];
        
        [self.webView loadHTMLString:imageHTML baseURL: [[NSBundle mainBundle] bundleURL]];
    }else{
        
        self.vwTopArea.hidden = NO;
        if ([[self.items objectForKey:@"HTML_YN"] isEqualToString:@"Y"]) {
            
            [self.webView.scrollView setBounces:false];
            [self.webView.scrollView setShowsVerticalScrollIndicator:false];
            [self.webView.scrollView setShowsHorizontalScrollIndicator:false];
            
            [self.webView setHidden:false];
            NSString* htmlString;
            
            htmlString = [NSString stringWithFormat:@"%@",[self.items objectForKey:@"kbc"]];
            if (htmlString) {
                htmlString = [self htmlEntityDecode:htmlString];
            }
            [self.webView loadHTMLString:[self htmlEntityDecode:htmlString] baseURL: [[NSBundle mainBundle] bundleURL]];
            
        }else {
            [self.contentTv setHidden:false];
            self.contentTv.text = [self.items objectForKey:@"kbc"];
        }
        
        
        // pdf 없을때는 가리기
        if ([[self.items objectForKey:@"kbt_pdf"] length] < 3) {
            self.btnDownload.hidden = YES;
            self.constDownload.constant = -34;
        }else{
            self.btnDownload.hidden = NO;
            self.constDownload.constant = 8;
        }
    }
    
}

#pragma mark - html string
- (NSString *)htmlEntityDecode:(NSString *)string {
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return string;
}
- (IBAction)pressPdfdownload:(id)sender {
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//
//    NSArray *arrUrls;
//    NSString *fileNm;
//    if ([[self.items objectForKey:@"KBT_PDF"] length] > 3) {
//       arrUrls = [[self.items objectForKey:@"KBT_PDF"] componentsSeparatedByString:@"/"];
//       fileNm  = [arrUrls objectAtIndex:[arrUrls count]-1];
//    }
//    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileNm];
//
//
//    if (fileNm != nil) {
//        fileNm  = [NSString stringWithFormat:@"noti_%@", fileNm];
//    }
//    NSString *filePathLocal = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileNm];
//    if ([fileManager fileExistsAtPath:filePathLocal]) {
//
//        PdfModalVC *vc = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PdfModal"];
//        vc.pdfUrl = [NSURL fileURLWithPath:filePathLocal];
//        vc.nTitle = @"다운로드";
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        [self presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
//
//    }else {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"다운받으시겠습니까?" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//
//            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//
//            NSURL *URL = [NSURL URLWithString:[self.items objectForKey:@"KBT_PDF"]];
//            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//
//            NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
//                NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//                return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
//            } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
//
//
//                NSLog(@"File downloaded to: %@", filePath);
//
//                NSError *error2 = nil;
//                [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:filePath error:&error2];
//
//
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"다운로드 되었습니다." preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    PdfModalVC *vc = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PdfModal"];
//                    vc.pdfUrl = filePath;
//                    vc.nTitle = @"다운로드";
//                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                    [self presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
//                }];
//                [alert addAction:okAction];
//                [self.navigationController presentViewController:alert animated:true completion:nil];
//
//
//            }];
//            [downloadTask resume];
//        }];
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:cancelAction];
//        [alert addAction:okAction];
//        [self.navigationController presentViewController:alert animated:true completion:nil];
//    }
}
@end
