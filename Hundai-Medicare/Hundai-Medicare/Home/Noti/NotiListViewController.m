//
//  NotiListViewController.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 5..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NotiListViewController.h"

@interface NotiListViewController () {
    int page;
    int pln;
    bool scrollbottom;
    NSMutableArray *items;
}

@end

@implementation NotiListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01008 s_cod:HM01008001];
    
    [self.navigationItem setTitle:@"알리미"];
    
    items = [[NSMutableArray alloc] init];
    
    [self dataInit];
    
    _mainTv.rowHeight = 60;
    
    _mainTv.delegate = self;
    _mainTv.dataSource = self;
    
    [self apiList];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:false];
}

-(void)dataInit {
    page = 1;
    pln = 15;
    [items removeAllObjects];
    
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString*)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary*)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"asstb_kbtg_alimi"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            NSMutableArray *DATA = [[NSMutableArray alloc] init];
            DATA = [result objectForKey:@"chlmReadern"];
            for (int i = 0 ; i < DATA.count ; i ++) {
                [items addObject:[DATA objectAtIndex:i]];
            }
            page += 1;
            [_mainTv reloadData];
            scrollbottom = true;
        }
    }
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotiListBasicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotiListBasicCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dic = items[indexPath.row];
    
    [cell.titleLbl setNotoText:[dic objectForKey:@"kbt"]];
    [cell.subTitleLbl setNotoText:[dic objectForKey:@"sub_tit"]];
    NSString *date = [Utils getTimeFormat:[dic objectForKey:@"kbvd"] beTime:@"yyyyMMdd" afTime:@"yyyy.MM.dd"];
    if (![[dic objectForKey:@"ka_timg"] isEqualToString:@""]) {
        [cell.imgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"ka_timg"]]];
    }else{
        [cell.imgVw setImage:[UIImage imageNamed:@"img_empty_ch"]];
    }
    [cell.dateLbl setText:date];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        
        NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.touchedCancelButton = ^{
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        };
        vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
            NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
            NSString *seq = self->_model.getLoginData.mber_sn;
            
            [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                if (isSuccess == TRUE) {
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                    
                    [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                    self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                    
                    NSDictionary *dic = self->items[indexPath.row];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                    FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                    feedDetailVC.feedSeq = [dic objectForKey:@"kbta_idx"];
                    feedDetailVC.seq = self->_model.getLoginData.mber_sn;
                    [self.navigationController pushViewController:feedDetailVC animated:TRUE];
                }else {
                    
                }
            }];
        };
        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
    }else {
        NSDictionary *dic = items[indexPath.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
        feedDetailVC.feedSeq = [dic objectForKey:@"kbta_idx"];
        feedDetailVC.seq = _model.getLoginData.mber_sn;
        [self.navigationController pushViewController:feedDetailVC animated:TRUE];
    }
}
    
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    if(40.f > (h - y) && scrollbottom) {
        scrollbottom = false;
        if (((page - 1) * pln) == items.count) {
            [self apiList];
        }
    }
}

#pragma mark - api method
- (void)apiList {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"asstb_kbtg_alimi", @"api_code",
                                [NSString stringWithFormat:@"%d", page], @"pageNumber",
                                [NSString stringWithFormat:@"%d", pln], @"pln",
                                @"0", @"pushk",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

@end
