//
//  NotiListBasicCell.m
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 5..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NotiListBasicCell.h"

@implementation NotiListBasicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _imgVw.layer.cornerRadius = _imgVw.frame.size.height / 2;
    _imgVw.layer.masksToBounds = true;
    
    _titleLbl.font = FONT_NOTO_MEDIUM(15);
    _titleLbl.textColor = [UIColor blackColor];
    
    _subTitleLbl.font = FONT_NOTO_REGULAR(13);
    _subTitleLbl.textColor = COLOR_GRAY_SUIT;
    
    _dateLbl.font = FONT_NOTO_REGULAR(11);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
