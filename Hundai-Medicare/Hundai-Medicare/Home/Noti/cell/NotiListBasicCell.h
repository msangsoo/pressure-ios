//
//  NotiListBasicCell.h
//  kyobototalcare
//
//  Created by INSYSTEMS CO., LTD on 2018. 9. 5..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiListBasicCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVw;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@end
