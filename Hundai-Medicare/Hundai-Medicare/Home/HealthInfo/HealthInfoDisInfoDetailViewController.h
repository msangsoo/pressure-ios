//
//  HealthInfoDisInfoDetailViewController.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HealthInfoDisInfoDetailViewController : BaseViewController

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *targetUrl;
@property (nonatomic, retain) IBOutlet UIImageView *imgVw;
@property (nonatomic, retain) IBOutlet UIImage *selectImg;
@property (nonatomic, retain) IBOutlet UIWebView *webVw;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *imageViewHeightLayout;

@end
