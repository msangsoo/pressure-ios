//
//  HealthInfoMainViewController.m
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthInfoMainViewController.h"

@interface HealthInfoMainViewController ()

@end

@implementation HealthInfoMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01003 s_cod:HM01003004];
    
    [self.navigationItem setTitle:@"건강정보"];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    vcHIC = [self.storyboard instantiateViewControllerWithIdentifier:@"HealthInfoCalrumViewController"];
    [self addChildViewController:vcHIC];
    vcHIC.view.bounds = _vwMain.bounds;
    vcHIC.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHIC.view];
    
    vcHD = [self.storyboard instantiateViewControllerWithIdentifier:@"HealthInfoDisInfoViewController"];
    [self addChildViewController:vcHD];
    vcHD.view.bounds = _vwMain.bounds;
    vcHD.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHD.view];
    
    [self viewInit];
}

#pragma mark - press action method
- (IBAction)pressTap:(id)sender {
    UIButton *btn =(UIButton *)sender;
    
    if(btn.tag == 1 && _btnTap1.isSelected == NO) {
        [LogDB insertDb:HM01 m_cod:HM01007 s_cod:HM01007001];
        
        _btnTap1.selected = YES;
        _btnTap2.selected = NO;
        _btnTap3.selected = NO;
        [vcHIC.view setHidden:NO];
        [vcHD.view setHidden:YES];
        vcHIC.searchTf.text = @"";
        [Utils setUserDefault:@"health_info_title" value:@"건강칼럼"];
        _tabBarVwXCenterConst.constant = -1 * ((DEVICE_WIDTH - 50) / 3);
        [vcHIC viewInit:1];
    }else if(btn.tag == 2 && _btnTap2.isSelected == NO) {
        [LogDB insertDb:HM01 m_cod:HM01007 s_cod:HM01007002];
        
        _btnTap1.selected = NO;
        _btnTap2.selected = YES;
        _btnTap3.selected = NO;
        [vcHIC.view setHidden:NO];
        [vcHD.view setHidden:YES];
        vcHIC.searchTf.text = @"";
        [Utils setUserDefault:@"health_info_title" value:@"건강식단"];
        _tabBarVwXCenterConst.constant = 0;
        [vcHIC viewInit:2];
    }else if(btn.tag == 3 && _btnTap3.isSelected == NO) {
        [LogDB insertDb:HM01 m_cod:HM01007 s_cod:HM01007003];
        
        _btnTap1.selected = NO;
        _btnTap2.selected = NO;
        _btnTap3.selected = YES;
        [vcHIC.view setHidden:YES];
        [vcHD.view setHidden:NO];
        [Utils setUserDefault:@"health_info_title" value:@"11대 질병정보"];
        _tabBarVwXCenterConst.constant = ((DEVICE_WIDTH - 50) / 3);
    }
    
}

#pragma mark - viewInit
- (void)viewInit {
    self.btnTap1.tag = 1;
    [self.btnTap1 setTitle:@"건강칼럼" forState:UIControlStateNormal];
    [self.btnTap1 setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.btnTap1 setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.btnTap2.tag = 2;
    [self.btnTap2 setTitle:@"건강식단" forState:UIControlStateNormal];
    [self.btnTap2 setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.btnTap2 setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.btnTap3.tag = 3;
    [self.btnTap3 setTitle:@"11대 질병정보" forState:UIControlStateNormal];
    [self.btnTap3 setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.btnTap3 setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _btnTap1.selected = YES;
    _btnTap2.selected = NO;
    _btnTap3.selected = NO;
    
    [vcHIC.view setHidden:NO];
    [vcHD.view setHidden:YES];
    
    _tabBarVwXCenterConst.constant = -1 * ((DEVICE_WIDTH - 50) / 3);
    
    [Utils setUserDefault:@"health_info_title" value:@"건강칼럼"];
    [vcHIC viewInit:1];
}


@end
