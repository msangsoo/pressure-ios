//
//  HealthInfoDisInfoViewController.m
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthInfoDisInfoViewController.h"


@interface HealthInfoDisInfoViewController () {
    NSString *selectImgName;
}

@end

@implementation HealthInfoDisInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectImgName = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - press method action
- (IBAction)pressDisButton:(id)sender {
    UIButton *btn = (UIButton *)sender;
    selectImgName = [NSString stringWithFormat:@"health_info_disinfo_detail_%d",(int)btn.tag];
    
    [self performSegueWithIdentifier:@"HealthInfoDisInfoDetailViewController" sender:self];
}

#pragma mark - sugar delegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"HealthInfoDisInfoDetailViewController"]) {
        HealthInfoDisInfoDetailViewController *vc = [segue destinationViewController];
        vc.targetUrl = selectImgName;
        vc.hidesBottomBarWhenPushed = YES;
    }
}

@end
