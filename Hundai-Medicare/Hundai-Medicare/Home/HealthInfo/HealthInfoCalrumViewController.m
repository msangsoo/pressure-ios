//
//  HealthInfoCalrumViewController.m
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthInfoCalrumViewController.h"

@interface HealthInfoCalrumViewController () {
    NSMutableArray *listData;
    NSString *viewType;
    NSString *urlStr;
    NSString *searchText;
    int maxpageNumber;
    int pagecount;
    bool scrollbottom;
}

@end

@implementation HealthInfoCalrumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listData = [[NSMutableArray alloc] init];
    viewType = @"info";
    urlStr = @"";
    searchText = @"";
    scrollbottom = YES;
    pagecount = 1;
    maxpageNumber = 1;
    _searchTf.delegate = self;
    _mainTb.delegate = self;
    
    _searchVw.layer.borderWidth = 1;
    _searchVw.layer.borderColor = RGB(233, 233, 233).CGColor;
    
    _mainTb.layer.borderWidth = 1;
    _mainTb.layer.borderColor = RGB(233, 233, 233).CGColor;
}

- (void)viewInit:(int)tag {
    pagecount = 1;
    maxpageNumber = 1;
    listData = [[NSMutableArray alloc] init];
    if(tag == 1) {
        viewType = @"info";
        [self callApi:@"content_special_bbslist_search"];
    }else if(tag == 2) {
        viewType = @"eat";
        [self callApi:@"content_special_bbslist_search_food"];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if([_searchTf isFirstResponder])
        [_searchTf resignFirstResponder];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    NSLog(@"HealthInfoCalrumViewController result = %@", result);
    if([[result objectForKey:@"api_code"] isEqualToString:@"content_special_bbslist_search"]
       || [[result objectForKey:@"api_code"] isEqualToString:@"content_special_bbslist_search_food"]) {
        maxpageNumber = [[result objectForKey:@"maxpageNumber"] intValue];
        if(maxpageNumber == 0) {
            [_mainTb reloadData];
        }else if(maxpageNumber >= pagecount && pagecount == 1) {
            pagecount ++;
            listData = [result objectForKey:@"bbslist"];
            [_mainTb reloadData];
            scrollbottom = YES;
        }else {
            NSMutableArray *paths = [[NSMutableArray alloc] init];
            paths = [result objectForKey:@"bbslist"];
            
            if (paths.count > 0) {
                for (int i = 0 ; i < paths.count ; i ++) {
                    [listData addObject:[paths objectAtIndex:i]];
                }
                
                [_mainTb reloadData];
                
                pagecount ++;
                scrollbottom = YES;
            }
            
            
//            paths = [[NSMutableArray alloc] init];
//            int nowCount = (int)[listData count];
//            for(int i = 0 ; i < ((int)[listData count] - nowCount) ; i ++){
//                NSIndexPath *path = [NSIndexPath indexPathForRow:(nowCount + i) inSection:0];
//                [paths addObject:path];
//            }
//
//            int64_t delayInSeconds = 0.5;
//            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_mainTb beginUpdates];
//                [_mainTb insertRowsAtIndexPaths:[paths copy] withRowAnimation:UITableViewRowAnimationTop];
//                [_mainTb endUpdates];
//                pagecount ++;
//                scrollbottom = YES;
//            });
        }
    }
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([_searchTf isFirstResponder]) {
        [LogDB insertDb:HM01 m_cod:HM01007 s_cod:HM01007004];
        
        [_searchTf resignFirstResponder];
        
        searchText = _searchTf.text;
        pagecount = 1;
        maxpageNumber = 1;
        listData = [[NSMutableArray alloc] init];
        if([viewType isEqualToString:@"info"]) {
            [self callApi:@"content_special_bbslist_search"];
        }else if([viewType isEqualToString:@"eat"]) {
            [self callApi:@"content_special_bbslist_search_food"];
        }
    }
    
    return YES;
}

#pragma mark - press action method
- (IBAction)pressSearch:(id)sender {
    [self.view endEditing:true];
    
    [LogDB insertDb:HM01 m_cod:HM01007 s_cod:HM01007004];
    
    searchText = _searchTf.text;
    pagecount = 1;
    maxpageNumber = 1;
    listData = [[NSMutableArray alloc] init];
    if([viewType isEqualToString:@"info"]) {
        [self callApi:@"content_special_bbslist_search"];
    }else if([viewType isEqualToString:@"eat"]) {
        [self callApi:@"content_special_bbslist_search_food"];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HealthInfoCalrumTableViewCell *cell = (HealthInfoCalrumTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HealthInfoCalrumTableViewCell"];
    if (cell == nil) {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"HealthInfoCalrumTableViewCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dit = [listData objectAtIndex:indexPath.row];
    [cell.timeLbl setNotoText:[TimeUtils getDayformat:[dit objectForKey:@"info_day"] format:@"yyyy.MM.dd"]];
    [cell.titleLbl setNotoText:[dit objectForKey:@"info_subject"]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([viewType isEqualToString:@"info"])
        urlStr = [[listData objectAtIndex:indexPath.row] objectForKey:@"info_title_url"];
    else
        urlStr = [[listData objectAtIndex:indexPath.row] objectForKey:@"info_title_img"];
    
    [self performSegueWithIdentifier:@"HealthInfoDisInfoDetailViewController" sender:self];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    if(180.f > (h - y) && scrollbottom) {
        scrollbottom = NO;
        if([viewType isEqualToString:@"info"]) {
            if(maxpageNumber >= pagecount) {
                [self callApi:@"content_special_bbslist_search"];
            }
        }else if([viewType isEqualToString:@"eat"]) {
            if(maxpageNumber >= pagecount) {
                [self callApi:@"content_special_bbslist_search_food"];
            }
        }
    }
}

#pragma mark - sugar delegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"HealthInfoDisInfoDetailViewController"]) {
        HealthInfoDisInfoDetailViewController *vc = [segue destinationViewController];
        vc.targetUrl = urlStr;
        if([viewType isEqualToString:@"info"])
            vc.type = @"web";
        else
            vc.type = @"webimg";
        
        vc.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - api call
- (void)callApi:(NSString *)api_code {
    NSDictionary *parameters;
    if([api_code isEqualToString:@"content_special_bbslist_search"] || [api_code isEqualToString:@"content_special_bbslist_search_food"]) {
        parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                      api_code, @"api_code",
                      _searchTf.text, @"bbs_title",
                      [NSString stringWithFormat:@"%d",pagecount], @"pageNumber",
                      nil];
    }
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

@end
