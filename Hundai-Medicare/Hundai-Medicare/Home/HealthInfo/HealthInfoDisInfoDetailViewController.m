//
//  HealthInfoDisInfoDetailViewController.m
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthInfoDisInfoDetailViewController.h"

@interface HealthInfoDisInfoDetailViewController (){
    UIImage *selectImg2;
}

@end

@implementation HealthInfoDisInfoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:[Utils getUserDefault:@"health_info_title"]];
    
    if([_type isEqualToString:@"web"]) {
        NSURL *url = [[NSURL alloc] initWithString:_targetUrl];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        [_webVw loadRequest:request];
    }
    
    if([_type isEqualToString:@"web"]) {
        [_imgVw setHidden:YES];
        [_webVw setHidden:NO];
    }else if([_type isEqualToString:@"webimg"]) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_targetUrl]]];
        _imgVw.image = image;
        [_imgVw setFrame:CGRectMake(0, 0, self.view.frame.size.width, ((image.size.height * self.view.frame.size.width) /image.size.width) )];
        _imageViewHeightLayout.constant = ((image.size.height * self.view.frame.size.width) /image.size.width);
        [self.view layoutIfNeeded];
        [_imgVw setHidden:NO];
        [_webVw setHidden:YES];
    }else {
        selectImg2 = [UIImage imageNamed:_targetUrl];
        [_imgVw setImage:selectImg2];
        [_imgVw setFrame:CGRectMake(0, 0, self.view.frame.size.width, ((selectImg2.size.height * self.view.frame.size.width) /selectImg2.size.width) )];
        _imageViewHeightLayout.constant = ((selectImg2.size.height * self.view.frame.size.width) /selectImg2.size.width);
        [self.view layoutIfNeeded];
        [_imgVw setHidden:NO];
        [_webVw setHidden:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
