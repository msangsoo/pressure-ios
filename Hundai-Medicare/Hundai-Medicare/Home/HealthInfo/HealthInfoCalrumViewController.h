//
//  HealthInfoCalrumViewController.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "HealthInfoCalrumTableViewCell.h"
#import "HealthInfoDisInfoDetailViewController.h"
#import "TimeUtils.h"

@interface HealthInfoCalrumViewController : BaseViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UIView *searchVw;
@property (weak, nonatomic) IBOutlet UITextField *searchTf;

@property (weak, nonatomic) IBOutlet UITableView *mainTb;

- (void)viewInit:(int)tag;
- (IBAction)pressSearch:(id)sender;

@end
