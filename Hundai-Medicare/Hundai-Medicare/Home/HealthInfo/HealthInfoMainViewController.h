//
//  HealthInfoMainViewController.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "HealthInfoCalrumViewController.h"
#import "HealthInfoDisInfoViewController.h"

@interface HealthInfoMainViewController : BaseViewController {
    HealthInfoCalrumViewController *vcHIC;
    HealthInfoDisInfoViewController *vcHD;
}

@property (weak, nonatomic) IBOutlet UIButton *btnTap1;
@property (weak, nonatomic) IBOutlet UIButton *btnTap2;
@property (weak, nonatomic) IBOutlet UIButton *btnTap3;
@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwXCenterConst;

- (IBAction)pressTap:(id)sender;

@end
