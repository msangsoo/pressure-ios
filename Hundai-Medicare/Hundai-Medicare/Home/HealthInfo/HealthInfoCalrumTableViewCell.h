//
//  HealthInfoCalrumTableViewCell.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthInfoCalrumTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
