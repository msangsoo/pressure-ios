//
//  HealthInfoDisInfoViewController.h
//  HanWha
//
//  Created by INSYSTEMS CO., LTD on 2018. 4. 2..
//  Copyright © 2018년 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "HealthInfoDisInfoDetailViewController.h"

@interface HealthInfoDisInfoViewController : BaseViewController

- (IBAction)pressDisButton:(id)sender;

@end
