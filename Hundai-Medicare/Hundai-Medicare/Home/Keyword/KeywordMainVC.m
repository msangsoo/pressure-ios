//
//  KeywordMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "KeywordMainVC.h"

@interface KeywordMainVC () {
    NSArray *my_keyword_list;
    NSArray *keyword_info_man_list;
    NSArray *keyword_info_woman_list;
    
    NSArray *humManImgArr;
    NSArray *humWomanImgArr;
}

@end

@implementation KeywordMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01003 s_cod:HM01003001];
    
    [self.navigationItem setTitle:@"지난 주 상담키워드"];
    
    keyword_info_man_list = [[NSArray alloc] init];
    keyword_info_woman_list = [[NSArray alloc] init];
    
    humManImgArr = [[NSArray alloc] initWithObjects:@"img_m_10", @"img_m_20", @"img_m_30", @"img_m_40", @"img_m_50", @"img_m_60",  nil];
    humWomanImgArr = [[NSArray alloc] initWithObjects:@"img_f_10", @"img_f_20", @"img_f_30", @"img_f_40", @"img_f_50", @"img_f_60",  nil];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [self viewInit];
    
    [self apiKeyword];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
- (void)delegateResultString:(id)result CODE:(NSString*)code {
}
- (void)delegatePush:(NSInteger)status {
}
- (void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_home_keyword_list"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            my_keyword_list = [result objectForKey:@"my_keyword_list"];
            keyword_info_man_list = [result objectForKey:@"keyword_info_man_list"];
            keyword_info_woman_list = [result objectForKey:@"keyword_info_woman_list"];
            
            NSNumber *num = [NSNumber numberWithInt:[[result objectForKey:@"counsel_cnt"] intValue]];
            NSString *numberStr = [NSNumberFormatter localizedStringFromNumber:num numberStyle:NSNumberFormatterDecimalStyle];
            
            NSNumber *totnum = [NSNumber numberWithInt:[[result objectForKey:@"tot_counsel_cnt"] intValue]];
            NSString *totnumberStr = [NSNumberFormatter localizedStringFromNumber:totnum numberStyle:NSNumberFormatterDecimalStyle];
            
            [self setServiceInfo:numberStr tNum:totnumberStr];
            [self viewRefresh];
        }else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"네트워크 상태가 좋지 않습니다." completion:^{
                [self.navigationController popViewControllerAnimated:true];
            }];
        }
    }
}

#pragma mark - UICollectionViewCellDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_mBtn.isSelected) {
        return keyword_info_man_list.count;
    }
    return keyword_info_woman_list.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KeywordMainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KeywordMainCell" forIndexPath:indexPath];
    
    if (_mBtn.isSelected) {
        cell.items = keyword_info_man_list[indexPath.row];
    }else {
        cell.items = keyword_info_woman_list[indexPath.row];
    }
    
    return cell;
}

#pragma mark - UICollectionViewCellDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int index = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    _pageControl.currentPage = index;
    
    UIImage *iconImg;
    iconImg = [UIImage imageNamed:humWomanImgArr[index]];
    int age = (index + 1) * 10;
    [self.ageLbl setNotoText:[NSString stringWithFormat:@"%d대 %@은", age, age==10?@"이하 자녀를 둔 여성":
                              [NSString stringWithFormat:@"%@여성", age==60?@"이상 ":@""]]];
    
    [self setTopRankText:@"1위" text:keyword_info_woman_list[index][0][@"keyword_nm"]];
    if (_mBtn.isSelected) {
        iconImg = [UIImage imageNamed:humManImgArr[index]];
        [self.ageLbl setNotoText:[NSString stringWithFormat:@"%d대 %@은", age, age==10?@"이하 자녀를 둔 남성":
                                  [NSString stringWithFormat:@"%@남성", age==60?@"이상 ":@""]]];
        [self setTopRankText:@"1위" text:keyword_info_man_list[index][0][@"keyword_nm"]];
    }
    
    _sexImgVw.image = iconImg;
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    if(sender.tag == 1 && !self.mBtn.isSelected) {
        [self.mBtn setSelected:true];
        [self.fBtn setSelected:false];
        
        self.tabBarLeftConst.constant = 0;
        
    }else if(sender.tag == 2 && !self.fBtn.isSelected) {
        [self.mBtn setSelected:false];
        [self.fBtn setSelected:true];
        
        self.tabBarLeftConst.constant = DEVICE_WIDTH / 2;
    }
    
    UIImage *iconImg;
    iconImg = [UIImage imageNamed:humWomanImgArr[0]];
    int age = (0 + 1) * 10;
    
    [self.ageLbl setNotoText:[NSString stringWithFormat:@"%d대 %@은", age, age==10?@"이하 자녀를 둔 여성":
                              [NSString stringWithFormat:@"%@여성", age==60?@"이상 ":@""]]];
    [self setTopRankText:@"1위" text:[keyword_info_woman_list firstObject][0][@"keyword_nm"]];
    if (_mBtn.isSelected) {
        iconImg = [UIImage imageNamed:humManImgArr[0]];
        [self.ageLbl setNotoText:[NSString stringWithFormat:@"%d대 %@은", age, age==10?@"이하 자녀를 둔 남성":
                                  [NSString stringWithFormat:@"%@남성", age==60?@"이상 ":@""]]];
        [self setTopRankText:@"1위" text:[keyword_info_man_list firstObject][0][@"keyword_nm"]];
    }
    
    _sexImgVw.image = iconImg;
    
    _pageControl.currentPage = 0;
    _pageControl.numberOfPages = keyword_info_woman_list.count;
    if (_mBtn.isSelected) {
        _pageControl.numberOfPages = keyword_info_man_list.count;
    }
    
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:false];
    [self.collectionView reloadData];
}

- (IBAction)pressCall:(id)sender {
    Tr_login *login = [_model getLoginData];
    if ([login.mber_grad isEqualToString:@"10"]) {
        
        [LogDB insertDb:HM01 m_cod:HM01005 s_cod:HM01005001];
        [Utils callTelephone:self withPhone:@"15885814" message:@"1588-5814"];
    }else {
        [AlertUtils EmojiDefaultShow:self emoji:error title:@"" msg:@"현대해상 메디케어서비스 미 가입자는\n상담하기를 이용할 수 없습니다.\n\n서비스 가입 및 문의는\n1588-5656에서 가능합니다." left:@"취소" right:@"전화하기"];
    }
}

- (IBAction)pressService:(id)sender {
    [LogDB insertDb:HM01 m_cod:HM01005 s_cod:HM01005002];
    [Utils setUserDefault:NAVI_TAB_BAR_MOVE value:@"3"];
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag {
    if (tag == 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:15885656"]] options:@{} completionHandler:nil];
    }
}
#pragma mark - api
- (void)apiKeyword {
    Tr_login *login = [_model getLoginData];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_home_keyword_list", @"api_code",
                                login.mber_sn, @"mber_sn",
                                login.mber_sex, @"sex",
                                @"1", @"pageNumber",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewRefresh {
    NSMutableArray *tempSexArr = [[NSMutableArray alloc] init];
    NSMutableArray *tempSexAgeArr = [[NSMutableArray alloc] init];
    
    NSString *age = @"";
    for (NSDictionary *dic in keyword_info_man_list) {
        if (![age isEqualToString:[dic objectForKey:@"age_group"]] && ![age isEqualToString:@""]) {
            [tempSexArr addObject:[tempSexAgeArr copy]];
            tempSexAgeArr = [[NSMutableArray alloc] init];
            age = [dic objectForKey:@"age_group"];
        }
        
        [tempSexAgeArr addObject:dic];
        
        if ([age isEqualToString:@""]) {
            age = [dic objectForKey:@"age_group"];
        }
    }
    if (keyword_info_man_list.count > 0) {
        [tempSexArr addObject:[tempSexAgeArr copy]];
    }
    keyword_info_man_list = [tempSexArr copy];
    
    tempSexArr = [[NSMutableArray alloc] init];
    tempSexAgeArr = [[NSMutableArray alloc] init];
    
    age = @"";
    for (NSDictionary *dic in keyword_info_woman_list) {
        if (![age isEqualToString:[dic objectForKey:@"age_group"]] && ![age isEqualToString:@""]) {
            [tempSexArr addObject:[tempSexAgeArr copy]];
            tempSexAgeArr = [[NSMutableArray alloc] init];
            age = [dic objectForKey:@"age_group"];
        }
        [tempSexAgeArr addObject:dic];
        
        if ([age isEqualToString:@""]) {
            age = [dic objectForKey:@"age_group"];
        }
    }
    if (keyword_info_woman_list.count > 0) {
        [tempSexArr addObject:[tempSexAgeArr copy]];
    }
    keyword_info_woman_list = [tempSexArr copy];
    
    if (my_keyword_list.count > 0) {
        NSDictionary *myKeyword = [my_keyword_list firstObject];
        NSString *sexStr = [NSString stringWithFormat:@"%@여성", [myKeyword[@"age_group"] isEqualToString:@"60"]?@"이상 ":@""];
        if ([myKeyword[@"age_sex"] isEqualToString:@"1"]) {
            sexStr = [NSString stringWithFormat:@"%@남성", [myKeyword[@"age_group"] isEqualToString:@"60"]?@"이상 ":@""];
        }
        [self.ageLbl setNotoText:[NSString stringWithFormat:@"%@대 %@은", myKeyword[@"age_group"],
                                  [myKeyword[@"age_group"] isEqualToString:@"10"]?[NSString stringWithFormat:@"이하 자녀를 둔 %@", sexStr]:sexStr]];
        [self setTopRankText:@"1위" text:myKeyword[@"keyword_nm"]];
        
        UIImage *iconImg;
        int index = ([myKeyword[@"age_group"] intValue] / 10) - 1;
        iconImg = [UIImage imageNamed:humWomanImgArr[index]];
        _mBtn.selected = false;
        _fBtn.selected = true;
        self.tabBarLeftConst.constant = DEVICE_WIDTH / 2;
        if ([sexStr hasSuffix:@"남성"]) {
            iconImg = [UIImage imageNamed:humManImgArr[index]];
            _mBtn.selected = true;
            _fBtn.selected = false;
            self.tabBarLeftConst.constant = 0;
        }
        _sexImgVw.image = iconImg;
        
        _pageControl.numberOfPages = keyword_info_woman_list.count;
        _pageControl.currentPage = index;
        
        if (_mBtn.isSelected) {
            _pageControl.numberOfPages = keyword_info_man_list.count;
        }
        [self.collectionView reloadData];
        
        [_collectionView performBatchUpdates:^{
            [self.collectionView reloadData];
        } completion:^(BOOL finished) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:false];
        }];
    }else {
        _mBtn.selected = true;
        _fBtn.selected = false;
        
        UIImage *iconImg = [UIImage imageNamed:humManImgArr[0]];
        int age = (0 + 1) * 10;
        [self.ageLbl setNotoText:[NSString stringWithFormat:@"%d대 %@은", age, age==10?@"이하 자녀를 둔 남성":
                                  [NSString stringWithFormat:@"%@남성", age==60?@"이상 ":@""]]];
        _sexImgVw.image = iconImg;
        
        _pageControl.numberOfPages = keyword_info_man_list.count;
        _pageControl.currentPage = 0;
        [self.collectionView reloadData];
    }
}

- (void)viewInit {
    self.mBtn.tag = 1;
    [self.mBtn setTitle:@"남자" forState:UIControlStateNormal];
    [self.mBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.mBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.fBtn.tag = 2;
    [self.fBtn setTitle:@"여자" forState:UIControlStateNormal];
    [self.fBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.fBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.mBtn.selected = true;
    
    self.tabBarLeftConst.constant = 0;
    
    self.ageLbl.font = FONT_NOTO_MEDIUM(24);
    self.ageLbl.textColor = COLOR_NATIONS_BLUE;
    self.ageLbl.minimumScaleFactor = 0.5;
    self.ageLbl.text = @"";
    
    self.wordLbl.font = FONT_NOTO_LIGHT(24);
    self.wordLbl.textColor = COLOR_GRAY_SUIT;
    [self.wordLbl setNotoText:@"어떤 상담을 받았을까요?"];
    
    self.topRankLbl.text = @"";
    
    self.rankVw.layer.borderWidth = 1;
    self.rankVw.layer.borderColor = RGB(237, 237, 241).CGColor;
    
    self.pageControl.numberOfPages = 0;
    self.pageControl.pageIndicatorTintColor = COLOR_GRAY_SUIT;
    self.pageControl.currentPageIndicatorTintColor = COLOR_NATIONS_BLUE;
    
    self.callBtn.backgroundColor = COLOR_NATIONS_BLUE;
    self.callBtn.titleLabel.font = FONT_NOTO_BOLD(16);
    [self.callBtn setTitle:@"건강상담 받기" forState:UIControlStateNormal];
    [self.callBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.callBtn setImage:[UIImage imageNamed:@"icon_white_call"] forState:UIControlStateNormal];
    
    self.serviceBtn.backgroundColor = [UIColor whiteColor];
    self.serviceBtn.titleLabel.font = FONT_NOTO_BOLD(16);
    [self.serviceBtn setTitle:@"건강상담 서비스란?" forState:UIControlStateNormal];
    [self.serviceBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    self.serviceBtn.layer.borderWidth = 1;
    self.serviceBtn.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    
    if (DEVICE_HEIGHT > 568.0) {
        _serviceBtnHeightConst.constant = 35;
    }
    
    [self.serviceBtn layoutIfNeeded];
    [self.callBtn layoutIfNeeded];
    
    [self.serviceBtn makeToRadius:true];
    [self.callBtn makeToRadius:true];
}

#pragma mark - text method
- (void)setTopRankText:(NSString *)rank text:(NSString *)text {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSString *rankText = [NSString stringWithFormat:@"“%@", rank];
    NSMutableAttributedString *rankStr = [[NSMutableAttributedString alloc] initWithString:rankText];
    [rankStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, rankText.length)];
    [rankStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_LIGHT(24)
                     range:NSMakeRange(0, rankText.length)];
    [rankStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_ORANGE
                     range:NSMakeRange(0, rankText.length)];
    [attstr appendAttributedString:rankStr];
    
    NSMutableAttributedString *barStr = [[NSMutableAttributedString alloc] initWithString:@" / "];
    [barStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, @" / ".length)];
    [barStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_LIGHT(24)
                    range:NSMakeRange(0, @" / ".length)];
    [barStr addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_ORANGE
                    range:NSMakeRange(0, @" / ".length)];
    [attstr appendAttributedString:barStr];
    
    NSMutableAttributedString *textStr = [[NSMutableAttributedString alloc] initWithString:text];
    [textStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, text.length)];
    [textStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_BOLD(24)
                    range:NSMakeRange(0, text.length)];
    [textStr addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_ORANGE
                    range:NSMakeRange(0, text.length)];
    [attstr appendAttributedString:textStr];
    
    NSString *maskText = @"”";
    NSMutableAttributedString *maskStr = [[NSMutableAttributedString alloc] initWithString:maskText];
    [maskStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, maskText.length)];
    [maskStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_LIGHT(24)
                    range:NSMakeRange(0, maskText.length)];
    [maskStr addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_ORANGE
                    range:NSMakeRange(0, maskText.length)];
    [attstr appendAttributedString:maskStr];
    
    self.topRankLbl.attributedText = attstr;
}

- (void)setServiceInfo:(NSString *)num tNum:(NSString*)tNum {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSString *woard1 = @"상담키워드 순위는 ";
    NSMutableAttributedString *wordStr = [[NSMutableAttributedString alloc] initWithString:woard1];
    [wordStr addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, woard1.length)];
    [wordStr addAttribute:NSFontAttributeName
                    value:FONT_NOTO_REGULAR(14)
                    range:NSMakeRange(0, woard1.length)];
    [wordStr addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_DARK
                    range:NSMakeRange(0, woard1.length)];
    [attstr appendAttributedString:wordStr];
    
    NSMutableAttributedString *infoStr = [[NSMutableAttributedString alloc] initWithString:num];
    [infoStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, num.length)];
    [infoStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_BOLD(14)
                   range:NSMakeRange(0, num.length)];
    [infoStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_NATIONS_BLUE
                   range:NSMakeRange(0, num.length)];
    [attstr appendAttributedString:infoStr];
    
    NSString *woard2 = @"명 고객이 이용하신 ";
    NSMutableAttributedString *wordStr2 = [[NSMutableAttributedString alloc] initWithString:woard2];
    [wordStr2 addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, woard2.length)];
    [wordStr2 addAttribute:NSFontAttributeName
                    value:FONT_NOTO_REGULAR(14)
                    range:NSMakeRange(0, woard2.length)];
    [wordStr2 addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_DARK
                    range:NSMakeRange(0, woard2.length)];
    [attstr appendAttributedString:wordStr2];
    
    
    NSMutableAttributedString *infoStr2 = [[NSMutableAttributedString alloc] initWithString:tNum];
    [infoStr2 addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, tNum.length)];
    [infoStr2 addAttribute:NSFontAttributeName
                    value:FONT_NOTO_BOLD(14)
                    range:NSMakeRange(0, tNum.length)];
    [infoStr2 addAttribute:NSForegroundColorAttributeName
                    value:COLOR_NATIONS_BLUE
                    range:NSMakeRange(0, tNum.length)];
    [attstr appendAttributedString:infoStr2];
    
    
    NSString *word3 = @"건의 건강상담 빅데이터를 분석하여 집계됩니다. ";
    NSMutableAttributedString *word2Str = [[NSMutableAttributedString alloc] initWithString:word3];
    [word2Str addAttribute:NSKernAttributeName
                    value:[NSNumber numberWithFloat:number]
                    range:NSMakeRange(0, word3.length)];
    [word2Str addAttribute:NSFontAttributeName
                    value:FONT_NOTO_REGULAR(14)
                    range:NSMakeRange(0, word3.length)];
    [word2Str addAttribute:NSForegroundColorAttributeName
                    value:COLOR_MAIN_DARK
                    range:NSMakeRange(0, word3.length)];
    [attstr appendAttributedString:word2Str];
    
    self.serviceInfoLbl.attributedText = attstr;
}

@end
