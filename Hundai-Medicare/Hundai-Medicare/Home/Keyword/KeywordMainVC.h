//
//  KeywordMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 18/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "KeywordMainCell.h"

@interface KeywordMainVC : BaseViewController<ServerCommunicationDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIButton *mBtn;
@property (weak, nonatomic) IBOutlet UIButton *fBtn;

@property (weak, nonatomic) IBOutlet UIImageView *sexImgVw;
@property (weak, nonatomic) IBOutlet UILabel *ageLbl;
@property (weak, nonatomic) IBOutlet UILabel *wordLbl;

@property (weak, nonatomic) IBOutlet UILabel *topRankLbl;

@property (weak, nonatomic) IBOutlet UIView *rankVw;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *serviceInfoLbl;

@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarLeftConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceBtnHeightConst;

@end
