//
//  KeywordMainCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 25/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeywordMainCell : UICollectionViewCell<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *items;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@interface KeywordMainTitleCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *rankLbl;
@property (weak, nonatomic) IBOutlet UILabel *middleLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
