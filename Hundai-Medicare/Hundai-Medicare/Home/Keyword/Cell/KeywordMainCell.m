//
//  KeywordMainCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 25/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "KeywordMainCell.h"

@implementation KeywordMainCell

- (void)awakeFromNib  {
    [super awakeFromNib];
    
    _items = [[NSArray alloc] init];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)setItems:(NSArray *)items {
    _items = items;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewCellDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _items.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height / _items.count);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KeywordMainTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KeywordMainTitleCell" forIndexPath:indexPath];
    
//    NSString *keywordStr = [NSString stringWithFormat:@"%d위   /   %@",(int)(indexPath.row + 1), [_items objectAtIndex:indexPath.row][@"keyword_nm"]];
//    [cell.titleLbl setNotoText:keywordStr];
    
    [cell.rankLbl setNotoText:[NSString stringWithFormat:@"%d위", (int)(indexPath.row + 1)]];
    [cell.titleLbl setNotoText:[NSString stringWithFormat:@"%@", [_items objectAtIndex:indexPath.row][@"keyword_nm"]]];
    
    cell.rankLbl.textColor = COLOR_MAIN_DARK;
    cell.middleLbl.textColor = COLOR_MAIN_DARK;
    cell.titleLbl.textColor = COLOR_MAIN_DARK;
    if (indexPath.row > 2) {
        cell.rankLbl.textColor = COLOR_GRAY_SUIT;
        cell.middleLbl.textColor = COLOR_GRAY_SUIT;
        cell.titleLbl.textColor = COLOR_GRAY_SUIT;
    }
    
    return cell;
}

#pragma mark - UICollectionViewCellDelegate

@end

@implementation KeywordMainTitleCell

- (void)awakeFromNib  {
    [super awakeFromNib];
    
    self.rankLbl.font = FONT_NOTO_REGULAR(14);
    self.rankLbl.text = @"";
    
    self.middleLbl.font = FONT_NOTO_REGULAR(7);
    self.middleLbl.text = @"/";
    
    _titleLbl.font = FONT_NOTO_REGULAR(14);
    _titleLbl.text = @"";
}

@end

