//
//  MyMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyInfoMainVC.h"
#import "ServiceHistoryMainVC.h"

@interface MyMainVC : UIViewController {
    MyInfoMainVC *vcMyInfo;
    ServiceHistoryMainVC *vcHistory;
}

@property (weak, nonatomic) IBOutlet UIButton *myinfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

- (void)naviPageMove:(int)index;
@end
