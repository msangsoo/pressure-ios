//
//  ServiceHistoryMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "TimeUtils.h"
#import "ServiceDetailVC.h"


@interface ServiceHistoryMainVC : BaseViewController<UITableViewDataSource, UITableViewDelegate, ServerCommunicationDelegate>
{
    NSMutableArray *arrData;
    NSArray *arrAPI;
    int pageNumber;
    long currService;
    BOOL isRequest;
    BOOL isEnd;
}
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *diagnosisBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkupBtn;
@property (weak, nonatomic) IBOutlet UIButton *visitBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblNotRecord;


@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
