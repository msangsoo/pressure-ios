//
//  ServiceHistoryDetailCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceHistoryDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *firstLbl;
@property (weak, nonatomic) IBOutlet UILabel *firstCha1;
@property (weak, nonatomic) IBOutlet UILabel *firstInfoLbl;

@property (weak, nonatomic) IBOutlet UILabel *firstCha2;
@property (weak, nonatomic) IBOutlet UILabel *firstInfoLbl2;

@property (weak, nonatomic) IBOutlet UILabel *secondLbl;
@property (weak, nonatomic) IBOutlet UILabel *secondInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCha1Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCha2Width;



@end
