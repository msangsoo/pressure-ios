//
//  ServiceHistoryDefaultCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ServiceHistoryDefaultCell.h"

@implementation ServiceHistoryDefaultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.dateLbl.font = FONT_NOTO_LIGHT(13);
    self.dateLbl.textColor = COLOR_GRAY_SUIT;
    
    self.titleLbl.font = FONT_NOTO_REGULAR(17);
    self.titleLbl.textColor = COLOR_MAIN_DARK;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
