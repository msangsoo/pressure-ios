//
//  ServiceHistoryDetailCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ServiceHistoryDetailCell.h"

@implementation ServiceHistoryDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

#pragma mark - viewInit
- (void)viewInit {
    _firstLbl.font = FONT_NOTO_REGULAR(16);
    _firstLbl.textColor = COLOR_MAIN_DARK;
    _firstLbl.text = @"";
    
    _firstInfoLbl.font = FONT_NOTO_REGULAR(16);
    _firstInfoLbl.textColor = COLOR_GRAY_SUIT;
    _firstInfoLbl.text = @"";
    
    _firstInfoLbl2.font = FONT_NOTO_REGULAR(16);
    _firstInfoLbl2.textColor = COLOR_GRAY_SUIT;
    _firstInfoLbl2.text = @"";
    
    _firstCha1.font = FONT_NOTO_REGULAR(16);
    _firstCha1.textColor = COLOR_MAIN_DARK;
    _firstCha1.text = @"1차";
    
    _firstCha2.font = FONT_NOTO_REGULAR(16);
    _firstCha2.textColor = COLOR_MAIN_DARK;
    _firstCha2.text = @"2차";
    
    _secondLbl.font = FONT_NOTO_REGULAR(16);
    _secondLbl.textColor = COLOR_MAIN_DARK;
    _secondLbl.text = @"";
    
    _secondInfoLbl.font = FONT_NOTO_REGULAR(16);
    _secondInfoLbl.textColor = COLOR_GRAY_SUIT;
    _secondInfoLbl.text = @"";
    
    _statusLbl.font = FONT_NOTO_REGULAR(16);
    _statusLbl.textColor = COLOR_MAIN_ORANGE;
    _statusLbl.text = @"";
    
    _locationLbl.font = FONT_NOTO_REGULAR(16);
    _locationLbl.textColor = [UIColor whiteColor];
    _locationLbl.text = @"";
}

@end
