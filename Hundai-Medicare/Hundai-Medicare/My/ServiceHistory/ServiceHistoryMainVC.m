//
//  ServiceHistoryMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ServiceHistoryMainVC.h"
#import "ServiceHistoryDefaultCell.h"
#import "ServiceHistoryDetailCell.h"

@interface ServiceHistoryMainVC ()

@end

@implementation ServiceHistoryMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    server.delegate = self;
    arrData = [[NSMutableArray alloc] init];
    
    isEnd = NO;
    pageNumber = 1;
    currService = 1;
    arrAPI  = @[@"serviceCounsel",          // 전화상담
                @"serviceReserve",          // 검진예약
                @"mber_hospital_booking",   // 방문간호
                @"serviceReserve_visit"     // 방문간호
                ];
    
    [self viewInit];
    [self requestData];
    
    [LogDB insertDb:HM06 m_cod:HM06016 s_cod:HM06016001];
}
#pragma mark -requestData
- (void)requestData{
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                arrAPI[currService-1], @"api_code",
                                [NSString stringWithFormat:@"%d", pageNumber], @"pageNumber",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:arrAPI[currService-1]]){
       
        
        NSString *retKey = arrAPI[currService-1];
        if (currService == 3) {
            retKey= @"booking_arr";
        }
        if ([[result objectForKey:retKey] count] == 0) {
            isEnd = YES;
        }else{
            [arrData addObjectsFromArray:[result objectForKey:retKey]];
        }
        isRequest = NO;
        [_tableView reloadData];
        
        
        if (pageNumber==1 && [[result objectForKey:retKey] count] == 0) {
            self.lblNotRecord.hidden = NO;
        }else {
            self.lblNotRecord.hidden = YES;
        }
    }
}


#pragma mark -UITableViewDelegate-

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (currService==1) {
        return 75;
    }else {
        return 136;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    
    if (currService ==1) {
        static NSString *cellIdentifier = @"ServiceHistoryDefaultCell";
        
        ServiceHistoryDefaultCell *cell = (ServiceHistoryDefaultCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ServiceHistoryDefaultCell" bundle:nil] forCellReuseIdentifier:@"ServiceHistoryDefaultCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        NSDictionary *dic = [arrData objectAtIndex:row];
        [cell.dateLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"time"]]];
        [cell.titleLbl setNotoText:[dic objectForKey:@"title"]];
        
        
        if (row >= [arrData count]-1 && !isRequest && !isEnd) {
            isRequest = YES;
            pageNumber ++;
            [self requestData];
        }
        
        return cell;
    }else if (currService ==2) {
        static NSString *cellIdentifier = @"ServiceHistoryDetailCell";
        
        ServiceHistoryDetailCell *cell = (ServiceHistoryDetailCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ServiceHistoryDetailCell" bundle:nil] forCellReuseIdentifier:@"ServiceHistoryDetailCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        cell.firstInfoLbl2.hidden = YES;
        [cell.constCha1Width setActive:YES];
        [cell.constCha2Width setActive:YES];
        
        NSDictionary *dic = [arrData objectAtIndex:row];
        [cell.firstLbl setNotoText:@"신청일"];
        [cell.secondLbl setNotoText:@"예약일"];
        
        [cell.firstInfoLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"regdate"]]];
        [cell.secondInfoLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"resrv_de"]]];
        
        NSString * txt= [NSString stringWithFormat:@"%@/%@",
                          [dic objectForKey:@"hospitalName"],
                          [dic objectForKey:@"resrv_step"]];
        
        [cell.locationLbl setNotoText:txt];
        [cell.statusLbl setNotoText:@"완료"];
        
        return cell;
    }else if (currService ==3) {
        static NSString *cellIdentifier = @"ServiceHistoryDetailCell";
        
        ServiceHistoryDetailCell *cell = (ServiceHistoryDetailCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ServiceHistoryDetailCell" bundle:nil] forCellReuseIdentifier:@"ServiceHistoryDetailCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        
        cell.firstInfoLbl2.hidden = YES;
        cell.firstCha1.hidden = YES;
        cell.firstCha2.hidden = YES;
        [cell.constCha1Width setActive:NO];
        [cell.constCha2Width setActive:NO];
        
        NSDictionary *dic = [arrData objectAtIndex:row];
        [cell.firstLbl setNotoText:@"신청일"];
        [cell.secondLbl setNotoText:@"예약일"];
        
        [cell.firstInfoLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"reg_date"]]];
        
        
        if ([[dic objectForKey:@"first_date"] length] >= 8) {
            cell.firstCha1.hidden = NO;
            [cell.constCha1Width setActive:NO];
            
            NSString *txt = [NSString stringWithFormat:@" : %@",
                             [TimeUtils getDayKoreaformat:[dic objectForKey:@"first_date"]]];
            [cell.secondInfoLbl setNotoText:txt];
        }
        
        if ([[dic objectForKey:@"second_date"] length] >= 8) {
            cell.firstCha2.hidden = NO;
            cell.firstInfoLbl2.hidden = NO;
            [cell.constCha2Width setActive:NO];
            
            NSString *txt = [NSString stringWithFormat:@" : %@",
                             [TimeUtils getDayKoreaformat:[dic objectForKey:@"second_date"]]];
            [cell.firstInfoLbl2 setNotoText:txt];
        }
        
        NSString *firStr = cell.secondInfoLbl.text;
        [cell.secondInfoLbl setNotoText:[NSString stringWithFormat:@"%@  / ", firStr]];
        
        
        NSString * txt= [NSString stringWithFormat:@"%@",
                         [dic objectForKey:@"h_name"]];
        
        [cell.locationLbl setNotoText:txt];
        [cell.statusLbl setNotoText:[dic objectForKey:@"step"]];
        
        return cell;
    }else  {
        static NSString *cellIdentifier = @"ServiceHistoryDetailCell";
        
        ServiceHistoryDetailCell *cell = (ServiceHistoryDetailCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ServiceHistoryDetailCell" bundle:nil] forCellReuseIdentifier:@"ServiceHistoryDetailCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        cell.firstInfoLbl2.hidden = YES;
        cell.firstCha1.hidden = YES;
        cell.firstCha2.hidden = YES;
        [cell.constCha1Width setActive:YES];
        [cell.constCha2Width setActive:YES];
        
        
        NSDictionary *dic = [arrData objectAtIndex:row];
        [cell.firstLbl setNotoText:@"신청일"];
        [cell.secondLbl setNotoText:@"방문일"];
        
        [cell.firstInfoLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"regdate"]]];
        [cell.secondInfoLbl setNotoText:[TimeUtils getDayKoreaformat:[dic objectForKey:@"visit_day"]]];
        
        NSString * txt=@"";
        
        if ([[dic objectForKey:@"hospital"] length] > 0) {
            txt = [NSString stringWithFormat:@"%@", [dic objectForKey:@"hospital"]];
        }
        
        if ([[dic objectForKey:@"branch"] length] > 0) {
            if ([txt length] > 0) {
                txt = [NSString stringWithFormat:@"%@/", txt];
            }
            txt = [NSString stringWithFormat:@"%@%@", txt, [dic objectForKey:@"branch"]];
        }
        
        if ([dic objectForKey:@"jindan"] > 0) {
            if ([txt length] > 0) {
                txt = [NSString stringWithFormat:@"%@/", txt];
            }
            txt = [NSString stringWithFormat:@"%@%@", txt, [dic objectForKey:@"jindan"]];
        }
        
        [cell.locationLbl setNotoText:txt];
        
        NSString *name = [NSString stringWithFormat:@"%@간호사", [dic objectForKey:@"charge"]];
        [cell.statusLbl setNotoText:name];
        
        return cell;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currService ==1) {
        long row = indexPath.row;
        
        NSDictionary *dic = [arrData objectAtIndex:row];
        
        ServiceDetailVC *vc = [SERVICEHISOTRY_STORYBOARD instantiateViewControllerWithIdentifier:@"ServiceDetailVC"];
        vc.strSeq = [dic objectForKey:@"seq"];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
        
    }
    
}


#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    
    isEnd = NO;
    pageNumber = 1;
    
    [arrData removeAllObjects];
    [_tableView reloadData];
    
    
    currService = sender.tag;
    
    [_callBtn setSelected:NO];
    [_diagnosisBtn setSelected:NO];
    [_checkupBtn setSelected:NO];
    [_visitBtn setSelected:NO];
    
    BOOL viewChage = false;
    
    if(sender.tag == 1 && !_callBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06016 s_cod:HM06016001];
        
        [_callBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 2 && !_diagnosisBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06017 s_cod:HM06017001];
        
        [_diagnosisBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 3 && !_checkupBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06018 s_cod:HM06018001];
        
        [_checkupBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 4 && !_visitBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06019 s_cod:HM06019001];
        
        [_visitBtn setSelected:YES];
        viewChage = true;
    }
    
    [self requestData];
}

#pragma mark - viewInit
- (void)viewInit {
    _callBtn.tag = 1;
    [_callBtn setTitle:@"전화상담" forState:UIControlStateNormal];
    [_callBtn mediServiceTabButton];
    
    _diagnosisBtn.tag = 2;
    [_diagnosisBtn setTitle:@"진료예약" forState:UIControlStateNormal];
    [_diagnosisBtn mediServiceTabButton];
    
    _checkupBtn.tag = 3;
    [_checkupBtn setTitle:@"검진예약" forState:UIControlStateNormal];
    [_checkupBtn mediServiceTabButton];
    
    _visitBtn.tag = 4;
    [_visitBtn setTitle:@"방문간호" forState:UIControlStateNormal];
    [_visitBtn mediServiceTabButton];
    
    _callBtn.selected = true;
}

@end
