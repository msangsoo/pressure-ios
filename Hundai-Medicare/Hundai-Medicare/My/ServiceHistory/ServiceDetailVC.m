//
//  ServiceDetailVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "ServiceDetailVC.h"

@interface ServiceDetailVC ()

@end

@implementation ServiceDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"전화상담내역"];
    
    server.delegate = self;
    
    [self viewInit];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}


#pragma mark -requestData
- (void)requestData{
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"serviceCounselContents", @"api_code",
                                self.strSeq, @"seq",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"serviceCounselContents"]){
        NSDictionary *contents = [[result objectForKey:@"serviceCounselContents"] objectAtIndex:0];
        
        [self.titleLbl setNotoText:[contents objectForKey:@"title"]];
        [self.questionMsgLbl setNotoText:[contents objectForKey:@"question"]];
        [self.anserMsgLbl setNotoText:[contents objectForKey:@"answer"]];
        [self.questionLbl setNotoText:@"질문"];
        [self.anserLbl setNotoText:@"답변"];
    }
}




#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_REGULAR(16);
    _titleLbl.textColor = COLOR_GRAY_SUIT;
    _titleLbl.text = @"";
    
    _questionLbl.font = FONT_NOTO_REGULAR(18);
    _questionLbl.textColor = [UIColor whiteColor];
    [_questionLbl setNotoText:@"질문"];
    
    _questionMsgLbl.font = FONT_NOTO_REGULAR(18);
    _questionMsgLbl.textColor = COLOR_GRAY_SUIT;
    _questionMsgLbl.text = @"";
    
    _anserLbl.font = FONT_NOTO_REGULAR(18);
    _anserLbl.textColor = [UIColor whiteColor];
    [_anserLbl setNotoText:@"답변"];
    
    _anserMsgLbl.font = FONT_NOTO_REGULAR(18);
    _anserMsgLbl.textColor = COLOR_GRAY_SUIT;
    _anserMsgLbl.text = @"";
}

@end
