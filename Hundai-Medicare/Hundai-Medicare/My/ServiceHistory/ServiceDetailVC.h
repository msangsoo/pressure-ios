//
//  ServiceDetailVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface ServiceDetailVC : BaseViewController<ServerCommunicationDelegate>

@property (nonatomic, retain) NSString *strSeq;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *questionLbl;
@property (weak, nonatomic) IBOutlet UILabel *questionMsgLbl;
@property (weak, nonatomic) IBOutlet UILabel *anserLbl;
@property (weak, nonatomic) IBOutlet UILabel *anserMsgLbl;

@end
