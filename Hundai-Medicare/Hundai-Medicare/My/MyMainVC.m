//
//  MyMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyMainVC.h"

@interface MyMainVC ()

@end

@implementation MyMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"마이페이지"];
    
    [self viewInit];
    
    vcMyInfo = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"MyInfoMainVC"];
    [self addChildViewController:vcMyInfo];
    vcMyInfo.view.bounds = _vwMain.bounds;
    vcMyInfo.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcMyInfo.view];
    [vcMyInfo.view setHidden:false];
    
    vcHistory = [SERVICEHISOTRY_STORYBOARD instantiateViewControllerWithIdentifier:@"ServiceHistoryMainVC"];
    [self addChildViewController:vcHistory];
    vcHistory.view.bounds = _vwMain.bounds;
    vcHistory.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHistory.view];
    [vcHistory.view setHidden:true];
    
    [self.myinfoBtn setSelected:true];
    [self.historyBtn setSelected:false];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.myinfoBtn setSelected:false];
    [self.historyBtn setSelected:false];
    
    if(!vcMyInfo.view.isHidden){
        [self.myinfoBtn setSelected:true];
        self.tabBarVwLeftConst.constant = 0;
        
        [vcMyInfo.view setHidden:false];
        [vcHistory.view setHidden:true];
        //        [vcFW calViewInit];
    }else{
        [self.historyBtn setSelected:true];
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcMyInfo.view setHidden:true];
        [vcHistory.view setHidden:false];
        //        [vcFH viewInit];
    }
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    if(sender.tag == 1 && !self.myinfoBtn.isSelected) {
        [self.myinfoBtn setSelected:true];
        [self.historyBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [vcMyInfo.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        //        [vcFW calViewInit];
    }else if(sender.tag == 2 && !self.historyBtn.isSelected){
        [self.myinfoBtn setSelected:false];
        [self.historyBtn setSelected:true];
        
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcMyInfo.view setHidden:true];
        [vcHistory.view setHidden:false];
        //        [vcFH viewInit];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.myinfoBtn.tag = 1;
    [self.myinfoBtn setTitle:@"내 정보" forState:UIControlStateNormal];
    [self.myinfoBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.myinfoBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.historyBtn.tag = 2;
    [self.historyBtn setTitle:@"서비스 이용내역" forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
}

#pragma mark - page move
- (void)naviPageMove:(int)index {
    if (index == 1) {
        [self.myinfoBtn setSelected:true];
        [self.historyBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [vcMyInfo.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        UIViewController *vc = [MYPOINT_STORYBOARD instantiateInitialViewController];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
    }
}

@end
