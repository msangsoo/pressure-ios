//
//  HealthAlarmEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthAlarmEditVC.h"

@interface HealthAlarmEditVC () {
    NSString *msgPlaceholder;
    NSDateFormatter *format;
    NSDate *timeDate;
    NSDictionary *notiUserInfo;
}

@end

@implementation HealthAlarmEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06013 s_cod:HM06013001];
    
    [self.navigationItem setTitle:@"알람 추가하기"];
    
    [self navigationSetup];
    
    msgPlaceholder = @"식사 맛있게 하셨나요? 식단을 입력해주세요.";
    
    notiUserInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                     [NSNumber numberWithInt:8], @"I_BUFFER2",
                     @"1", @"index",
                    nil];
    
    format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"HH:mm"];
    timeDate = [NSDate date];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Actions
- (IBAction)pressSave:(UIButton *)sender {
    NSDate *date = [_timePicker date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    // 알림 시간 설정
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    
    AlarmObject *newAlarmObject;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *alarmListData = [defaults objectForKey:ALARM_LIST_DATA];
    NSMutableArray *alarmList = [NSKeyedUnarchiver unarchiveObjectWithData:alarmListData];
    
    if(!alarmList) {
        alarmList = [[NSMutableArray alloc]init];
    }

    newAlarmObject = [[AlarmObject alloc]init];
    newAlarmObject.enabled = YES;
    newAlarmObject.notificationID = [self getUniqueNotificationID];
    newAlarmObject.userInfo = notiUserInfo;

    [dateFormat setDateFormat:@"HH"];
    dateComponents.hour    = [[dateFormat stringFromDate:date] intValue];
    
    [dateFormat setDateFormat:@"mm"];
    dateComponents.minute    = [[dateFormat stringFromDate:date] intValue];
    
    dateComponents.second = 0;
    
    NSLog(@"dateComponents : %@", dateComponents);
    
    NSDate *fireDate = [calendar dateFromComponents:dateComponents];
    
    // Schedule the notification
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.fireDate = [calendar dateFromComponents:dateComponents];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.repeatInterval = NSCalendarUnitDay;
    localNotification.alertTitle = @"";
    
    NSNumber* uidToStore = [NSNumber numberWithInt:newAlarmObject.notificationID];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                              uidToStore, @"notificationID",
                              notiUserInfo, @"aps",
                              nil];
    localNotification.userInfo = userInfo;
    localNotification.alertBody = [NSString stringWithFormat:@"%@", _msgTv.text];

    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    newAlarmObject.origiTime = date;
    newAlarmObject.label = _msgTv.text;
    newAlarmObject.name = _kindTitleLbl.text;
    [dateFormat setDateFormat:@"HH:mm"];
    newAlarmObject.timeToSetOff = [dateFormat stringFromDate:date];
    newAlarmObject.enabled = YES;
    [alarmList addObject:newAlarmObject];
    
    NSData *alarmListData2 = [NSKeyedArchiver archivedDataWithRootObject:alarmList];
    [[NSUserDefaults standardUserDefaults] setObject:alarmListData2 forKey:ALARM_LIST_DATA];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pressKind:(UIButton *)sender {
    [self prepareActionSheet];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (_msgTv.tag == 0) {
        _msgTv.tag = 1;
        _msgTv.textColor = COLOR_NATIONS_BLUE;
        _msgTv.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (_msgTv.text.length < 1) {
        _msgTv.tag = 0;
        _msgTv.textColor = COLOR_GRAY_SUIT;
        _msgTv.text = msgPlaceholder;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (textView.text.length > 49 && ![text isEqualToString:@""]) {
        return false;
    }
    
    return true;
}

- (void)textViewDidChange:(UITextView *)textView {
    _msgVwHeightConst.constant = ceilf([_msgTv sizeThatFits:CGSizeMake(_msgTv.frame.size.width, FLT_MAX)].height) + 10;
    
    if (_msgVwHeightConst.constant < 82) {
        _msgVwHeightConst.constant = 82;
    }
    
    [_msgVw layoutIfNeeded];
    [_msgVw updateConstraints];
    
    [_msgTv layoutIfNeeded];
}

#pragma mark - viewInit
- (void)viewInit {
    _timePicker = [[UIDatePicker alloc] init];
    _timePicker.datePickerMode = UIDatePickerModeTime;
    _timeTf.inputView = _timePicker;
    _timeTf.delegate = self;
    
    _kindLbl.font = FONT_NOTO_REGULAR(14);
    _kindLbl.textColor = COLOR_GRAY_SUIT;
    [_kindLbl setNotoText:@"알람 종류 선택"];
    
    _kindTitleLbl.font = FONT_NOTO_BOLD(18);
    _kindTitleLbl.textColor = COLOR_NATIONS_BLUE;
    _kindTitleLbl.text = @"식사";
    
    _timeLbl.font = FONT_NOTO_REGULAR(14);
    _timeLbl.textColor = COLOR_GRAY_SUIT;
    [_timeLbl setNotoText:@"알람 시간"];
    
    _timeTf.font = FONT_NOTO_BOLD(18);
    _timeTf.textColor = COLOR_NATIONS_BLUE;
    _timeTf.text = [format stringFromDate:timeDate];
    
    _msgLbl.font = FONT_NOTO_REGULAR(14);
    _msgLbl.textColor = COLOR_GRAY_SUIT;
    [_msgLbl setNotoText:@"알람 메시지"];
    
    _msgTv.font = FONT_NOTO_REGULAR(18);
    _msgTv.textColor = COLOR_GRAY_SUIT;
    _msgTv.tag = 0;
    _msgTv.text = msgPlaceholder;
    _msgTv.delegate = self;
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"저장"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressSave:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

#pragma mark - subview action sheet Mehtod
- (void)prepareActionSheet {
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *food = [UIAlertAction
                          actionWithTitle:@"식사"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action) {
                              self->_kindTitleLbl.text = @"식사";
                              self->_iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_food.png"];
                              self->msgPlaceholder = @"식사 맛있게 하셨나요? 식단을 입력해주세요.";
                              self->_msgTv.text = self->msgPlaceholder;
                              self->notiUserInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                    [NSNumber numberWithInt:8], @"I_BUFFER2",
                                                    @"1", @"index",
                                                    nil];
                          }];
    UIAlertAction *weight = [UIAlertAction
                              actionWithTitle:@"체중"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  self->_kindTitleLbl.text = @"체중";
                                  self->_iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_weight.png"];
                                  self->msgPlaceholder = @"체중 입력하실 시간이에요.";
                                  self->_msgTv.text = self->msgPlaceholder;
                                  self->notiUserInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                        [NSNumber numberWithInt:8], @"I_BUFFER2",
                                                        @"3", @"index",
                                                        nil];
                              }];
    UIAlertAction *pressure = [UIAlertAction
                                actionWithTitle:@"혈압"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    self->_kindTitleLbl.text = @"혈압";
                                    self->_iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_pre.png"];
                                    self->msgPlaceholder = @"혈압 입력하실 시간이에요.";
                                    self->_msgTv.text = self->msgPlaceholder;
                                    self->notiUserInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                          [NSNumber numberWithInt:8], @"I_BUFFER2",
                                                          @"4", @"index",
                                                          nil];
                                }];
    UIAlertAction *sugar = [UIAlertAction
                               actionWithTitle:@"혈당"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   self->_kindTitleLbl.text = @"혈당";
                                   self->_iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_sugar.png"];
                                   self->msgPlaceholder = @"혈당 입력하실 시간이에요.";
                                   self->_msgTv.text = self->msgPlaceholder;
                                   self->notiUserInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                         [NSNumber numberWithInt:8], @"I_BUFFER2",
                                                         @"5", @"index",
                                                         nil];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"취소"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 [actionController dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [actionController addAction:food];
    [actionController addAction:weight];
    [actionController addAction:pressure];
    [actionController addAction:sugar];
    [actionController addAction:cancel];
    
    [self presentViewController:actionController animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 선택한 값을 UITextField의 text값에 반영
     _timeTf.text = [format stringFromDate:_timePicker.date];
}

#pragma mark - sys
//Get Unique Notification ID for a new alarm O(n)
-(int)getUniqueNotificationID
{
    NSMutableDictionary * hashDict = [[NSMutableDictionary alloc]init];
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSNumber *uid= [userInfoCurrent valueForKey:@"notificationID"];
        NSNumber * value =[NSNumber numberWithInt:1];
        [hashDict setObject:value forKey:uid];
    }
    for (int i=0; i<[eventArray count]+1; i++)
    {
        NSNumber * value = [hashDict objectForKey:[NSNumber numberWithInt:i]];
        if(!value)
        {
            return i;
        }
    }
    return 0;
    
}

@end
