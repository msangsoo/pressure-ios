//
//  AlarmObject.m
//  HealthCare
//
//  Created by INSYSTEMS on 2016. 10. 28..
//  Copyright © 2016년 jangkun lee. All rights reserved.
//

#import "AlarmObject.h"

@implementation AlarmObject

@synthesize name;
@synthesize label;
@synthesize timeToSetOff;
@synthesize enabled;
@synthesize notificationID;
@synthesize origiTime;

//This is important to for saving the alarm object in user defaults
-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.label forKey:@"label"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.timeToSetOff forKey:@"timeToSetOff"];
    [encoder encodeObject:self.origiTime forKey:@"origiTime"];
    [encoder encodeBool:self.enabled forKey:@"enabled"];
    [encoder encodeInt:self.notificationID forKey:@"notificationID"];
}
//This is important to for loading the alarm object from user defaults
-(id)initWithCoder:(NSCoder *)decoder
{
    self.label = [decoder decodeObjectForKey:@"label"];
    self.name = [decoder decodeObjectForKey:@"name"];
    self.timeToSetOff = [decoder decodeObjectForKey:@"timeToSetOff"];
    self.origiTime = [decoder decodeObjectForKey:@"origiTime"];
    self.enabled = [decoder decodeBoolForKey:@"enabled"];
    self.notificationID = [decoder decodeIntForKey:@"notificationID"];
    return self;
}


@end
