//
//  HealthAlarmMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "AlarmObject.h"

@interface HealthAlarmMainVC : BaseViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


#pragma mark - Cell
@interface HealthAlarmMainCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *swichBtn;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImgVw;
@property (strong, nonatomic) IBOutlet UILabel *TimeLbl;

@end

