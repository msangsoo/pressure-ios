//
//  AlarmObject.h
//  HealthCare
//
//  Created by INSYSTEMS on 2016. 10. 28..
//  Copyright © 2016년 jangkun lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlarmObject : NSObject<NSCoding>


@property(nonatomic,strong) NSString *label;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *timeToSetOff;
@property(nonatomic,strong) NSDate *origiTime;
@property(nonatomic,strong) NSDictionary *userInfo;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic,assign) int notificationID;


@end
