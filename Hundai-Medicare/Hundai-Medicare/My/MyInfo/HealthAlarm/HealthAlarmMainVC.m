//
//  HealthAlarmMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthAlarmMainVC.h"

@interface HealthAlarmMainVC () {
    NSMutableArray *items;
    int selectNum;
    NSMutableDictionary *_swifDic;
}

@end

@implementation HealthAlarmMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06012 s_cod:HM06012001];
    
    [self.navigationItem setTitle:@"건강기록 알람 설정"];
    
    items = [[NSMutableArray alloc] init];
    _swifDic = [[NSMutableDictionary alloc] init];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *alarmListData = [defaults objectForKey:ALARM_LIST_DATA];
    items = [NSKeyedUnarchiver unarchiveObjectWithData:alarmListData];
    
    [_tableView reloadData];
}

#pragma mark - Actions
- (IBAction)pressAdd:(UIButton *)sender {
    UIViewController *vc = [HEALTHALARM_STORYBOARD instantiateViewControllerWithIdentifier:@"HealthAlarmEditVC"];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
    
}

- (void)alarmSwtichValueChage:(UIButton *)sender {
    HealthAlarmMainCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(int)sender.tag inSection:0]];
    if (sender.isSelected) {
        [cell.swichBtn setSelected:false];
        AlarmObject *currentAlarm = [self->items objectAtIndex:sender.tag];
        
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *eventArray = [app scheduledLocalNotifications];
        
        for (int i=0; i<[eventArray count]; i++) {
            UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
            NSDictionary *userInfoCurrent = oneEvent.userInfo;
            NSString *uid = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
            if ([uid isEqualToString:[NSString stringWithFormat:@"%i", currentAlarm.notificationID]]) {
                //Cancelling local notification
                [app cancelLocalNotification:oneEvent];
            }
        }
        
        currentAlarm.enabled = false;
        [self->items replaceObjectAtIndex:sender.tag withObject:currentAlarm];
        NSData *alarmListData = [NSKeyedArchiver archivedDataWithRootObject:self->items];
        [[NSUserDefaults standardUserDefaults] setObject:alarmListData forKey:ALARM_LIST_DATA];
    }else {
        [cell.swichBtn setSelected:true];
        
        AlarmObject *currentAlarm = [self->items objectAtIndex:sender.tag];
        
        NSDate *date = currentAlarm.origiTime;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        // 알림 시간 설정
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [calendar setLocale:[NSLocale currentLocale]];
        
        NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
        
        [dateFormat setDateFormat:@"HH"];
        dateComponents.hour    = [[dateFormat stringFromDate:date] intValue];
        
        [dateFormat setDateFormat:@"mm"];
        dateComponents.minute    = [[dateFormat stringFromDate:date] intValue];
        
        dateComponents.second = 0;
        
        NSLog(@"dateComponents : %@", dateComponents);

        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.timeZone = [NSTimeZone localTimeZone];
        localNotification.fireDate = [calendar dateFromComponents:dateComponents];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.repeatInterval = NSCalendarUnitDay;
        localNotification.alertTitle = @"";
        
        NSNumber *uidToStore = [NSNumber numberWithInt:currentAlarm.notificationID];
        NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  uidToStore, @"notificationID",
                                  currentAlarm.userInfo, @"aps",
                                  nil];
        localNotification.userInfo = userInfo;
        localNotification.alertBody = [NSString stringWithFormat:@"%@", currentAlarm.label];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        currentAlarm.enabled = true;
        [self->items replaceObjectAtIndex:sender.tag withObject:currentAlarm];
        NSData *alarmListData = [NSKeyedArchiver archivedDataWithRootObject:self->items];
        [[NSUserDefaults standardUserDefaults] setObject:alarmListData forKey:ALARM_LIST_DATA];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HealthAlarmMainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HealthAlarmMainCell" forIndexPath:indexPath];
    AlarmObject *currentAlarm = [items objectAtIndex:indexPath.row];
    
    if([currentAlarm.name isEqualToString:@"식사"]) {
        cell.iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_food.png"];
    } else if([currentAlarm.name isEqualToString:@"체중"]){
        cell.iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_weight.png"];
    } else if([currentAlarm.name isEqualToString:@"혈압"]){
        cell.iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_pre.png"];
    } else if([currentAlarm.name isEqualToString:@"혈당"]){
        cell.iconImgVw.image = [UIImage imageNamed:@"icon_gray_mini_sugar.png"];
    }
    
    [cell.titleLbl setNotoText:currentAlarm.name];
    [cell.TimeLbl setNotoText:currentAlarm.timeToSetOff];
    
    [cell.swichBtn setSelected:currentAlarm.enabled];
    cell.swichBtn.tag = indexPath.row;
    [cell.swichBtn addTarget:self action:@selector(alarmSwtichValueChage:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    HealthAlarmMainCell *commentCell = (HealthAlarmMainCell *)[tableView cellForRowAtIndexPath:indexPath];
    CGFloat height = commentCell.frame.size.height;
    selectNum = (int)indexPath.row;

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@""  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {

        NSLog(@"deleteAction index = %ld", indexPath.row);

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                 message:@"삭제하시겠습니까?"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            AlarmObject *currentAlarm = [self->items objectAtIndex:self->selectNum];
            
            UIApplication *app = [UIApplication sharedApplication];
            NSArray *eventArray = [app scheduledLocalNotifications];
            
            for (int i=0; i<[eventArray count]; i++) {
                UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
                if ([uid isEqualToString:[NSString stringWithFormat:@"%i", currentAlarm.notificationID]]) {
                    //Cancelling local notification
                    [app cancelLocalNotification:oneEvent];
                }
            }
            
            [self->items removeObjectAtIndex:self->selectNum];
            NSData *alarmListData2 = [NSKeyedArchiver archivedDataWithRootObject:self->items];
            [[NSUserDefaults standardUserDefaults] setObject:alarmListData2 forKey:ALARM_LIST_DATA];
        
            [self->_tableView reloadData];
        }];
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleDestructive handler:nil];

        [alertController addAction:cancelAlertAction];
        [alertController addAction:alertAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    deleteAction.backgroundColor = [UIColor redColor];

    UIImage *backgroundImage = [self cellImage:height imageName:@"icon_swipe_delete.png"];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];

    [commentCell.arrowImgVw setImage:[UIImage imageNamed:@"img_gray_right.png"]];

    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath{
    HealthAlarmMainCell *commentCell = (HealthAlarmMainCell *)[tableView cellForRowAtIndexPath:indexPath];
    [commentCell.arrowImgVw setImage:[UIImage imageNamed:@"img_gray_left.png"]];
}



- (UIImage*)cellImage:(CGFloat)height imageName:(NSString *)imgname{
    
    CGRect frame = CGRectMake(0, 0, 62, height);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(62, height), NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, RGB(142,144,157).CGColor);

    CGContextFillRect(context, frame);
    
    UIImage *image = [UIImage imageNamed:imgname];
    
    [image drawInRect:CGRectMake(frame.size.width/3.0, frame.size.height/6.0, frame.size.width/2, frame.size.height/6.0 * 4.5)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}





#pragma mark - viewInit
- (void)viewInit {
    _addBtn.titleLabel.font = FONT_NOTO_BOLD(18);
    [_addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addBtn setTitle:@"+ 알람 추가 하기" forState:UIControlStateNormal];
    [_addBtn setBackgroundColor:COLOR_NATIONS_BLUE];
    
}

@end

#pragma mark - Cell
@interface HealthAlarmMainCell ()

@end

@implementation HealthAlarmMainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _titleLbl.font = FONT_NOTO_MEDIUM(18);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    _titleLbl.text = @"";
    
    _TimeLbl.font = FONT_NOTO_MEDIUM(18);
    _TimeLbl.textColor = COLOR_MAIN_DARK;
    _TimeLbl.text = @"";
}

@end
