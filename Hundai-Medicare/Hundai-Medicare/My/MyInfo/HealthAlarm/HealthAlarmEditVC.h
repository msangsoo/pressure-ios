//
//  HealthAlarmEditVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "TimeUtils.h"
#import "AlarmObject.h"

@interface HealthAlarmEditVC : BaseViewController<UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *kindLbl;
@property (weak, nonatomic) IBOutlet UIImageView *iconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *kindTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UITextField *timeTf;

@property (weak, nonatomic) IBOutlet UIView *msgVw;
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;
@property (weak, nonatomic) IBOutlet UITextView *msgTv;


@property UIDatePicker *timePicker;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgVwHeightConst;

@end

