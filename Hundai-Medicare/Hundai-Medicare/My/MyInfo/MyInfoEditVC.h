//
//  MyInfoEditVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "MemberAuthAlertVC.h"
#import "MemberAuthTransAlertVC.h"
#import "TimeUtils.h"
#import "CommunityNetwork.h" //커뮤니티 통신모듈
#import "NickNameEditAlertVC.h"
#import "DateControlVC.h"
#import "AlertUtils.h"
#import "BasicInputVC.h"
#import "SignupStepTwoVC.h"
#import "LoginSignupNotAuthVC.h"

@interface MyInfoEditVC : BaseViewController<MemberAuthAlertVCDelegate, NickNameEditAlertVCDelegate,
                        ServerCommunicationDelegate, BasicInputVCDelegate, SignupStepTwoVCDelegate,
                        MemberAuthTransAlertVCDelegate, LoginSignupNotAuthVCDelegate>
{
    NSString *mber_no;
}
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameValLbl;
@property (weak, nonatomic) IBOutlet UILabel *sexLbl;
@property (weak, nonatomic) IBOutlet UILabel *sexValLbl;
@property (weak, nonatomic) IBOutlet UILabel *birthLbl;
@property (weak, nonatomic) IBOutlet UILabel *birthValLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneValLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightValLbl;
@property (weak, nonatomic) IBOutlet UILabel *weightLbl;
@property (weak, nonatomic) IBOutlet UILabel *weightValLbl;

@property (weak, nonatomic) IBOutlet UILabel *addInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *actqyLbl;
@property (weak, nonatomic) IBOutlet UILabel *actqyValLbl;
@property (weak, nonatomic) IBOutlet UILabel *jobLbl;
@property (weak, nonatomic) IBOutlet UILabel *jobValLbl;
@property (weak, nonatomic) IBOutlet UILabel *diseaseLbl;
@property (weak, nonatomic) IBOutlet UILabel *diseaseValLbl;
@property (weak, nonatomic) IBOutlet UILabel *smkingLbl;
@property (weak, nonatomic) IBOutlet UILabel *smkingValLbl;

@property (weak, nonatomic) IBOutlet UIButton *authBtn;

@property (weak, nonatomic) IBOutlet UILabel *regdateLbl;
@property (weak, nonatomic) IBOutlet UILabel *callinfoLbl;

@property (strong, nonatomic) IBOutlet UIView *nameView;
@property (strong, nonatomic) IBOutlet UIView *sexView;
@property (strong, nonatomic) IBOutlet UIView *birthView;
@property (strong, nonatomic) IBOutlet UIView *phoneView;

@property (strong, nonatomic) IBOutlet UIImageView *imageArrawName;
@property (strong, nonatomic) IBOutlet UIImageView *imageArrawSex;
@property (strong, nonatomic) IBOutlet UIImageView *imageArrawBirth;
@property (strong, nonatomic) IBOutlet UIImageView *imageArrawPhone;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constArrawName;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constArrawSex;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constArrawBirth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constArrawPhone;


- (IBAction)pressRow:(id)sender;

@property NSDictionary *myPageData;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentVwHeightConst;

@end
