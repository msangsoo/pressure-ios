//
//  MyInfoDefaulfCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoDefaulfCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UIImageView *rightImgVw;

@property (weak, nonatomic) IBOutlet UIImageView *iconNewImgVw;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;

@end
