//
//  MyInfoDefaulfCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyInfoDefaulfCell.h"

@implementation MyInfoDefaulfCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

#pragma mark - viewInit
- (void)viewInit {
    
    _titleLbl.font = FONT_NOTO_REGULAR(16);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    _titleLbl.text = @"";
    
    _infoLbl.font = FONT_NOTO_REGULAR(16);
    _infoLbl.textColor = COLOR_GRAY_SUIT;
    _infoLbl.text = @"";
    
    _versionLbl.font = FONT_NOTO_REGULAR(16);
    _versionLbl.textColor = COLOR_GRAY_SUIT;
    _versionLbl.text = @"";
    _versionLbl.hidden = true;
    
    _iconNewImgVw.hidden = true;
}

@end
