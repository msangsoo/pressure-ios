//
//  MyInfoCenterButtonCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyInfoCenterButtonCell.h"

@implementation MyInfoCenterButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

#pragma mark - viewInit
- (void)viewInit {
    _centerBtn.titleLabel.font = FONT_NOTO_BOLD(14);
    [_centerBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_centerBtn setBackgroundColor:[UIColor whiteColor]];
    _centerBtn.layer.borderWidth = 1;
    _centerBtn.layer.borderColor = RGB(132, 132, 132).CGColor;
    [_centerBtn makeToRadius:true];
}

@end
