//
//  MyInfoCenterButtonCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoCenterButtonCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *centerBtn;

@end
