//
//  MyInfoProFileCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoProFileCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImgVw;
@property (weak, nonatomic) IBOutlet UILabel *lvLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *myInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *regDateLbl;

@property (weak, nonatomic) IBOutlet UIButton *profileBtn;
@property (weak, nonatomic) IBOutlet UIButton *nickNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *userEditBtn;

@end
