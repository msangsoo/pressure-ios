//
//  MyInfoProFileCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyInfoProFileCell.h"

@implementation MyInfoProFileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

#pragma mark - viewInit
- (void)viewInit {
    _profileImgVw.layer.borderWidth = 1;
    _profileImgVw.layer.borderColor = RGB(223, 223, 223).CGColor;
    [_profileImgVw makeToRadius:true];
    
    _lvLbl.font = FONT_NOTO_REGULAR(14);
    _lvLbl.textColor = [UIColor whiteColor];
    _lvLbl.backgroundColor = COLOR_NATIONS_BLUE;
    _lvLbl.text = @"     ";
    
    _nameLbl.font = FONT_NOTO_MEDIUM(16);
    _nameLbl.textColor = COLOR_MAIN_DARK;
    _nameLbl.text = @"";
    
    _myInfoLbl.font = FONT_NOTO_REGULAR(16);
    _myInfoLbl.textColor = COLOR_MAIN_DARK;
    _myInfoLbl.text = @"";
    
    _regDateLbl.font = FONT_NOTO_LIGHT(14);
    _regDateLbl.textColor = COLOR_GRAY_SUIT;
    _regDateLbl.text = @"";
}

@end
