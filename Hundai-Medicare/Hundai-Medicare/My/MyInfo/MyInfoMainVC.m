//
//  MyInfoMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyInfoMainVC.h"

@interface MyInfoMainVC () {
    NSArray *infoArr;
    CGFloat nowApp;
    CGFloat app;
    NSString* disease_open;
    UIImage *selectedImage;
    NSString *tempNick;
    NSDictionary *TempMyPageData;
    
}

@end

@implementation MyInfoMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"프로필"];
    
    login = [_model getLoginData];
    
    infoArr = [[NSArray alloc] initWithObjects:
               @{@"title": @"포인트 (가용/누적)", @"img": @"icon_gray_point"},
               @{@"title": @"푸시 알람 설정", @"img": @"icon_gray_time"},
               @{@"title": @"건강기록 알람 설정", @"img": @"icon_gray_alaram"},
               @{@"title": @"비밀번호 변경", @"img": @"icon_gray_key"},
               @{@"title": @"개인정보정책", @"img": @"icon_gray_info"},
               @{@"title": @"버전정보", @"img": @"icon_gray_version"},
               nil];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self apiLoadMypage];
    
    // 일일미션 포인트내역으로 이동
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ShouldShowDayMisssion"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ShouldShowDayMisssion"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIViewController *vc = [MYPOINT_STORYBOARD instantiateInitialViewController];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
        return;
    }
}

#pragma mark - Actions
- (void)pressProfile {
    [LogDB insertDb:HM06 m_cod:HM06001 s_cod:HM06001001];
    
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *man = [UIAlertAction
                          actionWithTitle:@"Camera"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action) {
                              if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
                              }
                          }];
    UIAlertAction *wman = [UIAlertAction
                           actionWithTitle:@"Library"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                   [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                               }
                           }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 
                             }];
    [actionController addAction:man];
    [actionController addAction:wman];
    [actionController addAction:cancel];
    [self presentViewController:actionController animated:YES completion:nil];
}

- (void)pressNickname {
    [LogDB insertDb:HM06 m_cod:HM06002 s_cod:HM06002001];
    
    NickNameEditAlertVC *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"NickNameEditAlertVC"];
    vc.NickName = tempNick;
    vc.delegate = self;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (void)pressUserEdit {
    MyInfoEditVC *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"MyInfoEditVC"];
    vc.myPageData = TempMyPageData;
    vc.hidesBottomBarWhenPushed = true;
    
    [self.navigationController pushViewController:vc animated:true];
}

- (void)pressLogout {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"로그아웃 하시겠습니까?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"취소" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"로그아웃" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [Utils setUserDefault:AUTO_LOGIN value:@"N"];
        
        UIViewController *vc = [LOGIN_STORYBOARD instantiateInitialViewController];
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        UINavigationController *nc = (UINavigationController *)keyWindow.rootViewController;
        [nc setViewControllers:@[vc] animated:false];
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:doneAction];
    [self presentViewController:alertController animated:TRUE completion:nil];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 1) {
        return infoArr.count;
    }
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(_collectionView.frame.size.width, 120);
    }else if (indexPath.section == 1) {
        return CGSizeMake(_collectionView.frame.size.width, 50);
    }else if (indexPath.section == 2) {
        return CGSizeMake(_collectionView.frame.size.width, 50);
    }
    
    return CGSizeMake(_collectionView.frame.size.width, 30);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        return UIEdgeInsetsMake(0, 0, 5, 0);
    }else if (section == 1) {
        return UIEdgeInsetsMake(0, 0, 1, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MyInfoProFileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyInfoProFileCell" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        MyInfoProFileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyInfoProFileCell" forIndexPath:indexPath];
        
        [cell.profileBtn addTarget:self action:@selector(pressProfile) forControlEvents:UIControlEventTouchUpInside];
        [cell.nickNameBtn addTarget:self action:@selector(pressNickname) forControlEvents:UIControlEventTouchUpInside];
        [cell.userEditBtn addTarget:self action:@selector(pressUserEdit) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }else if (indexPath.section == 1) {
        MyInfoDefaulfCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyInfoDefaulfCell" forIndexPath:indexPath];
        
        [cell.titleLbl setNotoText:infoArr[indexPath.row][@"title"]];
        cell.iconImgVw.image = [UIImage imageNamed:infoArr[indexPath.row][@"img"]];
        
        if ([infoArr[indexPath.row][@"title"] isEqualToString:@"버전정보"]) {
            [cell.versionLbl setNotoText:APP_VERSION];
            
            cell.rightImgVw.hidden = true;
            cell.versionLbl.hidden = false;
            cell.iconNewImgVw.hidden = false;
        }else if ([infoArr[indexPath.row][@"title"] isEqualToString:@"포인트 (가용/누적)"]) {
            [cell.infoLbl setNotoText:@"0/0 Point"];
        }
        
        return cell;
    }else if (indexPath.section == 2) {
        MyInfoCenterButtonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyInfoCenterButtonCell" forIndexPath:indexPath];
        [cell.centerBtn addTarget:self action:@selector(pressLogout) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        
        if (indexPath.row == 0) { //포인트
            UIViewController *vc = [MYPOINT_STORYBOARD instantiateInitialViewController];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
        }else if (indexPath.row == 1) { //푸시 알람
            UIViewController *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"PushAlarmSettVC"];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
        }else if (indexPath.row == 2) { //건강 기록
            UIViewController *vc = [HEALTHALARM_STORYBOARD instantiateInitialViewController];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
        }else if (indexPath.row == 3) { //비밀번호 변경
            UIViewController *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"PassEditVC"];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
        }else if (indexPath.row == 4) { //개인정보 정책
            UIViewController *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"PrivacyPolicyVC"];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
        }else if (indexPath.row == 5) { //버전
            if (nowApp > app) {
                [Utils basicAlertView:self withTitle:@"알림" withMsg:@"새로운 버전이 존재합니다.\n업데이트 하시겠습니까?" completion:^{
                    NSURL *url = [NSURL URLWithString:APP_STORE_URL];
                    [UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];
                } fail:^{
                    
                }];
            }else {
                [Utils basicAlertView:self withTitle:@"" withMsg:@"최신 버전입니다."];
            }
        }
    }
}

#pragma mark - Touch moveFriend
- (void)moveFriend:(UITapGestureRecognizer *)recognizer{
    NSString *plusFriendId = @"_tyRjj";
    KPFPlusFriend *plusFriend = [[KPFPlusFriend alloc] initWithId:plusFriendId];
    [plusFriend addFriend];
}

#pragma mark - NickNameEditAlertVCDelegate
- (void)confirm:(NSString *)nickname {
    if ([nickname length]==0) {
        return;
    }
    [self requestSetNickname:nickname diseaseOpen:disease_open];
}

#pragma mark - viewInit
- (void)viewInit {
    _kakaoLbl.numberOfLines = 2;
    _kakaoLbl.font = FONT_NOTO_LIGHT(12);
    _kakaoLbl.textColor = RGB(62, 42, 46);
    [_kakaoLbl setNotoText:@"현대해상 메디케어서비스와 친구를 맺어보세요.\n유용한 건강정보와 다양한 이벤트가 함께합니다."];
    
    UITapGestureRecognizer *plusFriendMove =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(moveFriend:)];
    [_plusFriend addGestureRecognizer:plusFriendMove];
    
    UICollectionViewFlowLayout *layout =(UICollectionViewFlowLayout *) self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = 1;
    self.collectionView.collectionViewLayout = layout;
    
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
}



#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_load_mypage"]) {
        if ([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            
            //마이페이지 리턴값 저장
            TempMyPageData = result;
            
            //푸시 알람 설정 저장
            [Utils setUserDefault:ALRAM_SETTING value:[result objectForKey:@"NOTICE_YN"]];
            [Utils setUserDefault:NEW_HEALTH_INFO_ALRAM_SETTING value:[result objectForKey:@"HEALTH_YN"]];
            [Utils setUserDefault:LIKE_ALRAM_SETTING value:[result objectForKey:@"HEART_YN"]];
            [Utils setUserDefault:COMMENT_ALRAM_SETTING value:[result objectForKey:@"REPLY_YN"]];
            [Utils setUserDefault:DAILY_MISSION_ALRAM_SETTING value:[result objectForKey:@"DAILY_YN"]];
            
            tempNick = [result objectForKey:@"NICKNAME"];

            disease_open = [result objectForKey:@"DISEASE_OPEN"];
            
            MyInfoProFileCell *cell = (MyInfoProFileCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            // 닉넴없으면 이름
            if (tempNick!=nil && [tempNick length] >0) {
                [cell.nameLbl setNotoText:tempNick];
            }else {
                [cell.nameLbl setNotoText:[result objectForKey:@"MBER_NM"]];
            }
            
            if(![[result objectForKey:@"profile_pic"] isEqualToString:@""]){
                [cell.profileImgVw sd_setImageWithURL:[NSURL URLWithString:[result objectForKey:@"profile_pic"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    cell.profileImgVw.image = image;
                }];
            }
            
            if([[result objectForKey:@"mber_grad"] isEqualToString:@"10"]){
                double imageViewWidth = cell.profileImgVw.bounds.size.width;
                cell.profileImgVw.layer.cornerRadius = imageViewWidth / 2.0;
                cell.profileImgVw.layer.borderColor = RGB(243, 163, 41).CGColor;
                cell.profileImgVw.layer.borderWidth = 1.0;
            } else {
                double imageViewWidth = cell.profileImgVw.bounds.size.width;
                cell.profileImgVw.layer.cornerRadius = imageViewWidth / 2.0;
                cell.profileImgVw.layer.borderColor = RGB(223, 223, 223).CGColor;
                cell.profileImgVw.layer.borderWidth = 1.0;
            }
            
            
            
            int templevel = 1;
            if([[result objectForKey:@"POINT_TOTAL_AMT"]isEqualToString:@""]){
                templevel = 1;
            } else {
                templevel = [[result objectForKey:@"POINT_TOTAL_AMT"] intValue] / 1000;
                if( templevel < 1){
                    templevel = 1;
                }
            }
            [cell.lvLbl setNotoText:[NSString stringWithFormat:@"Lv.%d", templevel]];
            NSString* tempGender =@"";
            if([[result objectForKey:@"MBER_SEX"]isEqualToString:@"1"]){
                tempGender = @"남";
            }else {
                tempGender = @"여";
            }
            [cell.myInfoLbl setNotoText:[NSString stringWithFormat:@"%@ / %@ / 만%@ 세", [result objectForKey:@"MBER_NM"],tempGender,[result objectForKey:@"age"]]];
            [cell.regDateLbl setNotoText:[NSString stringWithFormat:@"앱 가입일 %@", [TimeUtils getDayKoreaformat:[result objectForKey:@"MBER_DE"]]]];
            
            for(int i=0; i<infoArr.count; i++){
                MyInfoDefaulfCell *cell1 = (MyInfoDefaulfCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                
                if ([infoArr[i][@"title"] isEqualToString:@"버전정보"]) {
                    NSString *nowAppVer = [result objectForKey:@"app_ver"];
                    nowApp = [nowAppVer floatValue];
                    app = [APP_VERSION floatValue];
                    if (nowApp > app) {
                        cell1.rightImgVw.hidden = true;
                        cell1.versionLbl.hidden = false;
                        cell1.iconNewImgVw.hidden = false;
                    }else {
                        cell1.rightImgVw.hidden = true;
                        cell1.versionLbl.hidden = false;
                        cell1.iconNewImgVw.hidden = true;
                    }
     
                    [cell1.versionLbl setNotoText:APP_VERSION];
                    
                
                }else if ([infoArr[i][@"title"] isEqualToString:@"포인트 (가용/누적)"]) {
                    NSString* point = @"";
                    NSString* point_sum = @"";
                    if([[result objectForKey:@"accml_sum_amt"]isEqualToString:@""]){
                        point = @"0";
                    } else {
                        point = [Utils decimalNumber:[[result objectForKey:@"accml_sum_amt"] intValue]];
                    }
                    
                    if([[result objectForKey:@"POINT_TOTAL_AMT"]isEqualToString:@""]){
                        point_sum = @"0";
                    } else {
                        point_sum = [Utils decimalNumber:[[result objectForKey:@"POINT_TOTAL_AMT"] intValue]];
                    }
                    [cell1.infoLbl setNotoText:[NSString stringWithFormat:@"%@ / %@ Point",point ,point_sum]];
                }
            }
        }
    }
    else if([result objectForKey:@"file_name"] && [result objectForKey:@"file_url"] ) {
//        NSString *file_name = [result objectForKey:@"file_name"];
        NSString *file_url  = [result objectForKey:@"file_url"];
        file_url = [file_url stringByReplacingOccurrencesOfString:@"profile/" withString:@"profile/tm_"];
        
        NSURL *url = [NSURL URLWithString:file_url];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:data];

        [self profileImageBind:img];
        
        [_model indicatorHidden];
    }
}

#pragma mark - api
- (void)apiLoadMypage {
//    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                @"mber_load_mypage", @"api_code",
//                                @"I", @"device_kind",
//                                nil];
//    
//    if(parameters != nil) {
//        if(server.delegate == nil) {
//            server.delegate = self;
//        }
//        [_model indicatorActive];
//        [server serverCommunicationData:parameters];
//    }else {
//        NSLog(@"server parameters error = %@", parameters);
//    }
}

- (void)requestSetNickname:(NSString *)nickName diseaseOpen:(NSString *)diseaseOpen {
    
    MyInfoProFileCell *cell = (MyInfoProFileCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [CommunityNetwork requestSetWithConfirmNickname:@{} seq:login.mber_sn nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        if (isSuccess == TRUE) {
            
            if (self->_model.getLoginData.nickname == nil || self->_model.getLoginData.nickname.length == 0) {
                [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
            }
            
            self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
            
            [cell.nameLbl setNotoText:nickName];
        } else {
//            NSError *error = (NSError *)responseObj;
//            [Utils basicAlertView:self withTitle:@"" withMsg:[NSString stringWithFormat:@"%@",error]];
//            NSLog(@"Error : %@", [error localizedDescription]);
            
        }
    }];
}


#pragma mark - UIImagePickerController delegate
- (void)showImagePickerControllerWithSourceType: (UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
//        [self profileImageBind:image];
        
        [self uploadProfile:image];
    }];
}

- (void)profileImageBind:(UIImage*)image {
    
    MyInfoProFileCell *cell = (MyInfoProFileCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if(image.size.width > cell.profileImgVw.frame.size.width){
        int reSize = 0;
        
        reSize = (int)image.size.width / cell.profileImgVw.frame.size.width;
        
        //해상도 줄이기
        UIImage *tempImage = nil;
        
        CGSize targetSize = CGSizeMake(image.size.width/(float)reSize, image.size.height/(float)reSize);
        
        UIGraphicsBeginImageContext(targetSize);
        
        CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
        thumbnailRect.origin = CGPointMake(0.0,0.0);
        thumbnailRect.size.width  = targetSize.width;
        thumbnailRect.size.height = targetSize.height;
        
        [image drawInRect:thumbnailRect];
        
        tempImage = UIGraphicsGetImageFromCurrentImageContext();
        
        [cell.profileImgVw setImage:tempImage];
        
        self->selectedImage = [tempImage copy];
        
    }else{
        [cell.profileImgVw setImage:image];
        
        self->selectedImage = [image copy];
        
    }
}

- (void)uploadProfile:(UIImage*)image {
    [server writeImage:image];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
