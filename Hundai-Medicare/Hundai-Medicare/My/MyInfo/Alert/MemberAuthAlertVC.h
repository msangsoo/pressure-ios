//
//  MemberAuthAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MemberAuthAlertVCDelegate
- (void)authDidTap;
- (void)callDidTap;
@end

@interface MemberAuthAlertVC : UIViewController

@property (nonatomic, retain) id<MemberAuthAlertVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;

@property (weak, nonatomic) IBOutlet UIButton *authBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;

@end
