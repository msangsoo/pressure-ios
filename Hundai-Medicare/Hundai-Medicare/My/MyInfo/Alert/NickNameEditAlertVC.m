//
//  NickNameEditAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "NickNameEditAlertVC.h"

@interface NickNameEditAlertVC ()

@end

@implementation NickNameEditAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    [self viewInit];
}

#pragma mark - Actions
- (IBAction)pressCancel:(UIButton *)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    [self dismissViewControllerAnimated:true completion:^{
        if (self.delegate)
            [self.delegate confirm:self.textFieldTf.text];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        return YES;
    }
    else if (textField.text.length >= 10) {
        return NO;
    }else {
        return YES;
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_MEDIUM(20);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"닉네임 변경"];
    
    _nickNameLbl.font = FONT_NOTO_REGULAR(16);
    _nickNameLbl.textColor = COLOR_GRAY_SUIT;
    [_nickNameLbl setNotoText:@"닉네임을 입력해주세요."];
    
    _textFieldVw.layer.borderWidth = 1;
    _textFieldVw.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    
    _textFieldTf.font = FONT_NOTO_BOLD(18);
    _textFieldTf.textColor = COLOR_NATIONS_BLUE;
    [_textFieldTf setPlaceholder:_NickName];
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

@end
