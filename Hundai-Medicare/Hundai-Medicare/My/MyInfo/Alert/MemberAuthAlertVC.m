//
//  MemberAuthAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MemberAuthAlertVC.h"

@interface MemberAuthAlertVC ()

@end

@implementation MemberAuthAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressCancel:(UIButton *)sender {
    [self dismiss:nil];
}

- (IBAction)pressAuth:(UIButton *)sender {
    [self dismiss:^{
        if (self.delegate) {
            [self.delegate authDidTap];
        }
    }];
}

- (IBAction)pressCall:(UIButton *)sender {
    [self dismiss:^{
        if (self.delegate) {
            [self.delegate callDidTap];
        }
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_MEDIUM(20);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"정회원 전환"];
    
    _word1Lbl.font = FONT_NOTO_REGULAR(16);
    _word1Lbl.textColor = COLOR_GRAY_SUIT;
    [_word1Lbl setNotoText:@"현대해상의 아래 보험에 가입 후 정회원으로\n전환하시면 건강상담, 지료 예약등의\n모든 서비스를 이용하실 수 있습니다."];
    
    _word2Lbl.font = FONT_NOTO_BOLD(16);
    _word2Lbl.textColor = COLOR_GRAY_SUIT;
    [_word2Lbl setNotoText:@"퍼펙트종합보험\n기세당당건강보험\n계속받는암보험\n암스트롱암보험\n간단하고편리한건강보험"];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(14);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"※ 보험상품 가입금액에 따라\n메디케어서비스 제공여부는 달라질 수 있습니다."];
    
    [_authBtn setTitle:@"정회원 전환" forState:UIControlStateNormal];
    [_authBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    [_callBtn setTitle:@"     보험 가입 상담" forState:UIControlStateNormal];
    [_callBtn setImage:[UIImage imageNamed:@"icon_blue_call"] forState:UIControlStateNormal];
    [_callBtn borderRadiusButton:COLOR_NATIONS_BLUE];
}

@end
