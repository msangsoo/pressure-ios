//
//  MemberAuthTransAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface MemberAuthTransAlertVC : BaseViewController
{
    NSString *selectedID;
    
}
@property (nonatomic, retain) id delegate;


@property (nonatomic, retain)  NSString *site_memid;
@property (nonatomic, retain)  NSString *app_memid;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;

@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end



@protocol MemberAuthTransAlertVCDelegate
- (void)doubleIDPopSelectID:(NSDictionary*)dic;
@end
