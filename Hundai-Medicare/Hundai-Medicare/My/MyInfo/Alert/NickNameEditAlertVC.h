//
//  NickNameEditAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NickNameEditAlertVCDelegate
- (void)confirm:(NSString *)nickname;
@end

@interface NickNameEditAlertVC : UIViewController

@property (nonatomic, retain) id<NickNameEditAlertVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl;
@property (weak, nonatomic) IBOutlet UIView *textFieldVw;
@property (weak, nonatomic) IBOutlet UITextField *textFieldTf;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property NSString *NickName;

@end
