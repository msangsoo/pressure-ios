//
//  MemberAuthTransAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MemberAuthTransAlertVC.h"

@interface MemberAuthTransAlertVC ()

@end

@implementation MemberAuthTransAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    selectedID = @"";
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressCancel:(UIButton *)sender {
    [self dismiss:nil];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    if ([selectedID length]==0) {
        return;
    }
    
    [self dismiss:^{
        if (self.delegate) {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [self->selectedID isEqualToString:self.site_memid]?@"Y":@"N", @"sel_site_yn",
                                 [self->selectedID isEqualToString:self.app_memid]?@"Y":@"N",@"app_site_yn",
                                 self->selectedID,@"sel_memid",
                                 nil];
            [self.delegate doubleIDPopSelectID:dic];
        }
    }];
    
}

- (IBAction)pressIDSelect:(UIButton *)sender {
    if (sender.tag == 1) {
        _firstBtn.selected = true;
        _secondBtn.selected = false;
        
        _firstBtn.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
        _firstBtn.layer.borderWidth = 1;
        
        _secondBtn.layer.borderColor = [UIColor clearColor].CGColor;
        _secondBtn.layer.borderWidth = 0;
        
        selectedID = self.site_memid;
    }else if (sender.tag == 2) {
        _firstBtn.selected = false;
        _secondBtn.selected = true;
        
        _secondBtn.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
        _secondBtn.layer.borderWidth = 1;
        
        _firstBtn.layer.borderColor = [UIColor clearColor].CGColor;
        _firstBtn.layer.borderWidth = 0;
        
        selectedID = self.app_memid;
    }
}


#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(18);
    _word1Lbl.textColor = COLOR_GRAY_SUIT;
    [_word1Lbl setNotoText:@"가입하신 계정이 여러 개 확인됩니다."];
    
    _word2Lbl.font = FONT_NOTO_REGULAR(18);
    _word2Lbl.textColor = COLOR_GRAY_SUIT;
    [_word2Lbl setNotoText:@"하나의 계정으로\n통합 후 정회원 전환이 가능합니다.\n이용하실 계정을 선택해주세요."];
    
    _firstBtn.tag = 1;
    [_firstBtn setTitle:self.site_memid forState:UIControlStateNormal];
    [_firstBtn setBackgroundColor:RGB(237, 237, 237)];
    [_firstBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_firstBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _secondBtn.tag = 2;
    [_secondBtn setTitle:self.app_memid forState:UIControlStateNormal];
    [_secondBtn setBackgroundColor:RGB(237, 237, 237)];
    [_secondBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_secondBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(16);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"(선택하신 계정으로 앱과 웹 사이트 모두\n사용가능하며 앱에서 입력하신 데이터는\n그대로 유지됩니다.)"];
    
    [_cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    
    
}

@end
