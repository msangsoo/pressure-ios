//
//  MyPointMallRegVC.m
//  Hundai-Medicare
//
//  Created by Daesun moon on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyPointMallRegVC.h"

@interface MyPointMallRegVC ()

@end

@implementation MyPointMallRegVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    server.delegate = self;
    [self viewInit];
}

- (void)viewInit {
    _btnAgreeInfo1.tag = 1;
    _btnAgreeInfo2.tag = 2;
    _btnAgreeInfo3.tag = 3;
   
    [_btnConfirm setTitle:@"확  인" forState:UIControlStateNormal];
    [_btnConfirm setTitle:@"확  인" forState:UIControlStateSelected];
    [_btnConfirm mediBaseButton];
}

#pragma mark - textfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self vaildateCheck];
    return YES;
}

#pragma mark - Actions
- (IBAction)pressAgree1:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.selected) {
       [self.imgCheck1 setImage:[UIImage imageNamed:@"commu_check_02"]];
    }else {
       [self.imgCheck1 setImage:[UIImage imageNamed:@"commu_check_01"]];
    }
    btn.selected = !btn.selected;
    [self vaildateCheck];
}

- (IBAction)pressAgree2:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.selected) {
        [self.imgCheck2 setImage:[UIImage imageNamed:@"commu_check_02"]];
    }else {
        [self.imgCheck2 setImage:[UIImage imageNamed:@"commu_check_01"]];
    }
    btn.selected = !btn.selected;
    [self vaildateCheck];
}

- (IBAction)pressAgree3:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.selected) {
        [self.imgCheck3 setImage:[UIImage imageNamed:@"commu_check_02"]];
    }else {
        [self.imgCheck3 setImage:[UIImage imageNamed:@"commu_check_01"]];
    }
    btn.selected = !btn.selected;
    
    [self vaildateCheck];
}
- (IBAction)pressAgreeAll:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.selected) {
        
        [self.imgCheckAll setImage:[UIImage imageNamed:@"commu_check_02"]];
        [self.imgCheck1 setImage:[UIImage imageNamed:@"commu_check_02"]];
        [self.imgCheck2 setImage:[UIImage imageNamed:@"commu_check_02"]];
        [self.imgCheck3 setImage:[UIImage imageNamed:@"commu_check_02"]];
        self.btnAgree1.selected = NO;
        self.btnAgree2.selected = NO;
        self.btnAgree3.selected = NO;
    }else {
        [self.imgCheckAll setImage:[UIImage imageNamed:@"commu_check_01"]];
        [self.imgCheck1 setImage:[UIImage imageNamed:@"commu_check_01"]];
        [self.imgCheck2 setImage:[UIImage imageNamed:@"commu_check_01"]];
        [self.imgCheck3 setImage:[UIImage imageNamed:@"commu_check_01"]];
        self.btnAgree1.selected = YES;
        self.btnAgree2.selected = YES;
        self.btnAgree3.selected = YES;
    }
    btn.selected = !btn.selected;
    
    [self vaildateCheck];
}

- (IBAction)pressAgreeInfo:(UIButton *)sender {
    NSString *titleStr = @"";
    NSString *urlStr = @"";
    
    if (sender.tag == 1) {
        titleStr = @"녹십자라이프케어몰 이용약관";
        urlStr = LIFECAREMOLL_TERMS_1_URL;
    }else if (sender.tag == 2) {
        titleStr = @"라이프케어몰 개인정보 제공 동의";
        urlStr = LIFECAREMOLL_TERMS_2_URL;
    }else if (sender.tag == 3) {
        titleStr = @"라이프케어몰 개인정보 취급 위탁 동의";
        urlStr = LIFECAREMOLL_TERMS_3_URL;
    }else {
        return;
    }
    
    CommonWebVC *vc = [COMMON_STORYBOARD instantiateViewControllerWithIdentifier:@"CommonWebVC"];
    vc.title = titleStr;
    vc.paramURL = urlStr;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressDoubleCheck:(id)sender {
    // 가입 전문에서 중복체크됨 (임시로 빼기로함)
}

- (IBAction)pressGo:(id)sender {
    NSString *message = @"";
    if ([self.txtID.text length]==0) {
        message = @"아이디를 입력해주세요.";
    }else if ([self.txtPass.text length]<8) {
        message = @"비밀번호는 영문, 숫자, 특수문자 포함 8자리 이상 입력해주세요.";
    }else if ([self.txtPass2.text length]<8) {
        message = @"비밀번호는 영문, 숫자, 특수문자 포함 8자리 이상 입력해주세요.";
    }else if (![Utils emailIsValid:self.txtEmail.text]) {
        message = @"이메일 형식이 아닙니다.";
    }
    if ([message length] > 0) {
        [Utils basicAlertView:self withTitle:@"" withMsg:message];
        return;
    }
    
    if(![self.txtPass.text isEqualToString:self.txtPass2.text]){
        [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호가 일치하지 않습니다."];
        return;
    }
    else if (![Utils NSStringIsValidPass:self.txtPass.text] || ![Utils NSStringIsValidPass:self.txtPass2.text]) {
        [Utils basicAlertView:self withTitle:@"" withMsg:@"영문, 숫자, 특수문자가 모두 포함된 8자리 이상으로 입력해주세요."];
        return;
    }
    else if (!self.btnAgree1.selected
             || !self.btnAgree2.selected
             || !self.btnAgree3.selected){
        [Utils basicAlertView:self withTitle:@"" withMsg:@"약관에 동의하여주세요."];
        return;
    }
    
    [self requestMallReg];
}

- (void)vaildateCheck {
    _btnConfirm.selected = false;
    _btnConfirm.userInteractionEnabled = false;
    
    if ([_txtID.text length] < 4) {
        return;
    }
    
    if ([_txtPass.text length] < 4) {
        return;
    }
    
    if ([_txtPass2.text length] < 4) {
        return;
    }
    
    if ([_txtEmail.text length] < 4) {
        return;
    }
    
    if (!_btnAgree1.selected
        || !_btnAgree2.selected
        || !_btnAgree3.selected
        ) {
        return;
    }
    
    _btnConfirm.userInteractionEnabled = true;
    _btnConfirm.selected = true;
}


#pragma mark -requestData
- (void)requestMallReg{
    
    [_model indicatorActive];
    
    Tr_login *login = [_model getLoginData];
    NSString *gender = [login.mber_sex isEqualToString:@"1"]?@"M":@"W";
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"gclife_join", @"api_code",
                                @"Y", @"agree",
                                self.txtID.text, @"id",
                                self.txtPass.text, @"pwd",
                                self.txtPass.text, @"pwdconf",
                                login.mber_nm, @"nm",
                                self.txtEmail.text, @"email",
                                gender, @"gender",
                                login.mber_sn, @"mber_sn",
                                @"", @"mber_no",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"gclife_join"]){
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]){
           
            Tr_login *login = [_model getLoginData];
            login.gclife_id = [result objectForKey:@"gclife_id"];
            
            NSString *msg = @"라이프케어몰 회원가입 및\n아이디 연동이 완료 되었습니다.";
            [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:msg left:@"" right:@"확인"];
            
        }else if([[result objectForKey:@"reg_yn"] isEqualToString:@"N"]){
            
            NSString *msg = [result objectForKey:@"err_msg"];;
            [AlertUtils BasicAlertShow:nil tag:99 title:@"" msg:msg left:@"" right:@"확인"];
        }
    }
}

- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag==100) {
        //정상가입
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (alertView.tag==101) {
        
    }
}


@end
