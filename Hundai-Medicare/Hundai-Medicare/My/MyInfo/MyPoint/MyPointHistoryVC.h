//
//  MyPointHistoryVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "TimeUtils.h"

@interface MyPointHistoryVC : BaseViewController<UITableViewDataSource, UITableViewDelegate, ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UIView *useVw;
@property (weak, nonatomic) IBOutlet UIView *totalVw;

@property (weak, nonatomic) IBOutlet UILabel *usePointLbl;
@property (weak, nonatomic) IBOutlet UILabel *point1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPointLbl;
@property (weak, nonatomic) IBOutlet UILabel *point2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *usePointValLbl;
@property (weak, nonatomic) IBOutlet UILabel *usePoint1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPointValLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPoint2Lbl;

@property (weak, nonatomic) IBOutlet UILabel *hitsLbl;

@property (weak, nonatomic) IBOutlet UIButton *oneMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *threeMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *sixMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(void)initRequest;

@end

#pragma mark - Cell
@interface MyPointHistoryCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *usePointLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *etcLbl;

@end
