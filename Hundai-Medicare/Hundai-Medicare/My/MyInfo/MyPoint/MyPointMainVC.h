//
//  MyPointMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyPointHistoryVC.h"
#import "MyPointUseVC.h"

@interface MyPointMainVC : UIViewController {
    MyPointHistoryVC *vcHistory;
    MyPointUseVC *vcUse;
}

@property (weak, nonatomic) IBOutlet UIButton *historyBtn;
@property (weak, nonatomic) IBOutlet UIButton *useBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@end
