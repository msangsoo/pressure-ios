//
//  MyPointUseVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "MallLoginVC.h"
#import "MyPointMallRegVC.h"

@interface MyPointUseVC : BaseViewController<ServerCommunicationDelegate, MallLoginVCDelegate>
{
}
@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;

@property (weak, nonatomic) IBOutlet UILabel *guideLbl;
@property (weak, nonatomic) IBOutlet UILabel *guide1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *guide2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *guide3Lbl;

@property (weak, nonatomic) IBOutlet UILabel *pointLbl;
@property (weak, nonatomic) IBOutlet UILabel *usePointLbl;
@property (weak, nonatomic) IBOutlet UITextField *pointTf;
@property (weak, nonatomic) IBOutlet UIButton *pointBtn;
@property (weak, nonatomic) IBOutlet UIButton *linkBtn;
@property (nonatomic, retain)  NSString *point_usr_amt;
- (IBAction)pressPointTrans:(id)sender;
- (void)setUsePointText;
- (IBAction)pressLifeCareGo:(id)sender;




@end
