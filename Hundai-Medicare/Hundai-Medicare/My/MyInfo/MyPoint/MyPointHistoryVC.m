//
//  MyPointHistoryVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyPointHistoryVC.h"

@interface MyPointHistoryVC () {
    NSMutableArray *arrData;
    int pageNumber;
    BOOL isRequest;
    BOOL isEnd;
    NSString *start_de;
}

@end

@implementation MyPointHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    server.delegate = self;
    arrData = [[NSMutableArray alloc] init];
    pageNumber = 1;
    isRequest = NO;
    isEnd = NO;
    
    start_de = [TimeUtils getDateAddDay:-(30*1)];
    
    [self viewInit];
    [self requestData];
}

-(void)initRequest {
    [arrData removeAllObjects];
    pageNumber = 1;
    
    [self requestData];
}

#pragma mark -requestData
- (void)requestData{
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_point_use_list", @"api_code",
                                [NSString stringWithFormat:@"%d", pageNumber], @"pageNumber",
                                start_de, @"start_de",
                                [TimeUtils getNowDateFormat:@"yyyyMMdd"], @"end_de",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_point_use_list"]){
        
        [self.usePointValLbl setNotoText:[Utils decimalNumber:[[result objectForKey:@"point_usr_amt"] intValue]]];
        [self.totalPointValLbl setNotoText:[Utils decimalNumber:[[result objectForKey:@"point_user_sum_amt"] intValue]]];
        
        //가용포인트를 다른곳에서 사용하기 위해서 저장
        [Utils setUserDefault:@"POINT_USR_AMT" value:[result objectForKey:@"point_usr_amt"]];
        
        if([[result objectForKey:@"point_day_list"] count] ==0){
            isEnd = YES;
        }else{
            
            [arrData addObjectsFromArray:[result objectForKey:@"point_day_list"]];
          
            isRequest = NO;
        }
        [_tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource


#pragma mark -UITableViewDelegate-

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 90;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    
    static NSString *cellIdentifier = @"MyPointHistoryCell";
    
    MyPointHistoryCell *cell = (MyPointHistoryCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"MyPointHistoryCell" bundle:nil] forCellReuseIdentifier:@"MyPointHistoryCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    NSDictionary *dic = [arrData objectAtIndex:row];
    [cell.titleLbl setNotoText:[dic objectForKey:@"point_txt"]];
    [cell.dateLbl setNotoText:[TimeUtils getDayKoreaTimeformat:[dic objectForKey:@"input_de"]]];
    
    NSString *use =[NSString stringWithFormat:@"%@ Point", [Utils decimalNumber:[[dic objectForKey:@"accml_amt"] intValue]]];
    [cell.usePointLbl setNotoText:use];
    
    NSString *etc =[NSString stringWithFormat:@"잔여 %@ Point", [dic objectForKey:@"remain_point_amt"]];
    [cell.etcLbl setNotoText:etc];
    
    if (row >= [arrData count]-1 && !isRequest && !isEnd) {
        isRequest = YES;
        pageNumber ++;
        [self requestData];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}




#pragma mark - UITableViewDelegate

#pragma mark - Actions
- (IBAction)pressTabBar:(UIButton *)sender {
    
    isEnd = NO;
    pageNumber = 1;
    
    [arrData removeAllObjects];
    [_tableView reloadData];
    
    
    if (sender.tag == 1 && !_oneMonthBtn.isSelected) {
        _oneMonthBtn.selected = true;
        _threeMonthBtn.selected = false;
        _sixMonthBtn.selected = false;
        _allBtn.selected = false;
       
        start_de = [TimeUtils getDateAddDay:-(30*1)];
    }else if (sender.tag == 2 && !_threeMonthBtn.isSelected) {
        _oneMonthBtn.selected = false;
        _threeMonthBtn.selected = true;
        _sixMonthBtn.selected = false;
        _allBtn.selected = false;
        start_de = [TimeUtils getDateAddDay:-(30*3)];
        
    }else if (sender.tag == 3 && !_sixMonthBtn.isSelected) {
        _oneMonthBtn.selected = false;
        _threeMonthBtn.selected = false;
        _sixMonthBtn.selected = true;
        _allBtn.selected = false;
        start_de = [TimeUtils getDateAddDay:-(30*6)];
        
    }else if (sender.tag == 4 && !_allBtn.isSelected) {
        _oneMonthBtn.selected = false;
        _threeMonthBtn.selected = false;
        _sixMonthBtn.selected = false;
        _allBtn.selected = true;
        start_de = [TimeUtils getDateAddDay:-(365*3)];
        
    }
    
    [self requestData];
}

#pragma mark - viewInit
- (void)viewInit {
    _useVw.layer.borderWidth = 1;
    _useVw.layer.borderColor = RGB(237, 237, 237).CGColor;
    
    _totalVw.layer.borderWidth = 1;
    _totalVw.layer.borderColor = RGB(237, 237, 237).CGColor;
    
    _usePointLbl.font = FONT_NOTO_BOLD(18);
    _usePointLbl.textColor = COLOR_MAIN_DARK;
    [_usePointLbl setNotoText:@"가용"];
    
    _point1Lbl.font = FONT_NOTO_LIGHT(18);
    _point1Lbl.textColor = COLOR_MAIN_DARK;
    [_point1Lbl setNotoText:@"포인트"];
    
    _totalPointLbl.font = FONT_NOTO_BOLD(18);
    _totalPointLbl.textColor = COLOR_MAIN_DARK;
    [_totalPointLbl setNotoText:@"누적"];
    
    _point2Lbl.font = FONT_NOTO_LIGHT(18);
    _point2Lbl.textColor = COLOR_MAIN_DARK;
    [_point2Lbl setNotoText:@"포인트"];
    
    _usePointValLbl.font = FONT_NOTO_MEDIUM(29);
    _usePointValLbl.textColor = COLOR_MAIN_DARK;
    _usePointValLbl.text = @"";
    
    _usePoint1Lbl.font = FONT_NOTO_LIGHT(20);
    _usePoint1Lbl.textColor = COLOR_MAIN_DARK;
    [_usePoint1Lbl setNotoText:@"Point"];
    
    _totalPointValLbl.font = FONT_NOTO_LIGHT(30);
    _totalPointValLbl.textColor = COLOR_MAIN_DARK;
    _totalPointValLbl.text = @"";
    
    _totalPoint2Lbl.font = FONT_NOTO_LIGHT(20);
    _totalPoint2Lbl.textColor = COLOR_MAIN_DARK;
    [_totalPoint2Lbl setNotoText:@"Point"];
    
    _hitsLbl.font = FONT_NOTO_MEDIUM(18);
    _hitsLbl.textColor = COLOR_GRAY_SUIT;
    [_hitsLbl setNotoText:@"조회기간"];
    
    _oneMonthBtn.tag = 1;
    _oneMonthBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    [_oneMonthBtn setTitle:@"1개월" forState:UIControlStateNormal];
    [_oneMonthBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_oneMonthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_oneMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(233, 233, 233) withFrame:_oneMonthBtn.bounds] forState:UIControlStateNormal];
    [_oneMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:_oneMonthBtn.bounds] forState:UIControlStateSelected];
    
    _threeMonthBtn.tag = 2;
    _threeMonthBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    [_threeMonthBtn setTitle:@"3개월" forState:UIControlStateNormal];
    [_threeMonthBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_threeMonthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_threeMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(233, 233, 233) withFrame:_threeMonthBtn.bounds] forState:UIControlStateNormal];
    [_threeMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:_threeMonthBtn.bounds] forState:UIControlStateSelected];
    
    _sixMonthBtn.tag = 3;
    _sixMonthBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    [_sixMonthBtn setTitle:@"6개월" forState:UIControlStateNormal];
    [_sixMonthBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_sixMonthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_sixMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(233, 233, 233) withFrame:_sixMonthBtn.bounds] forState:UIControlStateNormal];
    [_sixMonthBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:_sixMonthBtn.bounds] forState:UIControlStateSelected];
    
    _allBtn.tag = 4;
    _allBtn.titleLabel.font = FONT_NOTO_MEDIUM(18);
    [_allBtn setTitle:@"모두" forState:UIControlStateNormal];
    [_allBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_allBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_allBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(233, 233, 233) withFrame:_allBtn.bounds] forState:UIControlStateNormal];
    [_allBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_DARK_BLUE withFrame:_allBtn.bounds] forState:UIControlStateSelected];
    
    _oneMonthBtn.selected = true;
    
    _tableView.rowHeight = 80;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

@end

#pragma mark - Cell
@interface MyPointHistoryCell()

@end

@implementation MyPointHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

- (void)viewInit {
    _titleLbl.font = FONT_NOTO_REGULAR(16);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    _titleLbl.text = @"";
    
    _usePointLbl.font = FONT_NOTO_REGULAR(16);
    _usePointLbl.textColor = COLOR_MAIN_ORANGE;
    _usePointLbl.text = @"";
    
    _dateLbl.font = FONT_NOTO_REGULAR(16);
    _dateLbl.textColor = COLOR_GRAY_SUIT;
    _dateLbl.text = @"";
    
    _etcLbl.font = FONT_NOTO_REGULAR(16);
    _etcLbl.textColor = COLOR_MAIN_DARK;
    _etcLbl.text = @"";
}

@end
