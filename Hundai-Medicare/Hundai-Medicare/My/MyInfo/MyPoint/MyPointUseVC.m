//
//  MyPointUseVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyPointUseVC.h"

@interface MyPointUseVC ()

@end

@implementation MyPointUseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    server.delegate = self;
    [self viewInit];
    
    // TODO: 테스트입니다.
    Tr_login *login = [_model getLoginData];
//    login.gclife_id = @"";
    NSLog(@"viewDidLoad login:%@", login.gclife_id);
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
#pragma mark - viewInit
- (void)viewInit {
    _word1Lbl.font = FONT_NOTO_REGULAR(16);
    _word1Lbl.textColor = COLOR_GRAY_SUIT;
    [_word1Lbl setNotoText:@"획득한 포인트는 녹십자 라이프케어몰에서 사용하실 수 있으며 1Point당 1원으로 계산됩니다."];
    
    _word2Lbl.font = FONT_NOTO_BOLD(14);
    _word2Lbl.textColor = COLOR_MAIN_DARK;
    [_word2Lbl setNotoText:@"※ 포인트 유효기간 안내"];
    
    _word3Lbl.font = FONT_NOTO_REGULAR(14);
    _word3Lbl.textColor = COLOR_GRAY_SUIT;
    [_word3Lbl setNotoText:@"-포인트의 유효기간은 획득일로 부터 1년입니다."];
    
    _guideLbl.font = FONT_NOTO_MEDIUM(18);
    _guideLbl.textColor = COLOR_NATIONS_BLUE;
    [_guideLbl setNotoText:@"녹십자 라이프케어몰 포인트 사용 방법"];
    
    _guide1Lbl.font = FONT_NOTO_REGULAR(16);
    _guide1Lbl.textColor = COLOR_GRAY_SUIT;
    [_guide1Lbl setNotoText:@"• 라이프케어몰 포인트로 전환 후"];
    
    _guide2Lbl.font = FONT_NOTO_REGULAR(16);
    _guide2Lbl.textColor = COLOR_GRAY_SUIT;
    [_guide2Lbl setNotoText:@"• 라이프케어몰에서 주문 결제 시"];
    
    _guide3Lbl.font = FONT_NOTO_REGULAR(16);
    _guide3Lbl.textColor = COLOR_GRAY_SUIT;
    [_guide3Lbl setNotoText:@"• '적립금 사용'체크 후 결제하세요."];
    
    _pointLbl.font = FONT_NOTO_REGULAR(18);
    _pointLbl.textColor = COLOR_GRAY_SUIT;
    [_pointLbl setNotoText:@"라이프케어몰 포인트로 전환"];
    
    
    
//    [self setUsePointText];
    
    _pointTf.font = FONT_NOTO_REGULAR(18);
    _pointTf.textColor = COLOR_GRAY_SUIT;
    [_pointTf setPlaceholder:@"금액입력"];
    
    [_pointBtn setTitle:@"전환하기" forState:UIControlStateNormal];
    [_pointBtn mediBaseButton];
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:@" 라이프케어몰"];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @" 라이프케어몰".length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(18)
                     range:NSMakeRange(0, @" 라이프케어몰".length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor whiteColor]
                     range:NSMakeRange(0, @" 라이프케어몰".length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:@"바로가기  "];
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, @"바로가기  ".length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_BOLD(18)
                      range:NSMakeRange(0, @"바로가기  ".length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor blackColor]
                      range:NSMakeRange(0, @"바로가기  ".length)];
    [attstr appendAttributedString:secondStr];
    
    [_linkBtn makeToRadius:true];
    [_linkBtn setAttributedTitle:attstr forState:UIControlStateNormal];
}

#pragma mark - noto text method
- (void)setUsePointText {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:@"전환가능 포인트  "];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @"전환가능 포인트  ".length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(18)
                     range:NSMakeRange(0, @"전환가능 포인트  ".length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, @"전환가능 포인트  ".length)];
    [attstr appendAttributedString:firstStr];
    
    NSString *point = [NSString stringWithFormat:@"%@", [Utils getNumberComma:self.point_usr_amt]];
    NSMutableAttributedString *usePointStr = [[NSMutableAttributedString alloc] initWithString:point];
    [usePointStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, point.length)];
    [usePointStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_BOLD(18)
                      range:NSMakeRange(0, point.length)];
    [usePointStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:NSMakeRange(0, point.length)];
    [attstr appendAttributedString:usePointStr];
    
    NSMutableAttributedString *pointStr = [[NSMutableAttributedString alloc] initWithString:@" Point"];
    [pointStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, @" Point".length)];
    [pointStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_REGULAR(18)
                      range:NSMakeRange(0, @" Point".length)];
    [pointStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor blackColor]
                      range:NSMakeRange(0, @" Point".length)];
    [attstr appendAttributedString:pointStr];
    
    self.usePointLbl.attributedText = attstr;
}

- (IBAction)pressLifeCareGo:(id)sender {
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"http://m.gclifecare.com/"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url:%@", URL);
        }
    }];
    
}

- (IBAction)pressPointTrans:(id)sender {
    
    [self.pointTf resignFirstResponder];
    
    if ([self.pointTf.text length]==0
        || [self.pointTf.text intValue]==0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"전환 할 포인트를 입력해 주세요." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
        return;
    }
    
    int point = [self.pointTf.text intValue];
    int amt_point = [self.point_usr_amt intValue];
    
    if (point > amt_point) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"가용포인트 보다 많은 포인트를 전환할 수 업습니다." delegate:self cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
        return;
    }
    
    
    Tr_login *login = [_model getLoginData];
    NSLog(@"login:%@", login.gclife_id);
    
    if ([login.gclife_id length] > 0) {
        [LogDB insertDb:HM06 m_cod:HM06009 s_cod:HM06009001];
        
        NSString *msg = @"라이프케어몰 포인트로 전환합니다.\n전환 후에는 라이프케어몰에서 사용하실 수 있습니다.\n전환하시겠습니까?";
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:msg left:@"취소" right:@"확인"];
    }else {
        [LogDB insertDb:HM06 m_cod:HM06010 s_cod:HM06010001];
        
        NSString *msg = @"라이프케어몰 가입 후 전환이 가능합니다.\n이미 가입하셨다면 아이디와 비밀번호를 입력해주시기 바랍니다.";
        [AlertUtils BasicAlertShow:self tag:101 title:@"" msg:msg left:@"가입하기" right:@"아이디연동"];
    }

}

#pragma mark -requestData
- (void)requestPointTrans{
    
    [_model indicatorActive];
    
    Tr_login *login = [_model getLoginData];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_point_move_yn", @"api_code",
                                self.pointTf.text, @"user_point_amt",
                                login.gclife_id, @"mber_id",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_point_move_yn"]) {
         if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
             
             self.point_usr_amt = [NSString stringWithFormat:@"%d", [self.point_usr_amt intValue] - [self.pointTf.text intValue]];
             [self setUsePointText];
             
             self.pointTf.text = @"";
             [self.pointTf resignFirstResponder];

             
             NSString *msg = @"성공적으로 전환되었습니다.\n전환된 포인트는 라이프케어몰에 로그인 하시면 확인하실 수 있습니다.";
             [AlertUtils BasicAlertShow:nil tag:99 title:@"" msg:msg left:@"확인" right:@""];
             
         }else if([[result objectForKey:@"reg_yn"] isEqualToString:@"N"]){
             
             NSString *msg = @"계정정보가 확인되지 않습니다.\n계정정보를 잊으신 경우 라이프케어몰에서 아이디/비밀번호 찾기를 진행해 주시기 바랍니다. ";
             [AlertUtils BasicAlertShow:self tag:109 title:@"" msg:msg left:@"" right:@"확인"];
             
         }
    }
}

- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag==109) {
        
        NSString *msg = @"라이프케어몰 가입 후 전환이 가능합니다.\n이미 가입하셨다면 아이디와 비밀번호를 입력해주시기 바랍니다.";
        [AlertUtils BasicAlertShow:self tag:101 title:@"" msg:msg left:@"가입하기" right:@"아이디연동"];
    }
    else if (alertView.tag==100) {
        if (tag==1) {
            //취소
        }else {
            [self requestPointTrans];
        }
    }
    else if (alertView.tag==101) {
        if (tag==1) {
            //가입하기
            MyPointMallRegVC *vc = [MYPOINT_STORYBOARD instantiateViewControllerWithIdentifier:@"MyPointMallRegVC"];
            vc.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:vc animated:true];
            
        }else {
            //아이디연동
            MallLoginVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"MallLoginVC"];
            vc.delegate = self;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
            [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        }
    }
}

- (void)MallLoginAlert:(MallLoginVC *)alertView returnData:(NSDictionary*)dic {
    NSString *data_yn =[dic objectForKey:@"data_yn"];
    
    if ([data_yn isEqualToString:@"Y"]) {
        
        NSString *gclife_id =[dic objectForKey:@"gclife_id"];
        
        // 아이디 매핑
        Tr_login *login = [_model getLoginData];
        login.gclife_id = gclife_id;
        
        self.pointTf.text = @"";
        
        NSString *msg = @"아이디가 연동 되었습니다.";
        [AlertUtils BasicAlertShow:nil tag:99 title:@"" msg:msg left:@"확인" right:@""];
        
    }else if ([data_yn isEqualToString:@"N"]) {
        
        NSString *msg = @"계정정보가 확인되지 않습니다.\n계정정보를 잊으신 경우 라이프케어몰에서 아이디/비밀번호 찾기를 진행해 주시기 바랍니다.";
        [AlertUtils BasicAlertShow:nil tag:99 title:@"" msg:msg left:@"" right:@"확인"];
    }
}
@end
