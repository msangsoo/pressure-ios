//
//  MyPointMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyPointMainVC.h"

@interface MyPointMainVC ()

@end

@implementation MyPointMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM01002 s_cod:HM01002001];
    
    [self.navigationController setNavigationBarHidden:false];
    
    [self.navigationItem setTitle:@"포인트"];
    
    [self viewInit];
    
    vcHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPointHistoryVC"];
    [self addChildViewController:vcHistory];
    vcHistory.view.bounds = _vwMain.bounds;
    vcHistory.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHistory.view];
    [vcHistory.view setHidden:false];
    
    vcUse = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPointUseVC"];
    [self addChildViewController:vcUse];
    vcUse.view.bounds = _vwMain.bounds;
    vcUse.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcUse.view];
    [vcUse.view setHidden:true];
    
    [self.historyBtn setSelected:true];
    [self.useBtn setSelected:false];
    
    [LogDB insertDb:HM06 m_cod:HM06007 s_cod:HM06007001];
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    if(sender.tag == 1 && !self.historyBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06007 s_cod:HM06007001];
        
        [self.historyBtn setSelected:true];
        [self.useBtn setSelected:false];
        
        [vcHistory.view setHidden:false];
        [vcUse.view setHidden:true];
        [vcHistory initRequest];
        
    }else if(sender.tag == 2 && !self.useBtn.isSelected) {
        [LogDB insertDb:HM06 m_cod:HM06008 s_cod:HM06008001];
        
        [self.historyBtn setSelected:false];
        [self.useBtn setSelected:true];
        
        [vcHistory.view setHidden:true];
        [vcUse.view setHidden:false];
        //        [vcFH viewInit];
        
        NSString* point_usr_amt = [Utils getUserDefault:@"POINT_USR_AMT"];
        vcUse.point_usr_amt = point_usr_amt;
        [vcUse setUsePointText];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.historyBtn.tag = 1;
    [self.historyBtn setTitle:@"포인트 내역" forState:UIControlStateNormal];
    [self.historyBtn mediHealthSecondTabButton];
    
    self.useBtn.tag = 2;
    [self.useBtn setTitle:@"사용 방법" forState:UIControlStateNormal];
    [self.useBtn mediHealthSecondTabButton];
}

@end
