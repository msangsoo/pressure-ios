//
//  MyPointMallRegVC.h
//  Hundai-Medicare
//
//  Created by Daesun moon on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CommonWebVC.h"

@interface MyPointMallRegVC : BaseViewController<ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtID;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
@property (weak, nonatomic) IBOutlet UITextField *txtPass2;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheckAll;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck3;

@property (weak, nonatomic) IBOutlet UIButton *btnAgree1;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree3;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;

@property (weak, nonatomic) IBOutlet UIButton *btnAgreeInfo1;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreeInfo2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreeInfo3;

- (IBAction)pressAgree1:(id)sender;
- (IBAction)pressAgree2:(id)sender;
- (IBAction)pressAgree3:(id)sender;
- (IBAction)pressDoubleCheck:(id)sender;
- (IBAction)pressGo:(id)sender;
- (IBAction)pressAgreeAll:(id)sender;

@end

