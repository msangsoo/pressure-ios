//
//  PushAlarmSettVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface PushAlarmSettVC : BaseViewController<ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *notiLbl;
@property (weak, nonatomic) IBOutlet UILabel *healthInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *likeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentLbl;
@property (weak, nonatomic) IBOutlet UILabel *missionLbl;

@property (weak, nonatomic) IBOutlet UIButton *notiBtn;
@property (weak, nonatomic) IBOutlet UIButton *healthInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UIButton *missionBtn;

@end
