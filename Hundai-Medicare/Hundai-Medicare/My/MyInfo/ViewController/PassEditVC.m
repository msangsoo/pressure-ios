//
//  PassEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PassEditVC.h"

@interface PassEditVC ()

@property NSArray <UITextField *> *fields;

@end

@implementation PassEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06014 s_cod:HM06014001];
    
    [self.navigationItem setTitle:@"비밀번호 변경"];
    
    [self viewInit];
    
    [self prepareFields];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Actions
- (IBAction)pressCancel:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    if (_confirmBtn.tag != 1) {
        return;
    }
    NSString *currPass = [Utils getUserDefault:USER_PASS];
    
    if(![_passTf.text isEqualToString:currPass]){
        [Utils basicAlertView:self withTitle:@"" withMsg:@"현재 비밀번호를 확인바랍니다."];
        return;
    }
    
    if(![_rePassTf.text isEqualToString:_rePassConfirmTf.text]){
        [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호가 일치하지 않습니다."];
        return;
    }
    
    if([_passTf.text isEqualToString:_rePassConfirmTf.text]){
        [Utils basicAlertView:self withTitle:@"" withMsg:@"현재 비밀번호와 같을 수 없습니다."];
        return;
    }
    
    if (![Utils NSStringIsValidPass:_rePassTf.text] || ![Utils NSStringIsValidPass:_rePassConfirmTf.text]) {
        [Utils basicAlertView:self withTitle:@"" withMsg:@"영문, 숫자, 특수문자가 모두 포함된 8자리 이상으로 입력해주세요."];
        return;
    }
    
    [self apiEditPassword];
 
    
}

#pragma mark - viewInit
- (void)viewInit {
    
    _emailLbl.font = FONT_NOTO_REGULAR(18);
    _emailLbl.textColor = COLOR_MAIN_DARK;
    [_emailLbl setNotoText:@"아이디"];
    
    _emailValueLbl.font = FONT_NOTO_REGULAR(17);
    _emailValueLbl.textColor = COLOR_GRAY_SUIT;
    [_emailValueLbl setNotoText:[Utils getUserDefault:USER_ID]];
    
    _passLbl.font = FONT_NOTO_REGULAR(18);
    _passLbl.textColor = COLOR_MAIN_DARK;
    [_passLbl setNotoText:@"현재 비밀번호"];
    
    _rePassLbl.font = FONT_NOTO_REGULAR(18);
    _rePassLbl.textColor = COLOR_MAIN_DARK;
    [_rePassLbl setNotoText:@"새 비밀번호"];
    
    _rePassConfirmLbl.font = FONT_NOTO_REGULAR(18);
    _rePassConfirmLbl.textColor = COLOR_MAIN_DARK;
    [_rePassConfirmLbl setNotoText:@"확인"];
    
    _passTf.layer.borderWidth = 1;
    _passTf.layer.borderColor = RGB(218, 218, 218).CGColor;
    _passTf.font = FONT_NOTO_REGULAR(14);
    _passTf.textColor = RGB(218, 218, 218);
    [_passTf setPlaceholder:@"현재 비밀번호"];
    _passTf.secureTextEntry = YES;
    
    
    _rePassTf.layer.borderWidth = 1;
    _rePassTf.layer.borderColor = RGB(218, 218, 218).CGColor;
    _rePassTf.font = FONT_NOTO_REGULAR(14);
    _rePassTf.textColor = RGB(218, 218, 218);
    [_rePassTf setPlaceholder:@"새 비밀번호"];
    _rePassTf.secureTextEntry = YES;
    
    _rePassConfirmTf.layer.borderWidth = 1;
    _rePassConfirmTf.layer.borderColor = RGB(218, 218, 218).CGColor;
    _rePassConfirmTf.font = FONT_NOTO_REGULAR(14);
    _rePassConfirmTf.textColor = RGB(218, 218, 218);
    [_rePassConfirmTf setPlaceholder:@"새 비밀번호 다시 입력"];
    _rePassConfirmTf.secureTextEntry = YES;
    
    [_cencelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cencelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
   
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 선택한 값을 UITextField의 text값에 반영
    
    [self nextButtonCheck];
}

- (BOOL)simpleFieldCheck {
    if([_passTf.text isEqualToString:@""]) {
        return NO;
    }
    if([_rePassTf.text isEqualToString:@""] ) {
        
        return NO;
    }
    if([_rePassConfirmTf.text isEqualToString:@""] ) {
        
        return NO;
    }
    return YES;
}

#pragma mark - Vaildate
- (void)nextButtonCheck {
    if ([self simpleFieldCheck]) {
        _confirmBtn.tag = 1;
        
    }else {
        _confirmBtn.tag = 0;
        
    }
}


- (void)prepareFields {
    _fields = @[_passTf, _rePassTf, _rePassConfirmTf];
    
    for(UITextField *field in self.fields) {
        field.delegate = self;
    }
    _passTf.keyboardType = UIKeyboardTypeDefault;
    _rePassTf.keyboardType = UIKeyboardTypeDefault;
    _rePassConfirmTf.keyboardType = UIKeyboardTypeDefault;
}



#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_pwd_edit_exe"]) {
        if ([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            
            [Utils setUserDefault:USER_PASS value:_rePassConfirmTf.text];
            
            _passTf.text = @"";
            _rePassTf.text = @"";
            _rePassConfirmTf.text = @"";
            
            [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호 변경이 완료되었습니다."];
        } else {
            _passTf.text = @"";
            [Utils basicAlertView:self withTitle:@"" withMsg:@"비밀번호 변경에 실패하였습니다."];
        }
    }
}





#pragma mark - api
- (void)apiEditPassword {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_pwd_edit_exe", @"api_code",
                                [Utils getUserDefault:USER_ID], @"mber_id",
                                _passTf.text, @"bef_mber_pwd",
                                _rePassConfirmTf.text, @"aft_mber_pwd",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

@end
