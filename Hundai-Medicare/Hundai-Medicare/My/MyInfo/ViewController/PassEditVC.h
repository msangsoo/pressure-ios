//
//  PassEditVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface PassEditVC : BaseViewController<UITextFieldDelegate, ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailValueLbl;

@property (weak, nonatomic) IBOutlet UILabel *passLbl;
@property (weak, nonatomic) IBOutlet UILabel *rePassLbl;
@property (weak, nonatomic) IBOutlet UILabel *rePassConfirmLbl;

@property (weak, nonatomic) IBOutlet UITextField *passTf;
@property (weak, nonatomic) IBOutlet UITextField *rePassTf;
@property (weak, nonatomic) IBOutlet UITextField *rePassConfirmTf;

@property (weak, nonatomic) IBOutlet UIButton *cencelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end
