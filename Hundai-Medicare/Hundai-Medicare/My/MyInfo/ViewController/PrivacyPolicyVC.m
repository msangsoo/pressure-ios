//
//  PrivacyPolicyVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PrivacyPolicyVC.h"

@interface PrivacyPolicyVC () {
    Tr_login *login;
}

@end

@implementation PrivacyPolicyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06015 s_cod:HM06015001];
    
    [self.navigationItem setTitle:@"개인정보정책"];
    
    login = [_model getLoginData];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Actions
-(IBAction)pressMemberOut:(id)sender {
    
    [AlertUtils BasicAlertShow:self tag:9999 title:@"" msg:@"서비스 탈퇴 시 계정정보를 포함한\n모든 데이터가 삭제됩니다.\n정말로 탈퇴하시겠습니까?\n\n(정회원의 경우 앱 및 웹사이트의 모든 정보가 삭제됩니다.)" left:@"취소" right:@"확인"];
}

- (IBAction)pressTab:(UIButton *)sender {
    
    NSString *titleStr = @"";
    NSString *urlStr = @"";
    if (sender.tag == 1) {
        titleStr = @"개인정보 제공 동의";
        urlStr = PERSONAL_TERMS_1_URL;
        
    }else if (sender.tag == 2) {
        titleStr = @"개인민감정보 제공 동의";
        urlStr = PERSONAL_TERMS_2_URL;
        
    }else if (sender.tag == 3) {
        titleStr = @"개인정보 제3자 제공 동의";
        urlStr = PERSONAL_TERMS_4_URL;
        
    }else if (sender.tag == 4) {
        titleStr = @"마케팅 활동 동의(선택)";
        urlStr = PERSONAL_ASSO_TERMS_5_URL;
    }
    
    CommonWebVC *vc = [COMMON_STORYBOARD instantiateViewControllerWithIdentifier:@"CommonWebVC"];
    vc.title = titleStr;
    vc.paramURL = urlStr;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - api
- (void)apiMemberOut {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_drop_info", @"api_code",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_drop_info"]) {
        if ([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            
            [AlertUtils BasicAlertShow:self tag:9997 title:@"" msg:@"서비스 탈퇴가 완료 되었습니다.\n재이용을 원하실 경우 다시 가입하시기 바랍니다." left:@"확인" right:@""];
            
        } else {
            [Utils basicAlertView:self withTitle:@"" withMsg:@"정상처리 되지 않았습니다."];
        }
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    
    if(alertView.view.tag == 9999) {
        if(tag == 2) {
            [self apiMemberOut];
        }
    }else if(alertView.view.tag == 9997) {
        // 로그아웃처리
        [Utils setUserDefault:AUTO_LOGIN value:@"N"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [NSUserDefaults resetStandardUserDefaults];
        [prefs synchronize];
        
        UIViewController *vc = [LOGIN_STORYBOARD instantiateInitialViewController];
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        UINavigationController *nc = (UINavigationController *)keyWindow.rootViewController;
        [nc setViewControllers:@[vc] animated:false];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _useInfoLbl.font = FONT_NOTO_REGULAR(18);
    _useInfoLbl.textColor = COLOR_MAIN_DARK;
    [_useInfoLbl setNotoText:@"개인정보 제공 동의"];
    
    _useInfoDetailLbl.font = FONT_NOTO_REGULAR(18);
    _useInfoDetailLbl.textColor = COLOR_MAIN_DARK;
    [_useInfoDetailLbl setNotoText:@"개인민감정보 제공 동의"];
    
    _useInfoOtherLbl.font = FONT_NOTO_REGULAR(18);
    _useInfoOtherLbl.textColor = COLOR_MAIN_DARK;
    [_useInfoOtherLbl setNotoText:@"개인정보 제3자 제공 동의"];
    
    _marketInfoLbl.font = FONT_NOTO_REGULAR(18);
    _marketInfoLbl.textColor = COLOR_MAIN_DARK;
    [_marketInfoLbl setNotoText:@"마케팅 활동 동의(선택)"];
    
    _useInfoBtn.tag = 1;
    _useInfoDetailBtn.tag = 2;
    _useInfoOtherBtn.tag = 3;
    _marketInfoBtn.tag = 4;
    
    /*
    if ([login.mber_grad isEqualToString:@"10"]) {
        self.contentVwHeightConst.constant = 152;
        self.marketInfoVw.hidden = true;
    }else {
        self.contentVwHeightConst.constant = 203;
        self.marketInfoVw.hidden = false;
    }*/
    
    // 회원, 준회원 모두 마케팅동의는 안보이도록 처리.
    self.contentVwHeightConst.constant = 152;
    self.marketInfoVw.hidden = true;
    
    
    _mberExitBtn.titleLabel.font = FONT_NOTO_BOLD(15);
    [_mberExitBtn setTitle:@"서비스 탈퇴" forState:UIControlStateNormal];
    [_mberExitBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
}


@end
