//
//  PushAlarmSettVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PushAlarmSettVC.h"

@interface PushAlarmSettVC() {
    NSString* NOTICE_YN;
    NSString* HEALTH_YN;
    NSString* HEART_YN;
    NSString* REPLY_YN;
    NSString* DAILY_YN;
    
}

@end

@implementation PushAlarmSettVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06011 s_cod:HM06011001];
    
    [self.navigationItem setTitle:@"푸시 알람 설정"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Actions
- (IBAction)pressSW:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            if(!_notiBtn.isSelected){
                [_notiBtn setSelected:YES];
                NOTICE_YN = @"Y";
            } else {
                [_notiBtn setSelected:NO];
                NOTICE_YN = @"N";
            }
            
            break;
        case 2:
            if(!_healthInfoBtn.isSelected) {
                [_healthInfoBtn setSelected:YES];
                HEALTH_YN = @"Y";
            } else {
                [_healthInfoBtn setSelected:NO];
                HEALTH_YN = @"N";
            }
            break;
        case 3:
            if(!_likeBtn.isSelected) {
                [_likeBtn setSelected:YES];
                HEART_YN = @"Y";
            } else {
                [_likeBtn setSelected:NO];
                HEART_YN = @"N";
            }
            break;
        case 4:
            if(!_commentBtn.isSelected) {
                [_commentBtn setSelected:YES];
                REPLY_YN = @"Y";
            } else {
                [_commentBtn setSelected:NO];
                REPLY_YN = @"N";
            }
            break;
        case 5:
            if(!_missionBtn.isSelected) {
                [_missionBtn setSelected:YES];
                DAILY_YN = @"Y";
            } else {
                [_missionBtn setSelected:NO];
                DAILY_YN = @"N";
            }
            break;
    }
    [self apiPushSet];
    
}

#pragma mark - viewInit
- (void)viewInit {
    _notiLbl.font = FONT_NOTO_REGULAR(18);
    _notiLbl.textColor = COLOR_MAIN_DARK;
    [_notiLbl setNotoText:@"공지사항"];
    
    _healthInfoLbl.font = FONT_NOTO_REGULAR(18);
    _healthInfoLbl.textColor = COLOR_MAIN_DARK;
    [_healthInfoLbl setNotoText:@"새 건강정보"];
    
    _likeLbl.font = FONT_NOTO_REGULAR(18);
    _likeLbl.textColor = COLOR_MAIN_DARK;
    [_likeLbl setNotoText:@"게시글 좋아요"];
    
    _commentLbl.font = FONT_NOTO_REGULAR(18);
    _commentLbl.textColor = COLOR_MAIN_DARK;
    [_commentLbl setNotoText:@"게시글 댓글"];
    
    _missionLbl.font = FONT_NOTO_REGULAR(18);
    _missionLbl.textColor = COLOR_MAIN_DARK;
    [_missionLbl setNotoText:@"일일 미션 달성"];
    
    [self initAlramSetting];
    
    
    
    
}

- (void)initAlramSetting{
    NOTICE_YN = [Utils getUserDefault:ALRAM_SETTING];
    HEALTH_YN = [Utils getUserDefault:NEW_HEALTH_INFO_ALRAM_SETTING];
    HEART_YN =  [Utils getUserDefault:LIKE_ALRAM_SETTING];
    REPLY_YN =  [Utils getUserDefault:COMMENT_ALRAM_SETTING];
    DAILY_YN =  [Utils getUserDefault:DAILY_MISSION_ALRAM_SETTING];
    
    _notiBtn.tag = 1;
    if([NOTICE_YN isEqualToString:@"Y"])
        _notiBtn.selected = true;
    else
        _notiBtn.selected = false;
    
    _healthInfoBtn.tag = 2;
    if([HEALTH_YN isEqualToString:@"Y"])
        _healthInfoBtn.selected = true;
    else
        _healthInfoBtn.selected = false;
    
    _likeBtn.tag = 3;
    if([HEART_YN isEqualToString:@"Y"])
        _likeBtn.selected = true;
    else
        _likeBtn.selected = false;
    
    _commentBtn.tag = 4;
    if([REPLY_YN isEqualToString:@"Y"])
        _commentBtn.selected = true;
    else
        _commentBtn.selected = false;
    
    _missionBtn.tag = 5;
    if([DAILY_YN isEqualToString:@"Y"])
        _missionBtn.selected = true;
    else
        _missionBtn.selected = false;
}



#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
    
}
-(void)delegatePush:(NSInteger)status {
    
}

-(void)delegateError {
    
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_push_set"]) {
        if ([[result objectForKey:@"result_code"] isEqualToString:@"0000"]) {
            [Utils setUserDefault:ALRAM_SETTING value:NOTICE_YN];
            [Utils setUserDefault:NEW_HEALTH_INFO_ALRAM_SETTING value:HEALTH_YN];
            [Utils setUserDefault:LIKE_ALRAM_SETTING value:HEART_YN] ;
            [Utils setUserDefault:COMMENT_ALRAM_SETTING value:REPLY_YN];
            [Utils setUserDefault:DAILY_MISSION_ALRAM_SETTING value:DAILY_YN];
        } else {
            [self initAlramSetting];
        }
    }
}

#pragma mark - api
- (void)apiPushSet {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_push_set", @"api_code",
                                NOTICE_YN, @"notice_yn",
                                HEALTH_YN, @"health_yn",
                                HEART_YN, @"heart_yn",
                                REPLY_YN, @"reply_yn",
                                DAILY_YN, @"daily_yn",
                                @"N", @"cm_yn",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

@end
