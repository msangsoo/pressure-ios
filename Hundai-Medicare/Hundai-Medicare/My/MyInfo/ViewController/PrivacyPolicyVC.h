//
//  PrivacyPolicyVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 20/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "CommonWebVC.h"

@interface PrivacyPolicyVC : BaseViewController<ServerCommunicationDelegate, BasicAlertVCDelegate>


@property (weak, nonatomic) IBOutlet UIView *marketInfoVw;

@property (weak, nonatomic) IBOutlet UILabel *useInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *useInfoDetailLbl;
@property (weak, nonatomic) IBOutlet UILabel *useInfoOtherLbl;
@property (weak, nonatomic) IBOutlet UILabel *marketInfoLbl;

@property (weak, nonatomic) IBOutlet UIButton *useInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *useInfoDetailBtn;
@property (weak, nonatomic) IBOutlet UIButton *useInfoOtherBtn;
@property (weak, nonatomic) IBOutlet UIButton *marketInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *mberExitBtn;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentVwHeightConst;

-(IBAction)pressMemberOut:(id)sender;
@end

