//
//  MyInfoMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 15/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "MyInfoProFileCell.h"
#import "MyInfoDefaulfCell.h"
#import "MyInfoCenterButtonCell.h"

#import "NickNameEditAlertVC.h"
#import "TimeUtils.h"
#import <KakaoPlusFriend/KakaoPlusFriend.h> //카카오플러스 친구
#import "CommunityNetwork.h" //커뮤니티 통신모듈
#import <SDWebImage/UIImageView+WebCache.h>
#import "MyInfoEditVC.h"

@interface MyInfoMainVC : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource,
                UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
                NickNameEditAlertVCDelegate, ServerCommunicationDelegate, UIActionSheetDelegate>
{
    
    Tr_login *login;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *kakaoLbl;
@property (strong, nonatomic) IBOutlet UIView *plusFriend;

@end
