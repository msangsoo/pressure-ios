//
//  MyInfoEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MyInfoEditVC.h"


@interface MyInfoEditVC ()

@end

@implementation MyInfoEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM06 m_cod:HM06003 s_cod:HM06003001];
    
    server.delegate = self;
    
    [self.navigationItem setTitle:@"회원 정보 변경"];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    
    if (IS_IPHONEX) {
        self.contentVwHeightConst.constant = UIScreen.mainScreen.bounds.size.height +50;
    }else {
        
        self.contentVwHeightConst.constant = 850;
    }
}

#pragma mark - 닉네임 디리게이션
- (void)confirm:(NSString *)nickname {
    
    if (nickname== nil || [nickname length]==0) {
        return;
    }
    Tr_login *login = [_model getLoginData];
    
    NSString *disease_open = [self.myPageData objectForKey:@"DISEASE_OPEN"];
    
    [CommunityNetwork requestSetWithConfirmNickname:@{} seq:login.mber_sn nickName:nickname diseaseOpen:disease_open response:^(id responseObj, BOOL isSuccess) {
        [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        if (isSuccess == TRUE) {
            
            login.mber_nm = nickname;
            
            [self.nameValLbl setNotoText:nickname];
        } else {
//            NSError *error = (NSError *)responseObj;
//            [Utils basicAlertView:self withTitle:@"오류" withMsg:[NSString stringWithFormat:@"%@",error]];
//            NSLog(@"Error : %@", [error localizedDescription]);
        }
    }];
}

#pragma mark - 생녕월일 디리게이션
- (void)lifeDateView:(DateControlVC *)alertView selectedDate:(NSString *)date {
    if(alertView.tag==1001){
        
        
        self.birthValLbl.text = [date stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        
        NSString *strDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strDate, @"mber_lifyea", nil];
        
        [self myinfoEditRequest:dic];
    }
}

- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    if(alertView.tag==1001){
        
        
        self.birthValLbl.text = [date stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        
        NSString *strDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strDate, @"mber_lifyea", nil];
        
        [self myinfoEditRequest:dic];
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}

#pragma mark - 키, 몸무게, 휴대폰번호 디리게이션
- (void)BasicInput:(BasicInputVC *)alertView returnData:(NSString*)value {
    if(alertView.tag ==1006){
        if ([value length]==0) {
            return;
        }
        //이름
        self.weightValLbl.text = value;
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value, @"mber_name", nil];
        [self myinfoEditRequest:dic];
    }
    else if(alertView.tag ==1004){
        if ([value length]==0) {
            return;
        }
        //키
        self.heightValLbl.text =[NSString stringWithFormat:@"%@cm", value];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value, @"mber_height", nil];
        [self myinfoEditRequest:dic];
    }else if(alertView.tag ==1005){
        if ([value length]==0) {
            return;
        }
        //몸무게
        self.weightValLbl.text = [NSString stringWithFormat:@"%@kg", value];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value, @"mber_bdwgh", nil];
        [self myinfoEditRequest:dic];
    }else if(alertView.tag ==1007){
        if ([value length]==0) {
            return;
        }
        //휴대폰번호
        self.phoneValLbl.text = [NSString stringWithFormat:@"%@", value];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value, @"mber_hp", nil];
        [self myinfoEditRequest:dic];
    }
}
#pragma mark - Actions
- (IBAction)pressRow:(id)sender; {
    
    Tr_login *login = [_model getLoginData];
    
    long tag = [sender tag];
    if (tag == 100) {
        if([login.mber_grad isEqualToString:@"20"]) {
            
            [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
            
            //이름수정
            BasicInputVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"BasicInputVC"];
            vc.delegate = self;
            vc.itemsTitle = @"이름을 입력해주세요.";
            vc.itemsValue = _nameValLbl.text;
            vc.tag = 1006;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
            [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        }
    }else if (tag == 101) {
        if([login.mber_grad isEqualToString:@"20"]) {
            
            [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
            
            // 성별수정
            [self sexActionSheet];
        }
    }else if (tag == 102) {
        if([login.mber_grad isEqualToString:@"20"]) {
            
            [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
            
            // 생년월일수정
            [AlertUtils DateControlShow:self dateType:Date tag:1001 selectDate:[Utils getNowDate:@"yyyy-MM-dd"]];
        }
    }else if (tag == 103) {
        
        [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
        
        // 키수정
        BasicInputVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"BasicInputVC"];
        vc.delegate = self;
        vc.itemsTitle = @"키";
        vc.itemsValue = login.mber_height;
        vc.tag = 1004;
        vc.keyboardType = UIKeyboardTypeDecimalPad;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        
    }else if (tag == 104) {
        
        [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
        
        // 몸무게 수정
        BasicInputVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"BasicInputVC"];
        vc.delegate = self;
        vc.itemsTitle = @"몸무게";
        vc.itemsValue = login.mber_bdwgh;
        vc.tag = 1005;
        vc.keyboardType = UIKeyboardTypeDecimalPad;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        
    }else if (tag == 105) {
        
        if([login.mber_grad isEqualToString:@"20"]) {
            [LogDB insertDb:HM06 m_cod:HM06004 s_cod:HM06004001];
            
            // 몸무게 수정
            BasicInputVC *vc = [ALERTS_STORYBOARD instantiateViewControllerWithIdentifier:@"BasicInputVC"];
            vc.delegate = self;
            vc.isTxtFieldsDelegate = YES;
            vc.itemsTitle = @"휴대폰번호";
            vc.itemsValue = @"숫자만 입력하세요.";
            vc.tag = 1007;
            vc.keyboardType = UIKeyboardTypeNumberPad;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
            [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        }
    }else if (tag == 110) {
        
        [LogDB insertDb:HM06 m_cod:HM06005 s_cod:HM06005001];
        
        // 추가정보 페이지 이동
        SignupStepTwoVC *vc = [SIGNUPSTEP_STORYBOARD instantiateViewControllerWithIdentifier:@"SignupStepTwoVC"];
        vc.strTitle = @"추가 정보 수정";
        vc.delegate = self;
        vc.strActqy = login.mber_actqy;
        vc.strJobYN = [_myPageData objectForKey:@"job_yn"];
        vc.strDiseaseNm = login.disease_nm;
        vc.strDiseaseTxt = login.disease_txt;
        vc.strSmoke = login.smkng_yn;
        [self.navigationController pushViewController:vc animated:true];
        
    }
}

- (IBAction)pressAuth:(UIButton *)sender {
    [LogDB insertDb:HM06 m_cod:HM06006 s_cod:HM06006001];
    
    MemberAuthAlertVC *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"MemberAuthAlertVC"];
    vc.delegate = self;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

#pragma mark - 아이디여러개일때 콜백
- (void)doubleIDPop:(NSDictionary*)dic{
    
    mber_no = [dic objectForKey:@"mber_no"];
    
    // 여러개 아이디일때
    MemberAuthTransAlertVC *vc = [MYINFO_STORYBOARD instantiateViewControllerWithIdentifier:@"MemberAuthTransAlertVC"];
    vc.delegate = self;
    vc.site_memid = [dic objectForKey:@"site_memid"];
    vc.app_memid  = [dic objectForKey:@"app_memid"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

// Y 전환완료
- (void)memberTransComplete:(NSDictionary*)dic {
    [self apiLogin];
}

#pragma mark - 아이디여러개 선택 콜백
- (void)doubleIDPopSelectID:(NSDictionary*)dic {
    
    Tr_login *login = [_model getLoginData];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"asstb_mber_keep_member_id_change", @"api_code",
                                mber_no, @"mber_no",
                                login.mber_nm, @"memname",
                                [dic objectForKey:@"sel_site_yn"], @"sel_site_yn",
                                [dic objectForKey:@"app_site_yn"], @"app_site_yn",
                                [dic objectForKey:@"sel_memid"], @"sel_memid",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - MemberAuthAlertVCDelegate
- (void)authDidTap {
    LoginSignupNotAuthVC *vc = [LOGIN_STORYBOARD instantiateViewControllerWithIdentifier:@"LoginSignupNotAuthVC"];
    vc.delegate = self;
    vc.mber_gred = @"20";
    vc.strTitle = @"정회원 인증";
    vc.strTitleMsg = @"보험 가입 시 제공하셨던 정보를\n입력해주세요.";
    [self.navigationController pushViewController:vc animated:true];
}

- (void)callDidTap {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:15885656"]];
}


#pragma mark - viewInit
- (void)viewInit {
    
    Tr_login *login = [_model getLoginData];
    
    NSArray *labelArr = @[_nameLbl, _sexLbl, _birthLbl, _phoneLbl, _heightLbl, _weightLbl, _actqyLbl, _jobLbl, _diseaseLbl, _smkingLbl];
    NSArray *labelText = @[@"이름", @"성별", @"생년월일", @"휴대폰번호", @"키", @"체중", @"활동량", @"직업", @"보유질환", @"흡연여부"];
    
    NSArray *valLabelArr = @[_nameValLbl, _sexValLbl, _birthValLbl, _phoneValLbl, _heightValLbl, _weightValLbl, _actqyValLbl, _jobValLbl, _diseaseValLbl, _smkingValLbl];
    
    _infoLbl.font = FONT_NOTO_MEDIUM(18);
    _infoLbl.textColor = COLOR_NATIONS_BLUE;
    [_infoLbl setNotoText:@"기본정보"];
    
    _addInfoLbl.font = FONT_NOTO_MEDIUM(18);
    _addInfoLbl.textColor = COLOR_NATIONS_BLUE;
    [_addInfoLbl setNotoText:@"추가정보"];
    
    int count = 0;
    for (UILabel *lbl in labelArr) {
        lbl.font = FONT_NOTO_REGULAR(16);
        lbl.textColor = COLOR_MAIN_DARK;
        if([login.mber_grad isEqualToString:@"10"]){
            if(lbl == _nameLbl)
                lbl.textColor = COLOR_GRAY_SUIT;
            if(lbl == _sexLbl)
                lbl.textColor = COLOR_GRAY_SUIT;
            if(lbl == _birthLbl)
                lbl.textColor = COLOR_GRAY_SUIT;
            if(lbl == _phoneLbl)
                lbl.textColor = COLOR_GRAY_SUIT;
        }
        [lbl setNotoText:labelText[count]];
        count ++;
    }
    
    for (UILabel *lbl in valLabelArr) {
        lbl.font = FONT_NOTO_REGULAR(16);
        lbl.textColor = COLOR_GRAY_SUIT;
        if(lbl == _nameValLbl){
            lbl.text = login.mber_nm;
        }else if(lbl == _sexValLbl){
            lbl.text = [login.mber_sex isEqualToString:@"1"]?@"남성":@"여성";
        }else if(lbl == _birthValLbl){
            lbl.text = [TimeUtils getDayformat:login.mber_lifyea format:@"yyyy.MM.dd"];
        }else if(lbl == _phoneValLbl){
            lbl.text = [NSString stringWithFormat:@"%@", login.mber_hp];
        }else if(lbl == _heightValLbl){
            lbl.text = [NSString stringWithFormat:@"%@cm", login.mber_height];
        }else if(lbl == _weightValLbl){
            lbl.text = [NSString stringWithFormat:@"%@kg", login.mber_bdwgh];
            
        }else if(lbl == _actqyValLbl){
            
            if ([login.mber_actqy isEqualToString:@"1"]) {
                [lbl setNotoText:@"주로 앉아 있음"];
            }else if ([login.mber_actqy isEqualToString:@"2"]) {
                [lbl setNotoText:@"약간 활동적"];
            }else{
                [lbl setNotoText:@"활동적"];
            }
        }else if(lbl == _jobValLbl) {
            lbl.text = [[_myPageData objectForKey:@"job_yn"] isEqualToString:@"Y"]?@"있음":@"없음";
        }else if(lbl == _diseaseValLbl){
            
            if ([login.disease_nm isEqualToString:@"1"]) {
                [lbl setNotoText:@"고혈압"];
            }else if ([login.disease_nm isEqualToString:@"2"]) {
                [lbl setNotoText:@"당뇨"];
            }else if ([login.disease_nm isEqualToString:@"3"]) {
                [lbl setNotoText:@"고지혈증"];
            }else if ([login.disease_nm isEqualToString:@"4"]) {
                [lbl setNotoText:@"비만"];
            }else if ([login.disease_nm isEqualToString:@"5"]) {
                [lbl setNotoText:@"없음"];
            }else  {
                [lbl setNotoText:login.disease_txt];
            }
            
        }else if(lbl == _smkingValLbl){
            lbl.text = [login.smkng_yn isEqualToString:@"Y"]?@"흡연":@"비흡연";
            
        }
    }
    
    if([login.mber_grad isEqualToString:@"10"]){
        _nameView.backgroundColor = RGB(246, 246, 246);
        _sexView.backgroundColor = RGB(246, 246, 246);
        _birthView.backgroundColor = RGB(246, 246, 246);
        _phoneView.backgroundColor = RGB(246, 246, 246);
        
        _imageArrawName.hidden = YES;
        _imageArrawSex.hidden = YES;
        _imageArrawBirth.hidden = YES;
        _imageArrawPhone.hidden = YES;
        
        _constArrawName.constant = -8.0f;
        _constArrawSex.constant = -8.0f;
        _constArrawBirth.constant = -8.0f;
        _constArrawPhone.constant = -8.0f;
    } else {
        _nameView.backgroundColor = RGB(255, 255, 255);
        _sexView.backgroundColor = RGB(255, 255, 255);
        _birthView.backgroundColor = RGB(255, 255, 255);
        _phoneView.backgroundColor = RGB(255, 255, 255);
        
        _imageArrawName.hidden = NO;
        _imageArrawSex.hidden = NO;
        _imageArrawBirth.hidden = NO;
        _imageArrawPhone.hidden = NO;
        
        _constArrawName.constant = 15.0f;
        _constArrawSex.constant = 15.0f;
        _constArrawBirth.constant = 15.0f;
        _constArrawPhone.constant = 15.0f;
    }
    
    
    
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:@"   정회원"];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @"   정회원".length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(18)
                     range:NSMakeRange(0, @"   정회원".length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor whiteColor]
                     range:NSMakeRange(0, @"   정회원".length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:@"전환   "];
    [secondStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, @"전환   ".length)];
    [secondStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(18)
                     range:NSMakeRange(0, @"전환   ".length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, @"전환   ".length)];
    [attstr appendAttributedString:secondStr];
    
    [_authBtn makeToRadius:true];
    [_authBtn setAttributedTitle:attstr forState:UIControlStateNormal];
    
    // 정회원 변환버튼 숨김
    if ([login.mber_grad isEqualToString:@"10"]) {
        _authBtn.hidden = YES;
    }
    
    _callinfoLbl.font = FONT_NOTO_LIGHT(16);
    _regdateLbl.textColor = COLOR_MAIN_DARK;
    [_callinfoLbl setNotoText:[NSString stringWithFormat:@"앱 가입일 %@", [TimeUtils getDayKoreaformat:[_myPageData objectForKey:@"MBER_DE"]]]];
    
    if ([login.mber_grad isEqualToString:@"10"]) {
        [_regdateLbl setHidden:NO];
        _regdateLbl.font = FONT_NOTO_THIN(16);
        _regdateLbl.textColor = COLOR_MAIN_DARK;
        _regdateLbl.numberOfLines =2;
        [_regdateLbl setNotoText:@"기본정보 수정을 원하실 경우\n현대해상 고객센터(1588-5656)로 연락 바랍니다."];
    }else{
        [_regdateLbl setHidden:YES];
    }
}

#pragma mark - 추가정보 콜백
- (void)SignupStepEditDone:(NSDictionary*)dic {
    
    [self myinfoEditRequest:dic];
}


#pragma mark - subview action sheet Mehtod
- (void)sexActionSheet {
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *man = [UIAlertAction
                           actionWithTitle:@"남자"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1", @"mber_sex", nil];
                               [self myinfoEditRequest:dic];
                           }];
    UIAlertAction *wman = [UIAlertAction
                             actionWithTitle:@"여자"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"mber_sex", nil];
                                 [self myinfoEditRequest:dic];
                             }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"취소"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 [actionController dismissViewControllerAnimated:YES completion:nil];
                             }];
    [actionController addAction:man];
    [actionController addAction:wman];
    [actionController addAction:cancel];
    [self presentViewController:actionController animated:YES completion:nil];
}


#pragma mark -requestData
- (void)myinfoEditRequest:(NSDictionary*)dicData
{
    Tr_login *login = [_model getLoginData];
    
//    _actqyLbl, _jobLbl, _diseaseLbl, _smkingLbl
    [_model indicatorActive];
    
    NSString *mber_name     = login.mber_nm;
    NSString *mber_lifyea   = login.mber_lifyea;
    NSString *mber_hp       = login.mber_hp;
    NSString *mber_sex      = login.mber_sex;
    NSString *mber_height   = login.mber_height;
    NSString *mber_bdwgh    = login.mber_bdwgh;
    NSString *actqy         = login.mber_actqy;
    NSString *disease_nm    = login.disease_nm;
    NSString *disease_txt   = login.disease_txt;
    NSString *medicine_yn   = login.medicine_yn;
    NSString *smkng_yn      = login.smkng_yn;
    NSString *job_yn        = [_myPageData objectForKey:@"job_yn"];;
    
    if ([dicData objectForKey:@"mber_name"]) {
        mber_name = [dicData objectForKey:@"mber_name"];
        login.mber_nm = mber_name;
    }
    if ([dicData objectForKey:@"mber_lifyea"]) {
        mber_lifyea = [dicData objectForKey:@"mber_lifyea"];
        login.mber_lifyea = mber_lifyea;
    }
    if ([dicData objectForKey:@"mber_sex"]) {
        mber_sex = [dicData objectForKey:@"mber_sex"];
        login.mber_sex = mber_sex;
    }
    if ([dicData objectForKey:@"mber_hp"]) {
        mber_hp = [dicData objectForKey:@"mber_hp"];
        login.mber_hp = mber_hp;
    }
    if ([dicData objectForKey:@"mber_height"]) {
        mber_height = [dicData objectForKey:@"mber_height"];
        login.mber_height = mber_height;
    }
    if ([dicData objectForKey:@"mber_bdwgh"]) {
        mber_bdwgh = [dicData objectForKey:@"mber_bdwgh"];
        login.mber_bdwgh = mber_bdwgh;
    }
    if ([dicData objectForKey:@"disease_nm"]) {
        disease_nm = [dicData objectForKey:@"disease_nm"];
        login.disease_nm = disease_nm;
    }
    if ([dicData objectForKey:@"disease_txt"]) {
        disease_txt = [dicData objectForKey:@"disease_txt"];
        login.disease_txt = disease_txt;
    }
    if ([dicData objectForKey:@"medicine_yn"]) {
        medicine_yn = [dicData objectForKey:@"medicine_yn"];
        login.medicine_yn = medicine_yn;
    }
    if ([dicData objectForKey:@"smkng_yn"]) {
        smkng_yn = [dicData objectForKey:@"smkng_yn"];
        login.smkng_yn = smkng_yn;
    }
    if ([dicData objectForKey:@"actqy"]) {
        actqy = [dicData objectForKey:@"actqy"];
        login.mber_actqy = actqy;
    }
    if ([dicData objectForKey:@"job_yn"]) {
        job_yn = [dicData objectForKey:@"job_yn"];
        [_myPageData setValue:job_yn forKey:@"job_yn"];
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_modify_mberinfo", @"api_code",
                                mber_name, @"mber_name",
                                mber_lifyea, @"mber_lifyea",
                                mber_hp, @"mberhp",
                                mber_sex, @"mber_sex",
                                mber_height, @"mber_height",
                                mber_bdwgh, @"mber_bdwgh",
                                actqy, @"actqy",
                                disease_nm, @"disease_nm",
                                disease_txt, @"disease_txt",
                                medicine_yn, @"medicine_yn",
                                smkng_yn, @"smkng_yn",
                                job_yn, @"mber_job",
                                nil];
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_modify_mberinfo"]){
        [self viewInit];
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"asstb_mber_keep_member_id_change"]){
        // YY전환완료
        if ([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            
            [Utils basicAlertView:self withTitle:@"" withMsg:@"정회원 인증이 완료되었습니다.\n메디케어서비스 고객이 되신 것을 환영합니다." completion:^{
                Tr_login *login = [self->_model getLoginData];
                login = nil;
                [Utils setUserDefault:AUTO_LOGIN value:@"N"];
                
                UIViewController *vc = [LOGIN_STORYBOARD instantiateInitialViewController];
                UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
                UINavigationController *nc = (UINavigationController *)keyWindow.rootViewController;
                [nc setViewControllers:@[vc] animated:false];
            }];
            
            
        }
    }
    else if ([[result objectForKey:@"api_code"] isEqualToString:@"login"]) {
        if ([[result objectForKey:@"log_yn"] isEqualToString:@"Y"]) {
            [_model setLoginData:result];
            [self viewInit];
        }
    }
    
    
}

- (void)apiLogin {
    
    if ([Utils getUserDefault:USER_ID] == nil
        || [[Utils getUserDefault:USER_ID] length]==0) {
        return;
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"login", @"api_code",
                                [Utils getUserDefault:USER_ID], @"mber_id",
                                [Utils getUserDefault:USER_PASS], @"mber_pwd",
                                DIVECE_MODEL, @"phone_model",
                                APP_VERSION, @"app_ver",
                                @"", @"pushk",
                                [Utils getUserDefault:APP_TOKEN], @"token",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

@end
