//
//  SugarInputVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "SugarDBWraper.h"
#import "MessageDBWraper.h"

@interface SugarInputVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, DateControlVCDelegate, BasicAlertVCDelegate>

@property (weak, nonatomic) NSString *eatType;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;

@property (weak, nonatomic) IBOutlet UILabel *sugarLbl;
@property (weak, nonatomic) IBOutlet UITextField *sugarTf;

@property (weak, nonatomic) IBOutlet UIButton *beforeBtn;
@property (weak, nonatomic) IBOutlet UIButton *afterBtn;

@end
