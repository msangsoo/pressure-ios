//
//  SugarInputVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SugarInputVC.h"

@interface SugarInputVC () {
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
    NSString *healthMsg;
}

@end

@implementation SugarInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM03 m_cod:HM03028 s_cod:HM03028001];
    
    [self.navigationItem setTitle:@"혈당 입력"];
    
    [self navigationSetup];
    
    [self viewInit];
    
    [NSTimer scheduledTimerWithTimeInterval:0.3 repeats:NO block:^(NSTimer * _Nonnull timer) {
        if ([self.eatType isEqualToString:@"2"]) {
            UIButton *btn = [[UIButton alloc] init];
            btn.tag = 2;
            [self pressEatType:btn];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"bdsg_info_input_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            SugarDBWraper *db = [[SugarDBWraper alloc] init];
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            [arrResult addObject:writeData];
            [db insert:arrResult];
            
            Tr_login *login = [_model getLoginData];
            
            healthMsg = [_model getSugarMessage:[writeData objectForKey:@"sugar"] eatType:[writeData objectForKey:@"before"] regdate:[writeData objectForKey:@"regdate"]];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"infra_message_write", @"api_code",
                                        @"2", @"infra_ty",
                                        login.mber_sn, @"mber_sn",
                                        [writeData objectForKey:@"idx"], @"idx",
                                        healthMsg, @"infra_message",
                                        nil];
            
            [server serverCommunicationData:parameters];
            
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"infra_message_write"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]){
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        [writeData objectForKey:@"idx"], @"idx",
                                        @"2", @"infra_ty",
                                        healthMsg, @"message",
                                        @"YES", @"is_server_regist",
                                        [writeData objectForKey:@"regdate"], @"regdate",
                                        nil];
            
            [arrResult addObject:parameters];
            
            
            MessageDBWraper *db = [[MessageDBWraper alloc] init];
            [db insert:arrResult];
            [Utils setUserDefault:HEALTH_MSG_NEW value:@"N"];
            [Utils setUserDefault:HEALTH_POINT_ALERT value:@"4"];
            
            //관리팁 메세지
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            if ([userDefault objectForKey:HEALTH_SUGAR_TIP_TODAY_TIME]) {
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"yyyyMMdd"];
                NSDate *tipDate = [userDefault objectForKey:HEALTH_SUGAR_TIP_TODAY_TIME];
                
                int tipDateVal = [[format stringFromDate:tipDate] intValue];
                int nowDateVal = [[format stringFromDate:[NSDate date]] intValue];
                
                if (nowDateVal > tipDateVal) {
                    [self tipMessageAdd];
                }
            }else {
                [self tipMessageAdd];
            }
            
            //            [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"등록되었습니다." left:@"확인" right:@""];
        }
        
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - Tip Message
- (void)tipMessageAdd {
    float sugar = [[writeData objectForKey:@"sugar"] floatValue];
    if ([[writeData objectForKey:@"before"] isEqualToString:@"2"]) { // 식후 140
        if (sugar >= 140) {
            NSDictionary *tipData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"혈당", @"type",
                                     healthMsg, @"message",
                                     nil];
            [[NSUserDefaults standardUserDefaults] setObject:tipData forKey:HEALTH_TIP_DATA];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else { // 식전 100
        if (sugar >= 100) {
            NSDictionary *tipData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"혈당", @"type",
                                     healthMsg, @"message",
                                     nil];
            [[NSUserDefaults standardUserDefaults] setObject:tipData forKey:HEALTH_TIP_DATA];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:true];
    }else if (alertView.tag == 200) {
        if (tag == 2) {
            [self apiSave];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressSave:(UIButton *)sender {
    if ([_sugarTf.text length] == 0
        || [_sugarTf.text intValue] > 600
        || [_sugarTf.text intValue] < 10) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"혈당은 10이상 600이하 값으로 입력하여주세요."
                                                           delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
    }else {
        
        [self apiSave];
    }
}

- (IBAction)pressDate:(id)sender {
    [AlertUtils DateControlShow:self dateType:Date tag:1001 selectDate:nowDay];
}

- (IBAction)pressTime:(id)sender {
    [AlertUtils DateControlShow:self dateType:Time tag:1002 selectDate:nowTime];
}

- (IBAction)pressEatType:(UIButton *)sender {
    if (sender.tag == 1) {
        _beforeBtn.selected = true;
        [_beforeBtn setBackgroundColor:COLOR_NATIONS_BLUE];
        
        [_afterBtn setBackgroundColor:[UIColor whiteColor]];
        [_afterBtn borderRadiusButton:COLOR_GRAY_SUIT];
        _afterBtn.selected = false;
    }else {
        _afterBtn.selected = true;
        [_afterBtn setBackgroundColor:COLOR_NATIONS_BLUE];
        
        [_beforeBtn setBackgroundColor:[UIColor whiteColor]];
        [_beforeBtn borderRadiusButton:COLOR_GRAY_SUIT];
        _beforeBtn.selected = false;
    }
}

#pragma mark - DateControlVCDelegate
- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    // 미래날짜 선택불가.
    NSString *tempDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([tempDate longLongValue] > [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue]) {
        return;
    }
    
    if(alertView.view.tag == 1001) {
        [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [date stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:date]] forState:UIControlStateNormal];
        _dayBtn.tag = [[date stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        nowDay = date;
    }else if(alertView.view.tag == 1002) {
        [_timeBtn setTitle:[NSString stringWithFormat:@"%@", date] forState:UIControlStateNormal];
        nowTime = [NSString stringWithFormat:@"%@", date];
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _sugarTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_sugarTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_sugarTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float sugarVal = [[NSString stringWithFormat:@"%@%@", _sugarTf.text, string] floatValue];
        if (sugarVal >= 1000.f) {
            return false;
        }
    }
    
    return true;
}

#pragma mark - api
- (void)apiSave {
    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    
    NSString *tmpTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    writeData = [NSDictionary dictionaryWithObjectsAndKeys:
                [Utils getNowDate:@"yyMMddHHmmssSS"], @"idx",
                _sugarTf.text, @"sugar",
                @"", @"hiLow",
                _afterBtn.isSelected ? @"2" : @"1", @"before",
                @"", @"drugname",
                @"U", @"regtype",
                [NSString stringWithFormat:@"%ld%@", _dayBtn.tag, tmpTime], @"regdate",
                nil];
    [ast_mass addObject:writeData];
    
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"bdsg_info_input_data", @"api_code",
                                ast_mass, @"ast_mass",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _sugarLbl.font = FONT_NOTO_REGULAR(18);
    _sugarLbl.textColor = COLOR_MAIN_DARK;
    [_sugarLbl setNotoText:@"혈당"];
    
    _sugarTf.font = FONT_NOTO_MEDIUM(20);
    _sugarTf.textColor = COLOR_NATIONS_BLUE;
    [_sugarTf setPlaceholder:@"혈당"];
    _sugarTf.delegate = self;
    
    _beforeBtn.tag = 1;
    [_beforeBtn setTitle:@"식전" forState:UIControlStateNormal];
    [_beforeBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_beforeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_beforeBtn setBackgroundColor:COLOR_NATIONS_BLUE];
    [_beforeBtn makeToRadius:true];
    _beforeBtn.selected = true;
    
    _afterBtn.tag = 2;
    [_afterBtn setTitle:@"식후" forState:UIControlStateNormal];
    [_afterBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_afterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_afterBtn setBackgroundColor:[UIColor whiteColor]];
    [_afterBtn borderRadiusButton:COLOR_GRAY_SUIT];
    [_afterBtn makeToRadius:true];
    _afterBtn.selected = false;
    
    nowDay = [Utils getNowDate:@"yyyy-MM-dd"];
    nowTime = [Utils getNowDate:@"HH:mm"];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    _dayBtn.tag = [[nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@",nowTime] forState:UIControlStateNormal];
    _timeBtn.tag = [[nowTime stringByReplacingOccurrencesOfString:@":" withString:@""] integerValue];
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"저장"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressSave:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

@end
