//
//  SugarEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SugarEditVC.h"

@interface SugarEditVC () {
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
}

@end

@implementation SugarEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _titleLbl.font = FONT_NOTO_REGULAR(18);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"혈당 수정"];
    
    [self viewInit];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"bdsg_info_edit_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            SugarDBWraper *db = [[SugarDBWraper alloc] init];
            [db UpdateDb:[writeData objectForKey:@"idx"] sugar:[writeData objectForKey:@"sugar"] drugname:[writeData objectForKey:@"drugname"] before:[writeData objectForKey:@"before"] reg_de:[writeData objectForKey:@"regdate"]];
            
            [self dismissViewControllerAnimated:true completion:^{
                if (self.delegate) {
                    [self.delegate updateSuccess];
                }
            }];
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressCancel:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}

- (IBAction)pressEdit:(id)sender {
    if ([_sugarTf.text length] == 0
        || [_sugarTf.text intValue] > 600
        || [_sugarTf.text intValue] < 20) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"혈당은 10이상 600이하 값으로 입력하여주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
    }else {
        [self apiEdit];
        
    }
}

- (IBAction)pressEatType:(UIButton *)sender {
    if (sender.tag == 1) {
        _beforeBtn.selected = true;
        [_beforeBtn setBackgroundColor:COLOR_NATIONS_BLUE];
        
        [_afterBtn setBackgroundColor:[UIColor whiteColor]];
        [_afterBtn borderRadiusButton:COLOR_GRAY_SUIT];
        _afterBtn.selected = false;
    }else {
        _afterBtn.selected = true;
        [_afterBtn setBackgroundColor:COLOR_NATIONS_BLUE];
        
        [_beforeBtn setBackgroundColor:[UIColor whiteColor]];
        [_beforeBtn borderRadiusButton:COLOR_GRAY_SUIT];
        _beforeBtn.selected = false;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _sugarTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_sugarTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_sugarTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _sugarTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 선택한 값을 UITextField의 text값에 반영
    if(textField == self.timeTf) {
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar]
                                            components:NSCalendarUnitHour|NSCalendarUnitMinute
                                            fromDate:self.timePicker.date];
        [self.timeBtn setTitle:[NSString stringWithFormat:@"%02ld:%02ld", (long)dateComponents.hour, (long)dateComponents.minute] forState:UIControlStateNormal];
        nowTime = [self.timeTf.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
}

#pragma mark - api
- (void)apiEdit {
    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    
    nowTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    writeData = [NSDictionary dictionaryWithObjectsAndKeys:
                    [_userInfo objectForKey:@"idx"], @"idx",
                    self.sugarTf.text, @"sugar",
                    @"", @"hiLow",
                    self.afterBtn.isSelected ? @"2" : @"1", @"before",
                    @"", @"drugname",
                    @"U", @"regtype",
                    [NSString stringWithFormat:@"%@%@", nowDay, nowTime], @"regdate",
                    nil];
    [ast_mass addObject:writeData];
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"bdsg_info_edit_data", @"api_code",
                                ast_mass, @"ast_mass",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.timePicker = [[UIDatePicker alloc] init];
    self.timePicker.datePickerMode = UIDatePickerModeTime;
    self.timePicker.backgroundColor = [UIColor whiteColor];
    _timeTf.inputView = self.timePicker;
    _timeTf.delegate = self;
    
    _sugarLbl.font = FONT_NOTO_REGULAR(18);
    _sugarLbl.textColor = COLOR_MAIN_DARK;
    [_sugarLbl setNotoText:@"혈당"];
    
    _sugarTf.font = FONT_NOTO_MEDIUM(20);
    _sugarTf.textColor = COLOR_NATIONS_BLUE;
    [_sugarTf setPlaceholder:@"혈당"];
    _sugarTf.delegate = self;
    
    _beforeBtn.tag = 1;
    [_beforeBtn setTitle:@"식전" forState:UIControlStateNormal];
    [_beforeBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_beforeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_beforeBtn setBackgroundColor:COLOR_NATIONS_BLUE];
    [_beforeBtn makeToRadius:true];
    _beforeBtn.selected = true;
    
    _afterBtn.tag = 2;
    [_afterBtn setTitle:@"식후" forState:UIControlStateNormal];
    [_afterBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_afterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_afterBtn setBackgroundColor:[UIColor whiteColor]];
    [_afterBtn borderRadiusButton:COLOR_GRAY_SUIT];
    [_afterBtn makeToRadius:true];
    _afterBtn.selected = false;
    
    [_cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_editBtn setTitle:@"저장" forState:UIControlStateNormal];
    [_editBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    //수정시 기존 데이터 설정
    NSString *regdate = [_userInfo objectForKey:@"regdate"];
    
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dtF setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *d = [dtF dateFromString:regdate];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    nowDay = [dateFormat stringFromDate:d];
    
    [dateFormat setDateFormat:@"HH:mm"];
    nowTime = [dateFormat stringFromDate:d];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."],[Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    nowDay = [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@", nowTime] forState:UIControlStateNormal];
    nowTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    NSString *sugar = [_userInfo objectForKey:@"sugar"];
    self.sugarTf.text = sugar;
    
    _beforeBtn.selected = true;
    [_beforeBtn setBackgroundColor:COLOR_NATIONS_BLUE];
    
    [_afterBtn setBackgroundColor:[UIColor whiteColor]];
    [_afterBtn borderRadiusButton:COLOR_GRAY_SUIT];
    _afterBtn.selected = false;_beforeBtn.selected = true;
    _afterBtn.selected = false;
    if ([[_userInfo objectForKey:@"before"] intValue] == 2) {
        _afterBtn.selected = true;
        [_afterBtn setBackgroundColor:COLOR_NATIONS_BLUE];
        
        [_beforeBtn setBackgroundColor:[UIColor whiteColor]];
        [_beforeBtn borderRadiusButton:COLOR_GRAY_SUIT];
        _beforeBtn.selected = false;
    }
}

@end
