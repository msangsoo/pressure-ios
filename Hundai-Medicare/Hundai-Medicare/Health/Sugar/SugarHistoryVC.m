//
//  SugarHistoryVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "SugarHistoryVC.h"

@interface SugarHistoryVC () {
    NSDictionary *userInfo;
    NSInteger selectNum;
}

@property (strong, nonatomic) NSArray *items;

@end

@implementation SugarHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[SugarDBWraper alloc] init];
    
    _tableView.contentInset = UIEdgeInsetsMake(3, 0, 0, 0);
    
    _tableView.rowHeight = 60.f;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self viewSetup];
}

- (void)viewSetup {
    _items = [[db getResult] copy];
    [_tableView reloadData];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"bdsg_info_del_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            [db DeleteDb:[[_items objectAtIndex:selectNum] objectForKey:@"idx"]];
            
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[_items copy]];
            [tempArr removeObjectAtIndex:selectNum];
            _items = [NSArray arrayWithArray:tempArr];
            [_tableView reloadData];
        }
    }
}

#pragma mark - table delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cellIdentifier = @"HistoryCommonTableViewCell";
    HistoryCommonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"HistoryCommonTableViewCell" bundle:nil] forCellReuseIdentifier:@"HistoryCommonTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSDictionary *cellData = [_items objectAtIndex:indexPath.row];
    
    [cell.lblMediName setHidden:YES];
    [cell.lblInputType setHidden:true];
    
    [cell.lblMediValue setHidden:NO];
    [cell.lblEatType setHidden:NO];
    
    cell.lblEatType.textColor = COLOR_NATIONS_BLUE;
    if([[cellData objectForKey:@"before"] isEqualToString:@"2"]) {
        [cell.lblEatType setText:@"식후"];
    }else {
        [cell.lblEatType setText:@"식전"];
    }
    
    [cell.lblMediValue setText:[NSString stringWithFormat:@"%@mg/dL", [cellData objectForKey:@"sugar"]]];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [format dateFromString:[cellData objectForKey:@"regdate"]];
    if (date != nil) {
        [format setDateFormat:@"yyyy.MM.dd HH:mm"];
        [cell.lblDate setNotoText:[NSString stringWithFormat:@"%@", [format stringFromDate:date]]];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [view setBackgroundColor:[UIColor greenColor]];
    //add Buttons to view
    
    cell.editingAccessoryView = view;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableView select indexPath = %ld", indexPath.row);
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HistoryCommonTableViewCell *commentCell = (HistoryCommonTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    CGFloat height = commentCell.frame.size.height;
    selectNum = indexPath.row;
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"       " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        //userinfo에 셋팅해야함
        NSLog(@"editAction index = %ld", indexPath.row);
        
        if([[[self->_items objectAtIndex:self->selectNum] objectForKey:@"regtype"] isEqualToString:@"U"]) {
            self->userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"idx"], @"idx",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"sugar"], @"sugar",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"hilow"], @"hiLow",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"before"], @"before",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"drugname"], @"drugname",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"regtype"], @"regtype",
                              [[self->_items objectAtIndex:self->selectNum] objectForKey:@"regdate"], @"regdate",
                              nil];
            
            SugarEditVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SugarEditVC"];
            vc.userInfo = self->userInfo;
            vc.delegate = self;
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
            [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
        }else {
            //            [Utils showAlert:self tag:110 title:@"알림" message:@"장치에서 측정된 데이터는 수정 할 수 없습니다." okTitle:@"확인" cancelTitle:nil];
        }
    }];
    editAction.backgroundColor = COLOR_NATIONS_BLUE;
    UIImage *backgroundImage = [self cellImage:height imageName:@"btn_swipe_edit.png"];
    editAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"       "  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        NSLog(@"deleteAction index = %ld", indexPath.row);
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"삭제하시겠습니까?" left:@"취소" right:@"확인"];
    }];
    
    deleteAction.backgroundColor = RGB(143, 143, 143);
    backgroundImage = [self cellImage:height imageName:@"btn_swipe_del.png"];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [commentCell.ImgArrow setImage:[UIImage imageNamed:@"btn_light_blue_next"]];
    
    return @[deleteAction,editAction];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath {
    HistoryCommonTableViewCell *commentCell = (HistoryCommonTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [commentCell.ImgArrow setImage:[UIImage imageNamed:@"btn_light_blue_before"]];
}

#pragma mark - SugarEditVCDelegate
- (void)updateSuccess {
    [self viewSetup];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 100) {
        if (tag == 2) {
            Tr_login *login = [_model getLoginData];
            NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
            
            NSDictionary *dicTemp =[NSDictionary dictionaryWithObjectsAndKeys:
                                    [[_items objectAtIndex:selectNum] objectForKey:@"idx"], @"idx",
                                    nil];
            [ast_mass addObject:dicTemp];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"bdsg_info_del_data", @"api_code",
                                        login.mber_sn, @"mber_sn",
                                        ast_mass, @"ast_mass",
                                        nil];
            
            if(parameters != nil) {
                if(server.delegate == nil) {
                    server.delegate = self;
                }
                [_model indicatorActive];
                [server serverCommunicationData:parameters];
            }else {
                NSLog(@"server parameters error = %@", parameters);
            }
        }
    }
}

#pragma mark - image size mthod
- (UIImage *)cellImage:(CGFloat)height imageName:(NSString *)imgname {
    
    CGRect frame = CGRectMake(0, 0, 82, height);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(82, height), NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if([imgname isEqualToString:@"btn_swipe_edit.png"]) {
        CGContextSetFillColorWithColor(context, COLOR_NATIONS_BLUE.CGColor);
    }else{
        CGContextSetFillColorWithColor(context, RGB(143, 143, 143).CGColor);
    }
    
    CGContextFillRect(context, frame);
    
    UIImage *image = [UIImage imageNamed:imgname];
    
    [image drawInRect:CGRectMake(62/2.5, frame.size.height/6.0, 62/3.0, frame.size.height/6.0 * 4)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
