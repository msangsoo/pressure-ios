//
//  SugarHistoryVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SugarDBWraper.h"

#import "HistoryCommonTableViewCell.h"
#import "BaseViewController.h"
#import "SugarEditVC.h"

@interface SugarHistoryVC : BaseViewController<UITableViewDelegate, UITableViewDataSource, ServerCommunicationDelegate, BasicAlertVCDelegate, SugarEditVCDelegate> {
    SugarDBWraper *db;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)viewSetup;

@end
