//
//  SugarGraphVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "SugarDBWraper.h"
#import "MessageDBWraper.h"

#import "BaseViewController.h"
#import "SugarGraphZoomVC.h"

#import "FeedModel.h"
#import "CommunityWriteViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"
#import "SugarInputVC.h"

//소수차트
#import "PNScatterChart2.h"

@interface SugarGraphVC : BaseViewController<BasicAlertVCDelegate> {
    SugarDBWraper *db;
    MessageDBWraper *msgdb;
    TimeUtils *timeUtils;
    
    
    EAT_BEFORE_AFTER eatBeforeAfterType;
}

@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphRightLbl;

/* Type */
@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UIButton *beforeBtn;
@property (weak, nonatomic) IBOutlet UIButton *afterBtn;

/* Botoom */
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLbl;

@property (weak, nonatomic) IBOutlet UIView *bottomVw;

@property (weak, nonatomic) IBOutlet UILabel *avgTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *eatBeforeLbl;
@property (weak, nonatomic) IBOutlet UILabel *eatBeforeValLbl;
@property (weak, nonatomic) IBOutlet UILabel *eatBeforeUnitLbl;
@property (weak, nonatomic) IBOutlet UILabel *eatAfterLbl;
@property (weak, nonatomic) IBOutlet UILabel *eatAfterValLbl;
@property (weak, nonatomic) IBOutlet UILabel *eatAfterUnitLbl;

@property (weak, nonatomic) IBOutlet UILabel *maxLbl;
@property (weak, nonatomic) IBOutlet UILabel *maxValLbl;

@property (weak, nonatomic) IBOutlet UILabel *minLbl;
@property (weak, nonatomic) IBOutlet UILabel *minValLbl;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConst;

- (void)viewSetup;

@end
