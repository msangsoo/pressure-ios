//
//  WeightGraphZoomVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "WeightDBWraper.h"

#import "BaseViewController.h"

//라인차트관련
#import "SHLineGraphView.h"
#import "SHPlot.h"

@interface WeightGraphZoomVC : BaseViewController {
    WeightDBWraper *db;
    TimeUtils *timeUtils;
}

@property (assign) int tapTag;
@property (nonatomic, strong) TimeUtils *timeUtilsData;

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;

@property (weak, nonatomic) IBOutlet UIImageView *imageRotate;
@end
