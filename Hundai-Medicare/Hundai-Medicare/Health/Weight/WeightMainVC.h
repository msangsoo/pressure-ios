//
//  WeightMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "WeightGraphVC.h"
#import "WeightHistoryVC.h"

@interface WeightMainVC : BaseViewController {
    WeightGraphVC *vcGraph;
    WeightHistoryVC *vcHistory;
}

@property (weak, nonatomic) IBOutlet UIButton *graphBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

@end
