//
//  WeightEditVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "WeightDBWraper.h"

@interface WeightEditVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate>

@property UIDatePicker *timePicker;

@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UITextField *timeTf;

@property (weak, nonatomic) IBOutlet UILabel *weightLbl;
@property (weak, nonatomic) IBOutlet UITextField *weightTf;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@end

@protocol WeightEditVCDelegate
- (void)updateSuccess;
@end
