//
//  WeightEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WeightEditVC.h"

@interface WeightEditVC () {
    Tr_login *login;
    
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
}

@end

@implementation WeightEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    login = [_model getLoginData];
    
    [self viewInit];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"bdwgh_info_edit_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            WeightDBWraper *db = [[WeightDBWraper alloc] init];
            [db UpdateDb:[writeData objectForKey:@"idx"] fat:[writeData objectForKey:@"fat"] obesity:[writeData objectForKey:@"obesity"] bodywater:[writeData objectForKey:@"bodyWater"] muscle:[writeData objectForKey:@"muscle"] weight:[writeData objectForKey:@"weight"] reg_de:[writeData objectForKey:@"regdate"]];
            
            [self dismissViewControllerAnimated:true completion:^{
                if (self.delegate) {
                    [self.delegate updateSuccess];
                }
            }];
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressCancel:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}

- (IBAction)pressEdit:(id)sender {
    if ([_weightTf.text length]==0
        || [_weightTf.text intValue] < 20
        || [_weightTf.text intValue] > 300) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"체중은 20kg이상 300kg이하로 입력하여주세요."
                                                           delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
        return;
    }
    [self apiEdit];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _weightTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_weightTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound) {
            NSArray *textArr = [_weightTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 1 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _weightTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 선택한 값을 UITextField의 text값에 반영
    if(textField == self.timeTf) {
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar]
                                            components:NSCalendarUnitHour|NSCalendarUnitMinute
                                            fromDate:self.timePicker.date];
        [self.timeBtn setTitle:[NSString stringWithFormat:@"%02ld:%02ld", (long)dateComponents.hour, (long)dateComponents.minute] forState:UIControlStateNormal];
        nowTime = [self.timeTf.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
}

#pragma mark - api
- (void)apiEdit {
    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    
    
    
    writeData = [NSDictionary dictionaryWithObjectsAndKeys:
                [_userInfo objectForKey:@"idx"], @"idx",
                self.weightTf.text,@"weight",
                [self.userInfo objectForKey:@"fat"] ? [self.userInfo objectForKey:@"fat"] : @"", @"fat",
                [self.userInfo objectForKey:@"obesity"] ? [self.userInfo objectForKey:@"obesity"] : @"", @"obesity",
                [self.userInfo objectForKey:@"bodyWater"] ? [self.userInfo objectForKey:@"bodyWater"] : @"",  @"bodyWater",
                [self.userInfo objectForKey:@"muscle"] ? [self.userInfo objectForKey:@"muscle"] : @"", @"muscle",
                [self.userInfo objectForKey:@"bmr"] ? [self.userInfo objectForKey:@"bmr"] : @"", @"bmr",
                [self.userInfo objectForKey:@"heartRate"] ? [self.userInfo objectForKey:@"heartRate"] : @"", @"heartRate",
                [self.userInfo objectForKey:@"bone"] ? [self.userInfo objectForKey:@"bone"] : @"", @"bone",
                @"U",@"regtype",
                [NSString stringWithFormat:@"%@%@", nowDay, nowTime], @"regdate",
                nil];
    [ast_mass addObject:writeData];
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [_model indicatorHidden];
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"bdwgh_info_edit_data", @"api_code",
                                ast_mass, @"ast_mass",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}
- (void)apiMyinfo {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_user_call", @"api_code",
                                nil];
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_REGULAR(18);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"체중 수정"];
    
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.timePicker = [[UIDatePicker alloc] init];
    self.timePicker.datePickerMode = UIDatePickerModeTime;
    self.timePicker.backgroundColor = [UIColor whiteColor];
    _timeTf.inputView = self.timePicker;
    _timeTf.delegate = self;
    
    _weightLbl.font = FONT_NOTO_REGULAR(18);
    _weightLbl.textColor = COLOR_MAIN_DARK;
    [_weightLbl setNotoText:@"체중"];
    
    _weightTf.font = FONT_NOTO_MEDIUM(20);
    _weightTf.textColor = COLOR_NATIONS_BLUE;
    [_weightTf setPlaceholder:@"70.00"];
    _weightTf.delegate = self;
    
    [_cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_editBtn setTitle:@"저장" forState:UIControlStateNormal];
    [_editBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    //수정시 기존 데이터 설정
    NSString *regdate = [_userInfo objectForKey:@"regdate"];
    
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dtF setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *d = [dtF dateFromString:regdate];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    nowDay = [dateFormat stringFromDate:d];
    
    [dateFormat setDateFormat:@"HH:mm"];
    nowTime = [dateFormat stringFromDate:d];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."],[Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    nowDay = [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@", nowTime] forState:UIControlStateNormal];
    nowTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    self.weightTf.text = [self.userInfo objectForKey:@"weight"];
}

@end
