//
//  WeightInputVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WeightInputVC.h"

@interface WeightInputVC () {
    Tr_login *login;
    
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
    NSString *healthMsg;
}

@end

@implementation WeightInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM03 m_cod:HM03020 s_cod:HM03020001];
    
    [self.navigationItem setTitle:@"체중 입력"];
    
    login = [_model getLoginData];
    
    [self navigationSetup];
    
    [self viewInit];
    [self apiMyinfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"mber_user_call"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]){
            [_targetTf setPlaceholder:[result objectForKey:@"mber_bdwgh_goal"]];
            
            login.mber_bdwgh_goal = [result objectForKey:@"mber_bdwgh_goal"];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"bdwgh_info_input_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            login.mber_bdwgh_goal       = [writeData objectForKey:@"mber_bdwgh_goal"];
            login.mber_bdwgh            = [writeData objectForKey:@"weight"];
            login.mber_bdwgh_app        = [writeData objectForKey:@"weight"];
            
            WeightDBWraper *db = [[WeightDBWraper alloc] init];
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            [arrResult addObject:writeData];
            [db insert:arrResult isServerReg:YES];
            
            NSArray *tempArr = [db getBottomResult];
            if(tempArr.count > 0){
                login.mber_bdwgh_app = [[tempArr objectAtIndex:0] objectForKey:@"weight"];
            }
            
            healthMsg = [_model getWeightMessage:[writeData objectForKey:@"weight"] fat:[writeData objectForKey:@"fat"]];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"infra_message_write", @"api_code",
                                        @"2", @"infra_ty",
                                        [writeData objectForKey:@"idx"], @"idx",
                                        healthMsg, @"infra_message",
                                        nil];
            
            [server serverCommunicationData:parameters];
        }else {
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"bdwgh_goal_input"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            login.mber_bdwgh_goal = _targetTf.text;
            
//            [_model showAlert:self tag:1000 title:@"알림" message:@"등록되었습니다." okTitle:@"확인" cancelTitle:nil];
            
            [Utils setUserDefault:HEALTH_POINT_ALERT value:@"1"];
            [self.navigationController popViewControllerAnimated:true];
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"infra_message_write"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            
            healthMsg = [_model getWeightMessage:[writeData objectForKey:@"weight"] fat:[writeData objectForKey:@"fat"]];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        [writeData objectForKey:@"idx"], @"idx",
                                        @"2", @"infra_ty",
                                        healthMsg, @"message",
                                        @"YES", @"is_server_regist",
                                        [writeData objectForKey:@"regdate"], @"regdate",
                                        nil];
            
            [arrResult addObject:parameters];
            
            MessageDBWraper *db = [[MessageDBWraper alloc] init];
            [db insert:arrResult];
            [Utils setUserDefault:HEALTH_MSG_NEW value:@"N"];
            [Utils setUserDefault:HEALTH_POINT_ALERT value:@"1"];
            
            //관리팁 메세지
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            if ([userDefault objectForKey:HEALTH_WEIGHT_TIP_TODAY_TIME]) {
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"yyyyMMdd"];
                NSDate *tipDate = [userDefault objectForKey:HEALTH_WEIGHT_TIP_TODAY_TIME];
                
                int tipDateVal = [[format stringFromDate:tipDate] intValue];
                int nowDateVal = [[format stringFromDate:[NSDate date]] intValue];
                
                if (nowDateVal > tipDateVal) {
                    [self tipMessageAdd];
                }
            }else {
                [self tipMessageAdd];
            }
            
            [self.navigationController popViewControllerAnimated:true];
            
            //            [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"등록되었습니다." left:@"확인" right:@""];
        }
    }
}

#pragma mark - Tip Message
- (void)tipMessageAdd {
    NSString *rWeight = [NSString stringWithFormat:@"%.1f", [[writeData objectForKey:@"weight"] floatValue]];
    float rHeight = [login.mber_height floatValue] * 0.01f;
    float bmi = [[NSString stringWithFormat:@"%.1f", ([rWeight floatValue] / (rHeight * rHeight))] floatValue];
    
    if (bmi > 22.9) { //과체중 이상일때
        NSDictionary *tipData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"체중", @"type",
                                 healthMsg, @"message",
                                 nil];
        [[NSUserDefaults standardUserDefaults] setObject:tipData forKey:HEALTH_TIP_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - Actions
- (IBAction)pressSave:(UIButton *)sender {
    if ([_weightTf.text length]==0
        || [_weightTf.text intValue] < 20
        || [_weightTf.text intValue] > 300) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"체중은 20kg이상 300kg이하로 입력하여주세요."
                                                           delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
        [alertView show];
        return;
    }
    
    if ([_weightTf.text length] > 0) {
        [self apiSave];
    }else if ([_targetTf.text length] > 0) {
        [self apiTarget];
    }else {
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"체중을 입력해주세요." left:@"확인" right:@""];
    }
}

- (IBAction)pressDate:(id)sender {
    [AlertUtils DateControlShow:self dateType:Date tag:1001 selectDate:nowDay];
}

- (IBAction)pressTime:(id)sender {
    [AlertUtils DateControlShow:self dateType:Time tag:1002 selectDate:nowTime];
}

#pragma mark - DateControlVCDelegate
- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    // 미래날짜 선택불가.
    NSString *tempDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([tempDate longLongValue] > [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue]) {
        return;
    }
    
    if(alertView.view.tag == 1001) {
        [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [date stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:date]] forState:UIControlStateNormal];
        _dayBtn.tag = [[date stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        nowDay = date;
    }else if(alertView.view.tag == 1002) {
        [_timeBtn setTitle:[NSString stringWithFormat:@"%@", date] forState:UIControlStateNormal];
        nowTime = [NSString stringWithFormat:@"%@", date];
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _targetTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_targetTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_targetTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 1 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _targetTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }else if (textField == _weightTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_weightTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_weightTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 1 && ![string isEqualToString:@""]) {
               return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _weightTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }
    
    return true;
}

#pragma mark - api
- (void)apiSave {
    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    
    NSString *tmpTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    NSString *targetWeight = _targetTf.text;
    if(targetWeight.length < 1) {
        targetWeight = login.mber_bdwgh_goal;
    }
    
    writeData = [NSDictionary dictionaryWithObjectsAndKeys:
                    [Utils getNowDate:@"yyMMddHHmmssSS"], @"idx",
                    targetWeight, @"mber_bdwgh_goal",
                    _weightTf.text, @"weight",
                    @"0", @"fat",
                    @"0", @"obesity",
                    @"0", @"bodyWater",
                    @"0", @"muscle",
                    @"0", @"bmr",
                    @"0", @"heartRate",
                    @"0", @"bone",
                    @"U", @"regtype",
                    [NSString stringWithFormat:@"%ld%@", _dayBtn.tag, tmpTime], @"regdate",
                    nil];
    [ast_mass addObject:writeData];
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"bdwgh_info_input_data", @"api_code",
                                ast_mass, @"ast_mass",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiTarget {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"bdwgh_goal_input", @"api_code",
                                _targetTf.text, @"mber_bdwgh_goal",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiMyinfo {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mber_user_call", @"api_code",
                                nil];
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _targetLbl.font = FONT_NOTO_REGULAR(18);
    _targetLbl.textColor = COLOR_MAIN_DARK;
    [_targetLbl setNotoText:@"목표 체중"];
    
    _targetTf.font = FONT_NOTO_MEDIUM(18);
    _targetTf.textColor = COLOR_NATIONS_BLUE;
    [_targetTf setPlaceholder:@"70.00"];
    _targetTf.delegate = self;
    
    _weightLbl.font = FONT_NOTO_REGULAR(18);
    _weightLbl.textColor = COLOR_MAIN_DARK;
    [_weightLbl setNotoText:@"체중"];
    
    _weightTf.font = FONT_NOTO_MEDIUM((18));
    _weightTf.textColor = COLOR_NATIONS_BLUE;
    [_weightTf setPlaceholder:@"70.00"];
    _weightTf.delegate = self;
    
    nowDay = [Utils getNowDate:@"yyyy-MM-dd"];
    nowTime = [Utils getNowDate:@"HH:mm"];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    _dayBtn.tag = [[nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@",nowTime] forState:UIControlStateNormal];
    _timeBtn.tag = [[nowTime stringByReplacingOccurrencesOfString:@":" withString:@""] integerValue];
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"저장"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressSave:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

@end
