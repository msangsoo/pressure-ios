//
//  WeightInputVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "WeightDBWraper.h"
#import "MessageDBWraper.h"


@interface WeightInputVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, DateControlVCDelegate, BasicAlertVCDelegate>

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;

@property (weak, nonatomic) IBOutlet UILabel *targetLbl;
@property (weak, nonatomic) IBOutlet UITextField *targetTf;

@property (weak, nonatomic) IBOutlet UILabel *weightLbl;;
@property (weak, nonatomic) IBOutlet UITextField *weightTf;

@end
