//
//  WeightMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WeightMainVC.h"

@interface WeightMainVC ()

@end

@implementation WeightMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"체중관리"];
    
    [self viewInit];
    
    vcGraph = [self.storyboard instantiateViewControllerWithIdentifier:@"WeightGraphVC"];
    [self addChildViewController:vcGraph];
    vcGraph.view.bounds = _vwMain.bounds;
    vcGraph.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcGraph.view];
    [vcGraph.view setHidden:false];
    
    vcHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"WeightHistoryVC"];
    [self addChildViewController:vcHistory];
    vcHistory.view.bounds = _vwMain.bounds;
    vcHistory.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHistory.view];
    [vcHistory.view setHidden:true];
    
    [self.graphBtn setSelected:true];
    [self.historyBtn setSelected:false];
    
    [LogDB insertDb:HM03 m_cod:HM03018 s_cod:HM03018001];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.graphBtn setSelected:false];
    [self.historyBtn setSelected:false];
    
    if(!vcGraph.view.isHidden) {
        [self.graphBtn setSelected:true];
        self.tabBarVwLeftConst.constant = 0;
        
        [vcGraph.view setHidden:false];
        [vcHistory.view setHidden:true];
        [vcGraph viewSetup];
    }else {
        [self.historyBtn setSelected:true];
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcGraph.view setHidden:true];
        [vcHistory.view setHidden:false];
    }
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    if(sender.tag == 1 && !self.graphBtn.isSelected) {
        [LogDB insertDb:HM03 m_cod:HM03018 s_cod:HM03018001];
        
        [self.graphBtn setSelected:true];
        [self.historyBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [vcGraph.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        [vcGraph viewSetup];
    }else if(sender.tag == 2 && !self.historyBtn.isSelected) {
        [LogDB insertDb:HM03 m_cod:HM03019 s_cod:HM03019001];
        
        [self.graphBtn setSelected:false];
        [self.historyBtn setSelected:true];
        
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcGraph.view setHidden:true];
        [vcHistory.view setHidden:false];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.graphBtn.tag = 1;
    [self.graphBtn setTitle:@"그래프" forState:UIControlStateNormal];
    [self.graphBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.graphBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.historyBtn.tag = 2;
    [self.historyBtn setTitle:@"히스토리" forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
}


@end
