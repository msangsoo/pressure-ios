//
//  WeightGraphVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "WeightDBWraper.h"

#import "BaseViewController.h"
#import "WeightGraphZoomVC.h"

#import "FeedModel.h"
#import "CommunityWriteViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"

//라인차트관련
#import "SHLineGraphView.h"
#import "SHPlot.h"

@interface WeightGraphVC : BaseViewController<BasicAlertVCDelegate> {
    WeightDBWraper *db;
    TimeUtils *timeUtils;
}

@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;
@property (weak, nonatomic) IBOutlet UIButton *yearBtn;

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphInfoLbl;

/* Botoom */
@property (weak, nonatomic) IBOutlet UIView *bottomVw;

@property (weak, nonatomic) IBOutlet UILabel *nowTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *nowValLbl;
@property (weak, nonatomic) IBOutlet UILabel *targetTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *targetValLbl;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *resultValLbl;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphTypeWidthConst;

- (void) viewSetup;
@end
