//
//  WeightGraphZoomVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WeightGraphZoomVC.h"

@interface WeightGraphZoomVC () {
    Tr_login *login;
    
    NSArray *xItem;
    NSArray *xWeightData;
    NSArray *xFatData;
    int xWeightMaxValue;
    int xWeightMinValue;
}

@end

@implementation WeightGraphZoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[WeightDBWraper alloc] init];
    login = [_model getLoginData];
    
    if (self.tapTag == 0) {
        self.tapTag = 10;
    }
    
    [self viewInit];
    
   
    self.imageRotate.alpha = 0.3;
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.imageRotate.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0 delay:0.7 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.imageRotate.alpha = 0;
            } completion:^(BOOL finished) {
                self.imageRotate.hidden = YES;
            }];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self timeButtonInit];
    [self chartDataInit];
    [self weightChartInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(self.tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(self.tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (self.tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (self.tapTag == 13) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@", [startTime substringToIndex:4]]];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    UIImage *closeImg = [[UIImage imageNamed:@"btn_circle_gray_cancel"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:closeImg forState:UIControlStateNormal];
    [self.closeBtn setTintColor:COLOR_GRAY_SUIT];
    
    timeUtils = self.timeUtilsData;
}

#pragma mark - chart init
- (void)chartDataInit {
    if(self.tapTag == 10) {
        NSMutableArray *tempArr = [[NSMutableArray alloc] init];
        for(int i = 0 ; i < 24 ; i ++){
            NSDictionary *temp;
            if((i % 2) == 0){
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"",[NSString stringWithFormat:@"%d",i+1],nil];
            }else{
                
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i+1],nil];
            }
            [tempArr addObject:temp];
        }
        xItem = [NSArray arrayWithArray:tempArr];
        
        NSArray *arr = [db getResultDay:[timeUtils getNowTime] type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
    }else if(self.tapTag == 11) {
        xItem = @[@{@1 : @"일"}, @{@2 : @"월"}, @{@3 : @"화"}, @{@4 : @"수"}, @{@5 : @"목"}, @{@6 : @"금"}, @{@7 : @"토"}];
        
        NSArray *arr = [db getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime] type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else {
            xWeightData = arr;
        }
    }else if(self.tapTag == 12) {
        
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        for(int i = 1 ; i <= timeUtils.monthMaxDay ; i++){
            NSDictionary *temp;
            if((i % 2) == 0){
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"",[NSString stringWithFormat:@"%d",i],nil];
            }else{
                
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i],nil];
            }
            [temparr addObject:temp];
        }
        xItem = temparr;
        
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        
        NSArray *arr = [db getResultMonth:year nMonth:month type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
        //        xFatData = [[[db getResultMonth:year nMonth:month type:2] objectAtIndex:0] copy];
    }else if(self.tapTag == 13) {
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        
        for(int i = 1 ; i <= 12 ; i++) {
            NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%d",i], [NSString stringWithFormat:@"%d",i],nil];
            [temparr addObject:temp];
        }
        
        xItem = temparr;
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        
        NSArray *arr = [db getResultYear:year type:1];
        if ([arr count] >0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
    }
    
    while(xWeightData.count > xItem.count) {
        if(xWeightData.count > xItem.count) {
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xWeightData copy]];
            [tempArr removeLastObject];
            xWeightData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    NSMutableArray *temparr = [[NSMutableArray alloc] init];
    [temparr removeAllObjects];
    
    xWeightMaxValue = 0;
    xWeightMinValue = 0;
    if (xWeightData.count == 0) xWeightMinValue = 0;
    
    for(int i = 0 ; i < xWeightData.count ; i ++) {
        if(xWeightMaxValue < [[xWeightData objectAtIndex:i] intValue]) {
            xWeightMaxValue = [[xWeightData objectAtIndex:i] intValue];
        }
    }
    
    xWeightMinValue = xWeightMaxValue;
    
    for(int i = 0 ; i < xWeightData.count ; i ++) {
        NSDictionary *temp;
        
        if(xWeightMinValue > [[xWeightData objectAtIndex:i] intValue] &&
           [[xWeightData objectAtIndex:i] intValue] > 0) {
            xWeightMinValue = [[xWeightData objectAtIndex:i] intValue];
        }
        
        if([xWeightData objectAtIndex:i] == NULL ||
           [[xWeightData objectAtIndex:i] isEqualToString:@"(null)"] ||
           [[xWeightData objectAtIndex:i] intValue] == 0) {
            
        }else {
            temp = [NSDictionary dictionaryWithObjectsAndKeys:
                    [xWeightData objectAtIndex:i],[NSString stringWithFormat:@"%d",i + 1],nil];
            [temparr addObject:temp];
        }
    }
    
    // y축 최고값은 목표체중을 포함하여 최고값을 잡아야 한다.
//    Tr_login *login = [_model getLoginData];
//    double bdwghGoal = [login.mber_bdwgh_goal floatValue];
//    if (xWeightMaxValue < bdwghGoal) {
//        xWeightMaxValue = (int)bdwghGoal;
//    }
    
    if(temparr.count > 0) {
        xWeightData = [NSArray arrayWithArray:temparr];
    }else {
        xWeightData = temparr;
    }
}

- (void)weightChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    SHLineGraphView *_lineGraph = [[SHLineGraphView alloc] initWithFrame:CGRectMake(0, 0, _graphVw.layer.frame.size.width - 10, _graphVw.layer.frame.size.height - 10)];
    
    if (xWeightData.count == 0)  {
        xWeightMaxValue = 0;
        xWeightMinValue = 0;
    }
    
    _lineGraph.yAxisMaxRange = @(xWeightMaxValue + 3);
    _lineGraph.yAxisMinRange = @(xWeightMinValue - 3);
    _lineGraph.yAxisSuffix = @""; //Y축 단위
    _lineGraph.xAxisValues = xItem;
    
    SHPlot *_plot1 = [[SHPlot alloc] init];
    _plot1.plottingValues = xWeightData;
    
    
    NSDictionary *_plotThemeAttributes = @{ kPlotFillColorKey : RGBA(96, 123, 227, 1),
                                            kPlotStrokeWidthKey : @3,
                                            kPlotStrokeColorKey : RGBA(96, 123, 227, 1),
                                            kPlotPointFillColorKey : RGBA(96, 123, 227, 1),
                                            kPlotPointValueFontKey : FONT_NOTO_LIGHT(18) };
    
    _plot1.plotThemeAttributes = _plotThemeAttributes;
    [_lineGraph addPlot:_plot1];
    
    [_lineGraph setupTheView:0 cType:self.tapTag];
    _lineGraph.tag = 1;
    [_graphVw addSubview:_lineGraph];
}

#pragma mark - UIInterfaceOrientationMask
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)updatePreviewLayer:(AVCaptureConnection*)layer orientation:(AVCaptureVideoOrientation)orientation {
    layer.videoOrientation = orientation;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:false block:^(NSTimer * _Nonnull timer) {
        
        [self viewSetup];
    }];
}

@end
