//
//  WeightGraphVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WeightGraphVC.h"

@interface WeightGraphVC () {
    Tr_login *login;
    
    NSString *OCM_SEQ;
    
    int tapTag;
    
    NSArray *xItem;
    NSArray *xWeightData;
    NSArray *xFatData;
    int xWeightMaxValue;
    int xWeightMinValue;
}

@end

@implementation WeightGraphVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    OCM_SEQ = @"";
    
    db = [[WeightDBWraper alloc] init];
    timeUtils = [[TimeUtils alloc] init];
    login = [_model getLoginData];
    tapTag = 10;
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    _contentViewHeightConst.constant = _bottomVw.layer.frame.origin.y + _bottomVw.layer.frame.size.height + 15.f;
}

#pragma mark - Actions
- (IBAction)pressZoom:(id)sender {
    WeightGraphZoomVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WeightGraphZoomVC"];
    vc.tapTag = tapTag;
    vc.timeUtilsData = timeUtils;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressShare:(id)sender {
    OCM_SEQ = @"";
    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        
        NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.touchedCancelButton = ^{
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        };
        vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
            NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
            NSString *seq = self->_model.getLoginData.mber_sn;
            
            [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                if (isSuccess == TRUE) {
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                    
                    [LogDB insertDb:HM03 m_cod:HM03021 s_cod:HM03021001];
                    [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                    self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                    FeedModel *feed = [[FeedModel alloc] init];
                    feed.mberSn = [self->_model getLoginData].mber_sn;
                    NSString *meal = [NSString stringWithFormat:@"평균 체중 %@ kg", [self.graphInfoLbl.text substringToIndex:self.graphInfoLbl.text.length - 2]];
                    feed.cmMeal = meal;
                    
                    [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
                        if(success) {
                            self->OCM_SEQ = response[@"OCM_SEQ"];
                            [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
                        }
                    }];
                }else {
                    
                }
            }];
        };
        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
    }else {
        [LogDB insertDb:HM03 m_cod:HM03021 s_cod:HM03021001];
        
        FeedModel *feed = [[FeedModel alloc] init];
        feed.mberSn = [self->_model getLoginData].mber_sn;
        NSString *meal = [NSString stringWithFormat:@"평균 체중 %@ kg", [self.graphInfoLbl.text substringToIndex:self.graphInfoLbl.text.length - 2]];
        feed.cmMeal = meal;
        
        [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
            if(success) {
                self->OCM_SEQ = response[@"OCM_SEQ"];
                [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
            }
        }];
    }
}

- (IBAction)pressAdd:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WeightInputVC"];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressTab:(UIButton *)sender {
    
    BOOL pageChagneCheck = false;
    
    if(sender.tag == 1 && !_dayBtn.isSelected) {
        _dayBtn.selected = true;
        _weekBtn.selected = false;
        _monthBtn.selected = false;
        _yearBtn.selected = false;
        
        tapTag = 10;
        [timeUtils setTimeType:PERIOD_DAY];
        
        pageChagneCheck = true;
    }else if (sender.tag == 2 && !_weekBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = true;
        _monthBtn.selected = false;
        _yearBtn.selected = false;
        
        tapTag = 11;
        [timeUtils setTimeType:PERIOD_WEEK];
        
        pageChagneCheck = true;
    }else if (sender.tag == 3 && !_monthBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = false;
        _monthBtn.selected = true;
        _yearBtn.selected = false;
        
        tapTag = 12;
        [timeUtils setTimeType:PERIOD_MONTH];
        
        pageChagneCheck = true;
    }else if (sender.tag == 4 && !_yearBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = false;
        _monthBtn.selected = false;
        _yearBtn.selected = true;
        
        tapTag = 13;
        [timeUtils setTimeType:PERIOD_YEAR];
        pageChagneCheck = true;
    }
    
    if (pageChagneCheck) { //Chart view init
        [self viewSetup];
    }
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 2000) {
        if (tag == 2) {
            if (![OCM_SEQ isEqualToString:@""]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                feedDetailVC.seq = _model.getLoginData.mber_sn;
                feedDetailVC.feedSeq = OCM_SEQ;
                [self.navigationController pushViewController:feedDetailVC animated:TRUE];
            }
        }
    }
}

#pragma mark - viewSetup
- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (tapTag == 13) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@", [startTime substringToIndex:4]]];
    }
}

- (void)bottomInit {
    float avgWeight = [[db getAvgWeight:[timeUtils getNowTime] eDate:[timeUtils getEndTime]] floatValue];
    [_graphInfoLbl setNotoText:[NSString stringWithFormat:@"%.2fkg", avgWeight]];
    
    [_targetValLbl setNotoText:[NSString stringWithFormat:@"%@kg", login.mber_bdwgh_goal]];
    
    NSArray *tempArr = [db getBottomResult];
    
    NSString *dataWeight = @"";
    if(tempArr.count > 0){
        dataWeight = [[tempArr objectAtIndex:0] objectForKey:@"weight"];
    }else{
        // 없으면 회원가입시 등록한 기본정보의 몸무게
        dataWeight = [NSString stringWithFormat:@"%@", login.mber_bdwgh];
    }
    
    [_nowValLbl setNotoText: [NSString stringWithFormat:@"%.2fkg",[dataWeight floatValue]]];
    
    float temp = [dataWeight floatValue] - [login.mber_bdwgh_goal floatValue];
    if(tempArr.count < 1){
        [_resultValLbl setNotoText:@"--kg"];
    }else if(temp > 0){
        [_resultValLbl setNotoText:[NSString stringWithFormat:@"+ %.2fkg", temp]];
    }else{
        [_resultValLbl setNotoText:[NSString stringWithFormat:@"%.2fkg", temp]];
    }
    
    if(tempArr.count < 1){
        [_nowTitleLbl setNotoText:@"최근 측정(- -)"];
    }else{
        int nowDate = [[Utils getNowDate:@"yyyyMMdd"] intValue];
        int dbDate = [[Utils getTimeFormat:[[tempArr objectAtIndex:0] objectForKey:@"regdate"] beTime:@"yyyy-MM-dd HH:mm" afTime:@"yyyyMMdd"] intValue];
        
        if(nowDate == dbDate){
            [_nowTitleLbl setNotoText:@"최근 측정(오늘)"];
        }else if(nowDate > dbDate){
            long dbDatelong = [self getDateDiff:[[tempArr objectAtIndex:0] objectForKey:@"regdate"]];
            [_nowTitleLbl setNotoText:[NSString stringWithFormat:@"최근 측정(%ld일전)", dbDatelong]];
        }
        
    }
}

#pragma mark - chart init
- (void)chartDataInit {
    if(tapTag == 10) {
        NSMutableArray *tempArr = [[NSMutableArray alloc] init];
        for(int i = 0 ; i < 24 ; i ++){
            NSDictionary *temp;
            if((i % 2) == 0){
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"",[NSString stringWithFormat:@"%d",i+1],nil];
            }else{
                
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i+1],nil];
            }
            [tempArr addObject:temp];
        }
        xItem = [NSArray arrayWithArray:tempArr];
        
        NSArray *arr = [db getResultDay:[timeUtils getNowTime] type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
    }else if(tapTag == 11) {
        xItem = @[@{@1 : @"일"}, @{@2 : @"월"}, @{@3 : @"화"}, @{@4 : @"수"}, @{@5 : @"목"}, @{@6 : @"금"}, @{@7 : @"토"}];
        
        NSArray *arr = [db getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime] type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else {
            xWeightData = arr;
        }
    }else if(tapTag == 12) {
        
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        for(int i = 1 ; i <= timeUtils.monthMaxDay ; i++){
            NSDictionary *temp;
            if((i % 2) == 0){
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"",[NSString stringWithFormat:@"%d",i],nil];
            }else{
                
                temp = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i],nil];
            }
            [temparr addObject:temp];
        }
        xItem = temparr;
        
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        
        NSArray *arr = [db getResultMonth:year nMonth:month type:1];
        if ([arr count] > 0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
        //        xFatData = [[[db getResultMonth:year nMonth:month type:2] objectAtIndex:0] copy];
    }else if(tapTag == 13) {
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        
        for(int i = 1 ; i <= 12 ; i++) {
            NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%d",i], [NSString stringWithFormat:@"%d",i],nil];
            [temparr addObject:temp];
        }
        
        xItem = temparr;
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        
        NSArray *arr = [db getResultYear:year type:1];
        if ([arr count] >0) {
            xWeightData = [arr objectAtIndex:0];
        }else{
            xWeightData = arr;
        }
    }
    
    while(xWeightData.count > xItem.count) {
        if(xWeightData.count > xItem.count) {
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xWeightData copy]];
            [tempArr removeLastObject];
            xWeightData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    NSMutableArray *temparr = [[NSMutableArray alloc] init];
    [temparr removeAllObjects];
    
    xWeightMaxValue = 0;
    xWeightMinValue = 0;
    if (xWeightData.count == 0) xWeightMinValue = 0;
    
    for(int i = 0 ; i < xWeightData.count ; i ++) {
        if(xWeightMaxValue < [[xWeightData objectAtIndex:i] intValue]) {
            xWeightMaxValue = [[xWeightData objectAtIndex:i] intValue];
        }
    }
    
    xWeightMinValue = xWeightMaxValue;
    
    for(int i = 0 ; i < xWeightData.count ; i ++) {
        NSDictionary *temp;
        
        if(xWeightMinValue > [[xWeightData objectAtIndex:i] intValue] &&
           [[xWeightData objectAtIndex:i] intValue] > 0) {
            xWeightMinValue = [[xWeightData objectAtIndex:i] intValue];
        }
        
        if([xWeightData objectAtIndex:i] == NULL ||
           [[xWeightData objectAtIndex:i] isEqualToString:@"(null)"] ||
           [[xWeightData objectAtIndex:i] intValue] == 0) {
            
        }else {
            temp = [NSDictionary dictionaryWithObjectsAndKeys:
                    [xWeightData objectAtIndex:i],[NSString stringWithFormat:@"%d",i + 1],nil];
            [temparr addObject:temp];
        }
    }
    
    // y축 최고값은 목표체중을 포함하여 최고값을 잡아야 한다.
//    Tr_login *login = [_model getLoginData];
//    double bdwghGoal = [login.mber_bdwgh_goal floatValue];
//    if (xWeightMaxValue < bdwghGoal) {
//        xWeightMaxValue = (int)bdwghGoal;
//    }
    
    if(temparr.count > 0) {
        xWeightData = [NSArray arrayWithArray:temparr];
    }else {
        xWeightData = temparr;
    }
}

- (void)weightChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    SHLineGraphView *_lineGraph = [[SHLineGraphView alloc] initWithFrame:CGRectMake(0, 0, _graphVw.layer.frame.size.width - 10, _graphVw.layer.frame.size.height - 10)];
    
    if (xWeightData.count == 0)  {
        xWeightMaxValue = 0;
        xWeightMinValue = 0;
    }
    
    NSLog(@"xWeightMaxValue = %d", xWeightMaxValue);
    NSLog(@"xWeightMinValue = %d", xWeightMinValue);
    
    _lineGraph.yAxisMaxRange = @(xWeightMaxValue + 3);
    _lineGraph.yAxisMinRange = @(xWeightMinValue - 3);
    _lineGraph.yAxisSuffix = @""; //Y축 단위
    _lineGraph.xAxisValues = xItem;
    
    SHPlot *_plot1 = [[SHPlot alloc] init];
    _plot1.plottingValues = xWeightData;
    
    
    NSDictionary *_plotThemeAttributes = @{ kPlotFillColorKey : RGBA(96, 123, 227, 1),
                                            kPlotStrokeWidthKey : @3,
                                            kPlotStrokeColorKey : RGBA(96, 123, 227, 1),
                                            kPlotPointFillColorKey : RGBA(96, 123, 227, 1),
                                            kPlotPointValueFontKey : FONT_NOTO_LIGHT(18) };
    
    _plot1.plotThemeAttributes = _plotThemeAttributes;
    [_lineGraph addPlot:_plot1];
    
    [_lineGraph setupTheView:0 cType:tapTag];
    _lineGraph.tag = 1;
    [_graphVw addSubview:_lineGraph];
}

#pragma mark - viewInit
- (void)viewSetup {
    [self timeButtonInit];
    [self bottomInit];
    [self chartDataInit];
    [self weightChartInit];
}

- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _dayBtn.tag = 1;
    [_dayBtn setTitle:@"일간" forState:UIControlStateNormal];
    [_dayBtn mediHealthTabButton];
    
    _weekBtn.tag = 2;
    [_weekBtn setTitle:@"주간" forState:UIControlStateNormal];
    [_weekBtn mediHealthTabButton];
    
    _monthBtn.tag = 3;
    [_monthBtn setTitle:@"월간" forState:UIControlStateNormal];
    [_monthBtn mediHealthTabButton];
    
    _yearBtn.tag = 4;
    [_yearBtn setTitle:@"년간" forState:UIControlStateNormal];
    [_yearBtn mediHealthTabButton];
    
    _dayBtn.selected = true;
    
    _graphTypeLbl.font = FONT_NOTO_MEDIUM(17);
    _graphTypeLbl.textColor = COLOR_MAIN_DARK;
    [_graphTypeLbl setNotoText:@"평균 체중"];
    [_graphTypeLbl sizeToFit];
    _graphTypeWidthConst.constant = _graphTypeLbl.frame.size.width;
    
    _graphInfoLbl.textColor = COLOR_MAIN_ORANGE;
    _graphInfoLbl.text = @"";
    
    _bottomVw.layer.borderWidth = 1;
    _bottomVw.layer.borderColor = RGB(213, 213, 217).CGColor;

    _nowTitleLbl.font = FONT_NOTO_LIGHT(18);
    _nowTitleLbl.textColor = COLOR_MAIN_DARK;
    [_nowTitleLbl setNotoText:@"최근 측정(0일전)"];
    
    _nowValLbl.textColor = COLOR_MAIN_ORANGE;
    _nowValLbl.text = @"";
    
    _targetTitleLbl.font = FONT_NOTO_LIGHT(18);
    _targetTitleLbl.textColor = COLOR_MAIN_DARK;
    [_targetTitleLbl setNotoText:@"목표 체중"];
    
    [_targetValLbl setNotoText:login.mber_bdwgh_goal];
    _targetValLbl.textColor = COLOR_MAIN_ORANGE;
    
    _resultTitleLbl.font = FONT_NOTO_LIGHT(18);
    _resultTitleLbl.textColor = COLOR_MAIN_DARK;
    [_resultTitleLbl setNotoText:@"목표 대비"];
    
    _resultValLbl.textColor = COLOR_MAIN_ORANGE;
    _resultValLbl.text = @"";
}

#pragma mark - day result
- (long)getDateDiff:(NSString *)time {
    long tem = 0;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:time];
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    NSTimeInterval nowtimeInMiliseconds = [[NSDate date] timeIntervalSince1970]*1000;
    
    long diff = nowtimeInMiliseconds - timeInMiliseconds;
    tem = diff / (24 * 60 * 60 * 1000);
    return tem;
}


@end
