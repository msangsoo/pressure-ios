//
//  WeightHistoryVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WeightDBWraper.h"

#import "HistoryCommonTableViewCell.h"
#import "BaseViewController.h"
#import "WeightEditVC.h"

@interface WeightHistoryVC : BaseViewController<UITableViewDelegate, UITableViewDataSource, ServerCommunicationDelegate, WeightEditVCDelegate> {
    WeightDBWraper *db;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)viewSetup;

@end
