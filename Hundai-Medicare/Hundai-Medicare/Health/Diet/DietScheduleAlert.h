//
//  DietScheduleAlert.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 06/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DietScheduleAlert: UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *items;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end

#pragma mark - Cell
@interface DietScheduleAlertCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
