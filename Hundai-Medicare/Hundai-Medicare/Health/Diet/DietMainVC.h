//
//  DietMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 06/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "DietScheduleAlert.h"

@interface DietMainVC : BaseViewController<ServerCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *word1Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word2Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word3Lbl;
@property (weak, nonatomic) IBOutlet UILabel *word4Lbl;

@property (weak, nonatomic) IBOutlet UIButton *scheduleBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end


