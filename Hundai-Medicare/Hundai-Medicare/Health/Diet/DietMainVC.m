//
//  DietMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 06/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "DietMainVC.h"

@interface DietMainVC () {
    NSArray *schedule_list;
}

@end

@implementation DietMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"다이어트 프로그램"];
    
    schedule_list = [[NSArray alloc] init];
    
    [self viewInit];
    
    [self apiList];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_asstb_diet_program_schedule_list"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"N"]) {
            [self.word1Lbl setNotoText:[NSString stringWithFormat:@"매일 발송되는 체중관리 컨텐츠와\n%@회의 전문가 전화 상담을 통해\n맞춤형 체중관리를 제공해드리는\n서비스 입니다.\n%@일 동안 진행됩니다.", [result objectForKey:@"sch_day_cnt"], [result objectForKey:@"sch_day"]]];
            [self.word2Lbl setNotoText:[NSString stringWithFormat:@"%@ 명의 고객이 이용하고 있습니다.", [result objectForKey:@"sch_user"]]];
            [self setWord3Text:[result objectForKey:@"sch_per"]];
            
            schedule_list = [result objectForKey:@"schedule_list"];
        }
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"mvm_asstb_diet_program_req"]) {
        ///
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - Actions
- (IBAction)pressSchedule:(id)sender {
    DietScheduleAlert *vc = [DIET_STORYBOARD instantiateViewControllerWithIdentifier:@"DietScheduleAlert"];
    vc.items = schedule_list;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)pressConfirm:(id)sender {
    [self apiApply];
}

#pragma mark - api
- (void)apiList {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_asstb_diet_program_schedule_list", @"api_code",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiApply {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_asstb_diet_program_req", @"api_code",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.word1Lbl.font = FONT_NOTO_REGULAR(17);
    self.word1Lbl.textColor = COLOR_GRAY_SUIT;
    [self.word1Lbl setNotoText:[NSString stringWithFormat:@"매일 발송되는 체중관리 컨텐츠와\n%@회의 전문가 전화 상담을 통해\n맞춤형 체중관리를 제공해드리는\n서비스 입니다.\n%@일 동안 진행됩니다.", @"", @""]];
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSString *tipStr = [NSString stringWithFormat:@"                       스케줄"];
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:tipStr];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(17)
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor whiteColor]
                     range:NSMakeRange(0, tipStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:@"보기                       "];
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_BOLD(17)
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor blackColor]
                      range:NSMakeRange(0, secondStr.length)];
    [attstr appendAttributedString:secondStr];
    
    [self.scheduleBtn setAttributedTitle:attstr forState:UIControlStateNormal];
    [self.scheduleBtn makeToRadius:true];
    
    self.word2Lbl.font = FONT_NOTO_REGULAR(17);
    self.word2Lbl.textColor = COLOR_GRAY_SUIT;
    [self.word2Lbl setNotoText:[NSString stringWithFormat:@"%@ 명의 고객이 이용하고 있습니다.", @""]];
    
    [self setWord3Text:@"0"];
    
    self.word4Lbl.font = FONT_NOTO_REGULAR(17);
    self.word4Lbl.textColor = COLOR_GRAY_SUIT;
    [self.word4Lbl setNotoText:@"다이어트 프로그램을 신청하시겠습니다?"];
    
    [self.cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [self.cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [self.confirmBtn setTitle:@"신청" forState:UIControlStateNormal];
    [self.confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

#pragma mark - atter text
- (void)setWord3Text:(NSString *)string {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSString *tipStr = [NSString stringWithFormat:@"(이용 고객 만족도"];
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:tipStr];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_LIGHT(17)
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_GRAY_SUIT
                     range:NSMakeRange(0, tipStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSString *perString = [NSString stringWithFormat:@" %@o", string];
    perString = [perString stringByReplacingOccurrencesOfString:@"o" withString:@"%"];
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:perString];
    
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_LIGHT(17)
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:COLOR_MAIN_ORANGE
                      range:NSMakeRange(0, secondStr.length)];
    [attstr appendAttributedString:secondStr];
    
    NSMutableAttributedString *firs2tStr = [[NSMutableAttributedString alloc] initWithString:@")"];
    [firs2tStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, firs2tStr.length)];
    [firs2tStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_LIGHT(17)
                     range:NSMakeRange(0, firs2tStr.length)];
    [firs2tStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_GRAY_SUIT
                     range:NSMakeRange(0, firs2tStr.length)];
    [attstr appendAttributedString:firs2tStr];
    
    self.word3Lbl.attributedText = attstr;
}

@end
