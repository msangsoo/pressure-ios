//
//  DietScheduleAlert.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 06/04/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "DietScheduleAlert.h"

@interface DietScheduleAlert ()

@end

@implementation DietScheduleAlert

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cellIdentifier = @"DietScheduleAlertCell";
    DietScheduleAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"DietScheduleAlertCell" bundle:nil] forCellReuseIdentifier:@"DietScheduleAlertCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSDictionary *item = [self.items objectAtIndex:indexPath.row];
    
    NSString *cellString = [NSString stringWithFormat:@"%@일차 %@", item[@"schedule_day"], item[@"schedule_subject"]];
    [cell.titleLbl setNotoText:cellString];
    
    return cell;
}

#pragma mark - UITableViewDelegate

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismiss:nil];
}

- (IBAction)pressConfirm:(id)sender {
    [self dismiss:nil];
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_MEDIUM(20);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"스케줄 보기"];
    
    self.word1Lbl.font = FONT_NOTO_REGULAR(17);
    self.word1Lbl.textColor = COLOR_NATIONS_BLUE;
    [self.word1Lbl setNotoText:[NSString stringWithFormat:@"%ld일차 전문가 전화 상담", self.items.count]];
    
    self.word2Lbl.font = FONT_NOTO_LIGHT(17);
    self.word2Lbl.textColor = COLOR_GRAY_SUIT;
    [self.word2Lbl setNotoText:@"컨텐츠는 카카오 알림톡으로 발송되며\n카카오톡 미 이용자는 문자로 발송됩니다."];
    
    [self.confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [self.confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

@end


#pragma mark - Cell
@implementation DietScheduleAlertCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLbl.font = FONT_NOTO_REGULAR(16);
    self.titleLbl.textColor = COLOR_GRAY_SUIT;
    self.titleLbl.text = @"";
}

@end
