//
//  WalkGraphZoomVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "TimeUtils.h"
#import "WalkDBWraper.h"
#import "DataCalroieDBWraper.h"

//바차트관련
#import "SBCEViewController.h"

@interface WalkGraphZoomVC : BaseViewController {
    TimeUtils *timeUtils;
    WalkDBWraper *db;
    DataCalroieDBWraper *calDb;
}

@property (assign) int twoTap;
@property (assign) int oneTap;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) TimeUtils *timeUtilsData;

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *unitLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphRightLbl;
@property (weak, nonatomic) IBOutlet UIView *infoVw;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoHeightConst;

@property (weak, nonatomic) IBOutlet UIImageView *imageRotate;

@end

