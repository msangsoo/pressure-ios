//
//  WalkMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WalkMainVC.h"

#import "CaffeineDataStore.h"

@interface WalkMainVC () {
    BOOL dayMutiTouch;
    
    NSString *OCM_SEQ;
    
    int oneTap;
    int twoTap;
    
    NSArray *barChartData;
    NSArray *lifeFoodArr;
    NSArray *lifeFoodUnitArr;
    NSArray *lifeFoodCalorieArr;
    NSMutableArray *ast_mass;
    
    int Goal_cal;
    int Goal_step;
    int dayData;
    NSTimeInterval longTime;
    
    int static_total_cal;
    int static_total_step;
    int manualCal;
    
    Tr_login *login;
}

@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;

@end

@implementation WalkMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    OCM_SEQ = @"";
    
    self.title = @"걷기관리";
    dayMutiTouch = false;
    login = [_model getLoginData];
    
    longTime = 0;
    Goal_cal = 0;
    Goal_step = 0;
    manualCal = 0;
    dayData = 1;
    unit = @"kcal";
    
    lifeFoodArr = LIFE_FOOD;
    lifeFoodUnitArr = LIFE_FOOD_UNIT;
    lifeFoodCalorieArr = LIFE_FOOD_CALORIE;
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    self.caffeineDataStore = [[CaffeineDataStore alloc] initWithHealthStore:healthStore];
    
    db = [[WalkDBWraper alloc] init];
    calDb = [[DataCalroieDBWraper alloc] init];
    timeUtils = [[TimeUtils alloc] init];
    
    oneTap = 10;
    twoTap = 20;
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self viewSetup];
}

- (void)viewDidAppear:(BOOL)animated {
    _contentViewHeightConst.constant = _bottomVw.layer.frame.origin.y + _bottomVw.layer.frame.size.height + 15.f;
    [self.view layoutIfNeeded];
    
}

#pragma mark - Actions
- (IBAction)pressZoom:(id)sender {
    WalkGraphZoomVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WalkGraphZoomVC"];
    vc.oneTap = oneTap;
    vc.twoTap = twoTap;
    vc.timeUtilsData = timeUtils;
    vc.unit = self.graphUnitLbl.text;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressTarget:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WalkTargetVC"];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressShare:(id)sender {
    OCM_SEQ = @"";
    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        
        NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.touchedCancelButton = ^{
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        };
        vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
            NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
            NSString *seq = self->_model.getLoginData.mber_sn;
            
            [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                if (isSuccess == TRUE) {
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                    
                    [LogDB insertDb:HM03 m_cod:HM03017 s_cod:HM03017001];
                    [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                    self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                    FeedModel *feed = [[FeedModel alloc] init];
                    feed.mberSn = [self->_model getLoginData].mber_sn;
                    NSString *meal = [NSString stringWithFormat:@"%@ %@", self.graphTypeLbl.text, self.graphInfoLbl.text];
                    feed.cmMeal = meal;
                    
                    [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
                        if(success) {
                            self->OCM_SEQ = response[@"OCM_SEQ"];
                            [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
                        }
                    }];
                }else {
                    
                }
            }];
        };
        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
    }else {
        [LogDB insertDb:HM03 m_cod:HM03017 s_cod:HM03017001];
        
        FeedModel *feed = [[FeedModel alloc] init];
        feed.mberSn = [self->_model getLoginData].mber_sn;
        NSString *meal = [NSString stringWithFormat:@"%@ %@", self.graphTypeLbl.text, self.graphInfoLbl.text];
        feed.cmMeal = meal;
        
        [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
            if(success) {
                self->OCM_SEQ = response[@"OCM_SEQ"];
                [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
            }
        }];
    }
}

- (IBAction)pressAdd:(id)sender {
    NSString *dateString = [timeUtils getNowTime];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    WalkInputVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WalkInputVC"];
    vc.nowDate = [format dateFromString:dateString];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressTab:(UIButton *)sender {
    if (dayMutiTouch) {
        return;
    }
    dayMutiTouch = true;
    
    [_model indicatorActive];
    
    BOOL pageChagneCheck = false;
    
    if(sender.tag == 1 && !_dayBtn.isSelected) {
        _dayBtn.selected = true;
        _weekBtn.selected = false;
        _monthBtn.selected = false;
        
        [timeUtils setTimeType:PERIOD_DAY];
        dayData = 1;
        twoTap = 20;
        pageChagneCheck = true;
    }else if (sender.tag == 2 && !_weekBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = true;
        _monthBtn.selected = false;
        
        [timeUtils setTimeType:PERIOD_WEEK];
        dayData = 7;
        twoTap = 21;
        pageChagneCheck = true;
    }else if (sender.tag == 3 && !_monthBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = false;
        _monthBtn.selected = true;
        
        [timeUtils setTimeType:PERIOD_MONTH];
        dayData = (int)timeUtils.monthMaxDay;
        twoTap = 22;
        pageChagneCheck = true;
    }
    
    if (pageChagneCheck) { //Chart view init
        [self viewSetup];
    }else {
        if (self->timerProcess) {
            [self->timerProcess invalidate];
            self->timerProcess = nil;
        }
        self->timerProcess = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(healthKitProcessEnd:) userInfo:nil repeats:NO];
    }
}

- (IBAction)pressDataType:(UIButton *)sender {
    if (dayMutiTouch) {
        return;
    }
    
    dayMutiTouch = true;
    
    [_model indicatorActive];
    
    BOOL pageChagneCheck = false;
    
    if(sender.tag == 1 && !_kcalBtn.isSelected) {
        _kcalBtn.selected = true;
        _walkBtn.selected = false;
        oneTap = 10;
        unit = @"kcal";
        pageChagneCheck = true;
        self.graphInfoVwTopConst.constant = 35;
    }else if (sender.tag == 2 && !_walkBtn.isSelected) {
        _kcalBtn.selected = false;
        _walkBtn.selected = true;
        oneTap = 11;
        unit = @"step";
        pageChagneCheck = true;
        self.graphInfoVwTopConst.constant = 0;
    }
    [self.view layoutIfNeeded];
    
    if (pageChagneCheck) { //Chart view init
        [self viewSetup];
    }else {
        if (self->timerProcess) {
            [self->timerProcess invalidate];
            self->timerProcess = nil;
        }
        self->timerProcess = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(healthKitProcessEnd:) userInfo:nil repeats:NO];
    }
    
    
    _contentViewHeightConst.constant = _bottomVw.layer.frame.origin.y + _bottomVw.layer.frame.size.height + 15.f;
    [self.view layoutIfNeeded];
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 2000) {
        if (tag == 2) {
            if (![OCM_SEQ isEqualToString:@""]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                feedDetailVC.seq = _model.getLoginData.mber_sn;
                feedDetailVC.feedSeq = OCM_SEQ;
                [self.navigationController pushViewController:feedDetailVC animated:TRUE];
            }
        }
    }
}


#pragma mark - viewSetup
- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(twoTap == 20) {
        [_dateLbl setNotoText:startTime];
        _inputBtn.hidden = false;
    }else if(twoTap == 21) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
        _inputBtn.hidden = true;
    }else if (twoTap == 22) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
        _inputBtn.hidden = true;
    }
}

- (void)bottomInit {
    int totalCalorie;
    int totStep;
    
    totalCalorie = static_total_cal + manualCal;
    totStep = static_total_step;
    
    float avgStepDistance = [login.mber_height floatValue] - 100.f;  // [_login.mber_height floatValue] - 100.f;
    if(avgStepDistance < 0.f) {
        avgStepDistance = 60.f;
    }
    
    if(_kcalBtn.isSelected) {
        [_graphTypeLbl setNotoText:@"소모 칼로리"];
        [_graphTypeLbl sizeToFit];
        _graphTypeWidthConst.constant = _graphTypeLbl.frame.size.width;
         [self setGraphInfoText:[Utils getNumberComma:[NSString stringWithFormat:@"%d",totalCalorie]] totalVal:[Utils getNumberComma:[NSString stringWithFormat:@"%d",Goal_cal*dayData]] unit:@"kcal"];
        [self.graphUnitLbl setNotoText:@"kcal"];
        
        [_ImgVwBottom2 setImage:[UIImage imageNamed:@"step_totaltime_icon"]];
        [_ImgVwBottom3 setImage:[UIImage imageNamed:@"step_kcal_icon"]];
        [_ImgVwBottom4 setImage:[UIImage imageNamed:@"step_longtime_icon"]];
        
        [_lblBottom1SubTitle setNotoText:@"목표달성률"];
        [_lblBottom2SubTitle setNotoText:@"총활동 시간"];
        [_lblBottom3SubTitle setNotoText:@"활동 소모 칼로리 비교"];
        [_lblBottom4SubTitle setNotoText:@"최장 연속 활동 시간"];
        
        if(Goal_cal == 0) { //vau1
            [_lblBottom1SubNumber setNotoText:@"-"];
        }else {
            [_lblBottom1SubNumber setHealthBottom:[NSString stringWithFormat:@"%.1f", (float)((totalCalorie / ((float)Goal_cal*dayData)) * 100.f)] unit:@"%" fontsize:17];
        }
        
        [_lblBottom2SubNumber setHealthBottom:[NSString stringWithFormat:@"%1.f", (float)(totStep / 60.f)] unit:@"분" fontsize:17];
        
        [_lblBottom3SubNumber setNotoText:[self calcClaorieCompare:totalCalorie]];
    }else {
        [_graphTypeLbl setNotoText:@"총 걸음 수"];
        [_graphTypeLbl sizeToFit];
        _graphTypeWidthConst.constant = _graphTypeLbl.frame.size.width;
        [self setGraphInfoText:[Utils getNumberComma:[NSString stringWithFormat:@"%d",totStep]] totalVal:[Utils getNumberComma:[NSString stringWithFormat:@"%d",Goal_step*dayData]] unit:@"steps"];
        [self.graphUnitLbl setNotoText:@"steps"];
        
        
        [_ImgVwBottom2 setImage:[UIImage imageNamed:@"step_dis_icon"]];
        [_ImgVwBottom3 setImage:[UIImage imageNamed:@"step_money_icon"]];
        [_ImgVwBottom4 setImage:[UIImage imageNamed:@"step_avgdis_icon"]];
        
        [_lblBottom1SubTitle setNotoText:@"목표달성률"];
        [_lblBottom2SubTitle setNotoText:@"총 이동 거리"];
        [_lblBottom3SubTitle setNotoText:@"아낀 택시비"];
        [_lblBottom4SubTitle setNotoText:@"평균 보폭"];
        
        float totalMoveDistance = (avgStepDistance * totStep) * 0.1f;
        float disTotalDistance = totalMoveDistance / 1000.f;
        float value = disTotalDistance * 0.1f;
        float kmcm = [[NSString stringWithFormat:@"%.2fkm", value] floatValue] * 100000.f;
        
        if(Goal_step == 0) {
            [_lblBottom1SubNumber setText:@"-"];
        }else {
            [_lblBottom1SubNumber setHealthBottom:[NSString stringWithFormat:@"%.1f", (float)((totStep / ((float)Goal_step*dayData)) * 100.f)] unit:@"%" fontsize:17];
        }
        
        [_lblBottom2SubNumber setHealthBottom:[NSString stringWithFormat:@"%.2f", value] unit:@"km" fontsize:17];
        [_lblBottom3SubNumber setHealthBottom:[self calcSaveMoney:(totalMoveDistance / 10.f)] unit:@"원" fontsize:17];
        
        if(kmcm == 0.f) {
            [_lblBottom4SubNumber setText:@"0cm"];
        }else {
            [_lblBottom4SubNumber setHealthBottom:[NSString stringWithFormat:@"%.1f", (kmcm / (float)totStep)] unit:@"cm" fontsize:17];
        }
    }
    
    [self.graphUnitLbl sizeToFit];
}

- (void)stepChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    manualCal = 0;
    SBCEViewController *bar = [[SBCEViewController alloc] init];
    SimpleBarChart *barChart;
    if(twoTap == 20) {
        
        if (oneTap == 10) {
            //칼로리
            NSArray *arrCal = [[calDb getResultDay:[timeUtils getNowTime]] objectAtIndex:0];
            [self calcManualCal:arrCal];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_DAY xCount:24];
        }else{
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_DAY xCount:24];
        }
    }else if(twoTap == 21) {
        
        if (oneTap == 10) {
            //칼로리
            NSArray *arrCal = [[calDb getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime]] objectAtIndex:0];
            [self calcManualCal:arrCal];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_WEEK xCount:7];
        }else {
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_WEEK xCount:7];
        }
        
    }else if(twoTap == 22) {
        NSLog(@"_timeUtils.monthMaxDay:%ld", (long)timeUtils.monthMaxDay);
        
        if (oneTap == 10) {
            //칼로리
            NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
            NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
            
            NSArray *arrCal = [[calDb getResultMonth:year nMonth:month] objectAtIndex:0];
            [self calcManualCal:arrCal];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
        }else{
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
        }
    }
    [bar reloadData];
    
    barChart.unit = unit;
    barChart.tag = 1;
    [_graphVw addSubview:barChart];
}

- (void)healthNotCountMethod:(NSString *)timeType {
    if([timeType isEqualToString:@"day"]) {
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0)];
    }else if([timeType isEqualToString:@"week"]) {
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0),
                         @(0)];
    }else{
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0)];
    }
    [self stepChartInit];
    [self bottomInit];
    
    if (self->timerProcess) {
        [self->timerProcess invalidate];
        self->timerProcess = nil;
    }
    self->timerProcess = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(healthKitProcessEnd:) userInfo:nil repeats:NO];
}

- (void)calcManualCal:(NSArray*)arrData {
    
    manualCal = 0;
    for (int i=0; i < [arrData count]; i++) {
        manualCal += [[arrData objectAtIndex:i] intValue];
    }
}

#pragma mark - viewInit
- (void)viewSetup {
    [self.caffeineDataStore fetchCaffieneSamplesWithCompletion:^(NSArray *samples, NSError *error) {
        if(samples.count > 0) {
            NSLog(@"step = %@", samples);
            [self performSelectorOnMainThread:@selector(getHealthAccessCheck) withObject:self waitUntilDone:YES];
        }else {
            self->dayMutiTouch = false;
            [self->_model indicatorHidden];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"앱 접근권한" message:@"건강앱 접근권한이 설정되지 않았습니다.\n설정 > 개인정보보호 > 건강 > 현대해상 메디케어" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"설정" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                NSURL *url = [NSURL URLWithString: UIApplicationOpenSettingsURLString];
                [UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];
                
            }];
            UIAlertAction *canAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:canAction];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    
    [self timeButtonInit];
    [self bottomInit];
    
    if(_kcalBtn.isSelected) {
        [LogDB insertDb:HM01 m_cod:HM03012 s_cod:HM03012001];
    }else {
        [LogDB insertDb:HM01 m_cod:HM03013 s_cod:HM03013001];
    }
}

- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _dayBtn.tag = 1;
    [_dayBtn setTitle:@"일간" forState:UIControlStateNormal];
    [_dayBtn mediHealthTabButton];
    
    _weekBtn.tag = 2;
    [_weekBtn setTitle:@"주간" forState:UIControlStateNormal];
    [_weekBtn mediHealthTabButton];
    
    _monthBtn.tag = 3;
    [_monthBtn setTitle:@"월간" forState:UIControlStateNormal];
    [_monthBtn mediHealthTabButton];
    
    _dayBtn.selected = true;
    
    _kcalBtn.tag = 1;
    [_kcalBtn setTitle:@"  칼로리" forState:UIControlStateNormal];
    [_kcalBtn mediHealthSecondTabButton];
    
    _walkBtn.tag = 2;
    [_walkBtn setTitle:@"  걸음" forState:UIControlStateNormal];
    [_walkBtn mediHealthSecondTabButton];
    
    _kcalBtn.selected = true;
    
    _graphLeftLbl.font = FONT_NOTO_REGULAR(12);
    _graphLeftLbl.textColor = COLOR_MAIN_DARK;
    [_graphLeftLbl setNotoText:@"걸음칼로리"];
    
    _graphRightLbl.font = FONT_NOTO_REGULAR(12);
    _graphRightLbl.textColor = COLOR_MAIN_DARK;
    [_graphRightLbl setNotoText:@"운동칼로리"];
    
    _graphTypeLbl.font = FONT_NOTO_MEDIUM(17);
    _graphTypeLbl.textColor = COLOR_MAIN_DARK;
    
    _bottomVw.layer.borderWidth = 1;
    _bottomVw.layer.borderColor = RGB(213, 213, 217).CGColor;
    
    _lblBottom1SubTitle.font = FONT_NOTO_LIGHT(18);
    _lblBottom1SubTitle.textColor = COLOR_MAIN_DARK;
    
    _lblBottom2SubTitle.font = FONT_NOTO_LIGHT(18);
    _lblBottom2SubTitle.textColor = COLOR_MAIN_DARK;
    
    _lblBottom3SubTitle.font = FONT_NOTO_LIGHT(18);
    _lblBottom3SubTitle.textColor = COLOR_MAIN_DARK;
    
    _lblBottom4SubTitle.font = FONT_NOTO_LIGHT(18);
    _lblBottom4SubTitle.textColor = COLOR_MAIN_DARK;
    
    _lblBottom1SubNumber.font = FONT_NOTO_REGULAR(17);
    _lblBottom1SubNumber.textColor = COLOR_MAIN_ORANGE;
    
    _lblBottom2SubNumber.font = FONT_NOTO_REGULAR(17);
    _lblBottom2SubNumber.textColor = COLOR_MAIN_ORANGE;
    
    _lblBottom3SubNumber.font = FONT_NOTO_REGULAR(17);
    _lblBottom3SubNumber.textColor = COLOR_MAIN_ORANGE;
    
    _lblBottom4SubNumber.text = @"-";
    _lblBottom4SubNumber.font = FONT_NOTO_REGULAR(17);
    _lblBottom4SubNumber.textColor = COLOR_MAIN_ORANGE;
}

#pragma mark - health kit
- (void)getHealthAccessCheck {
    //목표칼로리, 목표걸음수
    if([login.goal_mvm_calory intValue] < 1){
        //        [[AppModal sharedApp] getStepCalori:0 _step:0];
    }
    Goal_step = [login.goal_mvm_stepcnt intValue];
    Goal_cal = [login.goal_mvm_calory intValue];
    
    if (Goal_cal==0) {
        Goal_cal  = [_model getDefaultGoalCalori];
    }
    
    int sex = [login.mber_sex intValue];
    float height = [login.mber_height floatValue];
    float weight = [login.mber_bdwgh_app intValue];
    
    if (weight==0) {
        weight = [login.mber_bdwgh intValue];
    }
    
    if (Goal_step==0) {
        Goal_step  = [[_model getStepTargetCalulator:sex height:height weight:weight calrori:(int)Goal_cal] intValue];
    }
    
    if(oneTap == 10) {
        [self healthKit];
    }
    
    static_total_step = 0;
    static_total_cal = 0;
    [self timeButtonInit];
    
//    int nowTime = [[Utils getNowDate:@"yyyyMMdd"] intValue];
    int timeUtilsNowTime = [[Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
    int timeUtilsEndTime = [[Utils getTimeFormat:[timeUtils getEndTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
    
    NSMutableArray *timeTemp = [[NSMutableArray alloc] init];
    for(int i = timeUtilsNowTime ; i <= timeUtilsEndTime ; i++) {
        [timeTemp addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    if(twoTap == 20) {
        // 19.02.13 KODEAWON - 로컬 디비 사용 안함
        //        for(NSString *time in timeTemp) {
        //            if([time intValue] == nowTime) {
        //                [self healthChartData:@"day" dayCount:24];
        //            }else {
        //                [self dbDataCheck];
        //            }
        //        }
        [self healthChartData:@"day" dayCount:24];
    }else {
        if(twoTap == 21){
            [self healthChartData:@"week" dayCount:7];
        }else {
            [self healthChartData:@"month" dayCount:(int)timeUtils.monthMaxDay];
        }
    }
}

- (void)healthChartData:(NSString *)timeType dayCount:(int)dayCount {
    if (self->timerProcess) {
        [self->timerProcess invalidate];
        self->timerProcess = nil;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self.caffeineDataStore healthKitSamplesWithCopletion:[[self->timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] endData:[[self->timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] :^(NSArray *samples, NSError *error) {
            if(samples.count> 0) {
                NSDateFormatter *dft = [[NSDateFormatter alloc] init];
                if([timeType isEqualToString:@"day"]) {
                    [dft setDateFormat:@"HH"];
                }else if([timeType isEqualToString:@"week"]){
                    [dft setDateFormat:@"yyyyMMdd"];
                }else {
                    [dft setDateFormat:@"dd"];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    int hourCount = 1;
                    if([timeType isEqualToString:@"day"]) {
                        hourCount = 1;
                    }else if([timeType isEqualToString:@"week"]) {
                        hourCount = [[Utils getTimeFormat:[self->timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
                    }else {
                        hourCount = 1;
                    }
                    
                    int tempStep = 0;
                    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
                    for(HKQuantitySample *sample in samples) {
                        NSLog(@"countUnit = %f, endDate = %@", [sample.quantity doubleValueForUnit:[HKUnit countUnit]], sample.endDate);
                        
                        while (hourCount < [[dft stringFromDate:sample.endDate] intValue]) {
                            NSLog(@"hourCount = %d : sample.endDate = %d",hourCount ,[[dft stringFromDate:sample.endDate] intValue]);
                            [tempArr addObject:@(tempStep)];
                            tempStep = 0;
                            if([timeType isEqualToString:@"week"]){
                                hourCount = [[Utils getNextTimeFormat:[NSString stringWithFormat: @"%d", hourCount] beTime:@"yyyyMMdd" afTime:@"yyyyMMdd"] intValue];
                            }else{
                                hourCount ++;
                            }
                        }
                        
                        if(hourCount == [[dft stringFromDate:sample.endDate] intValue]) {
                            NSString *step;
                            NSString *dataStep = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            if(self->oneTap == 10) {
                                step = [NSString stringWithFormat:@"%1.f", ([self->login.mber_bdwgh floatValue] * ([dataStep floatValue] / 10000.f) * 5.5f)];
                                /*[_login.mber_bdwgh floatValue] pp*/
                            }else {
                                step = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            }
                            self->static_total_step = [[NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]] intValue] + self->static_total_step;
                            self->static_total_cal = [[NSString stringWithFormat:@"%1.f", ([self->login.mber_bdwgh floatValue] * ([dataStep floatValue] / 10000.f) * 5.5f)] intValue] + self->static_total_cal;
                            /*[_login.mber_bdwgh floatValue] pp*/
                            tempStep = [step intValue] + tempStep;
                            NSLog(@"tempStep = %d", tempStep);
                        }
                        
                    }
                    [tempArr addObject:@(tempStep)];
                    
                    if([tempArr count] < dayCount) {
                        while ([tempArr count] < dayCount) {
                            [tempArr addObject:@(0)];
                        }
                    }
                    
                    self->barChartData = [tempArr copy];
                    
                    [self stepChartInit];
                    [self bottomInit];
                    
                    self->timerProcess = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(healthKitProcessEnd:) userInfo:nil repeats:NO];
                    
                });
            }else {
                [self performSelectorOnMainThread:@selector(healthNotCountMethod:) withObject:timeType waitUntilDone:YES];
            }
            
        }];
    });
}

- (void)healthKitProcessEnd:(id)sender {
    self->dayMutiTouch = false;
    [self->_model indicatorHidden];
}

- (void)healthKit {
    self.longTime = 0;
    [_lblBottom4SubNumber setNotoText:@"-"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self.caffeineDataStore healthKitSamplesWithCopletion:[[self->timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] endData:[[self->timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] :^(NSArray *samples, NSError *error) {
            if(samples.count> 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for(HKQuantitySample *sample in samples) {
                        NSLog(@"countUnit = %f, startDate = %@ endDate = %@", [sample.quantity doubleValueForUnit:[HKUnit countUnit]], sample.startDate, sample.endDate);
                        
                        NSTimeInterval nowTimeInter = [sample.startDate timeIntervalSince1970];
                        NSTimeInterval endTimeInter = [sample.endDate timeIntervalSince1970];
                        if((endTimeInter - nowTimeInter) >  self.longTime) {
                            self.longTime = endTimeInter - nowTimeInter;
                        }
                    }
                    
                    if(self.longTime == 0) {
                        [self.lblBottom4SubNumber setNotoText:@"-"];
                    }else {
                        [self.lblBottom4SubNumber setHealthBottom:[NSString stringWithFormat:@"%.1f", (self.longTime / 60.f)] unit:@"분" fontsize:17];
                    }
                });
            }
            
        }];
    });
}

#pragma mark - cal method
- (NSString *)calcClaorieCompare:(int)cal {
    if(cal == 0)
        return @"-";
    
    int r = arc4random() % (lifeFoodArr.count-1);
    
    NSString *foodCalorie = [lifeFoodCalorieArr objectAtIndex:r];
    NSString *value = [NSString stringWithFormat:@"%.1f", (cal / [foodCalorie floatValue])];
    
    return [NSString stringWithFormat:@"%@ %@ %@", lifeFoodArr[r], value, lifeFoodUnitArr[r]];
}

- (NSString *)calcSaveMoney:(float)distance {
    if(distance == 0) {
        return @"0";
    }
    
    int initDist = 2000;
    int initFee = 3800;
    int unitDist = 132;
    int unitDistFee = 100;
    
    if(distance <= initDist) {
        
        initFee = (initFee/100)*100;
        return [Utils getNumberComma:[NSString stringWithFormat:@"%d", initFee]];
    }
    
    distance -= initDist;
    float temp = 0.f;
    temp = (initFee + (distance /unitDist + 1) * unitDistFee);
    if (temp>0) {
        temp = (int)((int)temp/100)*100;
    }
    return [Utils getNumberComma:[NSString stringWithFormat:@"%1.f", temp]];
}

#pragma mark - graph Info text
- (void)setGraphInfoText:(NSString *)val totalVal:(NSString *)totalVal unit:(NSString *)unit {
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *valStr = [[NSMutableAttributedString alloc] initWithString:val];
    [valStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, valStr.length)];
    [valStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_MEDIUM(18)
                     range:NSMakeRange(0, valStr.length)];
    [valStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_ORANGE
                     range:NSMakeRange(0, valStr.length)];
    [attstr appendAttributedString:valStr];
    
    NSMutableAttributedString *valUnitStr = [[NSMutableAttributedString alloc] initWithString:unit];
    [valUnitStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, valUnitStr.length)];
    [valUnitStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_THIN(18)
                   range:NSMakeRange(0, valUnitStr.length)];
    [valUnitStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_ORANGE
                   range:NSMakeRange(0, valUnitStr.length)];
    [attstr appendAttributedString:valUnitStr];
    
    NSMutableAttributedString *middleStr = [[NSMutableAttributedString alloc] initWithString:@"/"];
    [middleStr addAttribute:NSFontAttributeName
                       value:FONT_NOTO_MEDIUM(18)
                       range:NSMakeRange(0, @"/".length)];
    [middleStr addAttribute:NSForegroundColorAttributeName
                       value:RGB(143, 144, 158)
                       range:NSMakeRange(0, @"/".length)];
    [attstr appendAttributedString:middleStr];
    
    NSMutableAttributedString *totalValStr = [[NSMutableAttributedString alloc] initWithString:totalVal];
    [totalValStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, totalValStr.length)];
    [totalValStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_MEDIUM(18)
                   range:NSMakeRange(0, totalValStr.length)];
    [totalValStr addAttribute:NSForegroundColorAttributeName
                   value:RGB(143, 144, 158)
                   range:NSMakeRange(0, totalValStr.length)];
    [attstr appendAttributedString:totalValStr];
    
    NSMutableAttributedString *totalValUnitStr = [[NSMutableAttributedString alloc] initWithString:unit];
    [totalValUnitStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, totalValUnitStr.length)];
    [totalValUnitStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_THIN(18)
                      range:NSMakeRange(0, totalValUnitStr.length)];
    [totalValUnitStr addAttribute:NSForegroundColorAttributeName
                      value:RGB(143, 144, 158)
                      range:NSMakeRange(0, totalValUnitStr.length)];
    [attstr appendAttributedString:totalValUnitStr];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    [attstr addAttribute:NSParagraphStyleAttributeName
                   value:style
                   range:NSMakeRange(0, attstr.length)];
    
    _graphInfoLbl.attributedText = attstr;
}

@end
