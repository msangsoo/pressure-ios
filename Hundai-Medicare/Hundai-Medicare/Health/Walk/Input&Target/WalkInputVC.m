//
//  WalkInputVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WalkInputVC.h"

@interface WalkInputVC () {
    int pageNumber;
    int maxpageNumber;
    bool scrollbottom;
    
    NSDateFormatter *format;
    
    NSMutableArray *items;
    int selectNum;
    
    NSArray *mvm_info_sport_list;
    NSDictionary *sportItem;
}

@end

@implementation WalkInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM03014 s_cod:HM03014001];
    
    [self.navigationItem setTitle:@"운동 입력"];
    
    db = [[DataCalroieDBWraper alloc] init];
    
    format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy.MM.dd"];
    
    items = [[NSMutableArray alloc] init];
    selectNum = 0;
    mvm_info_sport_list = [[NSArray alloc] init];
    sportItem = [[NSDictionary alloc] init];
    
    pageNumber = 1;
    maxpageNumber = 0;
    scrollbottom = false;
    
    [self viewInit];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self apiList];
}

- (void)viewWillAppear:(BOOL)animated {
    
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_info_sport_list"]) {
        mvm_info_sport_list = [result objectForKey:@"active_list"];
        pageNumber = 1;
        items = [[NSMutableArray alloc] init];
        [self apiSaveList];
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_info_input_data_sport"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            [db insert:[result objectForKey:@"sport_list"]];
            pageNumber = 1;
            items = [[NSMutableArray alloc] init];
            [self apiSaveList];
        }else if([[result objectForKey:@"reg_yn"] isEqualToString:@"YN"]) {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"같은 시간대에 입력한 운동이 있습니다.\n운동 시간을 확인해주세요." left:@"확인" right:@""];
        }else {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"다시 시도해 주세요." left:@"확인" right:@""];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_info_mber_sport_list"]) {
        maxpageNumber = [[result objectForKey:@"maxpageNumber"] intValue];
        pageNumber ++;
        
        scrollbottom = true;
        if (pageNumber > maxpageNumber) {
            scrollbottom = false;
        }
        
        for (NSDictionary *dic in [result objectForKey:@"sport_list"]) {
            [items addObject:dic];
        }
        [self.tableView reloadData];
        
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_info_sport_del"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            NSDictionary *item = items[selectNum];
            [db DeleteDb:[item objectForKey:@"active_seq"]];
            [items removeObjectAtIndex:selectNum];
            [self.tableView reloadData];
        }else {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"다시 시도해 주세요." left:@"확인" right:@""];
        }
    }
}

#pragma mark - table delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"WalkInputCell";
    WalkInputCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"WalkInputCell" bundle:nil] forCellReuseIdentifier:@"WalkInputCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSDictionary *item = [items objectAtIndex:indexPath.row];
    
    CGFloat number = -1.5f;
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    NSMutableAttributedString *sportStr = [[NSMutableAttributedString alloc] initWithString:[item objectForKey:@"sport_nm"]];
    [sportStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, sportStr.length)];
    [sportStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_MEDIUM(18)
                     range:NSMakeRange(0, sportStr.length)];
    [sportStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_NATIONS_BLUE
                     range:NSMakeRange(0, sportStr.length)];
    [attstr appendAttributedString:sportStr];
    
    NSMutableAttributedString *timeStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@분", [item objectForKey:@"active_time"]]];
    [timeStr addAttribute:NSKernAttributeName
                          value:[NSNumber numberWithFloat:number]
                          range:NSMakeRange(0, timeStr.length)];
    [timeStr addAttribute:NSFontAttributeName
                          value:FONT_NOTO_LIGHT(18)
                          range:NSMakeRange(0, timeStr.length)];
    [timeStr addAttribute:NSForegroundColorAttributeName
                          value:COLOR_NATIONS_BLUE
                          range:NSMakeRange(0, timeStr.length)];
    [attstr appendAttributedString:timeStr];
    
    cell.kindLbl.attributedText = attstr;
    
    [cell.dateLbl setNotoText:[NSString stringWithFormat:@"%@ ", [item objectForKey:@"active_de_stime"]]];
    [cell.kcalLbl setNotoText:[NSString stringWithFormat:@"%@ kcal", [item objectForKey:@"calory"]]];
    
    return cell;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WalkInputCell *cell = (WalkInputCell *)[tableView cellForRowAtIndexPath:indexPath];
    CGFloat height = cell.frame.size.height;
    selectNum = (int)indexPath.row;
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"       "  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        NSLog(@"deleteAction index = %ld", indexPath.row);
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"삭제하시겠습니까?" left:@"취소" right:@"확인"];
    }];
    
    deleteAction.backgroundColor = RGB(143, 143, 143);
    UIImage *backgroundImage = [self cellImage:height imageName:@"btn_swipe_del.png"];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [cell.arrowImgVw setImage:[UIImage imageNamed:@"btn_light_blue_next"]];
    
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath {
    WalkInputCell *cell = (WalkInputCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell.arrowImgVw setImage:[UIImage imageNamed:@"btn_light_blue_before"]];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    if(180.f > (h - y) && scrollbottom) {
        scrollbottom = false;
        [self apiSaveList];
    }
}

#pragma mark - Actions
- (IBAction)pressKind:(id)sender {
    WalkInputKindAlertVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WalkInputKindAlertVC"];
    vc.items = mvm_info_sport_list;
    vc.delegate = self;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressAdd:(id)sender {
    [LogDB insertDb:HM01 m_cod:HM03015 s_cod:HM03015001];
    
    if (sportItem.count > 0) {
//        if (items.count > 0) {
//            BOOL timeOverCheck = false;
//            int startVal = [[_startValLbl.text stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
//            int endVal = [[_endValLbl.text stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
//
//            for (NSDictionary *item in items) {
//                NSString *stime = [item objectForKey:@"active_de_stime"];
//                stime = [stime stringByReplacingOccurrencesOfString:@":" withString:@""];
//                stime = [stime stringByReplacingOccurrencesOfString:@" " withString:@""];
//                NSString *etime = [item objectForKey:@"active_de_etime"];
//                etime = [etime stringByReplacingOccurrencesOfString:@":" withString:@""];
//                etime = [etime stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//                int stimeVal = [stime intValue];
//                int etimeVal = [etime intValue];
//
//                if (stimeVal <= startVal && startVal <= etimeVal) {
//                    timeOverCheck = true;
//                }
//                if (stimeVal <= endVal && endVal <= etimeVal) {
//                    timeOverCheck = true;
//                }
//            }
            
//            if (timeOverCheck) {
//                [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"운동시간이 중복되었습니다." left:@"확인" right:@""];
//                return;
//            }
//        }
        
        [self.tableView setScrollsToTop:true];
        [self apiInput:self->sportItem];
        
    }else {
        [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"운동을 선택해주세요." left:@"확인" right:@""];
    }
}

- (IBAction)pressStart:(id)sender {
    [AlertUtils DateControlShow:self dateType:Time tag:1001 selectDate:_startValLbl.text];
}

- (IBAction)pressEnd:(id)sender {
    [AlertUtils DateControlShow:self dateType:Time tag:1002 selectDate:_endValLbl.text];
}

#pragma mark - DateControlVCDelegate
- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    if(alertView.view.tag == 1001) {
        int startTime = [[date stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
        int endTime = [[_endValLbl.text stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
        
        if (startTime >= endTime) {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"시작시간이 종료시간보다 크거나 같을 수 없습니다." left:@"확인" right:@""];
            return;
        }else {
            [_startValLbl setNotoText:[NSString stringWithFormat:@"%@", date]];
        }
        
    }else if(alertView.view.tag == 1002) {
        int startTime = [[_startValLbl.text stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
        int endTime = [[date stringByReplacingOccurrencesOfString:@":" withString:@""] intValue];
        
        int nowTime = [[TimeUtils getNowDateFormat:@"HHmm"] intValue];
        
        if (nowTime < endTime) {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"미래시간을 입력할 수 없습니다." left:@"확인" right:@""];
            return;
        }else if (startTime >= endTime) {
            [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"종료시간이 시작시간보다 작거나 같을 수 없습니다." left:@"확인" right:@""];
            return;
        }else {
            [_endValLbl setNotoText:[NSString stringWithFormat:@"%@", date]];
        }
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}


#pragma mark - WalkInputKindAlertVCDelegate
- (void)WalkInputKindAlert:(WalkInputKindAlertVC *)alertView item:(NSDictionary *)item {
    sportItem = item;
    [_kindLbl setNotoText:[item objectForKey:@"active_nm"]];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 100) {
        if (tag == 2) {
            [self apiDelete:items[selectNum]];
        }
    }
}

#pragma mark - api
- (void)apiList {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_info_sport_list", @"api_code",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiInput:(NSDictionary *)item {
    
    [format setDateFormat:@"yyyyMMdd"];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_info_input_data_sport", @"api_code",
                                item[@"active_seq"], @"active_seq",
                                item[@"mets"], @"mets",
                                [format stringFromDate:self.nowDate], @"active_de",
                                item[@"active_nm"], @"active_nm",
                                _startValLbl.text, @"active_de_stime",
                                _endValLbl.text, @"active_de_etime",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiSaveList {
    
    [format setDateFormat:@"yyyyMMdd"];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_info_mber_sport_list", @"api_code",
                                [NSString stringWithFormat:@"%d", pageNumber], @"pageNumber",
                                [format stringFromDate:self.nowDate], @"input_de",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

- (void)apiDelete:(NSDictionary *)item {
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"mvm_info_sport_del", @"api_code",
                                item[@"sport_sn"], @"sport_sn",
                                item[@"active_seq"], @"active_seq",
                                item[@"reg_de"], @"active_de",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _kindLbl.font = FONT_NOTO_REGULAR(18);
    _kindLbl.textColor = COLOR_GRAY_SUIT;
    [_kindLbl setNotoText:@"운동선택"];
    
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"날짜"];
    
    _dateValLbl.font = FONT_NOTO_REGULAR(18);
    _dateValLbl.textColor = COLOR_GRAY_SUIT;
    [format setDateFormat:@"yyyy.MM.dd"];
    [_dateValLbl setNotoText:[format stringFromDate:self.nowDate]];
    
    _startLbl.font = FONT_NOTO_REGULAR(18);
    _startLbl.textColor = COLOR_MAIN_DARK;
    [_startLbl setNotoText:@"시작 시간"];
    
    _startValLbl.font = FONT_NOTO_REGULAR(18);
    _startValLbl.textColor = COLOR_NATIONS_BLUE;
    [format setDateFormat:@"HH:mm"];
    [_startValLbl setNotoText:[format stringFromDate:[[NSDate date] dateByAddingTimeInterval:-1 * 60 * 60]]];
    
    _endLbl.font = FONT_NOTO_REGULAR(18);
    _endLbl.textColor = COLOR_MAIN_DARK;
    [_endLbl setNotoText:@"종료 시간"];
    
    _endValLbl.font = FONT_NOTO_REGULAR(18);
    _endValLbl.textColor = COLOR_NATIONS_BLUE;
    [format setDateFormat:@"HH:mm"];
    [_endValLbl setNotoText:[format stringFromDate:[NSDate date]]];
    
    [_addBtn setBackgroundColor:COLOR_NATIONS_BLUE];
    [_addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

#pragma mark - image size mthod
- (UIImage *)cellImage:(CGFloat)height imageName:(NSString *)imgname {
    CGRect frame = CGRectMake(0, 0, 82, height);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(82, height), NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if([imgname isEqualToString:@"btn_swipe_edit.png"]) {
        CGContextSetFillColorWithColor(context, COLOR_NATIONS_BLUE.CGColor);
    }else{
        CGContextSetFillColorWithColor(context, RGB(143, 143, 143).CGColor);
    }
    
    CGContextFillRect(context, frame);
    UIImage *image = [UIImage imageNamed:imgname];
    [image drawInRect:CGRectMake(62/2.5, frame.size.height/6.0, 62/3.0, frame.size.height/6.0 * 4)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end


#pragma mark - Cell
@implementation WalkInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _dateLbl.font = FONT_NOTO_THIN(14);
    _dateLbl.textColor = COLOR_GRAY_SUIT;
    _dateLbl.text = @"";
    
    _kindLbl.font = FONT_NOTO_MEDIUM(18);
    _kindLbl.textColor = COLOR_NATIONS_BLUE;
    _kindLbl.text = @"";
    
    _kcalLbl.font = FONT_NOTO_REGULAR(18);
    _kcalLbl.textColor = COLOR_NATIONS_BLUE;
    _kcalLbl.text = @"";
    
    _kcalLbl.font = FONT_NOTO_LIGHT(18);
    _kcalLbl.textColor = COLOR_NATIONS_BLUE;
    _kcalLbl.text = @"";
    
    _arrowImgVw.image = [UIImage imageNamed:@"btn_light_blue_before"];
}

@end
