//
//  WalkTargetVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface WalkTargetVC : BaseViewController<UITextFieldDelegate, ServerCommunicationDelegate, BasicAlertVCDelegate>

@property (weak, nonatomic) IBOutlet UILabel *kcalLbl;
@property (weak, nonatomic) IBOutlet UIButton *kcalBtn;
@property (weak, nonatomic) IBOutlet UITextField *kcalTf;

@property (weak, nonatomic) IBOutlet UILabel *stepLbl;
@property (weak, nonatomic) IBOutlet UIButton *stepBtn;
@property (weak, nonatomic) IBOutlet UITextField *stepTf;

@end
