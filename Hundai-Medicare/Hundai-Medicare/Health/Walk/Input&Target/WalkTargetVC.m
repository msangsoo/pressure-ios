//
//  WalkTargetVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WalkTargetVC.h"

@interface WalkTargetVC () {
    Tr_login *login;
}

@end

@implementation WalkTargetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM01 m_cod:HM03016 s_cod:HM03016001];
    
    [self.navigationItem setTitle:@"목표 수정"];
    
    login = [_model getLoginData];
    
    [self navigationSetup];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"mvm_goalqy"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            login.goal_mvm_calory  = [NSString stringWithFormat:@"%@", _kcalTf.text];
            login.goal_mvm_stepcnt = [NSString stringWithFormat:@"%@", _stepTf.text];

//            [AlertUtils BasicAlertShow:self tag:1000 title:@"" msg:@"수정되었습니다." left:@"확인" right:@""];
            [self.navigationController popViewControllerAnimated:true];
        }else {
            [AlertUtils BasicAlertShow:self tag:1001 title:@"" msg:@"수정에 실패 하였습니다." left:@"확인" right:@""];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressSave:(UIButton *)sender {
    if(_kcalTf.text.length <= 0) {
        [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"하루 소모 칼로리를 정확히 입력해 주세요." left:@"확인" right:@""];
        return;
    }
    if([_kcalTf.text intValue] < 150 || [_kcalTf.text intValue] > 1000 ) {
        [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"하루 소모 칼로리는 최소150Kcal 이고 최대1,000Kcal 입니다." left:@"확인" right:@""];
        return;
    }
    if(_stepTf.text.length <= 0 || [_stepTf.text intValue] <= 0 || [_stepTf.text intValue] > 500000 ) {
        [AlertUtils BasicAlertShow:self tag:1 title:@"" msg:@"하루 목표 걸음수를 정확히 입력해 주세요." left:@"확인" right:@""];
        return;
    }
    
    [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"하루목표 칼로리 및 목표걸음수를 수정 하시겠습니까?" left:@"취소" right:@"확인"];
}

- (IBAction)pressInputType:(UIButton *)sender {
    if (sender.tag == 1) {
        _kcalBtn.selected = true;
        _stepBtn.selected = false;
        
        _kcalTf.enabled = true;
        _stepTf.enabled = false;
        [_kcalTf becomeFirstResponder];
    }else {
        _kcalBtn.selected = false;
        _stepBtn.selected = true;
        
        _kcalTf.enabled = false;
        _stepTf.enabled = true;
        [_stepTf becomeFirstResponder];
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:true];
    }else if(alertView.view.tag == 100) {
        if(tag == 2) {
            [_model indicatorActive];
            server.delegate = self;
            
            NSString *goal_calory = _kcalTf.text.length > 0 ? _kcalTf.text : login.goal_mvm_calory;
            NSString *goal_stepcnt = _stepTf.text.length > 0 ? _stepTf.text : login.goal_mvm_stepcnt;
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"mvm_goalqy", @"api_code",
                                        goal_calory, @"goal_mvm_calory",
                                        goal_stepcnt, @"goal_mvm_stepcnt",
                                        nil];
            
            [server serverCommunicationData:parameters];
        }
    }
}

#pragma mark - textfield delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if([_kcalTf isFirstResponder]) {
        [_kcalTf resignFirstResponder];
    }else if([_stepTf isFirstResponder]) {
        [_stepTf resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField == _kcalTf) {
        [_kcalTf resignFirstResponder];
    }else if(textField == _stepTf) {
        [_stepTf resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == _kcalTf) {
        if(range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        if(range.location == 6 && range.length == 0){
            return NO;
        }
    }else if(textField == _stepTf) {
        if(range.location == 0 && [string isEqualToString:@"0"]){
            return NO;
        }
        
        if(range.location == 9 && range.length == 0){
            return NO;
        }
    }
    
    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    
    int sex = [login.mber_sex intValue];
    
    float weight = [login.mber_bdwgh_app intValue];
    if (weight==0) {
        weight = [login.mber_bdwgh intValue];
    }
    
    float height = [login.mber_height floatValue];
    int sInt = [textField.text intValue];
    
    if(textField == _kcalTf) {
        [_stepTf setText:[self getStepTarget:sex height:height weight:weight calrori:sInt]];
    }else if(textField == _stepTf) {
        [_kcalTf setText:[self getCalroriTarget:sex height:height weight:weight step:sInt]];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _kcalLbl.font = FONT_NOTO_REGULAR(18);
    _kcalLbl.textColor = COLOR_MAIN_DARK;
    [_kcalLbl setNotoText:@"하루 소모 칼로리"];
    
    _kcalTf.font = FONT_NOTO_MEDIUM(18);
    _kcalTf.textColor = COLOR_NATIONS_BLUE;
    [_kcalTf setPlaceholder:@"300"];
    _kcalTf.delegate = self;
    
    _stepLbl.font = FONT_NOTO_REGULAR(18);
    _stepLbl.textColor = COLOR_MAIN_DARK;
    [_stepLbl setNotoText:@"하루 목표 걸음 수"];
    
    _stepTf.font = FONT_NOTO_MEDIUM((18));
    _stepTf.textColor = COLOR_NATIONS_BLUE;
    [_stepTf setPlaceholder:@"7321"];
    _stepTf.delegate = self;
    
    _kcalBtn.tag = 1;
    _stepBtn.tag = 2;
    
    _kcalBtn.selected = true;
    _kcalTf.enabled = true;
    _stepTf.enabled = false;
    [_kcalTf becomeFirstResponder];
    
    int sex = [login.mber_sex intValue];
    float weight = [login.mber_bdwgh_app intValue];
    if (weight ==0 ) {
        weight = [login.mber_bdwgh intValue];
    }
    float height = [login.mber_height floatValue];
    
    int cal = [login.goal_mvm_calory floatValue];
    
    NSString *gStep = [_model getStepTargetCalulator:sex height:height weight:weight calrori:cal];
    
    [_kcalTf setPlaceholder:login.goal_mvm_calory];
    [_stepTf setPlaceholder:gStep];
    
    [_kcalTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_stepTf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"저장"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressSave:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

#pragma mark - step / calroi target method
- (NSString *)getStepTarget:(int)sex height:(float)height weight:(float)weight calrori:(int)calrori {
    NSString *rtnValue = @"";
    double stepTarget = 0.0f;
    if (sex == 1) {
        stepTarget = ((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60 / (height / 100 / (-0.5625 * 3.5 + 4.2294))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
    } else {
        stepTarget = ((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60 / (height / 100 / (-0.5133 * 3.5 + 4.1147))) * calrori / (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5);
    }
    rtnValue = [NSString stringWithFormat:@"%0.f", stepTarget];
    
    if (height==0 || weight==0){
        return @"0";
    }else{
        return rtnValue;
    }
}

- (NSString *)getCalroriTarget:(int)sex height:(float)height weight:(float)weight step:(int)step {
    NSString *rtnValue = @"";
    double calroriTarget = 0.0f;
    
    if (sex == 1) {
        calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5625 * 3.5 + 4.2294) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5625 * 3.5 + 4.2294)) * 3.5) / 2) * 60) / (height / 100 / (-0.5625 * 3.5 + 4.2294)));
    } else {
        calroriTarget = (step * (((3.5 + 0.1 * (height / 100 / (-0.5133 * 3.5 + 4.1147) * 3.5 / 2 * 60)) * weight) / 1000 * 5)) / (((((height / 100 / (-0.5133 * 3.5 + 4.1147)) * 3.5) / 2) * 60) / (height / 100 / (-0.5133 * 3.5 + 4.1147)));
    }
    rtnValue = [NSString stringWithFormat:@"%0.f", calroriTarget];
    
    if (height==0 || weight==0){
        return @"0";
    }else{
        return rtnValue;
    }
}

@end
