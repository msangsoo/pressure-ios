//
//  WalkInputKindAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalkInputKindAlertVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;


@end

@protocol WalkInputKindAlertVCDelegate
- (void)WalkInputKindAlert:(WalkInputKindAlertVC *)alertView item:(NSDictionary *)item;
@end

#pragma mark - Cell
@interface WalkInputKindAlertCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *kindLbl;

@end
