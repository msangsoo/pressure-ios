//
//  WalkInputVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "WalkInputKindAlertVC.h"

#import "DataCalroieDBWraper.h"

@interface WalkInputVC : BaseViewController<UITableViewDelegate, UITableViewDataSource, ServerCommunicationDelegate, BasicAlertVCDelegate, WalkInputKindAlertVCDelegate, DateControlVCDelegate> {
    DataCalroieDBWraper *db;
}

@property (nonatomic, strong) NSDate *nowDate;

@property (weak, nonatomic) IBOutlet UILabel *kindLbl;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateValLbl;
@property (weak, nonatomic) IBOutlet UILabel *startLbl;
@property (weak, nonatomic) IBOutlet UILabel *startValLbl;
@property (weak, nonatomic) IBOutlet UILabel *endLbl;
@property (weak, nonatomic) IBOutlet UILabel *endValLbl;

@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


#pragma mark - Cell
@interface WalkInputCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *kindLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalLbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImgVw;

@end
