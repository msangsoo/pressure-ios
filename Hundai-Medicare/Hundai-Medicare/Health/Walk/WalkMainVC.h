//
//  WalkMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "WalkGraphZoomVC.h"
#import "TimeUtils.h"
#import "WalkDBWraper.h"
#import "WalkInputVC.h"
#import "DataCalroieDBWraper.h"

#import "FeedModel.h"
#import "CommunityWriteViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"

//바차트관련
#import "SBCEViewController.h"

@interface WalkMainVC : BaseViewController<BasicAlertVCDelegate> {
    TimeUtils *timeUtils;
    WalkDBWraper *db;
    DataCalroieDBWraper *calDb;
    NSString *unit;
    
    NSTimer *timerProcess;
}

///GOOD AND GOOD
@property (assign) NSTimeInterval longTime;

@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UIButton *kcalBtn;
@property (weak, nonatomic) IBOutlet UIButton *walkBtn;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphRightLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphUnitLbl;
@property (weak, nonatomic) IBOutlet UIButton *inputBtn;

/* Botoom */
@property (weak, nonatomic) IBOutlet UIView *bottomVw;

@property (weak, nonatomic) IBOutlet UILabel *lblBottom1SubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom1SubNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom2SubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom2SubNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom3SubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom3SubNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom4SubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom4SubNumber;

@property (weak, nonatomic) IBOutlet UIImageView *ImgVwBottom1;
@property (weak, nonatomic) IBOutlet UIImageView *ImgVwBottom2;
@property (weak, nonatomic) IBOutlet UIImageView *ImgVwBottom3;
@property (weak, nonatomic) IBOutlet UIImageView *ImgVwBottom4;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphTypeWidthConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphInfoVwTopConst;

- (void)viewSetup;

@end

