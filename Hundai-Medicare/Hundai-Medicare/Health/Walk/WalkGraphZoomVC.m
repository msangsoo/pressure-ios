//
//  WalkGraphZoomVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "WalkGraphZoomVC.h"

#import "CaffeineDataStore.h"

@interface WalkGraphZoomVC () {
    
    NSArray *barChartData;
    
    
    int Goal_cal;
    int Goal_step;
    int dayData;
    NSTimeInterval longTime;
    
    Tr_login *login;
}

@property (strong, nonatomic) CaffeineDataStore *caffeineDataStore;

@end

@implementation WalkGraphZoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    login = [_model getLoginData];
    
    longTime = 0;
    Goal_cal = 0;
    Goal_step = 0;
    dayData = 1;
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    self.caffeineDataStore = [[CaffeineDataStore alloc] initWithHealthStore:healthStore];
    
    db = [[WalkDBWraper alloc] init];
    calDb = [[DataCalroieDBWraper alloc] init];
    
    if (self.twoTap == 0) {
        self.twoTap = 20;
    }
    
    [self viewInit];
    
    
   
    self.imageRotate.alpha = 0.3;
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.imageRotate.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0 delay:0.7 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.imageRotate.alpha = 0;
            } completion:^(BOOL finished) {
                self.imageRotate.hidden = YES;
            }];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [self viewSetup];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}


#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self.caffeineDataStore fetchCaffieneSamplesWithCompletion:^(NSArray *samples, NSError *error) {
        if(samples.count > 0) {
            NSLog(@"step = %@", samples);
            [self performSelectorOnMainThread:@selector(getHealthAccessCheck) withObject:self waitUntilDone:YES];
        }else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"앱 접근권한" message:@"건강앱 접근권한이 설정되지 않았습니다.\n설정 > 개인정보보호 > 건강 > 현대해상 메디케어" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"설정" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                NSURL *url = [NSURL URLWithString: UIApplicationOpenSettingsURLString];
                [UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];

            }];
            UIAlertAction *canAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            }];
            [alert addAction:canAction];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    
    [self timeButtonInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(self.twoTap == 20) {
        [_dateLbl setNotoText:startTime];
    }else if(self.twoTap == 21) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (self.twoTap == 22) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _graphLeftLbl.font = FONT_NOTO_REGULAR(16);
    _graphLeftLbl.textColor = COLOR_MAIN_DARK;
    [_graphLeftLbl setNotoText:@"걸음칼로리"];
    
    _graphRightLbl.font = FONT_NOTO_REGULAR(16);
    _graphRightLbl.textColor = COLOR_MAIN_DARK;
    [_graphRightLbl setNotoText:@"운동칼로리"];
    
    self.unitLbl.text = self.unit;
    
    UIImage *closeImg = [[UIImage imageNamed:@"btn_circle_gray_cancel"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:closeImg forState:UIControlStateNormal];
    [self.closeBtn setTintColor:COLOR_GRAY_SUIT];
    
    timeUtils = self.timeUtilsData;
    
    if (self.oneTap == 11) {
        self.infoVw.hidden = true;
        self.infoHeightConst.constant = 0;
    }
}

#pragma mark - health kit
- (void)getHealthAccessCheck {
    //목표칼로리, 목표걸음수
    Goal_step = [login.goal_mvm_stepcnt intValue];
    Goal_cal = [login.goal_mvm_calory intValue];
    
    if (Goal_cal==0) {
        Goal_cal  = [_model getDefaultGoalCalori];
    }
//
//    int sex = [login.mber_sex intValue];
//    float height = [login.mber_height floatValue];
    float weight = [login.mber_bdwgh_app intValue];
    
    if (weight==0) {
        weight = [login.mber_bdwgh intValue];
    }
    
    //    Goal_step = [[_model getStepTargetCalulator:sex height:height weight:weight calrori:(int)Goal_cal] longLongValue];
    
    [self timeButtonInit];
    
    //    int nowTime = [[Utils getNowDate:@"yyyyMMdd"] intValue];
    int timeUtilsNowTime = [[Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
    int timeUtilsEndTime = [[Utils getTimeFormat:[timeUtils getEndTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
    
    NSMutableArray *timeTemp = [[NSMutableArray alloc] init];
    for(int i = timeUtilsNowTime ; i <= timeUtilsEndTime ; i++) {
        [timeTemp addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    if(self.twoTap == 20) {
        [self healthChartData:@"day" dayCount:24];
    }else {
        if(self.twoTap == 21){
            [self healthChartData:@"week" dayCount:7];
        }else {
            [self healthChartData:@"month" dayCount:(int)timeUtils.monthMaxDay];
        }
    }
}

- (void)healthChartData:(NSString *)timeType dayCount:(int)dayCount {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self.caffeineDataStore healthKitSamplesWithCopletion:[[self->timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] endData:[[self->timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@""] :^(NSArray *samples, NSError *error) {
            if(samples.count> 0) {
                NSDateFormatter *dft = [[NSDateFormatter alloc] init];
                if([timeType isEqualToString:@"day"]) {
                    [dft setDateFormat:@"HH"];
                }else if([timeType isEqualToString:@"week"]){
                    [dft setDateFormat:@"yyyyMMdd"];
                }else {
                    [dft setDateFormat:@"dd"];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    int hourCount = 1;
                    if([timeType isEqualToString:@"day"]) {
                        hourCount = 1;
                    }else if([timeType isEqualToString:@"week"]) {
                        hourCount = [[Utils getTimeFormat:[self->timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyyMMdd"] intValue];
                    }else {
                        hourCount = 1;
                    }
                    
                    int tempStep = 0;
                    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
                    for(HKQuantitySample *sample in samples) {
                        NSLog(@"countUnit = %f, endDate = %@", [sample.quantity doubleValueForUnit:[HKUnit countUnit]], sample.endDate);
                        
                        while (hourCount < [[dft stringFromDate:sample.endDate] intValue]) {
                            NSLog(@"hourCount = %d : sample.endDate = %d",hourCount ,[[dft stringFromDate:sample.endDate] intValue]);
                            [tempArr addObject:@(tempStep)];
                            tempStep = 0;
                            if([timeType isEqualToString:@"week"]){
                                hourCount = [[Utils getNextTimeFormat:[NSString stringWithFormat: @"%d", hourCount] beTime:@"yyyyMMdd" afTime:@"yyyyMMdd"] intValue];
                            }else{
                                hourCount ++;
                            }
                        }
                        
                        if(hourCount == [[dft stringFromDate:sample.endDate] intValue]) {
                            NSString *step;
                            NSString *dataStep = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            if(self.oneTap == 10) {
                                step = [NSString stringWithFormat:@"%1.f", ([self->login.mber_bdwgh floatValue] * ([dataStep floatValue] / 10000.f) * 5.5f)];
                                /*[_login.mber_bdwgh floatValue] pp*/
                            }else {
                                step = [NSString stringWithFormat:@"%1.f",[sample.quantity doubleValueForUnit:[HKUnit countUnit]]];
                            }
                            /*[_login.mber_bdwgh floatValue] pp*/
                            tempStep = [step intValue] + tempStep;
                            NSLog(@"tempStep = %d", tempStep);
                        }
                        
                    }
                    [tempArr addObject:@(tempStep)];
                    
                    if([tempArr count] < dayCount) {
                        while ([tempArr count] < dayCount) {
                            [tempArr addObject:@(0)];
                        }
                    }
                    
                    self->barChartData = [tempArr copy];
                    
                    [self stepChartInit];
                    [self->_model indicatorHidden];
                });
            }else {
                [self performSelectorOnMainThread:@selector(healthNotCountMethod:) withObject:timeType waitUntilDone:YES];
            }
            
        }];
    });
}

#pragma mark - chart init
- (void)healthNotCountMethod:(NSString *)timeType {
    if([timeType isEqualToString:@"day"]) {
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0)];
    }else if([timeType isEqualToString:@"week"]) {
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0),
                         @(0)];
    }else{
        barChartData = @[@(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0),
                         @(0)];
    }
    [self stepChartInit];
    
    [_model indicatorHidden];
}

- (void)stepChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    SBCEViewController *bar = [[SBCEViewController alloc] init];
    SimpleBarChart *barChart;
//    if(self.twoTap == 20) {
//        barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_DAY xCount:24];
//    }else if(self.twoTap == 21) {
//        barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_WEEK xCount:7];
//    }else if(self.twoTap == 22) {
//        NSLog(@"_timeUtils.monthMaxDay:%ld", (long)timeUtils.monthMaxDay);
//        barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
//                            items:barChartData chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
//    }
    
    if(self.twoTap == 20) {
        
        if (self.oneTap == 10) {
            //칼로리
            NSArray *arrCal = [[calDb getResultDay:[timeUtils getNowTime]] objectAtIndex:0];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_DAY xCount:24];
        }else{
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_DAY xCount:24];
        }
    }else if(self.twoTap == 21) {
        
        if (self.oneTap == 10) {
            //칼로리
            NSArray *arrCal = [[calDb getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime]] objectAtIndex:0];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_WEEK xCount:7];
        }else {
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_WEEK xCount:7];
        }
        
    }else if(self.twoTap == 22) {
        NSLog(@"_timeUtils.monthMaxDay:%ld", (long)timeUtils.monthMaxDay);
        
        if (self.oneTap == 10) {
            //칼로리
            NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
            NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
            
            NSArray *arrCal = [[calDb getResultMonth:year nMonth:month] objectAtIndex:0];
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData itemOther:arrCal chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
        }else{
            //걸음
            barChart = [bar loadChart:CGRectMake(5, 0, _graphVw.layer.frame.size.width - 10 , _graphVw.layer.frame.size.height)
                                items:barChartData chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
        }
    }
    
    [bar reloadData];
    barChart.unit = self.unit;
    barChart.tag = 1;
    [_graphVw addSubview:barChart];
}

#pragma mark - UIInterfaceOrientationMask
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)updatePreviewLayer:(AVCaptureConnection*)layer orientation:(AVCaptureVideoOrientation)orientation {
    layer.videoOrientation = orientation;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:false block:^(NSTimer * _Nonnull timer) {
        
        [self viewSetup];
    }];
}

@end
