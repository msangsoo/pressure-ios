//
//  HistoryCommonTableViewCell.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 11..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "HistoryCommonTableViewCell.h"

@implementation HistoryCommonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewinit];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _ImgArrow.image = [UIImage imageNamed:@"btn_light_blue_before"];
}

#pragma mark - viewInit
- (void)viewinit {
    _lblMediValue.font = FONT_NOTO_MEDIUM(18);
    _lblMediValue.textColor = COLOR_NATIONS_BLUE;
    _lblMediValue.text = @"";
    
    _lblDate.font = FONT_NOTO_LIGHT(14);
    _lblDate.textColor = COLOR_GRAY_SUIT;
    _lblDate.text = @"";
    
    _ImgArrow.image = [UIImage imageNamed:@"btn_light_blue_before"];
}


@end
