//
//  HistoryCommonTableViewCell.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 11..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCommonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblInputType;
@property (weak, nonatomic) IBOutlet UILabel *lblMediValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEatType;
@property (weak, nonatomic) IBOutlet UILabel *lblMediName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIImageView *ImgArrow;
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;

@end
