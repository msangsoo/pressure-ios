//
//  FoodInputTableViewCell.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 21..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodInputTableViewCell.h"

@implementation FoodInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

- (void)viewInit {
    _lblTitle.font = FONT_NOTO_MEDIUM(16);
    _lblTitle.textColor = COLOR_NATIONS_BLUE;
    _lblTitle.text = @"";
    
    _lblSubTitle.font = FONT_NOTO_LIGHT(14);
    _lblSubTitle.textColor = COLOR_GRAY_SUIT;
    _lblSubTitle.text = @"";
}

@end
