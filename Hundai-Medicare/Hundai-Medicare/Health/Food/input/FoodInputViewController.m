//
//  FoodInputViewController.m
//  LifeCare
//
//  Created by insystems company on 2017. 5. 26..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodInputViewController.h"

@interface FoodInputViewController ()

@end

@implementation FoodInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:self.titleName];
    
    _swifDic = [[NSMutableDictionary alloc] init];
    
    server = [[ServerCommunication alloc] init];
    [server setDelegate:self];
    
    _items = [[NSMutableArray alloc] init];
    _foodMainDB     = [[FoodMainDBWraper alloc] init];
    _foodDetailDB   = [[FoodDetailDBWraper alloc] init];
    
    login = [_model getLoginData];
    _regdate = [Utils getNowDate:@"yyyy-MM-dd HH:mm"];
    
    [self viewInit];
}

- (void)setMealType {
    NSLog(@"self._mealtype:%@", self._mealtype);
    
    txtDate.text = [NSString stringWithFormat:@"%@", [self._inputNDate stringByReplacingOccurrencesOfString:@"-" withString:@"."]];
    
    if ([self._mealtype isEqualToString:@"a"]) {
        txtTime.text = @"오전 08:00";
        nowTime = @"08:00";
//        [self.navigationItem setTitle:@"아침"];
        
    }else if ([self._mealtype isEqualToString:@"b"]) {
        txtTime.text = @"오후 12:00";
        nowTime = @"12:00";
//        [self.navigationItem setTitle:@"점심"];
        
    }else if ([self._mealtype isEqualToString:@"c"]) {
        txtTime.text = @"오후 06:00";
        nowTime = @"18:00";
//        [self.navigationItem setTitle:@"저녁"];
        
    }else if ([self._mealtype isEqualToString:@"d"]) {
        txtTime.text = @"오전 09:00";
        nowTime = @"09:00";
//        [self.navigationItem setTitle:@"아침 간식"];
        
    }else if ([self._mealtype isEqualToString:@"e"]) {
        txtTime.text = @"오후 02:00";
        nowTime = @"14:00";
//        [self.navigationItem setTitle:@"점심 간식"];

    }else if ([self._mealtype isEqualToString:@"f"]) {
        txtTime.text = @"오후 08:00";
        nowTime = @"20:00";
//        [self.navigationItem setTitle:@"저녁 간식"];

    }
}

#pragma mark - view touch
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if([txtAccountTime isFirstResponder]){
        [txtAccountTime resignFirstResponder];
    }
    [self inputDeActivi];
}
- (void)inputDeActivi {
    [self.view endEditing:YES];
    
    txtDate.layer.shadowColor = [[UIColor clearColor] CGColor];
    txtDate.layer.borderColor = RGBA(222, 222, 222, 1).CGColor;
    
    txtTime.layer.shadowColor = [[UIColor clearColor] CGColor];
    txtTime.layer.borderColor = RGBA(222, 222, 222, 1).CGColor;
    
}

#pragma mark - UITextViewDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
   
    [self inputDeActivi];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == txtAccountTime) {
        if (txtAccountTime.text.length > 2 && ![string isEqualToString:@""]) {
            return false;
        }
    }
    return true;
}

#pragma mark - FoodSearchViewControllerDelegate Call
- (void)forPeopleSelectDelegate:(NSDictionary*)dicData {
    BOOL check = true;
    
    if (_isItemEdit) {
        for(int i = 0 ; i < _items.count ; i++){
            int row = [[_swifDic objectForKey:@"row"] intValue];
            if([[[_items objectAtIndex:i] objectForKey:@"code"]
                isEqualToString:[dicData objectForKey:@"code"]]
               && row == i){
                
                [_items replaceObjectAtIndex:i withObject:dicData];
                check = false;
            }
        }
    }
    
    if(check){
        [_items addObject:dicData];
    }
    
    [self._tableView reloadData];
}

- (void)forPeopleSelectItems:(NSArray *)items {
    for (NSDictionary *dic in items) {
        [_items addObject:dic];
    }
    [self._tableView reloadData];
}

#pragma mark - table delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"FoodInputTableViewCell";
    FoodInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"FoodInputTableViewCell" bundle:nil] forCellReuseIdentifier:@"FoodInputTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSDictionary *cellData = [_items objectAtIndex:indexPath.row];
    
    [cell.lblTitle setText:[NSString stringWithFormat:@"%@", [cellData objectForKey:@"name"]]];
    [cell.lblSubTitle setText:[NSString stringWithFormat:@"%@인분(%@%@)m, %@kcal",
                                 [cellData objectForKey:@"forpeople"]
                               , [cellData objectForKey:@"gram"]
                               , [cellData objectForKey:@"unit"]
                               , [cellData objectForKey:@"calorie"]]];
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [view setBackgroundColor:[UIColor greenColor]];
    //add Buttons to view
    
    cell.editingAccessoryView = view;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"tableView select indexPath = %ld", indexPath.row);
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FoodInputTableViewCell *commentCell = (FoodInputTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    CGFloat height = commentCell.frame.size.height;
    selectNum = (int)indexPath.row;
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"       " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        //userinfo에 셋팅해야함
        NSLog(@"editAction index = %ld", indexPath.row);
        
        [self->_swifDic setDictionary:[self->_items objectAtIndex:indexPath.row]];
        [self->_swifDic setValuesForKeysWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)indexPath.row], @"row", nil]];
        
        [self performSegueWithIdentifier:@"FoodForPeopleViewController" sender:nil];
    }];
    editAction.backgroundColor = COLOR_NATIONS_BLUE;
    
    UIImage *backgroundImage = [self cellImage:height imageName:@"btn_swipe_edit.png"];
    editAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"       "  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        NSLog(@"deleteAction index = %ld", indexPath.row);
        
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"삭제하시겠습니까?" left:@"아니오" right:@"네"];
        
    }];
    deleteAction.backgroundColor = RGB(143, 143, 143);
    
    backgroundImage = [self cellImage:height imageName:@"btn_swipe_del.png"];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [commentCell.ImgArrow setImage:[UIImage imageNamed:@"btn_light_blue_next"]];
    
    return @[deleteAction,editAction];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath{
    FoodInputTableViewCell *commentCell = (FoodInputTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [commentCell.ImgArrow setImage:[UIImage imageNamed:@"btn_light_blue_before"]];
}


#pragma mark - image size mthod
- (UIImage *)cellImage:(CGFloat)height imageName:(NSString *)imgname {
    
    CGRect frame = CGRectMake(0, 0, 82, height);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(82, height), NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if([imgname isEqualToString:@"btn_swipe_edit.png"]) {
        CGContextSetFillColorWithColor(context, COLOR_NATIONS_BLUE.CGColor);
    }else{
        CGContextSetFillColorWithColor(context, RGB(143, 143, 143).CGColor);
    }
    
    CGContextFillRect(context, frame);
    
    UIImage *image = [UIImage imageNamed:imgname];
    
    [image drawInRect:CGRectMake(62/2.5, frame.size.height/6.0, 62/3.0, frame.size.height/6.0 * 4)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (IBAction)pressSearch:(id)sender {
    
    [self performSegueWithIdentifier:@"FoodSearchViewController" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FoodSearchViewController"]) {
        FoodSearchViewController *destinationView = (FoodSearchViewController *)[segue destinationViewController];
        destinationView.delegate = self;
        _isItemEdit = NO;
    }else if ([segue.identifier isEqualToString:@"FoodForPeopleViewController"]) {
        
        FoodForPeopleViewController *destinationView = (FoodForPeopleViewController *)[segue destinationViewController];
        destinationView.delegate = self;
        destinationView.providesPresentationContextTransitionStyle = YES;
        destinationView.definesPresentationContext = YES;
        [destinationView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        
        NSString *calorie       = [Utils getNoneZeroString:[[_swifDic objectForKey:@"calorie"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        NSString *gram          = [Utils getNoneZeroString:[[_swifDic objectForKey:@"gram"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        NSString *carbohydrate  = [Utils getNoneZeroString:[[_swifDic objectForKey:@"carbohydrate"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        NSString *protein       = [Utils getNoneZeroString:[[_swifDic objectForKey:@"protein"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        NSString *fat           = [Utils getNoneZeroString:[[_swifDic objectForKey:@"fat"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        NSString *salt          = [Utils getNoneZeroString:[[_swifDic objectForKey:@"salt"] floatValue] / [[_swifDic objectForKey:@"forpeople"] floatValue]];
        
        [_swifDic setObject:calorie forKey:@"calorie"];
        [_swifDic setObject:gram forKey:@"gram"];
        [_swifDic setObject:carbohydrate forKey:@"carbohydrate"];
        [_swifDic setObject:protein forKey:@"protein"];
        [_swifDic setObject:fat forKey:@"fat"];
        [_swifDic setObject:salt forKey:@"salt"];
        
        destinationView._dicData = _swifDic;
        
        _isItemEdit = YES;
    }
}

// 식사데이터 전송
- (IBAction)pressSave:(id)sender {
    
    _currentIdx = [Utils getNowDate:@"yyMMddHHmmssSS"];
    
    NSString *regdate = [NSString stringWithFormat:@"%@%@",
                         [Utils getConvertServertDate:txtDate.text],
                         [Utils getConvertServertTime:txtTime.text]];
    
    int calorie = 0;
    
    for(int i = 0 ; i < _items.count ; i++){
        calorie += [[[_items objectAtIndex:i] objectForKey:@"calorie"] intValue];
    }
    
    if([self.title isEqualToString:@"수정"]){
        
        _currentIdx = [_wirteDic objectForKey:@"idx"];
        
        _mainParam = [[NSDictionary alloc] initWithObjectsAndKeys:
                      @"meal_input_edit_data", @"api_code",
                      _currentIdx, @"idx",
//                      login.mber_sn, @"mber_sn",
                      self._mealtype, @"mealtype",
                      txtAccountTime.text, @"amounttime",
                      [NSString stringWithFormat:@"%d",calorie], @"calorie",
                      @"", @"picture",
                      regdate, @"regdate",
                      nil];
        
        
        
//        //식사기록 입력여부 확인
//        if(_items.count <=0){
//            [_model indicatorHidden];
//            [AlertUtils BasicAlertShow:self tag:99 title:@"" msg:@"음식을 기록해주세요." left:@"" right:@"확인"];
//            return;
//        }
        
        
        //현재시간보다 미래면 입력안되게 막음.
        long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
        long iDate = [[_mainParam objectForKey:@"regdate"] longLongValue];
        if (nDate < iDate) {
            [_model indicatorHidden];
            [AlertUtils BasicAlertShow:self tag:99 title:@"" msg:@"미래시간을 입력할 수 없습니다." left:@"확인" right:@""];
            return;
        }
        
        [self foodDateSend];
        
//        [server serverCommunicationData:_mainParam];
        
    }else{
        _mainParam = [[NSDictionary alloc] initWithObjectsAndKeys:
                      @"meal_input_data", @"api_code",
                      _currentIdx, @"idx",
//                      login.mber_sn, @"mber_sn",
                      self._mealtype, @"mealtype",
                      txtAccountTime.text, @"amounttime",
                      [NSString stringWithFormat:@"%d",calorie], @"calorie",
                      @"", @"picture",
                      regdate, @"regdate",
                      nil];
        
        //식사기록 입력여부 확인
        if(_items.count <= 0) {
            [_model indicatorHidden];
            [AlertUtils BasicAlertShow:self tag:99 title:@"" msg:@"음식을 기록해주세요." left:@"" right:@"확인"];
            return;
        }
        
        //현재시간보다 미래면 입력안되게 막음.
        long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
        long iDate = [[_mainParam objectForKey:@"regdate"] longLongValue];
        if (nDate < iDate) {
            [_model indicatorHidden];
            [AlertUtils BasicAlertShow:self tag:99 title:@"" msg:@"미래시간을 입력할 수 없습니다." left:@"확인" right:@""];
            return;
        }
        
        if ([_items count] > 0) {
            [self foodDateSend];
        }
        
//        [server serverCommunicationData:_mainParam];
    }
    
}

// 음식데이터 전송
- (void)foodDateSend {
    NSString *regdate = [Utils getFoodNumberDatetime:txtDate.text nTime:txtTime.text];
    NSMutableArray *food_mass = [[NSMutableArray alloc] init];
    
    if ([_items count] > 0) {
        for (int i=0; i < [_items count]; i++) {
            NSDictionary *dicData = [_items objectAtIndex:i];
            
            NSDictionary *dicServer = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [dicData objectForKey:@"code"], @"foodcode",
                                       [dicData objectForKey:@"forpeople"], @"forpeople",
                                       regdate, @"regdate",
                                       nil];
            [food_mass addObject:dicServer];
        }
    }else {
        NSDictionary *dicServer = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"", @"foodcode",
                                   @"", @"forpeople",
                                   @"", @"regdate",
                                   nil];
        [food_mass addObject:dicServer];
    }
    
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"meal_input_food_data", @"api_code",
                                _currentIdx, @"idx",
                                login.mber_sn, @"mber_sn",
                                food_mass, @"food_mass",
                                nil];
    
    [_foodMainDB insertFoodMainDb:_mainParam];
    [_foodDetailDB initFoodDetailDb:_items idx:_currentIdx regdate:[_mainParam objectForKey:@"regdate"]];
    
    [self.navigationController popViewControllerAnimated:true];
    
//    [server serverCommunicationData:parameters];
}

// 음식이미지 전송
- (void)foodPictureSend {
    [_foodMainDB insertFoodMainDb:_mainParam];
    [_foodDetailDB initFoodDetailDb:_items idx:_currentIdx regdate:[_mainParam objectForKey:@"regdate"]];
    
    if(selectedImage) {
        [server writeImage:selectedImage key:@"sn" sn:_currentIdx];
    }else {
        [self.navigationController popViewControllerAnimated:true];
    }
}


- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size {
    double newCropWidth, newCropHeight;
    
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    
    double x = image.size.width/5.0 - newCropWidth/2.0;
    double y = image.size.height/5.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    
    // 식사메인
    if ([[result objectForKey:@"api_code"] isEqualToString:@"meal_input_data"]) {
        if ([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"] ) {
            
            if ([_items count] > 0) {
                [self foodDateSend];
            }else {
                // 이미지 전달
                [self foodPictureSend];
                
                [Utils setUserDefault:HEALTH_POINT_ALERT value:@"3"];
//                [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"등록되었습니다." left:@"확인" right:@""];
            }
        }
    }else if ([[result objectForKey:@"api_code"] isEqualToString:@"meal_input_food_data"]) {
        
        [self foodPictureSend];
        
        [Utils setUserDefault:HEALTH_POINT_ALERT value:@"3"];
//        [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"등록되었습니다." left:@"확인" right:@""];
        [self.navigationController popViewControllerAnimated:true];
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"meal_input_edit_data"]) {
        if ([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"] ) {
           
            [self foodDateSend];
            // 이미지 전달
//            [self foodPictureSend];
            
//            [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"수정되었습니다." left:@"확인" right:@""];
        }
    }else if([[result objectForKey:@"sn"] isEqualToString:_currentIdx]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            NSString *fileUrl = [NSString stringWithFormat:@"http://m.shealthcare.co.kr/HL_MED/food_img/%@",[result objectForKey:@"file_name"]];
            [_foodMainDB updateImage:_currentIdx filename:fileUrl];
        }
        
        [self.navigationController popViewControllerAnimated:true];
    }
    
}

- (IBAction)pressPhotoView:(id)sender {
    UIAlertController *actionsheet = [UIAlertController alertControllerWithTitle:@"" message:@"사진을 선택하세요." preferredStyle:UIAlertControllerStyleActionSheet];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [LogDB insertDb:HM03 m_cod:HM03010 s_cod:HM03010001];
            
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        }];
        
        [actionsheet addAction:cameraAction];
    }
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [LogDB insertDb:HM03 m_cod:HM03010 s_cod:HM03010001];
            
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }];
        
        [actionsheet addAction:libraryAction];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil];
    [actionsheet addAction:cancelAction];
    
    [self presentViewController:actionsheet animated:YES completion:nil];
}


// 이미지피커 활성
- (void)showImagePickerControllerWithSourceType: (UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

- (UIImage*)resizedImage:(UIImage*)image  Frame:(CGRect)frame {
    
    UIGraphicsBeginImageContext(frame.size);
    
    [image drawInRect:frame];
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

#pragma mark - BasicAlertVCDelegate Delegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if(alertView.view.tag == 100) {
        if(tag == 2) {
            [_items removeObjectAtIndex:selectNum];
            [__tableView reloadData];
        }
    }else if(alertView.view.tag == 1000){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        if(image.size.width > 300){
            int reSize = 0;
            
            reSize = (int)image.size.width / 300.f;
            
            //해상도 줄이기
            UIImage *tempImage = nil;
            
            CGSize targetSize = CGSizeMake(image.size.width/(float)reSize, image.size.height/(float)reSize);
            
            UIGraphicsBeginImageContext(targetSize);
            
            CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
            thumbnailRect.origin = CGPointMake(0.0,0.0);
            thumbnailRect.size.width  = targetSize.width;
            thumbnailRect.size.height = targetSize.height;
            
            [image drawInRect:thumbnailRect];
            
            tempImage = UIGraphicsGetImageFromCurrentImageContext();
            
            [self.selectedImageView setImage:tempImage];
            
            self->selectedImage = [tempImage copy];
        }else{
            [self.selectedImageView setImage:image];
            
            self->selectedImage = [image copy];
        }
        
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - DatePickerControllerDelegate
- (IBAction)pressDate:(id)sender {
    
    [self inputDeActivi];
    
    txtDate.layer.shadowColor = [[UIColor clearColor] CGColor];
//    txtDate.layer.borderColor = COLOR_;
    
    [AlertUtils DateControlShow:self dateType:Date tag:1001 selectDate:txtDate.text];
}

- (IBAction)pressTime:(id)sender {
    NSString *nTime = txtTime.text;
    
    [self inputDeActivi];
    
    if( [nTime rangeOfString:@"오전"].location != NSNotFound) {
        nTime = [nTime stringByReplacingOccurrencesOfString:@"오전 " withString:@""];
        
    }else if( [nTime rangeOfString:@"오후"].location != NSNotFound) {
        
        nTime = [nTime stringByReplacingOccurrencesOfString:@"오후 " withString:@""];
        NSArray *time      = [nTime componentsSeparatedByString:@":"];
        NSString *hour = @"";
        if([[time objectAtIndex:0] intValue] == 12){
            hour      = @"12";
        }else{
            hour      = [NSString stringWithFormat:@"%@", [Utils getZeroAppend:[[time objectAtIndex:0] intValue] + 12]];
        }
        
        NSString *minute    = [NSString stringWithFormat:@"%@", [Utils getZeroAppend:[[time objectAtIndex:1] intValue]]];
        
        nTime = [NSString stringWithFormat:@"%@:%@", hour, minute];
        
    }
    
    txtTime.layer.shadowColor = [[UIColor clearColor] CGColor];
//    txtTime.layer.borderColor = RGBA_ORAGNE_MAIN;
    
    [AlertUtils DateControlShow:self dateType:Time tag:1002 selectDate:nTime]; // 날짜
    
}

- (IBAction)preessClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - DateControlVCDelegate
- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    // 미래날짜 선택불가.
    NSString *tempDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([tempDate longLongValue] > [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue]) {
        return;
    }
    
    if (alertView.tag == 1001) {
        txtDate.text = [NSString stringWithFormat:@"%@", [date stringByReplacingOccurrencesOfString:@"-" withString:@"."]];
    }else if (alertView.tag == 1002) {
        txtTime.text = [NSString stringWithFormat:@"%@", [Utils getAmPmPlayHour:date]];
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}

#pragma mark - viewInit
- (void)viewInit {
    if([self.title isEqualToString:@"수정"]) {
        NSLog(@"writeDic = %@", _wirteDic);
        
        [_btnNavigationRight setTitle:@"수정" forState:UIControlStateNormal];
        _items = [_foodDetailDB getUserResult:[_wirteDic objectForKey:@"idx"]];
        [__tableView reloadData];
        
        [txtDate setText:[Utils getTimeFormat:[_wirteDic objectForKey:@"regdate"] beTime:@"yyyy-MM-dd HH:mm" afTime:@"yyyy.MM.dd"]];
        [txtTime setText:[Utils getAmPmPlayHour:[Utils getTimeFormat:[_wirteDic objectForKey:@"regdate"] beTime:@"yyyy-MM-dd HH:mm" afTime:@"HH:mm"]]];
        [txtAccountTime setText:[_wirteDic objectForKey:@"amounttime"]];
        
        if(![[_wirteDic objectForKey:@"picture"] isEqualToString:@""]){
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_wirteDic objectForKey:@"picture"]]]];
            
            [self.selectedImageView setImage:image];
            selectedImage = [image copy];
        }
    }else{
        [_btnNavigationRight setTitle:@"저장" forState:UIControlStateNormal];
        
        [self setMealType];
    }
    
    [_btnNavigationRight setBackgroundColor:[UIColor whiteColor]];
    [_btnNavigationRight setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    
    txtDate.textColor = COLOR_NATIONS_BLUE;
    txtTime.textColor = COLOR_NATIONS_BLUE;
    txtAccountTime.textColor = COLOR_NATIONS_BLUE;
    
}

@end
