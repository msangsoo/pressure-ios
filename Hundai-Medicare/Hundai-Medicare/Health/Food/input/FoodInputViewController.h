//
//  FoodInputViewController.h
//  LifeCare
//
//  Created by insystems company on 2017. 5. 26..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "FoodInputTableViewCell.h"
#import "FoodSearchViewController.h"
#import "FoodMainDBWraper.h"
#import "FoodDetailDBWraper.h"
#import "FoodForPeopleViewController.h"

@interface FoodInputViewController : BaseViewController<FoodSearchViewControllerDelegate, ServerCommunicationDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, FoodForPeopleViewControllerDelegate, BasicAlertVCDelegate, DateControlVCDelegate> {
    
    Boolean _isItemEdit;
    NSMutableArray *_items; //음식아이템
    int selectNum;
    NSMutableDictionary *_swifDic;
    NSString *_currentIdx;
    Tr_login *login;
    NSString *_regdate;
    NSString *nowTime;
    UIImage *selectedImage;
    
    FoodMainDBWraper *_foodMainDB;
    FoodDetailDBWraper *_foodDetailDB;
    NSDictionary*_mainParam;
    
    IBOutlet UITextField *txtDate;
    IBOutlet UITextField *txtTime;
    IBOutlet UITextField *txtAccountTime;
    
}

@property (weak, nonatomic) IBOutlet NSDictionary *wirteDic;
@property (weak, nonatomic) IBOutlet NSString *titleName;

@property (weak, nonatomic) IBOutlet UITableView *_tableView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigationRight;
@property (nonatomic, retain) NSString *_inputNDate;
@property (weak, nonatomic) NSString *_mealtype;

- (IBAction)pressSave:(id)sender;
- (IBAction)pressSearch:(id)sender;
- (IBAction)pressPhotoView:(id)sender;
- (IBAction)pressDate:(id)sender;
- (IBAction)pressTime:(id)sender;
@end
