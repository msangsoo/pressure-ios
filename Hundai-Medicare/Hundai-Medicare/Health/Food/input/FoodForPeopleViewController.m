//
//  FoodForPeopleViewController.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 20..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FoodForPeopleViewController.h"

@interface FoodForPeopleViewController ()

@end

@implementation FoodForPeopleViewController

- (void)dataInit {
    _minPeople = 0.25f;
    _maxPeople = 5.0f;
    self._curPeople = [[self._dicData objectForKey:@"forpeople"] floatValue];
    if(self._curPeople==0) {
        self._curPeople = 1.0f;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout =UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3]];
    
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self._viewHeader.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = self.view.bounds;
//    maskLayer.path  = maskPath.CGPath;
//    self._viewHeader.layer.mask = maskLayer;
    
//    self._viewBack.layer.cornerRadius = 10.0;
    self._btnCancle.layer.cornerRadius = 3.0;
    [self._btnCancle borderRadiusButton:COLOR_GRAY_SUIT];
    self._btnConfirm.layer.cornerRadius = 3.0;
    [self._btnConfirm backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    [self dataInit];
    
    self._titleLabel.font = FONT_NOTO_MEDIUM(18);
    [self._titleLabel setNotoText:[NSString stringWithFormat:@"%@", [self._dicData objectForKey:@"name"]]];
    self._titleLabel.textColor = COLOR_MAIN_DARK;
   
    self._forPeople.font = FONT_NOTO_REGULAR(16);
    self._forPeople.textColor = COLOR_NATIONS_BLUE;
    
    [self doForPeople];
}

- (void)doForPeople {
    NSString *calorie       = [Utils getNoneZeroString:[[self._dicData objectForKey:@"calorie"] floatValue] * self._curPeople];
    NSString *gram          = [Utils getNoneZeroString:[[self._dicData objectForKey:@"gram"] floatValue] * self._curPeople];
    NSString *carbohydrate  = [Utils getNoneZeroString:[[self._dicData objectForKey:@"carbohydrate"] floatValue] * self._curPeople];
    NSString *protein       = [Utils getNoneZeroString:[[self._dicData objectForKey:@"protein"] floatValue] * self._curPeople];
    NSString *fat           = [Utils getNoneZeroString:[[self._dicData objectForKey:@"fat"] floatValue] * self._curPeople];
    NSString *salt          = [Utils getNoneZeroString:[[self._dicData objectForKey:@"salt"] floatValue] * self._curPeople];
    
    NSString *forpeople = [Utils getNoneZeroString:self._curPeople];
    [self._forPeople setNotoText:[NSString stringWithFormat:@"%@인분(%@%@)", forpeople, gram, [self._dicData objectForKey:@"unit"]]];
    self._valueCalorie.text         = [NSString stringWithFormat:@"%@kcal", calorie];
    self._valueGram.text            = [NSString stringWithFormat:@"%@g", gram];
    self._valueCarbohydrate.text    = [NSString stringWithFormat:@"%@g", carbohydrate];
    self._valueProtein.text         = [NSString stringWithFormat:@"%@g", protein];
    self._valueFat.text             = [NSString stringWithFormat:@"%@g", fat];
    self._valueSolt.text            = [NSString stringWithFormat:@"%@mg", salt];
    
    // 최종 선택 전달용
    
    _saveDic =[NSDictionary dictionaryWithObjectsAndKeys:[self._dicData objectForKey:@"name"], @"name",
               [self._dicData objectForKey:@"code"], @"code",
               [self._dicData objectForKey:@"unit"], @"unit",
               calorie, @"calorie", gram, @"gram", carbohydrate, @"carbohydrate",
               protein, @"protein", fat, @"fat", salt, @"salt",
               forpeople, @"forpeople", nil];
    
}

- (IBAction)pressLeft:(id)sender {
    
    if (self._curPeople <= _minPeople) return;
    self._curPeople -= _minPeople;
    [self doForPeople];
}

- (IBAction)pressRight:(id)sender {
    if (self._curPeople >= _maxPeople) return;
    self._curPeople += _minPeople;
    [self doForPeople];
}

- (IBAction)pressConfirm:(id)sender {
    if (self.delegate != nil) {
        [self.delegate forPeopleSelectDelegate:_saveDic];
    }
    [self pressCancle:nil];
}

- (IBAction)pressCancle:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
