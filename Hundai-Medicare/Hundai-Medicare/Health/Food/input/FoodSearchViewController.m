//
//  FoodSearchViewController.m
//  LifeCare
//
//  Created by insystems company on 2017. 5. 26..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodSearchViewController.h"
#import "FoodSearchTableViewCell.h"

@interface FoodSearchViewController () {
    NSMutableArray *selectedItems;
    NSMutableArray *historyItems;
    UIBarButtonItem *saveBtn;
    
    //History
    FoodSearchCollectionViewLayout *collectionLayout;
}

@end

@implementation FoodSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [LogDB insertDb:HM03 m_cod:HM03011 s_cod:HM03011001];
    
    [self.navigationItem setTitle:@"음식 찾기"];
    
    saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"완료" style:UIBarButtonItemStylePlain target:self action:@selector(pressSave:)];
    saveBtn.tintColor = COLOR_NATIONS_BLUE;
    [self.navigationItem setRightBarButtonItem:saveBtn];
    
    selectedItems = [[NSMutableArray alloc] init];
    historyItems = [[NSMutableArray alloc] init];
    
    _arrResult = [[NSMutableArray alloc] init];
    
    foodDb = [[FoodCalorieDBWraper alloc] init];
    
    [_arrResult setArray:[foodDb getRealList:@""]];
    
    [self._txtSearch addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    self._txtSearch.layer.borderColor = COLOR_GRAY_SUIT.CGColor;
    self._txtSearch.layer.borderWidth = 1.5;
    [self._txtSearch setPlaceholder:@"음식명"];
    
    UIImageView *searchImgVw = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 15, self._txtSearch.frame.size.height)];
    [searchImgVw setContentMode:UIViewContentModeScaleAspectFit];
    [searchImgVw setImage:[UIImage imageNamed:@"icon_gray_search"]];
    UIView *searchVw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, self._txtSearch.frame.size.height)];
    [searchVw addSubview:searchImgVw];
    [self._txtSearch setLeftView: searchVw];
    self._txtSearch.leftViewMode = UITextFieldViewModeAlways;
    
    [self._tableView setHidden:true];
    
    historyItems = [FoodHistoryModel getFoodLatelyData];
    
    collectionLayout = [[FoodSearchCollectionViewLayout alloc] init];
    collectionLayout.headerReferenceSize = CGSizeMake(UIScreen.mainScreen.bounds.size.width, 0.1f);
    self.collectionView.collectionViewLayout = collectionLayout;
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self.collectionView setHidden:false];
}

#pragma mark - Actions
- (IBAction)pressSave:(id)sender {
        if (self.delegate != nil) {
            [self.delegate forPeopleSelectItems:[selectedItems copy]];
        }
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pressHistoryDel:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    [FoodHistoryModel removeFoodLatelData:btn.tag];
    
    historyItems = [FoodHistoryModel getFoodLatelyData];
    [self.collectionView reloadData];
}

- (IBAction)pressHistoryAllDel:(id)sender {
    [AlertUtils BasicAlertShow:self tag:101 title:@"" msg:@"전체 삭제하시겠습니까?" left:@"취소" right:@"확인"];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 101) {
        if (tag == 2) {
            [FoodHistoryModel removeAllData];
            
            historyItems = [FoodHistoryModel getFoodLatelyData];
            [self.collectionView reloadData];
        }
    }
}

#pragma mark - UITextFieldDelegate
- (void)textChanged:(UITextField *)textField {
    if (textField.text.length > 0) {
        [self._tableView setHidden:false];
        [self.collectionView setHidden:true];
        
        [_arrResult removeAllObjects];
        [_arrResult setArray:[foodDb getRealList:textField.text]];
        
        [self._tableView reloadData];
    }else {
        [self.collectionView setHidden:false];
        [self._tableView setHidden:true];
        
        [self.collectionView reloadData];
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodSearchTableViewCell" forIndexPath:indexPath];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FoodSearchTableViewCell" owner:self options:nil];
        cell = [nibs objectAtIndex:0];
    }
    NSDictionary *dicData = [_arrResult objectAtIndex:indexPath.row];
    NSString *title = [dicData objectForKey:@"name"];
    [cell.titleLabel setText:title];
    [cell.detailLbl setNotoText:[NSString stringWithFormat:@"1인분 (%@g)m,%@kcal", [dicData objectForKey:@"gram"], [dicData objectForKey:@"calorie"]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _currDic = [_arrResult objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"FoodForPeopleViewController" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self setEditing:NO];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.frame.size.width, 50);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return historyItems.count + 1;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        FoodSearchReusableVw *vw = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"FoodSearchReusableVw" forIndexPath:indexPath];
        vw.delegate = self;
        vw.items = [selectedItems copy];
        return vw;
    }
    return nil;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        FoodSearchLblCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FoodSearchLblCell" forIndexPath:indexPath];
        
        cell.titleLbl.textColor = COLOR_GRAY_SUIT;
        
        cell.delBtn.titleLabel.font = FONT_NOTO_REGULAR(13);
        [cell.delBtn setTitle:@"전체삭제" forState:UIControlStateNormal];
        [cell.delBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
        [cell.delBtn addTarget:self action:@selector(pressHistoryAllDel:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    FoodSearchHistoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FoodSearchHistoryCell" forIndexPath:indexPath];
    
    NSDictionary *item = historyItems[indexPath.row - 1];
    
    cell.titleLbl.text = item[@"name"];
    cell.cancelBtn.tag = indexPath.row - 1;
    [cell.cancelBtn addTarget:self action:@selector(pressHistoryDel:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        _currDic = [historyItems objectAtIndex:indexPath.row - 1];
        
        [self performSegueWithIdentifier:@"FoodForPeopleViewController" sender:nil];
    }
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FoodForPeopleViewController"]) {
        FoodForPeopleViewController *destinationView = (FoodForPeopleViewController *)[segue destinationViewController];
        destinationView.delegate = self;
        destinationView.providesPresentationContextTransitionStyle = YES;
        destinationView.definesPresentationContext = YES;
        [destinationView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        destinationView._dicData = _currDic;
    }
}

#pragma mark - Delegate Call
- (void)forPeopleSelectDelegate:(NSDictionary *)dicData {
    if (selectedItems.count > 14) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                 message:@"15개 까지 등록할 수 있습니다."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:alertAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    NSMutableArray *tempSelectedItems = [[NSMutableArray alloc] initWithArray:[selectedItems copy]];
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dicData];
    
    for (NSDictionary *dic in selectedItems) {
        if ([dic[@"name"] isEqualToString:dicData[@"name"]]) {
            CGFloat dicForpeople = [dic[@"forpeople"] floatValue];
            CGFloat dicDataForpeople = [dicData[@"forpeople"] floatValue];
            tempDic[@"forpeople"] = [NSString stringWithFormat:@"%g", dicForpeople + dicDataForpeople];
            [tempSelectedItems removeObject:dic];
        }
    }
    
    [tempSelectedItems addObject:tempDic];
    selectedItems = tempSelectedItems;
    
    if (selectedItems.count > 0) {
        [saveBtn setTitle:[NSString stringWithFormat:@"완료(%d)", (int)selectedItems.count]];
    }else {
        [saveBtn setTitle:@"완료"];
    }
    
    
    [FoodHistoryModel saveFoodLatelData:_currDic];
    
    historyItems = [FoodHistoryModel getFoodLatelyData];
    
    self._txtSearch.text = @"";
    
    [self._tableView setHidden:true];
    [self.collectionView setHidden:false];
    
    [self.collectionView reloadData];
}

#pragma mark - FoodSearchReusableVwDelegate
- (void)resusableContentHeight:(CGFloat)height {
    if (height <= 0.5f) {
        [collectionLayout updateHeaderHeight:height + 0.1f];
    }else {
        [collectionLayout updateHeaderHeight:height + 35.f];
    }
}

- (void)resusableItemsDelete:(int)index {
    [selectedItems removeObjectAtIndex:index];
    
    if (selectedItems.count > 0) {
        [saveBtn setTitle:[NSString stringWithFormat:@"완료(%d)", (int)selectedItems.count]];
    }else {
        [saveBtn setTitle:@"완료"];
    }
    
    [self._tableView setHidden:true];
    [self.collectionView setHidden:false];
    
    [self.collectionView reloadData];
}

@end


#pragma mark - Layout
@implementation FoodSearchCollectionViewLayout
- (void)updateHeaderHeight:(CGFloat)height {
    self.headerReferenceSize = CGSizeMake(UIScreen.mainScreen.bounds.size.width, height);
}
@end
