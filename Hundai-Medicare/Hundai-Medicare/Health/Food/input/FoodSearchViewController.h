//
//  FoodSearchViewController.h
//  LifeCare
//
//  Created by insystems company on 2017. 5. 26..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodForPeopleViewController.h"
#import "FoodCalorieDBWraper.h"
#import "FoodHistoryModel.h"
#import "FoodSearchHistoryCell.h"
#import "FoodSearchReusableVw.h"
#import "FoodSearchLblCell.h"

@protocol FoodSearchViewControllerDelegate <NSObject>
- (void)forPeopleSelectDelegate:(NSDictionary*)dicData;
- (void)forPeopleSelectItems:(NSArray *)items;
@end

@interface FoodSearchViewController : UIViewController<FoodForPeopleViewControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, FoodSearchReusableVwDelegate, BasicAlertVCDelegate>
{
    NSDictionary *_currDic;
    FoodCalorieDBWraper *foodDb;
    NSMutableArray *_arrResult;
    
}
@property(weak, nonatomic) id<FoodSearchViewControllerDelegate> delegate;

@property(nonatomic, retain) IBOutlet UITextField *_txtSearch;
@property(nonatomic, retain) IBOutlet UITableView *_tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

#pragma mark - layout
@interface FoodSearchCollectionViewLayout : UICollectionViewFlowLayout
- (void)updateHeaderHeight:(CGFloat)height;
@end
