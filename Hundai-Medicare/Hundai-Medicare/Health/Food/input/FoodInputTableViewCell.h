//
//  FoodInputTableViewCell.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 21..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodInputTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ImgArrow;

@end
