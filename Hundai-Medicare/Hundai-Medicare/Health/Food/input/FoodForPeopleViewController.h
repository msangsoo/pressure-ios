//
//  FoodForPeopleViewController.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 20..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"

@protocol FoodForPeopleViewControllerDelegate <NSObject>
- (void)forPeopleSelectDelegate:(NSDictionary*)dicData;
@end

@interface FoodForPeopleViewController : UIViewController{
    //float _curPeople;
    float _minPeople;
    float _maxPeople;
    NSDictionary *_saveDic;
}

@property(weak, nonatomic) id<FoodForPeopleViewControllerDelegate> delegate;
@property(nonatomic, assign) float _curPeople;  //현재 인분
@property(nonatomic, retain) NSDictionary *_dicData;
@property(nonatomic, retain) IBOutlet UIView *_viewBack;
@property(nonatomic, retain) IBOutlet UIView *_viewHeader;
@property(nonatomic, retain) IBOutlet UILabel *_titleLabel;
@property(nonatomic, retain) IBOutlet UILabel *_forPeople;
@property(nonatomic, retain) IBOutlet UILabel *_valueCalorie;
@property(nonatomic, retain) IBOutlet UILabel *_valueGram;
@property(nonatomic, retain) IBOutlet UILabel *_valueCarbohydrate;
@property(nonatomic, retain) IBOutlet UILabel *_valueProtein;
@property(nonatomic, retain) IBOutlet UILabel *_valueFat;
@property(nonatomic, retain) IBOutlet UILabel *_valueSolt;
@property(nonatomic, retain) IBOutlet UIButton *_btnCancle;
@property(nonatomic, retain) IBOutlet UIButton *_btnConfirm;

- (IBAction)pressLeft:(id)sender;
- (IBAction)pressRight:(id)sender;
- (IBAction)pressConfirm:(id)sender;
- (IBAction)pressCancle:(id)sender;

@end
