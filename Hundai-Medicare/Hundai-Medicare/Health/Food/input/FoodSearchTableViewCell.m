//
//  FoodSearchTableViewCell.m
//  LifeCare
//
//  Created by insystems company on 2017. 7. 20..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "FoodSearchTableViewCell.h"

@implementation FoodSearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _titleLabel.font = FONT_NOTO_REGULAR(15);
    _titleLabel.textColor = COLOR_MAIN_DARK;
    _titleLabel.text = @"";
    
    _detailLbl.font = FONT_NOTO_THIN(11);
    _detailLbl.textColor = COLOR_GRAY_SUIT;
    _detailLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
