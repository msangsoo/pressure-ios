//
//  FoodSearchTableViewCell.h
//  LifeCare
//
//  Created by insystems company on 2017. 7. 20..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodSearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;

@end
