//
//  FoodMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "FoodWriteVC.h"
#import "FoodHistoryVC.h"

@interface FoodMainVC : BaseViewController {
    FoodWriteVC *vcWrite;
    FoodHistoryVC *vcHistory;
}

@property (weak, nonatomic) IBOutlet UIButton *writeBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

@end

