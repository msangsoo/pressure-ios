//
//  FoodSearchHistoryCell.h
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 25/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodSearchHistoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end
