//
//  FoodSearchReusableVw.m
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 25/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import "FoodSearchReusableVw.h"

@implementation FoodSearchReusableVw {
    int itemsCount;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    itemsCount = 0;
    self.items = [[NSArray alloc] init];
    
    UICollectionViewFlowLayout *layout = [[FoodSearchReusableLayout alloc] init];
    self.collectionView.collectionViewLayout = layout;
    
    self.collectionView.showsVerticalScrollIndicator = false;
    self.collectionView.showsHorizontalScrollIndicator = false;
    self.collectionView.scrollEnabled = false;
    
    self.collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}

- (void)prepareForReuse {
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadData];
    } completion:^(BOOL finished) {
        if (self->itemsCount > self.items.count) {
            [self.collectionView reloadData];
        }else {
            [self.collectionView reloadItemsAtIndexPaths:self.collectionView.indexPathsForVisibleItems];
        }
        self->itemsCount = (int)self.items.count;
        [self.delegate resusableContentHeight:self.collectionView.contentSize.height];
    }];
}

#pragma mark - Actions
- (IBAction)pressDel:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [self.delegate resusableItemsDelete:(int)btn.tag];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = self.items[indexPath.row];
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.numberOfLines = 1;
    lbl.font = FONT_NOTO_REGULAR(16);
    [lbl setNotoText:[NSString stringWithFormat:@"%@ %@인분",dic[@"name"], [dic objectForKey:@"forpeople"]]];
    [lbl sizeToFit];
    
    if (UIScreen.mainScreen.bounds.size.width - 15 < lbl.frame.size.width + 10 + 30) {
        return CGSizeMake(UIScreen.mainScreen.bounds.size.width - 15, 30);
    }else {
        return CGSizeMake(lbl.frame.size.width + 10 + 30, 30);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FoodSearchReusableCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FoodSearchReusableCell" forIndexPath:indexPath];
    
    NSDictionary *dic = self.items[indexPath.row];
    
    [cell.titleLbl setNotoText:[NSString stringWithFormat:@"%@ %@인분",dic[@"name"], [dic objectForKey:@"forpeople"]]];
    cell.delBtn.tag = indexPath.row;
    [cell.delBtn addTarget:self action:@selector(pressDel:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

}

@end

#pragma mark - Cell
@implementation FoodSearchReusableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = COLOR_DARK_BLUE;
    
    self.titleLbl.textColor = [UIColor whiteColor];
    self.titleLbl.font = FONT_NOTO_REGULAR(16);
    
    UIImage *close = [UIImage imageNamed:@"btn_circle_white_cancel"];
    [self.delBtn setImage:close forState:UIControlStateNormal];
    [self.delBtn setBackgroundColor:[UIColor clearColor]];
}

- (void)prepareForReuse {
    self.titleLbl.text = @"";
}

@end

#pragma mark - Layout
@implementation FoodSearchReusableLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
    
    CGFloat leftMargin = self.sectionInset.left; //initalized to silence compiler, and actaully safer, but not planning to use.
    CGFloat maxY = -1.0f;
    
    //this loop assumes attributes are in IndexPath order
    for (UICollectionViewLayoutAttributes *attribute in attributes) {
        if (attribute.frame.origin.y >= maxY) {
            leftMargin = self.sectionInset.left;
        }
        
        attribute.frame = CGRectMake(leftMargin, attribute.frame.origin.y, attribute.frame.size.width, attribute.frame.size.height);
        
        leftMargin += attribute.frame.size.width + self.minimumInteritemSpacing;
        maxY = MAX(CGRectGetMaxY(attribute.frame), maxY);
    }
    
    return attributes;
}

@end
