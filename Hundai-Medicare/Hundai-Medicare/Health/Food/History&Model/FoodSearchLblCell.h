//
//  FoodSearchLblCell.h
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 07/03/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodSearchLblCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;

@end
