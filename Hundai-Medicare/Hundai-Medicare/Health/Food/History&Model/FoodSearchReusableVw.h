//
//  FoodSearchReusableVw.h
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 25/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FoodSearchReusableVwDelegate
- (void)resusableContentHeight:(CGFloat)height;
- (void)resusableItemsDelete:(int)index;
@end

@interface FoodSearchReusableVw : UICollectionReusableView<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource>

@property NSArray *items;

@property(nonatomic, weak) id<FoodSearchReusableVwDelegate> delegate;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

#pragma mark - Cell
@interface FoodSearchReusableCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;

@end

#pragma mark - Layout
@interface FoodSearchReusableLayout : UICollectionViewFlowLayout

@end
