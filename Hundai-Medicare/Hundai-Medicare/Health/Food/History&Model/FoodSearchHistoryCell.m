//
//  FoodSearchHistoryCell.m
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 25/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import "FoodSearchHistoryCell.h"

@implementation FoodSearchHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLbl.text = @"";
    self.titleLbl.textColor = COLOR_MAIN_DARK;
    self.titleLbl.font = [UIFont systemFontOfSize:13];
    
    [self.cancelBtn setImage:[UIImage imageNamed:@"btn_circle_gray_cancel"] forState:UIControlStateNormal];
}


@end
