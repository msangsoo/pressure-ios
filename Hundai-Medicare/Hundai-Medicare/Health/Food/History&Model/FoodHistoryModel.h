//
//  FoodHistoryModel.h
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodHistoryModel : NSObject

+ (void)saveFoodLatelData:(NSDictionary *)dic;

+ (void)removeFoodLatelData:(NSInteger)index;

+ (void)removeAllData;

+ (NSMutableArray *)getFoodLatelyData;

@end
