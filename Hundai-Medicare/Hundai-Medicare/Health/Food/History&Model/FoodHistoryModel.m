//
//  FoodHistoryModel.m
//  Hyundai Insurance
//
//  Created by INSYSTEMS CO., LTD on 15/02/2019.
//  Copyright © 2019 Kiboom. All rights reserved.
//

#import "FoodHistoryModel.h"

static NSString *Food_Lately_Input = @"Food_Lately_Input";

@implementation FoodHistoryModel

+ (void)saveFoodLatelData:(NSDictionary *)dic {
    NSMutableArray *dics = [[NSMutableArray alloc] initWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:Food_Lately_Input] copy]];
    
    if (dics == nil) {
        dics = [[NSMutableArray alloc] init];
        [dics addObject:dic];
    }else {
        NSMutableArray *tempDics = [NSMutableArray arrayWithArray:[dics copy]];
        
        NSUInteger count = 0;
        for(NSDictionary *tempDic in dics) {
            if([tempDic[@"name"] isEqual:dic[@"name"]]) {
                [tempDics removeObjectAtIndex:count];
            }
            count ++;
        }
        
        [tempDics insertObject:dic atIndex:0];
        dics = [tempDics copy];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:dics forKey:Food_Lately_Input];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeFoodLatelData:(NSInteger)index {
    NSMutableArray *dics = [[NSMutableArray alloc] initWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:Food_Lately_Input] copy]];
    
    if (dics != nil) {
        [dics removeObjectAtIndex:index];
        if (dics.count < 1) {
            dics = [[NSMutableArray alloc] init];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:dics forKey:Food_Lately_Input];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)removeAllData {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Food_Lately_Input];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSMutableArray *)getFoodLatelyData {
    NSMutableArray *dics = [[NSUserDefaults standardUserDefaults] objectForKey:Food_Lately_Input];
    if (dics == nil) {
        dics = [[NSMutableArray alloc] init];
    }
    return dics;
}

@end
