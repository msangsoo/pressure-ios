//
//  FoodWriteVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FoodWriteVC.h"

@interface FoodWriteVC () {
    NSString *OCM_SEQ;
    
    int totalKcalVal;
    int nowKcalVal;
    
    NSDictionary *writeDic;
}

@end

@implementation FoodWriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    OCM_SEQ = @"";
    
    _db = [[FoodMainDBWraper alloc] init];
    timeUtils = [[TimeUtils alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [self viewSetup];
}

- (void)viewDidLayoutSubviews {
    [_rice1IconImgVw makeToRadius:true];
    [_rice2IconImgVw makeToRadius:true];
    [_rice3IconImgVw makeToRadius:true];
    [_snack1IconImgVw makeToRadius:true];
    [_snack2IconImgVw makeToRadius:true];
    [_snack3IconImgVw makeToRadius:true];
}

#pragma mark - Actions
- (IBAction)pressShare:(id)sender {
    OCM_SEQ = @"";
    
    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
        
        NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.touchedCancelButton = ^{
            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
        };
        vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
            NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
            NSString *seq = self->_model.getLoginData.mber_sn;
            
            [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
                if (isSuccess == TRUE) {
                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
                    
                    [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
                    self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
                    FoodShareKindAlertVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodShareKindAlertVC"];
                    vc.delegate = self;
                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
                    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
                }else {
                    
                }
            }];
        };
        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
    }else {
        
        FoodShareKindAlertVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodShareKindAlertVC"];
        vc.delegate = self;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
    }
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

- (IBAction)pressItems:(id)sender {
    UIButton *btn = (UIButton *)sender;
    writeDic = nil;
    if(btn.tag == 1) { // 아침
        _selfMealType = @"a";
    }else if(btn.tag == 2) { // 아침간식
        _selfMealType = @"d";
    }else if(btn.tag == 3) { // 점심
        _selfMealType = @"b";
    }else if(btn.tag == 4) { // 점심간식
        _selfMealType = @"e";
    }else if(btn.tag == 5) { // 저녁
        _selfMealType = @"c";
    }else if(btn.tag == 6) { // 저녁 간식
        _selfMealType = @"f";
    }
    
    for(NSDictionary *dic in _arrFoodMain) {
        if([[dic objectForKey:@"mealtype"] isEqualToString:_selfMealType]) {
            writeDic = dic;
        }
    }
    
    [self inputPageMove];
}

#pragma mark - FoodShareKindAlertVCDelegate
- (void)selected:(int)index {
    FeedModel *feed = [[FeedModel alloc] init];
    feed.mberSn = [_model getLoginData].mber_sn;
    
    NSString *foodMealType = @"";
    NSDictionary *foodItem = [[NSDictionary alloc] init];
    
    NSString *mealType = @"";
    if (index == 0) {
        mealType = @"a";
    }else if (index == 1) {
        mealType = @"d";
    }else if (index == 2) {
        mealType = @"b";
    }else if (index == 3) {
        mealType = @"e";
    }else if (index == 4) {
        mealType = @"c";
    }else if (index == 5) {
        mealType = @"f";
    }
    
    if (index >= 0) {
        for(NSDictionary *dic in _arrFoodMain) {
            if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 아침 밥
                foodMealType = @"아침식사";
                foodItem = dic;
            }else if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 아침간식
                foodMealType = @"아침간식";
                foodItem = dic;
            }else if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 점심 밥
                foodMealType = @"점심식사";
                foodItem = dic;
            }else if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 점심 간식
                foodMealType = @"점심간식";
                foodItem = dic;
            }else if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 저녁 밥
                foodMealType = @"저녁식사";
                foodItem = dic;
            }else if([[dic objectForKey:@"mealtype"] isEqualToString:mealType]) { // 저녁 간식
                foodMealType = @"저녁간식";
                foodItem = dic;
            }
        }
    }
    
    if (foodMealType.length > 0) {
        _foodDetailDB = [[FoodDetailDBWraper alloc] init];
        NSArray *items = [_foodDetailDB getUserResult:[foodItem objectForKey:@"idx"]];
        NSString *foodMenu = @"";
        for (NSDictionary *dic in items) {
            foodMenu = [NSString stringWithFormat:@"%@%@,",foodMenu, [dic objectForKey:@"name"]];
        }
        
        if (foodMenu.length > 0) {
            foodMenu = [foodMenu substringToIndex:foodMenu.length - 1];
        }
        
        NSString *calorie = [NSString stringWithFormat:@"%@", [foodItem objectForKey:@"calorie"]];
        
        NSString *meal = [NSString stringWithFormat:@"%@ / %@ / %@ kcal", foodMealType, foodMenu, calorie];
        
        feed.cmMeal = meal;
    }else {
        feed.cmMeal = @"";
    }
    
    [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
        if(success) {
            self->OCM_SEQ = response[@"OCM_SEQ"];
            [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
        }
    }];
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 2000) {
        if (tag == 2) {
            if (![OCM_SEQ isEqualToString:@""]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                feedDetailVC.seq = _model.getLoginData.mber_sn;
                feedDetailVC.feedSeq = OCM_SEQ;
                [self.navigationController pushViewController:feedDetailVC animated:TRUE];
            }
        }
    }
}

#pragma mark - page Move
- (void)inputPageMove {
    NSString *title = @"";
    
    if([_selfMealType isEqualToString:@"a"]) {
        title = @"아침";
    }else if([_selfMealType isEqualToString:@"b"]) {
        title = @"점심";
    }if([_selfMealType isEqualToString:@"c"]) {
        title = @"저녁";
    }if([_selfMealType isEqualToString:@"d"]) {
        title = @"아침 간식";
    }if([_selfMealType isEqualToString:@"e"]) {
        title = @"점심 간식";
    }if([_selfMealType isEqualToString:@"f"]) {
        title = @"저녁 간식";
    }
    
    NSString *nDate = [self.dateLbl.text stringByReplacingOccurrencesOfString:@"." withString:@"-"];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:writeDic];
    [dic setValue:_selfMealType forKey:@"mealtype"];
    [dic setValue:title forKey:@"title"];
    [dic setValue:[NSString stringWithFormat:@"%@", nDate] forKey:@"inputNDate"];
    [dic setValue:@"수정" forKey:@"naviTitle"];
    [dic setValue:writeDic forKey:@"writeDic"];
    if(writeDic != nil) {
        [dic setValue:@"수정" forKey:@"naviTitle"];
    }else{
        [dic setValue:@"등록" forKey:@"naviTitle"];
    }
    
    [self performSegueWithIdentifier:@"FoodInputViewController" sender:nil];
}

#pragma mark - segue delegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *title = @"";
    
    if([_selfMealType isEqualToString:@"a"]) {
        title = @"아침";
    }else if([_selfMealType isEqualToString:@"b"]) {
        title = @"점심";
    }if([_selfMealType isEqualToString:@"c"]) {
        title = @"저녁";
    }if([_selfMealType isEqualToString:@"d"]) {
        title = @"아침 간식";
    }if([_selfMealType isEqualToString:@"e"]) {
        title = @"점심 간식";
    }if([_selfMealType isEqualToString:@"f"]) {
        title = @"저녁 간식";
    }
    
    if ([segue.identifier isEqualToString:@"FoodInputViewController"]) {
        
        FoodInputViewController *viewController = segue.destinationViewController;
        viewController.hidesBottomBarWhenPushed = YES;
        
        if(writeDic != nil) {
            NSString *nDate = [self.dateLbl.text stringByReplacingOccurrencesOfString:@"." withString:@"-"];
            NSLog(@"self.stateDateLbl.text:%@", nDate);
            
            FoodInputViewController *destinationView = (FoodInputViewController *)[segue destinationViewController];
            destinationView._mealtype = _selfMealType;
            destinationView.titleName = title;
            destinationView._inputNDate = [NSString stringWithFormat:@"%@", nDate];
            destinationView.wirteDic = writeDic;
            destinationView.title = @"수정";
        }else {
            NSString *nDate = [self.dateLbl.text stringByReplacingOccurrencesOfString:@"." withString:@"-"];
            NSLog(@"self.stateDateLbl.text:%@", nDate);
            
            FoodInputViewController *destinationView = (FoodInputViewController *)[segue destinationViewController];
            destinationView._mealtype = _selfMealType;
            destinationView.titleName = title;
            destinationView._inputNDate = [NSString stringWithFormat:@"%@", nDate];
            destinationView.title = @"등록";
        }
    }
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self viewInit];
    [self timeButtonInit];
    [self ChartDataInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    [_dateLbl setNotoText:startTime];
}

- (void)ChartDataInit {
    NSArray *foodImgViewArr = @[self.rice1IconImgVw, self.rice2IconImgVw, self.rice3IconImgVw, self.snack1IconImgVw, self.snack2IconImgVw, self.snack3IconImgVw];
    
    for (UIImageView *imgVw in foodImgViewArr) {
        for (CALayer *layer in imgVw.layer.sublayers) {
            [layer removeFromSuperlayer];
        }
    }
    
    _arrFoodMain = [_db getFoodMainResult:[timeUtils getNowTime]];
    
    _rice1KcalLbl.textColor = COLOR_MAIN_DARK;
    _rice2KcalLbl.textColor = COLOR_MAIN_DARK;
    _rice3KcalLbl.textColor = COLOR_MAIN_DARK;
    _snack1KcalLbl.textColor = COLOR_MAIN_DARK;
    _snack2KcalLbl.textColor = COLOR_MAIN_DARK;
    _snack3KcalLbl.textColor = COLOR_MAIN_DARK;
    
    CGFloat fontSize = 13.f;
    nowKcalVal = 0;
    
    CGFloat totalCar = 0.f;
    CGFloat totalPro = 0.f;
    CGFloat totalFat = 0.f;
    
    for(NSDictionary *dic in _arrFoodMain) {
        if([[dic objectForKey:@"food_car"] length] > 0){
            if ([[dic objectForKey:@"food_car"] floatValue] > 0) {
                totalCar += [[dic objectForKey:@"food_car"] floatValue];
            }
        }
        
        if([[dic objectForKey:@"food_protein"] length] > 0){
            if ([[dic objectForKey:@"food_protein"] floatValue] > 0) {
                totalPro += [[dic objectForKey:@"food_protein"] floatValue];
            }
        }
        
        if([[dic objectForKey:@"food_fat"] length] > 0){
            if ([[dic objectForKey:@"food_fat"] floatValue] > 0) {
                totalFat += [[dic objectForKey:@"food_fat"] floatValue];
            }
        }
        
        if([[dic objectForKey:@"mealtype"] isEqualToString:@"a"]) { // 아침 밥
            
            _rice1IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [_rice1KcalLbl setText:@"0 Kcal"];
            self.rice1KcalLbl.font = [UIFont fontWithName:MAIN_FONT_NAME size: fontSize];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.rice1KcalLbl.textColor = COLOR_MAIN_DARK;
                }
                self.rice1KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            
            [self.rice1Lbl setText:@"아침"];
            if([[dic objectForKey:@"amounttime"] length] > 0){
                [self.rice1Lbl setText:[NSString stringWithFormat:@"아침 %@분", [dic objectForKey:@"amounttime"]]];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]) {
                [self.rice1IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.rice1IconImgVw.image = [self cropImage:image];
                    self.rice1KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.rice1IconImgVw];
                }];
            }else {
                self.rice1IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
            
        }else if([[dic objectForKey:@"mealtype"] isEqualToString:@"d"]) { // 아침간식
            
            self.snack1IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [self.snack1KcalLbl setText:@"0 Kcal"];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.snack1KcalLbl.textColor = COLOR_MAIN_DARK;
                    
                }
                self.snack1KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]) {
                [self.snack1IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.snack1IconImgVw.image = [self cropImage:image];
                    self.snack1KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.snack1IconImgVw];
                }];
            }else {
                self.snack1IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
        }else if([[dic objectForKey:@"mealtype"] isEqualToString:@"b"]) { // 점심 밥
            
            self.rice2IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [self.rice2KcalLbl setText:@"0 Kcal"];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.rice2KcalLbl.textColor = COLOR_MAIN_DARK;
                }
                self.rice2KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            
            [self.rice2Lbl setText:@"점심"];
            if([[dic objectForKey:@"amounttime"] length] > 0){
                [self.rice2Lbl setText:[NSString stringWithFormat:@"점심 %@분", [dic objectForKey:@"amounttime"]]];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]) {
                [self.rice2IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.rice2IconImgVw.image = [self cropImage:image];
                    self.rice2KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.rice2IconImgVw];
                }];
            }else {
                self.rice2IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
        }else if([[dic objectForKey:@"mealtype"] isEqualToString:@"e"]) { // 점심 간식
            
            self.snack2IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [self.snack2KcalLbl setText:@"0 Kcal"];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.snack2KcalLbl.textColor = COLOR_MAIN_DARK;
                }
                self.snack2KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]) {
                [self.snack2IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.snack2IconImgVw.image = [self cropImage:image];
                    self.snack2KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.snack2IconImgVw];
                }];
            }else {
                self.snack2IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
            
        }else if([[dic objectForKey:@"mealtype"] isEqualToString:@"c"]) { // 저녁 밥
            
            self.rice3IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [self.rice3KcalLbl setText:@"0 Kcal"];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.rice3KcalLbl.textColor = COLOR_MAIN_DARK;
                }
                self.rice3KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            [self.rice3Lbl setText:@"저녁"];
            if([[dic objectForKey:@"amounttime"] length] > 0){
                [self.rice3Lbl setText:[NSString stringWithFormat:@"저녁 %@분", [dic objectForKey:@"amounttime"]]];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]){
                [self.rice3IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.rice3IconImgVw.image = [self cropImage:image];
                    self.rice3KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.rice3IconImgVw];
                    
                }];
            }else {
                self.rice3IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
        }else if([[dic objectForKey:@"mealtype"] isEqualToString:@"f"]) { // 저녁 간식
            
            self.snack3IconImgVw.layer.borderColor = COLOR_MAIN_DARK.CGColor;
            
            [self.snack3KcalLbl setText:@"0 Kcal"];
            if([[dic objectForKey:@"calorie"] length] > 0){
                if ([[dic objectForKey:@"calorie"] intValue] > 0) {
                    self.snack3KcalLbl.textColor = COLOR_MAIN_DARK;
                }
                self.snack3KcalLbl.attributedText = [self setEatKcalText:[NSString stringWithFormat:@"%.0f", [[dic objectForKey:@"calorie"] floatValue]]];
                nowKcalVal += [[dic objectForKey:@"calorie"] intValue];
            }
            
            if(![[dic objectForKey:@"picture"] isEqualToString:@""]){
                [self.snack3IconImgVw sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"picture"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    self.snack3IconImgVw.image = [self cropImage:image];
                    self.snack3KcalLbl.textColor = [UIColor whiteColor];
                    [self layerMaskMake:self.snack3IconImgVw];
                    
                }];
            }else {
                self.snack3IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
            }
        }
    }
    
    NSNumber *num1 = [NSNumber numberWithInt:nowKcalVal];
    NSString *numberStr = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterDecimalStyle];
    [self.nowKcalbl setNotoText:[NSString stringWithFormat:@"%@ %@", numberStr, @"kcal"]];
    
    int pregnancy = [_model getPregnancyRecommendCal];
    num1 = [NSNumber numberWithInt:pregnancy];
    NSString *numberStr2 = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterDecimalStyle];
    [self.totalKcallbl setNotoText:[NSString stringWithFormat:@"%@ %@", numberStr2, @"kcal"]];

    /*
    탄수화물 섭취량 평가 계산식 :(탄수화물 섭취량(g) * 4kcal)/하루권장섭취량(kcal)*100=00
    단백질 섭취량 평가 : 다음 슬라이드 표2.평가기준 참고
    지방 섭취평가 기준 : (지방 섭취량(g) * 9kcal)/하루권장섭취량(kcal)*100=00%
*/
    float car = (totalCar * 4.f) / (float)pregnancy * 100.f;
    self.carbohydrateValLbl.attributedText = [self setMiddleShameText:totalCar val:car type:1];
    
    float protein = (totalPro * 4.f) / (float)pregnancy * 100.f;
    self.proteinValLbl.attributedText = [self setMiddleShameText:totalPro val:protein type:2];
    
    float fat = (totalFat * 9.f) / (float)pregnancy * 100.f;
    self.fatValLbl.attributedText = [self setMiddleShameText:totalFat val:fat type:3];
    
    [self mainDrawCirular:nowKcalVal total:pregnancy];
}


#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_BOLD(16);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    [_dateLbl setNotoText:@""];
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _nowKcalbl.font = FONT_NOTO_BOLD(12);
    _nowKcalbl.textColor = COLOR_MAIN_GREEN;
    [_nowKcalbl setNotoText:@"kcal"];
    
    _totalKcallbl.font = FONT_NOTO_THIN(12);
    _totalKcallbl.textColor = COLOR_MAIN_DARK;
    [_totalKcallbl setNotoText:@"kcal"];
    
    _carbohydrateLbl.font = FONT_NOTO_LIGHT(14);
    _carbohydrateLbl.textColor = COLOR_MAIN_DARK;
    [_carbohydrateLbl setNotoText:@"탄수화물"];
    
    _proteinLbl.font = FONT_NOTO_LIGHT(14);
    _proteinLbl.textColor = COLOR_MAIN_DARK;
    [_proteinLbl setNotoText:@"단백질"];
    
    _fatLbl.font = FONT_NOTO_LIGHT(14);
    _fatLbl.textColor = COLOR_MAIN_DARK;
    [_fatLbl setNotoText:@"지방"];
    
    [self infoLabelText];
    
    /////////EAT
    ///////////////
    _rice1IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _rice1IconImgVw.layer.borderWidth = 1;
    _rice1IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
    
    _rice1KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _rice1Lbl.font = FONT_NOTO_MEDIUM(12);
    _rice1Lbl.textColor = COLOR_MAIN_DARK;
    [_rice1Lbl setNotoText:@"아침"];
    
    _rice2IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _rice2IconImgVw.layer.borderWidth = 1;
    _rice2IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
    
    _rice2KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _rice2Lbl.font = FONT_NOTO_MEDIUM(12);
    _rice2Lbl.textColor = COLOR_MAIN_DARK;
    [_rice2Lbl setNotoText:@"점심"];
    
    _rice3IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _rice3IconImgVw.layer.borderWidth = 1;
    _rice3IconImgVw.image = [UIImage imageNamed:@"icon_food_edt"];
    
    _rice3KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _rice3Lbl.font = FONT_NOTO_MEDIUM(12);
    _rice3Lbl.textColor = COLOR_MAIN_DARK;
    [_rice3Lbl setNotoText:@"저녁"];
    
    _snack1IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _snack1IconImgVw.layer.borderWidth = 1;
    _snack1IconImgVw.image = [UIImage imageNamed:@"icon_food_dinner"];
    
    _snack1KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _snack1Lbl.font = FONT_NOTO_MEDIUM(12);
    _snack1Lbl.textColor = COLOR_MAIN_DARK;
    [_snack1Lbl setNotoText:@"아침간식"];
    
    _snack2IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _snack2IconImgVw.layer.borderWidth = 1;
    _snack2IconImgVw.image = [UIImage imageNamed:@"icon_food_dinner"];
    
    _snack2KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _snack2Lbl.font = FONT_NOTO_MEDIUM(12);
    _snack2Lbl.textColor = COLOR_MAIN_DARK;
    [_snack2Lbl setNotoText:@"점심간식"];
    
    _snack3IconImgVw.layer.borderColor = COLOR_GRAY_LIGHT.CGColor;
    _snack3IconImgVw.layer.borderWidth = 1;
    _snack3IconImgVw.image = [UIImage imageNamed:@"icon_food_dinner"];
    
    _snack3KcalLbl.attributedText = [self setEatKcalText:@"0"];
    
    _snack3Lbl.font = FONT_NOTO_MEDIUM(12);
    _snack3Lbl.textColor = COLOR_MAIN_DARK;
    [_snack3Lbl setNotoText:@"저녁간식"];
}

#pragma mark - food write attri text
- (NSMutableAttributedString *)setMiddleShameText:(float)totalVal val:(float)val type:(int)type {
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    UIColor *valColor = COLOR_MAIN_GREEN;
    
    if (type == 1) { // 탄수화물
        if (val < 55.f) {
            valColor = COLOR_MAIN_ORANGE;
        }else if (val >= 55.f && val <= 65.f) {
            valColor = COLOR_MAIN_GREEN;
        }else {
            valColor = COLOR_MAIN_RED;
        }
    }else if (type == 2) { // 단백질
        if (val < 7.f) {
            valColor = COLOR_MAIN_ORANGE;
        }else if (val >= 7.f && val <= 20.f) {
            valColor = COLOR_MAIN_GREEN;
        }else {
            valColor = COLOR_MAIN_RED;
        }
        
    }else { //지방
        if (val < 15.f) {
            valColor = COLOR_MAIN_ORANGE;
        }else if (val >= 15.f && val <= 30.f) {
            valColor = COLOR_MAIN_GREEN;
        }else {
            valColor = COLOR_MAIN_RED;
        }
    }
    
    NSString *valString = [NSString stringWithFormat:@"%.1f",totalVal];
    
    NSMutableAttributedString *valStr = [[NSMutableAttributedString alloc] initWithString:valString];
    [valStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, valString.length)];
    [valStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_MEDIUM(14)
                     range:NSMakeRange(0, valString.length)];
    [valStr addAttribute:NSForegroundColorAttributeName
                     value:valColor
                     range:NSMakeRange(0, valString.length)];
    [attstr appendAttributedString:valStr];
    
    NSMutableAttributedString *unitStr = [[NSMutableAttributedString alloc] initWithString:@"g"];
    [unitStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, @"g".length)];
    [unitStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_LIGHT(14)
                   range:NSMakeRange(0, @"g".length)];
    [unitStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_DARK
                   range:NSMakeRange(0, @"g".length)];
    [attstr appendAttributedString:unitStr];
    
    return attstr;
}

- (NSMutableAttributedString *)setEatKcalText:(NSString *)kcal {
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] initWithString:@""];
    
    if (kcal.length < 1) {
        return attstr;
    }
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *valStr = [[NSMutableAttributedString alloc] initWithString:kcal];
    [valStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, kcal.length)];
    [valStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_BOLD(13)
                   range:NSMakeRange(0, kcal.length)];
    [valStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_DARK
                   range:NSMakeRange(0, kcal.length)];
    [attstr appendAttributedString:valStr];
    
    NSMutableAttributedString *unitStr = [[NSMutableAttributedString alloc] initWithString:@" kcal"];
    [unitStr addAttribute:NSKernAttributeName
                        value:[NSNumber numberWithFloat:number]
                        range:NSMakeRange(0, @" kcal".length)];
    [unitStr addAttribute:NSFontAttributeName
                        value:FONT_NOTO_LIGHT(13)
                        range:NSMakeRange(0, @" kcal".length)];
    [unitStr addAttribute:NSForegroundColorAttributeName
                        value:COLOR_MAIN_DARK
                        range:NSMakeRange(0, @" kcal".length)];
    [attstr appendAttributedString:unitStr];
    
    return attstr;
}

- (void)infoLabelText {
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    CGFloat number = -1.5f;
    
    NSString *one = @"●     부족               ";
    NSMutableAttributedString *oneStr = [[NSMutableAttributedString alloc] initWithString:one];
    [oneStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, one.length)];
    [oneStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_REGULAR(10)
                   range:NSMakeRange(0, one.length)];
    [oneStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_ORANGE
                   range:NSMakeRange(0, one.length)];
    [attstr appendAttributedString:oneStr];
    
    NSString *two = @"●     적정               ";
    NSMutableAttributedString *twoStr = [[NSMutableAttributedString alloc] initWithString:two];
    [twoStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, two.length)];
    [twoStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_REGULAR(10)
                   range:NSMakeRange(0, two.length)];
    [twoStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_GREEN
                   range:NSMakeRange(0, two.length)];
    [attstr appendAttributedString:twoStr];
    
    NSString *three = @"●       과잉";
    NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:three];
    [threeStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, three.length)];
    [threeStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_REGULAR(10)
                   range:NSMakeRange(0, three.length)];
    [threeStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_RED
                   range:NSMakeRange(0, three.length)];
    [attstr appendAttributedString:threeStr];
    
    self.infoLbl.attributedText = attstr;
}

#pragma mark - middle bar layer
- (CALayer *)barGaugeLayer:(int)val total:(int)total size:(CGRect)size {
    
    UIColor *barColor = COLOR_MAIN_GREEN;
    if (val > total) {
        barColor = COLOR_MAIN_RED;
    }
    
    CGFloat widthPer = ((CGFloat)val / (CGFloat)total) * 100.f;
    if (widthPer != widthPer) {
        widthPer = 0.f;
    }else if (widthPer > 100.f) {
        widthPer = 100.f;
    }

    CGFloat layerWidth = (size.size.width / 100) * widthPer;
    
    CALayer *layer = [[CALayer alloc] init];
    layer.backgroundColor = barColor.CGColor;
    layer.frame = CGRectMake(0, 0, layerWidth, size.size.height);
    
    return layer;
}

- (void)mainDrawCirular:(int)val total:(int)total {
    CGFloat height = DEVICE_WIDTH * 0.3;
    
    CAShapeLayer *trackLayer = [[CAShapeLayer alloc] init];
    
    CGPoint center = CGPointMake(height/2, height/2);
    UIBezierPath *cirularPath = [[UIBezierPath alloc] init];
    
    [cirularPath addArcWithCenter:center radius:height/2 startAngle:(M_PI/2) endAngle:(M_PI * 2.5) clockwise:true];
    trackLayer.path = cirularPath.CGPath;
    trackLayer.strokeColor = RGBA(220, 220, 220, 1).CGColor;
    trackLayer.lineWidth = height / 11;
    trackLayer.fillColor = [UIColor clearColor].CGColor;
    trackLayer.lineCap = kCALineCapSquare;
    [self.mainKcalVw.layer addSublayer:trackLayer];
    
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.path = cirularPath.CGPath;
    self.nowKcalbl.textColor = COLOR_NATIONS_BLUE;
    shapeLayer.strokeColor = COLOR_NATIONS_BLUE.CGColor;
//    
//    if (val < total) {
//        self.nowKcalbl.textColor = COLOR_MAIN_GREEN;
//        shapeLayer.strokeColor = COLOR_MAIN_GREEN.CGColor;
//    }else {
//        self.nowKcalbl.textColor = COLOR_MAIN_RED;
//        shapeLayer.strokeColor = COLOR_MAIN_RED.CGColor;
//    }
    shapeLayer.lineWidth = height / 11;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.lineCap = kCALineCapSquare;
    
    [self.mainKcalVw.layer addSublayer:shapeLayer];
    
    shapeLayer.strokeEnd = 0;
    CABasicAnimation *basicAnimation = [[CABasicAnimation alloc] init];
    [basicAnimation setKeyPath:@"strokeEnd"];
    basicAnimation.toValue = [NSNumber numberWithFloat:((float)val/(float)total)];;
    basicAnimation.duration = 0;
    basicAnimation.fillMode = kCAFillModeForwards;
    [basicAnimation setRemovedOnCompletion:false];
    [shapeLayer addAnimation:basicAnimation forKey:@"urSoBasic"];
}

#pragma mark - UIImage
- (UIImage *)cropImage:(UIImage*)image {
    float h = image.size.height;
    float w = image.size.width;
    float sSize = h>w?w:h;
    float x = w>h?(w/2)-((w/2)/2):0;
    float y = h>w?(h/2)-((h/2)/2):0;
    
    CGRect rect = CGRectMake(x, y, sSize, sSize);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:1.0f orientation:UIImageOrientationUp];
    CGImageRelease(imageRef);
    return result;
}

#pragma mark - layer
- (void)layerMaskMake:(UIView *)view {
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = view.bounds;
    [layer setBackgroundColor:RGBA(0, 0, 0, 0.2).CGColor];
    [view.layer addSublayer:layer];
}

@end
