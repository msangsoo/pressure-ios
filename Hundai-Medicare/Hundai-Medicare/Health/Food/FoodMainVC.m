//
//  FoodMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FoodMainVC.h"

@interface FoodMainVC (){
    UIBarButtonItem *naviLeftBtn;
    UIView *bubbleView;
}

@end

@implementation FoodMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   [self.navigationItem setTitle:@"식사"];
    
    [self viewInit];
    [self navigationSetup];
    
    vcWrite = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodWriteVC"];
    [self addChildViewController:vcWrite];
    vcWrite.view.bounds = _vwMain.bounds;
    vcWrite.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcWrite.view];
    [vcWrite.view setHidden:false];
    
    vcHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodHistoryVC"];
    [self addChildViewController:vcHistory];
    vcHistory.view.bounds = _vwMain.bounds;
    vcHistory.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHistory.view];
    [vcHistory.view setHidden:true];
    
    [self.writeBtn setSelected:true];
    [self.historyBtn setSelected:false];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    if ([[Utils getUserDefault:HEALTH_MSG_NEW] isEqualToString:@"N"]) {
        [self bubbleHealthMsg];
    } else {
        UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
        healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [naviLeftBtn setImage:healthMsgImg];
        [bubbleView removeFromSuperview];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [bubbleView removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    if(sender.tag == 1 && !self.writeBtn.isSelected) {
        
        
        [self.writeBtn setSelected:true];
        [self.historyBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [vcWrite.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        //        [vcFW calViewInit];
    }else if(sender.tag == 2 && !self.historyBtn.isSelected) {
        
        
        [self.writeBtn setSelected:false];
        [self.historyBtn setSelected:true];
        
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcWrite.view setHidden:true];
        [vcHistory.view setHidden:false];
        //        [vcFH viewInit];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.writeBtn.tag = 1;
    [self.writeBtn setTitle:@"기록하기" forState:UIControlStateNormal];
    [self.writeBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.writeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.historyBtn.tag = 2;
    [self.historyBtn setTitle:@"히스토리" forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    [self.writeBtn setSelected:true];
    [self.historyBtn setSelected:false];
    
    if(self.writeBtn.isSelected) {
        self.tabBarVwLeftConst.constant = 0;
        
        [vcWrite.view setHidden:false];
        [vcHistory.view setHidden:true];
        //        [vcFW calViewInit];
    }
}

- (void)navigationSetup {
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naviLeftBtn = [[UIBarButtonItem alloc] init];
    [naviLeftBtn setStyle:UIBarButtonItemStylePlain];
    [naviLeftBtn setImage:healthMsgImg];
    [naviLeftBtn setTarget:self];
    [naviLeftBtn setAction:@selector(pressHealthMsg:)];
    self.navigationItem.leftBarButtonItem = naviLeftBtn;
}

- (void)bubbleHealthMsg {
    CGFloat yPos = IS_IPHONEX ? 80 : 55;
    bubbleView = [[UIView alloc] initWithFrame:CGRectMake(20 ,yPos ,150 ,28)];
    
    bubbleView.layer.borderWidth = 1;
    bubbleView.layer.borderColor = COLOR_MAIN_DARK.CGColor;
    bubbleView.layer.cornerRadius = 14;
    bubbleView.layer.masksToBounds = true;
    bubbleView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 115, 20)];
    lbl.textColor = [UIColor blackColor];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.lineBreakMode = NSLineBreakByTruncatingTail;
    lbl.numberOfLines = 1;
    lbl.font = FONT_NOTO_MEDIUM(12);
    lbl.textColor = COLOR_MAIN_DARK;
    [bubbleView addSubview:lbl];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 120, 25)];
    [btn setTitle:@"" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:self action:@selector(pressHealthMsg:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:btn];
    
    UIButton *closebtn = [[UIButton alloc] initWithFrame:CGRectMake(125, 4.5, 25, 20)];
    [closebtn setImage:[UIImage imageNamed:@"path"] forState:UIControlStateNormal];
    [closebtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    [closebtn addTarget:self action:@selector(pressHealthMsgClose:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:closebtn];
    
    [lbl setNotoText:@"건강메시지가 도착했습니다."];
    [lbl sizeToFit];
    //    [self.view addSubview:bubbleView];
    [self.navigationController.view addSubview:bubbleView];
    
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_on_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
}


- (IBAction)pressHealthMsg:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
    [bubbleView removeFromSuperview];
    
    UIViewController *vc = [HEALTHMESSAGE_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressHealthMsgClose:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
    [bubbleView removeFromSuperview];
}
@end
