//
//  FoodShareKindAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 31/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodShareKindAlertVC : UIViewController

@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UIButton *select1Btn;
@property (weak, nonatomic) IBOutlet UIButton *select2Btn;
@property (weak, nonatomic) IBOutlet UIButton *select3Btn;
@property (weak, nonatomic) IBOutlet UIButton *select4Btn;
@property (weak, nonatomic) IBOutlet UIButton *select5Btn;
@property (weak, nonatomic) IBOutlet UIButton *select6Btn;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end

@protocol FoodShareKindAlertVCDelegate
- (void)selected:(int)index;
@end
