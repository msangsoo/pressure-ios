//
//  FoodHistoryVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "FoodMainDBWraper.h"
#import "FoodDetailDBWraper.h"

#import "BaseViewController.h"
#import "FoodGraphZoomVC.h"

//바차트관련
#import "SBCEViewController.h"

//방사형관련
#import "PNRadarChartDataItem.h"
#import "PNRadarChart.h"

@interface FoodHistoryVC : BaseViewController {
    TimeUtils *timeUtils;
    FoodMainDBWraper *db;
    FoodDetailDBWraper *fddb;
}

@property (nonatomic) PNRadarChart *radarChart;

@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Bar Graph */
@property (weak, nonatomic) IBOutlet UIView *barGraphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphInfoLbl;

/* Bottom */
@property (weak, nonatomic) IBOutlet UIView *kcalVw;
@property (weak, nonatomic) IBOutlet UILabel *kcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalBreakLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalBreakValLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalLunchLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalLunchValLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalDinnerLbl;
@property (weak, nonatomic) IBOutlet UILabel *kcalDinnerValLbl;

@property (weak, nonatomic) IBOutlet UIView *avgVw;;
@property (weak, nonatomic) IBOutlet UILabel *avgLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgBreakLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgBreakValLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgLunchLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgLunchValLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgDinnerLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgDinnerValLbl;

/* Empty */
@property (weak, nonatomic) IBOutlet UIView *emtpyVw;
@property (weak, nonatomic) IBOutlet UILabel *emtpyLbl;

/* Radar Graph */
@property (weak, nonatomic) IBOutlet UILabel *radarLbl;
@property (weak, nonatomic) IBOutlet UIView *radarGraphVw;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConst;

@end
