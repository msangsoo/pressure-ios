//
//  FoodShareKindAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 31/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FoodShareKindAlertVC.h"

@interface FoodShareKindAlertVC () {
    int seleted;
}

@end

@implementation FoodShareKindAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    seleted = -1;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismiss:nil];
}

- (IBAction)pressSeleted:(UIButton *)sender {
    self.select1Btn.selected = false;
    self.select2Btn.selected = false;
    self.select3Btn.selected = false;
    self.select4Btn.selected = false;
    self.select5Btn.selected = false;
    self.select6Btn.selected = false;
    
    if (sender.tag == 0) {
        self.select1Btn.selected = true;
        seleted = 0;
    }else if (sender.tag == 1) {
        self.select2Btn.selected = true;
        seleted = 1;
    }else if (sender.tag == 2) {
        self.select3Btn.selected = true;
        seleted = 2;
    }else if (sender.tag == 3) {
        self.select4Btn.selected = true;
        seleted = 3;
    }else if (sender.tag == 4) {
        self.select5Btn.selected = true;
        seleted = 4;
    }else if (sender.tag == 5) {
        self.select6Btn.selected = true;
        seleted = 5;
    }
}

- (IBAction)pressNext:(id)sender {
    if (seleted >= 0) {
        [self dismiss:^{
            if (self.delegate) {
                [self.delegate selected:self->seleted];
            }
        }];
    }else {
        [self dismiss:^{
            if (self.delegate) {
                [self.delegate selected:-1];
            }
        }];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.titleLbl.font = FONT_NOTO_MEDIUM(18);
    self.titleLbl.textColor = COLOR_MAIN_DARK;
    [self.titleLbl setNotoText:@"커뮤니티에 공유하실\n식사를 선택하세요."];
    
    self.select1Btn.tag = 0;
    [self.select1Btn setTitle:@"아침" forState:UIControlStateNormal];
    [self.select1Btn foodKindSelectedBtn];
    
    self.select2Btn.tag = 1;
    [self.select2Btn setTitle:@"아침간식" forState:UIControlStateNormal];
    [self.select2Btn foodKindSelectedBtn];
    
    self.select3Btn.tag = 2;
    [self.select3Btn setTitle:@"점심" forState:UIControlStateNormal];
    [self.select3Btn foodKindSelectedBtn];
    
    self.select4Btn.tag = 3;
    [self.select4Btn setTitle:@"점심간식" forState:UIControlStateNormal];
    [self.select4Btn foodKindSelectedBtn];
    
    self.select5Btn.tag = 4;
    [self.select5Btn setTitle:@"저녁" forState:UIControlStateNormal];
    [self.select5Btn foodKindSelectedBtn];
    
    self.select6Btn.tag = 5;
    [self.select6Btn setTitle:@"저녁간식" forState:UIControlStateNormal];
    [self.select6Btn foodKindSelectedBtn];
    
    self.cancelBtn.tag = 1;
    [self.cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [self.cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    self.confirmBtn.tag = 2;
    [self.confirmBtn setTitle:@"다음" forState:UIControlStateNormal];
    [self.confirmBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

@end
