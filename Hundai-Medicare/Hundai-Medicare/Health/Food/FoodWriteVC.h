//
//  FoodWriteVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "BaseViewController.h"
#import "FoodInputViewController.h"
#import "FoodShareKindAlertVC.h"

#import "TimeUtils.h"
#import "FoodMainDBWraper.h"
#import "FoodDetailDBWraper.h"

#import "FeedModel.h"
#import "CommunityWriteViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"

@interface FoodWriteVC : BaseViewController<FoodShareKindAlertVCDelegate, BasicAlertVCDelegate> {
    TimeUtils *timeUtils;
    FoodMainDBWraper *_db;
    FoodDetailDBWraper *_foodDetailDB;
    
    NSArray *_arrFoodMain;
    
    NSString *_selfMealType;
}

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UIView *mainKcalVw;
@property (weak, nonatomic) IBOutlet UILabel *nowKcalbl;
@property (weak, nonatomic) IBOutlet UILabel *totalKcallbl;

@property (weak, nonatomic) IBOutlet UILabel *carbohydrateLbl;
@property (weak, nonatomic) IBOutlet UILabel *carbohydrateValLbl;

@property (weak, nonatomic) IBOutlet UILabel *proteinLbl;
@property (weak, nonatomic) IBOutlet UILabel *proteinValLbl;

@property (weak, nonatomic) IBOutlet UILabel *fatLbl;
@property (weak, nonatomic) IBOutlet UILabel *fatValLbl;

@property (weak, nonatomic) IBOutlet UILabel *infoLbl;

@property (weak, nonatomic) IBOutlet UIImageView *rice1IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *rice1KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *rice1Lbl;

@property (weak, nonatomic) IBOutlet UIImageView *rice2IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *rice2KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *rice2Lbl;

@property (weak, nonatomic) IBOutlet UIImageView *rice3IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *rice3KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *rice3Lbl;

@property (weak, nonatomic) IBOutlet UIImageView *snack1IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *snack1KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *snack1Lbl;

@property (weak, nonatomic) IBOutlet UIImageView *snack2IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *snack2KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *snack2Lbl;

@property (weak, nonatomic) IBOutlet UIImageView *snack3IconImgVw;
@property (weak, nonatomic) IBOutlet UILabel *snack3KcalLbl;
@property (weak, nonatomic) IBOutlet UILabel *snack3Lbl;

@end
