//
//  FoodGraphZoomVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 31/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "FoodMainDBWraper.h"

#import "BaseViewController.h"

//바차트관련
#import "SBCEViewController.h"

@interface FoodGraphZoomVC : BaseViewController {
    TimeUtils *timeUtils;
    FoodMainDBWraper *db;
}

@property (assign) int tapTag;
@property (nonatomic, strong) TimeUtils *timeUtilsData;


/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

/* Bar Graph */
@property (weak, nonatomic) IBOutlet UIView *barGraphVw;

@property (weak, nonatomic) IBOutlet UIImageView *imageRotate;

@end
