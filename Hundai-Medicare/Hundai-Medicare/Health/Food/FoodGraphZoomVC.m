//
//  FoodGraphZoomVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 31/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FoodGraphZoomVC.h"

@interface FoodGraphZoomVC () {
    NSArray *barChartData;
}

@end

@implementation FoodGraphZoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[FoodMainDBWraper alloc] init];
    
    if (self.tapTag == 0) {
        self.tapTag = 10;
    }
    
    [self viewInit];
    
    
    self.imageRotate.alpha = 0.3;
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.imageRotate.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0 delay:0.7 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.imageRotate.alpha = 0;
            } completion:^(BOOL finished) {
                self.imageRotate.hidden = YES;
            }];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self timeButtonInit];
    [self dataInit];
    [self BarChartInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(self.tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(self.tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (self.tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    UIImage *closeImg = [[UIImage imageNamed:@"btn_circle_gray_cancel"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:closeImg forState:UIControlStateNormal];
    [self.closeBtn setTintColor:COLOR_GRAY_SUIT];
    
    timeUtils = self.timeUtilsData;
}

#pragma mark - chart
- (void)dataInit {
    if(self.tapTag == 10) {
        barChartData = [db getDayResult:[timeUtils getNowTime]];
        if(barChartData.count < 1){
            barChartData = @[@0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0];
        }
    }else if(self.tapTag == 11) {
        barChartData = [db getWeekResult:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        if(barChartData.count < 1){
            barChartData = @[@0,@0,@0,@0,@0,@0,@0];
        }
    }else if(self.tapTag == 12) {
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        
        barChartData = [db getMonthResult:year nMonth:month];
        if(barChartData.count < 1){
            barChartData = @[@0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,@0];
        }
    }
}

- (void)BarChartInit {
    UIView *viewToRemove = [_barGraphVw viewWithTag:111];
    [viewToRemove removeFromSuperview];
    
    SBCEViewController *bar = [[SBCEViewController alloc] init];
    SimpleBarChart *barChart;
    if(self.tapTag == 10) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_DAY xCount:24];
    }else if(self.tapTag == 11) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_WEEK xCount:7];
    }else if(self.tapTag == 12) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height)
                            items:barChartData chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
    }
    [bar reloadData];
    
    barChart.unit = @"kcal";
    barChart.tag = 111;
    [_barGraphVw addSubview:barChart];
}

#pragma mark - UIInterfaceOrientationMask
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)updatePreviewLayer:(AVCaptureConnection*)layer orientation:(AVCaptureVideoOrientation)orientation {
    layer.videoOrientation = orientation;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:false block:^(NSTimer * _Nonnull timer) {
        
        [self viewSetup];
    }];
}

@end
