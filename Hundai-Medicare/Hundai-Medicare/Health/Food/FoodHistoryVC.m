//
//  FoodHistoryVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "FoodHistoryVC.h"

@interface FoodHistoryVC () {
    int tapTag;
    
    int orgintargetCar;
    int targetCar;
    int eatCar;
    NSArray *barChartData;
}

@end

@implementation FoodHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[FoodMainDBWraper alloc] init];
    fddb = [[FoodDetailDBWraper alloc] init];
    timeUtils = [[TimeUtils alloc] init];
    
    tapTag = 10;
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressZoom:(id)sender {
    FoodGraphZoomVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodGraphZoomVC"];
    vc.tapTag = tapTag;
    vc.timeUtilsData = timeUtils;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressTab:(UIButton *)sender {
    BOOL pageChagneCheck = false;
    
    if(sender.tag == 1 && !_dayBtn.isSelected) {
        _dayBtn.selected = true;
        _weekBtn.selected = false;
        _monthBtn.selected = false;
        
        [self.kcalLbl setText:@"일간 영양 섭취 칼로리"];
        [self.avgLbl setText:@"일간 평균 식사 소요시간"];
        [self.radarLbl setText:@"일간 영양 섭취 균형도"];
        
        [timeUtils setTimeType:PERIOD_DAY];
        tapTag = 10;
        pageChagneCheck = true;
    }else if (sender.tag == 2 && !_weekBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = true;
        _monthBtn.selected = false;
        
        [self.kcalLbl setText:@"주간 영양 섭취 칼로리"];
        [self.avgLbl setText:@"주간 평균 식사 소요시간"];
        [self.radarLbl setText:@"주간 영양 섭취 균형도"];
        
        [timeUtils setTimeType:PERIOD_WEEK];
        tapTag = 11;
        pageChagneCheck = true;
    }else if (sender.tag == 3 && !_monthBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = false;
        _monthBtn.selected = true;
        
        [self.kcalLbl setText:@"월간 영양 섭취 칼로리"];
        [self.avgLbl setText:@"월간 평균 식사 소요시간"];
        [self.radarLbl setText:@"월간 영양 섭취 균형도"];
        
        [timeUtils setTimeType:PERIOD_MONTH];
        tapTag = 12;
        pageChagneCheck = true;
    }
    
    if (pageChagneCheck) { //Chart view init
        [self viewSetup];
    }
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - view Setup
- (void)viewSetup {
    [self timeButtonInit];
    [self dataInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }
}

- (void)dataInit {
    orgintargetCar = [_model getPregnancyRecommendCal];
    
    BOOL dataIsEmpty = false;
    
    if(tapTag == 10) {
        targetCar = orgintargetCar;
        
        barChartData = [db getDayResult:[timeUtils getNowTime]];
        if(barChartData.count < 1) {
            dataIsEmpty = true;
            barChartData = @[@0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0];
        }
    }else if(tapTag == 11) {
        targetCar = orgintargetCar * 7;
        barChartData = [db getWeekResult:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        
        if(barChartData.count < 1) {
            dataIsEmpty = true;
            barChartData = @[@0,@0,@0,@0,@0,@0,@0];
        }else {
            dataIsEmpty = true;
            for (int i = 0 ; i < barChartData.count ; i ++) {
                int val = [[barChartData objectAtIndex:i] intValue];
                if (val != 0) {
                    dataIsEmpty = false;
                }
            }
        }
        
        
    }else if(tapTag == 12) {
        
        targetCar = orgintargetCar * (int)[timeUtils monthMaxDay];
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        
        barChartData = [db getMonthResult:year nMonth:month];
        if(barChartData.count < 1) {
            dataIsEmpty = true;
            barChartData = @[@0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,
                             @0,@0,@0,@0,@0,@0,@0];
        }else {
            dataIsEmpty = true;
            for (int i = 0 ; i < barChartData.count ; i ++) {
                int val = [[barChartData objectAtIndex:i] intValue];
                if (val != 0) {
                    dataIsEmpty = false;
                }
            }
        }
    }
    
    if(dataIsEmpty) {
        _emtpyVw.hidden = false;
        _contentViewHeightConst.constant = _emtpyVw.layer.frame.origin.y + 70.f;
        [self lblTextinit];
        [self BarChartInit];
    }else {
        _emtpyVw.hidden = true;
        _contentViewHeightConst.constant = _radarGraphVw.layer.frame.origin.y + _radarGraphVw.layer.frame.size.height + 15.f;
        
        [self lblTextinit];
        [self BarChartInit];
        [self RadarChartInit];
    }
}

- (void)lblTextinit {
    //권장 섭취량
    NSNumber *num1 = [NSNumber numberWithInt:targetCar];
    NSString *numberStr = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterDecimalStyle];
    
    NSMutableArray *bottomArr = [[NSMutableArray alloc] init];
    if(tapTag == 10) {
        bottomArr = [db getMealSum:[timeUtils getNowTime] eDate:[timeUtils getNowTime]];
    }else if(tapTag == 11) {
        bottomArr = [db getMealSum:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
    }else if(tapTag == 12) {
        bottomArr = [db getMealSum:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
    }
    
    if(bottomArr.count > 0) {
        NSDictionary *dic = [bottomArr objectAtIndex:0];
        [_kcalBreakValLbl setText:[NSString stringWithFormat:@"%.0f kcal", [[dic objectForKey:@"breakfast"] floatValue]]];
        [_kcalLunchValLbl setText:[NSString stringWithFormat:@"%.0f kcal", [[dic objectForKey:@"lunch"] floatValue]]];
        [_kcalDinnerValLbl setText:[NSString stringWithFormat:@"%.0f kcal", [[dic objectForKey:@"dinner"] floatValue]]];
        
        [_avgBreakValLbl setText:[NSString stringWithFormat:@"%.0f 분", [[dic objectForKey:@"break_time"] floatValue]]];
        [_avgLunchValLbl setText:[NSString stringWithFormat:@"%.0f 분", [[dic objectForKey:@"lunch_time"] floatValue]]];
        [_avgDinnerValLbl setText:[NSString stringWithFormat:@"%.0f 분", [[dic objectForKey:@"dinner_time"] floatValue]]];
        //        float eatSum = [[dic objectForKey:@"breakfast"] floatValue] + [[dic objectForKey:@"lunch"] floatValue] + [[dic objectForKey:@"dinner"] floatValue];
        int eatSum = [[dic objectForKey:@"breakfast"] intValue] + [[dic objectForKey:@"lunch"] intValue] + [[dic objectForKey:@"dinner"] intValue];
        eatCar = eatSum;
        
        num1 = [NSNumber numberWithInt:eatSum];
        NSString *numberStr2 = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterDecimalStyle];
        [self setGraphInfoText:[NSString stringWithFormat:@"%@", numberStr2] totalVal:[NSString stringWithFormat:@"%@", numberStr]];
    }else{
        [_kcalBreakValLbl setText:@"0 kcal"];
        [_kcalLunchValLbl setText:@"0 kcal"];
        [_kcalDinnerValLbl setText:@"0 kcal"];
        [_avgBreakValLbl setText:@"0 분"];
        [_avgLunchValLbl setText:@"0 분"];
        [_avgDinnerValLbl setText:@"0 분"];
        [self setGraphInfoText:@"0" totalVal:[NSString stringWithFormat:@"%@", numberStr]];
        eatCar = 0;
    }
}

#pragma mark - chart
- (void)BarChartInit {
    UIView *viewToRemove = [_barGraphVw viewWithTag:111];
    [viewToRemove removeFromSuperview];
    
    SBCEViewController *bar = [[SBCEViewController alloc] init];
    SimpleBarChart *barChart;
    if(tapTag == 10) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_DAY xCount:24];
    }else if(tapTag == 11) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height) items:barChartData chartType:CHART_TYPE_WEEK xCount:7];
    }else if(tapTag == 12) {
        barChart = [bar loadChart:CGRectMake(5, 0, _barGraphVw.layer.frame.size.width - 10 , _barGraphVw.layer.frame.size.height)
                            items:barChartData chartType:CHART_TYPE_MONTH xCount:timeUtils.monthMaxDay];
    }
    [bar reloadData];
    
    barChart.unit = @"kcal";
    barChart.tag = 111;
    [_barGraphVw addSubview:barChart];
}

- (void)RadarChartInit {
    UIView *viewToRemove = [self.radarGraphVw viewWithTag:111];
    [viewToRemove removeFromSuperview];
    
    NSArray *arr = [db getRadial:[timeUtils getNowTime] eDate:[timeUtils getEndTime] totTakeCal:(float)eatCar recomCal:(float)targetCar];
    
    // 방사형 그래프 수치는 항상 100, 66.66, 33.33 의 3가지 형태로 되어야 함.
    NSArray *items;
    if(arr.count > 0) {
        NSDictionary *dicTemp = [arr objectAtIndex:0];
        
        items = @[[PNRadarChartDataItem dataItemWithValue:[[dicTemp objectForKey:@"gSalt"] floatValue] description:@"나트륨"],
                  [PNRadarChartDataItem dataItemWithValue:[[dicTemp objectForKey:@"gFat"] floatValue] description:@"지방"],
                  [PNRadarChartDataItem dataItemWithValue:[[dicTemp objectForKey:@"gCarbohydrate"] floatValue] description:@"탄수화물"],
                  [PNRadarChartDataItem dataItemWithValue:[[dicTemp objectForKey:@"gCalorie"] floatValue] description:@"열량"],
                  [PNRadarChartDataItem dataItemWithValue:[[dicTemp objectForKey:@"gProtein"] floatValue] description:@"단백질"]];
        
        //        items = @[[PNRadarChartDataItem dataItemWithValue:33.30 description:@"나트륨"],
        //                  [PNRadarChartDataItem dataItemWithValue:33.30 description:@"지방"],
        //                  [PNRadarChartDataItem dataItemWithValue:33.30 description:@"탄수화물"],
        //                  [PNRadarChartDataItem dataItemWithValue:33.30 description:@"열량"],
        //                  [PNRadarChartDataItem dataItemWithValue:33.60 description:@"단백질"]];
    }else{
        items = @[[PNRadarChartDataItem dataItemWithValue:0 description:@"나트륨"],
                  [PNRadarChartDataItem dataItemWithValue:0 description:@"지방"],
                  [PNRadarChartDataItem dataItemWithValue:0 description:@"탄수화물"],
                  [PNRadarChartDataItem dataItemWithValue:0 description:@"열량"],
                  [PNRadarChartDataItem dataItemWithValue:0 description:@"단백질"]];
    }
    
    self.radarChart = [[PNRadarChart alloc] initWithFrame:CGRectMake(-20, 0, DEVICE_WIDTH, 300.0)
                                                    items:items valueDivider:33.33];
    
    self.radarChart.plotColor = [UIColor colorWithRed: 0.5 green: 0.5 blue: 0.5 alpha: 0.5];
    
    [self.radarChart strokeChart];
    self.radarChart.tag = 111;
    [self.radarGraphVw addSubview:self.radarChart];
    
    //    // 방사형 그래프 수치는 항상 100, 66.66, 33.33 의 3가지 형태로 되어야 함.
    //    NSArray *items = @[[PNRadarChartDataItem dataItemWithValue:0 description:@"나트륨"], // 순서주의
    //                       [PNRadarChartDataItem dataItemWithValue:0 description:@"지방"],
    //                       [PNRadarChartDataItem dataItemWithValue:0 description:@"탄수화물"],
    //                       [PNRadarChartDataItem dataItemWithValue:0 description:@"열량"],
    //                       [PNRadarChartDataItem dataItemWithValue:0 description:@"단백질"],
    //                       ];
    //    self.radarChart = [[PNRadarChart alloc] initWithFrame:CGRectMake(0, 235.0, SCREEN_WIDTH, 300.0)
    //                                                    items:items valueDivider:33.33];
    //
    //    self.radarChart.plotColor = [UIColor colorWithRed: 0.5 green: 0.5 blue: 0.5 alpha: 0.5];
    //
    //    [self.radarChart strokeChart];
    //
    //    [self.view addSubview:self.radarChart];
}

#pragma mark - viewInit
- (void)viewInit {
    _dayBtn.tag = 1;
    [_dayBtn setTitle:@"일간" forState:UIControlStateNormal];
    [_dayBtn mediHealthTabButton];
    
    _weekBtn.tag = 2;
    [_weekBtn setTitle:@"주간" forState:UIControlStateNormal];
    [_weekBtn mediHealthTabButton];
    
    _monthBtn.tag = 3;
    [_monthBtn setTitle:@"월간" forState:UIControlStateNormal];
    [_monthBtn mediHealthTabButton];
    
    _dayBtn.selected = true;
    
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _graphInfoLbl.text = @"";
    
    _kcalVw.layer.borderWidth = 1;
    _kcalVw.layer.borderColor = RGB(213, 213, 217).CGColor;

    _kcalLbl.font = FONT_NOTO_MEDIUM(18);
    _kcalLbl.textColor = COLOR_NATIONS_BLUE;
    [_kcalLbl setNotoText:@"일간 영양 섭취 칼로리"];
    
    _kcalBreakLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalBreakLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalBreakLbl setNotoText:@"아침"];
    
    _kcalBreakValLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalBreakValLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalBreakValLbl setNotoText:@"30,000 kcal"];
    
    _kcalLunchLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalLunchLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalLunchLbl setNotoText:@"점심"];
    
    _kcalLunchValLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalLunchValLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalLunchValLbl setNotoText:@"3,000 kcal"];
    
    _kcalDinnerLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalDinnerLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalDinnerLbl setNotoText:@"저녁"];
    
    _kcalDinnerValLbl.font = FONT_NOTO_MEDIUM(16);
    _kcalDinnerValLbl.textColor = COLOR_GRAY_SUIT;
    [_kcalDinnerValLbl setNotoText:@"300 kcal"];
    
    _avgVw.layer.borderWidth = 1;
    _avgVw.layer.borderColor = RGB(213, 213, 217).CGColor;
    
    _avgLbl.font = FONT_NOTO_MEDIUM(18);
    _avgLbl.textColor = COLOR_NATIONS_BLUE;
    [_avgLbl setNotoText:@"일간 평균 식사 소요시간"];
    
    _avgBreakLbl.font = FONT_NOTO_MEDIUM(16);
    _avgBreakLbl.textColor = COLOR_GRAY_SUIT;
    [_avgBreakLbl setNotoText:@"아침"];
    
    _avgBreakValLbl.font = FONT_NOTO_MEDIUM(16);
    _avgBreakValLbl.textColor = COLOR_GRAY_SUIT;
    [_avgBreakValLbl setNotoText:@"20,000 kcal"];
    
    _avgLunchLbl.font = FONT_NOTO_MEDIUM(16);
    _avgLunchLbl.textColor = COLOR_GRAY_SUIT;
    [_avgLunchLbl setNotoText:@"점심"];
    
    _avgLunchValLbl.font = FONT_NOTO_MEDIUM(16);
    _avgLunchValLbl.textColor = COLOR_GRAY_SUIT;
    [_avgLunchValLbl setNotoText:@"2,000 kcal"];
    
    _avgDinnerLbl.font = FONT_NOTO_MEDIUM(16);
    _avgDinnerLbl.textColor = COLOR_GRAY_SUIT;
    [_avgDinnerLbl setNotoText:@"저녁"];
    
    _avgDinnerValLbl.font = FONT_NOTO_MEDIUM(16);
    _avgDinnerValLbl.textColor = COLOR_GRAY_SUIT;
    [_avgDinnerValLbl setNotoText:@"200 kcal"];
    
    _radarLbl.font = FONT_NOTO_MEDIUM(18);
    _radarLbl.textColor = COLOR_NATIONS_BLUE;
    [_radarLbl setNotoText:@"일간 영양 섭취 균형도"];
    
    _emtpyVw.hidden = true;
    
    _emtpyLbl.font = FONT_NOTO_LIGHT(16);
    _emtpyLbl.textColor = COLOR_NATIONS_BLUE;
    [_emtpyLbl setNotoText:@"식사 기록 데이터가 없습니다."];
}

#pragma mark - graph info text
- (void)setGraphInfoText:(NSString *)val totalVal:(NSString *)totalVal {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *valStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@kcal", val]];
    [valStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", val].length)];
    [valStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_MEDIUM(20)
                     range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", val].length)];
    [valStr addAttribute:NSForegroundColorAttributeName
                     value:RGB(34, 172, 56)
                     range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", val].length)];
    [attstr appendAttributedString:valStr];
    
    NSMutableAttributedString *valHintStr = [[NSMutableAttributedString alloc] initWithString:@"(섭취) / "];
    [valHintStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, @"(섭취) / ".length)];
    [valHintStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_THIN(16)
                   range:NSMakeRange(0, @"(섭취) / ".length)];
    [valHintStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_GRAY_SUIT
                   range:NSMakeRange(0, @"(섭취) / ".length)];
    [attstr appendAttributedString:valHintStr];
    
    NSMutableAttributedString *totalValStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@kcal", totalVal]];
    [totalValStr addAttribute:NSKernAttributeName
                   value:[NSNumber numberWithFloat:number]
                   range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", totalVal].length)];
    [totalValStr addAttribute:NSFontAttributeName
                   value:FONT_NOTO_MEDIUM(20)
                   range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", totalVal].length)];
    [totalValStr addAttribute:NSForegroundColorAttributeName
                   value:COLOR_MAIN_DARK
                   range:NSMakeRange(0, [NSString stringWithFormat:@"%@kcal", totalVal].length)];
    [attstr appendAttributedString:totalValStr];
    
    NSMutableAttributedString *totalValHintStr = [[NSMutableAttributedString alloc] initWithString:@"(권장)"];
    [totalValHintStr addAttribute:NSKernAttributeName
                       value:[NSNumber numberWithFloat:number]
                       range:NSMakeRange(0, @"(권장)".length)];
    [totalValHintStr addAttribute:NSFontAttributeName
                       value:FONT_NOTO_THIN(16)
                       range:NSMakeRange(0, @"(권장)".length)];
    [totalValHintStr addAttribute:NSForegroundColorAttributeName
                       value:COLOR_GRAY_SUIT
                       range:NSMakeRange(0, @"(권장)".length)];
    [attstr appendAttributedString:totalValHintStr];
    
    _graphInfoLbl.attributedText = attstr;
}

@end
