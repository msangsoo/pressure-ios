//
//  PresuGraphVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PresuGraphVC.h"

@interface PresuGraphVC () {
    int tapTag;
    
    NSString *OCM_SEQ;
    
    //char data
    NSArray *xItem;
    NSArray *yItem;
    NSArray *xTempData;
    NSArray *xMedicine;
    
    NSArray *xData;
    NSArray *xAfter_Data;
    
    NSArray *xOldDrawData;
    NSArray *xNewBefore_DrawData;
    NSArray *xNewAfter_DrawData;
    
    NSArray *xDrugData;
    NSArray *xDrugAfter_Data;
    NSArray *xDrugNewBefore_DrawData;
    
    NSArray *msgArr;
    NSString *healthMsg;
}

@end

@implementation PresuGraphVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    OCM_SEQ = @"";
    
    timeUtils = [[TimeUtils alloc] init];
    db = [[PresuDBWraper alloc] init];
    msgdb = [[MessageDBWraper alloc] init];
    tapTag = 10;
    healthMsg = @"";
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    _contentViewHeightConst.constant = _bottomVw.layer.frame.origin.y + _bottomVw.layer.frame.size.height + 15.f;
}

#pragma mark - Actions
- (IBAction)pressZoom:(id)sender {
    PresuGraphZoomVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PresuGraphZoomVC"];
    vc.tapTag = tapTag;
    vc.timeUtilsData = timeUtils;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:vc animated:UIModalPresentationFullScreen completion:nil];
}

- (IBAction)pressTab:(UIButton *)sender {
    BOOL pageChagneCheck = false;
    
    if(sender.tag == 1 && !_dayBtn.isSelected) {
        _dayBtn.selected = true;
        _weekBtn.selected = false;
        _monthBtn.selected = false;
        
        [timeUtils setTimeType:PERIOD_DAY];
        tapTag = 10;
        
        pageChagneCheck = true;
    }else if (sender.tag == 2 && !_weekBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = true;
        _monthBtn.selected = false;
        
        [timeUtils setTimeType:PERIOD_WEEK];
        tapTag = 11;
        
        pageChagneCheck = true;
    }else if (sender.tag == 3 && !_monthBtn.isSelected) {
        _dayBtn.selected = false;
        _weekBtn.selected = false;
        _monthBtn.selected = true;
        
        [timeUtils setTimeType:PERIOD_MONTH];
        tapTag = 12;
        
        pageChagneCheck = true;
    }
    
    if (pageChagneCheck) { //Chart view init
        [self viewSetup];
    }
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

- (IBAction)pressInput:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PresuInputVC"];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

//- (IBAction)pressShare:(id)sender {
//    OCM_SEQ = @"";
//    if (_model.getLoginData.nickname == nil || _model.getLoginData.nickname.length == 0) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
//
//        NickNameInputAlertViewController *vc = (NickNameInputAlertViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NickNameInputAlertViewController"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        vc.touchedCancelButton = ^{
//            [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
//        };
//        vc.touchedDoneButton = ^(NSString *nickName, BOOL isPublic) {
//            NSString *diseaseOpen = isPublic == TRUE ? @"Y" : @"N";
//            NSString *seq = self->_model.getLoginData.mber_sn;
//
//            [CommunityNetwork requestSetWithConfirmNickname:@{} seq:seq nickName:nickName diseaseOpen:diseaseOpen response:^(id responseObj, BOOL isSuccess) {
//                if (isSuccess == TRUE) {
//                    [self.navigationController.tabBarController dismissViewControllerAnimated:TRUE completion:nil];
//
//                    [Utils setUserDefault:NICKNAME_FIRST_INIT value:@"Y"];
//                    self->_model.getLoginData.nickname = [NSString stringWithString:nickName];
//                    FeedModel *feed = [[FeedModel alloc] init];
//                    feed.mberSn = [self->_model getLoginData].mber_sn;
//                    NSString *meal = [NSString stringWithFormat:@"평균 혈압 %@ / %@ mmHg", self.avgSystolicValLbl.text, self.avgDiastoleValLbl.text];
//                    feed.cmMeal = meal;
//
//                    [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
//                        if(success) {
//                            self->OCM_SEQ = response[@"OCM_SEQ"];
//                            [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
//                        }
//                    }];
//                }else {
//
//                }
//            }];
//        };
//        [self.navigationController.tabBarController presentViewController:vc animated:TRUE completion:nil];
//    }else {
//
//
//        FeedModel *feed = [[FeedModel alloc] init];
//        feed.mberSn = [self->_model getLoginData].mber_sn;
//        NSString *meal = [NSString stringWithFormat:@"평균 혈압 %@ / %@ mmHg", self.avgSystolicValLbl.text, self.avgDiastoleValLbl.text];
//        feed.cmMeal = meal;
//
//        [CommunityWriteViewController showContentWriteViewController:self.navigationController.tabBarController feed:[feed copy] isShare:true completion:^(BOOL success, id  _Nonnull response) {
//            if(success) {
//                self->OCM_SEQ = response[@"OCM_SEQ"];
//                [AlertUtils BasicAlertShow:self tag:2000 title:@"" msg:@"커뮤니티에 공유되었습니다." left:@"확인" right:@"게시글 보기"];
//            }
//        }];
//    }
//}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 2000) {
        if (tag == 2) {
            if (![OCM_SEQ isEqualToString:@""]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CommunityPaul" bundle:nil];
                FeedDetailViewController *feedDetailVC = (FeedDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
                feedDetailVC.seq = _model.getLoginData.mber_sn;
                feedDetailVC.feedSeq = OCM_SEQ;
                [self.navigationController pushViewController:feedDetailVC animated:TRUE];
            }
        }
    }
}

#pragma mark - viewSetup
- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }
}

- (void)bottomInit {
    if (_dayBtn.isSelected) {
        [_bottomTitleLbl setNotoText:@"일간 통계"];
    }else if (_weekBtn.isSelected) {
        [_bottomTitleLbl setNotoText:@"주간 통계"];
    }else if (_monthBtn.isSelected) {
        [_bottomTitleLbl setNotoText:@"월간 통계"];
    }
    
    NSArray *bottomItem = [db getBottomResult:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
    
    [_avgSystolicValLbl setNotoText:[NSString stringWithFormat:@"%1.f",[[[bottomItem objectAtIndex:0] objectForKey:@"AVGSYS"] floatValue]]];
    [_avgDiastoleValLbl setNotoText:[NSString stringWithFormat:@"%1.f",[[[bottomItem objectAtIndex:0] objectForKey:@"AVGDIA"] floatValue]]];
    
    _heightSystolicValLbl.attributedText = [self setBottomText:[[bottomItem objectAtIndex:0] objectForKey:@"MAXSYS"] unit:@" mmHg"];
    _heightDiatoleValLbl.attributedText = [self setBottomText:[[bottomItem objectAtIndex:0] objectForKey:@"MAXDIA"] unit:@" mmHg"];
}

#pragma mark - chart init
- (void)chartDataInit {
    if(tapTag == 10) {
        eatBeforeAfterType =  EAT_ALL;
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }else if(tapTag == 11) {
        eatBeforeAfterType =  EAT_ALL;
        //  차트 통일
        //  yItem = @[@"60", @"70", @"80", @"90", @"100", @"110", @"120", @"130", @"140", @"150"];
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }else if(tapTag == 12) {
        eatBeforeAfterType =  EAT_ALL;
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }
    
    if(tapTag == 10) {
        xItem = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14",
                  @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"];
        
        xOldDrawData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14",
                         @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"];
        xTempData = [db getResultDay:[timeUtils getNowTime]];
        xMedicine = [db getResultDrugDay:[timeUtils getNowTime]];
        
    }else if(tapTag == 11) {
        xItem = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
        
        xOldDrawData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
        xTempData = [db getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        xMedicine = [db getResultWeekDrug:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        
    }else if(tapTag == 12) {
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        for(int i = 1 ; i <= timeUtils.monthMaxDay ; i++) {
            NSString *tempDate = [NSString stringWithFormat:@"%d",i];
            [temparr addObject:tempDate];
        }
        xItem = temparr;
        xOldDrawData = temparr;
        
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        xTempData = [db getResultMonth:year nMonth:month];
        xMedicine = [db getResultMonthDrug:year nMonth:month];
    }
    
    xData = [[xTempData objectAtIndex:0] copy];
    xAfter_Data = [[xTempData objectAtIndex:1] copy];
    xNewBefore_DrawData = xOldDrawData;
    xNewAfter_DrawData = xOldDrawData;
    
    //투약
    xDrugNewBefore_DrawData = xOldDrawData;
    xDrugData = [[xMedicine objectAtIndex:0] copy];
    
    while(xData.count > xNewBefore_DrawData.count){
        if(xData.count > xNewBefore_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xData copy]];
            [tempArr removeLastObject];
            xData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    //투약
    while(xDrugData.count > xNewBefore_DrawData.count){
        if(xDrugData.count > xNewBefore_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xDrugData copy]];
            [tempArr removeLastObject];
            xDrugData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    while(xAfter_Data.count > xNewAfter_DrawData.count){
        if(xAfter_Data.count > xNewAfter_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xAfter_Data copy]];
            [tempArr removeLastObject];
            xAfter_Data = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    
    for(int i = 0 ; i < xData.count ; i ++){
        if([xData objectAtIndex:i] == NULL || [[xData objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xData copy]];
            [tempArr removeObjectAtIndex:i];
            xData = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xNewBefore_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xNewBefore_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    
    // 투약
    for(int i = 0 ; i < xDrugData.count ; i ++){
        if([xDrugData objectAtIndex:i] == NULL || [[xDrugData objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xDrugData copy]];
            [tempArr removeObjectAtIndex:i];
            xDrugData = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xDrugNewBefore_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xDrugNewBefore_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    
    
    for(int i = 0 ; i < xAfter_Data.count ; i ++){
        if([xAfter_Data objectAtIndex:i] == NULL || [[xAfter_Data objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xAfter_Data copy]];
            [tempArr removeObjectAtIndex:i];
            xAfter_Data = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xNewAfter_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xNewAfter_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    NSLog(@"end xData = %@", xData);
    NSLog(@"end xDrawData = %@", xNewBefore_DrawData);
    
    NSLog(@"end xAfter_Data = %@", xAfter_Data);
    NSLog(@"end xNewAfter_DrawData = %@", xNewAfter_DrawData);
    
    //투약
    NSLog(@"end xDrugData = %@", xDrugData);
    NSLog(@"end xDrugNewBefore_DrawData = %@", xDrugNewBefore_DrawData);
    
}

- (void)sugarChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    //일간 / 주간 / 월간 차트 표시 통일
    eatBeforeAfterType = EAT_ALL;
    PNScatterChart2 *scatterChart = [[PNScatterChart2 alloc] initWithFrame:CGRectMake((CGFloat)0, -10, _graphVw.layer.frame.size.width - 10, _graphVw.layer.frame.size.height + 10) chartType:eatBeforeAfterType];
    
    scatterChart.presuLine = YES;
    
    // X축 드로잉 값
    [scatterChart setAxisXLabel:xItem];
    
    // Y축 드로잉 값
    [scatterChart setAxisYLabel:yItem];
    
    // X축 최소값, 맥스값 총건수
    float maxXvalue = [[xItem objectAtIndex:xItem.count - 1] floatValue];
    [scatterChart setAxisXWithMinimumValue:1 andMaxValue:maxXvalue toTicks:(int)(xItem.count == 7 ? xItem.count : xItem.count/2)];
    
    // Y축 최소값, 맥스값 총건수
    float maxYvalue = [[yItem objectAtIndex:yItem.count - 1] floatValue];
    [scatterChart setAxisYWithMinimumValue:60 andMaxValue:maxYvalue  toTicks:(int)yItem.count];
    
    // 식전 드로잉 데이터값
    NSArray *xData2 = xNewBefore_DrawData;
    NSArray *yData2 = xData;
    NSArray *data02Array = [[NSArray alloc] initWithObjects:xData2, yData2, nil];
    
    // 식후 드로잉 데이터값
    NSArray *xData3 = xNewAfter_DrawData;
    NSArray *yData3 = xAfter_Data;
    NSArray *data03Array = [[NSArray alloc] initWithObjects:xData3, yData3, nil];
    
    // 투약
    NSArray *xData4 = xDrugNewBefore_DrawData;
    NSArray *yData4 = xDrugData;
    NSArray *data04Array = [[NSArray alloc] initWithObjects:xData4, nil];
    
    
    NSMutableArray *arrShapeLine1 = [[NSMutableArray alloc] init];
    NSMutableArray *arrShapeLine2 = [[NSMutableArray alloc] init];
    
    if([data02Array[0] count] != 0 || [data03Array[0] count] != 0) {
        if([data02Array[0] count] != 0) {
            PNScatterChartData2 *data01 = [PNScatterChartData2 new];
            data01.strokeColor = COLOR_MAIN_ORANGE;
            data01.fillColor = COLOR_MAIN_ORANGE;
            data01.size = 3.50f;    // 원 사이즈
            data01.itemCount = [data02Array[0] count];
            data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
            __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data02Array[0]];
            __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data02Array[1]];
            
            data01.getData = ^(NSUInteger index) {
                CGFloat xValue;
                xValue = [XAr1[index] floatValue];
                CGFloat yValue = [YAr1[index] floatValue];
                if(yValue > 240.f){
                    yValue = 240.f;
                }else if(yValue < 60.f){
                    yValue = 60.f;
                }
                
                return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
            };
            
            scatterChart.chartData = @[data01];
            
            [arrShapeLine1 setArray:@[data01]];
        }
        
        if([data03Array[0] count] != 0) {
            PNScatterChartData2 *data01 = [PNScatterChartData2 new];
            data01.strokeColor = COLOR_NATIONS_BLUE;
            data01.fillColor = COLOR_NATIONS_BLUE;
            data01.size = 3.50f;    // 원 사이즈
            data01.itemCount = [data03Array[0] count];
            data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
            __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data03Array[0]];
            __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data03Array[1]];
            
            data01.getData = ^(NSUInteger index) {
                CGFloat xValue;
                xValue = [XAr1[index] floatValue];
                CGFloat yValue = [YAr1[index] floatValue];
                if(yValue > 240.f){
                    yValue = 240.f;
                }else if(yValue < 60.f){
                    yValue = 60.f;
                }
                
                return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
            };
            
            
            
            scatterChart.chartData = @[data01];
            
            
            [arrShapeLine2 setArray:@[data01]];
            
            
            // 일봉일때만 라인그림.
            //            if(tapTag == 10){
            [scatterChart setChartLineData:arrShapeLine1 outData:arrShapeLine2];
            //            }
        }
        
    }
    
    // 투약
    if([data04Array[0] count] != 0) {
        PNScatterChartData2 *data01 = [PNScatterChartData2 new];
        data01.strokeColor = LIFERed;
        data01.fillColor = LIFERed;
        data01.size = 3.50f;    // 원 사이즈
        data01.itemCount = [data04Array[0] count];
        data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
        __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data04Array[0]];
        __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data04Array[0]];
        
        data01.getData = ^(NSUInteger index) {
            CGFloat xValue = [XAr1[index] floatValue];
            CGFloat yValue = [YAr1[index] floatValue];
            
            return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
        };
        
        scatterChart.medicineData = @[data01];
    }
    
    [scatterChart setup];
    scatterChart.tag = 1;
    [_graphVw addSubview:scatterChart];
}


#pragma mark - viewInit
- (void)viewSetup {
    [self timeButtonInit];
    [self bottomInit];
    [self chartDataInit];
    [self sugarChartInit];
}


- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _dayBtn.tag = 1;
    [_dayBtn setTitle:@"일간" forState:UIControlStateNormal];
    [_dayBtn mediHealthTabButton];
    
    _weekBtn.tag = 2;
    [_weekBtn setTitle:@"주간" forState:UIControlStateNormal];
    [_weekBtn mediHealthTabButton];
    
    _monthBtn.tag = 3;
    [_monthBtn setTitle:@"월간" forState:UIControlStateNormal];
    [_monthBtn mediHealthTabButton];
    
    _dayBtn.selected = true;
    
    _graphLeftLbl.font = FONT_NOTO_REGULAR(12);
    _graphLeftLbl.textColor = COLOR_MAIN_DARK;
    [_graphLeftLbl setNotoText:@"수축기"];
    
    _graphRightLbl.font = FONT_NOTO_REGULAR(12);
    _graphRightLbl.textColor = COLOR_MAIN_DARK;
    [_graphRightLbl setNotoText:@"이완기"];
    
    _bottomVw.layer.borderWidth = 1;
    _bottomVw.layer.borderColor = RGB(213, 213, 217).CGColor;
    
    _bottomTitleLbl.font = FONT_NOTO_MEDIUM(18);
    _bottomTitleLbl.textColor = RGB(105, 129, 236);
    _bottomTitleLbl.text = @"";
    
    _avgTitleLbl.font = FONT_NOTO_LIGHT(18);
    _avgTitleLbl.textColor = COLOR_MAIN_DARK;
    [_avgTitleLbl setNotoText:@"평균 혈압"];
    
    _avgSystolicLbl.font = FONT_NOTO_MEDIUM(14);
    _avgSystolicLbl.textColor = COLOR_GRAY_SUIT;
    [_avgSystolicLbl setNotoText:@"수축기"];
    
    _avgSystolicValLbl.font = FONT_NOTO_BOLD(17);
    _avgSystolicValLbl.textColor = COLOR_MAIN_ORANGE;
    _avgSystolicValLbl.text = @"";
    
    _avgSystolicUnitLbl.font = FONT_NOTO_LIGHT(16);
    _avgSystolicUnitLbl.textColor = COLOR_MAIN_ORANGE;
    [_avgSystolicUnitLbl setNotoText:@"mmHg "];
    
    _avgDiastoleLbl.font = FONT_NOTO_MEDIUM(14);
    _avgDiastoleLbl.textColor = COLOR_GRAY_SUIT;
    [_avgDiastoleLbl setNotoText:@"이완기"];
    
    _avgDiastoleValLbl.font = FONT_NOTO_BOLD(17);
    _avgDiastoleValLbl.textColor = COLOR_MAIN_ORANGE;
    _avgDiastoleValLbl.text = @"";
    
    _avgDiastoleUnitLbl.font = FONT_NOTO_LIGHT(16);
    _avgDiastoleUnitLbl.textColor = COLOR_MAIN_ORANGE;
    [_avgDiastoleUnitLbl setNotoText:@"mmHg "];
    
    _heightSystolicLbl.font = FONT_NOTO_LIGHT(18);
    _heightSystolicLbl.textColor = COLOR_MAIN_DARK;
    [_heightSystolicLbl setNotoText:@"수축기 최고"];
    
    _heightSystolicValLbl.text = @"";
    
    _heightDiatoleLbl.font = FONT_NOTO_LIGHT(18);
    _heightDiatoleLbl.textColor = COLOR_MAIN_DARK;
    [_heightDiatoleLbl setNotoText:@"이완기 최고"];
    
    _heightDiatoleValLbl.text = @"";
}

#pragma mark - Health Text
- (NSMutableAttributedString *)setBottomText:(NSString *)val unit:(NSString *)unit {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:val];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(17)
                     range:NSMakeRange(0, firstStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_ORANGE
                     range:NSMakeRange(0, firstStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:unit];
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_LIGHT(16)
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:COLOR_MAIN_ORANGE
                      range:NSMakeRange(0, secondStr.length)];
    [attstr appendAttributedString:secondStr];
    
    return attstr;
}

@end
