//
//  PresuMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PresuMainVC.h"

@interface PresuMainVC (){
    UIBarButtonItem *naviLeftBtn;
    UIView *bubbleView;
}

@end

@implementation PresuMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"혈압"];
    [self navigationSetup];
    
    [self viewInit];
    
    vcGraph = [self.storyboard instantiateViewControllerWithIdentifier:@"PresuGraphVC"];
    [self addChildViewController:vcGraph];
    vcGraph.view.bounds = _vwMain.bounds;
    vcGraph.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcGraph.view];
    [vcGraph.view setHidden:false];
    
    vcHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"PresuHistoryVC"];
    [self addChildViewController:vcHistory];
    vcHistory.view.bounds = _vwMain.bounds;
    vcHistory.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcHistory.view];
    [vcHistory.view setHidden:true];
    
    [self.graphBtn setSelected:true];
    [self.historyBtn setSelected:false];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [self.graphBtn setSelected:false];
    [self.historyBtn setSelected:false];
    
    if(!vcGraph.view.isHidden) {
        
        [self.graphBtn setSelected:true];
        self.tabBarVwLeftConst.constant = 0;
        
        [vcGraph.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        [vcGraph viewSetup];
    }else {
        
        [self.historyBtn setSelected:true];
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcGraph.view setHidden:true];
        [vcHistory.view setHidden:false];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    NSDictionary *tipData = [[NSUserDefaults standardUserDefaults] objectForKey:HEALTH_TIP_DATA];
    if (tipData.count > 0) {
        [AlertUtils HealthMessageShow:self type:[tipData objectForKey:@"type"] msg:[tipData objectForKey:@"message"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:HEALTH_TIP_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([[Utils getUserDefault:HEALTH_MSG_NEW] isEqualToString:@"N"]) {
        [self bubbleHealthMsg];
    } else {
        UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
        healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [naviLeftBtn setImage:healthMsgImg];
        [bubbleView removeFromSuperview];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [bubbleView removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions
- (IBAction)pressTab:(UIButton *)sender {
    
    if(sender.tag == 1 && !self.graphBtn.isSelected) {
        
        
        [self.graphBtn setSelected:true];
        [self.historyBtn setSelected:false];
        
        self.tabBarVwLeftConst.constant = 0;
        
        [vcGraph.view setHidden:false];
        [vcHistory.view setHidden:true];
        
        [vcGraph viewSetup];
    }else if(sender.tag == 2 && !self.historyBtn.isSelected) {
        
        
        [self.graphBtn setSelected:false];
        [self.historyBtn setSelected:true];
        
        self.tabBarVwLeftConst.constant = DEVICE_WIDTH / 2;
        
        [vcGraph.view setHidden:true];
        [vcHistory.view setHidden:false];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    self.graphBtn.tag = 1;
    [self.graphBtn setTitle:@"그래프" forState:UIControlStateNormal];
    [self.graphBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.graphBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.historyBtn.tag = 2;
    [self.historyBtn setTitle:@"히스토리" forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [self.historyBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
}

- (void)bubbleHealthMsg {
    CGFloat yPos = IS_IPHONEX ? 80 : 55;
    bubbleView = [[UIView alloc] initWithFrame:CGRectMake(20 ,yPos ,150 ,28)];
    
    bubbleView.layer.borderWidth = 1;
    bubbleView.layer.borderColor = COLOR_MAIN_DARK.CGColor;
    bubbleView.layer.cornerRadius = 14;
    bubbleView.layer.masksToBounds = true;
    bubbleView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 115, 20)];
    lbl.textColor = [UIColor blackColor];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.lineBreakMode = NSLineBreakByTruncatingTail;
    lbl.numberOfLines = 1;
    lbl.font = FONT_NOTO_MEDIUM(12);
    lbl.textColor = COLOR_MAIN_DARK;
    [bubbleView addSubview:lbl];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 120, 25)];
    [btn setTitle:@"" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:self action:@selector(pressHealthMsg:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:btn];
    
    UIButton *closebtn = [[UIButton alloc] initWithFrame:CGRectMake(125, 4.5, 25, 20)];
    [closebtn setImage:[UIImage imageNamed:@"path"] forState:UIControlStateNormal];
    [closebtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    [closebtn addTarget:self action:@selector(pressHealthMsgClose:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:closebtn];
    
    [lbl setNotoText:@"건강메시지가 도착했습니다."];
    [lbl sizeToFit];
    //    [self.view addSubview:bubbleView];
    [self.navigationController.view addSubview:bubbleView];
    
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_on_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
}


- (void)navigationSetup {
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naviLeftBtn = [[UIBarButtonItem alloc] init];
    [naviLeftBtn setStyle:UIBarButtonItemStylePlain];
    [naviLeftBtn setImage:healthMsgImg];
    [naviLeftBtn setTarget:self];
    [naviLeftBtn setAction:@selector(pressHealthMsg:)];
    self.navigationItem.leftBarButtonItem = naviLeftBtn;
}

- (IBAction)pressHealthMsg:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
    [bubbleView removeFromSuperview];
    
    UIViewController *vc = [HEALTHMESSAGE_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressHealthMsgClose:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
    [bubbleView removeFromSuperview];
}

#pragma mark - HealthMessageAlertVCDelegate
- (void)HealthMessageAlertVC:(HealthMessageAlertVC *)alertView type:(NSString *)type actionsType:(HealthMessageAlertActions)actionsType {
    if (actionsType == Tip) {
        HealthMessageMainVC *vc = [HEALTHMESSAGE_STORYBOARD instantiateInitialViewController];
        vc.hidesBottomBarWhenPushed = true;
        vc.tapTag = 11;
        vc.healthType = type;
        [self.navigationController pushViewController:vc animated:true];
    }
}

@end
