//
//  PresuGraphZoomVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "PresuDBWraper.h"

#import "BaseViewController.h"

//소수차트
#import "PNScatterChart2.h"

@interface PresuGraphZoomVC : BaseViewController {
    PresuDBWraper *db;
    TimeUtils *timeUtils;
    
    EAT_BEFORE_AFTER eatBeforeAfterType;
}

@property (assign) int tapTag;
@property (nonatomic, strong) TimeUtils *timeUtilsData;

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphRightLbl;

@property (weak, nonatomic) IBOutlet UIImageView *imageRotate;

- (IBAction)pressClose:(id)sender;


@end
