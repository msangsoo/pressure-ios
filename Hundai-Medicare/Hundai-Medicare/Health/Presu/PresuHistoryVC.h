//
//  PresuHistoryVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PresuDBWraper.h"

#import "HistoryCommonTableViewCell.h"
#import "BaseViewController.h"
#import "PresuEditVC.h"

@interface PresuHistoryVC : BaseViewController<UITableViewDelegate, UITableViewDataSource, ServerCommunicationDelegate, BasicAlertVCDelegate, PresuEditVCDelegate> {
    PresuDBWraper *db;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)viewSetup;

@end
