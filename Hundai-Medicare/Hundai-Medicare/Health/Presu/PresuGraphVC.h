//
//  PresuGraphVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimeUtils.h"
#import "PresuDBWraper.h"
#import "MessageDBWraper.h"

#import "BaseViewController.h"
#import "PresuGraphZoomVC.h"

#import "FeedModel.h"
#import "CommunityWriteViewController.h"
#import "NickNameInputAlertViewController.h"
#import "CommunityNetwork.h"
#import "FeedDetailViewController.h"

//소수차트
#import "PNScatterChart2.h"

@interface PresuGraphVC : BaseViewController<BasicAlertVCDelegate> {
    PresuDBWraper *db;
    MessageDBWraper *msgdb;
    TimeUtils *timeUtils;
    
    EAT_BEFORE_AFTER eatBeforeAfterType;
}

@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;

/* Navi */
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

/* Graph */
@property (weak, nonatomic) IBOutlet UIView *graphVw;
@property (weak, nonatomic) IBOutlet UILabel *graphLeftLbl;
@property (weak, nonatomic) IBOutlet UILabel *graphRightLbl;

/* Botoom */
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLbl;

@property (weak, nonatomic) IBOutlet UIView *bottomVw;

@property (weak, nonatomic) IBOutlet UILabel *avgTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *avgSystolicLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgSystolicValLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgSystolicUnitLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgDiastoleLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgDiastoleValLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgDiastoleUnitLbl;

@property (weak, nonatomic) IBOutlet UILabel *heightSystolicLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightSystolicValLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightDiatoleLbl;
@property (weak, nonatomic) IBOutlet UILabel *heightDiatoleValLbl;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConst;

- (void)viewSetup;

@end
