//
//  PresuMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 13/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "PresuGraphVC.h"
#import "PresuHistoryVC.h"
#import "HealthMessageMainVC.h"

@interface PresuMainVC : BaseViewController<HealthMessageAlertVCDelegate> {
    PresuGraphVC *vcGraph;
    PresuHistoryVC *vcHistory;
}

@property (weak, nonatomic) IBOutlet UIButton *graphBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

@end
