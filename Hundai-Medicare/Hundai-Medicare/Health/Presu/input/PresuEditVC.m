//
//  PresuEditVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PresuEditVC.h"

@interface PresuEditVC () {
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
    NSString *healthMsg;
}

@end

@implementation PresuEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _titleLbl.font = FONT_NOTO_REGULAR(18);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"혈압 수정"];
    
    [self viewInit];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"brssr_info_edit_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            PresuDBWraper *db = [[PresuDBWraper alloc] init];
            [db UpdateDb:[writeData objectForKey:@"idx"] arterialPressure:@"0" diastolicPressure:[writeData objectForKey:@"diastolicPressure"] pulseRate:@"0" systolicPressure:[writeData objectForKey:@"systolicPressure"] drugname:[writeData objectForKey:@"drugname"] reg_de:[writeData objectForKey:@"regdate"]];
            
            [self dismissViewControllerAnimated:true completion:^{
                if (self.delegate) {
                    [self.delegate updateSuccess];
                }
            }];
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressCancel:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}

- (IBAction)pressEdit:(id)sender {
    if ([_systolicTf.text length] < 1) {
        [Utils basicAlertView:self withTitle:@"" withMsg:@"수축기를 입력해주세요."];
        return;
    }else if ([_diastolicTf.text length] < 1) {
        [Utils basicAlertView:self withTitle:@"" withMsg:@"이완기를 입력해주세요."];
        return;
    }else {
        if ([_systolicTf.text intValue] > 300
            || [_systolicTf.text intValue] < 20
            || [_diastolicTf.text intValue] > 300
            || [_diastolicTf.text intValue] < 20) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"혈압은 20이상 300이하의 값을 입력해주세요"
                                                               delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
            [alertView show];
            return;
        }
        
        [self apiEdit];
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _systolicTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_systolicTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_systolicTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _systolicTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }else if (textField == _diastolicTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_diastolicTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_diastolicTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _diastolicTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 선택한 값을 UITextField의 text값에 반영
    if(textField == self.timeTf) {
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar]
                                            components:NSCalendarUnitHour|NSCalendarUnitMinute
                                            fromDate:self.timePicker.date];
        [self.timeBtn setTitle:[NSString stringWithFormat:@"%02ld:%02ld", (long)dateComponents.hour, (long)dateComponents.minute] forState:UIControlStateNormal];
        nowTime = [self.timeTf.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
}


#pragma mark - api
- (void)apiEdit {
    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    writeData = [NSDictionary dictionaryWithObjectsAndKeys:
                    [_userInfo objectForKey:@"idx"], @"idx",
                    @"", @"arterialPressure",
                    _systolicTf.text, @"systolicPressure",
                    _diastolicTf.text, @"diastolicPressure",
                    @"0", @"pulseRate",
                    @"", @"drugname",
                    @"U", @"regtype",
                    [NSString stringWithFormat:@"%@%@", nowDay, nowTime], @"regdate",
                    nil];
    [ast_mass addObject:writeData];
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [_model indicatorHidden];
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    PresuDBWraper *db = [[PresuDBWraper alloc] init];
    [db UpdateDb:[writeData objectForKey:@"idx"] arterialPressure:@"0" diastolicPressure:[writeData objectForKey:@"diastolicPressure"] pulseRate:@"0" systolicPressure:[writeData objectForKey:@"systolicPressure"] drugname:[writeData objectForKey:@"drugname"] reg_de:[writeData objectForKey:@"regdate"]];
    
    [self dismissViewControllerAnimated:true completion:^{
        if (self.delegate) {
            [self.delegate updateSuccess];
        }
    }];
    
//    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                @"brssr_info_edit_data", @"api_code",
//                                ast_mass, @"ast_mass",
//                                nil];
//
//    if(parameters != nil) {
//        if(server.delegate == nil) {
//            server.delegate = self;
//        }
//        [_model indicatorActive];
//        [server serverCommunicationData:parameters];
//    }else {
//        NSLog(@"server parameters error = %@", parameters);
//    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_GRAY_SUIT forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    self.timePicker = [[UIDatePicker alloc] init];
    self.timePicker.datePickerMode = UIDatePickerModeTime;
    self.timePicker.backgroundColor = [UIColor whiteColor];
    _timeTf.inputView = self.timePicker;
    _timeTf.delegate = self;
    
    _presuLbl.font = FONT_NOTO_REGULAR(18);
    _presuLbl.textColor = COLOR_MAIN_DARK;
    [_presuLbl setNotoText:@"혈압"];
    
    _systolicTf.font = FONT_NOTO_MEDIUM(20);
    _systolicTf.textColor = COLOR_NATIONS_BLUE;
    [_systolicTf setPlaceholder:@"수축기"];
    _systolicTf.delegate = self;
    
    _diastolicTf.font = FONT_NOTO_MEDIUM(20);
    _diastolicTf.textColor = COLOR_NATIONS_BLUE;
    [_diastolicTf setPlaceholder:@"수축기"];
    _diastolicTf.delegate = self;
    
    [_cancelBtn setTitle:@"취소" forState:UIControlStateNormal];
    [_cancelBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    [_editBtn setTitle:@"저장" forState:UIControlStateNormal];
    [_editBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    //수정시 기존 데이터 설정
    NSString *regdate = [_userInfo objectForKey:@"regdate"];
    
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dtF setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *d = [dtF dateFromString:regdate];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    nowDay = [dateFormat stringFromDate:d];
    
    [dateFormat setDateFormat:@"HH:mm"];
    nowTime = [dateFormat stringFromDate:d];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."],[Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    nowDay = [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@", nowTime] forState:UIControlStateNormal];
    nowTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    NSString *systolic = [_userInfo objectForKey:@"systolic"];
    NSString *diastolic = [_userInfo objectForKey:@"diastolic"];
    
    self.systolicTf.text = systolic;
    self.diastolicTf.text = diastolic;
}

@end
