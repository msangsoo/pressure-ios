//
//  PresuInputVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PresuInputVC.h"

@interface PresuInputVC () {
    NSString *nowDay;
    NSString *nowTime;
    NSDictionary *writeData;
    NSString *healthMsg;
}


@end

@implementation PresuInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.navigationItem setTitle:@"혈압 입력"];
    
    [self navigationSetup];
    
    [self viewInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:false];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    if([[result objectForKey:@"api_code"] isEqualToString:@"brssr_info_input_data"]) {
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]) {
            
            PresuDBWraper *db = [[PresuDBWraper alloc] init];
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            [arrResult addObject:writeData];
            [db insert:arrResult];
            
            Tr_login *login = [_model getLoginData];
            
            healthMsg = [_model getPresuMsg:[writeData objectForKey:@"systolicPressure"] diastolic:[writeData objectForKey:@"diastolicPressure"] regdate:[writeData objectForKey:@"regdate"]];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"infra_message_write", @"api_code",
                                        @"2", @"infra_ty",
                                        login.mber_sn, @"mber_sn",
                                        [writeData objectForKey:@"idx"], @"idx",
                                        healthMsg, @"infra_message",
                                        nil];
            
            [server serverCommunicationData:parameters];
            
        }else{
            [Utils basicAlertView:self withTitle:@"알림" withMsg:@"등록에 실패 하였습니다."];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"infra_message_write"]){
        if([[result objectForKey:@"reg_yn"] isEqualToString:@"Y"]){
            NSMutableArray *arrResult = [[NSMutableArray alloc] init];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        [writeData objectForKey:@"idx"], @"idx",
                                        @"2", @"infra_ty",
                                        healthMsg, @"message",
                                        @"YES", @"is_server_regist",
                                        [writeData objectForKey:@"regdate"], @"regdate",
                                        nil];
            
            [arrResult addObject:parameters];
            
            MessageDBWraper *db = [[MessageDBWraper alloc] init];
            [db insert:arrResult];
            [Utils setUserDefault:HEALTH_MSG_NEW value:@"N"];
            [Utils setUserDefault:HEALTH_POINT_ALERT value:@"2"];
            
            //관리팁 메세지
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            if ([userDefault objectForKey:HEALTH_PRESU_TIP_TODAY_TIME]) {
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"yyyyMMdd"];
                NSDate *tipDate = [userDefault objectForKey:HEALTH_PRESU_TIP_TODAY_TIME];
                
                int tipDateVal = [[format stringFromDate:tipDate] intValue];
                int nowDateVal = [[format stringFromDate:[NSDate date]] intValue];
                
                if (nowDateVal > tipDateVal) {
                    [self tipMessageAdd];
                }
            }else {
                [self tipMessageAdd];
            }
//            [AlertUtils BasicAlertShow:self tag:1000 title:@"알림" msg:@"등록되었습니다." left:@"확인" right:@""];
        }
        
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - Tip Message
- (void)tipMessageAdd {
    float systolicPressure = [[writeData objectForKey:@"systolicPressure"] floatValue]; // 수축기
    float diastolicPressure = [[writeData objectForKey:@"diastolicPressure"] floatValue]; // 이완기
    
    if (systolicPressure >= 120 || diastolicPressure >= 80) { // 수축기 120이상 또는 이완기 80이상
        NSDictionary *tipData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"혈압", @"type",
                                 healthMsg, @"message",
                                 nil];
        [[NSUserDefaults standardUserDefaults] setObject:tipData forKey:HEALTH_TIP_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark -BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:true];
    }else if (alertView.tag == 200) {
        if (tag == 2) {
            [self apiSave];
        }
    }
}

#pragma mark - Actions
- (IBAction)pressSave:(UIButton *)sender {
    if ([_systolicTf.text length] < 1) {
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"수축기를 입력해주세요." left:@"확인" right:@""];
        return;
    }else if ([_diastolicTf.text length] < 1) {
        [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"이완기를 입력해주세요." left:@"확인" right:@""];
        return;
    }else {
        if ([_systolicTf.text intValue] > 300
            || [_systolicTf.text intValue] < 20
            || [_diastolicTf.text intValue] > 300
            || [_diastolicTf.text intValue] < 20) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"혈압은 20이상 300이하의 값을 입력해주세요"
                                                               delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil ];
            [alertView show];
            return;
        }
        [self apiSave];
    }
}

- (IBAction)pressDate:(id)sender {
    [AlertUtils DateControlShow:self dateType:Date tag:1001 selectDate:nowDay];
}

- (IBAction)pressTime:(id)sender {
    [AlertUtils DateControlShow:self dateType:Time tag:1002 selectDate:nowTime];
}

#pragma mark - DateControlVCDelegate
- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date {
    // 미래날짜 선택불가.
    NSString *tempDate = [date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([tempDate longLongValue] > [[TimeUtils getNowDateFormat:@"yyyyMMdd"] longLongValue]) {
        return;
    }
    
    if(alertView.view.tag == 1001) {
        [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [date stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:date]] forState:UIControlStateNormal];
        _dayBtn.tag = [[date stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        nowDay = date;
    }else if(alertView.view.tag == 1002) {
        [_timeBtn setTitle:[NSString stringWithFormat:@"%@", date] forState:UIControlStateNormal];
        nowTime = [NSString stringWithFormat:@"%@", date];
    }
}

- (void)closeDateView:(DateControlVC *)alertView {
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _systolicTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_systolicTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_systolicTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _systolicTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }else if (textField == _diastolicTf) {
        if(range.location == 0 && ([string isEqualToString:@"0"] || [string isEqualToString:@"."])) {
            return false;
        }
        
        NSRange subRange = [_diastolicTf.text rangeOfString:@"."];
        if(subRange.location != NSNotFound){
            NSArray *textArr = [_diastolicTf.text componentsSeparatedByString:@"."];
            NSString *backText = [textArr objectAtIndex:1];
            if(backText.length > 0 && ![string isEqualToString:@""]) {
                return false;
            }else {
                if ([string isEqualToString:@"."]) {
                    return false;
                }else {
                    return true;
                }
            }
        }
        
        float textVal = [[NSString stringWithFormat:@"%@%@", _diastolicTf.text, string] floatValue];
        if (textVal > 1000.f) {
            return false;
        }
    }
    
    return true;
}

#pragma mark - api
- (void)apiSave {
//    NSMutableArray *ast_mass = [[NSMutableArray alloc] init];
    
    NSString *tmpTime = [nowTime stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    writeData =[NSDictionary dictionaryWithObjectsAndKeys:
                [Utils getNowDate:@"yyMMddHHmmssSS"], @"idx",
                @"", @"arterialPressure",
                _systolicTf.text, @"systolicPressure",
                _diastolicTf.text, @"diastolicPressure",
                @"0", @"pulseRate",
                @"", @"drugname",
                @"U", @"regtype",
                [NSString stringWithFormat:@"%ld%@", _dayBtn.tag, tmpTime], @"regdate",
                nil];
//    [ast_mass addObject:writeData];
    
    
    //현재시간보다 미래면 입력안되게 막음.
    long nDate = [[TimeUtils getNowDateFormat:@"yyyyMMddHHmm"] longLongValue];
    long iDate = [[writeData objectForKey:@"regdate"] longLongValue];
    if (nDate < iDate) {
        [Utils basicAlertView:self withTitle:@"알림" withMsg:@"미래시간을 입력할 수 없습니다."];
        return;
    }
    
    PresuDBWraper *db = [[PresuDBWraper alloc] init];
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    [arrResult addObject:writeData];
    [db insert:arrResult];
    
    healthMsg = [_model getPresuMsg:[writeData objectForKey:@"systolicPressure"] diastolic:[writeData objectForKey:@"diastolicPressure"] regdate:[writeData objectForKey:@"regdate"]];
    
    [self setMessage];
    
    
//    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                @"brssr_info_input_data", @"api_code",
//                                ast_mass, @"ast_mass",
//                                nil];
//
//    if(parameters != nil) {
//        if(server.delegate == nil) {
//            server.delegate = self;
//        }
//        [_model indicatorActive];
//        [server serverCommunicationData:parameters];
//    }else {
//        NSLog(@"server parameters error = %@", parameters);
//    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(18);
    _dateLbl.textColor = COLOR_MAIN_DARK;
    [_dateLbl setNotoText:@"측정 시간"];
    
    _dayBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_dayBtn setTitle:@"" forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_dayBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _timeBtn.titleLabel.font = FONT_NOTO_MEDIUM(16);
    [_timeBtn setTitle:@"" forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateNormal];
    [_timeBtn setTitleColor:COLOR_NATIONS_BLUE forState:UIControlStateSelected];
    
    _presuLbl.font = FONT_NOTO_REGULAR(18);
    _presuLbl.textColor = COLOR_MAIN_DARK;
    [_presuLbl setNotoText:@"혈압"];
    
    _systolicTf.font = FONT_NOTO_MEDIUM(18);
    _systolicTf.textColor = COLOR_NATIONS_BLUE;
    [_systolicTf setPlaceholder:@"수축기"];
    _systolicTf.delegate = self;
    
    _diastolicTf.font = FONT_NOTO_MEDIUM(18);
    _diastolicTf.textColor = COLOR_NATIONS_BLUE;
    [_diastolicTf setPlaceholder:@"이완기"];
    _diastolicTf.delegate = self;
    
    nowDay = [Utils getNowDate:@"yyyy-MM-dd"];
    nowTime = [Utils getNowDate:@"HH:mm"];
    
    [_dayBtn setTitle:[NSString stringWithFormat:@"%@ %@", [nowDay stringByReplacingOccurrencesOfString:@"-" withString:@"."], [Utils getMonthDay:nowDay]] forState:UIControlStateNormal];
    _dayBtn.tag = [[nowDay stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
    
    [_timeBtn setTitle:[NSString stringWithFormat:@"%@",nowTime] forState:UIControlStateNormal];
    _timeBtn.tag = [[nowTime stringByReplacingOccurrencesOfString:@":" withString:@""] integerValue];
}

- (void)navigationSetup {
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"저장"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressSave:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)setMessage {
    NSMutableArray *arrResult = [[NSMutableArray alloc] init];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                [writeData objectForKey:@"idx"], @"idx",
                                @"2", @"infra_ty",
                                healthMsg, @"message",
                                @"YES", @"is_server_regist",
                                [writeData objectForKey:@"regdate"], @"regdate",
                                nil];
    
    [arrResult addObject:parameters];
    
    MessageDBWraper *db = [[MessageDBWraper alloc] init];
    [db insert:arrResult];
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"N"];
    [Utils setUserDefault:HEALTH_POINT_ALERT value:@"2"];
    
    //관리팁 메세지
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([userDefault objectForKey:HEALTH_PRESU_TIP_TODAY_TIME]) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyyMMdd"];
        NSDate *tipDate = [userDefault objectForKey:HEALTH_PRESU_TIP_TODAY_TIME];
        
        int tipDateVal = [[format stringFromDate:tipDate] intValue];
        int nowDateVal = [[format stringFromDate:[NSDate date]] intValue];
        
        if (nowDateVal > tipDateVal) {
            [self tipMessageAdd];
        }
    }else {
        [self tipMessageAdd];
    }
    
    [self.navigationController popViewControllerAnimated:true];
}

@end
