//
//  PresuEditVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "PresuDBWraper.h"

@interface PresuEditVC : BaseViewController<ServerCommunicationDelegate, UITextFieldDelegate, BasicAlertVCDelegate>

@property UIDatePicker *timePicker;

@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UITextField *timeTf;

@property (weak, nonatomic) IBOutlet UILabel *presuLbl;
@property (weak, nonatomic) IBOutlet UITextField *systolicTf; // 수축기
@property (weak, nonatomic) IBOutlet UITextField *diastolicTf; // 이완기

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@end

@protocol PresuEditVCDelegate
- (void)updateSuccess;
@end
