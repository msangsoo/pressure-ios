//
//  PresuGraphZoomVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 30/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PresuGraphZoomVC.h"

@interface PresuGraphZoomVC () {
    //char data
    NSArray *xItem;
    NSArray *yItem;
    NSArray *xTempData;
    NSArray *xMedicine;
    
    NSArray *xData;
    NSArray *xAfter_Data;
    
    NSArray *xOldDrawData;
    NSArray *xNewBefore_DrawData;
    NSArray *xNewAfter_DrawData;
    
    NSArray *xDrugData;
    NSArray *xDrugAfter_Data;
    NSArray *xDrugNewBefore_DrawData;
    
    NSArray *msgArr;
    NSString *healthMsg;
}

@end

@implementation PresuGraphZoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[PresuDBWraper alloc] init];
    
    if (self.tapTag == 0) {
        self.tapTag = 10;
    }
    
    [self viewInit];
    
    
    
    self.imageRotate.alpha = 0.3;
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.imageRotate.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0 delay:0.7 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.imageRotate.alpha = 0;
            } completion:^(BOOL finished) {
                self.imageRotate.hidden = YES;
            }];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewSetup];
}

#pragma mark - Actions
- (IBAction)pressClose:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)pressTime:(UIButton *)sender {
    if (sender.tag == 0) {
        [timeUtils getTime:-1];
    }else {
        [timeUtils getTime:1];
    }
    
    [self viewSetup];
}

#pragma mark - viewSetup
- (void)viewSetup {
    [self timeButtonInit];
    [self chartDataInit];
    [self sugarChartInit];
}

- (void)timeButtonInit {
    [_rightBtn setHidden:timeUtils.rightHidden];
    NSString *startTime = [[timeUtils getNowTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *endTime = [[timeUtils getEndTime] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    if(self.tapTag == 10) {
        [_dateLbl setNotoText:startTime];
    }else if(self.tapTag == 11) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }else if (self.tapTag == 12) {
        [_dateLbl setNotoText:[NSString stringWithFormat:@"%@ ~ %@", startTime, endTime]];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_REGULAR(14);
    _dateLbl.textColor = COLOR_NATIONS_BLUE;
    _dateLbl.text = @"";
    
    _leftBtn.tag = 0;
    _rightBtn.tag = 1;
    
    _graphLeftLbl.font = FONT_NOTO_REGULAR(16);
    _graphLeftLbl.textColor = COLOR_MAIN_DARK;
    [_graphLeftLbl setNotoText:@"수축기"];
    
    _graphRightLbl.font = FONT_NOTO_REGULAR(16);
    _graphRightLbl.textColor = COLOR_MAIN_DARK;
    [_graphRightLbl setNotoText:@"이완기"];
    
    UIImage *closeImg = [[UIImage imageNamed:@"btn_circle_gray_cancel"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:closeImg forState:UIControlStateNormal];
    [self.closeBtn setTintColor:COLOR_GRAY_SUIT];
    
    timeUtils = self.timeUtilsData;
}

#pragma mark - chart init
- (void)chartDataInit {
    if(self.tapTag == 10) {
        eatBeforeAfterType =  EAT_ALL;
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }else if(self.tapTag == 11) {
        eatBeforeAfterType =  EAT_ALL;
        //  차트 통일
        //  yItem = @[@"60", @"70", @"80", @"90", @"100", @"110", @"120", @"130", @"140", @"150"];
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }else if(self.tapTag == 12) {
        eatBeforeAfterType =  EAT_ALL;
        yItem = @[@"60", @"80", @"100", @"120", @"140", @"160", @"180", @"200", @"220", @"240"];
    }
    
    if(self.tapTag == 10) {
        xItem = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14",
                  @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"];
        
        xOldDrawData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14",
                         @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"];
        xTempData = [db getResultDay:[timeUtils getNowTime]];
        xMedicine = [db getResultDrugDay:[timeUtils getNowTime]];
        
    }else if(self.tapTag == 11) {
        xItem = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
        
        xOldDrawData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
        xTempData = [db getResultWeek:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        xMedicine = [db getResultWeekDrug:[timeUtils getNowTime] eDate:[timeUtils getEndTime]];
        
    }else if(self.tapTag == 12) {
        NSMutableArray *temparr = [[NSMutableArray alloc] init];
        for(int i = 1 ; i <= timeUtils.monthMaxDay ; i++) {
            NSString *tempDate = [NSString stringWithFormat:@"%d",i];
            [temparr addObject:tempDate];
        }
        xItem = temparr;
        xOldDrawData = temparr;
        
        NSString *year = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"yyyy"];
        NSString *month = [Utils getTimeFormat:[timeUtils getNowTime] beTime:@"yyyy-MM-dd" afTime:@"MM"];
        xTempData = [db getResultMonth:year nMonth:month];
        xMedicine = [db getResultMonthDrug:year nMonth:month];
    }
    
    xData = [[xTempData objectAtIndex:0] copy];
    xAfter_Data = [[xTempData objectAtIndex:1] copy];
    xNewBefore_DrawData = xOldDrawData;
    xNewAfter_DrawData = xOldDrawData;
    
    //투약
    xDrugNewBefore_DrawData = xOldDrawData;
    xDrugData = [[xMedicine objectAtIndex:0] copy];
    
    while(xData.count > xNewBefore_DrawData.count){
        if(xData.count > xNewBefore_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xData copy]];
            [tempArr removeLastObject];
            xData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    //투약
    while(xDrugData.count > xNewBefore_DrawData.count){
        if(xDrugData.count > xNewBefore_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xDrugData copy]];
            [tempArr removeLastObject];
            xDrugData = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    while(xAfter_Data.count > xNewAfter_DrawData.count){
        if(xAfter_Data.count > xNewAfter_DrawData.count){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xAfter_Data copy]];
            [tempArr removeLastObject];
            xAfter_Data = [NSArray arrayWithArray:tempArr];
        }
    }
    
    
    
    for(int i = 0 ; i < xData.count ; i ++){
        if([xData objectAtIndex:i] == NULL || [[xData objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xData copy]];
            [tempArr removeObjectAtIndex:i];
            xData = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xNewBefore_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xNewBefore_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    
    // 투약
    for(int i = 0 ; i < xDrugData.count ; i ++){
        if([xDrugData objectAtIndex:i] == NULL || [[xDrugData objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xDrugData copy]];
            [tempArr removeObjectAtIndex:i];
            xDrugData = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xDrugNewBefore_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xDrugNewBefore_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    
    
    for(int i = 0 ; i < xAfter_Data.count ; i ++){
        if([xAfter_Data objectAtIndex:i] == NULL || [[xAfter_Data objectAtIndex:i] intValue] == 0){
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[xAfter_Data copy]];
            [tempArr removeObjectAtIndex:i];
            xAfter_Data = [NSArray arrayWithArray:tempArr];
            
            NSMutableArray *tempArr2 = [NSMutableArray arrayWithArray:[xNewAfter_DrawData copy]];
            [tempArr2 removeObjectAtIndex:i];
            xNewAfter_DrawData = [NSArray arrayWithArray:tempArr2];
            
            i = -1;
        }
    }
    
    NSLog(@"end xData = %@", xData);
    NSLog(@"end xDrawData = %@", xNewBefore_DrawData);
    
    NSLog(@"end xAfter_Data = %@", xAfter_Data);
    NSLog(@"end xNewAfter_DrawData = %@", xNewAfter_DrawData);
    
    //투약
    NSLog(@"end xDrugData = %@", xDrugData);
    NSLog(@"end xDrugNewBefore_DrawData = %@", xDrugNewBefore_DrawData);
    
}

- (void)sugarChartInit {
    UIView *viewToRemove = [_graphVw viewWithTag:1];
    [viewToRemove removeFromSuperview];
    
    //일간 / 주간 / 월간 차트 표시 통일
    eatBeforeAfterType = EAT_ALL;
    PNScatterChart2 *scatterChart = [[PNScatterChart2 alloc] initWithFrame:CGRectMake((CGFloat)0, -10, _graphVw.layer.frame.size.width - 10, _graphVw.layer.frame.size.height + 10) chartType:eatBeforeAfterType];
    
    scatterChart.presuLine = YES;
    
    // X축 드로잉 값
    [scatterChart setAxisXLabel:xItem];
    
    // Y축 드로잉 값
    [scatterChart setAxisYLabel:yItem];
    
    // X축 최소값, 맥스값 총건수
    float maxXvalue = [[xItem objectAtIndex:xItem.count - 1] floatValue];
    [scatterChart setAxisXWithMinimumValue:1 andMaxValue:maxXvalue toTicks:(int)(xItem.count == 7 ? xItem.count : xItem.count/2)];
    
    // Y축 최소값, 맥스값 총건수
    float maxYvalue = [[yItem objectAtIndex:yItem.count - 1] floatValue];
    [scatterChart setAxisYWithMinimumValue:60 andMaxValue:maxYvalue  toTicks:(int)yItem.count];
    
    // 식전 드로잉 데이터값
    NSArray *xData2 = xNewBefore_DrawData;
    NSArray *yData2 = xData;
    NSArray *data02Array = [[NSArray alloc] initWithObjects:xData2, yData2, nil];
    
    // 식후 드로잉 데이터값
    NSArray *xData3 = xNewAfter_DrawData;
    NSArray *yData3 = xAfter_Data;
    NSArray *data03Array = [[NSArray alloc] initWithObjects:xData3, yData3, nil];
    
    // 투약
    NSArray *xData4 = xDrugNewBefore_DrawData;
    NSArray *yData4 = xDrugData;
    NSArray *data04Array = [[NSArray alloc] initWithObjects:xData4, nil];
    
    
    NSMutableArray *arrShapeLine1 = [[NSMutableArray alloc] init];
    NSMutableArray *arrShapeLine2 = [[NSMutableArray alloc] init];
    
    if([data02Array[0] count] != 0 || [data03Array[0] count] != 0) {
        if([data02Array[0] count] != 0) {
            PNScatterChartData2 *data01 = [PNScatterChartData2 new];
            data01.strokeColor = COLOR_MAIN_ORANGE;
            data01.fillColor = COLOR_MAIN_ORANGE;
            data01.size = 3.50f;    // 원 사이즈
            data01.itemCount = [data02Array[0] count];
            data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
            __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data02Array[0]];
            __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data02Array[1]];
            
            data01.getData = ^(NSUInteger index) {
                CGFloat xValue;
                xValue = [XAr1[index] floatValue];
                CGFloat yValue = [YAr1[index] floatValue];
                if(yValue > 240.f){
                    yValue = 240.f;
                }else if(yValue < 60.f){
                    yValue = 60.f;
                }
                
                return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
            };
            
            scatterChart.chartData = @[data01];
            
            [arrShapeLine1 setArray:@[data01]];
        }
        
        if([data03Array[0] count] != 0) {
            PNScatterChartData2 *data01 = [PNScatterChartData2 new];
            data01.strokeColor = COLOR_NATIONS_BLUE;
            data01.fillColor = COLOR_NATIONS_BLUE;
            data01.size = 3.50f;    // 원 사이즈
            data01.itemCount = [data03Array[0] count];
            data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
            __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data03Array[0]];
            __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data03Array[1]];
            
            data01.getData = ^(NSUInteger index) {
                CGFloat xValue;
                xValue = [XAr1[index] floatValue];
                CGFloat yValue = [YAr1[index] floatValue];
                if(yValue > 240.f){
                    yValue = 240.f;
                }else if(yValue < 60.f){
                    yValue = 60.f;
                }
                
                return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
            };
            
            
            
            scatterChart.chartData = @[data01];
            
            
            [arrShapeLine2 setArray:@[data01]];
            
            
            // 일봉일때만 라인그림.
            //            if(tapTag == 10){
            [scatterChart setChartLineData:arrShapeLine1 outData:arrShapeLine2];
            //            }
        }
        
    }
    
    // 투약
    if([data04Array[0] count] != 0) {
        PNScatterChartData2 *data01 = [PNScatterChartData2 new];
        data01.strokeColor = LIFERed;
        data01.fillColor = LIFERed;
        data01.size = 3.50f;    // 원 사이즈
        data01.itemCount = [data04Array[0] count];
        data01.inflexionPointStyle = PNScatterChartPointStyleCircle;
        __block NSMutableArray *XAr1 = [NSMutableArray arrayWithArray:data04Array[0]];
        __block NSMutableArray *YAr1 = [NSMutableArray arrayWithArray:data04Array[0]];
        
        data01.getData = ^(NSUInteger index) {
            CGFloat xValue = [XAr1[index] floatValue];
            CGFloat yValue = [YAr1[index] floatValue];
            
            return [PNScatterChartDataItem2 dataItemWithX:xValue AndWithY:yValue];
        };
        
        scatterChart.medicineData = @[data01];
    }
    
    [scatterChart setup];
    scatterChart.tag = 1;
    [_graphVw addSubview:scatterChart];
}


#pragma mark - UIInterfaceOrientationMask
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)updatePreviewLayer:(AVCaptureConnection*)layer orientation:(AVCaptureVideoOrientation)orientation {
    layer.videoOrientation = orientation;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:false block:^(NSTimer * _Nonnull timer) {
        
        [self viewSetup];
    }];
}

@end
