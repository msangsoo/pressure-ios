//
//  HealthMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthMainVC.h"

@interface HealthMainVC () {
    UIBarButtonItem *naviLeftBtn;
    UIView *bubbleView;
}

@end

@implementation HealthMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"건강관리";

    [self viewInit];
    
    [self navigationSetup];
    
    vcFood = [FOOD_STORYBOARD instantiateInitialViewController];
    [self addChildViewController:vcFood];
    vcFood.view.bounds = _vwMain.bounds;
    vcFood.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcFood.view];
    
    [_foodBtn setSelected:YES];
    [_walkBtn setSelected:NO];
    [_weightBtn setSelected:NO];
    [_presuBtn setSelected:NO];
    [_sugarBtn setSelected:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PointAlertClose:) name:@"PointAlertClose" object:nil];
    
    if ([[Utils getUserDefault:HEALTH_MSG_NEW] isEqualToString:@"N"]) {
        [self bubbleHealthMsg];
    }
    
    //POINT
    if ([[Utils getUserDefault:HEALTH_POINT_ALERT] isEqualToString:@"1"] ||
        [[Utils getUserDefault:HEALTH_POINT_ALERT] isEqualToString:@"2"] ||
        [[Utils getUserDefault:HEALTH_POINT_ALERT] isEqualToString:@"3"] ||
        [[Utils getUserDefault:HEALTH_POINT_ALERT] isEqualToString:@"4"]) {
        [self apiPoint:[Utils getUserDefault:HEALTH_POINT_ALERT]];
        [Utils setUserDefault:HEALTH_POINT_ALERT value:@"0"];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [bubbleView removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}

- (void)delegateResultData:(NSDictionary *)result {
    if([[result objectForKey:@"api_code"] isEqualToString:@"misson_goal_alert"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            [AlertUtils PointAlertShow:self point:[result objectForKey:@"misson_goal_point"] pointTitle:@"" pointInfo:[result objectForKey:@"misson_goal_txt"]];
        }else {
            NSDictionary *tipData = [[NSUserDefaults standardUserDefaults] objectForKey:HEALTH_TIP_DATA];
            if (tipData.count > 0) {
                [AlertUtils HealthMessageShow:self type:[tipData objectForKey:@"type"] msg:[tipData objectForKey:@"message"]];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:HEALTH_TIP_DATA];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
}

#pragma mark - NSNotificationCenter
- (void)PointAlertClose:(NSNotification *)noti {
    NSDictionary *tipData = [[NSUserDefaults standardUserDefaults] objectForKey:HEALTH_TIP_DATA];
    if (tipData.count > 0) {
        [AlertUtils HealthMessageShow:self type:[tipData objectForKey:@"type"] msg:[tipData objectForKey:@"message"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:HEALTH_TIP_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - Actions
- (IBAction)pressHealthMsg:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
    [bubbleView removeFromSuperview];
    
    UIViewController *vc = [HEALTHMESSAGE_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressHealthCheck:(id)sender {
    UIViewController *vc = [HEALTHCHECK_STORYBOARD instantiateInitialViewController];
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)pressHealthMsgClose:(id)sender {
    [Utils setUserDefault:HEALTH_MSG_NEW value:@"Y"];
    [bubbleView removeFromSuperview];
}

- (IBAction)pressTab:(UIButton *)sender {
    [self subViewInit:(int)sender.tag];
    
    [_foodBtn setSelected:NO];
    [_walkBtn setSelected:NO];
    [_weightBtn setSelected:NO];
    [_presuBtn setSelected:NO];
    [_sugarBtn setSelected:NO];
    
    BOOL viewChage = false;
    
    if(sender.tag == 1 && !_foodBtn.isSelected) {
        [_foodBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 2 && !_walkBtn.isSelected) {
        [_walkBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 3 && !_weightBtn.isSelected) {
        [_weightBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 4 && !_presuBtn.isSelected) {
        [_presuBtn setSelected:YES];
        viewChage = true;
    }else if(sender.tag == 5 && !_sugarBtn.isSelected) {
        [_sugarBtn setSelected:YES];
        viewChage = true;
    }
    
    if (viewChage) {
        [self tabSelectedView];
    }
}

#pragma mark - HealthMessageAlertVCDelegate
- (void)HealthMessageAlertVC:(HealthMessageAlertVC *)alertView type:(NSString *)type actionsType:(HealthMessageAlertActions)actionsType {
    if (actionsType == Tip) {
        HealthMessageMainVC *vc = [HEALTHMESSAGE_STORYBOARD instantiateInitialViewController];
        vc.hidesBottomBarWhenPushed = true;
        vc.tapTag = 11;
        vc.healthType = type;
        [self.navigationController pushViewController:vc animated:true];
    }else if (actionsType == Call) {
        [AlertUtils BasicAlertShow:self tag:1000 title:@"" msg:@"헬스콜센터 운영시간은\n평일 09시 ~ 19시,\n토요일 09시 ~ 13시 이며\n일요일,공휴일 휴무입니다.\n\n헬스콜센터로 연결하시겠습니까?" left:@"취소" right:@"확인"];
    }else if (actionsType == Today) {
        if ([type isEqualToString:@"혈당"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:HEALTH_SUGAR_TIP_TODAY_TIME];
        }else if ([type isEqualToString:@"혈압"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:HEALTH_PRESU_TIP_TODAY_TIME];
        }else if ([type isEqualToString:@"체중"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:HEALTH_WEIGHT_TIP_TODAY_TIME];
        }
    }
}

#pragma mark - BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 1000) {
        if (tag == 2) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", @"15885814"]] options:@{} completionHandler:nil];
        }
    }
}

#pragma mark - api
- (void)apiPoint:(NSString *)point {
//    misson_goal_alert_typ : 1 :체중등록 // 2: 혈압등록 3: 식사일지 등록 4:혈당등록
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"misson_goal_alert", @"api_code",
                                point, @"misson_goal_alert_typ",
                                nil];
    
    if(parameters != nil) {
        if(server.delegate == nil) {
            server.delegate = self;
        }
        [_model indicatorActive];
        [server serverCommunicationData:parameters];
    }else {
        NSLog(@"server parameters error = %@", parameters);
    }
}

#pragma mark - tab setup
- (void)tabSelectedView {
    [vcFood.view setHidden:YES];
    [vcWalk.view setHidden:YES];
    [vcWeight.view setHidden:YES];
    [vcPresu.view setHidden:YES];
    [vcSugar.view setHidden:YES];
    
    if(_foodBtn.isSelected) {
        [vcFood.view setHidden:NO];
    }else if(_walkBtn.isSelected) {
        [vcWalk.view setHidden:NO];
        [vcWalk viewSetup];
    }else if(_weightBtn.isSelected) {
        [vcWeight.view setHidden:NO];
    }else if(_presuBtn.isSelected) {
        [vcPresu.view setHidden:NO];
    }else if(_sugarBtn.isSelected) {
        [vcSugar.view setHidden:NO];
    }
}

- (void)subViewInit:(int)tag {
    if(tag == 1 && vcFood == nil) {
        vcFood = [FOOD_STORYBOARD instantiateInitialViewController];
        [self addChildViewController:vcFood];
        vcFood.view.bounds = _vwMain.bounds;
        vcFood.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
        [_vwMain addSubview:vcFood.view];
    }else if(tag == 2 && vcWalk == nil) {
        vcWalk = [WALK_STORYBOARD instantiateInitialViewController];
        [self addChildViewController:vcWalk];
        vcWalk.view.bounds = _vwMain.bounds;
        vcWalk.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
        [_vwMain addSubview:vcWalk.view];
    }else if(tag == 3 && vcWeight == nil) {
        vcWeight = [WEIGHT_STORYBOARD instantiateInitialViewController];
        [self addChildViewController:vcWeight];
        vcWeight.view.bounds = _vwMain.bounds;
        vcWeight.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
        [_vwMain addSubview:vcWeight.view];
    }else if(tag == 4 && vcPresu == nil) {
        vcPresu = [PRESU_STORYBOARD instantiateInitialViewController];
        [self addChildViewController:vcPresu];
        vcPresu.view.bounds = _vwMain.bounds;
        vcPresu.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
        [_vwMain addSubview:vcPresu.view];
    }else if(tag == 5 && vcSugar == nil) {
        vcSugar = [SUGAR_STORYBOARD instantiateInitialViewController];
        [self addChildViewController:vcSugar];
        vcSugar.view.bounds = _vwMain.bounds;
        vcSugar.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
        [_vwMain addSubview:vcSugar.view];
    }
}

#pragma mark - viewInit
- (void)viewInit {
    _foodBtn.tag = 1;
    [_foodBtn setTitle:@"식사" forState:UIControlStateNormal];
    [_foodBtn mediTabButton];
    
    _walkBtn.tag = 2;
    [_walkBtn setTitle:@"걷기" forState:UIControlStateNormal];
    [_walkBtn mediTabButton];
    
    _weightBtn.tag = 3;
    [_weightBtn setTitle:@"체중" forState:UIControlStateNormal];
    [_weightBtn mediTabButton];
    
    _presuBtn.tag = 4;
    [_presuBtn setTitle:@"혈압" forState:UIControlStateNormal];
    [_presuBtn mediTabButton];
    
    _sugarBtn.tag = 5;
    [_sugarBtn setTitle:@"혈당" forState:UIControlStateNormal];
    [_sugarBtn mediTabButton];
}

- (void)navigationSetup {
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naviLeftBtn = [[UIBarButtonItem alloc] init];
    [naviLeftBtn setStyle:UIBarButtonItemStylePlain];
    [naviLeftBtn setImage:healthMsgImg];
    [naviLeftBtn setTarget:self];
    [naviLeftBtn setAction:@selector(pressHealthMsg:)];
    self.navigationItem.leftBarButtonItem = naviLeftBtn;
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] init];
    [rightBtn setStyle:UIBarButtonItemStylePlain];
    [rightBtn setTitle:@"건강체크"];
    [rightBtn setTintColor:COLOR_NATIONS_BLUE];
    [rightBtn setTarget:self];
    [rightBtn setAction:@selector(pressHealthCheck:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)bubbleHealthMsg {
    CGFloat yPos = IS_IPHONEX ? 80 : 55;
    bubbleView = [[UIView alloc] initWithFrame:CGRectMake(20 ,yPos ,150 ,28)];
    
    bubbleView.layer.borderWidth = 1;
    bubbleView.layer.borderColor = COLOR_MAIN_DARK.CGColor;
    bubbleView.layer.cornerRadius = 14;
    bubbleView.layer.masksToBounds = true;
    bubbleView.backgroundColor = [UIColor whiteColor];
   
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 115, 20)];
    lbl.textColor = [UIColor blackColor];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.lineBreakMode = NSLineBreakByTruncatingTail;
    lbl.numberOfLines = 1;
    lbl.font = FONT_NOTO_MEDIUM(12);
    lbl.textColor = COLOR_MAIN_DARK;
    [bubbleView addSubview:lbl];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 120, 25)];
    [btn setTitle:@"" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:self action:@selector(pressHealthMsg:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:btn];
    
    UIButton *closebtn = [[UIButton alloc] initWithFrame:CGRectMake(125, 4.5, 25, 20)];
    [closebtn setImage:[UIImage imageNamed:@"path"] forState:UIControlStateNormal];
    [closebtn setTitleColor:COLOR_MAIN_DARK forState:UIControlStateNormal];
    [closebtn addTarget:self action:@selector(pressHealthMsgClose:) forControlEvents:UIControlEventTouchUpInside];
    [bubbleView addSubview:closebtn];
    
    [lbl setNotoText:@"건강메시지가 도착했습니다."];
    [lbl sizeToFit];
//    [self.view addSubview:bubbleView];
    [self.navigationController.view addSubview:bubbleView];
    
    UIImage *healthMsgImg = [UIImage imageNamed:@"btn_health_on_msg"]; //btn_health_msg btn_health_on_msg
    healthMsgImg = [healthMsgImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [naviLeftBtn setImage:healthMsgImg];
}

- (void)moveIndexHealthTap:(int)index {
    [self subViewInit:index];
    
    [_foodBtn setSelected:NO];
    [_walkBtn setSelected:NO];
    [_weightBtn setSelected:NO];
    [_presuBtn setSelected:NO];
    [_sugarBtn setSelected:NO];
    
    BOOL viewChage = false;
    
    if(index == 1) {
        [_foodBtn setSelected:YES];
        viewChage = true;
    }else if(index == 2) {
        [_walkBtn setSelected:YES];
        viewChage = true;
    }else if(index == 3) {
        [_weightBtn setSelected:YES];
        viewChage = true;
    }else if(index == 4) {
        [_presuBtn setSelected:YES];
        viewChage = true;
    }else if(index == 5) {
        [_sugarBtn setSelected:YES];
        viewChage = true;
    }
    
    if (viewChage) {
        [self tabSelectedView];
    }
}

@end
