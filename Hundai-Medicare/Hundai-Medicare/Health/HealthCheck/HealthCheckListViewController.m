//
//  HealthCheckListViewController.m
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 10..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "HealthCheckListViewController.h"
#import "HealthCheckTableViewCell.h"
#import "HealthCheckQuestionVC.h"

@interface HealthCheckListViewController ()

@property (nonatomic, strong) NSMutableArray *itemList;

@end

@implementation HealthCheckListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"건강 체크"];
    
    self.itemList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _model.checkMedicalArray.count; i++) {
        [self.itemList addObject:@{@"title":_model.checkMedicalArray[i], @"desc":_model.checkLevelArray[i]}];
    }
    
    _TvMain.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [self refreshTable];
}

-(void)refreshTable{
    self.itemList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _model.checkMedicalArray.count; i++) {
        [self.itemList addObject:@{@"title":_model.checkMedicalArray[i], @"desc":_model.checkLevelArray[i]}];
    }
    
    [_TvMain reloadData];
}

#pragma mark - table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.itemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* cellIdentifier = @"HealthCheckTableViewCell";
    HealthCheckTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[HealthCheckTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell.lblTitle setText:self.itemList[indexPath.row][@"title"]];
    [cell.lblDetail setText:self.itemList[indexPath.row][@"desc"]];
    
    tableView.separatorInset = UIEdgeInsetsZero;
    tableView.layoutMargins = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"HealthInfoWebViewController" sender:self];
    
}

#pragma mark - sugar delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"HealthInfoWebViewController"]) {
        HealthCheckQuestionVC *vc = [segue destinationViewController];
        
        vc.isBundle = YES;
        NSInteger index = [_TvMain indexPathForSelectedRow].row + 1;
        
        switch (index) {
            case 1:
                [LogDB insertDb:HM03 m_cod:HM03030 s_cod:HM03030001];
                break;
            case 2:
                [LogDB insertDb:HM03 m_cod:HM03031 s_cod:HM03031001];
                break;
            case 3:
                [LogDB insertDb:HM03 m_cod:HM03032 s_cod:HM03032001];
                break;
            case 4:
                [LogDB insertDb:HM03 m_cod:HM03033 s_cod:HM03033001];
                break;
            case 5:
                [LogDB insertDb:HM03 m_cod:HM03034 s_cod:HM03034001];
                break;
            case 6:
                [LogDB insertDb:HM03 m_cod:HM03035 s_cod:HM03035001];
                break;
            case 7:
                [LogDB insertDb:HM03 m_cod:HM03036 s_cod:HM03036001];
                break;
            case 8:
                [LogDB insertDb:HM03 m_cod:HM03037 s_cod:HM03037001];
                break;
            case 9:
                [LogDB insertDb:HM03 m_cod:HM03038 s_cod:HM03038001];
                break;
            default:
                break;
        }
        
        vc.title = [self.itemList objectAtIndex:[_TvMain indexPathForSelectedRow].row][@"title"];
        vc.titleString = [self.itemList objectAtIndex:[_TvMain indexPathForSelectedRow].row][@"title"];
        vc.LoadFilesName = [NSString stringWithFormat:@"question%ld", (long)index];
        vc.indexMedical = [_TvMain indexPathForSelectedRow].row;
    }
}

@end
