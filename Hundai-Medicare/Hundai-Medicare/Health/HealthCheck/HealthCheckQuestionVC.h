//
//  HealthCheckVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface HealthCheckQuestionVC : BaseViewController<ServerCommunicationDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *wvMain;

@property(nonatomic, strong) NSString *urlString;
@property(nonatomic, assign) BOOL isBundle;
@property(nonatomic, strong) NSString *titleString;
@property(nonatomic, strong) NSString *LoadFilesName;
@property(nonatomic, assign) NSInteger indexMedical;

@end
