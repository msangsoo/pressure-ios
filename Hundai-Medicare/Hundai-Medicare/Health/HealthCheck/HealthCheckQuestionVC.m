//
//  HealthCheckVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthCheckQuestionVC.h"

@interface HealthCheckQuestionVC () {
    NSString *totalString;
    NSString *keyString;
}

@property (nonatomic, strong) NSString *strReceivedComment;

@end

@implementation HealthCheckQuestionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:self.title];
    
    totalString = @"";
    keyString = @"";
    
    server = [[ServerCommunication alloc] init];
    [server setDelegate:self];
    
    _wvMain.scrollView.bounces = false;
    _wvMain.scrollView.bouncesZoom = false;
    _wvMain.delegate = self;
    
    if (self.isBundle) {
        if (self.LoadFilesName != nil) {
            NSArray *checkLevelArr = [[NSArray arrayWithArray:_model.checkLevelArray] mutableCopy];
            if ([[checkLevelArr objectAtIndex:_indexMedical] isEqualToString:@"체크하기"]) {
                [self loadHTMLFileWithFileName:self.LoadFilesName];
            }else {
                NSArray *checkResultArr = [[NSArray arrayWithArray:_model.checkResultArray] mutableCopy];
                [self requestDiagnosisWithArray:[checkResultArr objectAtIndex:_indexMedical]];
            }
        }
    } else {
        [self loadWithWebView:_wvMain webString:self.urlString];
    }
}

#pragma mark - webview touch event method
-(void)loadHTMLFileWithFileName:(NSString *)fileName {
    NSString *infoTextSrc = [[NSBundle mainBundle] pathForResource:fileName ofType:@"html"];
    NSString *infoText = [NSString stringWithContentsOfFile:infoTextSrc encoding:NSUTF8StringEncoding error:nil];
    NSString *strPath = [[NSBundle mainBundle] bundlePath];
    [_wvMain loadHTMLString:infoText baseURL:[NSURL fileURLWithPath:strPath]];
}

- (void)loadWithWebView:(UIWebView*)webView webString:(NSString*)webString {
    NSURL* url = [NSURL URLWithString:webString];
    NSURLRequest* urlReq = [NSURLRequest requestWithURL:url];
    [_wvMain loadRequest:urlReq];
}

- (void) loadLoginRes:(NSString*)urlStr {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlStr]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
}

-(void)refreshMedical{
    [self.navigationItem setTitle:_titleString];
    [self loadHTMLFileWithFileName:self.LoadFilesName];
}

-(void)requestDiagnosisWithArray:(NSArray *)receivedArray {
    [_model indicatorActive];
    
    totalString = [NSString stringWithFormat:@"%@",[receivedArray lastObject]];
    keyString = [NSString stringWithFormat:@"%@",[receivedArray firstObject]];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"hra_check_result_input", @"api_code",
                                totalString, @"total_score",
                                keyString, @"moon_key",
                                nil];
    
    [server serverCommunicationData:parameters];
}

#pragma mark - server delegate

-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"hra_check_result_input"]) {
        NSString *htmlURL =  [result objectForKey:@"comment"];
        htmlURL = [htmlURL stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        htmlURL = [htmlURL stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        htmlURL = [htmlURL stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        htmlURL = [htmlURL stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        htmlURL = [htmlURL stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        [self.navigationItem setTitle:@"체크결과"];
        self.strReceivedComment = [NSString stringWithFormat:@"%@", htmlURL];
        [self loadHTMLFileWithFileName:@"answer"];
        
        NSString *levelStr = [NSString stringWithFormat:@"%@", [result objectForKey:@"d_level"]];
        
        NSString *descStr;
        if ([levelStr isEqualToString:@"1"]) {
            descStr = @"높음";
        } else if ([levelStr isEqualToString:@"2"]) {
            descStr = @"중간";
        } else if ([levelStr isEqualToString:@"3"]) {
            descStr = @"낮음";
        } else if ([levelStr isEqualToString:@"4"]) {
            descStr = @"매우낮음";
        } else {
            descStr = @"체크하기";
        }
        
        NSMutableArray *muArray = [[NSMutableArray arrayWithArray:_model.checkLevelArray] mutableCopy];
        [muArray replaceObjectAtIndex:self.indexMedical withObject:descStr];
        [_model setCheckLevelArray:muArray];
        
        NSMutableArray *resultArray = [[NSMutableArray arrayWithArray:_model.checkResultArray] mutableCopy];
        [resultArray replaceObjectAtIndex:self.indexMedical withObject:@[keyString, totalString]];
        [_model setCheckResultArray:resultArray];
    }
    
}

#pragma mark Button Actions

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"request = %@",request);
    
    NSString *requestStr = [[request URL] absoluteString];
    NSLog(@"requestStr:%@",requestStr);
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"jscall:"]) {
        NSString* urlStrings = [[[request URL] absoluteString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        urlStrings = [urlStrings stringByReplacingOccurrencesOfString:@"jscall:" withString:@""];
        NSArray *getDataArray = [urlStrings componentsSeparatedByString:@"&"];
        [self performSelectorOnMainThread:@selector(requestDiagnosisWithArray:) withObject:getDataArray waitUntilDone:NO];
        return NO;
    } else if ( [[[request URL] absoluteString] hasPrefix:@"open:"]) {
        NSString* urlStrings = [[[request URL] absoluteString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        urlStrings = [urlStrings stringByReplacingOccurrencesOfString:@"open:" withString:@""];
        [self performSelectorOnMainThread:@selector(loadLoginRes:) withObject:urlStrings waitUntilDone:NO];
        return NO;
    } else if ( [[[request URL] absoluteString] hasPrefix:@"recall:"]) {
        [self performSelectorOnMainThread:@selector(refreshMedical) withObject:nil waitUntilDone:NO];
        return NO;
    } else if ( [[[request URL] absoluteString] hasPrefix:@"finish:"]) {
     
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    } else if ( [[[request URL] absoluteString] hasPrefix:@"tel:"]) {
        return NO;
    } else if ( [[[request URL] absoluteString] hasPrefix:@"heathcal:"]) {
        [LogDB insertDb:HM01 m_cod:HM03040 s_cod:HM03040001];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", @"15885814"]] options:@{} completionHandler:nil];
        return NO;
    }else if ([[[request URL] absoluteString] hasPrefix:@"ioscall:"]) {
        NSString* urlStrings = [[[request URL] absoluteString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        urlStrings = [urlStrings stringByReplacingOccurrencesOfString:@"ioscall:" withString:@""];
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }else if ([[[request URL] absoluteString] hasPrefix:@"hisback:"]) {
        
        [self loadHTMLFileWithFileName:self.LoadFilesName];
        return NO;
    }
    
    
    return YES;
}

#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if (!self.isBundle)  {
    } else {
        if (self.strReceivedComment != nil) {
            NSString *functionCall = [NSString stringWithFormat:@"sendContent('%@');", self.strReceivedComment];
            [_wvMain stringByEvaluatingJavaScriptFromString:functionCall];
            self.strReceivedComment = nil;
        }
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

@end
