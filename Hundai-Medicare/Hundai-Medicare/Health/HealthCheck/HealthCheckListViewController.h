//
//  HealthCheckListViewController.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 10..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HealthCheckListViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *TvMain;


@end
