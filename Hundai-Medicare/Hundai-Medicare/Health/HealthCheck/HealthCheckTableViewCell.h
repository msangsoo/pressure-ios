//
//  HealthCheckTableViewCell.h
//  LifeCare
//
//  Created by INSYSTEMS CO., LTD on 2017. 7. 10..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthCheckTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;

@end
