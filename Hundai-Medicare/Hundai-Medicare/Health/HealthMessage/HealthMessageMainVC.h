//
//  HealthMessageMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "HealthMessageVC.h"
#import "HealthManagementTIPVC.h"

@interface HealthMessageMainVC : BaseViewController {
    HealthMessageVC *vcMessage;
    HealthManagementTIPVC *vcManagementTip;
}

@property (nonatomic) int tapTag;
@property (strong, nonatomic) IBOutlet NSString *healthType;

@property (weak, nonatomic) IBOutlet UIButton *messageBtn;
@property (weak, nonatomic) IBOutlet UIButton *managementTipBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarVwLeftConst;

@end
