//
//  HealthManagementTIPVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface HealthManagementTIPVC : BaseViewController

@property (strong, nonatomic) IBOutlet NSString *healthType;

@property (weak, nonatomic) IBOutlet UIButton *foodBtn;
@property (weak, nonatomic) IBOutlet UIButton *walkBtn;
@property (weak, nonatomic) IBOutlet UIButton *weightBtn;
@property (weak, nonatomic) IBOutlet UIButton *preBtn;
@property (weak, nonatomic) IBOutlet UIButton *sugarBtn;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBottomConst;

- (void)viewSett;

@end
