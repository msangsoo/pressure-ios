//
//  HealthMessageVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "HealthMessageCell.h"

#import "MessageDBWraper.h"

@interface HealthMessageVC : BaseViewController<ServerCommunicationDelegate, UITableViewDelegate, UITableViewDataSource> {
    MessageDBWraper *_db;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@end
