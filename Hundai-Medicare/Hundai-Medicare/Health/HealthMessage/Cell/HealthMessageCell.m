//
//  HealthMessageCell.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 29/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthMessageCell.h"

@implementation HealthMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self viewInit];
}

#pragma mark - viewInit
- (void)viewInit {
    _dateLbl.font = FONT_NOTO_LIGHT(12);
    _dateLbl.textColor = COLOR_GRAY_SUIT;
    _dateLbl.text = @"";
    
    _msgLbl.font = FONT_NOTO_REGULAR(16);
    _msgLbl.textColor = COLOR_MAIN_DARK;
    _msgLbl.text = @"";
    
    [_delBtn setImage:[UIImage imageNamed:@"btn_del"] forState:UIControlStateNormal];
}

@end
