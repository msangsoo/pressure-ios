//
//  HealthMessageCell.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 29/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;


@end
