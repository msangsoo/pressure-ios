//
//  HealthMessageMainVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthMessageMainVC.h"

@interface HealthMessageMainVC ()

@end

@implementation HealthMessageMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"건강메시지"];
    
    vcMessage = [self.storyboard instantiateViewControllerWithIdentifier:@"HealthMessageVC"];
    [self addChildViewController:vcMessage];
    vcMessage.view.bounds = _vwMain.bounds;
    vcMessage.view.layer.frame = CGRectMake(0, 0, _vwMain.layer.frame.size.width, _vwMain.layer.frame.size.height);
    [_vwMain addSubview:vcMessage.view];
    [vcMessage.view setHidden:false];
    
    [self viewInit];

}


#pragma mark - viewInit
- (void)viewInit {
    
}

@end
