//
//  HealthManagementTIPVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthManagementTIPVC.h"

@interface HealthManagementTIPVC ()

@end

@implementation HealthManagementTIPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.scrollView.bounces = false;
    
    [self viewInit];
    
    [self viewSett];
}

#pragma mark - Actions
- (IBAction)pressTapBtn:(UIButton *)sender {
    BOOL viewChage = false;
    
    if (sender.tag == 1 && !_foodBtn.isSelected) {
        _healthType = @"식사";
        viewChage = true;
    }else if (sender.tag == 2 && !_walkBtn.isSelected) {
        _healthType = @"걷기";
        viewChage = true;
    }else if (sender.tag == 3 && !_weightBtn.isSelected) {
        _healthType = @"체중";
        viewChage = true;
    }else if (sender.tag == 4 && !_preBtn.isSelected) {
        _healthType = @"혈압";
        viewChage = true;
    }else if (sender.tag == 5 && !_sugarBtn.isSelected) {
        _healthType = @"혈당";
        viewChage = true;
    }
    
    if (viewChage) {
        [self viewSett];
    }
}

- (IBAction)pressBottom:(id)sender {
    
}

#pragma mark - viewInit
- (void)viewSett {
    NSString *m_code = @"HL0101";
    _foodBtn.selected = false;
    _walkBtn.selected = false;
    _weightBtn.selected = false;
    _preBtn.selected = false;
    _sugarBtn.selected = false;
    
    self.bottomBtn.hidden = true;
    self.webViewBottomConst.constant = 0;
    if([_healthType isEqualToString:@"식사"]) {
        m_code = @"HL0101";
        _foodBtn.selected = true;
    }else if([_healthType isEqualToString:@"걷기"]) {
        m_code = @"HL0103";
        _walkBtn.selected = true;
    }else if([_healthType isEqualToString:@"체중"]) {
        m_code = @"HL0102";
        self.weightBtn.selected = true;
        
        //        self.bottomBtn.hidden = false;
        //        self.webViewBottomConst.constant = 80;
        //        [self.bottomBtn setTitle:@"다이어트" forState:UIControlStateNormal];
        //        [self.bottomBtn borderRadiusButton:COLOR_GRAY_SUIT];
    }else if([_healthType isEqualToString:@"혈압"]) {
        m_code = @"HL0104";
        self.preBtn.selected = true;
    }else if([_healthType isEqualToString:@"혈당"]) {
        m_code = @"HL0105";
        self.sugarBtn.selected = true;
    }
    
    NSString *webString = [NSString stringWithFormat:@"https://m.shealthcare.co.kr/HL_MED_COMMUNITY/MTIP/MTIP.ASP?WKEY=%@", m_code];
    NSURL* url = [NSURL URLWithString:webString];
    NSURLRequest* urlReq = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:urlReq];
    [_webView.scrollView setScrollsToTop:YES];
}

- (void)viewInit {
    _foodBtn.tag = 1;
    _foodBtn.titleLabel.font = FONT_NOTO_MEDIUM(14);
    [_foodBtn setTitle:@"식사" forState:UIControlStateNormal];
    [_foodBtn setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [_foodBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_foodBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:_foodBtn.bounds] forState:UIControlStateNormal];
    [_foodBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_foodBtn.bounds] forState:UIControlStateSelected];
    
    _walkBtn.tag = 2;
    _walkBtn.titleLabel.font = FONT_NOTO_MEDIUM(14);
    [_walkBtn setTitle:@"걷기" forState:UIControlStateNormal];
    [_walkBtn setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [_walkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_walkBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:_walkBtn.bounds] forState:UIControlStateNormal];
    [_walkBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_walkBtn.bounds] forState:UIControlStateSelected];
    
    _weightBtn.tag = 3;
    _weightBtn.titleLabel.font = FONT_NOTO_MEDIUM(14);
    [_weightBtn setTitle:@"체중" forState:UIControlStateNormal];
    [_weightBtn setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [_weightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_weightBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:_weightBtn.bounds] forState:UIControlStateNormal];
    [_weightBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_weightBtn.bounds] forState:UIControlStateSelected];
    
    _preBtn.tag = 4;
    _preBtn.titleLabel.font = FONT_NOTO_MEDIUM(14);
    [_preBtn setTitle:@"혈압" forState:UIControlStateNormal];
    [_preBtn setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [_preBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_preBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:_preBtn.bounds] forState:UIControlStateNormal];
    [_preBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_preBtn.bounds] forState:UIControlStateSelected];
    
    _sugarBtn.tag = 5;
    _sugarBtn.titleLabel.font = FONT_NOTO_MEDIUM(14);
    [_sugarBtn setTitle:@"혈당" forState:UIControlStateNormal];
    [_sugarBtn setTitleColor:RGB(143, 144, 156) forState:UIControlStateNormal];
    [_sugarBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_sugarBtn setBackgroundImage:[UIImage setBackgroundImageByColor:RGB(218, 218, 218) withFrame:_sugarBtn.bounds] forState:UIControlStateNormal];
    [_sugarBtn setBackgroundImage:[UIImage setBackgroundImageByColor:COLOR_NATIONS_BLUE withFrame:_sugarBtn.bounds] forState:UIControlStateSelected];
    
    if ([_healthType isEqualToString:@""] || _healthType == nil) {
        _healthType = @"식사";
        _foodBtn.selected = true;
    }
}

@end
