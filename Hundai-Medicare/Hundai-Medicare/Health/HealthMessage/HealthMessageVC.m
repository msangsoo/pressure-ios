//
//  HealthMessageVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 17/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthMessageVC.h"

@interface HealthMessageVC () {
    NSInteger msgSelectNum;
    NSArray *_items;
    float lblHeight;
}

@end

@implementation HealthMessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lblHeight = 0.f;
    
    _items = [[NSArray alloc] init];
    _db = [[MessageDBWraper alloc] init];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    _items = [_db getResult:@"2"];
    [_tableView reloadData];
}

#pragma mark - server delegate
-(void)delegateResultString:(id)result CODE:(NSString *)code {
}
-(void)delegatePush:(NSInteger)status {
}
-(void)delegateError {
}
- (void)delegateResultData:(NSDictionary *)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"infra_del_message"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            [_db DeleteDb:[[_items objectAtIndex:msgSelectNum] objectForKey:@"idx"]];
            
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[_items copy]];
            [tempArr removeObjectAtIndex:msgSelectNum];
            _items = [NSArray arrayWithArray:tempArr];
            [_tableView reloadData];
            
            [AlertUtils BasicAlertShow:self tag:101 title:@"" msg:@"삭제되었습니다." left:@"확인" right:@""];
        }else {
            [_db DeleteDb:[[_items objectAtIndex:msgSelectNum] objectForKey:@"idx"]];
            
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[_items copy]];
            [tempArr removeObjectAtIndex:msgSelectNum];
            _items = [NSArray arrayWithArray:tempArr];
            [_tableView reloadData];
            
            [AlertUtils BasicAlertShow:self tag:101 title:@"" msg:@"삭제되었습니다." left:@"확인" right:@""];
        }
    }else if([[result objectForKey:@"api_code"] isEqualToString:@"infra_del_message_all"]) {
        if([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
            [_db AllDeleteDB];
            _items = [_db getResult:@"2"];
            [_tableView reloadData];
            
            [AlertUtils BasicAlertShow:nil tag:1 title:@"" msg:@"삭제되었습니다." left:@"확인" right:@""];
        }else {
            [_db AllDeleteDB];
            _items = [_db getResult:@"2"];
            [_tableView reloadData];
            
//            [AlertUtils BasicAlertShow:nil tag:1 title:@"알림" msg:@"다시 시도해 주세요." left:@"확인" right:@""];
            [AlertUtils BasicAlertShow:nil tag:1 title:@"알림" msg:@"삭제되었습니다." left:@"확인" right:@""];
        }
    }
}

#pragma mark - table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dit = [_items objectAtIndex:indexPath.row];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH - 60, 0)];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.font = FONT_NOTO_REGULAR(16);
    [lbl setNotoText:[dit objectForKey:@"msg"]];
    return [Utils getLabelHeight:lbl] + 30.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"HealthMessageCell";
    HealthMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"HealthMessageCell" bundle:nil] forCellReuseIdentifier:@"HealthMessageCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSDictionary *dit = [_items objectAtIndex:indexPath.row];
    
    [cell.msgLbl setNotoText:[dit objectForKey:@"msg"]];
    [cell.dateLbl setNotoText:[Utils getTimeFormat:[dit objectForKey:@"regdate"] beTime:@"yyyy-MM-dd HH:mm" afTime:@"yy년MM월dd일  HH시mm분"]];
    cell.delBtn.tag = indexPath.row;
    [cell.delBtn addTarget:self action:@selector(selectCellDel:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

#pragma mark - Actions
- (void)selectCellDel:(id)sender {
    UIButton *btn = (UIButton *)sender;
    msgSelectNum = btn.tag;
    
    [AlertUtils BasicAlertShow:self tag:100 title:@"" msg:@"삭제하시겠습니까?" left:@"취소" right:@"확인"];
}

- (IBAction)pressAllDelete:(id)sender {
    [LogDB insertDb:HM01 m_cod:HM03042 s_cod:HM03042001];
    
    [AlertUtils BasicAlertShow:self tag:102 title:@"" msg:@"모든 건강메시지를\n삭제하시겠습니까?" left:@"취소" right:@"확인"];
}

#pragma mark -BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag {
    if (alertView.tag == 100) {
        if(tag == 1) {
            NSLog(@"취소");
        }else {
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"infra_del_message", @"api_code",
                                        [[_items objectAtIndex:msgSelectNum] objectForKey:@"idx"], @"idx",
                                        nil];
            
            if(server.delegate == nil) {
                server.delegate = self;
            }
            [server serverCommunicationData:parameters];
        }
    }else if (alertView.tag == 102) {
        if(tag == 2) {
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        @"infra_del_message_all", @"api_code",
                                        nil];
            
            if(server.delegate == nil) {
                server.delegate = self;
            }
            [server serverCommunicationData:parameters];
        }
    }
}

#pragma mark - viewInit
- (void)viewInit {
    [_allBtn setTitle:@"모두 삭제" forState:UIControlStateNormal];
    [_allBtn borderRadiusButton:COLOR_GRAY_SUIT];
}

@end
