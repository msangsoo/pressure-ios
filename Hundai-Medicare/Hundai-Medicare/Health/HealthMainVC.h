//
//  HealthMainVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 01/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "FoodMainVC.h"
#import "WalkMainVC.h"
#import "WeightMainVC.h"
#import "PresuMainVC.h"
#import "SugarMainVC.h"

#import "HealthMessageMainVC.h"


@interface HealthMainVC : BaseViewController<ServerCommunicationDelegate, HealthMessageAlertVCDelegate, BasicAlertVCDelegate> {
    FoodMainVC *vcFood;
    WalkMainVC *vcWalk;
    WeightMainVC *vcWeight;
    PresuMainVC *vcPresu;
    SugarMainVC *vcSugar;
}

@property (weak, nonatomic) IBOutlet UIButton *foodBtn;
@property (weak, nonatomic) IBOutlet UIButton *walkBtn;
@property (weak, nonatomic) IBOutlet UIButton *weightBtn;
@property (weak, nonatomic) IBOutlet UIButton *presuBtn;
@property (weak, nonatomic) IBOutlet UIButton *sugarBtn;

@property (weak, nonatomic) IBOutlet UIView *vwMain;

- (void)moveIndexHealthTap:(int)index;

@end
