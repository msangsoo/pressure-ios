//
//  BasicAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasicAlertVC : UIViewController

@property (nonatomic, assign) long tag;
@property (nonatomic, retain) id delegate;

@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *msgStr;
@property (nonatomic, strong) NSString *leftStr;
@property (nonatomic, strong) NSString *rightStr;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@property (weak, nonatomic) IBOutlet UIView *leftVw;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIView *rightVw;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contairVwHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgLblTopConst;

@end

@protocol BasicAlertVCDelegate
- (void)BasicAlert:(BasicAlertVC *)alertView clickButtonTag:(NSInteger)tag;
@end

