//
//  PointAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PointAlertVC : UIViewController

@property (nonatomic, strong) NSString *point;
@property (nonatomic, strong) NSString *pointTitle;
@property (nonatomic, strong) NSString *pointInfo;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *pointLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end
