//
//  BasicInputVC.h
//  Hundai-Medicare
//
//  Created by Daesun moon on 27/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
 

@interface BasicInputVC : UIViewController<UITextFieldDelegate>

@property (nonatomic, assign) long tag;
@property (nonatomic, retain) id delegate;
@property (assign) BOOL isTxtFieldsDelegate;
@property (nonatomic, assign) int dot;  //소숫점 자리수
@property (nonatomic, retain) NSString* itemsTitle;
@property (nonatomic, retain) NSString* itemsValue;

@property (nonatomic, assign) UIKeyboardType keyboardType;


@property (weak, nonatomic) IBOutlet UIView *contairVw;
@property (weak, nonatomic) IBOutlet UILabel *itemTitleLbl;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldInput;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;


- (IBAction)pressCancel:(id)sender;
- (IBAction)pressConfirm:(id)sender;

@end


@protocol BasicInputVCDelegate
- (void)BasicInput:(BasicInputVC *)alertView returnData:(NSString*)value;
@end
