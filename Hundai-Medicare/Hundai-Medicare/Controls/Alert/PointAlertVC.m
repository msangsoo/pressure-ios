//
//  PointAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 19/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "PointAlertVC.h"

@interface PointAlertVC ()

@end

@implementation PointAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

- (IBAction)pressConfirm:(UIButton *)sender {
    [self dismiss:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PointAlertClose" object:nil];
    }];
}

#pragma mark - viewInit

- (void)viewInit {
    _pointLbl.font = FONT_NOTO_REGULAR(48);
    _pointLbl.textColor = [UIColor whiteColor];
    _pointLbl.text = @"";
    _pointLbl.numberOfLines = 0;
    
    _titleLbl.font = FONT_NOTO_REGULAR(14);
    _titleLbl.textColor = COLOR_GRAY_SUIT;
    _titleLbl.text = @"";
    
    _msgLbl.font = FONT_NOTO_REGULAR(14);
    _msgLbl.text = @"";
    _msgLbl.numberOfLines = 0;
    
    [_confirmBtn setTitle:@"확인" forState:UIControlStateNormal];
    [_confirmBtn borderRadiusButton:COLOR_NATIONS_BLUE];
    
    [_pointLbl setNotoText:[NSString stringWithFormat:@"%@P", self.point]];
    [_titleLbl setNotoText:self.pointTitle];
    [self setMsgText:self.pointInfo];
}

- (void)setMsgText:(NSString *)text {
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSMutableAttributedString *pointMsgStr = [[NSMutableAttributedString alloc] initWithString:text];
    [pointMsgStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, text.length)];
    [pointMsgStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_REGULAR(14)
                     range:NSMakeRange(0, text.length)];
    [pointMsgStr addAttribute:NSForegroundColorAttributeName
                     value:COLOR_MAIN_DARK
                     range:NSMakeRange(0, text.length)];
    [attstr appendAttributedString:pointMsgStr];
    
//    NSMutableAttributedString *lastStr = [[NSMutableAttributedString alloc] initWithString:@"가\n지급되었습니다."];
//    [lastStr addAttribute:NSKernAttributeName
//                        value:[NSNumber numberWithFloat:number]
//                        range:NSMakeRange(0, @"가\n지급되었습니다.".length)];
//    [lastStr addAttribute:NSFontAttributeName
//                        value:FONT_NOTO_REGULAR(14)
//                        range:NSMakeRange(0, @"가\n지급되었습니다.".length)];
//    [lastStr addAttribute:NSForegroundColorAttributeName
//                        value:COLOR_GRAY_SUIT
//                        range:NSMakeRange(0, @"가\n지급되었습니다.".length)];
//    [attstr appendAttributedString:lastStr];
    
    self.msgLbl.attributedText = attstr;
}

@end
