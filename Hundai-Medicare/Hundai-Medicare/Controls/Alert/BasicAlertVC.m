//
//  BasicAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 11/02/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "BasicAlertVC.h"

@interface BasicAlertVC ()

@end

@implementation BasicAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.tag = self.tag;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressChoies:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [self dismiss:^{
        if (self.delegate)
            [self.delegate BasicAlert:self clickButtonTag:btn.tag];
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_MEDIUM(21);
    _titleLbl.textColor = COLOR_GRAY_SUIT;
    _titleLbl.text = @"";
    
    _msgLbl.font = FONT_NOTO_LIGHT(16);
    _msgLbl.textColor = COLOR_GRAY_SUIT;
    _msgLbl.text = @"";
    
    _leftBtn.tag = 1;
    [_leftBtn setTitle:@"" forState:UIControlStateNormal];
    [_leftBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    _rightBtn.tag = 2;
    [_rightBtn setTitle:@"" forState:UIControlStateNormal];
    [_rightBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    if ([_titleStr isEqualToString:@""] && [_msgStr isEqualToString:@""]) {
        _contairVwHeightConst.constant = 260;
    }else if ([_titleStr length] > 0 && [_msgStr length] > 0) {
        if ([_msgStr length] > 0) {
            _msgLblTopConst.constant = 10;
            [_msgLbl setNotoText:_msgStr];
            [_msgLbl sizeToFit];
            _contairVwHeightConst.constant = 140 +_msgLbl.frame.size.height;
        }
    }else {
        _contairVwHeightConst.constant = 160;
        if ([_msgStr length] > 0) {
            _msgLblTopConst.constant = -31.5;
            [_msgLbl setNotoText:_msgStr];
            [_titleLbl setNotoText:@""];
            [_titleLbl sizeToFit];
            [_msgLbl sizeToFit];
            _contairVwHeightConst.constant = 110 +_msgLbl.frame.size.height;
        }
    }
    
    [_titleLbl setNotoText:_titleStr];
    [_msgLbl setNotoText:_msgStr];
    [_contairVw layoutIfNeeded];
    
    if (_leftStr.length < 1 && _rightStr.length < 1) {
        [_leftBtn setTitle:@"취소" forState:UIControlStateNormal];
        [_rightBtn setTitle:@"확인" forState:UIControlStateNormal];
    }else if (_leftStr.length > 0 && _rightStr.length < 1) {
        [_leftBtn setTitle:_leftStr forState:UIControlStateNormal];
        [_leftBtn borderRadiusButton:COLOR_NATIONS_BLUE];
        _rightVw.hidden = true;
    }else if (_leftStr.length < 1 && _rightStr.length > 0) {
        [_rightBtn setTitle:_rightStr forState:UIControlStateNormal];
        _leftVw.hidden = true;
    }else {
        [_leftBtn setTitle:_leftStr forState:UIControlStateNormal];
        [_rightBtn setTitle:_rightStr forState:UIControlStateNormal];
    }
}


@end
