//
//  EmojiDefaultVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

typedef enum {
    happy,
    sad,
    error,
    confirm,
    mail
} EmojiImgType;

@interface EmojiDefaultVC : BaseViewController

@property (nonatomic, assign) long tag;
@property (nonatomic, retain) id delegate;

@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *msgStr;
@property (nonatomic, strong) NSString *leftStr;
@property (nonatomic, strong) NSString *rightStr;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UIImageView *emojiImgVw;

@property (weak, nonatomic) IBOutlet UIView *titleVw;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *msgVw;
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@property (weak, nonatomic) IBOutlet UIView *leftVw;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIView *rightVw;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

/* NSLayoutConstraint */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contairVwHeightConst;


- (void)emojiType:(EmojiImgType)type;

@end

@protocol EmojiDefaultVCDelegate
- (void)EmojiDefaultVC:(EmojiDefaultVC *)alertView clickButtonTag:(NSInteger)tag;
@end

