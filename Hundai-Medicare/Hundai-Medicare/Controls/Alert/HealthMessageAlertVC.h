//
//  HealthMessageAlertVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    Tip,
    Call,
    Today
} HealthMessageAlertActions;

@interface HealthMessageAlertVC : UIViewController

@property (nonatomic, strong) NSString *healthTypeStr;
@property (nonatomic, strong) NSString *msgStr;

@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UITextView *msgTv;

@property (weak, nonatomic) IBOutlet UIButton *tipBtn;

@property (weak, nonatomic) IBOutlet UILabel *wordLbl;

@property (weak, nonatomic) IBOutlet UIButton *callBtn;

@end
@protocol HealthMessageAlertVCDelegate
- (void)HealthMessageAlertVC:(HealthMessageAlertVC *)alertView type:(NSString *)type actionsType:(HealthMessageAlertActions)actionsType;
@end
