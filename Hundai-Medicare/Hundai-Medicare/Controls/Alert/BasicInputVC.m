//
//  BasicInputVC.m
//  Hundai-Medicare
//
//  Created by Daesun moon on 27/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "BasicInputVC.h"

@interface BasicInputVC ()

@end

@implementation BasicInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.tag = self.tag;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    [self viewInit];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}


#pragma mark - viewInit
- (void)viewInit {
//    _titleLbl.font = FONT_NOTO_MEDIUM(25);
//    _titleLbl.textColor = COLOR_GRAY_SUIT;
//    _titleLbl.text = @"";
    
    _itemTitleLbl.font = FONT_NOTO_THIN(16);
    _itemTitleLbl.textColor = COLOR_MAIN_DARK;
    _itemTitleLbl.text = self.itemsTitle;
    
    _btnCancel.tag = 1;
    [_btnCancel setTitle:@"취소" forState:UIControlStateNormal];
    [_btnCancel borderRadiusButton:COLOR_GRAY_SUIT];
    
    _btnConfirm.tag = 2;
    [_btnConfirm setTitle:@"확인" forState:UIControlStateNormal];
    
    [_btnConfirm backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    
    _txtFieldInput.layer.borderWidth = 1;
    _txtFieldInput.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    _txtFieldInput.textColor = COLOR_NATIONS_BLUE;
    _txtFieldInput.placeholder = self.itemsValue;
    
    
    if(!self.keyboardType){
        [_txtFieldInput setKeyboardType:UIKeyboardTypeDefault];
    }else {
        [_txtFieldInput setKeyboardType:self.keyboardType];
    }
    if (self.keyboardType == UIKeyboardTypeDecimalPad
        || self.isTxtFieldsDelegate) {
        _txtFieldInput.delegate = self;
    }
    
    
    [_contairVw layoutIfNeeded];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        return YES;
    }
    if ([self.itemsTitle isEqualToString:@"휴대폰번호"]) {
        if ([textField.text length]>10) {
            return NO;
        }else{
            return YES;
        }
    }
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        NSString *full = textField.text;
        NSArray *arr = [full componentsSeparatedByString:@"."];
        NSString *fir = [arr objectAtIndex:0];
        NSString *se = [arr objectAtIndex:1];
        if ([self.itemsTitle isEqualToString:@"키"]) {
            if ([se length]>=1) {
                return NO;
            }
        }else {
            if ([se length]>=2) {
                return NO;
            }
        }
        if ([fir length]>3) {
            return NO;
        }
        if ([full length] >=6) {
            return NO;
        }
        return YES;
    }
    else if ([textField.text length] >= 3) {
        
        if ([string isEqualToString:@"."] && [textField.text rangeOfString:@"."].location == NSNotFound) {
            return YES;
        }
        return NO;
    }
    
    if ([string isEqualToString:@"."] && [[textField.text stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        return NO;
    }
    if ([string isEqualToString:@"."] && [textField.text rangeOfString:@"."].location != NSNotFound) {
        return NO;
    }
    return YES;
}


#pragma mark - Actions
- (IBAction)pressCancel:(id)sender {
    
    [self dismiss:^{
    
    }];
}

- (IBAction)pressConfirm:(id)sender {
    
    [self dismiss:^{
        if (self.delegate)
            [self.delegate BasicInput:self returnData:self.txtFieldInput.text];
    }];
}
@end
