//
//  MallLoginVC.m
//  Hundai-Medicare
//
//  Created by Daesun moon on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "MallLoginVC.h"

@interface MallLoginVC ()

@end

@implementation MallLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    server.delegate = self;
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    [self viewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string length]==0) {
        return YES;
    }
    if (textField == _txtFieldsID) {
        _missidMsg.hidden = YES;
    }else if (textField == _txtFieldsPass) {
        _missPassMsg.hidden = YES;
    }
    return YES;
}

#pragma mark - viewInit
- (void)viewInit {
    
    _titleLbl.font = FONT_NOTO_BLACK(18);
    _titleLbl.textColor = [UIColor blackColor];
    _titleLbl.text = @"라이프케어몰 아이디 연동";
    
    _titleIDLbl.font = FONT_NOTO_REGULAR(14);
    _titleIDLbl.textColor = COLOR_GRAY_SUIT;
    _titleIDLbl.text = @"아이디";
    
    _titlePassLbl.font = FONT_NOTO_REGULAR(14);
    _titlePassLbl.textColor = COLOR_GRAY_SUIT;
    _titlePassLbl.text = @"비밀번호";
    
    _txtFieldsID.layer.borderWidth = 1;
    _txtFieldsID.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    _txtFieldsID.textColor = COLOR_NATIONS_BLUE;
    
    _txtFieldsPass.layer.borderWidth = 1;
    _txtFieldsPass.layer.borderColor = COLOR_NATIONS_BLUE.CGColor;
    _txtFieldsPass.textColor = COLOR_NATIONS_BLUE;
    
    
    _missidMsg.font = FONT_NOTO_LIGHT(11);
    _missidMsg.textColor = COLOR_MAIN_RED;
    _missidMsg.text = @"아이디를 입력하세요.";
    
    _missPassMsg.font = FONT_NOTO_LIGHT(11);
    _missPassMsg.textColor = COLOR_MAIN_RED;
    _missPassMsg.text = @"비밀번호를 입력하세요.";
    
    _missidMsg.hidden = YES;
    _missPassMsg.hidden = YES;
    
    [_btnConfirm setTitle:@"확인" forState:UIControlStateNormal];
    [_btnConfirm backgroundRadiusButton:COLOR_NATIONS_BLUE];
}

- (IBAction)pressClose:(id)sender {

    [self dismiss:nil];
}

- (IBAction)pressAction:(id)sender {
//    [self dismiss:^{
//        if (self.delegate)
//            [self.delegate MallLoginAlert:self returnData:nil];
//    }];
    
    if ([_txtFieldsID.text length]==0) {
        _missidMsg.hidden = NO;
        return;
    }else if ([_txtFieldsPass.text length]==0) {
        _missPassMsg.hidden = NO;
        return;
    }
    
    [self requestLogin];
}



#pragma mark -requestData
- (void)requestLogin{
    
    [_model indicatorActive];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"gclife_login", @"api_code",
                                _txtFieldsID.text, @"gclife_id",
                                _txtFieldsPass.text, @"gclife_pw",
                                nil];
    [server serverCommunicationData:parameters];
}


#pragma mark - server delegate
-(void)delegateResultData:(NSDictionary*)result {
    [_model indicatorHidden];
    
    if([[result objectForKey:@"api_code"] isEqualToString:@"gclife_login"]){
        
        [self dismiss:^{
            if (self.delegate){
                
                NSString *gclife_id = @"";
                if ([[result objectForKey:@"data_yn"] isEqualToString:@"Y"]) {
                    gclife_id = [result objectForKey:@"gclife_id"];
                }
                
                NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [result objectForKey:@"data_yn"], @"data_yn",
                                      [result objectForKey:@"gclife_id"], @"gclife_id",
                                      nil];
                [self.delegate MallLoginAlert:self returnData:dic];
            }
        }];
        
    }
}

@end
