//
//  EmojiDefaultVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 21/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "EmojiDefaultVC.h"

@interface EmojiDefaultVC ()

@end

@implementation EmojiDefaultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    [self viewInit];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressChoies:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [self dismiss:^{
        if (self.delegate)
            [self.delegate EmojiDefaultVC:self clickButtonTag:btn.tag];
    }];
}

#pragma mark - viewInit
- (void)emojiType:(EmojiImgType)type {
    if (type == happy) {
        _emojiImgVw.image = [UIImage imageNamed:@"emoji_happy"];
    }else if (type == sad) {
        _emojiImgVw.image = [UIImage imageNamed:@"emoji_awkward"];
    }else if (type == error) {
        _emojiImgVw.image = [UIImage imageNamed:@"emoji_fail"];
    }else if (type == confirm) {
        _emojiImgVw.image = [UIImage imageNamed:@"emoji_confirm"];
    }else if (type == mail) {
        _emojiImgVw.image = [UIImage imageNamed:@"emoji_mail"];
    }
}

- (void)viewInit {
    _titleLbl.font = FONT_NOTO_REGULAR(16);
    _titleLbl.textColor = COLOR_GRAY_SUIT;
    _titleLbl.text = @"";
    
    _msgLbl.font = FONT_NOTO_REGULAR(16);
    _msgLbl.textColor = COLOR_GRAY_SUIT;
    _msgLbl.text = @"";
    
    _leftBtn.tag = 1;
    [_leftBtn setTitle:@"" forState:UIControlStateNormal];
    [_leftBtn borderRadiusButton:COLOR_GRAY_SUIT];
    
    _rightBtn.tag = 2;
    [_rightBtn setTitle:@"" forState:UIControlStateNormal];
    [_rightBtn backgroundRadiusButton:COLOR_NATIONS_BLUE];
    
    _titleVw.hidden = false;
    _msgVw.hidden = false;
    if ([_titleStr isEqualToString:@""] && [_msgStr isEqualToString:@""]) {
        _contairVwHeightConst.constant = 340;
    }else if ([_titleStr length] > 0 && [_msgStr length] > 0) {
        _contairVwHeightConst.constant = 340;
    }else {
        _contairVwHeightConst.constant = 300;
        if ([_msgStr length] > 0) {
            _msgLbl.textColor = COLOR_GRAY_SUIT;
            _titleVw.hidden = true;
        }else {
            _msgVw.hidden = true;
        }
    }
    
    self.titleLbl.textAlignment = NSTextAlignmentCenter;
    self.msgLbl.textAlignment = NSTextAlignmentCenter;
    if ([_titleStr isEqualToString:@"현대해상 보험 가입 고객이 아닌 경우\n준회원으로 가입되며 건강상담, 진료예약 등의 메디케어 서비스와 관련된 일부 기능이 제한됩니다."]) {
        self.titleLbl.textAlignment = NSTextAlignmentLeft;
        self.msgLbl.textAlignment = NSTextAlignmentLeft;
        _titleLbl.font = FONT_NOTO_REGULAR(14);
        _msgLbl.font = FONT_NOTO_REGULAR(14);
    }
    
    [_titleLbl setNotoText:_titleStr];
    [_msgLbl setNotoText:_msgStr];
    [_titleLbl sizeToFit];
    [_msgLbl sizeToFit];
//    [_contairVw layoutIfNeeded];
    
    _contairVwHeightConst.constant = _titleLbl.frame.size.height + _msgLbl.frame.size.height + 280;
    [_contairVw layoutIfNeeded];
    
    if (_leftStr.length < 1 && _rightStr.length < 1) {
        [_leftBtn setTitle:@"취소" forState:UIControlStateNormal];
        [_rightBtn setTitle:@"확인" forState:UIControlStateNormal];
    }else if (_leftStr.length > 0 && _rightStr.length < 1) {
        [_leftBtn setTitle:_leftStr forState:UIControlStateNormal];
        [_leftBtn borderRadiusButton:COLOR_NATIONS_BLUE];
        _rightVw.hidden = true;
    }else if (_leftStr.length < 1 && _rightStr.length > 0) {
        [_rightBtn setTitle:_rightStr forState:UIControlStateNormal];
        _leftBtn.hidden = true;
    }else {
        [_leftBtn setTitle:_leftStr forState:UIControlStateNormal];
        [_rightBtn setTitle:_rightStr forState:UIControlStateNormal];
    }
}
@end
