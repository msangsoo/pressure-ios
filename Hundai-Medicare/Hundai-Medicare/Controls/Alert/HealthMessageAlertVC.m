//
//  HealthMessageAlertVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "HealthMessageAlertVC.h"

@interface HealthMessageAlertVC ()

@end

@implementation HealthMessageAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    [self viewInit];
    
    _contairVw.alpha = 0;
    _contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contairVw.alpha = 1;
        self.contairVw.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss:(void (^)(void))completion {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contairVw.alpha = 0;
        self.contairVw.transform = CGAffineTransformMakeScale(0.5, 0.5);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:false completion:^{
            if(completion)
                completion();
        }];
    }];
}

#pragma mark - Actions
- (IBAction)pressCancel:(id)sender {
    [self dismiss:nil];
}

- (IBAction)pressTip:(UIButton *)sender {
    [self dismiss:^{
        if (self.delegate) {
            [self.delegate HealthMessageAlertVC:self type:self->_healthTypeStr actionsType:Tip];
        }
    }];
}

- (IBAction)pressCall:(UIButton *)sender {
    [self dismiss:^{
        if (self.delegate) {
            [self.delegate HealthMessageAlertVC:self type:self->_healthTypeStr actionsType:Call];
        }
    }];
}

- (IBAction)pressNotToday:(UIButton *)sender {
    [self dismiss:^{
        if (self.delegate) {
            if ([self.healthTypeStr isEqualToString:@"혈압"]) {
//                [[NSUserDefaults standardUserDefaults]  setObject:[NSDate date] forKey:PRESU_TODAY_ALERT];
            }else {
//                [[NSUserDefaults standardUserDefaults]  setObject:[NSDate date] forKey:SUGAR_TODAY_ALERT];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self.delegate HealthMessageAlertVC:self type:self->_healthTypeStr actionsType:Today];
        }
    }];
}

#pragma mark - viewInit
- (void)viewInit {
    _titleLbl.font = FONT_NOTO_MEDIUM(21);
    _titleLbl.textColor = COLOR_MAIN_DARK;
    [_titleLbl setNotoText:@"건강메시지"];
    
    self.msgTv.font = FONT_NOTO_REGULAR(15);
    self.msgTv.textColor = COLOR_GRAY_SUIT;
    self.msgTv.text = self.msgStr;
    
    CGFloat number = -1.5f;
    
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    
    NSString *tipStr = [NSString stringWithFormat:@"                      %@ 관리", _healthTypeStr];
    NSMutableAttributedString *firstStr = [[NSMutableAttributedString alloc] initWithString:tipStr];
    [firstStr addAttribute:NSKernAttributeName
                     value:[NSNumber numberWithFloat:number]
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSFontAttributeName
                     value:FONT_NOTO_BOLD(18)
                     range:NSMakeRange(0, tipStr.length)];
    [firstStr addAttribute:NSForegroundColorAttributeName
                     value:[UIColor whiteColor]
                     range:NSMakeRange(0, tipStr.length)];
    [attstr appendAttributedString:firstStr];
    
    NSString *secondString = @"TIP 보기                      ";
    NSMutableAttributedString *secondStr = [[NSMutableAttributedString alloc] initWithString:secondString];
    [secondStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:number]
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSFontAttributeName
                      value:FONT_NOTO_BOLD(18)
                      range:NSMakeRange(0, secondStr.length)];
    [secondStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor blackColor]
                      range:NSMakeRange(0, secondStr.length)];
    [attstr appendAttributedString:secondStr];
    
    [_tipBtn makeToRadius:true];
    [_tipBtn setAttributedTitle:attstr forState:UIControlStateNormal];
    
    _wordLbl.font = FONT_NOTO_LIGHT(14);
    _wordLbl.textColor = COLOR_GRAY_SUIT;
    [_wordLbl setNotoText:@"더 궁금하신 점이 있으신가요?\n전문 간호사와 전문의 건강상담을 받아보세요."];
    
    [_callBtn setTitle:@"     상담 받기" forState:UIControlStateNormal];
    [_callBtn setImage:[UIImage imageNamed:@"icon_blue_call"] forState:UIControlStateNormal];
    [_callBtn borderRadiusButton:COLOR_NATIONS_BLUE];
}


@end
