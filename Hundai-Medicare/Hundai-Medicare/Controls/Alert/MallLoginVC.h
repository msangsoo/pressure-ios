//
//  MallLoginVC.h
//  Hundai-Medicare
//
//  Created by Daesun moon on 26/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallLoginVC : BaseViewController<ServerCommunicationDelegate>

@property (nonatomic, retain) id delegate;

@property (weak, nonatomic) IBOutlet UIView *contairVw;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleIDLbl;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldsID;
@property (weak, nonatomic) IBOutlet UILabel *titlePassLbl;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldsPass;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UILabel *missidMsg;
@property (weak, nonatomic) IBOutlet UILabel *missPassMsg;

- (IBAction)pressAction:(id)sender;
- (IBAction)pressClose:(id)sender;

@end


@protocol MallLoginVCDelegate
- (void)MallLoginAlert:(MallLoginVC *)alertView returnData:(NSDictionary*)dic;
@end
