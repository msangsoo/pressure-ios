//
//  HanwhaDateViewController.h
//  LifeCare
//
//  Created by insystems company on 2017. 5. 30..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    Date,
    DateAndTime,
    Time
} DateControlDateType;

@interface DateControlVC : UIViewController {
    NSDateFormatter *_dateFormatter;
    
    IBOutlet UIBarButtonItem *btnLeft;
    IBOutlet UIBarButtonItem *btnRight;
    IBOutlet UIDatePicker *_picker;
}

@property (nonatomic, assign) long tag;
@property (nonatomic, retain) id delegate;

- (IBAction)pressLeft:(id)sender;
- (IBAction)pressRight:(id)sender;
- (void)dateControllerWithDelegate:(id)del dateType:(DateControlDateType)type defaultDate:(NSString *)strDate;

@end

@protocol DateControlVCDelegate

- (void)DateControlVC:(DateControlVC *)alertView selectedDate:(NSString *)date;
- (void)closeDateView:(DateControlVC *)alertView;

@end
