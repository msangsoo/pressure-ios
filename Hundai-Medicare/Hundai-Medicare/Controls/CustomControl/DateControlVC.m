//
//  HanwhaDateViewController.m
//  LifeCare
//
//  Created by insystems company on 2017. 5. 30..
//  Copyright © 2017년 insystems company. All rights reserved.
//

#import "DateControlVC.h"

@interface DateControlVC ()

@end

@implementation DateControlVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout =UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    self.view.tag = self.tag;
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
}

#pragma mark - Actions
- (IBAction)pressLeft:(id)sender {
    
    if(self.delegate)
        [self.delegate closeDateView:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressRight:(id)sender {
    
    NSDate *myDate = _picker.date;
    NSString *sDate = [_dateFormatter stringFromDate:myDate];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if(self.delegate)
            [self.delegate DateControlVC:self selectedDate:sDate];
    }];
}

#pragma mark - view setup
- (void)dateControllerWithDelegate:(id)del dateType:(DateControlDateType)type defaultDate:(NSString *)strDate {
    
    if (type == Date) {
        _picker.datePickerMode = UIDatePickerModeDate;
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFrom = [_dateFormatter dateFromString:strDate];
        [_picker setDate:dateFrom];
        
    }else if (type == DateAndTime) {
        _picker.datePickerMode = UIDatePickerModeDateAndTime;
        [_dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *dateFrom = [_dateFormatter dateFromString:strDate];
        [_picker setDate:dateFrom];
        
    }else if (type == Time) {
        _picker.datePickerMode = UIDatePickerModeTime;
        [_dateFormatter setDateFormat:@"HH:mm"];
        NSDate *dateFrom = [_dateFormatter dateFromString:strDate];
        [_picker setDate:dateFrom];
    }
    
    self.delegate = del;
}


@end
