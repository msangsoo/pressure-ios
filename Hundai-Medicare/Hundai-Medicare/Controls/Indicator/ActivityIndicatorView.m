//
//  CrowdSource
//
//  Created by insystems company on 2015. 10. 30..
//  Copyright © 2015년 digiparts. All rights reserved.
//

#import <QuartzCore/QuartzCore.h> 
#import "ActivityIndicatorView.h"

@implementation ActivityIndicatorView

@synthesize  _activityIndicator;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) { 
    }
    return self;
}

- (BOOL) prefersStatusBarHidden{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout =UIRectEdgeNone;
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    self._viewLoadingFrame.layer.cornerRadius = 5;
    self._viewLoadingFrame.layer.masksToBounds = true;
    
    [_btnCancel setHidden:YES];
    [_loginviewHeight setConstant:65.f];
    
}

- (void)doInit {
    self._lblLoadingMessage.hidden = YES;
    [_btnCancel setHidden:YES];
    self._viewLoadingFrame.backgroundColor  = RGBA(0, 0, 0, 0);
}

- (void)setLoadingMessage:(NSString*)message {
    
    self._lblLoadingMessage.hidden = NO;
    [_btnCancel setHidden:YES];
    [_loginviewHeight setConstant:65.f];
    self._viewLoadingFrame.backgroundColor  = RGBA(0, 0, 0, 0.5);
    [self._lblLoadingMessage setText:message];
    [self.view setNeedsDisplay];
}


- (void)setLoadingMessageButton:(NSString*)message del:(id)del{
    self.delegate = del;
    self._lblLoadingMessage.hidden = NO;
    [_btnCancel setHidden:NO];
    [_loginviewHeight setConstant:112.f];
    self._viewLoadingFrame.backgroundColor  = RGBA(0, 0, 0, 0.5);
    [self._lblLoadingMessage setText:message];
}

- (IBAction)pressCancel:(id)sender {
    if (self.delegate)
        [self.delegate ActivityIndicatorView:self clickedButtonAtIndex:0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; 
}

@end
