//
//  CrowdSource
//
//  Created by insystems company on 2015. 10. 30..
//  Copyright © 2015년 digiparts. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface ActivityIndicatorView : UIViewController

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain)IBOutlet UIActivityIndicatorView *_activityIndicator;
@property (nonatomic, retain)IBOutlet UILabel *_lblLoadingMessage;
@property (nonatomic, retain)IBOutlet UIView *_viewLoadingFrame;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginviewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

- (void)doInit;
- (void)setLoadingMessage:(NSString*)message;
- (void)setLoadingMessageButton:(NSString*)message del:(id)del;
- (IBAction)pressCancel:(id)sender;

@end

@protocol ActivityIndicatorViewDelegate

- (void)ActivityIndicatorView:(ActivityIndicatorView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end
