//
//  CommonWebVC.h
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface CommonWebVC : BaseViewController<UIWebViewDelegate>

@property (nonatomic, strong) NSString *paramURL;

@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end
