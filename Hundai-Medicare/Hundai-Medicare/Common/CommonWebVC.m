//
//  CommonWebVC.m
//  Hundai-Medicare
//
//  Created by INSYSTEMS CO., LTD on 23/03/2019.
//  Copyright © 2019 INSYSTEMS CO., LTD. All rights reserved.
//

#import "CommonWebVC.h"

@interface CommonWebVC ()

@end

@implementation CommonWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.paramURL]];
    self.webView.delegate = self;
    [self.webView loadRequest:url];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webViewDidStartLoad = %@", webView);
    [_model indicatorActive];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webViewDidFinishLoad = %@", webView);
    [_model indicatorHidden];
}

@end
